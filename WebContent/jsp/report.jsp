<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>RHIS_Report</title>

		<link href="../css/jquery-ui.min.css" rel="stylesheet" type="text/css">
		<link href="../css/multiple-select.css" rel="stylesheet" type="text/css">
		<link href="../css/style.css" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="../js/jquery-2.1.3.min.js"></script>
		<script type="text/javascript" src="../js/jquery-ui.min.js"></script>
		<script type="text/javascript" src="../js/jquery.multiple.select.js"></script>
		<script type="text/javascript" src="../js/moment.js"></script>
		<script type="text/javascript" src="../js/combodate.js"></script>
		<script type="text/javascript" src="../js/number-conversion.js"></script>
		<script>
		
		$(document).ready(function() {
		    var date = new Date();

		    var day = date.getDate();
		    var month = date.getMonth() + 1;
		    var year = date.getFullYear();

		    if (month < 10) month = "0" + month;
		    if (day < 10) day = "0" + day;

		    var today = year + "-" + month;    
		    $("#Xmonth").attr("value", today);
		    $("#Xmonth").val(moment($("#Xmonth").val()).format("MMM YYYY"));
		});

		</script>
		
</head>
<body class="REP">

<div id='formR'>
		<form action="../Report" method="post">
			
		<section class='formsR'>
			
			<table>
				<tbody>
					<tr><h2><b>ROUTINE HEALTH INFORMATION SYSTEM</b></h2>
			          <h3><b>REPORT MODULE</b></h3>
			            Please Enter Provider Id and Month 
			         </tr>
					<tr>
					    <td width='40%'>Month:</td>
						<td width='60%'><input type="month" id="Xmonth" name="Xmonth"></td>		
						</tr>
					<tr>
						<td class='msg' width='40%'></td>
						<td width='60%'><input type="Submit" id="report" class='design' value="Report"/></td>
					</tr>
				</tbody>
			</table>
			
		</section>
		</form>
</div>

<div id=tableR hidden="true">


 
</div>
</body>
</html>










    
    
  