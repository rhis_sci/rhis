<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%
	if( request.getAttribute("uID") == null || request.getAttribute("uName") == null){
		response.sendRedirect("../index.html");     
    }
%>
		<div id="menu" class="wrapper">
			<nav class="menu">	
				<ul>
					<li id ="homeMenu" class="home_selected"><a href="#">Home</a></li>
<%
	if(!String.valueOf(request.getAttribute("csba")).equals("1")){		
		out.println( "<li id ='reportMenu' class='report'><a href='#'>Report</a></li>"
				   + "<li id ='aboutMenu' class='about'><a href='#'>About</a></li>");
	}
%>					
					<!-- 
					<li id ="reportMenu" class="report"><a href="#">Report</a></li>
					<li id ="aboutMenu" class="about"><a href="#">About</a></li>
					
					<li><a>সেবা<span class="arrow">&#9660;</span></a>
					<ul class="sub-menu"><span class="serviceList">
						<li id="1"><a><span class="pregList">গর্ভকালীন সেবা</span></a></li>
						<li id="2"><a><span class="pregList">প্রসবকালীন সেবা</span></a></li>
						<li id="3"><a><span class="pregList">প্রসবোত্তর সেবা</span></a></li>
						<li id="4"><a><span class="pregList">গর্ভপাত পরবর্তী সেবা</span></a></li>
						<li id="5"><a><span class="othersList">এম আর সেবা</span></a></li>
						<li id="6"><a><span class="othersList">মৃত্যু</span></a></li>
						<li id="7"><a><span class="othersList">অন্যান্য সেবা ১</span></a></li>
						<li id="8"><a><span class="othersList">অন্যান্য সেবা ২</span></a></li>
						<li id="9"><a><span class="othersList">অন্যান্য সেবা ৩</span></a></li>
						<li id="10"><a><span class="othersList">অন্যান্য সেবা ৪</span></a></li>
						</span>
					</ul>
					</li>
					<li id="report"><a>রিপোর্ট </a></li>
					-->
					
					<div class="right">
						<li>
							<span id="providerid" hidden="true"><%= request.getAttribute("uID") %></span>
							<span id="providerType" hidden="true"><%= request.getAttribute("ProvType") %></span>
							<span id="csba" hidden="true"><%= request.getAttribute("csba") %></span>
							<span id="facName" hidden="true"><%= request.getAttribute("faciltyName") %></span>
							<b>[<span id="providerName"><%= request.getAttribute("uName") %></span>] &nbsp;&nbsp;
							<span id="zillaid" hidden="true"><%= request.getAttribute("zillaid") %></span>
							<span id="upazilaid" hidden="true"><%= request.getAttribute("upazilaid") %></span>
							<span id="unionid" hidden="true"><%= request.getAttribute("unionid") %></span>
							<a href="">[Logout]</a></b>
						</li>											
					</div>
										
				</ul>
			</nav>
		</div>
		<!-- 
		<div id="heading" class="heading">
		</div>
		 -->
		<div id="search" class="search">
			<div id="searchLabel" class="label">
				<select id = "searchOption">
					<option value="1" selected> HEALTH ID </option>
					<option value="2"> MOBILE NO. </option>
					<option value="3"> NATIONAL ID </option>
					<option value="4"> BIRTH REGISTRATION NO. </option>
					<!-- <option value="5"> NAME </option> -->
					<option value="5"> NRC ID </option>
					<option value="6"> ADVANCE SEARCH </option>
					<!-- <option value="7"> SERVICE BASED SEARCH </option>-->
				</select>
			</div>
			
			<div id="healthNumber" class="searchInput">
				<span><input id="searchString" type="text" style="height:20px;width:100%;"></span>
			</div>
			<div id="searchButton" class="searchImage">
                        <img id="search_Image" src="image/find.png" style="width:20%; height:18px;">
            </div>
            <div id="notRegisteredOption">
                         <input type="button" class="hasNoHealthIdCSS" id="hasNoHealthId" value="Register New Client"/>
            </div>
		</div>
		
		<div id="register_personal" class="person_detail">
			<div id="personalDetailNonReg" class="person_detailleft">
				<div id="subheading" class="subheadingside">
					সেবাগ্রহীতার পরিচিতি 			
				</div>
				
				<table width="100%">
					<tbody>
						<tr>
							<td>নাম*</td><td>	<input type="text" id="RclientName"/></td>
						
							<td>বয়স*</td><td>	<input type="text" id="RclientAge" maxlength="3" size="2"/></td>
							
							<td>লিঙ্গ*</td><td>	
								<select id="RclientGender">
									<option value="1" >পুরুষ</option>
									<option value="2" selected>মহিলা</option>
									<option value="3">হিজরা</option>
								</select>
							</td>													
						</tr>	
						<tr>
							<td>স্বামীর নাম	</td><td><input type="text" id="RclientHusbandName"/></td>
							
							<td>পিতার নাম*	</td><td><input type="text" id="RclientFatherName"/></td>
							
							<td>মাতার নাম*	</td><td><input type="text" id="RclientMotherName"/></td>
						</tr>
						<tr>
							<td>জেলা*</td><td>	<SELECT id="RzillaName"></SELECT></td>		
															
							<td>উপজেলা*</td><td>	<SELECT id="RupaZilaName"></SELECT></td>
							
							<td>ইউনিয়ন*</td><td>	<SELECT id="RunionName"></SELECT></td>													
						</tr>
						<tr>
							<td>গ্রাম / মহল্লা</td><td>	<SELECT id="RvillageName"></SELECT></td>
							
							<td>বাড়ি / জিআর / হোল্ডিং নম্বর</td><td>	<input type="text" id="RhhNo" maxlength="5" size="4"/></td>
							
							<td>মোবাইল নম্বর</td><td>	<input type="text" id="RcellNo" maxlength="11" size="11"/></td>
						</tr>
						
					</tbody>
				</table>
				<span class="mandatory">(*) চিহ্নিত ঘর অবশ্যই পূরণ করতে হবে</span>
				&nbsp;<input type = "button" class="right" id = "healthIdGen" value="Next">
				<br>
			</div>
		</div>
 
		<div id="personal" class="person_detail">
			<div id="personalReg" class="person_detailleft">
				<div id="subheading" class="subheadingsideLeft">
					রেজি: নম্বর <span id="regNumber"></span>   &nbsp; <span id="regDate"></span>
					<span id="regNoMNCH" hidden="true"></span><span id="regDateMNCH" hidden="true"></span>
				</div>
			</div>
			<div id="personalDetail" class="person_detailleft">
				<div id="subheading" class="subheadingside">
					সেবাগ্রহীতার পরিচিতি<br>					
				<span class="font_size_english"><span id="healthIdTitle"></span><span id="visibleHealthid"></span></span>
				</div>
				<span id="healthid" hidden="true"></span>
				<span id="deathStatus" hidden="true"></span>
				<span id="gender" hidden="true"></span>
				<table width="100%">
					<tbody>
						<tr>
							<td>নাম</td><td>	<input type="text" id="clientName" disabled/></td>
						
							<!-- <td>বয়স</td><td>	<input type="text" id="clientAge" maxlength="3" size="4" disabled/></td>
							<td colspan="2">বয়স &nbsp;&nbsp;<input type="text" id="clientAge" maxlength="3" size="3" disabled/>&nbsp;&nbsp;
							লিঙ্গ &nbsp;&nbsp;<input type="text" id="clientGender" size="4" disabled/></td>-->
							
							<td>বয়স </td> <td><input type="text" id="clientAge" maxlength="3" size="4" disabled/>&nbsp;
							লিঙ্গ &nbsp;<input type="text" id="clientGender" size="4" disabled/></td>
						
							<td>স্বামীর নাম	</td><td><input type="text" id="clientHusbandName" disabled/></td>						
						</tr>	
						<tr>
							<td>জেলা</td><td><input type="text" id="zillaName" disabled/></td>									
						
						
							<td>উপজেলা</td><td>	<input type="text" id="upaZilaName" disabled/> </td>
							
							<td>ইউনিয়ন</td><td>	<input type="text" id="unionName" disabled/></td>
						</tr>
						<tr>								
							<td>গ্রাম / মহল্লা</td><td>	<input type="text" id="villageName" disabled/></td>
							
							<td>ওয়ার্ড নম্বর	</td><td>	<input type="text" id="wardNo" maxlength="5" size="4" disabled/></td>
						
							<td>ইউনিট নম্বর</td><td>	<input type="text" id="unitNo" maxlength="5" size="4" disabled/></td>
						</tr>
						<tr>
							<td>বাড়ি / জিআর / হোল্ডিং নম্বর</td><td>	<input type="text" id="hhNo" maxlength="5" size="4"/></td>
						
							<td>দম্পতি নম্বর</td><td>	<input type="text" id="elcoNo" maxlength="4" size="4" disabled/></td>
						
							<td>মোবাইল নম্বর</td><td>	<input type="text" id="cellNo" maxlength="11" size="11"/></td>
						</tr>		
					</tbody>
				</table>
			</div>
			
			<div id="service_tab"> 
				<button class="serviceTabButton" id="21">মা ও নবজাতকের সেবা</button>
				<button class="serviceTabButton" id="22">পরিবার পরিকল্পনা সেবা</button>
				<button class="serviceTabButton" id="14">সাধারণ রোগী সেবা</button>
				<!-- <button class="serviceTabButton" id="15">শিশু সেবা</button>
				<button class="serviceTabButton" id="7">গর্ভ নিরোধক খাবার বড়ি ও কনডম বিতরণ সেবা</button>
				<button class="serviceTabButton" id="8">গর্ভ নিরোধক ইনজেকটেবলস সেবা</button>
				<button class="serviceTabButton" id="5">এম আর সেবা</button>-->
				<button class="serviceTabButton" id="6">মৃত্যু</button>								
			</div>
							
			<div id="personalInfo" class="person_detailleft">
				<div id="subheading" class="subheadingside">গর্ভবতী মহিলার তথ্য</div>
				<span id="pregNumber" hidden="true"></span>
				<table width="100%">
					<tbody>
						<tr>
							<td>
							<span class="tp" id="lmp_tp" >   শেষ মাসিকের তারিখ*	</span><input type="text" id="lmp" maxlength="11" size="11"/><br>
								<span class="tp" id="lmpDisagreement" style = "color:blue;font-size:14px;" hidden="true"></span>
							</td>
							
							<td><span class="tp" id="edd_tp" >প্রসবের সম্ভাব্য তারিখ	</span><input type="text" id="edd" maxlength="11" size="11"/></td>
				
				
				
							<td>
				
							<span class="tp" id ="para_tp">প্যারা*</span>&nbsp;<input type="text" id="para" maxlength="2" size="2"/>
							<span class="tp" id ="gravida_tp">গ্রাভিডা*</span>	<input type="text" id="gravida" maxlength="2" size="2"/>
							
							</td>
							
						</tr>
				
						<tr>
							<td>
							
								<span class="tp" id ="alive_child_no">জীবিত সন্তান সংখ্যা&nbsp;</span>
								ছেলে	<input type="text" id="boy" maxlength="2" size="2"/>&nbsp;
								মেয়ে	<input type="text" id="girl" maxlength="2" size="2"/>&nbsp;								
							</td>
	
							<td>শেষ সন্তানের বয়স*&nbsp;&nbsp;&nbsp;<input type="text" id="lastChildAgeYear" maxlength="2" size="2"/>&nbsp;বছর <input type="text" id="lastChildAgeMonth" maxlength="2" size="2"/>&nbsp;মাস </td>

							
							<td>উচ্চতা &nbsp;<input type="text" id="hfoot" maxlength="2" size="2"/>&nbsp;ফুট 	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" id="hinch" maxlength="2" size="2"/>&nbsp;ইঞ্চি </td>
													
						</tr>
						<tr>
							<td>রক্তের গ্রুপ		
								<select id="bloodGroup">
									<option value="0"></option>
									<option value="1">A+</option>
									<option value="2">A-</option>
									<option value="3">B+</option>
									<option value="4">B-</option>
									<option value="5">AB+</option>
									<option value="6">AB-</option>
									<option value="7">O+</option>
									<option value="8">O-</option>
								</select>
								&nbsp;&nbsp;&nbsp;
							</td>
							
							<td>টিটি টিকা:	
								<span id="tt1span" title="" style="cursor:help">১ <input type="checkbox" id="tt1" title="" value=""/><span id="tt1Date" hidden="true"></span></span>
								<span id="tt2span" title="" style="cursor:help">২ <input type="checkbox" id="tt2" title="" value=""/><span id="tt2Date" hidden="true"></span></span>
								<span id="tt3span" title="" style="cursor:help">৩ <input type="checkbox" id="tt3" title="" value=""/><span id="tt3Date" hidden="true"></span></span>
								<span id="tt4span" title="" style="cursor:help">৪ <input type="checkbox" id="tt4" title="" value=""/><span id="tt4Date" hidden="true"></span></span>
								<span id="tt5span" title="" style="cursor:help">৫ <input type="checkbox" id="tt5" title="" value=""/><span id="tt5Date" hidden="true"></span></span>
								&nbsp;&nbsp;&nbsp;
							</td>							
						</tr>						
					</tbody>
				</table>
				<table>
					<tbody>
						<tr>
							<td>
								<span id="pregHistory">
								<br><span class="tp" id="pregHistory_tp">পূর্ববর্তী গর্ভের ইতিহাস:</span><br>
								<input type="checkbox" id="bloodPreg"/> রক্তপাত (প্রসবপূর্ব অথবা প্রসবোত্তর) &nbsp;&nbsp;
								<input type="checkbox" id="delayedDeliveryPreg"/> বিলম্বিত প্রসব &nbsp;&nbsp;
								<input type="checkbox" id="blockedDeliveryPreg"/> বাধাগ্রস্থ প্রসব &nbsp;&nbsp;
								<input type="checkbox" id="uterusPreg"/> গর্ভফুল না পড়া &nbsp;&nbsp;
								<input type="checkbox" id="stillBirthPreg"/> মৃত সন্তান প্রসব করা &nbsp;&nbsp;<br>
								<input type="checkbox" id="newbornDeathPreg"/> ৪৮ ঘন্টার মধ্যে নবজাতক মরে যাওয়া &nbsp;&nbsp;
								<input type="checkbox" id="edemaPreg"/> পা বা সমস্ত শরীর বেশি রকম ফোলা (ইডিমা) &nbsp;&nbsp;
								<input type="checkbox" id="eclampsiaPreg"/> একল্যাম্পসিয়া বা খিঁচুনিসহ বার বার অজ্ঞান হওয়া &nbsp;&nbsp;<br>
								<input type="checkbox" id="csectionPreg"/> সিজারিয়ান অপারেশন বা যন্ত্রের মাধ্যমে (ফরসেফ/ভেকুয়াম) প্রসব &nbsp;&nbsp;
								</span>
							</td>
						</tr>
					</tbody>
				</table>				
				<table style="width:100%;">
					<tbody>
						<tr>
							<td style="flaot:left;"><span class="mandatory">(*) চিহ্নিত ঘর অবশ্যই পূরণ করতে হবে</span></td>
						
							<td id="personalInfoEditColumn" class="right"><input type="button" id="personalInfoEditButton" width="25px" height="15px" value="Edit"></td>

							<td id="personalInfoUpdateColumn" class="right"><input type="button" id="personalInfoUpdateButton" width="25px" height="15px" value="Update"></td>
							
							<td id="personalInfoSaveColumn" class="right"><input type="button" id="personalInfoSaveButton" width="25px" height="15px" value="Save"></td>

							<td id="personalInfoNewColumn" class="right"><span class="tp" id="Add_new_preg_info"><input type="button" id="personalInfoNewButton" width="25px" height="15px" value="Add new pregnancy info"></span></td>						
						</tr>
						
					</tbody>
				</table>
			</div>
			
			<div id="service_tab_MNCH"> 
				<button class="serviceTabButton" id="1">গর্ভকালীন সেবা</button>
				<button class="serviceTabButton" id="2">প্রসবকালীন সেবা</button>
				<button class="serviceTabButton" id="3">প্রসবোত্তর সেবা</button>
				<button class="serviceTabButton" id="4">গর্ভপাত পরবর্তী সেবা</button>				
			</div>
			
			<div id="service_tab_FP"> 
				<button class="serviceTabButton" id="7">গর্ভ নিরোধক খাবার বড়ি ও কনডম বিতরণ সেবা</button>
				<button class="serviceTabButton" id="8">গর্ভ নিরোধক ইনজেকটেবলস সেবা</button>
				<button class="serviceTabButton" id="9">আই ইউ ডি সেবা</button>								
			</div>
												
		</div>	
	
	<div id="heading" class="heading">
	</div>
		
	<!--  	
	<div id="ancVisitOpen" class="wrapper">
		<div id="subheadingANCSlider" class="subheadingside"><span id="subheadingANCSliderSpan" style="cursor:pointer;">  গর্ভকালীন সেবা (+)</span></div>
	</div>
	-->
		
	<div id="anc" class="person_detail">			
		<div id="ancVisit" class="person_ANCdetail">
			<table>
				<tbody>
					<tr>
						<td><b>আদর্শ গর্ভকালীন পরিদর্শনের সময়:</b></td>
					</tr>
					<tr>
						<td>
							<table width="924px" border="1px">
								<tbody>
									<tr>
										<td> পরিদর্শন ১ </td>
										<td> পরিদর্শন ২ </td>
										<td> পরিদর্শন ৩ </td>
										<td> পরিদর্শন ৪ </td>
									</tr>
									<tr>
										<td class="ideal"><span id="idealANC1Date" style="font-size:14px;"></span>&nbsp;এর মধ্যে</td>
										<td class="ideal"><span id="idealANC2Date" style="font-size:14px;"></span></td>
										<td class="ideal"><span id="idealANC3Date" style="font-size:14px;"></span></td>
										<td class="ideal"><span id="idealANC4Date" style="font-size:14px;"></span></td>			
									</tr>
								</tbody>
							</table>
						</td>						
					</tr>
				</tbody>
			</table>			
			<div id="subheading" class="subheadingside">বর্তমান সেবার তথ্য </div>				
		</div>
	</div>
	<!--
	<div id="deliveryOpen" class="wrapper">
			<div id="subheadingDeliverySlider" class="subheadingside"><span id="subheadingDeliverySliderSpan" style="cursor:pointer;"> প্রসব সংক্রান্ত তথ্য (+)</span></div>
	</div>	
	-->
	<div id="delivery" class="person_detail">
		
		<div id="deliveryInfo" class="person_Deliverydetail">
				<div id="subheading" class="subheadingside">প্রসব সংক্রান্ত তথ্য</div>
				<span id="hasDeliveryInfo" hidden="true"></span>
				<span id="hasAbortionInfo" hidden="true"></span>
				<!-- <input type="radio" id="deliverySelection" name="outcomeProcessSelection" value="1"/> প্রসব  <input type="radio" id="abortionSelection" name="outcomeProcessSelection" value="2"/> গর্ভপাত  <br>
				<span id="outcomeProcess" hidden="true">-->
				<table width="100%">
					<tbody>
						<tr>
							<td>প্রসবের স্থান*  
								<select id="deliveryPlace">
									<option value="0" ></option>
									<option value="1"> বাড়ি </option>
									<option value="2" selected> সেবাকেন্দ্র </option>									
								</select>
							</td>
														
							<td>প্রসবের তারিখ*   	<input type="text" id="deliveryDate" maxlength="11" size="11" /></td>
							
							<td>প্রসবের সময়	<input type="text" id="deliveryTimeHour" maxlength="2" size="2"/>:<input type="text" id="deliveryTimeMinute" maxlength="2" size="2"/>&nbsp;&nbsp;&nbsp;<select id="ampm"><option value="AM" selected>AM</option><option value="PM">PM</option></select></td>
							
							<td>প্রসবের ধরণ* 
								<select id="deliveryType">
									<option value="0"> </option>
									<option value="1"> স্বাভাবিক </option>
									<option value="2"> সিজারিয়ান </option>
									<!-- <option value="3"> গর্ভপাত </option>-->
								</select>
							</td>				
						</tr>
					</tbody>
				</table>
					
				<table width="100%">
					<tbody>		
						<tr id="centerRelatedInfoRow">
							<td id="centerRelatedInfoColumn" width="100%">
								সেবাকেন্দ্রের নাম 
								<select id="deliveryPlaceCenter">										
								</select>&nbsp;&nbsp;&nbsp;&nbsp;									
								<span id="csba1" >
								আগমনের তারিখ <input type="text" id="admissionDate" maxlength="11" size="11"/>&nbsp;&nbsp;&nbsp;&nbsp;
								<span id="wardBedSection" hidden="true">
								ওয়ার্ড <input type="text" id="wardN" maxlength="20" size="4"/> &nbsp;&nbsp;
								শয্যা <input type="text" id="bedN" maxlength="12" size="4"/>
								</span>
								</span>
							</td>																		
						</tr>
					</tbody>
				</table>
				<span id="abortion">
				<table style="width:100%;">
					<tbody>		
						<tr>
							<td>							
								<span id="delivery_result">
									<span class="tp" id="delivery_result_tp">প্রসবের ফলাফল:</span>&nbsp;&nbsp;
									<span class="sectionHighlight">
									জীবিত জন্ম 	<input type="text" id="liveBirth" maxlength="2" size="1" disabled/>
									</span>&nbsp;||&nbsp;
									<span class="sectionHighlight">
									মৃত জন্ম:&nbsp;<span id="stillBirth"></span>&nbsp;
									ফ্রেশ	<input type="text" id="stillBirthFresh" maxlength="2" size="1" disabled/>&nbsp;
									ম্যাসারেটেড	<input type="text" id="stillBirthMacerated" maxlength="2" size="1" disabled/>								
									</span>&nbsp;||&nbsp;									
									<span class="sectionHighlight">
									লিঙ্গ:&nbsp;
									ছেলে <input type="text" id="newbornBoy" maxlength="2" size="1" disabled/>&nbsp;
									মেয়ে <input type="text" id="newbornGirl" maxlength="2" size="1" disabled/>&nbsp;
									সনাক্ত করা যাইনি <input type="text" id="newbornUnidentified" maxlength="2" size="1" disabled/>
									</span>
								</span>							
							</td>
						</tr>						
					</tbody>
				</table>
				&nbsp;<br>প্রসবের তৃতীয় ধাপের সক্রিয় ব্যবস্থাপনায় <span class="tp" id="amtsl_tp">(AMTSL)</span> যা অনুসরণ করা হয়েছে:<br>
				<div id="amtsl">
					<input type="checkbox" id="applyOxytocin"/> প্রসবের ১ মিনিটের মধ্যে অক্সিটোসিন প্রয়োগ &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="checkbox" id="applyTraction"/> গর্ভফুল প্রসবের জন্য নাভিরজ্জুতে নিয়ন্ত্রিত টান &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="checkbox" id="uterusMassage"/> জরায়ু ম্যাসাজ  
				</div>							
				
				<span id="epsiotomySpan">
				&nbsp;ইপিসিওটমি করা হয়েছে? <input type="radio" id="episiotomyYes" name="episiotomy" value="1"/> হ্যাঁ  <input type="radio" id="episiotomyNo" name="episiotomy" value="2"/> না<br>
				</span>
				&nbsp;<span class="tp" id="Miso_instead_Oxi">অক্সিটোসিন না থাকলে মিসোপ্রোস্টল খাওয়ানো হয়েছে?</span><input type="radio" id="misoprostolYes" name="misoprostol" value="1"/> হ্যাঁ <input type="radio" id="misoprostolNo" name="misoprostol" value="2"/> না <br>
				</span>
				<br>
				
				<span id="csba2">
				<input type="checkbox" id="deliveryDoneByProvider"/> আমি প্রসব সম্পাদনকারী <br>
				<span class="tp" id="abortionC">
				&nbsp;প্রসব সম্পাদনকারীর নাম </span><input type="text" id="deliveryDoneByPerson"/>&nbsp;<span class="tp" id="abortionC2"> পদবী</span>
					<select id="deliveryDoneByDesignation">												
					</select>
				<br>
				</span>
				<div id="delivery_complexity">
				&nbsp;<span class="tp" id="delivery_complexity_tp" >প্রসব সংক্রান্ত জটিলতা:</span><br>				
					<input type="checkbox" id="excessBloodLoss"/> অতিরিক্ত রক্তক্ষরণ &nbsp;&nbsp;&nbsp;
					<span id="abortion1">
					<input type="checkbox" id="lateDelivery"/> বিলম্বিত প্রসব &nbsp;&nbsp;&nbsp;
					<input type="checkbox" id="blockedDelivery"/> বাধাগ্রস্থ প্রসব &nbsp;&nbsp;&nbsp;
					<input type="checkbox" id="placenta"/> গর্ভফুল না পড়া &nbsp;&nbsp;&nbsp;
					</span>
					<input type="checkbox" id="headache"/> প্রচন্ড মাথা ব্যাথা &nbsp;&nbsp;&nbsp;
					<input type="checkbox" id="blurryVision"/> চোখে ঝাপসা দেখা 	
					<span  id="abortion2">
					<br>
					<input type="checkbox" id="otherBodyPart"/> মাথা ছাড়া অন্য কোন অঙ্গ বের হওয়া &nbsp;&nbsp;&nbsp;
					</span>
					<input type="checkbox" id="convulsions"/> খিচুঁনি &nbsp;&nbsp;&nbsp;
					<input type="checkbox" id="others"/> অন্যান্য  <input type="text" id="otherReason" hidden="true"/>
				</div>
		
				<span  id="abortion3">							
				<span id="treatment">&nbsp;চিকিৎসা 
					<select id="dTreatment" multiple="multiple">															
					</select>
				</span>&nbsp;&nbsp;&nbsp;&nbsp;					
				পরামর্শ	
					<select id="dAdvice" multiple="multiple">															
					</select>
				<br>								
				<input type="checkbox" id="deliveryRefer"/> রেফার  &nbsp;&nbsp;&nbsp;&nbsp;
				<span id="dReferRest" hidden="true">
					কেন্দ্রের নাম	
					<select id="dReferCenter" >					
					</select>&nbsp;&nbsp;&nbsp;&nbsp;	
										
					কারণ	
					<select id="dReferReason" multiple="multiple">																			
					</select>&nbsp;&nbsp;&nbsp;&nbsp;										
				</span>
				<br>
				</span>
					<span class="mandatory">(*) চিহ্নিত ঘর অবশ্যই পূরণ করতে হবে</span>						
				<span id="dButtonSaveColumn"><input type="button" id="saveButtonDelivery" class="right" width="25px" height="15px" value="Save"></span>&nbsp;
				<span id="dButtonEditColumn"><input type="button" id="editButtonDelivery" class="right" width="25px" height="15px" value="Edit"></span>&nbsp;				
				<span id="dButtonUpdateColumn"><input type="button" id="updateButtonDelivery" class="right" width="25px" height="15px" value="Update"></span>&nbsp;
				<!-- </span>-->				
			</div>
		</div>
	
	<div id="newborn" class="person_detail">
			
			<div id="newBornInfo" class="person_detailleft">
				<div id="subheading" class="subheadingside">নবজাতকের সংক্রান্ত তথ্য</div>
				<span id="childCount" hidden="true"></span>
				<span id="outcomePlace" hidden="true"></span>
				<span id="outcomeDate" hidden="true"></span>
				<span id="outcomeTime" hidden="true"></span>
				<span id="outcomeType" hidden="true"></span>
				<span id="immatureBirthInfo" hidden="true"></span>
				<span id="immatureBirthInfoWeek" hidden="true"></span>			
				<span id="addNewBorn">
					&nbsp;<input type="button" id="addLiveBirth" width="25px" height="15px" value="নবজাতক সংযোজন">&nbsp;&nbsp;&nbsp;
					<input type="button" id="addStillBirthFresh" width="25px" height="15px" value="মৃত জন্ম সংযোজন: ফ্রেশ">&nbsp;&nbsp;&nbsp;
					<input type="button" id="addStillBirthMacerated" width="25px" height="15px" value="মৃত জন্ম সংযোজন: ম্যাসারেটেড">
					<span id="deleteLastNewborn" hidden="true"><input type="button" id="deleteLastButtonNewborn" class="right" width="25px" height="15px" value="Delete Last"></span>
				</span>
				
				<div id="newBornInfo_1" class="person_detailright">	
					<table id="newbornHighlight1" style="width:100%;"><tbody><tr><td>সন্তান: ১ <span id="birthstat1"></span>&nbsp; <span id="immatureBirthWeekSection1"></span></td></tr></tbody></table><!-- id="newbornHighlight1" style="width:910px;"> সন্তান: ১ <span id="birthstat1"></span>&nbsp; <span id="immatureBirthWeekSection1"></span></span>--><hr>	
					<span id="birthStatus1" hidden="true"></span>
					<input type="radio" id="newBornSexMale1" name="newBornSex1" value="1"/>ছেলে  <input type="radio" id="newBornSexFemale1" name="newBornSex1" value="2"/>মেয়ে 
					<span id="macerated0_1">
					<input type="radio" id="newbornSexUnidentified1" name="newBornSex1" value="3"/>সনাক্ত করা যাই নি
					</span>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<span id="macerated1_1">
					জন্ম ওজন (কেজি)	<input type="text" id="birthWeight1" maxlength="4" size="4"/> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					</span>
					<!-- অপরিণত জন্ম (৩৭ সপ্তাহ পূর্ণ হওয়ার আগে):	<input type="radio" id="immatureBirthYes1" name="immatureBirth1" value="1"/>হ্যাঁ  <input type="radio" id="immatureBirthNo1" name="immatureBirth1" value="2"/>না--><br>
					<span id="macerated2_1">		
					১ মিনিটের মধ্যে মোছানো হয়েছে:	<input type="radio" id="washAfterBirthYes1" name="washAfterBirth1" value="1"/>হ্যাঁ  <input type="radio" id="washAfterBirthNo1" name="washAfterBirth1" value="2"/>না&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;											
					
					<span  id="resastation1">
					রিসাসসিটেশন:</span>	<input type="radio" id="resassitationYes1" name="resassitation1" value="1"/>হ্যাঁ  <input type="radio" id="resassitationNo1" name="resassitation1" value="2"/>না &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						
					<span id="newBornresassitation1" hidden="true">
					<input type="checkbox" id="stimulation1"/>স্টিমুলেশন  &nbsp;&nbsp;&nbsp;
							
					<input type="checkbox" id="bagNMask1"/>ব্যাগ ও মাস্ক ব্যবহার 	
					</span>
					
					<br>
					<span id="fresh1">
					নাড়ি কাটার পর নাড়িতে ৭.১% ক্লোরহেক্সিডিন দ্রবন ব্যবহার করা হয়েছে:	<input type="radio" id="chlorehexidinYes1" name="chlorehexidin1" value="1"/>হ্যাঁ  <input type="radio" id="chlorehexidinNo1" name="chlorehexidin1" value="2"/>না <br>
					
					নাড়ি কাটার পর মায়ের ত্বকে নবজাতককে লাগানো হয়েছে:	<input type="radio" id="skinTouchYes1" name="skinTouch1" value="1"/>হ্যাঁ  <input type="radio" id="skinTouchNo1" name="skinTouch1" value="2"/>না<br>
							
					জন্মের ১ ঘন্টার মধ্যে বুকের দুধ খাওয়ানো হয়েছে:	<input type="radio" id="breastfeedYes1" name="breastfeed1" value="1"/>হ্যাঁ  <input type="radio" id="breastfeedNo1" name="breastfeed1" value="2"/>না<br>
													
					<input type="checkbox" id="newBornRefer1"/> রেফার &nbsp;&nbsp;&nbsp;&nbsp;
					<span id="newBornReferSpan1" hidden="true">
								
						কেন্দ্রের নাম	
						<select id="newBornReferCenter1" >							
						</select>&nbsp;&nbsp;&nbsp;&nbsp;	
											
						কারণ	
						<select id="newBornReferReason1" multiple="multiple">																			
						</select>&nbsp;&nbsp;&nbsp;&nbsp;										
					
					</span>
					</span>
					</span>
					<br>
					<input type="button" id="newbornSave1" class="right" width="25px" height="15px" value="Save">				
				</div>	
			
				<div id="newBornInfo_2" class="person_detailright">	
					<table id="newbornHighlight2" style="width:100%;"><tbody><tr><td>সন্তান: ২ <span id="birthstat2"></span>&nbsp; <span id="immatureBirthWeekSection2"></span></td></tr></tbody></table><!-- <span id="newbornHighlight2">সন্তান: ২ <span id="birthstat2"></span>&nbsp; <span id="immatureBirthWeekSection2"></span></span>--><hr>	
					<span id="birthStatus2" hidden="true"></span>
					<input type="radio" id="newBornSexMale2" name="newBornSex2" value="1"/>ছেলে  <input type="radio" id="newBornSexFemale2" name="newBornSex2" value="2"/>মেয়ে 
					<span id="macerated0_2">
					<input type="radio" id="newbornSexUnidentified2" name="newBornSex2" value="3"/>সনাক্ত করা যাই নি
					</span>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<span id="macerated1_2">
					জন্ম ওজন (কেজি):	<input type="text" id="birthWeight2" maxlength="4" size="4"/> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					</span>
					<!-- অপরিণত জন্ম (৩৭ সপ্তাহ পূর্ণ হওয়ার আগে):	<input type="radio" id="immatureBirthYes2" name="immatureBirth2" value="1"/>হ্যাঁ  <input type="radio" id="immatureBirthNo2" name="immatureBirth2" value="2"/>না--><br>
					<span id="macerated2_2">		
					১ মিনিটের মধ্যে মোছানো হয়েছে:	<input type="radio" id="washAfterBirthYes2" name="washAfterBirth2" value="1"/>হ্যাঁ  <input type="radio" id="washAfterBirthNo2" name="washAfterBirth2" value="2"/>না&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;											
					<span class="tp" id="resastation2">
					রিসাসসিটেশন:	</span><input type="radio" id="resassitationYes2" name="resassitation2" value="1"/>হ্যাঁ  <input type="radio" id="resassitationNo2" name="resassitation2" value="2"/>না &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						
					<span id="newBornresassitation2" hidden="true">
					<input type="checkbox" id="stimulation2"/>স্টিমুলেশন  &nbsp;&nbsp;&nbsp;
							
					<input type="checkbox" id="bagNMask2"/>ব্যাগ ও মাস্ক ব্যবহার 	
					</span>
					<br>
					<span id="fresh2">
					নাড়ি কাটার পর নাড়িতে ৭.১% ক্লোরহেক্সিডিন দ্রবন ব্যবহার করা হয়েছে:	<input type="radio" id="chlorehexidinYes2" name="chlorehexidin2" value="1"/>হ্যাঁ  <input type="radio" id="chlorehexidinNo2" name="chlorehexidin2" value="2"/>না <br>
					
					নাড়ি কাটার পর মায়ের ত্বকে নবজাতককে লাগানো হয়েছে:	<input type="radio" id="skinTouchYes2" name="skinTouch2" value="1"/>হ্যাঁ  <input type="radio" id="skinTouchNo2" name="skinTouch2" value="2"/>না<br>
							
					জন্মের ১ ঘন্টার মধ্যে বুকের দুধ খাওয়ানো হয়েছে:	<input type="radio" id="breastfeedYes2" name="breastfeed2" value="1"/>হ্যাঁ  <input type="radio" id="breastfeedNo2" name="breastfeed2" value="2"/>না<br>
													
					<input type="checkbox" id="newBornRefer2"/> রেফার &nbsp;&nbsp;&nbsp;&nbsp;
					<span id="newBornReferSpan2" hidden="true">
								
						কেন্দ্রের নাম	
						<select id="newBornReferCenter2" >							
						</select>&nbsp;&nbsp;&nbsp;&nbsp;	
											
						কারণ	
						<select id="newBornReferReason2" multiple="multiple">																				
						</select>&nbsp;&nbsp;&nbsp;&nbsp;										
					
					</span>
					</span>
					</span>
					<br>
					<input type="button" id="newbornSave2" class="right" width="25px" height="15px" value="Save">				
				</div>
			
				<div id="newBornInfo_3" class="person_detailright">	
					<table id="newbornHighlight3" style="width:100%;"><tbody><tr><td>সন্তান: ৩ <span id="birthstat3"></span>&nbsp; <span id="immatureBirthWeekSection3"></span></td></tr></tbody></table><!-- <span id="newbornHighlight3">সন্তান: ৩ <span id="birthstat3"></span>&nbsp; <span id="immatureBirthWeekSection3"></span></span>--><hr>
					<span id="birthStatus3" hidden="true"></span>
					<input type="radio" id="newBornSexMale3" name="newBornSex3" value="1"/>ছেলে  <input type="radio" id="newBornSexFemale3" name="newBornSex3" value="2"/>মেয়ে 
					<span id="macerated0_3">
					<input type="radio" id="newbornSexUnidentified3" name="newBornSex3" value="3"/>সনাক্ত করা যাই নি
					</span>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<span id="macerated1_3">
					জন্ম ওজন (কেজি)	<input type="text" id="birthWeight3" maxlength="4" size="4"/> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					</span>
					<!-- অপরিণত জন্ম (৩৭ সপ্তাহ পূর্ণ হওয়ার আগে):	<input type="radio" id="immatureBirthYes3" name="immatureBirth3" value="1"/>হ্যাঁ  <input type="radio" id="immatureBirthNo3" name="immatureBirth3" value="2"/>না --><br>
					<span id="macerated2_3">		
					১ মিনিটের মধ্যে মোছানো হয়েছে:	<input type="radio" id="washAfterBirthYes3" name="washAfterBirth3" value="1"/>হ্যাঁ  <input type="radio" id="washAfterBirthNo3" name="washAfterBirth3" value="2"/>না&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;											
					<span class="tp" id="resastation1">
					রিসাসসিটেশন:</span>	<input type="radio" id="resassitationYes3" name="resassitation3" value="1"/>হ্যাঁ  <input type="radio" id="resassitationNo3" name="resassitation3" value="2"/>না&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							
					<span id="newBornresassitation3" hidden="true">
					<input type="checkbox" id="stimulation3"/>স্টিমুলেশন  &nbsp;&nbsp;&nbsp;
							
					<input type="checkbox" id="bagNMask3"/>ব্যাগ ও মাস্ক ব্যবহার 	
					</span>
					<br>
					<span id="fresh3">
					নাড়ি কাটার পর নাড়িতে ৭.১% ক্লোরহেক্সিডিন দ্রবন ব্যবহার করা হয়েছে:	<input type="radio" id="chlorehexidinYes3" name="chlorehexidin3" value="1"/>হ্যাঁ  <input type="radio" id="chlorehexidinNo3" name="chlorehexidin3" value="2"/>না <br>
					
					নাড়ি কাটার পর মায়ের ত্বকে নবজাতককে লাগানো হয়েছে:	<input type="radio" id="skinTouchYes3" name="skinTouch3" value="1"/>হ্যাঁ  <input type="radio" id="skinTouchNo3" name="skinTouch3" value="2"/>না<br>
							
					জন্মের ১ ঘন্টার মধ্যে বুকের দুধ খাওয়ানো হয়েছে:	<input type="radio" id="breastfeedYes3" name="breastfeed3" value="1"/>হ্যাঁ  <input type="radio" id="breastfeedNo3" name="breastfeed3" value="2"/>না<br>
													
					<input type="checkbox" id="newBornRefer3"/> রেফার &nbsp;&nbsp;&nbsp;&nbsp;
					<span id="newBornReferSpan3" hidden="true">
								
						কেন্দ্রের নাম	
						<select id="newBornReferCenter3" >							
						</select>&nbsp;&nbsp;&nbsp;&nbsp;	
											
						কারণ	
						<select id="newBornReferReason3" multiple="multiple">																				
						</select>&nbsp;&nbsp;&nbsp;&nbsp;										
					
					</span>
					</span>
					</span>
					<br>
					<input type="button" id="newbornSave3" class="right" width="25px" height="15px" value="Save">				
				</div>
			
				<div id="newBornInfo_4" class="person_detailright">	
					<table id="newbornHighlight4" style="width:100%;"><tbody><tr><td>সন্তান: ৪ <span id="birthstat4"></span>&nbsp; <span id="immatureBirthWeekSection4"></span></td></tr></tbody></table><!-- <span id="newbornHighlight4">সন্তান: ৪ <span id="birthstat4"></span>&nbsp; <span id="immatureBirthWeekSection4"></span></span>--><hr>	
					<span id="birthStatus4" hidden="true"></span>
					<input type="radio" id="newBornSexMale4" name="newBornSex4" value="1"/>ছেলে  <input type="radio" id="newBornSexFemale4" name="newBornSex4" value="2"/>মেয়ে 
					<span id="macerated0_4">
					<input type="radio" id="newbornSexUnidentified4" name="newBornSex4" value="3"/>সনাক্ত করা যাই নি
					</span>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<span id="macerated1_4">
					জন্ম ওজন (কেজি)	<input type="text" id="birthWeight4" maxlength="4" size="4"/> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					</span>
					<!-- অপরিণত জন্ম (৩৭ সপ্তাহ পূর্ণ হওয়ার আগে):	<input type="radio" id="immatureBirthYes4" name="immatureBirth4" value="1"/>হ্যাঁ  <input type="radio" id="immatureBirthNo4" name="immatureBirth4" value="2"/>না --><br>
					<span id="macerated2_4">		
					১ মিনিটের মধ্যে মোছানো হয়েছে:	<input type="radio" id="washAfterBirthYes4" name="washAfterBirth4" value="1"/>হ্যাঁ  <input type="radio" id="washAfterBirthNo4" name="washAfterBirth4" value="2"/>না&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;											
					
					<span class="tp" id="resastation4">
					রিসাসসিটেশন:</span>	<input type="radio" id="resassitationYes4" name="resassitation4" value="1"/>হ্যাঁ  <input type="radio" id="resassitationNo4" name="resassitation4" value="2"/>না&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							
					<span id="newBornresassitation4" hidden="true">
					<input type="checkbox" id="stimulation4"/>স্টিমুলেশন  &nbsp;&nbsp;&nbsp;
							
					<input type="checkbox" id="bagNMask4"/>ব্যাগ ও মাস্ক ব্যবহার 	
					</span>
					<br>
					<span id="fresh4">
					নাড়ি কাটার পর নাড়িতে ৭.১% ক্লোরহেক্সিডিন দ্রবন ব্যবহার করা হয়েছে:	<input type="radio" id="chlorehexidinYes4" name="chlorehexidin4" value="1"/>হ্যাঁ  <input type="radio" id="chlorehexidinNo4" name="chlorehexidin4" value="2"/>না <br>
					
					নাড়ি কাটার পর মায়ের ত্বকে নবজাতককে লাগানো হয়েছে:	<input type="radio" id="skinTouchYes4" name="skinTouch4" value="1"/>হ্যাঁ  <input type="radio" id="skinTouchNo4" name="skinTouch4" value="2"/>না<br>
							
					জন্মের ১ ঘন্টার মধ্যে বুকের দুধ খাওয়ানো হয়েছে:	<input type="radio" id="breastfeedYes4" name="breastfeed4" value="1"/>হ্যাঁ  <input type="radio" id="breastfeedNo4" name="breastfeed4" value="2"/>না<br>
													
					<input type="checkbox" id="newBornRefer4"/> রেফার &nbsp;&nbsp;&nbsp;&nbsp;
					<span id="newBornReferSpan4" hidden="true">
								
						কেন্দ্রের নাম	
						<select id="newBornReferCenter4" >							
						</select>&nbsp;&nbsp;&nbsp;&nbsp;	
											
						কারণ	
						<select id="newBornReferReason4" multiple="multiple">
						</select>&nbsp;&nbsp;&nbsp;&nbsp;										
					
					</span>
					</span>
					</span>
					<br>
					<input type="button" id="newbornSave4" class="right" width="25px" height="15px" value="Save">				
				</div>
			
				<div id="newBornInfo_5" class="person_detailright">	
					<table id="newbornHighlight5" style="width:100%;"><tbody><tr><td>সন্তান: ৫ <span id="birthstat5"></span>&nbsp; <span id="immatureBirthWeekSection5"></span></td></tr></tbody></table><!-- <span id="newbornHighlight5">সন্তান: ৫ <span id="birthstat5"></span>&nbsp; <span id="immatureBirthWeekSection5"></span></span>--><hr>
					<span id="birthStatus5" hidden="true"></span>	
					<input type="radio" id="newBornSexMale5" name="newBornSex5" value="1"/>ছেলে  <input type="radio" id="newBornSexFemale5" name="newBornSex5" value="2"/>মেয়ে
					<span id="macerated0_5">
					<input type="radio" id="newbornSexUnidentified5" name="newBornSex5" value="3"/>সনাক্ত করা যাই নি
					</span>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<span id="macerated1_5">
					জন্ম ওজন (কেজি)	<input type="text" id="birthWeight5" maxlength="4" size="4"/> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					</span>
					<!--অপরিণত জন্ম (৩৭ সপ্তাহ পূর্ণ হওয়ার আগে):	<input type="radio" id="immatureBirthYes5" name="immatureBirth5" value="1"/>হ্যাঁ  <input type="radio" id="immatureBirthNo5" name="immatureBirth5" value="2"/>না--><br>
					<span id="macerated2_5">		
					১ মিনিটের মধ্যে মোছানো হয়েছে:	<input type="radio" id="washAfterBirthYes5" name="washAfterBirth5" value="1"/>হ্যাঁ  <input type="radio" id="washAfterBirthNo5" name="washAfterBirth5" value="2"/>না&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;											
					
					<span class="tp" id="resastation1">
					রিসাসসিটেশন:</span>	<input type="radio" id="resassitationYes5" name="resassitation5" value="1"/>হ্যাঁ  <input type="radio" id="resassitationNo5" name="resassitation5" value="2"/>না &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						
					<span id="newBornresassitation5" hidden="true">
					<input type="checkbox" id="stimulation5"/>স্টিমুলেশন  &nbsp;&nbsp;&nbsp;
							
					<input type="checkbox" id="bagNMask5"/>ব্যাগ ও মাস্ক ব্যবহার 	
					</span>
					<br>
					<span id="fresh5">
					নাড়ি কাটার পর নাড়িতে ৭.১% ক্লোরহেক্সিডিন দ্রবন ব্যবহার করা হয়েছে:	<input type="radio" id="chlorehexidinYes5" name="chlorehexidin5" value="1"/>হ্যাঁ  <input type="radio" id="chlorehexidinNo5" name="chlorehexidin5" value="2"/>না <br>
					
					নাড়ি কাটার পর মায়ের ত্বকে নবজাতককে লাগানো হয়েছে:	<input type="radio" id="skinTouchYes5" name="skinTouch5" value="1"/>হ্যাঁ  <input type="radio" id="skinTouchNo5" name="skinTouch5" value="2"/>না<br>
							
					জন্মের ১ ঘন্টার মধ্যে বুকের দুধ খাওয়ানো হয়েছে:	<input type="radio" id="breastfeedYes5" name="breastfeed5" value="1"/>হ্যাঁ  <input type="radio" id="breastfeedNo5" name="breastfeed5" value="2"/>না<br>
													
					<input type="checkbox" id="newBornRefer5"/> রেফার &nbsp;&nbsp;&nbsp;&nbsp;
					<span id="newBornReferSpan5" hidden="true">
								
						কেন্দ্রের নাম	
						<select id="newBornReferCenter5" >							
						</select>&nbsp;&nbsp;&nbsp;&nbsp;	
											
						কারণ	
						<select id="newBornReferReason5" multiple="multiple">																				
						</select>&nbsp;&nbsp;&nbsp;&nbsp;										
					
					</span>
					</span>
					</span>
					<br>
					<input type="button" id="newbornSave5" class="right" width="25px" height="15px" value="Save">				
				</div>			
			</div>
		</div>
	
	<div id="pncmother" class="person_detail">			
		<div id="pncVisitMother" class="person_ANCdetail">
		<table>
				<tbody>
					<tr>
						<td><b>আদর্শ পরিদর্শনের সময়</b></td>
					</tr>
					<tr>
						<td>
							<table  width="924px" border="1px">
								<tbody>
									<tr>
										<td> পরিদর্শন ১ </td>
										<td> পরিদর্শন ২ </td>
										<td> পরিদর্শন ৩ </td>
										<td> পরিদর্শন ৪ </td>
									</tr>
									<tr>
										<td class="ideal"><span id="idealPNC1Date" style="font-size:14px;"></span></td>
										<td class="ideal"><span id="idealPNC2Date" style="font-size:14px;"></span></td>
										<td class="ideal"><span id="idealPNC3Date" style="font-size:14px;"></span></td>
										<td class="ideal"><span id="idealPNC4Date" style="font-size:14px;"></span></td>			
									</tr>
								</tbody>
							</table>
						</td>						
					</tr>
				</tbody>
			</table>
			<div id="subheading" class="subheadingside">বর্তমান সেবার তথ্য: মা </div>				
		</div>
	</div>
	
	<div id="pncchild" class="person_detail">			
		<div id="pncVisitChild" class="person_ANCdetail">						
		</div>
	</div>
	
	<div id="pac" class="person_detail">			
		<div id="pacVisit" class="person_ANCdetail">
			<table>
				<tbody>
					<tr>
						<td><b>গর্ভপাত সংক্রান্ত তথ্য</b></td>
					</tr>
					<tr>
						<td>
							<b>স্থান:</b>						
							<input type="text" id="abortionPlace" maxlength="8" size="8" disabled="true">
							<b>তারিখ:</b>
							<input type="text" id="abortionDate" maxlength="11" size="11" disabled="true">
						</td>				
					</tr>
				</tbody>
			</table>
			<div id="subheading" class="subheadingside">বর্তমান সেবার তথ্য </div>
							
		</div>
	</div>
	
	<div id="content" class="person_detail">
	</div>			
	<div id= "report" class="report_content">
		
	</div>
	<div id="pillCondomScreening" class="popupDialog">			
		
	</div>
	
	<div id= "new" class="person_detail">
		<span class = "nextC" style="cursor:pointer">পরবর্তী সেবা গ্রহীতা</span>
	</div>
	<div id="detailInfoWindow" class="popupDialog">
		<span id="detailList"></span>
		<br><hr>
		<input type="button" id="popupButton" class="right" value="Close" >
	</div>
	
	<div id="ttInfo" class="popupDialog">
		<input type="radio" id="ttWithDate" name="ttDateRadio" value="1" checked="checked"/> তারিখ জানা আছে  <br>		
			<span id="ttDateInput"><input type="text" id="ttDate" maxlength="11" size="11"/></span><span id="ttField" hidden="true"></span>
		<br><br>
		<input type="radio" id="ttWithoutDate" name="ttDateRadio" value="2"/> তারিখ জানা নেই
		<br><hr>
		<input type="button" id="popupButtonTTClose" class="right" value="Cancel" >
		<input type="button" id="popupButtonTT" class="right" value="OK" >	
	</div>
	
	<div id="deathInfo" class="popupDialog">
		<span id="deathReportUpdate"> 
			মৃত্যু সংক্রান্ত তথ্য:&nbsp; 
			<input type="radio" id="deathReport" name="updateReport" value="1"/> সংরক্ষণ  
			<input type="radio" id="deathUpdate" name="updateReport" value="2"/> সংশোধন
		</span>
		<br><br>
		<span id="deathInputSectionSpan">
		<span id="deathMotherSpan">
			<input type="checkbox" id="deathMother"/> মাতৃ মৃত্যু
		</span>
		<span id="deathNewbornSpan">
			<input type="checkbox" id="deathNewborn"/> নবজাতক মৃত্যু				
		</span>
		<br><br>
		<span id="deathIdType" class="font_size_english"></span>	<input type="text" id="deathHealthId" disabled="true"/>
		&nbsp;&nbsp;
		<span id="healthIdDeath" hidden="true"></span>
		<span id="pregNoDeath" hidden="true"></span>
		<span id="providedBy" hidden="true"></span>
		<span id="newbornDeathSection" hidden="true">
		<span class="font_size_english">CHILD NO.</span>	
		<SELECT id="childNoList">
		</SELECT>
		</span>
		<br><br>
		তারিখ&nbsp;<input type="text" id="deathDate">&nbsp;
		স্থান&nbsp;<SELECT id="deathPlace"><option value="1">বাড়ি</option><option value="2">সেবাকেন্দ্র</option><option value="3">অন্যান্য</option></SELECT>&nbsp;
		কারণ&nbsp;<SELECT id="deathReason" class="deathReason_selectwidth"></SELECT>&nbsp;
		
		<br><hr>
		<span id="reportDeathSpan"><input type="button" id="popupButtonDeath" class="right" value="Report Death" >&nbsp;&nbsp;</span>
		<span id="updateDeathSpan"><input type="button" id="popupButtonDeathUpdate" class="right" value="Update Death Information" >&nbsp;&nbsp;</span>
		<input type="button" id="popupButtonDeathCancel" class="right" value="Cancel" >
		</span>
	</div>
	
	

	
	<div id="advanceSearch" class="popupDialog">
		<div id="subheadingAdvanceSearch" class="subheadingside">
					Advance Search 			
				</div>				
				<table id="tableAdvanceSearchOption">
					<tbody>
						<tr>
							<td> Search Options*</td>
							<td>	
								<select id="AdvanceSearchoption">
									<option value="1"> মোবাইল নম্বর </option>
									<option value="2"> নাম, ঠিকানা </option>
								</select>
							</td>																				
						</tr>
					</tbody>
				</table>
				<br>
				<table id="AdvanceSearchnameAddressFields">
					<tbody>
						<tr>
							<td>নাম*</td><td>	<input type="text" id="SclientName"/></td>
												
							<td>লিঙ্গ*</td><td>	
								<select id="SclientGender">
									<option value="1" >পুরুষ</option>
									<option value="2" selected>মহিলা</option>
									<option value="3">হিজরা</option>
								</select>
							</td>
							
							<td>জেলা</td><td>	<SELECT id="SzillaName"></SELECT></td>													
						</tr>	
						<tr>															
							<td>উপজেলা</td><td>	<SELECT id="SupaZilaName"></SELECT></td>
							
							<td>ইউনিয়ন</td><td>	<SELECT id="SunionName"></SELECT></td>
							
							<td>গ্রাম / মহল্লা</td><td>	<SELECT id="SvillageName"></SELECT></td>													
						</tr>
					</tbody>
				</table>
				<table id="AdvanceSearchmobileFields">
					<tbody>
						<tr>
							<td> মোবাইল নম্বর*</td><td>	<input type="text" id="SmobileNumber" maxlength="11" size="11"/></td>																				
						</tr>
					</tbody>
				</table>			
		<br>
		&nbsp;&nbsp;
		<input type="button" id="popupButtonSearchCancel" class="right" value="Cancel">&nbsp;&nbsp;
		<input type="button" id="popupButtonSearch" class="right" value="Search">		
		<span id="searchResult"></span>
	</div>	
	
	<div id="closePreg" class="popupDialog">
	<span id="closePregTitle1" hidden="true"><h1 style="font-size:22px; text-align: center;">|| পূর্ববর্তী গর্ভ সংক্রান্ত তথ্য ||</h1></span>
	<span id="closePregTitle2" hidden="true"><h1 style="font-size:22px; text-align: center;">|| গর্ভপাত সংক্রান্ত তথ্য ||</h1></span>
	<span id="closePregCallFrom" hidden="true"></span>
		<span id="closeTypeSpan">
		<table style="width:100%;">				
			<tr>						
				<td colspan="2">প্রসব <input type="radio" name="closeType" value="delivery" id="closeTypeDelivery" checked="checked"/>
				গর্ভপাত <input type="radio" name="closeType" value="abortion" id="closeTypeAbortion"/></td>
			</tr>
		</table>		
		</span>	
		
		<table width="100%">			
			<tr>									
				<td>
					স্থান*&nbsp;
					<SELECT id="closePlace">
					<option value="0"></option>
					<option value="1">বাড়ি</option>
					<option value="2">সেবাকেন্দ্র</option></SELECT>
				</td>
				<td>
					তারিখ* &nbsp;
					<input type="text" id="closeDate">
				</td>
			</tr>
			<tr>
				<td colspan="2">
				<span id="closeFacility" hidden="true">
					সেবাকেন্দ্রের নাম &nbsp; 
					<select id="closeDeliveryPlaceCenter">										
					</select>
				</span>
				</td>																		
			</tr>															
		</table>		
													
		<span id="closeDeliverySpan">
			<table>
				<tr>
					<td colspan="2">
						প্রসবের ধরণ* 
						<select id="closeDeliveryType">
							<option value="0"> </option>
							<option value="1"> স্বাভাবিক </option>
							<option value="2"> সিজারিয়ান </option>													
						</select>
					</td>									
				</tr>	
				<tr>																																																	
					<td colspan="3">
										প্রসবের ফলাফল:&nbsp;&nbsp;									
										জীবিত জন্ম&nbsp;&nbsp;<input type="text" id="closeLiveBirth" maxlength="2" size="1" />
										&nbsp;||&nbsp;												
										মৃত জন্ম&nbsp;
										<input type="text" id="closeStillBirth" maxlength="2" size="1" />
										&nbsp;||&nbsp;										
										লিঙ্গ:&nbsp;
										ছেলে<input type="text" id="closeNewbornBoy" maxlength="2" size="1" />&nbsp;
										মেয়ে<input type="text" id="closeNewbornGirl" maxlength="2" size="1" />&nbsp;																						
					</td>
				</tr>								
				<tr>
					<td colspan="3">	
						<span class="tp" id="closeAbortionC">
						প্রসব সম্পাদনকারীর নাম &nbsp;</span><input type="text" id="closeDeliveryDoneByPerson"/>						
					</td>			
				</tr>
				<tr>	
					<td colspan="3">	
						<span class="tp" id="closeAbortionC2"> পদবী&nbsp;</span>
						<select id="closeDeliveryDoneByDesignation">												
						</select>															
					</td>					 
				</tr>
			</table>
		</span>																																						
		<br><hr>
		<span class="right">		
		<input type="button" id="closeButtonSave" value="Save" >&nbsp;
		<input type="button" id="closeButtonCancel" value="Cancel" >&nbsp;
		</span>		
	</div>
	
	<div id="shortPreg" class="popupDialog">
		<span id="ShortPregTitle"><h1 style="font-size:22px; text-align: center;">|| গর্ভ সংক্রান্ত তথ্য ||</h1></span>
		<span id="shortPregDateSpan">
			প্রসবের তারিখ* &nbsp;<input type="text" id="shortPregDeliveryDate">
			<br>
			আনুমানিক শেষ মাসিকের তারিখ* &nbsp;<input type="text" id="shortPregLMPDate">
		</span>		
		<!-- 
			<br><br>
		<span id="shortPregInfoSpan">	
			উচ্চতা &nbsp;<input type="text" id="shortPregHfoot" maxlength="2" size="2"/>&nbsp;ফুট <input type="text" id="shortPregHinch" maxlength="2" size="2"/>&nbsp;ইঞ্চি 
			&nbsp;&nbsp;&nbsp;&nbsp;
			রক্তের গ্রুপ		
				<select id="shortPregBloodGroup">
					<option value="0"></option>
					<option value="1">A+</option>
					<option value="2">A-</option>
					<option value="3">B+</option>
					<option value="4">B-</option>
					<option value="5">AB+</option>
					<option value="6">AB-</option>
					<option value="7">O+</option>
					<option value="8">O-</option>
				</select>
		</span>
		 -->																													
		<br><hr>
		<span class="right">		
		<input type="button" id="shortPregSaveButton" value="Save" >&nbsp;
		<input type="button" id="shortPregCancelButton" value="Cancel" >&nbsp;
		</span>		
	</div>
	<div id="loading_div" class="loadPopup">
	</div>
