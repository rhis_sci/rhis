function deleteLastANCInfo(){
	 
	var ANCObj = new Object();
	ANCObj = setCommonParam();
	
	ANCObj.healthid = $('#healthid').text();
	ANCObj.pregNo = $('#pregNumber').text();
	
	ANCObj.ancLoad = "delete";
	
	ANCInfo(ANCObj);	
}

/**
 * saving ANC information
 */
function insertANCInfo() {

	var ANCObj = new Object();
	ANCObj = setCommonParam();

	ANCObj.healthid = $('#healthid').text();
	ANCObj.pregNo = $('#pregNumber').text();
	ANCObj.providerid = $('#providerid').text();

	ANCObj.ancdate = $('#ancDate').val();
	ANCObj.ancbpsys = $('#bpSystolicANC').val();
	ANCObj.ancbpdias = $('#bpDiastolicANC').val();
	ANCObj.ancweight = $('#weightANC').val();
	ANCObj.ancedema = $('#edemaANC').val();
	ANCObj.ancuheight = $('#uHeightANC').val();
	ANCObj.anchrate = $('#hRateANC').val();
	ANCObj.ancfpresentation = $('#fPresentationANC').val();
	ANCObj.ancanemia = $('#anemiaANC').val();
	ANCObj.anchemoglobin = $('#hemoglobinANC').val();
	ANCObj.ancjaundice = $('#jaundiceANC').val();
	ANCObj.ancsugar = $('#sugarANC').val();
	ANCObj.ancalbumin = $('#albuminANC').val();
	ANCObj.anccomplication = $('#complicationANC').multipleSelect("getSelects");
	ANCObj.ancsymptom = $('#symptomANC').multipleSelect("getSelects");
	ANCObj.ancdisease = $('#diseaseANC').multipleSelect("getSelects");
	ANCObj.anctreatment = $('#treatmentANC').multipleSelect("getSelects");
	ANCObj.ancadvice = $('#adviceANC').multipleSelect("getSelects");
	ANCObj.ancrefer = $('#referANC').prop('checked') ? 1 : 2;
	ANCObj.ancreferreason = $('#referReasonANC').multipleSelect("getSelects");
	ANCObj.anccentername = $('#centerNameANC').val();
	ANCObj.ancsatelitecentername = "";
	ANCObj.client = "1";
	if($('#otherServiceProviderCheckboxANC').prop('checked')){
		ANCObj.ancservicesource = $('#otherServiceProviderANC').val();
	}
	else{
		ANCObj.ancservicesource = "";
	}
	//ANCObj.ancservicesource = $('#otherServiceProvider').val();
	ANCObj.ancLoad = "";

	var userVal = "";
	userVal = userVal + "<table class=confirmation><tr><td>সেবাগ্রহনের তারিখ:" + moment(ANCObj.ancdate).format("DD MMM YYYY") + "</td></tr></table>";
	
	userVal = userVal + "<table class='list'><tr><td>অসুবিধা: </td></tr>";
    textVal=ANCObj.ancsymptom;
    symptomANCJS = JSON.parse(symptomANC);
	convertedText = "  ";
	
	for(i =0; i < textVal.length; i++){
		convertedText = convertedText +"<tr><td>-"+symptomANCJS[0][textVal[i]] +"</td></tr>";
	}
	userVal+=convertedText+"</table>";
	
	if(ANCObj.ancbpsys!="" || ANCObj.ancbpdias!="" )
		var slash="/";
	else    
		var slash="";
	userVal = userVal + "<table class=confirmation><tr><td>"+(ANCObj.ancbpsys=="" || ANCObj.ancbpdias=="" ? "<span style=\"color: red;\">" : "<span style=\"color: black;\">") +"রক্তচাপ (মিঃ মিঃ মার্কারি):</span>" + ANCObj.ancbpsys + slash + ANCObj.ancbpdias+"</td></tr>";
    userVal = userVal + "<tr><td>"+linemark("ওজন (কেজি)",ANCObj.ancweight)+"</td>";	 
    userVal = userVal + "<td>ইডিমা:";
    switch (ANCObj.ancedema) {
    case '0':
		userVal+= '';
		break;
    case '1':
		userVal+= 'নাই';
		break;
	case '2':
		userVal+= '+ (অল্প)';
		break;
	case '3':
		userVal+= '++ (মোটামুটি)';
		break;
	case '4':
		userVal+= '+++ (অধিক)';
		break;
	}
    userVal+="</td></tr>";    
   
    userVal = userVal + "<tr><td>"+ linemark("জরায়ুর উচ্চতা (সপ্তাহ)",ANCObj.ancuheight)+"</td>";
    userVal = userVal + "<td>"+ linemark("ফিটাসের হৃদপিণ্ডের গতি (প্রতি  মিনিটে)",ANCObj.anchrate)+"</td></tr>";   
    userVal = userVal + "<tr><td>ফিটাল প্রেজেনটেশন: ";
    switch (ANCObj.ancfpresentation) {
    	case '0':
    		userVal+= '';
    		break;
       	case '1':
			userVal+= 'স্বাভাবিক';
			break;
		case '2':
			userVal+= 'অস্বাভাবিক';
			break;
	}
    userVal+="</td>"; 
    
    userVal = userVal + "<td>জন্ডিস: ";
    switch (ANCObj.ancjaundice) {
    	case '0':
			userVal+= '';
			break;
       	case '1':
			userVal+= 'নাই';
			break;
		case '2':
			userVal+= '+ (অল্প)';
			break;
		case '3':
			userVal+= '++ (মোটামুটি)';
			break;
		case '4':
			userVal+= '+++ (অধিক)';
			break;
	}
    userVal+="</td></tr>";
    
    
    userVal = userVal +"<tr><td>"+ linemark_switch("রক্তস্বল্পতা ",ANCObj.ancanemia,'none');
	switch (ANCObj.ancanemia) {
		case '0':
			userVal+= '';
			break;
		case '1':
			userVal+= '+ (স্বল্প )';
			break;
		case '2':
			userVal+= '++ (মধ্যম)';
			break;
		case '3':
			userVal+= '+++ (অতিরিক্ত)';
			break;
	}
	userVal+="</td>";
	
    userVal = userVal +"<td>"+ linemark("হিমোগ্লোবিন",ANCObj.anchemoglobin);
    if(ANCObj.anchemoglobin!='')
    	userVal+="%";
    userVal+="</td></tr>"; 
    
    userVal = userVal + "<tr><td>প্রস্রাব পরীক্ষা: সুগার: ";
    switch (ANCObj.ancsugar) {
    case '0':
		userVal+= '';
		break;
    case '1':
		userVal+= 'নাই';
		break;
	case '2':
		userVal+= '+ (অল্প)';
		break;
	case '3':
		userVal+= '++ (মোটামুটি)';
		break;
	case '4':
		userVal+= '+++ (অধিক)';
		break;
	case '5':
		userVal+= '++++ (অধিক)';	
		break;
	}
    userVal+="</td>";
    
    userVal = userVal + "<td>প্রস্রাব পরীক্ষা: এলবুমিন: ";
    switch (ANCObj.ancalbumin) {
    case '0':
		userVal+= '';
		break;
    case '1':
		userVal+= 'নাই';
		break;
	case '2':
		userVal+= '+ (অল্প)';
		break;
	case '3':
		userVal+= '++ (মোটামুটি)';
		break;
	case '4':
		userVal+= '+++ (অধিক)';
		break;
	case '5':
		userVal+= '++++ (অধিক)';		
		break;
	}
    userVal+="</td></tr></table>";
    
    userVal = userVal + "<table class='list'><tr><td> বিপদ চিহ্ন / জটিলতা: </td></tr> ";
    textVal=ANCObj.anccomplication;
    dangerSignANCJS = JSON.parse(dangerSignANC);
	var i;
	var convertedText = "  ";
	
	for(i =0; i < textVal.length; i++){
		convertedText = convertedText +"<tr><td>-"+dangerSignANCJS[0][textVal[i]] +"</td></tr>";				
	}
	userVal+=convertedText;
   
    userVal = userVal + "<tr><td>রোগ: </td></tr> ";
    textVal=ANCObj.ancdisease;
    diseaseANCJS = JSON.parse(diseaseANC);
    convertedText = "  ";
    
	for(i =0; i < textVal.length; i++){
		convertedText = convertedText +"<tr><td>-"+ diseaseANCJS[0][textVal[i]] +"</td></tr>";
	}
	userVal+=convertedText;
    
	userVal = userVal + "<tr><td>চিকিৎসা: </td></tr> ";
	textVal=ANCObj.anctreatment;
	treatmentJS = JSON.parse(treatment);
	convertedText = "  ";
	
	for(i =0; i < textVal.length; i++){
		convertedText = convertedText +"<tr><td>-"+ treatmentJS[0][textVal[i]] +"</td></tr>";
	}
	userVal+=convertedText;
	
    userVal = userVal + "<tr><td>পরামর্শ: </td></tr> ";
    textVal=ANCObj.ancadvice;
    adviceANCJS = JSON.parse(adviceANC);
    convertedText = " ";
	
	for(i =0; i < textVal.length; i++){
		convertedText = convertedText +"<tr><td>-"+ adviceANCJS[0][textVal[i]] +"</td></tr>";
	}
	userVal+=convertedText;
    
   userVal = userVal + "<tr><td>রেফার: "+(ANCObj.ancrefer=="1" ? "হ্যাঁ" : "না")+"</td></tr>";

   if(ANCObj.ancrefer=="1"){
	   
	   referCenterJS = JSON.parse(referCenter);
	   userVal = userVal +"<tr><td>"+ linemark_switch("কেন্দ্রের নাম",ANCObj.anccentername,0);
	   
	   userVal+=referCenterJS[0][ANCObj.anccentername];
	   
	   userVal+="</td></tr>"; 
	    
	    userVal = userVal +"<tr><td>"+linemark_switch("কারণ",ANCObj.anccentername,0)+"</td></tr>";
	    
	    textVal=ANCObj.ancreferreason;
	    referReasonANCJS = JSON.parse(referReasonANC);
		var i;
		var convertedText = "  ";
		
		for(i =0; i < textVal.length; i++){
			convertedText+="<tr><td>-"+referReasonANCJS[0][textVal[i]]+"</td></tr>";					
		}
		userVal+=convertedText+"</table>";
	}
   	if(moment(ANCObj.ancdate) > moment($('#lmp').val())){
	    $('<div></div>').dialog({
	        modal: true,
	        title: "নিশ্চিত করুন",
	        width: 560,
	        hide: {effect: 'fade', duration: 500},
	        show: {effect: 'fade', duration: 600},
	        open: function () {
	            $(this).html(userVal);
	        },
	        buttons: {
	        	"Save & Continue" : function () {
	        		ANCInfo(ANCObj);
	            	$(this).dialog("close");
	            },
	            
				"Save & Serve Next Client": function () {
	        		ANCInfo(ANCObj);
	        		goToId='.nextC';	
				    $(goToId).click();
	            	$(this).dialog("close");
	            },
				"Save & Open Delivery Information": function () {
	        		ANCInfo(ANCObj);				
				    goToId='#2.serviceTabButton';	
				    $(goToId).click();
	            	$(this).dialog("close");
	            },
				Cancel: function () {
	        		$(this).dialog("close");
	        	}			
	        }
	    }); //end confirm dialog
   	}
   	else{
   		callDialog(ancDateLessThanLMP);
   	}
}

/**
 * retrieve ANC information
 */
function retrieveANCInfo() {

	var ANCObj = new Object();
	ANCObj = setCommonParam();
	
	ANCObj.pregNo = $('#pregNumber').text();
	ANCObj.healthid = $('#healthid').text();
	ANCObj.ancLoad = "retrieve";
	ANCInfo(ANCObj);

}

/**
 * insert and retrieve ANC information in a single AJAX call
 */
function ANCInfo(ancInfo) {

	if (ancInfo.healthid != "" && ancInfo.pregNo != "") {
		$.ajax({
			type : "POST",
			url : "anc",
			timeout:60000, //60 seconds timeout
			dataType : "json",
			data : {"ANCInfo" : JSON.stringify(ancInfo)},
			success : function(response) {
				parseANCInfo(response);
			},
			error : function(xhr, status, error) {
				alert(status);
				alert(error);
			}
		});
	}
}

/**
 * populating anc table dynamically
 */
function parseANCInfo(response) {

	var count = Object.keys(response).length;
	
	if (response["ancStatus"]) {
		//count = count - 1; // removing ancStatus from column count
		callDialog(noANC);
	}
	if(count>0){
		count = count - 1; 
	}
	var response1 = [];
	for ( var prop in response) {
		response1.push(prop);
	}
	response1.sort();
		
	var edd_date = moment($('#edd').val());		
	var lmp_Date = edd_date.subtract({days:280});
	
	//suggested visit date
	//var suggestedANCDate1 = moment(lmp_Date).add({days:106}).format("DD MMM YYYY") + " - " + moment(lmp_Date).add({days:112}).format("DD MMM YYYY");
	var suggestedANCDate1 = moment(lmp_Date).add({days:112}).format("DD MMM YYYY");
	var suggestedANCDate2 = moment(lmp_Date).add({days:162}).format("DD MMM YYYY") + " - " + moment(lmp_Date).add({days:196}).format("DD MMM YYYY");
	var suggestedANCDate3 = moment(lmp_Date).add({days:218}).format("DD MMM YYYY") + " - " + moment(lmp_Date).add({days:224}).format("DD MMM YYYY");
	var suggestedANCDate4 = moment(lmp_Date).add({days:246}).format("DD MMM YYYY") + " - " + moment(lmp_Date).add({days:252}).format("DD MMM YYYY");
	
	$('#idealANC1Date').html(suggestedANCDate1);
	$('#idealANC2Date').html(suggestedANCDate2);
	$('#idealANC3Date').html(suggestedANCDate3);
	$('#idealANC4Date').html(suggestedANCDate4);
	
	
	// populating anc table
	var tableAnc = $("<table id='ancVisitTable' border='1px'><tbody>");
	var tr, col, endCol;

	// row 1
	tr = $("<tr><td>পরিদর্শন </td>");
	
	for (var i = 1; i <= count; i++) {
		col = "<td>" + en_to_ben_number_conversion(i.toString()) + "</td>";
		tr.append(col);
	}
	if (!response["ancStatus"]) {
		endCol = "<td>" + en_to_ben_number_conversion((count+1).toString())
				+ "</td>";
		tr.append(endCol);
	}
	tr.append("</tr>");
	tableAnc.append(tr);

	// row 2
	tr = $("<tr><td>সেবাগ্রহনের তারিখ	</td>");
	otherServiceCenterJS =  JSON.parse(referCenter);
	$.each(response1, function(key, val) {
		// alert("key : "+key+" ; value : "+val[1]);
		if(response[val][1]<t_ancdate)
			mark=danger;
			else mark=safe;
		
		
		if (val != "ancStatus") {
			col = "<td id='ancDate-" + response[val][0] + "'" + mark + "'>"
					+ moment(response[val][1]).format("DD MMM YYYY");
			if(response[val][21]!=""){
				col = col + "<br>" + otherServiceCenterJS[0][response[val][21]];
			}
			col = col + "</td>";
			tr.append(col);
		}

	});

	if (!response["ancStatus"]) {
		endCol = "<td><input type='text' id='ancDate' maxlength='11' size='11'/>&nbsp;<input type='checkbox' id='otherServiceProviderCheckboxANC'/> অন্যান্য উৎস <br>"
				+ "<select id='otherServiceProviderANC' hidden='true'>";
			
		$.each(otherServiceCenterJS[0], function(key, val) {
			endCol = endCol + "<option value = " + key + ">" + val + "</option>"; 			
		});	
		
		endCol = endCol + "</select></td>";
		tr.append(endCol);
	}
	tr.append("</tr>");
	tableAnc.append(tr);

	// row 3 (old 15)
	var tr = $("<tr><td>অসুবিধা </td>");
	symptomANCJS = JSON.parse(symptomANC);
	$.each(response1, function(key, val) {
		if (val != "ancStatus") {
			var colIdSpan = "#symptomANC-" + response[val][0] + "-span";
						
			var col = "<td id='symptomANC-" + response[val][0] + "'>"
					 + "<span id='symptomANC-" + response[val][0] + "-span' style='cursor:pointer'> <strong>"+detail(response[val][14])+"</strong> </span>";
			
			col = col + "<div id ='symptomANC-" + response[val][0] + "-dialog' hidden>" + response[val][14] + "</div></td>"
			
			$(document).on("click",colIdSpan, function(){
				id = $(this).attr('id');
				id = id.split("-")[1];
				colDialogId = "#symptomANC-" + id + "-dialog";
				
				textVal = $(colDialogId).text();
				textVal = textVal.slice(1, -1);
				
				if(textVal!=""){
					textVal = textVal.split(",");
				
					var i;
					var convertedText = "";
					for(i =0; i < textVal.length; i++){
						textVal[i] = textVal[i].slice(1,-1);
						convertedText = convertedText + "- " + symptomANCJS[0][textVal[i]] + "<br>";						
					}				
					//convertedText = convertedText.slice(0,-2);
					$('#detailList').html(convertedText);
					$('#detailInfoWindow').modal();
				}
				else{
					$('#detailList').html("There is no information")
					$('#detailInfoWindow').modal();
					//alert("There is no information");
				}										
			});			
			tr.append(col);
		}
	});

	if (!response["ancStatus"]) {
		endCol = "<td><select id='symptomANC'  multiple='multiple'>";
		
		endCol = endCol + populateDropdownList(symptomANCJS[0]);		
		
		endCol = endCol + "</select></td>";
		tr.append(endCol);
	}
	tr.append("</tr>");
	tableAnc.append(tr);
	
	// row 3
	var tr = $("<tr><td><span class=\"tp\" id=\"anc_bp\">রক্তচাপ (মিঃ মিঃ মার্কারি) </span></td>");
	$.each(response1, function(key, val) {
		// alert("key : "+key+" ; value : "+val[1]);
		if(response[val][2]!="" || response[val][3]!="" )
			var slash="/";
		else    
			var slash="";
		
		if( (response[val][2]!='' && response[val][3]!='') && ((response[val][2]>=t_ancbpsys) || (response[val][3]>=t_ancbpdias) ))
		mark=danger;
		else mark=safe; 

		if (val != "ancStatus") {
			var col = "<td id='bpSystolicANC-" + response[val][0] + "'" + mark+ ">"
					+ en_to_ben_number_conversion(response[val][2].toString())
					+ slash
					+ en_to_ben_number_conversion(response[val][3].toString())
					+ "</td>";
			tr.append(col);
		}
	});

	if (!response["ancStatus"]) {
		endCol = "<td><input type='text' id='bpSystolicANC' maxlength='3' size='3'/> / <input type='text' id='bpDiastolicANC' maxlength='3' size='3'/></td>";
		tr.append(endCol);
	}
	tr.append("</tr>");
	tableAnc.append(tr);
	
	// row 4 (old representation of blood pressure)
	
	// row 5
	var tr = $("<tr><td>ওজন (কেজি)	</td>");
	$.each(response1, function(key, val) {
		// alert("key : "+key+" ; value : "+val[1]);
		
		/*if(response[val][4]>=t_ancweight)
			mark=danger;
			else*/ mark=safe;
		
		if (val != "ancStatus") {
			var col = "<td id='weightANC-" + response[val][0] + "'"+ mark + ">"
					+ en_to_ben_number_conversion(parseFloat(response[val][4]).toFixed(1).toString()) ;   
					+ "</td>";
			tr.append(col);
		}
	});

	if (!response["ancStatus"]) {
		endCol = "<td><input type='text' id='weightANC' maxlength='5' size='4'/></td>";
		tr.append(endCol);
	}
	tr.append("</tr>");
	tableAnc.append(tr);
	
	// row 6
	var tr = $("<tr><td><span class=\"tp\" id=\"anc_edima\">ইডিমা </span>	</td>");
	$.each(response1, function(key, val) {
		// alert("key : "+key+" ; value : "+val[1]);
		if (val != "ancStatus") {
			var value = response[val][5];
			switch (value) {
			case '0':
				value = '';
				break;
			case '1':
				value = 'নাই';
				break;
			case '2':
				value = '+ (অল্প)';
				break;
			case '3':
				value = '++ (মোটামুটি)';
				break;
			case '4':
				value = '+++ (অধিক)';
				break;
			}
			
			if(response[val][5]==t_ancedema)
				 mark=danger;
			else mark=safe;
			
			var col = "<td id='edemaANC-" + response[val][0] + "'"+mark+">" + value
					+ "</td>";
			tr.append(col);
		}

	});

	if (!response["ancStatus"]) {
		endCol = "<td><select id='edemaANC'> "
				+ "<option value='0'></option>"
				+ "<option value='1'>নাই</option>"
				+ "<option value='2'>+ (অল্প)</option>"
				+ "<option value='3'>++ (মোটামুটি)</option>"
				+ "<option value='4'>+++ (অধিক)</option>" + "</select></td>";
		tr.append(endCol);
	}
	tr.append("</tr>");
	tableAnc.append(tr);

	// row 7
	var tr = $("<tr><td>জরায়ুর উচ্চতা (সপ্তাহ)	</td>");
	$.each(response1, function(key, val) {
		// alert("key : "+key+" ; value : "+val[1]);
		
		/*if(response[val][6]<=t_ancuheight) 
			mark=danger;
			else*/ mark=safe;
		
		if (val != "ancStatus") {
			var col = "<td id='uHeightANC-" + response[val][0] + "'"+mark+">"
					+ en_to_ben_number_conversion(response[val][6].toString())
					+ "</td>";
			tr.append(col);
		}

	});

	if (!response["ancStatus"]) {
		endCol = "<td><input type='text' id='uHeightANC' maxlength='3' size='3'/></td>";
		tr.append(endCol);
	}
	tr.append("</tr>");
	tableAnc.append(tr);
	
	// row 8
	var tr = $("<tr><td>ফিটাসের হৃদপিণ্ডের গতি (প্রতি  মিনিটে)	</td>");
	$.each(response1, function(key, val) {
		// alert("key : "+key+" ; value : "+val[1]);
		if((response[val][7]!='') && (response[val][7]>=t_anchrate_up || response[val][7]<=t_anchrate_down)) 
			mark=danger;
			else mark=safe;
		
		if (val != "ancStatus") {
			var col = "<td id='hRateANC-" + response[val][0] + "'" + mark +">"
					+ en_to_ben_number_conversion(response[val][7].toString())
					+ "</td>";
			tr.append(col);
		}

	});

	if (!response["ancStatus"]) {
		endCol = "<td><input type='text' id='hRateANC' maxlength='3' size='3'/></td>";
		tr.append(endCol);
	}
	tr.append("</tr>");
	tableAnc.append(tr);

	// row 9
	var tr = $("<tr><td>ফিটাল প্রেজেনটেশন	</td>");
	$.each(response1, function(key, val) {
		// alert("key : "+key+" ; value : "+val[1]);
		if (val != "ancStatus") {
			var value = response[val][8];
			switch (value) {
			case '0':
				value = '';
				break;
			case '1':
				value = 'স্বাভাবিক';
				break;
			case '2':
				value = 'অস্বাভাবিক';
				break;
			}
			
			if(response[val][8]==t_ancFetalPresentation)
				mark=danger;
			else mark=safe;
			
			
			var col = "<td id='fPresentationANC-" + response[val][0] + "'"+mark+">"
					+ value + "</td>";
			tr.append(col);
		}

	});

	if (!response["ancStatus"]) {
		endCol = "<td>" + "<select id='fPresentationANC'>"
				+ "<option value='0'></option>"
				+ "<option value='1'>স্বাভাবিক</option>"
				+ "<option value='2'>অস্বাভাবিক</option>"
				+ "</select></td>";
		tr.append(endCol);
	}
	tr.append("</tr>");
	tableAnc.append(tr);
	
	// row 10.1
	var tr = $("<tr><td><span class=\"tp\" id=\"anc_ancanemia\"> রক্তস্বল্পতা </span></td>");
	$.each(response1, function(key, val) {
		
		if (val != "ancStatus") {
			
		var value=response[val][22];
		
		switch (value) {
			case '0':
				value= '';
				break;
			case '1':
				value= '+ (স্বল্প )';
				break;
			case '2':
				value= '++ (মধ্যম)';
				break;
			case '3':
				value= '+++ (অতিরিক্ত)';
				break;
		}
					
			if((response[val][22]==t_ancanemia))
					mark=danger;	
			else
					mark=safe;
							
			var col = "<td id='anemiaANC-" + response[val][0] + "'"+mark+">"
					+ value + "</td>";
			tr.append(col);
			
		}
	});

	if (!response["ancStatus"]) {
		endCol = "<td>" + "<select id='anemiaANC'>"
				+ "<option value='0'selected></option>"
				+ "<option value='1'>+</option>"
				+ "<option value='2'>++</option>"
				+ "<option value='3'>+++</option>"				 
				+ "</select></td>";
		
		tr.append(endCol);
	}

	tr.append("</tr>");
	tableAnc.append(tr);
	
	// row 10.2
	var tr = $("<tr><td><span class=\"tp\" id=\"anc_himoglobin\"> হিমোগ্লোবিন	</span></td>");
	$.each(response1, function(key, val) {
		
		if (val != "ancStatus") {
			
			if (response[val][9]!='' && response[val][9]<=t_anchemoglobinH)
				mark=danger;
			else mark=safe;			
			
			if(response[val][9]=="")
				percent="";
			else
				percent="%";
			
			var col = "<td id='hemoglobinANC-" + response[val][0] + "'"+mark+">"
					+ en_to_ben_number_conversion(response[val][9].toString()) + percent + "</td>";
			tr.append(col);
		}

	});
	
	if (!response["ancStatus"]) {
		endCol = "<td><input type='text' id='hemoglobinANC' maxlength='3' size='2'/>%</td>";
		tr.append(endCol);
	}
	tr.append("</tr>");
	tableAnc.append(tr);
	

	// row 11
	var tr = $("<tr><td><span class=\"tp\" id=\"anc_jondis\">জন্ডিস</span>	</td>");
	$.each(response1, function(key, val) {
		// alert("key : "+key+" ; value : "+val[1]);
		if (val != "ancStatus") {
			var value = response[val][10];
			switch (value) {
			case '0':
				value = '';
				break;
			case '1':
				value = 'নাই';
				break;
			case '2':
				value = '+ (অল্প)';
				break;
			case '3':
				value = '++ (মোটামুটি)';
				break;
			case '4':
				value = '+++ (অধিক)';
				break;
			}
			
			if(response[val][10]==t_ancjaundice || response[val][10]=='0' || response[val][10]=='')
				 mark=safe;
			else mark=danger;
			
			var col = "<td id='jaundiceANC-" + response[val][0] + "'"+mark+">" + value
					+ "</td>";
			tr.append(col);
		}

	});

	if (!response["ancStatus"]) {
		endCol = "<td>" + "<select id='jaundiceANC'>"
				+ "<option value='0'></option>"
				+ "<option value='1'>নাই</option>"
				+ "<option value='2'>+ (অল্প)</option>"
				+ "<option value='3'>++ (মোটামুটি)</option>"
				+ "<option value='4'>+++ (অধিক)</option>" + "</select></td>";

		tr.append(endCol);
	}
	tr.append("</tr>");
	tableAnc.append(tr);

	// row 12
	var tr = $("<tr><td><span class=\"tp\" id=\"anc_sugar\">প্রস্রাব পরীক্ষা - সুগার </span>	</td>");
	$.each(response1, function(key, val) {
		// alert("key : "+key+" ; value : "+val[1]);
		if (val != "ancStatus") {
			var value = response[val][11];
			switch (value) {
			case '0':
				value = '';
				break;
			case '1':
				value = 'নাই';
				break;
			case '2':
				value = '+';
				break;
			case '3':
				value = '++';
				break;
			case '4':
				value = '+++';
				break;
			case '5':
				value = '++++';
				break;
			}
			
			if(value==t_ancsugar) 
				 mark=danger;
			else mark=safe;
			
			var col = "<td id='sugarANC-" + response[val][0] + "'"+mark+">" + value
					+ "</td>";
			tr.append(col);
		}

	});

	if (!response["ancStatus"]) {
		endCol = "<td>" + "<select id='sugarANC'>"
				+ "<option value='0'></option>"
				+ "<option value='1'>নাই</option>"
				+ "<option value='2'>+</option>"
				+ "<option value='3'>++</option>"
				+ "<option value='4'>+++</option>"
				+ "<option value='5'>++++</option>" + "</select></td>";

		tr.append(endCol);
	}
	tr.append("</tr>");
	tableAnc.append(tr);

	// row 13
	var tr = $("<tr><td><span class=\"tp\" id=\"anc_albumin\">প্রস্রাব পরীক্ষা - এলবুমিন	</span></td>");
	$.each(response1, function(key, val) {
		// alert("key : "+key+" ; value : "+val[1]);
		if (val != "ancStatus") {
			var value = response[val][12];
			switch (value) {
			case '0':
				value = '';
				break;
			case '1':
				value = 'নাই';
				break;
			case '2':
				value = '+';
				break;
			case '3':
				value = '++';
				break;
			case '4':
				value = '+++';
				break;
			case '5':
				value = '++++';
				break;
			}
			
			if(value==t_ancalbumin) 
				 mark=danger;
			else mark=safe;
			
			var col = "<td id='albuminANC-" + response[val][0] + "'"+mark+">" + value
					+ "</td>";
			tr.append(col);
		}

	});

	if (!response["ancStatus"]) {
		endCol = "<td>" + "<select id='albuminANC'>"
				+ "<option value='0'></option>"
				+ "<option value='1'>নাই</option>"
				+ "<option value='2'>+</option>"
				+ "<option value='3'>++</option>"
				+ "<option value='4'>+++</option>"
				+ "<option value='5'>++++</option>" + "</select></td>";

		tr.append(endCol);
	}
	tr.append("</tr>");
	tableAnc.append(tr);
	
	// row 14
	var tr = $("<tr><td>বিপদ চিহ্ন / জটিলতা	</td>");
	dangerSignANCJS = JSON.parse(dangerSignANC);
	$.each(response1, function(key, val) {
		if (val != "ancStatus") {
			var colIdSpan = "#complicationANC-" + response[val][0] + "-span";
						
			var col = "<td id='complicationANC-" + response[val][0] + "'>"
					 + "<span id='complicationANC-" + response[val][0] + "-span' style='cursor:pointer'> <strong>"+detail(response[val][13])+"</strong> </span>";
			
			col = col + "<div id ='complicationANC-" + response[val][0] + "-dialog' hidden>" + response[val][13] + "</div></td>"
			
			$(document).on("click",colIdSpan, function(){
				id = $(this).attr('id');
				id = id.split("-")[1];
				colDialogId = "#complicationANC-" + id + "-dialog";
				
				textVal = $(colDialogId).text();
				textVal = textVal.slice(1, -1);
				
				if(textVal!=""){
					textVal = textVal.split(",");
				
					var i;
					var convertedText = "";
					for(i =0; i < textVal.length; i++){
						textVal[i] = textVal[i].slice(1,-1);
						convertedText = convertedText + "- " + dangerSignANCJS[0][textVal[i]] + "<br>";						
					}
										
					$('#detailList').html(convertedText);
					$('#detailInfoWindow').modal();
				}
				else{
					$('#detailList').html("There is no information")
					$('#detailInfoWindow').modal();					
				}				
			});			
			tr.append(col);
		}
	});

	if (!response["ancStatus"]) {
		endCol = "<td><select id='complicationANC'  multiple='multiple'>";
		
		endCol = endCol + populateDropdownList(dangerSignANCJS[0]);
		
		endCol = endCol + "</select></td>";
		tr.append(endCol);
	}
	tr.append("</tr>");
	tableAnc.append(tr);

	// row 16
	var tr = $("<tr><td>রোগ	</td>");
	diseaseANCJS = JSON.parse(diseaseANC);
	$.each(response1, function(key, val) {

		if (val != "ancStatus") {			
			var colIdSpan = "#diseaseANC-" + response[val][0] + "-span";
			
			var col = "<td id='diseaseANC-" + response[val][0] + "'>"
					 + "<span id='diseaseANC-" + response[val][0] + "-span' style='cursor:pointer'> <strong>"+detail(response[val][15])+"</strong> </span>";
			
			col = col + "<div id ='diseaseANC-" + response[val][0] + "-dialog' hidden>" + response[val][15] + "</div></td>"
			
			$(document).on("click",colIdSpan, function(){
				id = $(this).attr('id');
				id = id.split("-")[1];
				colDialogId = "#diseaseANC-" + id + "-dialog";
				
				textVal = $(colDialogId).text();
				textVal = textVal.slice(1, -1);
				
				if(textVal!=""){
					textVal = textVal.split(",");
				
					var i;
					var convertedText = "";
					for(i =0; i < textVal.length; i++){
						textVal[i] = textVal[i].slice(1,-1);
						convertedText = convertedText + "- " + diseaseANCJS[0][textVal[i]] + "<br>";
					}					
					$('#detailList').html(convertedText);
					$('#detailInfoWindow').modal();
				}
				else{
					$('#detailList').html("There is no information")
					$('#detailInfoWindow').modal();					
				}										
			});
			tr.append(col);
		}
	});
	
	if (!response["ancStatus"]) {
		endCol = "<td><select id='diseaseANC'  multiple='multiple'>";
		
		endCol = endCol + populateDropdownList(diseaseANCJS[0]);
		
		endCol = endCol	+ "</select></td>";

		tr.append(endCol);
	}
	tr.append("</tr>");
	tableAnc.append(tr);

	// row 17
	var tr = $("<tr><td>চিকিৎসা	</td>");
	
	treatmentJS = JSON.parse(treatment);
	$.each(response1, function(key, val) {
		if (val != "ancStatus") {
			var colIdSpan = "#treatmentANC-" + response[val][0] + "-span";
			
			var col = "<td id='treatmentANC-" + response[val][0] + "'>"
					 + "<span id='treatmentANC-" + response[val][0] + "-span' style='cursor:pointer'> <strong>"+detail(response[val][16])+"</strong> </span>";
			
			col = col + "<div id ='treatmentANC-" + response[val][0] + "-dialog' hidden>" + response[val][16] + "</div></td>"
			
			$(document).on("click",colIdSpan, function(){
				id = $(this).attr('id');
				id = id.split("-")[1];
				colDialogId = "#treatmentANC-" + id + "-dialog";
				
				textVal = $(colDialogId).text();
				textVal = textVal.slice(1, -1);
				
				if(textVal!=""){
					textVal = textVal.split(",");
				
					var i;
					var convertedText = "";
					for(i =0; i < textVal.length; i++){
						textVal[i] = textVal[i].slice(1,-1);
						convertedText = convertedText + "- " + treatmentJS[0][textVal[i]] + "<br>";						
					}
					$('#detailList').html(convertedText);
					$('#detailInfoWindow').modal();
				}
				else{
					$('#detailList').html("There is no information")
					$('#detailInfoWindow').modal();				
				}										
			});
			tr.append(col);
		}
	});

	if (!response["ancStatus"]) {
		endCol = "<td><select id='treatmentANC'  multiple='multiple'>";
		
		endCol = endCol + populateDropdownList(treatmentJS[0]);
		
		endCol = endCol + "</select></td>";
		tr.append(endCol);
	}
	tr.append("</tr>");
	tableAnc.append(tr);

	// row 18
	var tr = $("<tr><td>পরামর্শ	</td>");
	adviceANCJS = JSON.parse(adviceANC);
	$.each(response1, function(key, val) {
		if (val != "ancStatus") {
			var colIdSpan = "#adviceANC-" + response[val][0] + "-span";
			
			var col = "<td id='adviceANC-" + response[val][0] + "'>"
					 + "<span id='adviceANC-" + response[val][0] + "-span' style='cursor:pointer'> <strong>"+detail(response[val][17])+"</strong> </span>";
			
			col = col + "<div id ='adviceANC-" + response[val][0] + "-dialog' hidden>" + response[val][17] + "</div></td>"
			
			$(document).on("click",colIdSpan, function(){
				id = $(this).attr('id');
				id = id.split("-")[1];
				colDialogId = "#adviceANC-" + id + "-dialog";
				
				textVal = $(colDialogId).text();
				textVal = textVal.slice(1, -1);
				
				if(textVal!=""){
					textVal = textVal.split(",");
				
					var i;
					var convertedText = "";
					for(i =0; i < textVal.length; i++){
						textVal[i] = textVal[i].slice(1,-1);
						convertedText = convertedText + "- " + adviceANCJS[0][textVal[i]] + "<br>";						
					}
					$('#detailList').html(convertedText);
					$('#detailInfoWindow').modal();
				}
				else{
					$('#detailList').html("There is no information")
					$('#detailInfoWindow').modal();				
				}										
			});
			tr.append(col);
		}
	});

	if (!response["ancStatus"]) {
		endCol = "<td><select id='adviceANC'  multiple='multiple'>";
		
		endCol = endCol + populateDropdownList(adviceANCJS[0]);
		
		endCol = endCol + "</select></td>";
		tr.append(endCol);
	}
	tr.append("</tr>");
	tableAnc.append(tr);

	// row 19
	var tr = $("<tr><td>রেফার	</td>");
	$.each(response1, function(key, val) {
		// alert("key : "+key+" ; value : "+val[1]);
		if (val != "ancStatus") {
			var value;
			if (response[val][18] == 1) {
				value = 'হ্যাঁ';
			} else {
				value = 'না';
			}
			var col = "<td id='referANC-" + response[val][0] + "'>" + value
					+ "</td>";
			tr.append(col);
		}

	});

	if (!response["ancStatus"]) {
		endCol = "<td><input type='checkbox' id='referANC'/></td>";
		tr.append(endCol);
	}
	tr.append("</tr>");
	tableAnc.append(tr);

	// row 20
	var tr = $("<tr><td>কেন্দ্রের নাম	</td>");
	referCenterJS = JSON.parse(referCenter);
	$.each(response1, function(key, val) {
		// alert("key : "+key+" ; value : "+val[1]);
		if (val != "ancStatus") {
			var col = "<td id='centerNameANC-" + response[val][0] + "'>"
					+ referCenterJS[0][response[val][19]] + "</td>";
			tr.append(col);
		}
	});

	if (!response["ancStatus"]) {
		endCol = "<td>"
				+ "<select id='centerNameANC' disabled='true'>";
		
		endCol = endCol + populateDropdownList(referCenterJS[0]);		
		
		endCol = endCol + "</select></td>";
		tr.append(endCol);
	}
	tr.append("</tr>");
	tableAnc.append(tr);

	// row 21
	var tr = $("<tr><td>কারণ	</td>");
	referReasonANCJS = JSON.parse(referReasonANC);
	$.each(response1, function(key, val) {
		 //alert("key : "+key+" ; value : "+val[1]);
		if (val != "ancStatus") {	
			//var col = "<td id='referReasonANC-" + response[val][0] + "'>"
				//	+ response[val][20] + "</td>";
			var colIdSpan = "#referReasonANC-" + response[val][0] + "-span";
			var colDialogId = "#referReasonANC-" + response[val][0] + "-dialog";
				
			var col = "<td id='referReasonANC-" + response[val][0] + "'>"
					 + "<span id='referReasonANC-" + response[val][0] + "-span' style='cursor:pointer'> <strong>"+detail(response[val][20])+"</strong> </span>";
			
			col = col + "<div id ='referReasonANC-" + response[val][0] + "-dialog' hidden>" + response[val][20] + "</div>"
			
			if(!response["ancStatus"] && key==count){
				ancProviderId=response[val][23];
				col = col + "<br><input type='button' class='right' id='deleteLastButtonANC' value='Delete'/>";
			}
			
			col = col + "</td>";
			$(document).on("click",colIdSpan, function(){
				textVal = $(colDialogId).text();
				textVal = textVal.slice(1, -1);		
				if(textVal!=""){
					textVal = textVal.split(",");			
					var i;
					var convertedText = "";
					for(i =0; i < textVal.length; i++){
						textVal[i] = textVal[i].slice(1,-1);
						convertedText = convertedText + "- " + referReasonANCJS[0][textVal[i]]+ "<br>";
					}					
					convertedText = convertedText.slice(0,-2);

					$('#detailList').html(convertedText);
					$('#detailInfoWindow').modal();		            	
				}
				else{
					$('#detailList').html("There is no information");
					$('#detailInfoWindow').modal();					
				}										
			});
			tr.append(col);
		}
	});
	
	if (!response["ancStatus"]) {
		endCol = "<td>"
				+ "<select id='referReasonANC' multiple='multiple' disabled='true'>";
		
		endCol = endCol + populateDropdownList(referReasonANCJS[0]);
		
		endCol = endCol + "</select><br>"
				+ "<input type='button' class='right' id='saveButtonANC' value='Save'/>"
				+ "</td>";

		tr.append(endCol);
	}
	tr.append("</tr>");
	tableAnc.append(tr);

	tableAnc.append("</tbody></table>");
	$('#ancVisitTable').remove();
	$('#ancVisit').append(tableAnc);

	$('#ancDate').combodate({
		format : "YYYY-MM-DD",
		template : "DD MMM YYYY",
		smartDays : true,
		//maxYear : 2050,
		value : moment()
	});

	fieldIds = ["#complicationANC","#symptomANC","#diseaseANC","#treatmentANC","#adviceANC","#referReasonANC"];
	$.each(fieldIds, function(key, val) {
		//multiSelectDropDownInit(val);
		$(val).multipleSelect({
			width: 300,
			maxHeight: 180,
			position : 'top',
			selectAll: false,
			selectedList : 1,
			styler : function() {
				return 'font-size: 18px;';
			}		
		});
	});
	
	//$('#bpSystolicANC').focus();
	// alert(response.ancVisit1[1]);
}

/*function populateDropdownList(jsonVal){
	
	output = "";		
	$.each(jsonVal, function(key, val) {
		output = output + "<option value = " + key + ">" + val + "</option>"; 			
	});		
	return output;
}
*/
/*function multiSelectDropDownInit(fieldId){
	$(fieldId).multipleSelect({
		width: 250,
		position : 'top',
		selectAll: false,
		selectedList : 1,
		styler : function() {
			return 'font-size: 14px;';
		}
	
	});
}*/

//onclick event on other service provider checkbox
$(document).on("click", '#otherServiceProviderCheckboxANC' ,function (){
	if($('#otherServiceProviderCheckboxANC').prop('checked')){
		$('#otherServiceProviderANC').show();		
	}   		
	else{
		$('#otherServiceProviderANC').hide();
	}
});

/*
// retrieve ANC information by expand the div and collapse
$(document).on("click", '#subheadingANCSlider',	function() {
	retrieveANCInfo();
	$('div#anc').slideToggle(100,function() {
		$('#subheadingANCSliderSpan').text(function() {
			// change text based on condition
			return $('div#anc').is(":visible") ? "গর্ভকালীন সেবা (-)" : "গর্ভকালীন সেবা (+)";
		});
	});
});
*/

// Only allowing numeric values for input fields and convert that to Bangla

$(document).on("keyup", '#bpSystolicANC', function() {
	$(this).val($(this).val().replace(/[^\d]/, ''));
	// $(this).val(en_to_ben_number_conversion($('#bpSystolicANC').val()));
});


$(document).on("keyup", '#bpDiastolicANC', function() {
	$(this).val($(this).val().replace(/[^\d]/, ''));
	// $(this).val(en_to_ben_number_conversion($('#bpDiastolicANC').val()));
});

$(document).on("keyup", '#weightANC', function() {
	// $(this).val($(this).val().replace(/[^\d]/,''));
	$(this).val($(this).val().match(/^\d*\.?\d*$/) ? $(this).val() : "");
	// $(this).val(en_to_ben_number_conversion($('#weightANC').val()));
});

$(document).on("keyup", '#uHeightANC', function() {
	$(this).val($(this).val().replace(/[^\d]/, ''));
	// $(this).val(en_to_ben_number_conversion($('#uHeightANC').val()));
});

$(document).on("keyup", '#hRateANC', function() {
	$(this).val($(this).val().replace(/[^\d]/, ''));
	// $(this).val(en_to_ben_number_conversion($('#hRateANC').val()));
});

$(document).on("keyup", '#hemoglobinANC', function() {
	$(this).val($(this).val().replace(/[^\d]/, ''));
	// $(this).val(en_to_ben_number_conversion($('#hRateANC').val()));
});

$(document).on("click", '#referANC',function (){
	handleOnclickCheckbox("#referANC","#centerNameANC", "#referReasonANC","enable","");	
});