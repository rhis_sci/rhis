
function getMIS3Data(MIS3RequestData){
	$.ajax({
		type : "POST",
		url : "mis3report",
		timeout:60000, //60 seconds timeout
		dataType : "json",
		data : {"MIS3RequestInfo" : JSON.stringify(MIS3RequestData)},
		success : function(response) {
			parseMIS3Info(response);
		},
		error : function(xhr, status, error) {
			alert(status);
			alert(error);
		}
	});
}

function parseMIS3Info(response){
	$('#mis3mnch').empty();
	
	var month_name = "";
	var year_value = "";
	if ($('#monthRange').is(':checked')) {
		var monthReport = $('#monthReport').val();
		var month_year = monthReport.split("-");
		month_name = month_convert(month_year[1]);
		year_value = en_to_ben_number_conversion(month_year[0]);
	}
	
	var facilityname="",zillaname="",upazilaname="",unionname="";
	
	var anc1center=0, anc2center=0, anc3center=0, anc4center=0, ancrefercenter=0;
	var anc1satelite=0, anc2satelite=0, anc3satelite=0, anc4satelite=0, ancrefersatelite=0;
	
	var normaldelivery=0, csectiondelivery=0, amtsl=0, livebirth=0, stillbirth=0, deliveryrefercenter=0, deliveryrefersatelite=0;
	
	var pacservicecount = 0;
		
	var pncmother1center=0, pncmother2center=0, pncmother3center=0, pncmother4center=0, pncmotherfpcenter=0, pncmotherrefercenter=0;
	var pncmother1satelite=0, pncmother2satelite=0, pncmother3satelite=0, pncmother4satelite=0, pncmotherfpsatelite=0, pncmotherrefersatelite=0;
	
	var pncchild1center=0, pncchild2center=0, pncchild3center=0, pncchild4center=0;
	var pncchild1satelite=0, pncchild2satelite=0, pncchild3satelite=0, pncchild4satelite=0;
	
	var newbornreferalcenter = 0, newbornreferalsatelite=0;
	
	if(response["providerinfo"][$('#providerid').text()].hasProviderdata == 1){
		facilityname = response["providerinfo"][$('#providerid').text()].facilityname;
		zillaname = response["providerinfo"][$('#providerid').text()].zillaname;
		upazilaname = response["providerinfo"][$('#providerid').text()].upazilaname;
		unionname = response["providerinfo"][$('#providerid').text()].unionname;
	}
	
	if(response["anc"][$('#providerid').text()].hasancdata == 1){
		anc1center = response["anc"][$('#providerid').text()].anc1center;
		anc2center = response["anc"][$('#providerid').text()].anc2center;
		anc3center = response["anc"][$('#providerid').text()].anc3center;
		anc4center = response["anc"][$('#providerid').text()].anc4center;
		
		anc1satelite = response["anc"][$('#providerid').text()].anc1satelite;
		anc2satelite = response["anc"][$('#providerid').text()].anc2satelite;
		anc3satelite = response["anc"][$('#providerid').text()].anc3satelite;
		anc4satelite = response["anc"][$('#providerid').text()].anc4satelite;
		
		ancrefercenter = response["anc"][$('#providerid').text()].ancrefercenter;
		ancrefersatelite = response["anc"][$('#providerid').text()].ancrefersatelite;
	}
	
	if(response["pncmother"][$('#providerid').text()].haspncmotherdata == 1){
		pncmother1center = response["pncmother"][$('#providerid').text()].pncmother1center;
		pncmother2center = response["pncmother"][$('#providerid').text()].pncmother2center;
		pncmother3center = response["pncmother"][$('#providerid').text()].pncmother3center;
		pncmother4center = response["pncmother"][$('#providerid').text()].pncmother4center;
		
		pncmother1satelite = response["pncmother"][$('#providerid').text()].pncmother1satelite;
		pncmother2satelite = response["pncmother"][$('#providerid').text()].pncmother2satelite;
		pncmother3satelite = response["pncmother"][$('#providerid').text()].pncmother3satelite;
		pncmother4satelite = response["pncmother"][$('#providerid').text()].pncmother4satelite;
		
		pncmotherfpcenter = response["pncmother"][$('#providerid').text()].pncmotherfpcenter;
		pncmotherfpsatelite = response["pncmother"][$('#providerid').text()].pncmotherfpsatelite;
		
		pncmotherrefercenter = response["pncmother"][$('#providerid').text()].pncmotherrefercenter;
		pncmotherrefersatelite = response["pncmother"][$('#providerid').text()].pncmotherrefersatelite;
	}
	
	if(response["pncchild"][$('#providerid').text()].haspncchilddata == 1){
		
		pncchild1center = response["pncchild"][$('#providerid').text()].pncchild1center;
		pncchild2center = response["pncchild"][$('#providerid').text()].pncchild2center;
		pncchild3center = response["pncchild"][$('#providerid').text()].pncchild3center;
		pncchild4center = response["pncchild"][$('#providerid').text()].pncchild4center;
		
		pncchild1satelite = response["pncchild"][$('#providerid').text()].pncchild1satelite;
		pncchild2satelite = response["pncchild"][$('#providerid').text()].pncchild2satelite;
		pncchild3satelite = response["pncchild"][$('#providerid').text()].pncchild3satelite;
		pncchild4satelite = response["pncchild"][$('#providerid').text()].pncchild4satelite;
	}
	
	if(response["delivery"][$('#providerid').text()].hasdeliverydata == 1){
		
		normaldelivery=response["delivery"][$('#providerid').text()].normaldelivery;
		csectiondelivery=response["delivery"][$('#providerid').text()].csectiondelivery;
		amtsl=response["delivery"][$('#providerid').text()].amtsl;
		livebirth=response["delivery"][$('#providerid').text()].livebirth;
		stillbirth=response["delivery"][$('#providerid').text()].stillbirth;
		
		deliveryrefercenter=response["delivery"][$('#providerid').text()].deliveryrefercenter;
		deliveryrefersatelite=response["delivery"][$('#providerid').text()].deliveryrefersatelite;
		
	}
	
	if(response["pac"][$('#providerid').text()].haspacdata == 1){
		
		pacservicecount=response["pac"][$('#providerid').text()].pacservicecount;
	}
	
	if(response["mnchnewbornrefer"][$('#providerid').text()].hasnewbornreferdata == 1){
		
		newbornreferalcenter=response["mnchnewbornrefer"][$('#providerid').text()].newbornreferalcenter;
		newbornreferalsatelite=response["mnchnewbornrefer"][$('#providerid').text()].newbornreferalsatelite;
	}
	
	//var table = "<table class='misTable'><tbody style='border:1px solid black;'>";
		
	var table = "<button id='btnPrint'>Print</button><div id='printDiv'>" 
		+ "<table width=\"940\" cellspacing=\"0\" style='font-size:10px;background-color: #EEE8AA;'><tr>"
		+ "<td><div style='border: 1px solid black;text-align: center;'>দুটি সন্তানের বেশি নয়<br>একটি হলে ভাল হয়।</div></td>"
		+ "<td style='text-align: center;'><b>গণপ্রজাতন্ত্রী বাংলাদেশ সরকার<br>পরিবার পরিকল্পনা অধিদপ্তর<br>পরিবার পরিকল্পনা, মা ও শিশু স্বাস্থ্য কার্যক্রমের মাসিক অগ্রগতির প্রতিবেদন</b>"
		+ "<br>(UH&FWC এবং অন্যান্য প্রতিষ্ঠানের জন্য প্রযোজ্য)<br>"
		+ "<b>মাসঃ</b> "+ month_name +" <b>সালঃ</b> "+ year_value +" </td>"
		+ "<td style='text-align: right;'>এমআইএস ফরম-৩</td>"
		+ "</tr><tr><td colspan='3'><img width='60' src='image/dgfp_logo.png'></td></tr><tr></table>";


	table += "<table width=\"940\" cellspacing=\"0\" class='misHeading'><tr>"
		+ "<td style='width: 38%'><b>কেন্দ্রের নামঃ  </b> "+facilityname+" </td>"
		+ "<td><b>ইউনিয়নঃ</b> "+unionname+"</td>"
		+ "<td style='text-align: center;'><b>উপজেলা/থানাঃ </b> "+upazilaname+"</td>"
		+ "<td style='text-align: right;'><b>জেলাঃ</b> "+zillaname+"</td>"
		+"</tr></table>";

	table += "<table class='misTable' border='1' cellspacing='0' width='940'><tbody style='border:1px solid black;'>";

	table +=  "<tr>" 
			+ "<td rowspan='23'>প্রজনন স্বাস্থ্য সেবা</td>" 
			+ "<td colspan='3'>সেবার ধরণ</td>"
			+ "<td>কেন্দ্র</td>"
			+ "<td>স্যাটেলাইট</td>"
			+ "<td>মোট</td>"
			+ "</tr>";
	
	table +=  "<tr>" 
			+ "<td rowspan='4' colspan='2'>গর্ভকালীন সেবা</td>"
			+ "<td>পরিদর্শন - ১</td>"
			+ "<td>" + anc1center + "</td>"
			+ "<td>" + anc1satelite + "</td>"
			+ "<td>" + (parseInt(anc1center) + parseInt(anc1satelite)) + "</td>"
			+ "</tr>";
		
	table +=  "<tr>" 
			+ "<td>পরিদর্শন - ২</td>"
			+ "<td>" + anc2center + "</td>"
			+ "<td>" + anc2satelite + "</td>"
			+ "<td>" + (parseInt(anc2center) + parseInt(anc2satelite)) + "</td>"
			+ "</tr>";
	
	table +=  "<tr>" 
			+ "<td>পরিদর্শন - ৩</td>"
			+ "<td>" + anc3center + "</td>"
			+ "<td>" + anc3satelite + "</td>"
			+ "<td>" + (parseInt(anc3center) + parseInt(anc3satelite)) + "</td>"
			+ "</tr>";
	
	table +=  "<tr>" 
			+ "<td>পরিদর্শন - ৪</td>"
			+ "<td>" + anc4center + "</td>"
			+ "<td>" + anc4satelite + "</td>"
			+ "<td>" + (parseInt(anc4center) + parseInt(anc4satelite)) + "</td>"
			+ "</tr>";
	
	table +=  "<tr>" 
			+ "<td rowspan='5' colspan='2'>প্রসব সেবা</td>"
			+ "<td>স্বাভাবিক</td>"
			+ "<td>" + normaldelivery + "</td>"
			+ "<td bgcolor='grey'></td>"
			+ "<td>" + normaldelivery + "</td>"
			+ "</tr>";
	
	table +=  "<tr>" 
			+ "<td>সিজারিয়ান</td>"
			+ "<td>" + csectiondelivery + "</td>"
			+ "<td bgcolor='grey'></td>"
			+ "<td>" + csectiondelivery + "</td>"
			+ "</tr>";
	
	table +=  "<tr>" 
			+ "<td>প্রসবের তৃতীয় ধাপের সক্রিয় ব্যবস্থাপনা (AMTSL) অনুসরণ করে প্রসব করানো</td>"
			+ "<td>" + amtsl + "</td>"
			+ "<td bgcolor='grey'></td>"
			+ "<td>" + amtsl + "</td>"
			+ "</tr>";
	
	table +=  "<tr>" 
			+ "<td>জীবিত জন্ম (Live Birth)</td>"
			+ "<td>" + livebirth + "</td>"
			+ "<td bgcolor='grey'></td>"
			+ "<td>" + livebirth + "</td>"
			+ "</tr>";
	
	table +=  "<tr>" 
			+ "<td>মৃত জন্ম (Still Birth)</td>"
			+ "<td>" + stillbirth + "</td>"
			+ "<td bgcolor='grey'></td>"
			+ "<td>" + stillbirth + "</td>"
			+ "</tr>";
	
	table +=  "<tr>" 
			+ "<td colspan='3'>গর্ভপাত পরবর্তী সেবা প্রদানের সংখ্যা</td>"
			+ "<td>" + pacservicecount + "</td>"
			+ "<td bgcolor='grey'></td>"
			+ "<td>" + pacservicecount + "</td>"
			+ "</tr>";
	
	table +=  "<tr>" 
			+ "<td rowspan='9'>প্রসবোত্তর সেবা</td>"
			+ "<td rowspan='5'>মা</td>"
			+ "<td>পরিদর্শন - ১</td>"
			+ "<td>" + pncmother1center + "</td>"
			+ "<td>" + pncmother1satelite + "</td>"
			+ "<td>" + (parseInt(pncmother1center) + parseInt(pncmother1satelite)) + "</td>"
			+ "</tr>";
	
	table +=  "<tr>" 
			+ "<td>পরিদর্শন - ২</td>"
			+ "<td>" + pncmother2center + "</td>"
			+ "<td>" + pncmother2satelite + "</td>"
			+ "<td>" + (parseInt(pncmother2center) + parseInt(pncmother2satelite)) + "</td>"
			+ "</tr>";

	table +=  "<tr>" 
			+ "<td>পরিদর্শন - ৩</td>"
			+ "<td>" + pncmother3center + "</td>"
			+ "<td>" + pncmother3satelite + "</td>"
			+ "<td>" + (parseInt(pncmother3center) + parseInt(pncmother3satelite)) + "</td>"
			+ "</tr>";

	table +=  "<tr>" 
			+ "<td>পরিদর্শন - ৪</td>"
			+ "<td>" + pncmother4center + "</td>"
			+ "<td>" + pncmother4satelite + "</td>"
			+ "<td>" + (parseInt(pncmother4center) + parseInt(pncmother4satelite)) + "</td>"
			+ "</tr>";
	
	table +=  "<tr>" 
			+ "<td>প্রসব পরবর্তী পরিবার পরিকল্পনা পদ্ধতি প্রদানের সংখ্যা</td>"
			+ "<td>" + pncmotherfpcenter + "</td>"
			+ "<td>" + pncmotherfpsatelite + "</td>"
			+ "<td>" + (parseInt(pncmotherfpcenter) + parseInt(pncmotherfpsatelite)) + "</td>"
			+ "</tr>";
	
	table +=  "<tr>" 
			+ "<td rowspan='4'>নবজাতক</td>"
			+ "<td>পরিদর্শন - ১</td>"
			+ "<td>" + pncchild1center + "</td>"
			+ "<td>" + pncchild1satelite + "</td>"
			+ "<td>" + (parseInt(pncchild1center) + parseInt(pncchild1satelite)) + "</td>"
			+ "</tr>";

	table +=  "<tr>" 
			+ "<td>পরিদর্শন - ২</td>"
			+ "<td>" + pncchild2center + "</td>"
			+ "<td>" + pncchild1satelite + "</td>"
			+ "<td>" + (parseInt(pncchild2center) + parseInt(pncchild1satelite)) + "</td>"
			+ "</tr>";

	table +=  "<tr>" 
			+ "<td>পরিদর্শন - ৩</td>"
			+ "<td>" + pncchild3center + "</td>"
			+ "<td>" + pncchild3satelite + "</td>"
			+ "<td>" + (parseInt(pncchild3center) + parseInt(pncchild3satelite)) + "</td>"
			+ "</tr>";

	table +=  "<tr>" 
			+ "<td>পরিদর্শন - ৪</td>"
			+ "<td>" + pncchild4center + "</td>"
			+ "<td>" + pncchild4satelite + "</td>"
			+ "<td>" + (parseInt(pncchild4center) + parseInt(pncchild4satelite)) + "</td>"
			+ "</tr>";

	table +=  "<tr>" 
			+ "<td rowspan='3' colspan='2'>রেফার</td>"
			+ "<td>গর্ভকালীন, প্রসবকালীন ও প্রসবোত্তর জটিলতার সংখ্যা</td>"
			+ "<td>" + (parseInt(ancrefercenter) + parseInt(deliveryrefercenter) + parseInt(pncmotherrefercenter)) + "</td>"
			+ "<td>" + (parseInt(ancrefersatelite) + parseInt(deliveryrefersatelite) + parseInt(pncmotherrefersatelite)) + "</td>"
			+ "<td>" + (parseInt(ancrefercenter) + parseInt(deliveryrefercenter) + parseInt(pncmotherrefercenter) + parseInt(ancrefersatelite) + parseInt(deliveryrefersatelite) + parseInt(pncmotherrefersatelite)) + "</td>"
			+ "</tr>";
	
	table +=  "<tr>" 
			+ "<td>একলাম্পসিয়া রোগীকে MgSO4 ইনজেকশন প্রদানের সংখ্যা</td>"
			+ "<td>0</td>"
			+ "<td>0</td>"
			+ "<td>0</td>"
			+ "</tr>";

	table +=  "<tr>" 
			+ "<td>নবজাতককে জটিলতার জন্য প্রেরণের সংখ্যা</td>"
			+ "<td>" + newbornreferalcenter + "</td>"
			+ "<td>" + newbornreferalsatelite + "</td>"
			+ "<td>" + (parseInt(newbornreferalcenter) + parseInt(newbornreferalsatelite)) + "</td>"
			+ "</tr>";
	
	table = table + "</tbody></table></div>"; 
		
	$('#mis3mnch').append(table);

}

function printData()
{
   var divToPrint=document.getElementById("printDiv");
   newWin= window.open("");
   newWin.document.write(divToPrint.outerHTML);
   newWin.print();
   newWin.close();
}

$(document).on("click", '#btnPrint' ,function (){
	printData();
});

$(document).on("click", '#getMIS3' ,function (){
	var MIS3RequestObj = new Object();
	MIS3RequestObj = setCommonParam();
	
	MIS3RequestObj.providerId = $('#providerid').text();
	
	if($('input[name=duration]:checked').val()==1){
		MIS3RequestObj.startDate = $('#startDateReport').val();
		MIS3RequestObj.endDate = $('#endDateReport').val();
	}
	else{
		if($('#monthReport').val()!= ""){
			MIS3RequestObj.startDate = moment($('#monthReport').val()).startOf('month').format('YYYY-MM-DD');
			MIS3RequestObj.endDate = moment($('#monthReport').val()).endOf('month').format('YYYY-MM-DD');
		}
		else{
			MIS3RequestObj.startDate = "";
			MIS3RequestObj.endDate = "";
		}
	}	
	
	if(MIS3RequestObj.startDate == "" || MIS3RequestObj.endDate == ""){
		callDialog(noDate);
	}
	else{
		getMIS3Data(MIS3RequestObj);
	}
});

$(document).on("click", 'input[name=duration]' ,function (){
		
	if($('input[name=duration]:checked').val()==1){
		$('#dayRangeSpan').show();
		$('#monthRangeSpan').hide();
	}
	else{
		$('#dayRangeSpan').hide();
		$('#monthRangeSpan').show();
	}		
});