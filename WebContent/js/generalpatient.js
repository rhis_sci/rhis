anemiaList = '[{"0":"","1":"+","2":"++","3":"+++"}]';
anemiaGPJS = JSON.parse(anemiaList);

jaundiceList = '[{"0":"","1":"নাই","2":"+ (অল্প)","3":"++ (মোটামুটি)","4":"+++ (অধিক)"}]';
jaundiceGPJS = JSON.parse(jaundiceList);

edemaList = '[{"0":"","1":"নাই","2":"+ (অল্প)","3":"++ (মোটামুটি)","4":"+++ (অধিক)"}]';
edemaGPJS = JSON.parse(edemaList);

urineSugarList = '[{"0":"","1":"নাই","2":"+","3":"++","4":"+++","5":"++++"}]';
urineSugarGPJS = JSON.parse(urineSugarList);

urineAlbuminList = '[{"0":"","1":"নাই","2":"+","3":"++","4":"+++","5":"++++"}]';
urineAlbuminGPJS = JSON.parse(urineAlbuminList);

generalDropdownList = '[{"0":"","1":"স্বাভাবিক","2":"স্বাভাবিক নয়"}]';
generalDropdownJS = JSON.parse(generalDropdownList);

/**
 * saving general patient's visit information
 */

function insertGPInfo() {

	var gpObj = new Object();
	gpObj = setCommonParam();
	
	gpObj.healthId = $('#healthid').text();
	gpObj.providerId = $('#providerid').text();
	gpObj.mobileNo = $('#cellNo').val();
	gpObj.age = ben_to_en_number_conversion($('#clientAge').val().toString());
	
	gpObj.visitDate = $('#GPvisitDate').val();
	gpObj.symptom = $('#GPsymptom').multipleSelect("getSelects");
	gpObj.anemia = $('#GPanemia').val();
	gpObj.hemoglobin = $('#GPhemoglobin').val();
	gpObj.jaundice = $('#GPjaundice').val();
	gpObj.edema = $('#GPedema').val();
	gpObj.pulse = $('#GPpulse').val();
	gpObj.temperature = $('#GPtemperature').val();
	gpObj.bpSystolic = $('#GPbpSystolic').val();
	gpObj.bpDiastolic = $('#GPbpDiastolic').val();
	gpObj.weight = $('#GPweight').val();
	gpObj.urineSugar = "";//$('#GPurineSugar').val();
	gpObj.urineAlbumin = "";//$('#GPurineAlbumin').val();
	
	gpObj.lungs = $('#GPlungs').val();
	gpObj.liver = $('#GPliver').val();
	gpObj.splin = $('#GPsplin').val();
	gpObj.tonsil = $('#GPtonsil').val();
	gpObj.other = $('#GPother').val();
	
	gpObj.complicationSign = "";//$('#GPcomplicationSign').multipleSelect("getSelects");
	gpObj.disease = $('#GPdisease').multipleSelect("getSelects");
	gpObj.treatment = $('#GPtreatment').multipleSelect("getSelects");
	gpObj.advice = $('#GPadvice').multipleSelect("getSelects");
	
	gpObj.refer = $('#GPrefer').prop('checked') ? 1 : 2;
	gpObj.referCenterName = $('#GPreferCenterName').val();
	gpObj.referReason = $('#GPreferReason').multipleSelect("getSelects");
	
	gpObj.serviceSource = "";
	gpObj.sateliteCenterName = "";
	gpObj.client = "1";
	
	gpObj.gpLoad = "";
		
	confirmationBoxGP(gpObj);
}

/**
 * retrieve general patient information
 */

function retrieveGPInfo() {
	
	var gpObj = new Object();
	gpObj = setCommonParam();

	gpObj.healthId = $('#healthid').text();
	
	gpObj.gpLoad = "retrieve";
	
	gpInfo(gpObj);	
}

/**
 * delete last general patient visit information
 */

function deleteLastGPInfo(){
	 
	var gpObj = new Object();
	gpObj = setCommonParam();
	
	gpObj.healthId = $('#healthid').text();
	
	gpObj.gpLoad = "delete";
		
	gpInfo(gpObj);	
}


/**
 * insert and retrieve gp visit information in a single AJAX call
 */

function gpInfo(gpObj) {
	
	if (gpObj.healthId != "") {
		$.ajax({
			type : "POST",
			url : "gp",
			timeout:60000, //60 seconds timeout
			dataType : "json",
			data : {"GPInfo" : JSON.stringify(gpObj)},
			success : function(response) {
				parseGPInfo(response);									
			},
			error : function(xhr, status, error) {
				alert(status);
				alert(error);
			}
		});		
	}	
}

/**
 * populating gp register's table dynamically
 */

function parseGPInfo(response) {
	
	if(response.regSerialNo!="" && response.regDate!=""){
		$("#regNumber").text(en_to_ben_number_conversion((response.regSerialNo<=9 ? ("0" + response.regSerialNo): response.regSerialNo) + "/" + moment(response.regDate).format("YY")).toString());
		$("#regDate").text(en_to_ben_number_conversion(moment(response.regDate).format("DD/MM/YYYY").toString()));
	}
	else{
		$("#regNumber").text("");
		$("#regDate").text("");
	}
	
	$('#content').empty();
	
	var divGeneralPatient = $("<div id='generalpatient' class='person_ANCdetail'>");
	
	var divHeading = $("<div id='subheading' class='subheadingside'> সেবার তথ্য </div>");
	divGeneralPatient.append(divHeading);
	
	var tableGeneralPatient = $("<table id='generalpatientVisitTable' border='1px'><tbody>");
	var tr, col, endCol, i;

	// row 1
	tr = $("<tr><td>পরিদর্শন </td>");
	for (i = 1; i <= response.count; i++) {
		col = "<td>" + en_to_ben_number_conversion(i.toString()) + "</td>";
		tr.append(col);
	}
	endCol = "<td>" + en_to_ben_number_conversion(((response.count)+1).toString())
			+ "</td>";
	tr.append(endCol);
	
	tr.append("</tr>");
	tableGeneralPatient.append(tr);
	
	//row 2
	tr = $("<tr><td>তারিখ </td>");
	for (i = 1; i <= response.count; i++) {
		col = "<td id='GPvisitDate-" + i + "'>"
			+ moment(response[i].visitDate).format("DD MMM YYYY") + "</td>";
		
		tr.append(col);
	}
	
	endCol = "<td><input type='text' id='GPvisitDate' maxlength='11' size='11'/>";	
	tr.append(endCol);
	
	tr.append("</td></tr>");
	tableGeneralPatient.append(tr);
	
	// row 3
	symptomGPJS = JSON.parse(symptomGP.replace(/\s+/g,""));
	tr = $(returnMultiSelectDropdownRow('অসুবিধা / উপসর্গ','GPsymptom',response,response.count, 'symptom','gpProviderId','providerId',symptomGPJS, 'deleteFalse','deleteLastButtonGP', 'saveDisable','saveButtonGP','enable'));
	
	tableGeneralPatient.append(tr);
	
	// row 4
	var tr = $("<tr><td>ওজন (কেজি)	</td>");
	for (i = 1; i <= response.count; i++) {
		
		if(response[i].weight >= t_gpweight){
			mark=danger;
		}
		else{
			mark=safe;
		}
		
		col = "<td id='GPweight-" + i + "'"+mark+">"
			+ en_to_ben_number_conversion((response[i].weight).toString()) + "</td>";
		tr.append(col);
	}
	
	endCol = "<td><input type='text' id='GPweight' maxlength='6' size='4'/></td>";
	
	tr.append(endCol);
	tr.append("</tr>");
	tableGeneralPatient.append(tr);
	
	//row 5
	tr = $("<tr><td>রক্তস্বল্পতা</td>");
	for (i = 1; i <= response.count; i++) {
		if(response[i].anemia==t_gpanemia){ 
			mark=danger;
		}
		else{
			mark=safe;
		}
		
		col = "<td id='GPanemia-" + i + "'"+mark+">"
			+ anemiaGPJS[0][response[i].anemia] + "</td>";
		tr.append(col);
	}
		
	endCol = "<td><select id='GPanemia'>";
	endCol = endCol + populateDropdownList(anemiaGPJS[0]);
	endCol = endCol + "</select></td>";
		
	tr.append(endCol);
	tr.append("</tr>");
	tableGeneralPatient.append(tr);
	
	//row 6
	tr = $("<tr><td> হিমোগ্লোবিন </td>");
	for (i = 1; i <= response.count; i++) {
		
			if(response[i].hemoglobin!='' && response[i].hemoglobin<=t_gphemoglobin){
				mark=danger;
			}		  
			else{
				mark=safe;	
			}
			
			if(response[i].hemoglobin==""){
				percent="";
			}
			else{
				percent="%";
			}
	
		col = "<td id='GPhemoglobin-" + i + "'"+mark+">"
			+ en_to_ben_number_conversion(response[i].hemoglobin) + percent + "</td>";
		tr.append(col);
	}
	
	endCol = "<td><input type='text' id='GPhemoglobin' maxlength='3' size='2'/>%</td>";
	
	tr.append(endCol);
	tr.append("</tr>");
	tableGeneralPatient.append(tr);
	
	// row 7
	tr = $("<tr><td>জন্ডিস </td>");
	for (i = 1; i <= response.count; i++) {
		
		col = "<td id='GPjaundice-" + i + "'>"
			+ jaundiceGPJS[0][response[i].jaundice] + "</td>";
		tr.append(col);
	}
	
	endCol = "<td><select id='GPjaundice'>";
	endCol = endCol + populateDropdownList(jaundiceGPJS[0]);
	endCol = endCol + "</select></td>";
		
	tr.append(endCol);
	tr.append("</tr>");
	tableGeneralPatient.append(tr);
	
	// row 8
	tr = $("<tr><td>ইডিমা </td>");
	for (i = 1; i <= response.count; i++) {
		
		col = "<td id='GPedema-" + i + "'>"
			+ edemaGPJS[0][response[i].edema] + "</td>";
		tr.append(col);
	}
	
	endCol = "<td><select id='GPedema'>";
	endCol = endCol + populateDropdownList(edemaGPJS[0]);
	endCol = endCol + "</select></td>";
		
	tr.append(endCol);
	tr.append("</tr>");
	tableGeneralPatient.append(tr);
	
	//row 9
	tr = $("<tr><td> নাড়ির গতি	</td>");
	for (i = 1; i <= response.count; i++) {
		
		if(response[i].pulse!='' && response[i].pulse>=t_gppulse){
			mark=danger;
		}		  
		else{
			mark=safe;	
		}
					
		col = "<td id='GPpulse-" + i + "'"+mark+">"
			+ en_to_ben_number_conversion(response[i].pulse) + "</td>";
		tr.append(col);
	}
	
	endCol = "<td><input type='text' id='GPpulse' maxlength='3' size='2'/></td>";
	
	tr.append(endCol);
	tr.append("</tr>");
	tableGeneralPatient.append(tr);
	
	// row 10
	tr = $("<tr><td>তাপমাত্রা	</td>");
	for (i = 1; i <= response.count; i++) {
		
		if(response[i].temperature >=t_gptemperature){
			mark=danger;
		}
		else{
			mark=safe;
		}
		
		col = "<td id='GPtemperature-" + i + "'"+mark+">"
			+ en_to_ben_number_conversion((response[i].temperature).toString()) 
			+ (response[i].temperature!=""?"°ফা:":"")
			+ "</td>";
		tr.append(col);
	}
	
	endCol = "<td><input type='text' id='GPtemperature' maxlength='3' size='3'/>°ফা:</td>";
	
	tr.append(endCol);
	tr.append("</tr>");
	tableGeneralPatient.append(tr);
	
	// row 11
	tr = $("<tr><td>রক্তচাপ (মিঃ মিঃ মার্কারি) </td>");
	for (i = 1; i <= response.count; i++) {
		
		if(response[i].bpSystolic!="" || response[i].bpDiastolic!="" )
			var slash="/";
		else    
			var slash="";
		
		if( (response[i].bpSystolic!='' && response[i].bpDiastolic!='') && ((response[i].bpSystolic>=t_gpbpsys) || (response[i].bpDiastolic>=t_gpbpdias) )){
			mark=danger;
		}
		else{
			mark=safe; 
		}
		
		col = "<td id='GPbpSystolic-" + i + "'"+mark+">"
			+ en_to_ben_number_conversion((response[i].bpSystolic).toString())
			+ slash
			+ en_to_ben_number_conversion((response[i].bpDiastolic).toString())
			+ "</td>";
		tr.append(col);
	}
	
	endCol = "<td><input type='text' id='GPbpSystolic' maxlength='3' size='3'/> / <input type='text' id='GPbpDiastolic' maxlength='3' size='3'/></td>";
	
	tr.append(endCol);
	tr.append("</tr>");
	tableGeneralPatient.append(tr);
	/*
	// row 12
	tr = $("<tr><td>প্রস্রাব পরীক্ষা - সুগার </td>");
	for (i = 1; i <= response.count; i++) {
		
		if(response[i].urineSugar >=t_gpsugar){
			mark=danger;
		}
		else{
			mark=safe;
		}
					
		col = "<td id='GPurineSugar-" + i + "'" + mark + ">"
			+ urineSugarGPJS[0][response[i].urineSugar] + "</td>";
		tr.append(col);
	}
	
	endCol = "<td><select id='GPurineSugar'>";
	endCol = endCol + populateDropdownList(urineSugarGPJS[0]);
	endCol = endCol + "</select></td>";
		
	tr.append(endCol);
	tr.append("</tr>");
	tableGeneralPatient.append(tr);
	
	// row 13
	tr = $("<tr><td>প্রস্রাব পরীক্ষা - এলবুমিন </td>");
	for (i = 1; i <= response.count; i++) {
		if(response[i].urineAlbumin >=t_gpalbumin){
			mark=danger;
		}
		else{
			mark=safe;
		}
			
		col = "<td id='GPurineAlbumin-" + i + "'" + mark + ">"
			+ urineAlbuminGPJS[0][response[i].urineAlbumin] + "</td>";
		tr.append(col);
	}
	
	endCol = "<td><select id='GPurineAlbumin'>";
	endCol = endCol + populateDropdownList(urineAlbuminGPJS[0]);
	endCol = endCol + "</select></td>";
		
	tr.append(endCol);
	tr.append("</tr>");
	tableGeneralPatient.append(tr);
	*/
	// row 14
	tr = $("<tr><td>ফুসফুস </td>");
	for (i = 1; i <= response.count; i++) {
		
		col = "<td id='GPlungs-" + i + "'>"
			+ generalDropdownJS[0][response[i].lungs] + "</td>";
		tr.append(col);
	}
	
	endCol = "<td><select id='GPlungs'>";
	endCol = endCol + populateDropdownList(generalDropdownJS[0]);
	endCol = endCol + "</select></td>";
		
	tr.append(endCol);
	tr.append("</tr>");
	tableGeneralPatient.append(tr);
	
	// row 15
	tr = $("<tr><td>লিভার </td>");
	for (i = 1; i <= response.count; i++) {
		
		col = "<td id='GPliver-" + i + "'>"
			+ generalDropdownJS[0][response[i].liver] + "</td>";
		tr.append(col);
	}
	
	endCol = "<td><select id='GPliver'>";
	endCol = endCol + populateDropdownList(generalDropdownJS[0]);
	endCol = endCol + "</select></td>";
		
	tr.append(endCol);
	tr.append("</tr>");
	tableGeneralPatient.append(tr);
	
	// row 16
	tr = $("<tr><td>প্লিহা </td>");
	for (i = 1; i <= response.count; i++) {
		
		col = "<td id='GPsplin-" + i + "'>"
			+ generalDropdownJS[0][response[i].splin] + "</td>";
		tr.append(col);
	}
	
	endCol = "<td><select id='GPsplin'>";
	endCol = endCol + populateDropdownList(generalDropdownJS[0]);
	endCol = endCol + "</select></td>";
		
	tr.append(endCol);
	tr.append("</tr>");
	tableGeneralPatient.append(tr);
	
	// row 17
	tr = $("<tr><td>টনসিল </td>");
	for (i = 1; i <= response.count; i++) {
		
		col = "<td id='GPtonsil-" + i + "'>"
			+ generalDropdownJS[0][response[i].tonsil] + "</td>";
		tr.append(col);
	}
	
	endCol = "<td><select id='GPtonsil'>";
	endCol = endCol + populateDropdownList(generalDropdownJS[0]);
	endCol = endCol + "</select></td>";
		
	tr.append(endCol);
	tr.append("</tr>");
	tableGeneralPatient.append(tr);
	
	// row 18
	tr = $("<tr><td>অন্যান্য </td>");
	for (i = 1; i <= response.count; i++) {
		
		col = "<td id='GPother-" + i + "'>"
			+ generalDropdownJS[0][response[i].other] + "</td>";
		tr.append(col);
	}
	
	endCol = "<td><select id='GPother'>";
	endCol = endCol + populateDropdownList(generalDropdownJS[0]);
	endCol = endCol + "</select></td>";
		
	tr.append(endCol);
	tr.append("</tr>");
	tableGeneralPatient.append(tr);
	
	// row 18
	//dangerSignGPJS = JSON.parse(dangerSignGP);
	//tr = $(returnMultiSelectDropdownRow('বিপদ চিহ্ন / জটিলতা','GPcomplicationSign',response,response.count, 'complicationSign','gpProviderId','providerId',dangerSignGPJS, 'deleteFalse','deleteLastButtonGP', 'saveDisable','saveButtonGP','enable'));

	//tableGeneralPatient.append(tr);
	
	// row 19
	diseaseGPJS = JSON.parse(diseaseGP.replace(/\s+/g,""));
	tr = $(returnMultiSelectDropdownRow('রোগ','GPdisease',response,response.count, 'disease','gpProviderId','providerId',diseaseGPJS, 'deleteFalse','deleteLastButtonGP', 'saveDisable','saveButtonGP','enable'));
	
	tableGeneralPatient.append(tr);
	
	// row 20
	treatmentGPJS = JSON.parse(treatmentGP.replace(/\s+/g,""));
	tr = $(returnMultiSelectDropdownRow('চিকিৎসা','GPtreatment',response,response.count, 'treatment','gpProviderId','providerId',treatmentGPJS, 'deleteFalse','deleteLastButtonGP', 'saveDisable','saveButtonGP','enable'));
	
	tableGeneralPatient.append(tr);
	
	// row 21
	adviceGPJS = JSON.parse(adviceGP);
	tr = $(returnMultiSelectDropdownRow('উপদেশ','GPadvice',response,response.count, 'advice','gpProviderId','providerId',adviceGPJS, 'deleteFalse','deleteLastButtonGP', 'saveDisable','saveButtonGP','enable'));
	
	tableGeneralPatient.append(tr);
	
	// row 22
	var tr = $("<tr><td>রেফার	</td>");
	for (i = 1; i <= response.count; i++) {
		col = "<td id='GPrefer-" + i + "'>"
			+ (response[i].refer==1?'হ্যাঁ':'না') + "</td>";
		tr.append(col);		
	}
	
	endCol = "<td><input type='checkbox' id='GPrefer'/></td>";
	
	tr.append(endCol);	
	tr.append("</tr>");
	tableGeneralPatient.append(tr);
	
	// row 23
	var tr = $("<tr><td>কেন্দ্রের নাম	</td>");
	referCenterJS = JSON.parse(referCenter);
	for (i = 1; i <= response.count; i++) {
		
		col = "<td id='GPreferCenterName-" + i + "'>"
			+ referCenterJS[0][response[i].referCenterName] + "</td>";
		tr.append(col);
	}
	
	endCol = "<td><select id='GPreferCenterName' disabled='true'>";
	endCol = endCol + populateDropdownList(referCenterJS[0]);
	endCol = endCol + "</select></td>";
	
	tr.append(endCol);
	tr.append("</tr>");
	tableGeneralPatient.append(tr);
	
	// row 24
	referReasonGPJS = JSON.parse(referReasonGP.replace(/\s+/g,""));
	tr = $(returnMultiSelectDropdownRow('কারণ','GPreferReason',response,response.count, 'referReason','gpProviderId','providerId',referReasonGPJS, 'deleteTrue','deleteLastButtonGP', 'saveEnable','saveButtonGP','disable'));
	
	tableGeneralPatient.append(tr);
	
	tableGeneralPatient.append("</tbody></table>");
	divGeneralPatient.append(tableGeneralPatient);
	divGeneralPatient.append("</div>");
	$('#content').append(divGeneralPatient);
	
	fieldInitGP();	
}

// Only allowing numeric values for input fields
$(document).on("keyup", '#GPhemoglobin,#GPpulse,#GPbpSystolic,#GPbpDiastolic', function() {
	$(this).val($(this).val().replace(/[^\d]/, ''));
});
$(document).on("keyup", '#GPweight,#GPtemperature', function() {
	$(this).val($(this).val().match(/^\d*\.?\d*$/) ? $(this).val() : "");
});

function fieldInitGP(){
		
	$('#GPvisitDate').combodate({
		format : "YYYY-MM-DD",
		template : "DD MMM YYYY",
		smartDays : true,
		value : moment()
	});
	
	fieldId = ["#GPcomplicationSign,#GPdisease,#GPtreatment,#GPadvice,#GPreferReason"];
	$.each(fieldId, function(key, val) {
		$(val).multipleSelect({
			width : 250,
			position : 'top',
			selectAll: false,
			selectedList: 1,
			maxHeight: 100,
			styler : function() {
				return 'font-size: 18px;';
			}	
		});
	});	
	
	$('#GPsymptom').multipleSelect({
		width : 250,
		selectAll: false,
		selectedList: 1,
		maxHeight: 100,
		styler : function() {
			return 'font-size: 18px;';
		}	
	});
}


function confirmationBoxGP(gpObj){
	
	var userVal = "";
	
	userVal = userVal + "<table class='list'><tr><td>তারিখ: " + moment(gpObj.visitDate).format("DD MMM YYYY") + "</td></tr>";
	
	userVal = userVal + "<table class='list'><tr><td>অসুবিধা / উপসর্গ:</td></tr> ";
	userVal+=returnMultiSelectedDropdownContent(gpObj.symptom, symptomGPJS);
	
	userVal = userVal +"<tr><td>"+ linemark_switch("ওজন (কেজি)",gpObj.weight,"");		   
	userVal += en_to_ben_number_conversion(gpObj.weight.toString()); 
	userVal+="</td></tr>";
	
	userVal = userVal +"<tr><td>"+ linemark_switch("রক্তস্বল্পতা",gpObj.anemia,0);		   
	userVal += anemiaGPJS[0][gpObj.anemia]; 
	userVal+="</td></tr>"; 
	
	userVal = userVal +"<tr><td>"+ linemark_switch("হিমোগ্লোবিন",gpObj.hemoglobin,"");		   
	userVal += en_to_ben_number_conversion(gpObj.hemoglobin.toString()) + (gpObj.hemoglobin==""?"":"%"); 
	userVal+="</td></tr>";
		
	userVal = userVal +"<tr><td>"+ linemark_switch("জন্ডিস",gpObj.jaundice,0);		   
	userVal += jaundiceGPJS[0][gpObj.jaundice]; 
	userVal+="</td></tr>";
	
	userVal = userVal +"<tr><td>"+ linemark_switch("ইডিমা",gpObj.edema,0);		   
	userVal += edemaGPJS[0][gpObj.edema]; 
	userVal+="</td></tr>";
	
	userVal = userVal +"<tr><td>"+ linemark_switch("নাড়ির গতি",gpObj.pulse,"");		   
	userVal += en_to_ben_number_conversion(gpObj.pulse.toString()); 
	userVal+="</td></tr>";
	
	userVal = userVal +"<tr><td>"+ linemark_switch("তাপমাত্রা",gpObj.temperature,"");		   
	userVal += en_to_ben_number_conversion(gpObj.temperature.toString()) + (gpObj.temperature==""?"":"°ফা:"); 
	userVal+="</td></tr>";
	
	if(gpObj.bpSystolic!="" || gpObj.bpDiastolic!="" ){
		var slash="/";
	}
	else{    
		var slash="";
	}
	userVal = userVal +"<tr><td>"+((gpObj.bpSystolic=="" || gpObj.bpDiastolic=="") ? "<span style=\"color: red;\">" : "<span style=\"color: black;\">")+ "রক্তচাপ (মিঃ মিঃ মার্কারি):</span>" 
			+ en_to_ben_number_conversion(gpObj.bpSystolic.toString()) + slash + en_to_ben_number_conversion(gpObj.bpDiastolic.toString()) +"</td></tr>";
	
	/*userVal = userVal +"<tr><td>"+ linemark_switch("প্রস্রাব পরীক্ষা - সুগার",gpObj.urineSugar,0);		   
	userVal += urineSugarGPJS[0][gpObj.urineSugar]; 
	userVal+="</td></tr>";
	
	userVal = userVal +"<tr><td>"+ linemark_switch("প্রস্রাব পরীক্ষা - এলবুমিন",gpObj.urineAlbumin,0);		   
	userVal += urineAlbuminGPJS[0][gpObj.urineAlbumin]; 
	userVal+="</td></tr>";
	*/
	userVal = userVal +"<tr><td>"+ linemark_switch("ফুসফুস",gpObj.lungs,0);		   
	userVal += generalDropdownJS[0][gpObj.lungs]; 
	userVal+="</td></tr>";
	
	userVal = userVal +"<tr><td>"+ linemark_switch("লিভার",gpObj.liver,0);		   
	userVal += generalDropdownJS[0][gpObj.liver]; 
	userVal+="</td></tr>";
	
	userVal = userVal +"<tr><td>"+ linemark_switch("প্লিহা",gpObj.splin,0);		   
	userVal += generalDropdownJS[0][gpObj.splin]; 
	userVal+="</td></tr>";

	userVal = userVal +"<tr><td>"+ linemark_switch("টনসিল",gpObj.tonsil,0);		   
	userVal += generalDropdownJS[0][gpObj.tonsil]; 
	userVal+="</td></tr>";
	
	userVal = userVal +"<tr><td>"+ linemark_switch("অন্যান্য",gpObj.other,0);		   
	userVal += generalDropdownJS[0][gpObj.other]; 
	userVal+="</td></tr>";
	
	//userVal = userVal + "<table class='list'><tr><td>বিপদ চিহ্ন / জটিলতা:</td></tr> ";
	//userVal+=returnMultiSelectedDropdownContent(gpObj.complicationSign, dangerSignGPJS);
	
	userVal = userVal + "<table class='list'><tr><td>রোগ:</td></tr> ";
	userVal+=returnMultiSelectedDropdownContent(gpObj.disease, diseaseGPJS);
		
	userVal = userVal + "<table class='list'><tr><td>চিকিৎসা:</td></tr> ";
	userVal+=returnMultiSelectedDropdownContent(gpObj.treatment, treatmentGPJS);
		
	userVal = userVal + "<table class='list'><tr><td>উপদেশ:</td></tr> ";
	userVal+=returnMultiSelectedDropdownContent(gpObj.advice, adviceGPJS);
		
	userVal = userVal + "<tr><td>রেফার: "+(gpObj.refer=="1" ? "হ্যাঁ" : "না")+"</td></tr>";
    
    if(gpObj.refer=="1"){
    	   userVal = userVal +"<tr><td>"+ linemark_switch("কেন্দ্রের নাম",gpObj.referCenterName,0);		   
		   userVal += referCenterJS[0][gpObj.referCenterName]; 
		   userVal+="</td></tr>"; 
		   
    	   userVal = userVal +"<tr><td>"+linemark_switch("কারণ",gpObj.referReason,"")+"</td></tr>";
    	   userVal+=returnMultiSelectedDropdownContent(gpObj.referReason, referReasonGPJS);   	   
    }   
	
	userVal = userVal + "</table>";
	
	if(gpObj.visitDate!=""){
	    $('<div></div>').dialog({
	        modal: true,
	        width: 450,
	        hide: {effect: 'fade', duration: 500},
	        show: {effect: 'fade', duration: 600},
	        title: "নিশ্চিত করুন",
	        open: function () {
	            $(this).html(userVal);
	        },
	        buttons: {
	        	 "Save" : function () {      
	        		 gpInfo(gpObj);
	             	$(this).dialog("close");
	             },
	             "Cancel": function () {
	        		$(this).dialog("close");
	        	}
	        }
	    });
	}
	else{
		callDialog(noDate);
	}	
}

$(document).on("click", '#saveButtonGP',function (){
	insertGPInfo();
});

$(document).on("click", '#deleteLastButtonGP', function(event){
	deleteLast('gp',gpProviderId,'')
});

$(document).on("click", '#GPrefer',function (){
	handleOnclickCheckbox("#GPrefer","#GPreferCenterName", "#GPreferReason","enable","");	
});