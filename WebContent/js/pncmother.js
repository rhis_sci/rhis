function deleteLastPNCMotherInfo(){
	 
	var PNCObj = new Object();
	PNCObj = setCommonParam();
	
	PNCObj.healthid = $('#healthid').text();
	PNCObj.pregno = $('#pregNumber').text();
	
	PNCObj.pncMLoad = "delete";
	
	PNCMotherInfo(PNCObj);	
}

/**
 * saving PNC Mother information
 */

function insertPNCMotherInfo() {

	var PNCObj = new Object();
	PNCObj = setCommonParam();

	PNCObj.healthid = $('#healthid').text();
	PNCObj.pregno = $('#pregNumber').text();
	PNCObj.providerid = $('#providerid').text();
	PNCObj.pncdate = $('#pncmotherDate').val();
	PNCObj.pnctemperature = $('#pncmotherTemperature').val();
	PNCObj.pncbpsys = $('#pncmotherbpSystolic').val();
	PNCObj.pncbpdias = $('#pncmotherbpDiastolic').val();
	PNCObj.pncanemia = $('#pncmotherAnemia').val();
	PNCObj.pnchemoglobin = $('#pncmotherHemoglobin').val();
	PNCObj.pncedema = $('#pncmotherEdema').val();
	PNCObj.pncbreastcondition = $('#pncmotherBreastCondition').val();
	PNCObj.pncuterusinvolution = $('#pncmotherUterusInvolution').val();
	PNCObj.pnchematuria = $('#pncmotherHematuria').val();
	PNCObj.pncperineum = $('#pncmotherPerineum').val();
	PNCObj.pncfpmethod = $('#pncmotherFPMethod').val();
	PNCObj.pnccomplicationsign = $('#pncmotherComplication').multipleSelect("getSelects");
	PNCObj.pncsymptom = $('#pncmotherSymptom').multipleSelect("getSelects");
	PNCObj.pncdisease = $('#pncmotherDisease').multipleSelect("getSelects");
	PNCObj.pnctreatment = $('#pncmotherTreatment').multipleSelect("getSelects");
	PNCObj.pncadvice = $('#pncmotherAdvice').multipleSelect("getSelects");
	PNCObj.pncrefer = $('#pncmotherRefer').prop('checked') ? 1 : 2;
	PNCObj.pncreferreason = $('#pncmotherReferReason').multipleSelect("getSelects");
	PNCObj.pncrefercentername = $('#pncmotherReferCenterName').val();
		
	if($('#otherServiceProviderCheckboxPNCM').prop('checked')){
		PNCObj.pncservicesource = $('#otherServiceProviderPNCM').val();
	}
	else{
		PNCObj.pncservicesource = "";
	}
	//PNCObj.pncservicesource = "GOVERNMENT";
	PNCObj.sateliteCenterName = "";
	PNCObj.client = "1";
	PNCObj.pncMLoad = "";

	
	
	var userVal = "<table class='list'>";
	userVal = userVal + "<tr><td>সেবাগ্রহনের তারিখ: " + moment(PNCObj.pncdate).format("DD MMM YYYY") + "</td></tr>";
	
	userVal = userVal + "<tr><td>অসুবিধা:</td></tr>";
		
		textVal=PNCObj.pncsymptom;
		symptomPNCMJS = JSON.parse(symptomPNCM);
		var i;
		var convertedText = "  ";

		for(i =0; i < textVal.length; i++){
			convertedText = convertedText + "<tr><td>-"+symptomPNCMJS[0][textVal[i]] + "</td></tr>";  			
	}
	userVal+=convertedText+"</table>";
		
	userVal = userVal +"<table class='confirmation'><tr><td>"+linemark("তাপমাত্রা",PNCObj.pnctemperature)+"</td>";
	
	if(PNCObj.pncbpsys!="" && PNCObj.pncbpdias!="" )
		var slash="/";
	else    
		var slash="";
	
	userVal = userVal +"<td colspan='2'>"+((PNCObj.pncbpsys=="" || PNCObj.pncbpdias=="") ? "<span style=\"color: red;\">" : "<span style=\"color: black;\">")+ "রক্তচাপ (মিঃ মিঃ মার্কারি):</span>" + PNCObj.pncbpsys + slash + PNCObj.pncbpdias+"</td></tr>";
	
	userVal = userVal +"<tr><td>"+ linemark_switch("রক্তস্বল্পতা ",PNCObj.pncanemia,'none');
	switch (PNCObj.pncanemia) {
		case '0':
			userVal+= '';
			break;
		case '1':
			userVal+= '+ (স্বল্প )';
			break;
		case '2':
			userVal+= '++ (মধ্যম)';
			break;
		case '3':
			userVal+= '+++ (অতিরিক্ত)';
			break;
	}
	userVal+="</td>";
	
	userVal = userVal +"<td>"+ linemark("হিমোগ্লোবিন",PNCObj.pnchemoglobin);
		if(PNCObj.pnchemoglobin!='')
			userVal+="%";
    userVal+="</td></tr>";
	
	userVal = userVal + "<tr><td>ইডিমা:";
    switch (PNCObj.pncedema) {
    case '0':
		userVal+= '';
		break;
    case '1':
		userVal+= 'নাই';
		break;
	case '2':
		userVal+= '+ (অল্প)';
		break;
	case '3':
		userVal+= '++ (মোটামুটি)';
		break;
	case '4':
		userVal+= '+++ (অধিক)';
		break;
	}
    userVal+="</td>";
   
	userVal = userVal + "<td>স্তনের অবস্থা: ";
    switch (PNCObj.pncbreastcondition) {
    	case '0':
    		userVal+= '';
    		break;
    	case '1':
    		userVal+= 'স্বাভাবিক';
    		break;
	    case '2':
			userVal+= 'ফোলা';
			break;
	    case '3':
			userVal+= 'ব্যথাযুক্ত';
			break;
       }
      userVal+="</td></tr>";
      
    userVal = userVal +"<tr><td>জরায়ুর ইনভলিউশন : ";
	switch (PNCObj.pncuterusinvolution) {
		case '0':
			userVal+= '';
			break;
		case '1':
			userVal+= 'নাভি বরাবর';
			break;
		case '2':
			userVal+= 'নাভি হতে ২-৩ ইঞ্চি নিচে';
			break;
		case '3':
			userVal+= 'অনুভব করা যাচ্ছে না';
			break;
	}
	userVal+="</td>";
    
    userVal = userVal + "<td>স্রাব/ রক্তস্রাব: ";
      switch (PNCObj.pnchematuria) {
      	case '0':
	      	userVal+= '';
			break;
      	case '1':
        	userVal+= 'স্বাভাবিক';
        	break;
	    case '2':
	    	userVal+= 'দূর্গন্ধযুক্ত';
	    	break;
	    case '3':
	    	userVal+= 'অতিরিক্ত';
	    	break;
      }
      userVal+="</td></tr>";
      
      
      userVal = userVal + "<tr><td>পেরিনিয়াম: ";
        switch (PNCObj.pncperineum) {
        	case '0':
				userVal+='';
				break;
        	case '1':
  				userVal+='স্বাভাবিক';
  				break;
  			case '2':
  				userVal+='ছিঁড়ে গিয়েছে';
  				break;
  			case '3':
  				userVal+='অন্যান্য জটিলতা';
  				break;
  		}
  		userVal+="</td>";
  	
  		userVal = userVal + "<td>পরিবার পরিকল্পনা পদ্ধতি(পরামর্শ / পদ্ধতি প্রদান):";
  		
  		switch (PNCObj.pncfpmethod) {
  			case '0':
				userVal+='';
				break;
  			case '1':
  				userVal+='পরামর্শ';
  				break;
  			case '2':
  				userVal+='Condom';
  				break;
  			case '3':
  				userVal+='IUD';
  				break;
  			case '4':
  				userVal+='Emergency Contraceptive Pill (ECP)';
  				break;
  			case '5':
  				userVal+='Injectable';
  				break;
  			case '6':
  				userVal+='Implant';
  				break;
  		}
  		
  		userVal+="</td></tr></table>";
  		
		userVal = userVal + "<table class='list'><tr><td>বিপদ চিহ্ন / জটিলতা:</tr></td> ";
		textVal=PNCObj.pnccomplicationsign;
		dangerSignPNCMJS = JSON.parse(dangerSignPNCM);
		var i;
		var convertedText = "  ";
		
		for(i =0; i < textVal.length; i++){
			convertedText = convertedText +"<tr><td>-"+dangerSignPNCMJS[0][textVal[i]] +"</tr></td>";				
		}
		userVal+=convertedText;

  		userVal = userVal + "<tr><td>রোগ:</tr></td>";	
  		textVal=PNCObj.pncdisease;
  		diseasePNCMJS = JSON.parse(diseasePNCM);
  		var i;
  		var convertedText = "";
	
  		for(i =0; i < textVal.length; i++){
  			convertedText = convertedText +"<tr><td>-"+ diseasePNCMJS[0][textVal[i]] + "</tr></td>";
		}
  		userVal+=convertedText;
			
    userVal = userVal + "<tr><td>চিকিৎসা: </tr></td>";
    textVal=PNCObj.pnctreatment;
    treatmentJS = JSON.parse(treatment);
    convertedText = "";
	
	for(i =0; i < textVal.length; i++){
		convertedText = convertedText + "<tr><td>-"+treatmentJS[0][textVal[i]] + "</tr></td>";
	}
	userVal+=convertedText;
    
	userVal = userVal + "<tr><td>পরামর্শ:</tr></td>";
	textVal=PNCObj.pncadvice;
	advicePNCMJS = JSON.parse(advicePNCM);
	var i;
	var convertedText = "  ";
	
	for(i =0; i < textVal.length; i++){
		convertedText = convertedText + "<tr><td>-"+advicePNCMJS[0][textVal[i]] + "</tr></td>";
	}
	userVal+=convertedText;
    
    
    userVal = userVal + "<tr><td>রেফার: "+(PNCObj.pncrefer=="1" ? "হ্যাঁ" : "না")+"</tr></td>";
    
    if(PNCObj.pncrefer=="1"){
    	   userVal = userVal +"<tr><td>"+ linemark_switch("কেন্দ্রের নাম",PNCObj.pncrefercentername,0);		   
		   
    	   referCenterJS = JSON.parse(referCenter);
    	   userVal += referCenterJS[0][PNCObj.pncrefercentername]; 
		   
    	   userVal+="</tr></td>"; 
		   
    	   userVal = userVal +"<tr><td>"+linemark_switch("কারণ",PNCObj.pncreferreason,0)+"</tr></td>";
    	   referReasonPNCMJS = JSON.parse(referReasonPNCM);
		   textVal=PNCObj.pncreferreason;
		   var i;
		   var convertedText = "  ";
	
		   for(i =0; i < textVal.length; i++){
			   convertedText = convertedText +"<tr><td>-"+ referReasonPNCMJS[0][textVal[i]] + "</tr></td>";	
		   }
		   userVal+=convertedText.slice(0,-2)+"";
	
    }   
    
    userVal+="</table>";
    $('#idealPNC1Date')
    if(moment(PNCObj.pncdate) >= moment($('#idealPNC1Date').text())){
	    $('<div></div>').dialog({
	        modal: true,
	        width: 680,
	        hide: {effect: 'fade', duration: 500},
	        show: {effect: 'fade', duration: 600},
	        title: "নিশ্চিত করুন",
	        open: function () {
	            $(this).html(userVal);
	        },
	        buttons: {
	        	"Save & Continue" : function () {      
	        		PNCMotherInfo(PNCObj);
	            	$(this).dialog("close");
	            },           
				"Save & Serve Next Client": function () {
					hideFlag="doHide";
					PNCMotherInfo(PNCObj);				
	        		goToId='.nextC';	
				    $(goToId).click();	
	            	$(this).dialog("close");
	            },
	           /* "Save & Open PNC Child Information": function () {
	            	hideFlag="doGoToChild";
	            	PNCMotherInfo(PNCObj);
	            	var divPosition = $('div#pncchild').offset();
	        		$('html, body').animate({scrollTop: divPosition.top}, "slow");
	            	//$('#pncVisitChild').focus();
				    /*goToId='#2.serviceTabButton';	
				    $(goToId).click();          	
	            	$(this).dialog("close");
	            },*/
	            Cancel: function () {
	        		$(this).dialog("close");
	        	}
	        }
	    }); //end confirm dialog
	}
	else{
		callDialog(pncDateLessThanDelivery);
	}
}

/**
 * retrieve PNC Mother information
 */
function retrievePNCMotherInfo() {
	
	var PNCObj = new Object();
	PNCObj = setCommonParam();

	PNCObj.pregno = $('#pregNumber').text();
	PNCObj.healthid = $('#healthid').text();
	PNCObj.pncMLoad = "retrieve";
	
	PNCMotherInfo(PNCObj);	
}

/**
 * insert and retrieve PNC Mother information in a single AJAX call
 */
function PNCMotherInfo(pncMotherInfo) {
	var deliveryStatusAjax = "";
	
	if (pncMotherInfo.healthid != "" && pncMotherInfo.pregno != "") {
		$.ajax({
			type : "POST",
			url : "pncmother",
			timeout:60000, //60 seconds timeout
			dataType : "json",
			data : {"PNCMotherInfo" : JSON.stringify(pncMotherInfo)},
			success : function(response) {
				deliveryStatusAjax = parsePNCMotherInfo(response);
				deliveryInformationPNC(deliveryStatusAjax);						
			},
			error : function(xhr, status, error) {
				alert(status);
				alert(error);
			}
		});		
	}	
}

/**
 * populating pnc table dynamically for mother
 */
function parsePNCMotherInfo(response) {
	var deliveryStatusParse = "";
	if(response.hasDeliveryInformation=="Yes"){
		
		if (response.pncStatus) {		
			callDialog(noPNC);			
		}
		
		//suggested visit date
		var suggestedPNCDate1 = moment(response.outcomeDate).format("DD MMM YYYY");
		var suggestedPNCDate2 = moment(response.outcomeDate).add({days:1}).format("DD MMM YYYY") + " - " + moment(response.outcomeDate).add({days:2}).format("DD MMM YYYY");
		var suggestedPNCDate3 = moment(response.outcomeDate).add({days:6}).format("DD MMM YYYY") + " - " + moment(response.outcomeDate).add({days:13}).format("DD MMM YYYY");
		var suggestedPNCDate4 = moment(response.outcomeDate).add({days:36}).format("DD MMM YYYY") + " - " + moment(response.outcomeDate).add({days:42}).format("DD MMM YYYY");
		
		$('#idealPNC1Date').html(suggestedPNCDate1);
		$('#idealPNC2Date').html(suggestedPNCDate2);
		$('#idealPNC3Date').html(suggestedPNCDate3);
		$('#idealPNC4Date').html(suggestedPNCDate4);
		
		document.getElementById('hasDeliveryInfo').innerHTML="";
		document.getElementById('hasDeliveryInfo').innerHTML=response.hasDeliveryInformation;
		deliveryStatusParse = 1;
				
	// populating pnc table
	var tablePNCMother = $("<table id='pncVisitMotherTable' border='1px'><tbody>");
	var tr, col, endCol, i;

	// row 1
	tr = $("<tr><td>পরিদর্শন </td>");
	for (i = 1; i <= response.count; i++) {
		col = "<td>" + en_to_ben_number_conversion(i.toString()) + "</td>";
		tr.append(col);
	}
	if (!response.pncStatus) {
		endCol = "<td>" + en_to_ben_number_conversion(((response.count)+1).toString())
				+ "</td>";
		tr.append(endCol);
	}
	tr.append("</tr>");
	tablePNCMother.append(tr);

	// row 2
	tr = $("<tr><td>সেবাগ্রহনের তারিখ	</td>");
	otherServiceCenterJS =  JSON.parse(referCenter);
	for (i = 1; i <= response.count; i++) {
		col = "<td id='pncmotherDate-" + i + "'>"
			+ moment(response[i].visitDate).format("DD MMM YYYY");
		if(response[i].serviceSource!=""){
			col = col + "<br>" + otherServiceCenterJS[0][response[i].serviceSource];
		}
		col = col + "</td>";
		tr.append(col);
	}
	if (!response.pncStatus) {
		endCol = "<td><input type='text' id='pncmotherDate' maxlength='11' size='11'/>&nbsp;<input type='checkbox' id='otherServiceProviderCheckboxPNCM'/> অন্যান্য উৎস <br>"
				+ "<select id='otherServiceProviderPNCM' hidden='true'>";
	
		$.each(otherServiceCenterJS[0], function(key, val) {
			endCol = endCol + "<option value = " + key + ">" + val + "</option>"; 			
		});	
				
		endCol = endCol + "</select></td>"; 
		tr.append(endCol);
	}
	tr.append("</tr>");
	tablePNCMother.append(tr);
	
	// row 15
	tr = $("<tr><td>অসুবিধা	</td>");
	symptomPNCMJS = JSON.parse(symptomPNCM);
	for(i=1; i<=response.count; i++){
		var colIdSpan = "#pncmotherSymptom-" + i + "-span";
							
		col = "<td id='pncmotherSymptom-" + i + "'>"
				 + "<span id='pncmotherSymptom-" + i + "-span' style='cursor:pointer'> <strong>"+detail(response[i].symptom)+"</strong> </span>";
		
		col = col + "<div id ='pncmotherSymptom-" + i + "-dialog' hidden>" + response[i].symptom + "</div></td>";
		
		$(document).on("click",colIdSpan, function(){
			id = $(this).attr('id');
			id = id.split("-")[1];
			colDialogId = "#pncmotherSymptom-" + id + "-dialog";
			
			textVal = $(colDialogId).text();
			textVal = textVal.slice(1, -1);
			
			
			if(textVal!=""){
				textVal = textVal.split(",");
				var j;
				var convertedText = "";
			
				for(j =0; j < textVal.length; j++){
					textVal[j] = textVal[j].slice(1,-1);
					convertedText = convertedText + "- " + symptomPNCMJS[0][textVal[j]] + "<br>";					
				}
				$('#detailList').html(convertedText);
				$('#detailInfoWindow').modal();			
			}
			else{
				$('#detailList').html("There is no information")
				$('#detailInfoWindow').modal();
			}
		});
		tr.append(col);
	}
	
	if (!response.pncStatus) {
		endCol = "<td><select id='pncmotherSymptom'  multiple='multiple'>";
		
		endCol = endCol + populateDropdownList(symptomPNCMJS[0]);
		
		endCol = endCol + "</select></td>";
	
		tr.append(endCol);
	}
	tr.append("</tr>");
	tablePNCMother.append(tr);
	
	// row 3
	tr = $("<tr><td>তাপমাত্রা	</td>");
	for (i = 1; i <= response.count; i++) {
		
		if(response[i].temperature >=t_pncMtemperature)
			mark=danger;
		else mark=safe;
		
		col = "<td id='pncmotherTemperature-" + i + "'"+mark+">"
			+ en_to_ben_number_conversion((response[i].temperature).toString()) + "</td>";
		tr.append(col);
	}
	if (!response.pncStatus) {
		endCol = "<td><input type='text' id='pncmotherTemperature' maxlength='6' size='6'/></td>";
		tr.append(endCol);
	}
	tr.append("</tr>");
	tablePNCMother.append(tr);
	
	// row 4
	tr = $("<tr><td><span class=\"tp\" id=\"pncm_bp\">রক্তচাপ (মিঃ মিঃ মার্কারি) </span></td>");
	for (i = 1; i <= response.count; i++) {
		
		if(response[i].bpSystolic!="" || response[i].bpDiastolic!="" )
			var slash="/";
		else    
			var slash="";
		
		if( (response[i].bpSystolic!='' && response[i].bpDiastolic!='') && ((response[i].bpSystolic<=t_pncMbpsys) || (response[i].bpDiastolic>=t_pncMbpdias) ))
		mark=danger;
		else mark=safe; 
		
		col = "<td id='pncmotherbpSystolic-" + i + "'"+mark+">"
			+ en_to_ben_number_conversion((response[i].bpSystolic).toString())
			+ slash
			+ en_to_ben_number_conversion((response[i].bpDiastolic).toString())
			+ "</td>";
		tr.append(col);
	}
	if (!response.pncStatus) {
		endCol = "<td><input type='text' id='pncmotherbpSystolic' maxlength='3' size='3'/> / <input type='text' id='pncmotherbpDiastolic' maxlength='3' size='3'/></td>";
		tr.append(endCol);
	}
	tr.append("</tr>");
	tablePNCMother.append(tr);
	
	// row 5 old bp representation

	// row 6
	tr = $("<tr><td><span class=\"tp\" id=\"pncm_anemia\">রক্তস্বল্পতা	</span></td>");
	for (i = 1; i <= response.count; i++) {
		
			if(response[i].anemia==t_pncManemia) 
			mark=danger;
			else mark=safe;
			 
			//if(response[i].anemia=="0")
				//response[i].anemia="";
			var value = response[i].anemia;
			switch (value) {
			case '0':
				value = '';
				break;
			case '1':
				value = '+';
				break;
			case '2':
				value = '++';
				break;
			case '3':
				value = '+++';
				break;
			}
		col = "<td id='pncmotherAnemia-" + i + "'"+mark+">"
			+ value + "</td>";
		tr.append(col);
	}
	if (!response.pncStatus) {
		endCol = "<td>" + "<select id='pncmotherAnemia'>"
		+ "<option value='0'></option>"
		+ "<option value='1'>+</option>"
		+ "<option value='2'>++</option>"
		+ "<option value='3'>+++</option>"				 
		+ "</select></td>";
		tr.append(endCol);
	}
	tr.append("</tr>");
	tablePNCMother.append(tr);
	
	// row 7
	tr = $("<tr><td><span class=\"tp\" id=\"pncm_himoglobin\"> হিমোগ্লোবিন	</span></td>");
	for (i = 1; i <= response.count; i++) {
		
			if(response[i].hemoglobin!='' && response[i].hemoglobin<=t_pncMhemoglobin)
			  mark=danger;
			  else mark=safe;	
			
			if(response[i].hemoglobin=="")
				percent="";
			else
				percent="%";
	
		col = "<td id='pncmotherHemoglobin-" + i + "'"+mark+">"
			+ en_to_ben_number_conversion(response[i].hemoglobin) + percent + "</td>";
		tr.append(col);
	}
	if (!response.pncStatus) {
		endCol = "<td><input type='text' id='pncmotherHemoglobin' maxlength='3' size='2'/>%</td>";
		tr.append(endCol);
	}
	tr.append("</tr>");
	tablePNCMother.append(tr);
	
	// row 8
	var tr = $("<tr><td><span class=\"tp\" id=\"pncm_edima\">ইডিমা </span>	</td>");
	for (i = 1; i <= response.count; i++) {
		var value = response[i].edema;
		switch (value) {
		case '0':
			value = '';
			break;
		case '1':
			value = 'নাই';
			break;
		case '2':
			value = '+ (অল্প)';
			break;
		case '3':
			value = '++ (মোটামুটি)';
			break;
		case '4':
			value = '+++ (অধিক)';
			break;
		}
		
		if(response[i].edema==t_pncMedema)
				 mark=danger;
			else mark=safe;
			
		var col = "<td id='pncmotherEdema-" + i + "'"+mark+">" + value
					+ "</td>";
		
		tr.append(col);
	}
	
	if (!response.pncStatus) {
		endCol = "<td><select id='pncmotherEdema'> "
				+ "<option value='0'></option>"
				+ "<option value='1'>নাই</option>"
				+ "<option value='2'>+ (অল্প)</option>"
				+ "<option value='3'>++ (মোটামুটি)</option>"
				+ "<option value='4'>+++ (অধিক)</option>" + "</select></td>";
		tr.append(endCol);
	}
	tr.append("</tr>");
	tablePNCMother.append(tr);
	
	// row 9
	tr = $("<tr><td>স্তনের অবস্থা	</td>");
	for (i = 1; i <= response.count; i++) {
		var value = response[i].breastCondition;
		switch (value) {
			case '0':
				value = '';
				break;
			case '1':
				value = 'স্বাভাবিক';
				break;
			case '2':
				value = 'ফোলা';
				break;
			case '3':
				value = 'ব্যথাযুক্ত';
				break;
		}
		
		/*if(response[i].breastCondition!=t_pncMbreastcondition)
				 mark=danger;
			else*/ mark=safe;
			
		col = "<td id='pncmotherBreastCondition-" + i + "'"+mark+">"
			+ value + "</td>";
		tr.append(col);
	}
	if (!response.pncStatus) {
		endCol = "<td><select id='pncmotherBreastCondition'> "
				+ "<option value='0'></option>"
				+ "<option value='1'>স্বাভাবিক</option>"
				+ "<option value='2'>ফোলা</option>"
				+ "<option value='3'>ব্যথাযুক্ত</option>" 
				+ "</select></td>";
		tr.append(endCol);		
	}
	tr.append("</tr>");
	tablePNCMother.append(tr);
	
	// row 10
	tr = $("<tr><td><span class=\"tp\" id=\"pncm_Utr_Inv\">জরায়ুর ইনভলিউশন<span>	</td>");
	for (i = 1; i <= response.count; i++) {	
		var value = response[i].uterusInvolution;
		switch (value) {
			case '0':
				value = '';
				break;
			case '1':
				value = 'নাভি বরাবর';
				break;
			case '2':
				value = 'নাভি হতে ২-৩ ইঞ্চি নিচে';
				break;
			case '3':
				value = 'অনুভব করা যাচ্ছে না';
				break;
		}
		/*if(response[i].uterusInvolution>=t_pncMuterusinvolution)
				mark=danger;
			else*/ mark=safe;
		
		col = "<td id='pncmotherUterusInvolution-" + i + "'"+mark+">"
			+  value+ "</td>";
		tr.append(col);		
	}
	if (!response.pncStatus) {
		endCol = "<td><select id='pncmotherUterusInvolution'> "
			+ "<option value='0'></option>"
			+ "<option value='1'>নাভি বরাবর</option>"
			+ "<option value='2'>নাভি হতে ২-৩ ইঞ্চি নিচে</option>"
			+ "<option value='3'>অনুভব করা যাচ্ছে না</option>" 
			+ "</select></td>";
		tr.append(endCol);		
	}
	tr.append("</tr>");
	tablePNCMother.append(tr);
	
	// row 11
	tr = $("<tr><td>স্রাব/ রক্তস্রাব	</td>");
	for (i = 1; i <= response.count; i++) {
		var value = response[i].hematuria;
		switch (value) {
			case '0':
				value = '';
				break;
			case '1':
				value = 'স্বাভাবিক';
				break;
			case '2':
				value = 'দূর্গন্ধযুক্ত';
				break;
			case '3':
				value = 'অতিরিক্ত';
				break;
		}
	/*if(response[i].hematuria!=t_pncMhematuria)
				mark=danger;
			else*/ mark=safe;
			
		col = "<td id='pncmotherHematuria-" + i + "'"+mark+">"
			+ value + "</td>";
		tr.append(col);
	}
	if (!response.pncStatus) {
		endCol = "<td><select id='pncmotherHematuria'> "
				+ "<option value='0'></option>"
				+ "<option value='1'>স্বাভাবিক</option>"
				+ "<option value='2'>দূর্গন্ধযুক্ত</option>"
				+ "<option value='3'>অতিরিক্ত</option>" 
				+ "</select></td>";
		tr.append(endCol);		
	}
	tr.append("</tr>");
	tablePNCMother.append(tr);
	
	// row 12
	tr = $("<tr><td><span class=\"tp\" id=\"perineum\">পেরিনিয়াম	</span></td>");
	for (i = 1; i <= response.count; i++) {
		var value = response[i].perineum;
		switch (value) {
			case '0':
				value = '';
				break;
			case '1':
				value = 'স্বাভাবিক';
				break;
			case '2':
				value = 'ছিঁড়ে গিয়েছে';
				break;
			case '3':
				value = 'অন্যান্য জটিলতা';
				break;
		}
		
		/*if(response[i].perineum!=t_pncMperineum)
				mark=danger;
			else*/ mark=safe;
			
		col = "<td id='pncmotherPerineum-" + i + "'"+mark+">"
			+ value + "</td>";
		tr.append(col);
	}
	if (!response.pncStatus) {
		endCol = "<td><select id='pncmotherPerineum'> "
				+ "<option value='0'></option>"
				+ "<option value='1'>স্বাভাবিক</option>"
				+ "<option value='2'>ছিঁড়ে গিয়েছে</option>"
				+ "<option value='3'>অন্যান্য জটিলতা</option>" 
				+ "</select></td>";
		tr.append(endCol);		
	}
	tr.append("</tr>");
	tablePNCMother.append(tr);
	
	// row 13
	tr = $("<tr><td style='white-space: nowrap'>পরিবার পরিকল্পনা পদ্ধতি(পরামর্শ / পদ্ধতি প্রদান)	</td>");
	for (i = 1; i <= response.count; i++) {
		var value = response[i].FPMethod;
		switch (value) {
			case '0':
				value = '';
				break;
			case '1':
				value = 'পরামর্শ';
				break;
			case '2':
				value = 'Condom';
				break;
			case '3':
				value = 'IUD';
				break;
			case '4':
				value = 'Emergency Contraceptive Pill (ECP)';
				break;
			case '5':
				value = 'Injectable';
				break;
			case '6':
				value = 'Implant';
				break;
		}
		col = "<td id='pncmotherFPMethod-" + i + "'>"
			+ value + "</td>";
		tr.append(col);
	}
	if (!response.pncStatus) {
		endCol = "<td><select id='pncmotherFPMethod'> "
				+ "<option value='0'></option>"
				+ "<option value='1'>পরামর্শ</option>"
				+ "<option value='2'>Condom</option>"
				+ "<option value='3'>IUD</option>"
				+ "<option value='4'>Emergency Contraceptive Pill (ECP)</option>"
				+ "<option value='5'>Injectable</option>"
				+ "<option value='6'>Implant</option>"
				+ "</select></td>";
		tr.append(endCol);		
	}
	tr.append("</tr>");
	tablePNCMother.append(tr);
	
	// row 14
	tr = $("<tr><td>বিপদ চিহ্ন / জটিলতা	</td>");
	dangerSignPNCMJS = JSON.parse(dangerSignPNCM);
	for(i=1; i<=response.count; i++){
		var colIdSpan = "#pncmotherComplication-" + i + "-span";
							
		col = "<td id='pncmotherComplication-" + i + "'>"
				 + "<span id='pncmotherComplication-" + i + "-span' style='cursor:pointer'> <strong>"+detail(response[i].complicationsign)+"</strong> </span>";
		
		col = col + "<div id ='pncmotherComplication-" + i + "-dialog' hidden>" + response[i].complicationsign + "</div></td>";
		
		$(document).on("click",colIdSpan, function(){
			id = $(this).attr('id');
			id = id.split("-")[1];
			colDialogId = "#pncmotherComplication-" + id + "-dialog";
			
			textVal = $(colDialogId).text();
			textVal = textVal.slice(1, -1);
			
			
			if(textVal!=""){
				textVal = textVal.split(",");
				var j;
				var convertedText = "";
			
				for(j =0; j < textVal.length; j++){
					textVal[j] = textVal[j].slice(1,-1);
					convertedText = convertedText + "- " + dangerSignPNCMJS[0][textVal[j]] + "<br>";					
				}
				$('#detailList').html(convertedText);
				$('#detailInfoWindow').modal();			
			}
			else{
				$('#detailList').html("There is no information")
				$('#detailInfoWindow').modal();
			}
		});
		tr.append(col);
	}
	
	if (!response.pncStatus) {
		endCol = "<td><select id='pncmotherComplication'  multiple='multiple'>";
		
		endCol = endCol + populateDropdownList(dangerSignPNCMJS[0]);
		
		endCol = endCol + "</select></td>";
	
		tr.append(endCol);
	}
	tr.append("</tr>");
	tablePNCMother.append(tr);
		
	// row 16
	tr = $("<tr><td>রোগ	</td>");
	diseasePNCMJS = JSON.parse(diseasePNCM);
	for(i=1; i<=response.count; i++){
		var colIdSpan = "#pncmotherDisease-" + i + "-span";
							
		col = "<td id='pncmotherDisease-" + i + "'>"
				 + "<span id='pncmotherDisease-" + i + "-span' style='cursor:pointer'> <strong>"+detail(response[i].disease)+"</strong> </span>";
		
		col = col + "<div id ='pncmotherDisease-" + i + "-dialog' hidden>" + response[i].disease + "</div></td>";
		
		$(document).on("click",colIdSpan, function(){
			id = $(this).attr('id');
			id = id.split("-")[1];
			colDialogId = "#pncmotherDisease-" + id + "-dialog";
			
			textVal = $(colDialogId).text();
			textVal = textVal.slice(1, -1);
			
			if(textVal!=""){
				textVal = textVal.split(",");
			
				var j;
				var convertedText = "";
				for(j =0; j < textVal.length; j++){
					textVal[j] = textVal[j].slice(1,-1);
					convertedText = convertedText + "- " + diseasePNCMJS[0][textVal[j]] + "<br>";					
				}				
				$('#detailList').html(convertedText);
				$('#detailInfoWindow').modal();
			}
			else{
				$('#detailList').html("There is no information")
				$('#detailInfoWindow').modal();				
			}
		});
		tr.append(col);
	}
	
	if (!response.pncStatus) {
		endCol = "<td><select id='pncmotherDisease'  multiple='multiple'>";
		
		endCol = endCol + populateDropdownList(diseasePNCMJS[0]);
		
		endCol = endCol + "</select></td>";

		tr.append(endCol);
	}
	tr.append("</tr>");
	tablePNCMother.append(tr);
	
	// row 17
	tr = $("<tr><td>চিকিৎসা	</td>");
	
	treatmentJS = JSON.parse(treatment);
	for(i=1; i<=response.count; i++){
		var colIdSpan = "#pncmotherTreatment-" + i + "-span";
							
		col = "<td id='pncmotherTreatment-" + i + "'>"
				 + "<span id='pncmotherTreatment-" + i + "-span' style='cursor:pointer'> <strong>"+detail(response[i].treatment)+"</strong> </span>";
		
		col = col + "<div id ='pncmotherTreatment-" + i + "-dialog' hidden>" + response[i].treatment + "</div></td>";
		
		$(document).on("click",colIdSpan, function(){
			id = $(this).attr('id');
			id = id.split("-")[1];
			colDialogId = "#pncmotherTreatment-" + id + "-dialog";
			
			textVal = $(colDialogId).text();
			textVal = textVal.slice(1, -1);
			
			if(textVal!=""){
				textVal = textVal.split(",");
			
				var j;
				var convertedText = "";
				for(j =0; j < textVal.length; j++){
					textVal[j] = textVal[j].slice(1,-1);
					convertedText = convertedText + "- " + treatmentJS[0][textVal[j]] + "<br>";					
				}				
				$('#detailList').html(convertedText);
				$('#detailInfoWindow').modal();
			}
			else{
				$('#detailList').html("There is no information")
				$('#detailInfoWindow').modal();				
			}
		});
		tr.append(col);
	}
	
	if (!response.pncStatus) {
		endCol = "<td><select id='pncmotherTreatment'  multiple='multiple'>";
		
		endCol = endCol + populateDropdownList(treatmentJS[0]);
		
		endCol = endCol + "</select></td>";
		tr.append(endCol);
	}
	tr.append("</tr>");
	tablePNCMother.append(tr);
	
	
	// row 18
	tr = $("<tr><td>পরামর্শ	</td>");
	advicePNCMJS = JSON.parse(advicePNCM);
	for(i=1; i<=response.count; i++){
		var colIdSpan = "#pncmotherAdvice-" + i + "-span";
							
		col = "<td id='pncmotherAdvice-" + i + "'>"
				 + "<span id='pncmotherAdvice-" + i + "-span' style='cursor:pointer'> <strong>"+detail(response[i].advice)+"</strong> </span>";
		
		col = col + "<div id ='pncmotherAdvice-" + i + "-dialog' hidden>" + response[i].advice + "</div></td>";
		
		$(document).on("click",colIdSpan, function(){
			id = $(this).attr('id');
			id = id.split("-")[1];
			colDialogId = "#pncmotherAdvice-" + id + "-dialog";
			
			textVal = $(colDialogId).text();
			textVal = textVal.slice(1, -1);
			
			if(textVal!=""){
				textVal = textVal.split(",");
			
				var j;
				var convertedText = "";
				for(j =0; j < textVal.length; j++){
					textVal[j] = textVal[j].slice(1,-1);
					convertedText = convertedText + "- " + advicePNCMJS[0][textVal[j]] + "<br>";					
				}				
				$('#detailList').html(convertedText);
				$('#detailInfoWindow').modal();
			}
			else{
				$('#detailList').html("There is no information")
				$('#detailInfoWindow').modal();				
			}
		});
		tr.append(col);
	}
	
	if (!response.pncStatus) {
		endCol = "<td><select id='pncmotherAdvice'  multiple='multiple'>";
		
		endCol = endCol + populateDropdownList(advicePNCMJS[0]);
		
		endCol = endCol + "</select></td>";
		tr.append(endCol);
	}
	tr.append("</tr>");
	tablePNCMother.append(tr);
	
	// row 19
	var tr = $("<tr><td>রেফার	</td>");
	
	for (i = 1; i <= response.count; i++) {
		var value = response[i].refer;
		switch (value) {
			case '1':
				value = 'হ্যাঁ';
				break;
			case '2':
				value = 'না';
				break;			
		}
		col = "<td id='pncmotherRefer-" + i + "'>"
			+ value + "</td>";
		tr.append(col);		
	}
	if (!response.pncStatus) {
		endCol = "<td><input type='checkbox' id='pncmotherRefer'/></td>";
		tr.append(endCol);	
	}
	tr.append("</tr>");
	tablePNCMother.append(tr);
	
	// row 20
	var tr = $("<tr><td>কেন্দ্রের নাম	</td>");
	referCenterJS = JSON.parse(referCenter);
	for (i = 1; i <= response.count; i++) {
		col = "<td id='pncmotherReferCenterName-" + i + "'>"
			+ referCenterJS[0][response[i].referCenterName] + "</td>";
		tr.append(col);		
	}
	
	if (!response.pncStatus) {
		endCol = "<td>"
				+ "<select id='pncmotherReferCenterName' disabled='true'>";
		
		endCol = endCol + populateDropdownList(referCenterJS[0]);			
		
		endCol = endCol + "</select></td>";
		tr.append(endCol);
	}
	tr.append("</tr>");
	tablePNCMother.append(tr);
	
	// row 21
	tr = $("<tr><td>কারণ	</td>");
	referReasonPNCMJS = JSON.parse(referReasonPNCM);
	for(i=1; i<=response.count; i++){
		var colIdSpan = "#pncmotherReferReason-" + i + "-span";
						
		col = "<td id='pncmotherReferReason-" + i + "'>"
				 + "<span id='pncmotherReferReason-" + i + "-span' style='cursor:pointer'> <strong>"+detail(response[i].referReason)+"</strong> </span>";
		
		col = col + "<div id ='pncmotherReferReason-" + i + "-dialog' hidden>" + response[i].referReason+"</div>";
		
		if(!response.pncStatus && i==response.count){					
			pncMProviderId=response[i].providerId;			
			col = col + "<br>"
			+ "<input type='button' class='right' id='deleteLastButtonPNCMother' value='Delete'/>";								
		}
		col = col + "</td>";
		
		$(document).on("click",colIdSpan, function(){
			id = $(this).attr('id');
			id = id.split("-")[1];
			colDialogId = "#pncmotherReferReason-" + id + "-dialog";
			
			textVal = $(colDialogId).text();
			
			textVal = textVal.slice(1, -1);
			
			if(textVal!=""){
				textVal = textVal.split(",");
			
				var j;
				var convertedText = "";
				for(j =0; j < textVal.length; j++){
					textVal[j] = textVal[j].slice(1,-1);
					convertedText = convertedText + "- " + referReasonPNCMJS[0][textVal[j]] + "<br>";			
				}
				
				$('#detailList').html(convertedText);
				$('#detailInfoWindow').modal();
			}
			else{
				$('#detailList').html("There is no information")
				$('#detailInfoWindow').modal();
				//alert("There is no information");
			}
		});
		tr.append(col);	
	}
		
	if (!response.pncStatus) {
		endCol = "<td><select id='pncmotherReferReason'  multiple='multiple' disabled='true'>";
		
		endCol = endCol + populateDropdownList(referReasonPNCMJS[0]);
			
		endCol = endCol + "</select><br>"
				+ "<input type='button' class='right' id='saveButtonPNCMother' value='Save'/>"
				+ "</td>";

		tr.append(endCol);
	}
	tr.append("</tr>");
	tablePNCMother.append(tr);
	
	tablePNCMother.append("</tbody></table>");
	$('#pncVisitMotherTable').remove();
	$('#pncVisitMother').append(tablePNCMother);

	$('#pncmotherDate').combodate({
		format : "YYYY-MM-DD",
		template : "DD MMM YYYY",
		smartDays : true,
		maxYear : 2050,
		value : moment()
	});
	
	fieldId = ["#pncmotherComplication","#pncmotherSymptom","#pncmotherDisease","#pncmotherTreatment","#pncmotherAdvice","#pncmotherReferReason"];
	$.each(fieldId, function(key, val) {
		//multiSelectDropDownInit(val);
		$(val).multipleSelect({
			width : 300,
			position : 'top',
			selectAll: false,
			selectedList : 1,
			styler : function() {
				return 'font-size: 18px;';
			}	
		});
	});	
	
	
	$('div#pncmother').show();	
	//$('#pncmotherTemperature').focus();

	
	
	if(hideFlag=="doHide")
	{
		$('div#pncmother').hide();
		$('.nextC').hide();
		hideFlag="";
	}	
	
  }
  else{
	  document.getElementById('hasDeliveryInfo').innerHTML=response.hasDeliveryInformation;
	  deliveryStatusParse = 2;
	  	  
	  $('div#pncmother').hide();	  
  }

	//alert(deliveryStatusParse);
	return deliveryStatusParse;
}

/*function populateDropdownList(jsonVal){
	
	output = "";		
	$.each(jsonVal, function(key, val) {
		output = output + "<option value = " + key + ">" + val + "</option>"; 			
	});		
	return output;
}*/

/*function multiSelectDropDownInit(fieldId){
	$(fieldId).multipleSelect({
		width : 188,
		position : 'top',
		selectAll: false,
		multiple: true,
		selectedList : 1,
		styler : function() {
			return 'font-size: 14px;';
		}	
	});
}*/

//onclick event on other service provider checkbox
$(document).on("click", '#otherServiceProviderCheckboxPNCM' ,function (){
	if($('#otherServiceProviderCheckboxPNCM').prop('checked')){
		$('#otherServiceProviderPNCM').show();		
	}   		
	else{
		$('#otherServiceProviderPNCM').hide();
	}
});

// Only allowing numeric values for input fields and convert that to Bangla
$(document).on("keyup", '#pncmotherTemperature', function() {
	$(this).val($(this).val().match(/^\d*\.?\d*$/) ? $(this).val() : "");
});

$(document).on("keyup", '#pncmotherbpSystolic', function() {
	$(this).val($(this).val().replace(/[^\d]/, ''));
	// $(this).val(en_to_ben_number_conversion($('#bpSystolicANC').val()));
});

$(document).on("keyup", '#pncmotherbpDiastolic', function() {
	$(this).val($(this).val().replace(/[^\d]/, ''));
	// $(this).val(en_to_ben_number_conversion($('#bpDiastolicANC').val()));
});

$(document).on("keyup", '#pncmotherHemoglobin', function() {
	$(this).val($(this).val().replace(/[^\d]/, ''));
});

$(document).on("click", '#pncmotherRefer',function (){
	handleOnclickCheckbox("#pncmotherRefer","#pncmotherReferCenterName", "#pncmotherReferReason","enable","");	
});