	//saving delivery information
	function insertUpdateDeliveryInfo(callFrom){
			
		if (callFrom=='service') 		
		{		
			if($('#deliveryType').val()!="0" && $('#deliveryPlace').val()!="0" && $('#deliveryDate').val()!="")
			{
				var deliveryObj = new Object();
				deliveryObj = setCommonParam();
				
				deliveryObj.healthid = $('#healthid').text();
				deliveryObj.pregno = $('#pregNumber').text();
				deliveryObj.providerid=$('#providerid').text();
				deliveryObj.dPlace = $('#deliveryPlace').val();
				deliveryObj.dDate = $('#deliveryDate').val();
				
				var dTimeHt = $('#deliveryTimeHour').val();
				var dTimeMt = $('#deliveryTimeMinute').val();
				var dTimeAMPM = $('#ampm').val();
				
				var dTimeH = dTimeHt.length==1 ? "0" + dTimeHt : dTimeHt;
				var dTimeM = dTimeMt.length==1 ? "0" + dTimeMt : dTimeMt;
						
				deliveryObj.dTime = dTimeH+":"+dTimeM+" "+dTimeAMPM;
				deliveryObj.dType = $('#deliveryType').val();
				if(deliveryObj.dPlace =="1"){
					deliveryObj.dCenterName = "";
					deliveryObj.dAdmissionDate = "";
					deliveryObj.dWard = "";
					deliveryObj.dBed = "";
				}
				else{
					deliveryObj.dCenterName = $('#deliveryPlaceCenter').val();
					deliveryObj.dAdmissionDate = $('#admissionDate').val();
					deliveryObj.dWard = $('#wardN').val();
					deliveryObj.dBed = $('#bedN').val();
				}
				deliveryObj.dNoLiveBirth = $('#liveBirth').val();
				deliveryObj.dStillFresh = ($('#stillBirthFresh').val()==""?"0":$('#stillBirthFresh').val());
				deliveryObj.dStillMacerated = ($('#stillBirthMacerated').val()==""?"0":$('#stillBirthMacerated').val());
				deliveryObj.dNoStillBirth = parseInt(deliveryObj.dStillFresh) + parseInt(deliveryObj.dStillMacerated);
				deliveryObj.dNewBornBoy = $('#newbornBoy').val();
				deliveryObj.dNewBornGirl = $('#newbornGirl').val();		
				deliveryObj.dNewBornUnidentified = $('#newbornUnidentified').val();
				
				deliveryObj.dOxytocin = $('#applyOxytocin').prop('checked')? 1 : 2;
				deliveryObj.dTraction = $('#applyTraction').prop('checked')? 1 : 2;
				deliveryObj.dUMassage = $('#uterusMassage').prop('checked')? 1 : 2;
				deliveryObj.dEpisiotomy = ($('input[name=episiotomy]:checked').val()==null?"":$('input[name=episiotomy]:checked').val());
				deliveryObj.dMisoprostol = ($('input[name=misoprostol]:checked').val()==null?"":$('input[name=misoprostol]:checked').val());
				
				deliveryObj.dAttendantThisProvider = $('#deliveryDoneByProvider').prop('checked')? 1 : 2;
				deliveryObj.dAttendantName = $('#deliveryDoneByPerson').val();
				deliveryObj.dAttendantDesignation = $('#deliveryDoneByDesignation').val();
				
				deliveryObj.dBloodLoss = $('#excessBloodLoss').prop('checked')? 1 : 2;
				deliveryObj.dLateDelivery = $('#lateDelivery').prop('checked')? 1 : 2;
				deliveryObj.dBlockedDelivery = $('#blockedDelivery').prop('checked')? 1 : 2;
				deliveryObj.dPlacenta = $('#placenta').prop('checked')? 1 : 2;
				deliveryObj.dHeadache = $('#headache').prop('checked')? 1 : 2;
				deliveryObj.dBVision = $('#blurryVision').prop('checked')? 1 : 2;
				deliveryObj.dOBodyPart = $('#otherBodyPart').prop('checked')? 1 : 2;
				deliveryObj.dConvulsions = $('#convulsions').prop('checked')? 1 : 2;
				deliveryObj.dOthers = $('#others').prop('checked')? 1 : 2;
				deliveryObj.dOtherReason = $('#otherReason').val();
				
				deliveryObj.dTreatment = $('#dTreatment').multipleSelect("getSelects");
				deliveryObj.dAdvice = $('#dAdvice').multipleSelect("getSelects");
				deliveryObj.dRefer = $('#deliveryRefer').prop('checked')? 1 : 2;
				deliveryObj.dReferCenter = $('#dReferCenter').val();
				deliveryObj.dReferReason = $('#dReferReason').multipleSelect("getSelects");
				
				deliveryObj.sateliteCenterName = "";
				deliveryObj.client = "1";
				deliveryObj.deliveryLoad = "";
				//deliveryInfo(deliveryObj);
				
				
				var userVal = "<table class='confirmation'>";
		        userVal+= "<tr><td>প্রসবের স্থান: "+(deliveryObj.dPlace=="0" ? "" :(deliveryObj.dPlace=="1" ? "বাড়ি" : "সেবাকেন্দ্র"))+"</td>";
		        
		        userVal+= "</td>";
		        userVal+= "<td>প্রসবের তারিখ: " + moment(deliveryObj.dDate).format("DD MMM YYYY")+ "</td>";     
		        userVal+="<td>"+linemark_switch("সময়",dTimeHt+dTimeMt,'');
		        userVal+=deliveryObj.dTime+"</td>";
				
		        userVal+="<td>"+linemark_switch("প্রসবের ধরণ",deliveryObj.dType,"0");
				
				switch(deliveryObj.dType)
				{
					case '0': userVal+="";break;
					case '1': userVal+="স্বাভাবিক";break; 
					case '2': userVal+="সিজারিয়ান";break;
					//case '3': userVal+="গর্ভপাত";break; 
					default: break;
					}
				
				userVal+="</td></tr></table>";
				
				if(deliveryObj.dPlace!="1")
				     {
					 userVal+= "<table class='confirmation'><tr><td colspan='2'>সেবাকেন্দ্রের নাম:  ";
					 
					 referCenterJS = JSON.parse(referCenter);
					 userVal+=referCenterJS[0][deliveryObj.dCenterName]+"</td>";
					 
					 if($('#csba').text()!='1'){ 
						if(deliveryObj.dCenterName!="5" && deliveryObj.dCenterName!="6"){
					        userVal+= "<td>আগমনের তারিখ:"+moment(deliveryObj.dAdmissionDate).format("DD MMM YYYY")+"</td>"; 
							userVal+= "<td>"+linemark("ওয়ার্ড",deliveryObj.dWard)+"&nbsp&nbsp"+linemark("শয্যা",deliveryObj.dBed)+"</td></table>";
						}				
				     }
					 userVal+="</tr></table>";
				     }
			       if(deliveryObj.dType!="3"){  	
						
				    	userVal+= "<table class='list'><tr><td colspan='2'>প্রসবের তৃতীয় ধাপের সক্রিয় ব্যবস্থাপনায় (AMTSL) যা অনুসরণ করা হয়েছে:<td></tr>" +(deliveryObj.dOxytocin=="1" ? "<tr><td>-প্রসবের ১ মিনিটের মধ্যে অক্সিটোসিন প্রয়োগ</td></tr>" : "")+(deliveryObj.dTraction=="1" ? "<tr><td>-গর্ভফুল প্রসবের জন্য নাভিরজ্জুতে নিয়ন্ত্রিত টান</td></tr>" : "")+(deliveryObj.dUMassage=="1" ? "<tr><td>-জরায়ু ম্যাসাজ </td></tr>" : "");
				        if($('#csba').text()!='1'){
				        	userVal+= "<tr></tr><tr><td>ইপিসিওটমি করা হয়েছে? "+(deliveryObj.dEpisiotomy=="1" ? "&nbspহ্যাঁ" : "&nbspনা")+"</td>";
				        }
				        else{
				        	deliveryObj.dEpisiotomy = "2";
				        }
						
				    	userVal+= "<td>অক্সিটোসিন না থাকলে মিসোপ্রোস্টল খাওয়ানো হয়েছে?"+(deliveryObj.dMisoprostol=="1" ? "&nbsp হ্যাঁ" : " &nbspনা")+"</td></tr>";
				        }
			       
			       if($('#csba').text()!='1'){
			    	   
			    	   userVal+= "<tr><td>"+linemark("প্রসব সম্পাদনকারীর নাম",deliveryObj.dAttendantName)+"</td>";
			    	   deliveryAttendantJS = JSON.parse(deliveryAttendant);
			    	   userVal+= "<td><span style=\"color: black;\">"+"পদবী: </span>"+deliveryAttendantJS[0][deliveryObj.dAttendantDesignation]+"</td></tr></table>";
			       }
			       else{
			    	   deliveryObj.dAttendantName = "";
			    	   deliveryObj.dAttendantDesignation = "0";
			       }
					userVal+= "<table id='list'><tr><td>প্রসব সংক্রান্ত জটিলতা: </td></tr><tr>" +(deliveryObj.dBloodLoss=="1" ? "<tr><td>-অতিরিক্ত রক্তক্ষরণ</td></tr>" : "")+(deliveryObj.dLateDelivery=="1" ? "<tr><td>-বিলম্বিত প্রসব</td></tr>" : "")+(deliveryObj.dBlockedDelivery=="1" ? "<tr><td>-বাধাগ্রস্থ প্রসব </td></tr>" : "")+(deliveryObj.dPlacenta=="1" ? "<tr><td>-গর্ভফুল না পড়া</td></tr>" : "")+(deliveryObj.dHeadache=="1" ? "<tr><td>-প্রচন্ড মাথা ব্যাথা</td></tr>" : "")+(deliveryObj.dBVision=="1" ? "<tr><td>-চোখে ঝাপসা দেখা </td></tr>" : "")+(deliveryObj.dOBodyPart=="1" ? "<tr><td>-মাথা ছাড়া অন্য কোন অঙ্গ বের হওয়া </td></tr>" : "")+(deliveryObj.dConvulsions=="1" ? "<tr><td>-খিচুঁনি</td></tr>" : "")+(deliveryObj.dOthers=="1" ? "<tr><td>-অন্যান্য :"+deliveryObj.dOtherReason+"</td></tr>" : "");
			        userVal+= "<tr><td>চিকিৎসা:</td></tr>";
				    textVal=deliveryObj.dTreatment;
					treatmentJS = JSON.parse(treatment);
					var i;
					var convertedText = "";
					for(i =0; i < textVal.length; i++){
						convertedText = convertedText + "<tr><td>-"+treatmentJS[0][textVal[i]]+"</td></tr>";
					}
					userVal+=convertedText;
					
				    userVal+= "<tr><td>পরামর্শ:</td></tr>";
				    textVal=deliveryObj.dAdvice;
				    adviceDeliveryJS = JSON.parse(adviceDelivery);
				    var i;
					var convertedText = "";
				
					for(i =0; i < textVal.length; i++){
						convertedText = convertedText + "<tr><td>-"+adviceDeliveryJS[0][textVal[i]] + "</td></tr>";
					}
					userVal+=convertedText;
			   
					userVal = userVal + "<tr><td>রেফার: "+(deliveryObj.dRefer=="1" ? "হ্যাঁ" : "না")+"</td></tr>";
			    
					if(deliveryObj.dRefer=="1"){
						referCenterJS = JSON.parse(referCenter);
						userVal = userVal + linemark_switch("<tr><td>কেন্দ্রের নাম",deliveryObj.dReferCenter,0); 
						userVal+=referCenterJS[0][deliveryObj.dReferCenter]+"</td></tr>";				
				    userVal+= "<tr><td>"+linemark_switch("কারণ",deliveryObj.dReferReason,0)+"</td></tr>"; 
					textVal=deliveryObj.dReferReason;
					referReasonDeliveryJS = JSON.parse(referReasonDelivery);
					var i;
					var convertedText = "";
					
					for(i =0; i < textVal.length; i++){
						convertedText = convertedText + "<tr><td>-" + referReasonDeliveryJS[0][textVal[i]] + "</td></tr>";					
					}
				   userVal+=convertedText+"</table>";
				}
				if(moment(deliveryObj.dDate) > moment($('#lmp').val())){
					$('<div></div>').dialog({
			        modal: true,
			        title: "নিশ্চিত করুন",
			        width: 760,
			        hide: {effect: 'fade', duration: 500},
			        show: {effect: 'fade', duration: 600},
			        open: function () {
			            $(this).html(userVal);
			        },
			        buttons: {
						"Save & Open Newborn Information" : function () {
							deliveryInfo(deliveryObj);
							if(Abortion==1){
								$("div#newborn").hide();
							}
							else{
								$("div#newborn").show();
								$("div#newborn").focus();
							}
							Abortion="";
							$(this).dialog("close");
						},
						
						"Save & Serve Next Client": function () {
							deliveryInfo(deliveryObj);
							goToId='.nextC';	
							$(goToId).click();
							$(this).dialog("close");
						},			
						Cancel: function () {
							$(this).dialog("close");
						}
			        }
					});
				}
				else{
					callDialog(dDateLessThanLMP);
				}

			}
			else{
				callDialog(fill);
			}
		}
		
		else if (callFrom=='close') 		
		{		
			if($('#closePlace').val()!="0" && $('#closeDate').val()!="" && ($('#closeDeliveryType').val()!="0" || $('input[name=closeType]:checked').val()=="abortion")){
				//$('input[name=closeType]:checked').val()
				var deliveryObj = new Object();
				deliveryObj = setCommonParam();
				deliveryObj.healthid = $('#healthid').text();
				deliveryObj.pregno = $('#pregNumber').text();
				deliveryObj.providerid=$('#providerid').text();
				deliveryObj.dPlace = $('#closePlace').val();
				deliveryObj.dDate = $('#closeDate').val();
				
				if(deliveryObj.dPlace=="1")
					deliveryObj.dCenterName = "";
				else
					deliveryObj.dCenterName = $('#closeDeliveryPlaceCenter').val();
				
						
				if($('input[name=closeType]:checked').val()!="abortion")
				{	
					deliveryObj.dType = $('#closeDeliveryType').val();
					
					deliveryObj.dNoLiveBirth = $('#closeLiveBirth').val()==""?'0':$('#closeLiveBirth').val();				
					deliveryObj.dNoStillBirth =$('#closeStillBirth').val()==""?'0':$('#closeStillBirth').val();
					deliveryObj.dNewBornBoy = $('#closeNewbornBoy').val()==""?'0':$('#closeNewbornBoy').val();
					deliveryObj.dNewBornGirl = $('#closeNewbornGirl').val()==""?'0':$('#closeNewbornGirl').val();
					
					deliveryObj.dAttendantName = $('#closeDeliveryDoneByPerson').val();
					deliveryObj.dAttendantDesignation = $('#closeDeliveryDoneByDesignation').val();
				}
				
				else
					
				{	
					deliveryObj.dType = '3';
					
					deliveryObj.dNoLiveBirth = '0';				
					deliveryObj.dNoStillBirth = '0';
					deliveryObj.dNewBornBoy = '0';
					deliveryObj.dNewBornGirl = '0';
					
					deliveryObj.dAttendantName = "";
					deliveryObj.dAttendantDesignation = "";
				}
				
				// NOT REQUIRED for this type so blank				
				deliveryObj.dTime = "";
				deliveryObj.dAdmissionDate = "";
				deliveryObj.dWard = "";
				deliveryObj.dBed = "";
				deliveryObj.dStillFresh = '0';
				deliveryObj.dStillMacerated = '0';
				deliveryObj.dNewBornUnidentified = '0';			
				deliveryObj.dOxytocin = '2';
				deliveryObj.dTraction = '2';
				deliveryObj.dUMassage = '2';
				deliveryObj.dEpisiotomy = '2';
				deliveryObj.dMisoprostol = '2';
				deliveryObj.dAttendantThisProvider = '2';
				deliveryObj.dBloodLoss = '2';
				deliveryObj.dLateDelivery = '2';
				deliveryObj.dBlockedDelivery = '2';
				deliveryObj.dPlacenta = '2';
				deliveryObj.dHeadache = '2';
				deliveryObj.dBVision = '2';
				deliveryObj.dOBodyPart = '2';
				deliveryObj.dConvulsions = '2';
				deliveryObj.dOthers ='2';
				deliveryObj.dOtherReason = "";				
				deliveryObj.dTreatment = "";
				deliveryObj.dAdvice = "";
				deliveryObj.dRefer = '2';
				deliveryObj.dReferCenter = "";
				deliveryObj.dReferReason = "";
				deliveryObj.sateliteCenterName = "";
				deliveryObj.client = "1";
				
				//
						
				deliveryObj.deliveryLoad = "";
			
				
				var userVal = "<table class='confirmation'>";
				userVal+= "<tr><td>ধরণ: "+ ($('input[name=closeType]:checked').val()=="abortion" ? "গর্ভপাত" : "প্রস্রব")+"</td></tr>";
		        userVal+= "<tr><td>স্থান: "+ (deliveryObj.dPlace=="0" ? "" :(deliveryObj.dPlace=="1" ? "বাড়ি" : "সেবাকেন্দ্র"))+"</td>";
		        userVal+= "<td>তারিখ: " + moment(deliveryObj.dDate).format("DD MMM YYYY")+ "</td></tr>";
		        
		        if(deliveryObj.dPlace!="1")
			     {
					 userVal+= "<tr><table class='confirmation'><tr><td colspan='2'>সেবাকেন্দ্রের নাম:  ";
					 
					 referCenterJS = JSON.parse(referCenter);
					 userVal+=referCenterJS[0][deliveryObj.dCenterName]+"</td>";
					 
					userVal+="</tr></table></tr>";
			     }
				
		        if($('input[name=closeType]:checked').val()!="abortion")
		        {
			        	
			        userVal+="<tr><table class='confirmation'><tr><td>"+linemark_switch("প্রসবের ধরণ",deliveryObj.dType,"0");
					
					switch(deliveryObj.dType)
					{	
						case '0': userVal+="";break;
						case '1': userVal+="স্বাভাবিক";break; 
						case '2': userVal+="সিজারিয়ান";break;					
						default: break;
					}					
					userVal+="</td></tr>";
					
					userVal+="<tr><td colspan='3'>প্রসবের ফলাফল &nbsp&nbsp জীবিত জন্ম:&nbsp"+deliveryObj.dNoLiveBirth+"&nbsp||&nbsp মৃত জন্ম: &nbsp"+deliveryObj.dNoStillBirth+"&nbsp||&nbsp লিঙ্গ&nbspছেলে:"+deliveryObj.dNewBornBoy+"&nbspমেয়ে:&nbsp"+deliveryObj.dNewBornGirl+"</td></tr>";					
			        
					userVal+= "<tr><td colspan='2'>"+linemark("প্রসব সম্পাদনকারীর নাম",deliveryObj.dAttendantName)+"</td>";
			        deliveryAttendantJS = JSON.parse(deliveryAttendant);
			        userVal+= "<td colspan='2'><span style=\"color: black;\">"+"পদবী: </span>"+deliveryAttendantJS[0][deliveryObj.dAttendantDesignation]+"</td></tr></table></tr>";
					
		        }   
		        
				userVal+="</table>";

				$('#closePreg').hide();
				if(moment(deliveryObj.dDate) > moment($('#lmp').val())){
					if(deliveryObj.dType!=3){
						$('<div></div>').dialog({
					        modal: true,
					        title: "নিশ্চিত করুন",
					        width: 760,
					        hide: {effect: 'fade', duration: 500},
					        show: {effect: 'fade', duration: 600},
					        open: function () {
					            $(this).html(userVal);
					        },
					        buttons: {
								"Save & Add New Pregnancy" : function () {
									deliveryInfo(deliveryObj);
									addNewPreg();
									$(this).dialog("close");	
								},
								
								"Save & Serve Next Client": function () {
									deliveryInfo(deliveryObj);
									goToId='.nextC';	
									$(goToId).click();
									$(this).dialog("close");
								},			
								Cancel: function () {
									resetClosePreg("new");
									$(this).dialog("close");
								}
					        }
							});
					}
					else{
						$('<div></div>').dialog({
					        modal: true,
					        title: "নিশ্চিত করুন",
					        width: 760,
					        hide: {effect: 'fade', duration: 500},
					        show: {effect: 'fade', duration: 600},
					        open: function () {
					            $(this).html(userVal);
					        },
					        buttons: {
								"Save & Proceed" : function () {
									deliveryInfo(deliveryObj);
									openPAC(riskyPreg);
									$(this).dialog("close");	
								},
			
								Cancel: function () {
									resetClosePreg("new");
									$(this).dialog("close");
								}
					        }
							});
					}
				}
				else{
					callDialog(dDateLessThanLMP);
				}				
			}
			else{
				callDialog(fill);
			}
			}	
		
		}	
	
	
	//retrieve delivery Information
	function retrieveDeliveryInfo(){
		
		var deliveryObj = new Object();
		deliveryObj = setCommonParam();
		
		deliveryObj.healthid = $('#healthid').text();
		deliveryObj.pregno = $('#pregNumber').text();
		deliveryObj.deliveryLoad = "retrieve";
		deliveryInfo(deliveryObj);
		
	}

	/**
	 * insert and retrieve Delivery information in a single AJAX call
	*/
	function deliveryInfo(deliveryInfoObj) {
		
		if (deliveryInfoObj.healthid != "" && deliveryInfoObj.pregno != "") {
			$.ajax({
				type: "POST",
	  			url: "delivery",
	  			timeout:60000, //60 seconds timeout
	  			dataType: "json",
	  			data: {"deliveryInfo" : JSON.stringify(deliveryInfoObj)},
	  			success : function(response) {  					
	  				parseDeliveryInfo(response);
	  			},
	  			error : function(xhr, status, error) {
	  				alert(status);
	  				alert(error);
	  			}
	  		});
	  	}				
	}

	//Parsing delivery information and update the html accordingly
	function parseDeliveryInfo(response){
		
		if(response.dNew=='No'){
			document.getElementById('outcomePlace').innerHTML=response.dPlace;
			//$('#outcomePlace').text(response.dPlace);
			document.getElementById('outcomeDate').innerHTML=response.dDate;
			//$('#outcomeDate').text(response.dDate);
			document.getElementById('outcomeTime').innerHTML=response.dTime;
			//$('#outcomeTime').text(response.dTime);
			document.getElementById('outcomeType').innerHTML=response.dType;
			//$('#outcomeType').text(response.dType);
			document.getElementById('immatureBirthInfo').innerHTML=response.immatureBirth;
			//$('#immatureBirthInfo').text(response.immatureBirth);
			document.getElementById('hasDeliveryInfo').innerHTML="Yes";
			
			for(var j=1;j<=5;j++){
				immatureBirthWeek = "immatureBirthWeekSection" + j;
				newbornHighlight = "newbornHighlight" + j;
				if(response.immatureBirth=="1"){
					document.getElementById(immatureBirthWeek).innerHTML= "(অপরিণত জন্ম - " + en_to_ben_number_conversion(response.immatureBirthWeek.toString()) + " সপ্তাহ)";
					document.getElementById(newbornHighlight).style.background = ("#D39C8A");
				}
				else{
					document.getElementById(immatureBirthWeek).innerHTML= "";
					document.getElementById(newbornHighlight).style.background = "none";
				}
			}
			
			$('#deliveryPlace option[value="' + response.dPlace + '"]').prop('selected', true);
			if(response.dPlace=="1"){
				$("#centerRelatedInfoRow").hide();
			}
			else{
				$('#deliveryPlaceCenter option[value="' + response.dCenterName + '"]').prop('selected', true);
				if(response.dCenterName=="5"||response.dCenterName=="6"){
					$("#wardBedSection").hide();
				}
				else{
					$("#wardN").val(response.dWard);
					$("#bedN").val(response.dBed);
					$("#wardBedSection").show();
				}
				
				$("#centerRelatedInfoRow").show();
			}
			
			$('#admissionDate').combodate('setValue', response.dAdmissionDate, true);
			$('#admissionDate').combodate('setValue', response.dAdmissionDate, true);
			
						
			$('#deliveryDate').combodate('setValue', response.dDate, true);
			$('#deliveryDate').combodate('setValue', response.dDate, true);
			
			var deliveryTime = response.dTime;
			if(deliveryTime!=""){
				deliveryTime = deliveryTime.split(" ");
				var deliveryAMPM = deliveryTime[1].toUpperCase();
				
				deliveryTime = deliveryTime[0].split(":");
				
				var deliveryH = deliveryTime[0];
				var deliveryM = deliveryTime[1];
							
				$("#deliveryTimeHour").val(deliveryH);
				$("#deliveryTimeMinute").val(deliveryM);
				$('#ampm option[value="' + deliveryAMPM + '"]').prop('selected', true);
			}
			else{
				$("#deliveryTimeHour").val("");
				$("#deliveryTimeMinute").val("");
				$('#ampm option[value="AM"]').prop('selected', true);
			}
			
			if(response.dType==""){
				$('#deliveryType option[value="0"]').prop('selected', true);				   
			}
			else{
				$('#deliveryType option[value="' + response.dType + '"]').prop('selected', true);
			}
						
			if(response.dAbortion=="1"){
				$('#deliveryType option[value="3"]').prop('selected', true);
				$("#abortion").hide();
				$("#abortion1").hide();
				$("#abortion2").hide();
				$("#abortion3").hide();
				if(response.dPlace=="1"){
					$("#abortionC").hide();
				}
				else{
					$("#abortionC").show();
				}
				//$('input[name=deliveryType][value="3"]').prop("checked", true);
			}
			
			$("#liveBirth").val(en_to_ben_number_conversion(response.dNoLiveBirth.toString()));
			$("#stillBirth").text(en_to_ben_number_conversion((response.dNoStillBirth==0?"":response.dNoStillBirth).toString()));
			$("#stillBirthFresh").val(en_to_ben_number_conversion((response.dStillFresh==0?"":response.dStillFresh).toString()));
			$("#stillBirthMacerated").val(en_to_ben_number_conversion((response.dStillMacerated==0?"":response.dStillMacerated).toString()));
						
			$("#newbornBoy").val(en_to_ben_number_conversion(response.dNewBornBoy.toString()));
			$("#newbornGirl").val(en_to_ben_number_conversion(response.dNewBornGirl.toString()));
			$("#newbornUnidentified").val(en_to_ben_number_conversion(response.dNewBornUnidentified.toString()));
			
			if(response.dOxytocin=="1"){
				$( "#applyOxytocin" ).prop( "checked", true );			
			}
			else{
				$( "#applyOxytocin" ).prop( "checked", false );
			}
			
			if(response.dTraction=="1"){
				$( "#applyTraction" ).prop( "checked", true );				
			}
			else{
				$( "#applyTraction" ).prop( "checked", false );
			}
			
			if(response.dUMassage=="1"){
				$( "#uterusMassage" ).prop( "checked", true );				
			}
			else{
				$( "#uterusMassage" ).prop( "checked", false );
			}
			
			if(response.dEpisiotomy=="1"){
				   $('input[name=episiotomy][value="1"]').prop("checked", true);
			}
			else if(response.dEpisiotomy=="2"){
				$('input[name=episiotomy][value="2"]').prop("checked", true);				   
			}
			else{
				$('input[name=episiotomy][value="1"]').prop("checked", false);
				$('input[name=episiotomy][value="2"]').prop("checked", false);
			}
			
			if(response.dMisoprostol=="1"){
				   $('input[name=misoprostol][value="1"]').prop("checked", true);
			}
			else if(response.dMisoprostol=="2"){
				$('input[name=misoprostol][value="2"]').prop("checked", true);				   
			}
			else{
				$('input[name=misoprostol][value="1"]').prop("checked", false);
				$('input[name=misoprostol][value="2"]').prop("checked", false);
			}
			
			if(response.dAttendantThisProvider=="1"){
				if($('#providerName').text().toUpperCase()==response.dAttendantName.toUpperCase()){
					$('#deliveryDoneByProvider').prop( "checked", true );
				}
				else{
					$('#deliveryDoneByProvider').prop( "checked", false );
				}				
			}
			else{
				$('#deliveryDoneByProvider').prop( "checked", false );
			}
			
			$('#deliveryDoneByPerson').val(response.dAttendantName);		
			$('#deliveryDoneByDesignation option[value= "' + response.dAttendantDesignation + '"]').prop('selected', true);
			//$('#deliveryDoneByDesignation').val(response.dAttendantDesignation);
						
			if(response.dBloodLoss=="1"){
				$('#excessBloodLoss').prop( "checked", true );			
			}
			else{
				$('#excessBloodLoss').prop( "checked", false );
			}
			if(response.dLateDelivery=="1"){
				$('#lateDelivery').prop( "checked", true );				
			}
			else{
				$('#lateDelivery').prop( "checked", false );
			}
			if(response.dBlockedDelivery=="1"){
				$('#blockedDelivery').prop( "checked", true );				
			}
			else{
				$('#blockedDelivery').prop( "checked", false );
			}
			if(response.dPlacenta=="1"){
				$('#placenta').prop( "checked", true );				
			}
			else{
				$('#placenta').prop( "checked", false );
			}
			if(response.dHeadache=="1"){
				$('#headache').prop( "checked", true );
			}
			else{
				$('#headache').prop( "checked", false );
			}
			if(response.dBlurryVision=="1"){
				$('#blurryVision').prop( "checked", true );
			}
			else{
				$('#blurryVision').prop( "checked", false );
			}
			if(response.dOtherPartofBody=="1"){
				$('#otherBodyPart').prop( "checked", true );				
			}
			else{
				$('#otherBodyPart').prop( "checked", false );
			}
			if(response.dconvulsion=="1"){
				$('#convulsions').prop( "checked", true );				
			}
			else{
				$('#convulsions').prop( "checked", false );
			}
			if(response.dOthers=="1"){
				$('#others').prop( "checked", true );				
				$('#otherReason').val(response.dOthersReason);
				$('#otherReason').show();
			}
			else{
				$('#others').prop( "checked", false );
				$('#otherReason').val("");
				$('#otherReason').hide();
			}
			
			var treatment = response.dTreatment;
			treatment = treatment.slice(1,-1);
			treatment = treatment.split(",");
			
			var dTreatment = "";
			for(i=0;i<treatment.length;i++){
				dTreatment = dTreatment + treatment[i].slice(1,-1) + ",";
			}
			
			dTreatment = dTreatment.slice(0,-1);
			dTreatment = dTreatment.split(",");
			$('#dTreatment').multipleSelect("uncheckAll");
			$('#dTreatment').multipleSelect("setSelects", dTreatment);
						
			var advice = response.dAdvice;
			advice = advice.slice(1,-1);
			advice = advice.split(",");
			
			var dAdvice = "";
			for(i=0;i<advice.length;i++){
				dAdvice = dAdvice + advice[i].slice(1,-1) + ",";
			}
			
			dAdvice = dAdvice.slice(0,-1);
			dAdvice = dAdvice.split(",");
			$('#dAdvice').multipleSelect("uncheckAll");
			$('#dAdvice').multipleSelect("setSelects", dAdvice);
						
			if(response.dRefer=="1"){
				$('#deliveryRefer').prop("checked", true );
				$('#dReferCenter option[value="' + response.dReferCenter + '"]').prop('selected', true);
				
				var referReason = response.dReferReason;
				referReason = referReason.slice(1,-1);
				referReason = referReason.split(",");
				
				var dReferReason = "";
				for(i=0;i<referReason.length;i++){
					dReferReason = dReferReason + referReason[i].slice(1,-1) + ",";
				}
				dReferReason = dReferReason.slice(0,-1);
				dReferReason = dReferReason.split(",");
				$('#dReferReason').multipleSelect("uncheckAll");
				$('#dReferReason').multipleSelect("setSelects", dReferReason);
				$('#dReferRest').show();				
			}
			else{
				$('#deliveryRefer').prop("checked", false );
				$('#dReferCenter option[value="0"]').prop('selected', true);
				$('#dReferReason').multipleSelect("uncheckAll");
				$('#dReferRest').hide();				
			}
			
			$('#dButtonSaveColumn').hide();
			$('#dButtonUpdateColumn').hide();
			$('#dButtonEditColumn').show();
			$('#deliveryInfo').find('*').prop('disabled',true);
			$('#editButtonDelivery').prop('disabled',false);
			
			Abortion=response.dAbortion;
			
			if(response.dAbortion==""){	// part pac non-eligible women											
				   toggleServices("pacDisable");					
				}
			 
			   else {	// part pac eligible women											
				   toggleServices("pacEnable");					
				}
			 
			
			/*  Service Guide replaced this
			if(response.dAbortion==1){
				$("div#newborn").hide();
			}
			else{
				$("div#newborn").show();
			}*/
			
		}
		else if(response.dNew=='Yes'){
			
			$('#outcomePlace').text("");
			$('#outcomeDate').text("");
			$('#outcomeTime').text("");
			$('#outcomeType').text("");
			$('#immatureBirthInfo').text("");
			
			//enabling all the fields for delivery div
			$('#deliveryInfo').find('*').prop('disabled',false);
			
			//clearing the fields' value
			$('#deliveryPlace option[value=2]').prop('selected', true);
			if(deliveryDate==""){
				$('#deliveryDate').combodate('setValue',moment(/*$('#edd').val()*/).format("YYYY-MM-DD"),true);
			}
			else{
				$('#deliveryDate').combodate('setValue',moment(deliveryDate).format("YYYY-MM-DD"),true);
				deliveryDate = "";
			}
			$('#deliveryType option[value=0]').prop('selected', true);
			if($('#csba').text()=='1'){
				$('#deliveryPlaceCenter option[value=6]').prop('selected', true);
			}
			else{
				$('#deliveryPlaceCenter option[value=5]').prop('selected', true);
			}
			$('#deliveryTimeHour').val("");
			$('#deliveryTimeMinute').val("");
			
			$('#admissionDate').combodate('setValue','',true);
						
			$('#wardN').val("");
			$('#bedN').val("");
			
			
			$('#liveBirth').val("");
			$('#liveBirth').prop('disabled', true);
			$('#stillBirth').html("");
			$('#stillBirthFresh').val("");
			$('#stillBirthFresh').prop('disabled', true);
			$('#stillBirthMacerated').val("");
			$('#stillBirthMacerated').prop('disabled', true);
						
			$('#newbornBoy').val("");
			$('#newbornBoy').prop('disabled', true);
			$('#newbornGirl').val("");
			$('#newbornGirl').prop('disabled', true);
			$('#newbornUnidentified').val("");
			$('#newbornUnidentified').prop('disabled', true);
			
			$('#applyOxytocin').prop( "checked", false );
			$('#applyTraction').prop( "checked", false );
			$('#uterusMassage').prop( "checked", false );
			
			$('input[name=episiotomy][value="1"]').prop("checked", false);
			$('input[name=episiotomy][value="2"]').prop("checked", false);
			$('input[name=misoprostol][value="1"]').prop("checked", false);
			$('input[name=misoprostol][value="2"]').prop("checked", false);
			
			$('#deliveryDoneByProvider').prop( "checked", false );
			$('#deliveryDoneByPerson').val("");
			$('#deliveryDoneByDesignation option[value= "3"]').prop('selected', true);
			//$('#deliveryDoneByDesignation').val("");
			
			$('#excessBloodLoss').prop( "checked", false );
			$('#lateDelivery').prop( "checked", false );
			$('#blockedDelivery').prop( "checked", false );
			$('#placenta').prop( "checked", false );
			$('#headache').prop( "checked", false );
			$('#blurryVision').prop( "checked", false );
			$('#otherBodyPart').prop( "checked", false );
			$('#convulsions').prop( "checked", false );
			$('#others').prop( "checked", false );
			$('#dTreatment').val("");
			$('#dAdvice').multipleSelect("uncheckAll");
			$('#deliveryRefer').prop( "checked", false );	
			$('#dReferCenter option[value="0"]').prop('selected', true);
			$('#dReferReason').multipleSelect("uncheckAll");
			
			
			$('#centerRelatedInfoRow').show();			
			$('#wardBedSection').hide();
			$('#otherReason').hide();
			$('#dReferRest').hide();			
			$('#dButtonSaveColumn').show();
			$('#dButtonEditColumn').hide();
			$('#dButtonUpdateColumn').hide();
		}		
	}
	
	
	//handle client side activities
	
	//Only allowing numeric values for time input fields
	
	$(document).on("keyup", '#deliveryTimeHour',function (){
		if(!$(this).val().match(/^((0?[1-9])|[1][0-2])$/)){
			$(this).val("");
		}		
	});
	
	$(document).on("keyup", '#deliveryTimeMinute',function (){
		if(!$(this).val().match(/^([0-5]?[0-9])$/)){
			$(this).val("");
		}		
	});
	
	//service center dropdown
	$(document).on("change", '#deliveryPlace',function (){
		
		if($(this).find("option:selected").attr('value')=="2"){
			$('#deliveryType option[value="2"]').show();
			$('#deliveryPlaceCenter option[value=5]').prop('selected', true);
			$("#centerRelatedInfoRow").show();
			$("#epsiotomySpan").show();
			if($('#deliveryType').find("option:selected").attr('value')=="3"){
				$("#abortion").hide();
				$("#abortion1").hide();
				$("#abortion2").hide();
				$("#abortion3").hide();
				$("#abortionC").show();
				
			}
			if($('#deliveryPlaceCenter').val()=="5"||$('#deliveryPlaceCenter').val()=="6"){
				$("#wardBedSection").hide();
			}
			else{
				$("#wardBedSection").show();
			}
		} 
		else{
			$('#deliveryType option[value="2"]').hide();
			$('#deliveryPlaceCenter option[value=0]').prop('selected', true);
			$("#centerRelatedInfoRow").hide();
			$("#epsiotomySpan").hide();			
			if($('#deliveryType').find("option:selected").attr('value')=="3"){
				$("#abortion").hide();
				$("#abortion1").hide();
				$("#abortion2").hide();
				$("#abortion3").hide();
				$("#abortionC").hide();
			}
		}	
	});
	
	//service center name dropdown
	$(document).on("change", '#deliveryPlaceCenter',function (){
		
		if($('#deliveryPlaceCenter').val()=="5" || $('#deliveryPlaceCenter').val()=="6"){
			$("#wardBedSection").hide();
		}
		else{
			$("#wardBedSection").show();
		}	
	});
	
	//delivery type dropdown
	$(document).on("change", '#deliveryType',function (){
		/*
		if($('#deliveryType').val()=="3"){
			$("#abortion").hide();
			$("#abortion1").hide();
			$("#abortion2").hide();
			$("#abortion3").hide();
			if($('#deliveryPlace').find("option:selected").attr('value')=="1"){
				$("#abortionC").hide();
			}
			else{
				$("#abortionC").show();
			}
		}
		else{*/
			$("#abortion").show();
			$("#abortion1").show();
			$("#abortion2").show();
			$("#abortion3").show();
			$("#abortionC").show();
		//}
	});
	
	//other checkbox
	$(document).on("click", '#others',function (){
		
		if ($('#others').is(":checked")){
			
			$('#otherReason').show();
			
		}
		else{
			$('#otherReason').val("");
			$('#otherReason').hide();
		}	
		
	});
	
	$(document).on("click", '#deliveryDoneByProvider',function (){
		if ($('#deliveryDoneByProvider').is(":checked")){
			
			$('#deliveryDoneByPerson').val($('#providerName').text());
			$('#deliveryDoneByDesignation option[value=3]').prop('selected', true);
			$('#deliveryDoneByPerson').prop('disabled', true);
			$('#deliveryDoneByDesignation').prop('disabled', true);
		}
		else{
			$('#deliveryDoneByPerson').val("");
			$('#deliveryDoneByDesignation option[value=0]').prop('selected', true);
			$('#deliveryDoneByPerson').prop('disabled', false);
			$('#deliveryDoneByDesignation').prop('disabled', false);
		}
	});
	
	//refer checkbox
	$(document).on("click", '#deliveryRefer',function (){
		
		handleOnclickCheckbox("#deliveryRefer","#dReferCenter", "#dReferReason","show","#dReferRest");
		
	});
	
	/*
	$(document).on("click", 'input[name=outcomeProcessSelection]',function (){

		if($(this).val()=="1"){
			$("#outcomeProcess").show();
		}
		else{
			$("#outcomeProcess").show();
			$("#abortion").hide();
			$("#abortion1").hide();
			$("#abortion2").hide();
			$("#abortion3").hide();
			if($('#deliveryPlace').find("option:selected").attr('value')=="1"){
				$("#abortionC").hide();
			}
			else{
				$("#abortionC").show();
			}			
		}	
	});*/
/*	
	// retrieve delivery information by expand the div and collapse
	$(document).on("click", '#subheadingDeliverySlider',function() {
		retrieveDeliveryInfo();
		retrieveNewbornInfo();
		$('div#delivery').slideToggle(100,function() {
			$('div#newborn').toggle();
			$('#subheadingDeliverySliderSpan').text(function() {
				// change text based on condition
				return $('div#delivery').is(":visible") ? "প্রসব সংক্রান্ত তথ্য (-)" : "প্রসব সংক্রান্ত তথ্য (+)";
			});
		});			
	});	
*/