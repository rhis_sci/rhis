function deleteNewbornInfo(id){
	
	var newbornObj = new Object();
	newbornObj = setCommonParam();
	
	var birthStatus = "#birthStatus"+id;
	var gender = "newBornSex" + id;
	
	newbornObj.healthid = $('#healthid').text();
	newbornObj.pregno = $('#pregNumber').text();
	newbornObj.childno = id;	
	
	newbornObj.birthStatus = $(birthStatus).text();		//data required to update delivery Info
	newbornObj.gender = ($('input[name='+gender+']:checked').val()==null?"":$('input[name='+gender+']:checked').val()); //data required to update delivery Info 
	
	newbornObj.newbornLoad = "delete";
		
    newbornInfo(newbornObj);
	
}

function insertNewbornInfo(divIdName){
	
	id = divIdName.split("_")[1];
	var gender = "newBornSex" + id;
	if($('input[name='+gender+']:checked').val()!=null){
	var newbornObj = new Object();
	newbornObj = setCommonParam();
	
	newbornObj.healthid = $('#healthid').text();
	newbornObj.pregno = $('#pregNumber').text();
	newbornObj.providerid = $('#providerid').text();
	newbornObj.outcomeplace = $('#outcomePlace').text();
	newbornObj.outcomedate = $('#outcomeDate').text();
	newbornObj.outcometime = $('#outcomeTime').text();
	newbornObj.outcometype = $('#outcomeType').text();
	newbornObj.immature = $('#immatureBirthInfo').text();
	var birthStatus = "#birthStatus"+id;
	newbornObj.birthStatus = $(birthStatus).text();
	
	newbornObj.gender = ($('input[name='+gender+']:checked').val()==null?"":$('input[name='+gender+']:checked').val());
	var weight = "#birthWeight" + id;
	newbornObj.weight = $(weight).val();
	/*
	 * no need to provide the opportunity to put immature information to provider
	 */
	//var immature = "immatureBirth" + id;
	//newbornObj.immature = ($('input[name='+immature+']:checked').val()==null?"":$('input[name='+immature+']:checked').val());
	var washAfterBirth = "washAfterBirth" + id;
	newbornObj.dryingAfterBirth = ($('input[name='+washAfterBirth+']:checked').val()==null?"":$('input[name='+washAfterBirth+']:checked').val());
	var resassitation = "resassitation" + id;
	newbornObj.resassitation = ($('input[name='+resassitation+']:checked').val()==null?"":$('input[name='+resassitation+']:checked').val());
	var stimulation = "#stimulation" + id;
	newbornObj.stimulation = $(stimulation).prop('checked')? 1 : 2;
	var bagNMask = "#bagNMask" + id;
	newbornObj.bagNMask = $(bagNMask).prop('checked')? 1 : 2;
	var chlorehexidin = "chlorehexidin" + id;
	newbornObj.chlorehexidin = ($('input[name='+chlorehexidin+']:checked').val()==null?"":$('input[name='+chlorehexidin+']:checked').val());
	var skinTouch = "skinTouch" + id;
	newbornObj.skinTouch = ($('input[name='+skinTouch+']:checked').val()==null?"":$('input[name='+skinTouch+']:checked').val());
	var breastfeed = "breastfeed" + id;
	newbornObj.breastFeed = ($('input[name='+breastfeed+']:checked').val()==null?"":$('input[name='+breastfeed+']:checked').val());
	var newBornRefer = "#newBornRefer" + id;
	newbornObj.refer = $(newBornRefer).prop('checked')? 1 : 2;
	var newBornReferCenter = "#newBornReferCenter" + id;
	newbornObj.referCenterName = $(newBornReferCenter).val();
	var newBornReferReason = "#newBornReferReason" + id;
	newbornObj.referReason = $(newBornReferReason).multipleSelect("getSelects");
	newbornObj.sateliteCenterName = "";
	newbornObj.client = "1";
	newbornObj.newbornLoad = "";
 

	var userVal="";
	userVal = userVal + linemark_switch("লিঙ্গ",newbornObj.gender,'');
		switch(newbornObj.gender){
			case '1':userVal +="ছেলে";break;
			case '2':userVal +="মেয়ে";break;
			case '3':userVal +="সনাক্ত করা যাই নি";break;			
		}
	userVal = userVal +	"<br>";
	if(newbornObj.birthStatus==1){userVal = userVal + linemark("জন্ম ওজন (কেজি)",newbornObj.weight)+"<br>";}
	//userVal = userVal +( newbornObj.immature=="" ? "<span style=\"color: red;\">" : "<span style=\"color: black;\">") + "অপরিণত জন্ম(৩৭ সপ্তাহ পূর্ণ হওয়ার আগে): </span><strong>"+ (newbornObj.immature=="" ? "" :(newbornObj.immature=="1" ? "হ্যাঁ" : "না"))+"</strong><br>";
	if(newbornObj.birthStatus!=3){userVal = userVal +( newbornObj.dryingAfterBirth=="" ? "<span style=\"color: red;\">" : "<span style=\"color: black;\">") + "১ মিনিটের মধ্যে মোছানো হয়েছে:  </span>"+ (newbornObj.dryingAfterBirth=="" ? "" :(newbornObj.dryingAfterBirth=="1" ? "হ্যাঁ" : "না"))+"<br>";}
	
	if(newbornObj.birthStatus!=3){
		userVal = userVal +(newbornObj.resassitation=="" ? "<span style=\"color: red;\">" : "<span style=\"color: black;\">")+"রিসাসসিটেশন: </span>"+(newbornObj.resassitation=="" ? "" :(newbornObj.resassitation=="1" ? "হ্যাঁ" : "না"))+"<br>";
	    if(newbornObj.stimulation==1) userVal+="স্টিমুলেশন<br>";     
		if(newbornObj.bagNMask==1) userVal+="ব্যাগ ও মাস্ক ব্যবহার <br>";
	}	
	
	if(newbornObj.birthStatus==1)
	{
		userVal = userVal +(newbornObj.chlorehexidin=="" ? "<span style=\"color: red;\">" : "<span style=\"color: black;\">")+ "নাড়ি কাটার পর নাড়িতে ৭.১% ক্লোরহেক্সিডিন দ্রবন ব্যবহার করা হয়েছে: </span>"+(newbornObj.chlorehexidin=="" ? "" :(newbornObj.chlorehexidin=="1" ? "হ্যাঁ" : "না"))+"<br>";
		userVal = userVal +(newbornObj.skinTouch=="" ? "<span style=\"color: red;\">" : "<span style=\"color: black;\">")+ "নাড়ি কাটার পর মায়ের ত্বকে নবজাতককে লাগানো হয়েছে: </span>"+(newbornObj.skinTouch=="" ? "" :(newbornObj.skinTouch=="1" ? "হ্যাঁ" : "না"))+"<br>";
		userVal = userVal +(newbornObj.breastFeed=="" ? "<span style=\"color: red;\">" : "<span style=\"color: black;\">")+ "জন্মের ১ ঘন্টার মধ্যে বুকের দুধ খাওয়ানো হয়েছে: </span>"+(newbornObj.breastFeed=="" ? "" :(newbornObj.breastFeed=="1" ? "হ্যাঁ" : "না"))+"<br>";
	    userVal = userVal +(newbornObj.refer=="" ? "<span style=\"color: red;\">" : "<span style=\"color: black;\">")+ "রেফার: </span>"+(newbornObj.refer=="" ? "" :(newbornObj.refer=="1" ? "হ্যাঁ" : "না"))+"<br>";
	    
		    if(newbornObj.refer=="1"){
		    userVal = userVal +linemark_switch("কেন্দ্রের নাম",newbornObj.referCenterName,0);
		    
		    referCenterJS = JSON.parse(referCenter);
		    userVal+=referCenterJS[0][newbornObj.referCenterName];
		    
		    userVal+="<br>"; 
		    
		    userVal = userVal + linemark_switch("কারণ",newbornObj.referCenterName,0) +"<br>";		    
		    textVal=newbornObj.referReason;
		    referReasonNewbornJS = JSON.parse(referReasonNewborn);
			var i;
			var convertedText = " ";
			
			for(i =0; i < textVal.length; i++){
				convertedText = convertedText + "-"+referReasonNewbornJS[0][textVal[i]] + "<br>";
			}
			userVal+=convertedText;		
	    }
	}
	
	$('<div></div>').dialog({
        modal: true,
        width: 600,
        title: "নিশ্চিত করুন",        
        open: function () {
            $(this).html(userVal);
        },
        buttons: {
        	"Save & Add next Newborn": function () {
        		var divName="#newBornInfo_"+id;
        		$( "#addNewBorn" ).clone().insertAfter( divName );
        		newbornInfo(newbornObj);
        		$(divIdName).find('*').prop('disabled', true);
        		var divButtonName="#newbornSave"+id;
        		$( divButtonName ).hide();
        		$( "#addNewBorn" ).remove();
        		retrieveDeliveryInfo();
        		retrieveNewbornInfo();
                $(this).dialog("close");     	 
            },
			"Save & Open PNC Information": function () {
				var divName="#newBornInfo_"+id;
        		$( "#addNewBorn" ).clone().insertAfter( divName );
        		newbornInfo(newbornObj);
        		$(divIdName).find('*').prop('disabled', true);
        		var divButtonName="#newbornSave"+id;
        		$( divButtonName ).hide();
        		$( "#addNewBorn" ).remove();
        		//retrieveDeliveryInfo();
        		//retrieveNewbornInfo();       	
			    goToId='#3.serviceTabButton';	
			    $(goToId).click();
				$('div#pncmother').focus();
            	$(this).dialog("close");
            },
			"Save & Serve Next Client": function () {
				var divName="#newBornInfo_"+id;
        		$( "#addNewBorn" ).clone().insertAfter( divName );
        		newbornInfo(newbornObj);
        		$(divIdName).find('*').prop('disabled', true);
        		var divButtonName="#newbornSave"+id;
        		$( divButtonName ).hide();
        		$( "#addNewBorn" ).remove();
        		goToId='.nextC';	
			    $(goToId).click();
            	$(this).dialog("close");
            },
            Cancel: function () {                	  
        		$(this).dialog("close");      
        	}
        }
    });
	
	}
	else{
		callDialog(selectGender);		
	}	   		
	//alert(newbornObj.providerid + " " + newbornObj.birthStatus + " "+ newbornObj.gender);
}

//retrieving newborn information
function retrieveNewbornInfo(){
	var newbornObj = new Object();
	newbornObj = setCommonParam();
	
	newbornObj.healthid = $('#healthid').text();
	newbornObj.pregno = $('#pregNumber').text();
	newbornObj.newbornLoad = "retrieve";
	
    newbornInfo(newbornObj);
}

/**
 * insert and retrieve newborn information
 * */
function newbornInfo(newbornInfoObj) {
	
	if (newbornInfoObj.healthid != "" && newbornInfoObj.pregno != "") {
		$.ajax({
			type: "POST",
			url: "newborn",
			timeout:60000, //60 seconds timeout
			dataType: "json",
			data: {"newbornInfo" : JSON.stringify(newbornInfoObj)},
			success : function(response) {
				if(response.operation=="retrieve"){
					parseNewbornInfo(response);
				}	
				if(response.operation=="delete"){					
					retrieveNewbornInfo();
				}	
			},
			error : function(xhr, status, error) {
				alert(status);
				alert(error);
			}
		});
	}
}

function parseNewbornInfo(response){
	numberOfChild=response.count;
	newbornInit();
	if(response.deliveryInfo==0){
		//newbornInit();		
		$("div#newborn").hide();
	}	
	else{
		//newbornInit();		
		/*for(var j=1;j<=5;j++){
			immature = "immatureBirth" + j;
			if($('#immatureBirthInfo').text()!=""){
				$('input[name='+immature+'][value=' + $('#immatureBirthInfo').text() + ']').prop("checked", true);
			}				
			else{
				$('input[name='+immature+'][value="1"]').prop("checked", false);
				$('input[name='+immature+'][value="2"]').prop("checked", false);
			}
		}*/
		if(response.hasNewbornInfo=="Yes"){
			//$( "#addNewBorn" ).remove();
			for(var i=1;i<=response.count;i++){
				//alert(response[i].birthStatus);
				//alert(response[i].weight);				
				
				birthStatus = "#birthStatus" + i;
				$(birthStatus).text(response[i].birthStatus);				
				
				birthstat = "#birthstat" + i;
				$(birthstat).text(response[i].birthStatus=="1"?"জীবিত জন্ম":"মৃত জন্ম");
				
				immaturebirth = "#immatureBirthWeekSection" + i;
				newbornHighlight = "newbornHighlight" + i;
				if(response[i].birthStatus=="1"){
					$(immaturebirth).show();
				}
				else{
					$(immaturebirth).hide();
					document.getElementById(newbornHighlight).style.background = "none";
				}
								
				newBornSex = "newBornSex" + i;
				if(response[i].gender!=""){
					$('input[name='+newBornSex+'][value=' + response[i].gender + ']').prop("checked", true);
				}				
				else{
					$('input[name='+newBornSex+'][value="1"]').prop("checked", false);
					$('input[name='+newBornSex+'][value="2"]').prop("checked", false);
					$('input[name='+newBornSex+'][value="3"]').prop("checked", false);
				}
				
				weight = "#birthWeight" + i;
				if(response[i].weight<t_newborn_weight)

				{$(weight).addClass("logic_danger_font");
				}
				else
				$(weight).removeClass("logic_danger_font");
				
				$(weight).val(parseFloat(response[i].weight).toFixed(2));
				
				
				/*immature = "immatureBirth" + i;
				if($('#immatureBirthInfo').text()!=""){
					$('input[name='+immature+'][value=' + $('#immatureBirthInfo').text() + ']').prop("checked", true);
				}				
				else{
					$('input[name='+immature+'][value="1"]').prop("checked", false);
					$('input[name='+immature+'][value="2"]').prop("checked", false);
				}*/
				
				washAfterBirth = "washAfterBirth" + i;
				if(response[i].dryingAfterBirth!=""){
					$('input[name='+washAfterBirth+'][value=' + response[i].dryingAfterBirth + ']').prop("checked", true);
				}				
				else{
					$('input[name='+washAfterBirth+'][value="1"]').prop("checked", false);
					$('input[name='+washAfterBirth+'][value="2"]').prop("checked", false);
				}
				
				resassitation = "resassitation" + i;
				if(response[i].resassitation!=""){
					$('input[name='+resassitation+'][value=' + response[i].resassitation + ']').prop("checked", true);
					
					stimulation = "#stimulation" + i;
					if(response[i].stimulation=="1"){
						$(stimulation).prop( "checked", true );				
					}
					else{
						$(stimulation).prop( "checked", false );
					}
					
					bagNMask = "#bagNMask" + i;
					if(response[i].bagNMask=="1"){
						$(bagNMask).prop( "checked", true );				
					}
					else{
						$(bagNMask).prop( "checked", false );
					}
					newBornresassitationSpan = "#newBornresassitation" + i;
					$(newBornresassitationSpan).show();
				}				
				else{
					$('input[name='+resassitation+'][value="1"]').prop("checked", false);
					$('input[name='+resassitation+'][value="2"]').prop("checked", false);
					newBornresassitationSpan = "#newBornresassitation" + i;
					$(newBornresassitationSpan).hide();
				}
				
				stimulation = "#stimulation" + i;
				if(response[i].stimulation=="1"){
					$(stimulation).prop( "checked", true );				
				}
				else{
					$(stimulation).prop( "checked", false );
				}
				
				bagNMask = "#bagNMask" + i;
				if(response[i].bagNMask=="1"){
					$(bagNMask).prop( "checked", true );				
				}
				else{
					$(bagNMask).prop( "checked", false );
				}
				
				chlorehexidin = "chlorehexidin" + i;
				if(response[i].chlorehexidin!=""){
					$('input[name='+chlorehexidin+'][value=' + response[i].chlorehexidin + ']').prop("checked", true);
				}				
				else{
					$('input[name='+chlorehexidin+'][value="1"]').prop("checked", false);
					$('input[name='+chlorehexidin+'][value="2"]').prop("checked", false);
				}
				
				skinTouch = "skinTouch" + i;
				if(response[i].skinTouch!=""){
					$('input[name='+skinTouch+'][value=' + response[i].skinTouch + ']').prop("checked", true);
				}				
				else{
					$('input[name='+skinTouch+'][value="1"]').prop("checked", false);
					$('input[name='+skinTouch+'][value="2"]').prop("checked", false);
				}
				
				breastfeed = "breastfeed" + i;
				if(response[i].breastFeed!=""){
					$('input[name='+breastfeed+'][value=' + response[i].breastFeed + ']').prop("checked", true);
				}				
				else{
					$('input[name='+breastfeed+'][value="1"]').prop("checked", false);
					$('input[name='+breastfeed+'][value="2"]').prop("checked", false);
				}
				
				newBornRefer = "#newBornRefer" + i;
				if(response[i].refer=="1"){
					$(newBornRefer).prop( "checked", true );
					newBornReferCenter = "#newBornReferCenter" + i;
					$(newBornReferCenter + ' option[value="' + response[i].referCenterName + '"]').prop('selected', true);
					
					referReason = response[i].referReason;
					referReason = referReason.slice(1,-1);
					referReason = referReason.split(",");
					
					var nReferReason = "";
					for(var k=0;k<referReason.length;k++){
						nReferReason = nReferReason + referReason[k].slice(1,-1) + ",";
					}
					nReferReason = nReferReason.slice(0,-1);
					nReferReason = nReferReason.split(",");
					newBornReferReason = "#newBornReferReason" + i;
					$(newBornReferReason).multipleSelect("uncheckAll");
					$(newBornReferReason).multipleSelect("setSelects", nReferReason);
					
					newBornReferSpan = "#newBornReferSpan" + i;
					$(newBornReferSpan).show();
					
				}
				else{
					$(newBornRefer).prop("checked", false );
					newBornReferSpan = "#newBornReferSpan" + i;
					$(newBornReferSpan).hide();
				}
				
				divId = "#newBornInfo_" + i;
				saveId = "#newbornSave" + i;
				$(saveId).hide();
				
				if(response[i].birthStatus == "1"){
					var fresh = "#fresh" + i; 
					$(fresh).show();
					var macerated = "#macerated0_" + i;
					$(macerated).hide();
					macerated = "#macerated1_" + i;
					$(macerated).show();
					macerated = "#macerated2_" + i;
					$(macerated).show();
				}
				else if(response[i].birthStatus == "2"){
					var fresh = "#fresh" + i; 
					$(fresh).hide();
					var macerated = "#macerated0_" + i;
					$(macerated).hide();
					macerated = "#macerated1_" + i;
					$(macerated).hide();
					macerated = "#macerated2_" + i;
					$(macerated).show();
				}
				else if(response[i].birthStatus == "3"){
					var macerated = "#macerated0_" + i;
					$(macerated).show();
					macerated = "#macerated1_" + i;
					$(macerated).hide();
					macerated = "#macerated2_" + i;
					$(macerated).hide();
				}
				
				newbornProviderId=response[response.count].providerId;
				$(divId).find('*').prop('disabled', true);
				$(divId).show();			
			}
			//var divName="#newBornInfo_"+id;
			for(var j=response.count+1;j<=5;j++){
				divXId = "#newBornInfo_" + j;
				$(divXId).find('*').prop('disabled', false);
			}			
    		$( "#addNewBorn" ).clone().insertAfter(divId);
    		$( "#addNewBorn" ).remove();
		}
		else{
			newbornInit();
		}
		//$("div#newborn").show();
		if(response.abortion==1){
			newbornInit();
			$("div#newborn").hide();
		}
		else{			
			$("div#newborn").show();
		}
		
		deleteButtonUI(numberOfChild);
	}	
}

//refer checkbox,Save Button,resassitation handling
	for(i=1;i<=5;i++){
         
		newBornRefer="#newBornRefer"+i;
		$(document).on("click", newBornRefer ,function (){
			
			id=$(this).attr('id').slice(-1);
		
			newBornRefer="#newBornRefer"+id;
			newBornReferCenter="#newBornReferCenter" + id;
			newBornReferReason="#newBornReferReason"+ id;
			newBornReferSpan="#newBornReferSpan" + id;
					
			handleOnclickCheckbox(newBornRefer,newBornReferCenter,newBornReferReason,"show",newBornReferSpan);
		});
		
		
		newbornSave="#newbornSave"+i;
		$(document).on("click", newbornSave ,function (){
			var divIdName = "#" + $(this).parent().attr('id');
			//alert(divIdName);
			insertNewbornInfo(divIdName);
		 });
		
		resassitation='input[name=resassitation'+i+']';
		$(document).on("click", resassitation,function (){
			id=$(this).attr('id').slice(-1);
			resassitation='input[name=resassitation'+id+']';
			stimulation='#stimulation'+id;
			bagNMask='#bagNMask'+id;
			newBornresassitation='#newBornresassitation'+id;
			if($(resassitation+':checked').val()=="1"){
				$(stimulation).prop('checked', true);
				$(newBornresassitation).show();
			}
			else{
				$(stimulation).prop('checked', false);
				$(bagNMask).prop('checked', false);
				$(newBornresassitation).hide();
			}
		});
	
}

	
$(document).on("click", '#addLiveBirth' ,function (){
	//var divId =numberOfChild+1; /*$(this).parent().next().attr('id');*/
	var divId = $(this).parent().next().attr('id');
	divId = divId.split("_")[1];
	var spanId = "#birthStatus" + divId;
	$(spanId).text("1");
	var fresh = "#fresh" + divId;
	$(fresh).show();
	var macerated = "#macerated0_" + divId;
	$(macerated).hide();
	macerated = "#macerated1_" + divId;
	$(macerated).show();
	macerated = "#macerated2_" + divId;
	$(macerated).show();
	
	$(this).parent().next().show();
	
	/*
	div = "#newBornInfo_" + divId;
	addSpanDiv = "#newBornInfo_" + numberOfChild;
	$( "#addNewBorn" ).clone().insertAfter(addSpanDiv);
	$( "#addNewBorn" ).remove();
	$(div).show();*/
});

$(document).on("click", '#addStillBirthFresh' ,function (){
	//var divId =numberOfChild+1;
	var divId = $(this).parent().next().attr('id');
	divId = divId.split("_")[1];
	var spanId = "#birthStatus" + divId;
	$(spanId).text("2");
	var fresh = "#fresh" + divId;
	$(fresh).hide();
	var macerated = "#macerated0_" + divId;
	$(macerated).hide();
	macerated = "#macerated1_" + divId;
	$(macerated).hide();
	macerated = "#macerated2_" + divId;
	$(macerated).show();
	
	$(this).parent().next().show();
	/*
	div = "#newBornInfo_" + divId;
	$(div).show();
	*/
});

$(document).on("click", '#addStillBirthMacerated' ,function (){
	//var divId =numberOfChild+1;
	var divId = $(this).parent().next().attr('id');
	divId = divId.split("_")[1];
	var spanId = "#birthStatus" + divId;
	$(spanId).text("3");
	var macerated = "#macerated0_" + divId;
	$(macerated).show();
	macerated = "#macerated1_" + divId;
	$(macerated).hide();
	macerated = "#macerated2_" + divId;
	$(macerated).hide();
		
	$(this).parent().next().show();
/*
	div = "#newBornInfo_" + divId;
	$(div).show();*/
});

$(document).on("click", '#deleteLastButtonNewborn' ,function(event){
	deleteButtonAction(numberOfChild,newbornProviderId)});		

function newbornInit(){
	for(i=1;i<=5;i++){
		
		gender = "newBornSex" + i;
		$('input[name=' + gender + '][value="1"]').prop("checked", false);
		$('input[name=' + gender + '][value="2"]').prop("checked", false);
		$('input[name=' + gender + '][value="3"]').prop("checked", false);
		
		weight = "#birthWeight" + i;
		$(weight).val("");
		
		/*
		immatureBirth = "immatureBirth" + i;
		$('input[name=' + immatureBirth + '][value="1"]').prop("checked", false);
		$('input[name=' + immatureBirth + '][value="2"]').prop("checked", false);
		*/
		
		washAfterBirth = "washAfterBirth" + i;
		$('input[name=' + washAfterBirth + '][value="1"]').prop("checked", false);
		$('input[name=' + washAfterBirth + '][value="2"]').prop("checked", false);
		
		resassitation = "resassitation" + i;
		$('input[name=' + resassitation + '][value="1"]').prop("checked", false);
		$('input[name=' + resassitation + '][value="2"]').prop("checked", false);
		
		stimulation = "#stimulation" + i;
		$(stimulation).prop( "checked", false );
		
		bagNMask = "#bagNMask" + i;
		$(bagNMask).prop( "checked", false );
		
		newBornresassitation = "#newBornresassitation" + i;
		$(newBornresassitation).hide();
		
		chlorehexidin = "chlorehexidin" + i;
		$('input[name=' + chlorehexidin + '][value="1"]').prop("checked", false);
		$('input[name=' + chlorehexidin + '][value="2"]').prop("checked", false);
		
		skinTouch = "skinTouch" + i;
		$('input[name=' + skinTouch + '][value="1"]').prop("checked", false);
		$('input[name=' + skinTouch + '][value="2"]').prop("checked", false);
		
		breastfeed = "breastfeed" + i;
		$('input[name=' + breastfeed + '][value="1"]').prop("checked", false);
		$('input[name=' + breastfeed + '][value="2"]').prop("checked", false);
		
		newBornRefer = "#newBornRefer" + i;
		$(newBornRefer).prop( "checked", false );
		
		newBornReferCenter = "#newBornReferCenter" + i;
		$(newBornReferCenter + ' option[value="0"]').prop('selected', true);
		
		newBornReferReason = "#newBornReferReason" + i;
		$(newBornReferReason).multipleSelect("uncheckAll");
		
		newBornReferSpan = "#newBornReferSpan" + i;
		$(newBornReferSpan).hide();
		
		newbornSave = "#newbornSave" + i;
		$(newbornSave).show();
		
		id = "#newBornInfo_" + i;
		$(id).hide();		
	}		
}

function deleteButtonAction(id,providerId){
	
	if($('#providerid').text()!=providerId){
		callDialog(diffProvider);
	}
	else{
		$('<div></div>').dialog({
			   modal: true,
			   title: "নিশ্চিত করুন",
			   width: 420,
			   open: function () {
				   $(this).html(confirmDelete);
			   },
			   buttons: {
				   Yes: function () {
					   deleteNewbornInfo(id);
					   $(this).dialog("close");   
				   },  
				   No: function () {
					   $(this).dialog("close");
				 }  
			   }
		   });				
	}
}

function deleteButtonUI(id){
	if(id==0){
		$('#deleteLastNewborn').hide();
	}
	else{
		$('#deleteLastNewborn').show();
	}
}


/*
//retrieve newborn information by expand the div and collapse
$(document).on("click", '#subheadingNewbornSlider',	function() {
	retrieveNewbornInfo();
	$('div#newborn').slideToggle(100,function() {
		$('div#subheadingNewbornSlider').text(function() {
			// change text based on condition
			return $('div#newborn').is(":visible") ? "নবজাতকের সংক্রান্ত তথ্য (-)" : "নবজাতকের সংক্রান্ত তথ্য (+)";
		});
	});
});*/