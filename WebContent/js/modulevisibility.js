function openANC(){
		
		hideServiceDiv();
		$("#heading").show();
    	
    	retrieveANCInfo();
    	  	
		$('div#anc').show();		 
		
		var divPosition = $('div#anc').offset();
		$('html, body').animate({scrollTop: divPosition.top}, "slow");		
	}
	
	function openDelivery(){

		hideServiceDiv();
		$("#heading").show();
    	
		retrieveDeliveryInfo();
    	retrieveNewbornInfo();
    	
    	$('div#delivery').show();
    	
    	var divPosition = $('div#delivery').offset();
		$('html, body').animate({scrollTop: divPosition.top}, "slow");	
	}
	
	function openPNC(){
		
		hideServiceDiv();
		$("#heading").show();
		    	    	
    	retrievePNCMotherInfo();
    	
    	var divPosition = $('div#pncmother').offset();
		$('html, body').animate({scrollTop: divPosition.top}, "slow");	
	}
	
	function openPAC(){
		
		hideServiceDiv();
		$("#heading").show();
		
		retrievePACInfo();	
		
		$('div#pac').show();
		
		var divPosition = $('div#pac').offset();
		$('html, body').animate({scrollTop: divPosition.top}, "slow");	
    					
	}
	
	function openPillCondom(){
		
		hideServiceDiv();
		
		$("#heading").show();
		
		retrievePillCondomInfo();	
		
		$('div#content').show();
		
		var divPosition = $('div#content').offset();
		$('html, body').animate({scrollTop: divPosition.top}, "slow");	
    					
	}
	
	function openInjectable(){
		
		hideServiceDiv();
		
		$("#heading").show();
		
		retrieveInjectableInfo();
		
		$('div#content').show();
		
		var divPosition = $('div#content').offset();
		$('html, body').animate({scrollTop: divPosition.top}, "slow");
		
	}
	
	function openIUD(){
		
		hideServiceDiv();
		
		$("#heading").show();
		
		retrieveIUDInfo();
		
		$('div#content').show();
		
		var divPosition = $('div#content').offset();
		$('html, body').animate({scrollTop: divPosition.top}, "slow");
		
	}
	
	function openGP(){
		
		hideServiceDiv();
		
		$("#heading").show();
		
		retrieveGPInfo();
		
		$('div#content').show();
		
		var divPosition = $('div#content').offset();
		$('html, body').animate({scrollTop: divPosition.top}, "slow");
		
	}
	
	function openReportSection(){
		
		var contentReport = "<h2>এমআইএস ফরম - ৩</h2><br>";
		contentReport += "<span class='left'> <table><tbody><tr><td><input type='radio' id='dayRange' name='duration' value='1'/> তারিখ অনুযায়ী&nbsp;&nbsp;&nbsp;&nbsp;</td>";
		contentReport += "<td><input type='radio' id='monthRange' name='duration' value='2' checked='checked'/> মাস অনুযায়ী &nbsp;&nbsp;&nbsp;&nbsp;</td></tr></tbody></table></span>";
		contentReport += "<span id='monthRangeSpan' class='left'> <table><tbody><tr><td>মাস </td><td><input type='text' id='monthReport' maxlength='11' size='11'/></td></tr></tbody></table></span>";
		contentReport += "<span id='dayRangeSpan' hidden='true' class='left'> <table><tbody><tr><td>শুরুর তারিখ </td><td><input type='text' id='startDateReport' maxlength='11' size='11'/></td></tr>";
		contentReport += "<tr><td> শেষ  তারিখ </td><td> <input type='text' id='endDateReport' maxlength='11' size='11'/></td></tr></tbody></table></span>";
		contentReport += "<span class='left'> <table><tbody><tr><td>&nbsp;&nbsp;<input type='button' id='getMIS3' value='Generate'/> </td></tr></tbody></table></span>";
		
		contentReport += "<br><br><br><br><br><div id='mis3mnch'> </div>";
		
		$('div#report').append(contentReport);
		
		$('#startDateReport').combodate({
    		format: "YYYY-MM-DD",
    		template: "DD MMM YYYY",
    		smartDays: true,
    		value : moment()
    	});
		$('#endDateReport').combodate({
    		format: "YYYY-MM-DD",
    		template: "DD MMM YYYY",
    		smartDays: true,
    		value : moment()
    	});
		$('#monthReport').combodate({
    		format: "YYYY-MM",
    		template: "MMM YYYY",
    		smartDays: true,
    		value : moment()
    	});
				
		$('div#report').show();
	}
	
	function hideServiceDiv(){
		
		$('div#anc').hide();
    	$('div#delivery').hide();
    	$('div#newborn').hide();
    	$("div#pncmother").hide();
    	$('div#pncchild').hide();
    	$('div#pac').hide();
    	$('div#content').hide();
	}
	
	function addNewPreg(){ 	
		$("div#ancVisitOpen").hide();
		$("div#anc").hide();
		$("div#deliveryOpen").hide();
		$("div#delivery").hide();
		$("div#newborn").hide();
		$("div#pncmother").hide();
		$('div#pncchild').hide();
		$('div#new').show();
		$("div#heading").hide();
		$("#search").hide();
		
		$('#personalInfo').find('*').prop('disabled',false);
		$('#pregNumber').html("");
		$('#lmp').combodate('setValue', moment(),true);
		$('#lmpDisagreement').html("");
		$('#lmpDisagreement').hide();
		eddDate = moment().add({days:280}).format("DD MMM YYYY");
		$("#edd").prop('disabled',true);
		$('#personalInfo').find('input[type=text]').val("");	
		//$('#personalInfo').find('input[type=checkbox]').prop( "disabled", true );
		$('#personalInfo').find('input[type=radio]').prop( "checked", false );
		$('#anyAlermingHistoryContent').val("");
		$('#anyAlermingHistoryContentSection').hide();
		$('#bloodGroup option[value=none]').prop('selected', true);
		$('#personalInfo').find('input[type=checkbox]').prop( "disabled", false );
		disableTTCheckBox();
		
		$('#personalInfoNewColumn').hide();
		$('#personalInfoEditColumn').hide();
		$('#personalInfoUpdateColumn').hide();
		$('#personalInfoSaveColumn').show();
		$("#edd").val(eddDate);
		$("#regNumber").text("");
	   	$("#regDate").text("");
	   	$('#cause').remove();
	}
	
	function deliveryInformationPNC(deliveryStatus){
		
		if(deliveryStatus==2){ 
			
    		$('<div></div>').dialog({
				modal: true,
				title: "Information Shortage",
				width: 420,
				open: function () {
					$(this).html(PNCwithoutDelivery);
				},
				buttons: {
					Ok: function () {
						openTab='2';   //openDelivery
			        	goToId='#'+openTab+'.serviceTabButton';	
			        	$(goToId).click();		        	
			        	openTab=""; 						
						$(this).dialog("close");
					}      
				}
			});
    		
    	}
    	else{
    		retrievePNCChildInfo();
        	
        	$('div#new').show();
           	//alert(deliveryStatus + "init");
        	var divPosition = $('div#pncmother').offset();
    	   	$('html, body').animate({scrollTop: divPosition.top}, "slow");
    	   	
    	}
	}
	
	function openDeath(riskyPregX){
		
		if(riskyPregX==true){
    		$('#personalInfo').css("background-color","#D39C8A");
	    	$('#personalDetail').css("background-color","#D39C8A");
    	}
    	if(riskyPregX==false){
    		$('#personalInfo').css("background-color","#FFFFFF");
    		$('#personalDetail').css("background-color","#FFFFFF");
    	}
		
		resetDeathFields();
		//retrieveDeathInfo();		
		//$('#deathInfo').modal();
		document.getElementById('deathIdType').innerHTML=($('#healthIdTitle').text()); 
		$('#deathInfo').show();
	}
		
	////Function to Handle Gender
	
	function toggleServices(type){
		i=1;
		while(i<23)
		{
			if(type=="dead"){
				if(i!=6){
					flag=true;
					$("button#"+i+".serviceTabButton").addClass("disable_font");
					$(".pregList").addClass("disable_font");
					$(".othersList").addClass("disable_font");
				}
				else{
					flag=false;
					$("button#"+i+".serviceTabButton").removeClass("disable_font");
					$(".pregList").removeClass("disable_font");
					$(".othersList").removeClass("disable_font");
				}

			}
			
			else if(type=="notElco"){
				
				if(i<6 || i == 21){
					flag=true;
					$("button#"+i+".serviceTabButton").addClass("disable_font");
					$(".pregList").addClass("disable_font");
				}
				else{
					flag=false;
					$("button#"+i+".serviceTabButton").removeClass("disable_font");
					$(".othersList").removeClass("disable_font");
				}
				
			}
			
			else{
				if(type=="pacEnable"){
					flag=true;
					if(i==2 || i==3){
						$("button#"+i+".serviceTabButton").addClass("disable_font");
						$(".pregList").addClass("disable_font");
						$(".othersList").addClass("disable_font");
					}
					else{
						flag=false;
						$("button#"+i+".serviceTabButton").removeClass("disable_font");
						$(".pregList").removeClass("disable_font");
						$(".othersList").removeClass("disable_font");
						}					
				}
				
				else if(type=="pacDisable"){
					flag=true;
					if(i==4){
						$("button#"+i+".serviceTabButton").addClass("disable_font");
						$(".pregList").addClass("disable_font");
						$(".othersList").addClass("disable_font");
					}
					else{
						flag=false;
						$("button#"+i+".serviceTabButton").removeClass("disable_font");
						$(".pregList").removeClass("disable_font");
						$(".othersList").removeClass("disable_font");
						}				
				}
				else{
					flag=false;
					$("button#"+i+".serviceTabButton").removeClass("disable_font");
					$(".pregList").removeClass("disable_font");
					$(".othersList").removeClass("disable_font");
					}				
			}
			
				$("ul li#"+i).prop('disabled',flag);
				$("button#"+i+".serviceTabButton").prop('disabled',flag);
			
				i++;
		
		}
				
	}
	
	function detail(val){		
		if(val!="[]" && val!="")
			return "বিস্তারিত";
		else
			return "";	
	}
	
	function closePregType(){
		if ($('input[name=closeType]:checked').val()=='abortion')
				$("#closeDeliverySpan").hide();
		else if ($('input[name=closeType]:checked').val()=='delivery')	
				$("#closeDeliverySpan").show();		
	}
	function resetClosePreg(type){
		if(type=="new"){
			$('#closePregTitle1').show();
			$('#closePregTitle2').hide();
			$("#closeTypeDelivery").prop("checked", true);
			$("#closeTypeSpan").show();
			closePregType();
			$('#deliveryDate').combodate('setValue',moment(),true);
			$('#closePreg').show();
		}
			
		if(type=="pac"){
			$('#closePregTitle2').show();
			$('#closePregTitle1').hide();
			$("#closeTypeAbortion").prop("checked", true);
			$("#closeTypeSpan").hide();
			closePregType();
			$('#deliveryDate').combodate('setValue',moment(),true);
			$('#closePreg').show();
		}
	}
	
	function regInfoMNCH(){
		$("#regNumber").html($("#regNoMNCH").text());
		$("#regDate").html($("#regDateMNCH").text());
	}