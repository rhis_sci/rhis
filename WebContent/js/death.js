/**
 * insert death information
 **/

function insertDeathInfo(){
		
	var deathObj = new Object();
	deathObj = setCommonParam();
		
	deathObj.healthId = $('#healthIdDeath').text();
	deathObj.providerId=$('#providerid').text();
	
	if($('#deathMother').prop('checked')){
		deathObj.deathOfPregWomen = "1";
	}
	else{
		deathObj.deathOfPregWomen = "";
	}
		
	if($('#deathNewborn').prop('checked')){
		deathObj.pregNo = $('#pregNoDeath').text();
		deathObj.childNo = $('#childNoList').val();
	}
	else{
		deathObj.pregNo = "";
		deathObj.childNo = "";
	}
	
	deathObj.deathDT = $('#deathDate').val();
	deathObj.placeOfDeath = $('#deathPlace').val();
	deathObj.causeOfDeath = $('#deathReason').val();
	
	deathObj.client = "1";
	deathObj.deathLoad = "";
	
	deathInfo(deathObj);
	
}

/**
 * update death information
 * */

function UpdateDeathInfo(){
	
	var deathObj = new Object();
	deathObj = setCommonParam();
	
	deathObj.healthId = $('#healthIdDeath').text();
	deathObj.providerId=$('#providerid').text();
	
	if($('#deathMother').prop('checked')){
		deathObj.deathOfPregWomen = "1";
	}
	else{
		deathObj.deathOfPregWomen = "";
	}
		
	if($('#deathNewborn').prop('checked')){
		deathObj.pregNo = $('#pregNoDeath').text();
		deathObj.childNo = $('#childNoList').val();
	}
	else{
		deathObj.pregNo = "";
		deathObj.childNo = "";
	}
	
	deathObj.deathDT = $('#deathDate').val();
	deathObj.placeOfDeath = $('#deathPlace').val();
	
	deathObj.causeOfDeath = $('#deathReason').val();		
	deathObj.deathLoad = "update";
	
	if($('#providedBy').text()==$('#providerid').text())
		{
		deathInfo(deathObj);
		if($('#visibleHealthid').text()!=$('#healthid').text()){
			$('#searchOption option[value=1]').prop('selected', true);			
		}
		else{
			$('#searchOption option[value=5]').prop('selected', true);
		}
		$('#searchString').val($('#visibleHealthid').text());
		$.modal.close();
		$('#deathInfo').hide();
		getAjaxClientInfo();			
	}	
	else{		
		callDialog(diffProvider);
	}	
}
/**
 * retrieve death information
 * */
function retrieveDeathInfo(){
		
	var deathObj = new Object();
	deathObj = setCommonParam();
	
	deathObj.healthId = $('#healthIdDeath').text();
	
	if($('#deathNewborn').prop('checked')){
		deathObj.pregNo = $('#pregNoDeath').text();
		deathObj.childNo = $('#childNoList').val();
	}
	else{
		deathObj.pregNo = "";
		deathObj.childNo = "";
	}
			
	deathObj.deathLoad = "retrieve";
	
	deathInfo(deathObj);
}
/**
 * retrieve child no
 * */
function retrieveChildInfo(){
		
	var deathObj = new Object();
	deathObj = setCommonParam();
	
	deathObj.healthId = $('#healthIdDeath').text();
	deathObj.pregNo = $('#pregNoDeath').text();
		
	deathObj.deathLoad = "retrieveChild";
	
	deathInfo(deathObj);
}

/**
 * insert and retrieve
 * */
function deathInfo(deathInfoObj) {
	
	if (deathInfoObj.healthId != "") {
		$.ajax({
			type: "POST",
			url: "death",
			timeout:60000, //60 seconds timeout
			dataType: "json",
			data: {"deathInfo" : JSON.stringify(deathInfoObj)},
			success : function(response) {
				if(response.operation=="retrieveChild"){
					output = "";
					output = output + "<option value = 0></option>";
					$('#childNoList').empty();
					for(i=1; i<=response.count;i++){
						output = output + "<option value = " + response[i] + ">" + response[i] + "</option>";					
					}
					$('#childNoList').append(output);
					$('#deathNewborn').prop('disabled',false);
				}
				else if(response.operation=="retrieve"){
					
					if(response.count==0){
						callDialog(noDeath);	
						resetDeathFields();
					}
					else{
						if(response.clientDeathStatus==1){
							for(i=1; i<=response.count; i++){
								if(response[i].pregNo==0 && response[i].childNo==0){
									(response[i].deathOfPregWomen== 1 ? $('#deathMother').prop('checked',true): $('#deathMother').prop('checked', false));
									response[i].deathOfPregWomen== 1 ? ($('#deathReason').empty(),$('#deathReason').append(populateSpecificDropdownList(maternalDeathReason,deathReasonJS[0]))) :"";
									//$('#deathNewborn').prop('checked',false);								
									$('#deathDate').combodate('setValue', response[i].deathDT, true);
									$('#deathDate').combodate('setValue', response[i].deathDT, true);
									$('#deathPlace option[value="' + response[i].placeOfDeath + '"]').prop('selected', true);
									$('#deathReason option[value="' + response[i].causeOfDeath + '"]').prop('selected', true);
									document.getElementById('providedBy').innerHTML=response[i].providerId;
								}
							}
							
						}
						else{
							//alert("client is not dead");
							$('#deathMother').prop('disabled',true);
							$('#deathNewborn').prop('checked',true);
							$('#deathNewborn').prop('disabled',true);
							$('#newbornDeathSection').show();
							
						}
						
						if(response.childDeathStatus==1){
							//retrieve dead child info
							if($('input[name=updateReport]:checked').val()==2 && $('#deathNewborn').prop('checked')){
								for(i=1; i<=response.count; i++){
									if(response[i].pregNo==deathInfoObj.pregNo && response[i].childNo==deathInfoObj.childNo){
									   (response[i].deathOfPregWomen== 1 ? $('#deathMother').prop('checked',true): $('#deathMother').prop('checked', false));
										$('#deathDate').combodate('setValue', response[i].deathDT, true);
										$('#deathDate').combodate('setValue', response[i].deathDT, true);
										$('#deathPlace option[value="' + response[i].placeOfDeath + '"]').prop('selected', true);
										$('#deathReason option[value="' + response[i].causeOfDeath + '"]').prop('selected', true);
										document.getElementById('providedBy').innerHTML=response[i].providerId;
									}
								}
							}
							//create dead child selection list
							set=deathInfoObj.childNo;
							output = "";
							output = output + "<option value = 0></option>";
							$('#childNoList').empty();
							for(i=1; i<=response.count; i++){
								if(response[i].pregNo!=0 && response[i].childNo!=0){
									output = output + "<option value = " + response[i].childNo + ">" + response[i].childNo + "</option>";
								}						
							}
							$('#childNoList').append(output);
							$('#childNoList').val(set);
							$('#deathNewbornSpan').show();
							
						}
						else{
							$('#deathNewborn').prop('disabled',true);
							$('#childNoList').empty();
							$('#deathNewbornSpan').hide();
						}
						
						
					}
				}
				else{
					$('#deathStatus').html("1");
				}
			},
			error : function(xhr, status, error) {
				alert(status);
				alert(error);
			}
		});
	}
}

function resetDeathFields(){
	
	$('input[name="updateReport"][value="1"]').prop("checked", true);	
	
	$('#deathMother').prop('checked', false);
	$('#deathNewborn').prop('checked', false);
	$('#deathMother').prop('disabled',false);
	$('#deathNewborn').prop('disabled',false);
	
	if($('#gender').text()!='2' || ben_to_en_number_conversion($('#clientAge').val().toString()) < 15 || ben_to_en_number_conversion($('#clientAge').val().toString()) > 49){
		$('#deathMotherSpan').hide();
		$('#deathNewbornSpan').hide();		
	}
	else{
		$('#deathMotherSpan').show();
		$('#deathNewbornSpan').show();		
	}
	
	$('#deathHealthId').val($('#visibleHealthid').text());
	$('#healthIdDeath').text($('#healthid').text());
	$('#pregNoDeath').text($('#pregNumber').text());
	$('#childNoList').empty();
	$('#newbornDeathSection').hide();
	$('#deathDate').combodate('setValue', moment().format("YYYY-MM-DD"), true);
	
	$('#deathUpdateNewborn').hide();
	$('#reportDeathSpan').show();
	$('#updateDeathSpan').hide();
	
	$('#deathInputSectionSpan').show();
}

function toggleDeathNewborn(){
	if($('#deathNewborn').prop('checked')){
		if($('input[name=updateReport]:checked').val()==1){
			retrieveChildInfo();
		}
		else{
			retrieveDeathInfo();
		}
		$('#newbornDeathSection').show();
	}
	else{
		$('#newbornDeathSection').hide();
	}
}

$(document).on("click", '#deathNewborn' ,function() {
	
	toggleDeathNewborn();
	$('#deathReason').empty();
	$('#deathReason').append(populateSpecificDropdownList(generalDeathReason,deathReasonJS[0]));
	
	if($('#deathNewborn').prop('checked')){//was unchecked first
		if(!$('#deathMother').prop('checked')){
			$('#deathMother').attr('checked', false);	//uncheck
			$('#deathMother').prop('disabled',true);	//disable
		}
		else{
			$('#deathMother').prop('disabled',false);
		}
	}
	else{
		$('#deathMother').prop('disabled',false);
	}
	
	/*$('#deathNewborn').attr('checked', false);
	
	if($('#deathMother').prop('checked'))
		$('#deathNewborn').prop('disabled',true);
	else
		$('#deathNewborn').prop('disabled',false);
	toggleDeathNewborn();*/
});

$(document).on("click", '#deathMother' ,function (){
	
	$('#deathReason').empty();
	
	if($('#deathMother').prop('checked')){//was unchecked first
		$('#deathReason').append(populateSpecificDropdownList(maternalDeathReason,deathReasonJS[0]));
		if(!$('#deathNewborn').prop('checked')){
			$('#deathNewborn').attr('checked', false);	//uncheck
			toggleDeathNewborn();//hide childList
			$('#deathNewborn').prop('disabled',true);		
		}
		else{
			$('#deathNewborn').prop('disabled',false);
		}						
	}
	else{
		$('#deathReason').append(populateSpecificDropdownList(generalDeathReason,deathReasonJS[0]));
		$('#deathNewborn').prop('disabled',false);		
	}
	
	/*$('#deathNewborn').attr('checked', false);
	
	if($('#deathMother').prop('checked'))
		$('#deathNewborn').prop('disabled',true);
	else
		$('#deathNewborn').prop('disabled',false);
	toggleDeathNewborn();*/
});

$(document).on("click", 'input[name=updateReport]',function (){
	
	if($(this).val()=="1"){
		resetDeathFields();
	}
	else{
		retrieveDeathInfo();
		$('#reportDeathSpan').hide();
		$('#updateDeathSpan').show();		
	}	
});

$(document).on("change", '#childNoList',function (){
		
	if($('input[name=updateReport]:checked').val()==2 && $('#deathNewborn').prop('checked')){
		retrieveDeathInfo();
	}	
});