$(document).ready(function(){
	
	var currentyear = (new Date).getFullYear();
	
	var f = $("<div id='bottom' class='wrapper'><footer>\u00A9"+currentyear+" RHIS (Version 20220914)</footer></div>"); // previous was 0.8
	$("body").append(f);
	
	var preventMultiCall = 0;
		
	$(document).on("keypress", '#providerPass',function (event){
		if(event.keyCode == 13){  //13 means 'enter' key		
			if(preventMultiCall==0){
				preventMultiCall = 1;
				preventMultiCall = loginCheck();												
			}				    
		}
	});
	
	$(document).on("click",'#login', function(){
		if(preventMultiCall==0){
			preventMultiCall = 1;
			preventMultiCall = loginCheck();
			//preventMultiCall = 0;		
		}		
	});
	
	//login check
	function loginCheck(){
		
		//var preventMultiC = 1;
		var loginObj = new Object();
		loginObj.uid = $('#providerID').val();
		loginObj.upass = $('#providerPass').val();
		loginObj.client = "1"; //web
		
		$.ajax({
           type: "POST",
           url:"login",
           timeout:60000, //60 seconds timeout
           data:{"loginInfo":JSON.stringify(loginObj)},
           success: function (response) {
        	   if(response=='false'){
             		   $("section").find("td.msg").text("Sorry! Wrong information provided.");
           	   }
        	   else{
        		   
        		   if($(response).find('#providerType').html()=='4' || $(response).find('#providerType').html() == '5' || 
        		      $(response).find('#providerType').html() == '6' || $(response).find('#providerType').html() == '101' ||
        		      $(response).find('#providerType').html() == '999' ||$(response).find('#csba').html()== '1'){
        			   
        			   $("#body").remove();
	        		   $( "#top" ).after(response);
	        		           		   
	        		   var headText = $('#facName').text();
	        		   var h = $("<header>" + headText + "</header>");
	        		   $("#top").prepend(h);
	        		   
	        		   getInitialSetup();
	        		   initializeSpecificField();
	        		   
	        		   if($(response).find('#csba').html()== '1'){
	        			   hideSetFieldsForCSBA();	        			   
	        		   }
        		   }
        		   else{
        			   callDialog(notAllowed);
        		   }
        		   
        	   }
           },
           complete: function() {
        	   preventMultiCall = 0;
           },
           error: function(xhr, status, error) {
				alert(xhr.status);
        	   	alert(status);
				//alert(error);
           }
		});	
		//alert(preventMultiC);
		return preventMultiCall;
	}
	
	//handle client who doesn't have healthid
	$(document).on("click",'#hasNoHealthId', function(){
		
		getInitialSetup();
		
		$("div#register_personal").show();
		$('#personalDetailNonReg').find('input[type=text]').val("");
		$('#RzillaName').html("");
		$('#RupaZilaName').html("");
		$('#RunionName').html("");
		$('#RvillageName').html("");
		
		$("#RclientName").focus();
		
		loadZilla("#RzillaName");
		   
	});
	
	$(document).on("click",'#healthIdGen', function(){
		retrieveGeneratedId();
	});
	
	//search client for details
	$(document).on("keypress", '#searchString',function (event){
		//checking if enter is pressed enter key code is 13
		if(event.keyCode == 13){
			if($('#searchString').val()!=""){
				if(preventMultiCall==0){
					preventMultiCall = 1;	
					getAjaxClientInfo();
					var divPosition = $('div#service_tab').offset();
		         	$('html, body').animate({scrollTop: divPosition.top}, "slow");
					preventMultiCall = 0;				
				}
			}
			else{
				callDialog(noString);
			}
		}
	});
	
	
	//event handler for getting client information
	$(document).on("click", '#searchButton' ,function (){
		if($('#searchString').val()!=""){
			if(preventMultiCall==0){
				preventMultiCall = 1;
				getAjaxClientInfo();
				var divPosition = $('div#service_tab').offset();
	         	$('html, body').animate({scrollTop: divPosition.top}, "slow");
				preventMultiCall = 0;		
			}
		}
		else{
			callDialog(noString);
		}
	});  
	
	
    //event handler for menu(report)
	/*
	$(document).on("click", '#report' ,function (){
		
		window.open("jsp/report.jsp", '_blank');

	});
	*/
	//event handler for submenu
	$(document).on("click", "ul li ul li,.serviceTabButton,.serviceTabButtonSelected" ,function (){
		if ($('#healthid').text()!="") {
		
			$content = $(this).text();
			$id = $(this).attr('id');
			openTab=$id;
			
			$("#search").hide();
			$("#heading").html("");
			$("#heading").hide();
			
			clearServicesUI();
			resetAllServiceTab();
			
	    	switch ($id) { 
					
				case "21": 
					changeColorSelectedServiceTab(["21"]);
					$("div#service_tab_MNCH").show();						
				break;
				
				case "22":
					changeColorSelectedServiceTab(["22"]);
					$("div#service_tab_FP").show();						
				break;
							
				case "1": 
					regInfoMNCH();
					highlightRisk(riskyPreg);
					changeColorSelectedServiceTab(["21","1"]);
					$("#heading").append($content);	    				
					$("div#personalReg").show();
					$("div#personalInfo").show();	
					$("div#service_tab_MNCH").show();
					$('div#new').show();
						
											
					if ($('#pregNumber').text()!=""){
						openANC();
					}
					else{		//To make sure if there is no pregInfo Register PregInfo is enabled							
						highlightRisk(false);
						callDialog(noPreg);
					}
				break;
						
				case "2":
					regInfoMNCH();
					highlightRisk(riskyPreg);
					changeColorSelectedServiceTab(["21","2"]);
					$("#heading").append($content);				
					$("div#personalReg").show();
					$("div#personalInfo").show();	
					$("div#service_tab_MNCH").show();
					$('div#new').show();
						
						
					if ($('#pregNumber').text()!=""){
						openDelivery();
					}
					else{
						highlightRisk(false);		//To make sure if there is no pregInfo Register PregInfo is enabled
						callDialog(noPreg);
					}
				break;		
						
				case "3": 
					regInfoMNCH();
					highlightRisk(riskyPreg);
					changeColorSelectedServiceTab(["21","3"]);
					$("#heading").append($content);			
					$("div#personalReg").show();
					$("div#personalInfo").show();	
					$("div#service_tab_MNCH").show();
					$('div#new').show();				
						
					if ($('#pregNumber').text()!=""){						
						openPNC();
					}
					else{
						//clearServicesUI();
						highlightRisk(false);
						$("div#personalInfo").hide();
						$('#shortPreg').show();       //To create short pregnancy information & skip clientInfo
							
						/*$callDialog(noPreg);*/											
					}
				break;	
				
				case "4":	
					regInfoMNCH();
					highlightRisk(riskyPreg);
					changeColorSelectedServiceTab(["21","4"]);
					$("#heading").append($content);	    				
					$("div#personalReg").show();
					$("div#personalInfo").show();	
					$("div#service_tab_MNCH").show();
					$('div#new').show();
					
					if ($('#pregNumber').text()!=""){
						if($('#hasDeliveryInfo').text()=="No"){
							resetClosePreg("pac");
						}
						else{
							openPAC();
						}
					}
					else{	
						highlightRisk(false);		//To make sure if there is no pregInfo Register PregInfo is enabled
						callDialog(noPreg);
					}
				break;
				
				case "6":
					openDeath();
					highlightRisk(false);
					changeColorSelectedServiceTab(["6"]);
					$("div#personalReg").hide();
					$("div#personalInfo").hide();
					$('div#anc').hide();
					$('div#delivery').hide();
					$('div#newborn').hide();
					$("div#pncmother").hide();
					$('div#pncchild').hide();
					$('div#pac').hide();
					$('div#content').hide();
					$("#heading").html("");
					$('div#new').show();
				break;
				
				case "7":
					highlightRisk(false);
					changeColorSelectedServiceTab(["22","7"]);
					openPillCondom();
					
					$("#heading").append($content);	    				
					$("div#personalReg").show();
					$("div#service_tab_FP").show();
					$("div#content").show();		
					$('div#new').show();
				break;
				
				case "8":
					highlightRisk(false);
					changeColorSelectedServiceTab(["22","8"]);
					openInjectable();
					
					$("#heading").append($content);	    				
					$("div#personalReg").show();
					$("div#service_tab_FP").show();
					$("div#content").show();		
					$('div#new').show();
				break;
				
				case "9":
					highlightRisk(false);
					changeColorSelectedServiceTab(["22","9"]);
					openIUD();
					
					$("#heading").append($content);	    				
					$("div#personalReg").show();
					$("div#service_tab_FP").show();
					$("div#content").show();		
					$('div#new').show();
				break;
				
				case "14":
					highlightRisk(false);
					changeColorSelectedServiceTab(["14"]);
					openGP();
					
					$("#heading").append($content);	    				
					$("div#personalReg").show();
					$("div#content").show();		
					$('div#new').show();
				break;
				
				/*case "22":
				case "4":
				case "14":*/
				case "5":
				case "10":
				case "15":

				//	clearServicesUI();	
					callDialog(upcoming);										
				break;					
				}
		}
		
		else{
			callDialog(noClient);		 
		}	
		
	});
	
	//event handler for new client
	$(document).on("click", '.nextC, .home, .home_selected' ,function (){
		riskyPreg=false;      //to make sure next client is not marked as red
		resetAllMenuItem();
		$('#homeMenu').removeClass();
		$('#homeMenu').addClass("home_selected");
		getInitialSetup();
		
	});
	
	//event handler for report
	$(document).on("click", '.report, .report_selected' ,function (){
		riskyPreg=false;      //to make sure next client is not marked as red
		resetAllMenuItem();
		$('#reportMenu').removeClass();
		$('#reportMenu').addClass("report_selected");
		getInitialSetup();
		$("#search").hide();
		openReportSection();
		
	});	
		
	//Only allowing numeric values for input fields and convert that to Bangla
	$(document).on("keyup", '#RclientAge',function (){
		$(this).val($(this).val().replace(/[^\d]/,''));
	});
	
	$(document).on("keyup", '#RcellNo',function (){
		$(this).val($(this).val().replace(/[^\d]/,''));
	});
	
	$(document).on("keyup", '#cellNo',function (){
		$(this).val($(this).val().replace(/[^\d]/,''));
	});
	
	$(document).on("keyup", '#para',function (){
		$(this).val($(this).val().replace(/[^\d]/,''));
	});
	//Also To handle first pregnacy logic
	$(document).on("keyup", '#gravida',function (){
		if($('#gravida').val()=='1')
		{	
			$('#para').val('0');
			$('#boy').val('0');
			$('#girl').val('0');
			$('#lastChildAgeYear').val('0');
			$('#lastChildAgeMonth').val('0');
			$("#para").prop('disabled',true);
			$("#boy").prop('disabled',true);
			$("#girl").prop('disabled',true);
			$("#lastChildAgeYear").prop('disabled',true);
			$("#lastChildAgeMonth").prop('disabled',true);
			removePregHistory();		
		}
		
		else
		{
			$("#para").prop('disabled',false);
			$("#boy").prop('disabled',false);
			$("#girl").prop('disabled',false);
			$("#lastChildAgeYear").prop('disabled',false);
			$("#lastChildAgeMonth").prop('disabled',false);
			$('#pregHistory').show();
		}
		
		$(this).val($(this).val().replace(/[^\d]/,''));		
		});
	/*	
	// To handle para>gravida case on key
	$(document).on("keyup", '#para, #gravida',function (){
		if($('#gravida').val()!='' && $('#para').val()!='' && $('#gravida').val()<= $('#para').val())  
		{	$('<div></div>').dialog({
				modal: true,
				title: "Something Went Wrong",
				width: 420,
				open: function () {
					$(this).html(greaterPara);
				},
				buttons: {
					Ok: function () {
						$(this).dialog("close");
						$('#para').val("");
						$('#para').focus();
					}      
				}
			});	
		}
	});
	*/
	
	$(document).on("keyup", '#boy',function (){
		$(this).val($(this).val().replace(/[^\d]/,''));		
	});
	
	$(document).on("keyup", '#girl',function (){
		$(this).val($(this).val().replace(/[^\d]/,''));		
	});
	
	$(document).on("keyup", '#lastChildAgeYear',function (){
		$(this).val($(this).val().replace(/[^\d]/,''));		
	});
	$(document).on("keyup", '#lastChildAgeMonth',function (){
		$(this).val($(this).val().replace(/[^\d]/,''));
	});
	
	$(document).on("keyup", '#hfoot',function (){
		$(this).val($(this).val().replace(/[^\d]/,''));
	});
	$(document).on("keyup", '#hinch',function (){
		$(this).val($(this).val().replace(/[^\d]/,''));
	});
	
	
	//EDD calculation based on LMP
	$(document).on("change", '#lmp',function (){
		
		var lmpDate = moment($('#lmp').val());
		
		//var eddDate = lmpDate.add({days:7,months:9}).format("DD MMM YYYY");
		var eddDate = lmpDate.add({days:280}).format("DD MMM YYYY");
		//$('#edd').combodate('setValue', eddDate, true);
		$('#edd').val(eddDate);
	});
	
	$(document).on("click", '#popupButton' ,function (){
		$.modal.close();
		$('#detailInfoWindow').hide();
		$('#detailList').empty();
	});
	
	$(document).on("click", '#popupButtonTT' ,function (){
		ttDate = "#tt" + $('#ttField').text() + "Date";
		
		type=$('input[name=ttDateRadio]:checked').val();
		
		if(type=="2"){
			addTTCSSWithoutDate($('#ttField').text());
		}
		else if(type=="1"){
			$(ttDate).html($('#ttDate').val());
			addTTCSSWithDate($('#ttField').text(), $('#ttDate').val());			 
		}		
		$('#ttInfo').hide();
		
		ttspan = "#tt" + (parseInt($('#ttField').text())+ 1) + "span";
		$(ttspan).show();
	});
	
	$(document).on("click", '#popupButtonTTClose' ,function (){	
		ttCheckBox = "#tt" + $('#ttField').text();
		$(ttCheckBox).prop('checked',false);
		$('#ttInfo').hide();
	});
	
	$(document).on("change", "input[name=ttDateRadio]", function(){
		type=$('input[name=ttDateRadio]:checked').val();
	
		if (type=="2")
			$("#ttDateInput").hide();
		else if (type=="1")
			$("#ttDateInput").show();
	});
	
	$(document).on("click", '#popupButtonDeath' ,function (){
		insertDeathInfo();
		if ($('#healthIdTitle').text()=="NRC ID: ")
		{
			$('#searchOption option[value=5]').prop('selected', true);
		}
		else {
			$('#searchOption option[value=1]').prop('selected', true);
			}
		$('#searchString').val($('#visibleHealthid').text());
		getAjaxClientInfo();
		$.modal.close();
		$('#deathInfo').hide();	

	});
	
	$(document).on("click", '#popupButtonDeathUpdate' ,function (){
		UpdateDeathInfo();
		
	});
	
	$(document).on("click", '#popupButtonDeathCancel' ,function (){
		$.modal.close();
		resetAllServiceTab();
		$('#deathInfo').hide();		
	});
	
	$(document).on("click", '#popupButtonSearch' ,function (){
		getAdvanceSearchResult();
	});
	
	$(document).on("click", '#popupButtonSearchCancel' ,function (){
		$('#searchOption option[value=1]').prop('selected', true);
		$('#searchString').val("");
		$('#searchResult').empty();
		$("#searchString").prop("disabled", false);
		$("#searchString").focus();
		$('#advanceSearch').hide();		
	});
	
	$(document).on("click", '#popupButtonPCScreening' ,function (){
		$('#PCMethod option[value=0]').prop('selected', true);
		$('#methodDetail').empty();
		for(var i = 1; i <= 12; i++){
			if($('input[name=q'+ i +']:checked').val()==1){
				$("select#PCMethod option[value='1']").remove();				
			}	
			break;
		}		
		$.modal.close();
		$('#pillCondomScreening').hide();
		
	});
	   
	//new button functionality in personal info section
	$(document).on("click", '#personalInfoNewButton' ,function (){
			$('<div></div>').dialog({
				modal: true,
				title: "Confirmation",
				width: 420,
				open: function () {
					$(this).html(newPreg);
				},
				buttons: {
					Yes: function () {
						
						if($('#hasDeliveryInfo').text()!="Yes")
							{resetClosePreg("new");

							}
						else
							addNewPreg();
						
						$(this).dialog("close");
					},
					No: function () {
						$(this).dialog("close");
					}      
					}
				});				
	});		
	
	//create Client information for MCH services
	$(document).on("click", '#personalInfoSaveButton' ,function (){
		insertUpdatePregInfo();	   		
	});
	
	//edit button functionality in personal info section
	$(document).on("click", '#personalInfoEditButton' ,function (){
		//$('#cellNo').val(ben_to_en_number_conversion($('#cellNo').val()));
		$('#para').val(ben_to_en_number_conversion($('#para').val()));
		$('#gravida').val(ben_to_en_number_conversion($('#gravida').val()));
		$('#boy').val(ben_to_en_number_conversion($('#boy').val()));
		$('#girl').val(ben_to_en_number_conversion($('#girl').val()));
		$('#lastChildAgeYear').val(ben_to_en_number_conversion($('#lastChildAgeYear').val()));
		$('#lastChildAgeMonth').val(ben_to_en_number_conversion($('#lastChildAgeMonth').val()));
		$('#hfoot').val(ben_to_en_number_conversion($('#hfoot').val()));
		$('#hinch').val(ben_to_en_number_conversion($('#hinch').val()));
		
		$('#personalInfo').find('*').prop('disabled',false);
		$('#personalInfo').find('input[type=checkbox]').prop( "disabled", false );
		disableTTCheckBox();
		
		//$('#anyAlermingHistoryContentSection').find('input[type=checkbox]').prop( "disabled", false );
		$('#personalInfoNewColumn').show();
		$('#personalInfoSaveColumn').hide();
	   	$('#personalInfoUpdateColumn').show();
	   	$('#personalInfoUpdateButton').prop('disabled',false);
	   	$("#edd").prop('disabled',true);
	   	$('#personalInfoEditColumn').hide();	   	
	});
	
	//update button functionality in personal info section
	$(document).on("click", '#personalInfoUpdateButton' ,function (){
		insertUpdatePregInfo();
	});
	
	//create ANC visit for MNCH services
	$(document).on("click", '#saveButtonANC' ,function (){
		insertANCInfo();    		
	});   

	//Delivery div form adjustment at the beginning
	
	//Insert Delivery information for MCH services
	$(document).on("click", "#saveButtonDelivery, #updateButtonDelivery" ,function (){
		insertUpdateDeliveryInfo('service');    		
	});
	
	$(document).on("click", '#editButtonDelivery' ,function (){
		$('#delivery').find('*').prop('disabled',false);
		
		$('#liveBirth').prop('disabled', true);
		$('#stillBirthFresh').prop('disabled', true);
		$('#stillBirthMacerated').prop('disabled', true);					
		$('#newbornBoy').prop('disabled', true);
		$('#newbornGirl').prop('disabled', true);
		$('#newbornUnidentified').prop('disabled', true);
		
		
		$('#dButtonUpdateColumn').show();
		$('#dButtonEditColumn').hide();
	});
	
	//Insert Delivery information for MCH services to encolse PregInfo
	$(document).on("click", '#closeButtonSave' ,function (){
		insertUpdateDeliveryInfo('close');  
		$('#personalDetail').css("background-color","#FFFFFF");
		$('#personalInfo').css("background-color","#FFFFFF");
	});
	
	$(document).on("click", '#shortPregSaveButton' ,function (){
		insertShortPregInfo();		
	});
	
	$(document).on("click", '#closeButtonCancel' ,function (){
		$('#closePreg').hide();		
	});
	
	$(document).on("click", '#shortPregCancelButton' ,function (){
		$('#shortPreg').hide();		
	});
	
	//Insert PNC Mother information for MCH services
	$(document).on("click", '#saveButtonPNCMother' ,function (){
		insertPNCMotherInfo();    		
	});

	//Insert PNC Child information for MCH services
	$(document).on("click", '#saveButtonPNCChild' ,function (){
		id =  $(this).closest('table').attr('id');
		id = id.split("-")[1];
		insertPNCChildInfo(id);    		
	});
	
	//Insert PAC information for MCH services
	$(document).on("click", '#saveButtonPAC' ,function (){
		insertPACInfo();    		
	});
	
	//Insert Pill, Condom information for FP
	$(document).on("click", '#saveButtonPC' ,function (){
		insertPillCondomInfo();    		
	});
	
	//Insert Injectable information for FP
	$(document).on("click", '#saveButtonWI' ,function (){
		insertInjectableInfo();    		
	});

	var inputs = $(':input').keypress(function(e){ 
		if (e.which == 13) {
		   e.preventDefault();
		   //focusNext(this);
		   var nextInput = inputs.get(inputs.index(this) + 1);
		   if (nextInput) {
			  nextInput.focus();
		   }
		}
	});		
	
	//handle enter key press as tab and move focus to next field
    /*$(document).keypress(function(event) {
    	if(event.which == 13){
    		event.preventDefault();
            $(this).nextAll('input:first').focus();
    		//alert('Handler for .keypress() called. - ' + $('body').index($(':input').filter(':focus')));
    	}
        
    });
	
	var inputs = $(':input').keypress(function(e){ 
		if (e.which == 13) {
		   e.preventDefault();
		   //focusNext(this);
		   var nextInput = inputs.get(inputs.index(this) + 1);
		   if (nextInput) {
			  nextInput.focus();
		   }
		}
	});	
	var inputs = $(':select').keypress(function(e){ 
		if (e.which == 13) {
		   e.preventDefault();
		   var nextInput = inputs.get(inputs.index(this) + 1);
		   if (nextInput) {
			  nextInput.focus();
		   }
		}
	});
	
	var inputsCheck = $(':checkbox').keypress(function(e){ 
		if (e.which == 13) {
		   e.preventDefault();
		   var nextInput = inputsCheck.get(inputsCheck.index(this) + 1);
		   if (nextInput) {
			  nextInput.focus();
		   }
		}
	});
	
	var inputsRadio = $(':radio').keypress(function(e){ 
		if (e.which == 13) {
		   e.preventDefault();
		   var nextInput = inputsRadio.get(inputsRadio.index(this) + 1);
		   if (nextInput) {
			  nextInput.focus();
		   }
		}
	});*/

	function initializeSpecificField(){
		//General fileds initialization
		$('#lmp').combodate({
    		format: "YYYY-MM-DD",
    		template: "DD MMM YYYY",
    		smartDays: true,
    		//maxYear: 2025,
    		value: moment()
    	});		
		
		//Delivery fields initialization
		$('#admissionDate').combodate({
    		format: "YYYY-MM-DD",
    		template: "DD MMM YYYY",
    		smartDays: true,
    		//maxYear: 2025,
    		value : moment()
    	});
    	$('#deliveryDate').combodate({
    		format: "YYYY-MM-DD",
    		template: "DD MMM YYYY",
    		smartDays: true,
    		//maxYear: 2025,
    		value : moment()
    	});
    	$('#closeDate').combodate({
    		format: "YYYY-MM-DD",
    		template: "DD MMM YYYY",
    		smartDays: true,
    		//maxYear: 2025,
    		value : moment()
    	});
    	    	
    	$('#shortPregLMPDate').combodate({
    		format: "YYYY-MM-DD",
    		template: "DD MMM YYYY",
    		smartDays: true,
    		//maxYear: 2025,
    		value : moment(moment().subtract({days:280}).format("YYYY-MM-DD"))
    	});
    	
    	$('#shortPregDeliveryDate').combodate({
			format: "YYYY-MM-DD",
			template: "DD MMM YYYY",
			smartDays: true,
			//maxYear: 2025,
			value : moment()
		});
    	
    	//delivery attendant designation dropdown
    	deliveryAttendantJS = JSON.parse(deliveryAttendant);
    	$('#deliveryDoneByDesignation').empty();
    	$('#closeDeliveryDoneByDesignation').empty();
    	/*output = "";
    	$.each(deliveryAttendantJS[0], function(key, val) {
    		output = output + "<option value = " + key + ">" + val + "</option>"; 			
		});*/							
    	$('#deliveryDoneByDesignation').append(populateDropdownList(deliveryAttendantJS[0]));
    	$('#closeDeliveryDoneByDesignation').append(populateDropdownList(deliveryAttendantJS[0]));
    	//delivery treatment dropdown
    	treatmentJS = JSON.parse(treatment);
		$('#dTreatment').empty();
		$('#dTreatment').append(populateDropdownList(treatmentJS[0]));
    	    	
    	//delivery advice dropdown
    	adviceDeliveryJS = JSON.parse(adviceDelivery);
		$('#dAdvice').empty();   
		$('#dAdvice').append(populateDropdownList(adviceDeliveryJS[0]));
    	    	
    	//delivery place and refer center dropdown
    	referCenterJS = JSON.parse(referCenter);
    	$('#deliveryPlaceCenter').empty();
    	$('#dReferCenter').empty();
    	$('#closeDeliveryPlaceCenter').empty();
    	$('#deliveryPlaceCenter').append(populateDropdownList(referCenterJS[0]));
		$('#dReferCenter').append(populateDropdownList(referCenterJS[0]));
		$('#closeDeliveryPlaceCenter').append(populateDropdownList(referCenterJS[0]));
		//delivery refer reason dropdown
		referReasonDeliveryJS = JSON.parse(referReasonDelivery);
		$('#dReferReason').empty();    								
    	$('#dReferReason').append(populateDropdownList(referReasonDeliveryJS[0]));
    	
		//NewBorn fields initialization
    	referReasonNewbornJS = JSON.parse(referReasonNewborn);
		for(i=1;i<=5;i++){
			referCenterId = "#newBornReferCenter" + i;
			$(referCenterId).empty();	    	
			$(referCenterId).append(populateDropdownList(referCenterJS[0]));
			
			referReasonId = "#newBornReferReason" + i;
			$(referReasonId).empty();
			$(referReasonId).append(populateDropdownList(referReasonNewbornJS[0]));
			
			multiSelectDropDownInit(referReasonId);			
		}
		
    	//ttDate field initialization
    	$('#ttDate').combodate({
    		format: "YYYY-MM-DD",
    		template: "DD MMM YYYY",
    		smartDays: true,
    		value : moment()
    	});
 
    	//deathDate field initialization
    	deathReasonJS = JSON.parse(deathReason);
    	$('#deathReason').empty();
    	$('#deathReason').append(populateSpecificDropdownList(generalDeathReason,deathReasonJS[0]));
    	
    	$('#deathDate').combodate({
    		format: "YYYY-MM-DD",
    		template: "DD MMM YYYY",
    		smartDays: true,
    		value : moment()
    	});

    	fieldId = ["#dTreatment","#dAdvice","#dReferReason"];
    	$.each(fieldId, function(key, val) {
    		multiSelectDropDownInit(val); 			
    	});   	
	}
	
	/*function populateDropdownList(jsonVal){
		
		output = "";		
    	$.each(jsonVal, function(key, val) {
    		output = output + "<option value = " + key + ">" + val + "</option>"; 			
		});		
    	return output;
	}*/
	
	function multiSelectDropDownInit(fieldId){
		$(fieldId).multipleSelect({
			width : 350,
			position : 'top',
			selectAll: false,			
			selectedList : 1,
			styler : function() {
				return 'font-size: 18px;';
			}		
		});
	}
	
	//mouseover for tooltip 
	$(document).on('mouseover', '.tp', function(e) {			
	    document.getElementById($(e.target).attr('id')).title = get_tooltip($(e.target).attr('id'));	    
	    $(document).tooltip();
	});
	
	// make sure tooltip is closed when the mouse is gone
	$(document).on('mouseleave', '.tp', function(){	    
	    $(document).tooltip('close');
	});	
	
	 //retrieve clientinfo from advanced search
	$(document).on("click", ".searchButton, .searchLink",function (){
		$id = $(this).attr('id').split("_");	
		$.modal.close();
		if($id[1]==1){
			$('#searchOption option[value=1]').prop('selected', true);
		}
		else{
			$('#searchOption option[value=5]').prop('selected', true);
		}
		$('#searchString').val($id[0]);
		getAjaxClientInfo();
		$('#searchResult').empty();
		$("#searchString").prop("disabled", false);
		$('#advanceSearch').hide();
	 });	
	
	//functionality for closing PregInfo
	
	$(document).on("change", "#closePlace" ,function (){
		if ($(this).val()!='2')
			$("#closeFacility").hide();
		else
			$("#closeFacility").show();
	});	
					
	$(document).on("change", "input[name=closeType]" ,function (){
		closePregType();
	});

	$(document).on("click", '#deleteLastButtonANC', function(event){deleteLast('anc',ancProviderId,'')});
	$(document).on("click", '#deleteLastButtonPNCMother', function(event){deleteLast('pncM',pncMProviderId,'')});
	$(document).on("click", '#deleteLastButtonPAC', function(event){deleteLast('pac',pacProviderId,'')});
	
	$(document).on("click", '#deleteLastButtonPNCChild', function(event){
		id =  $(this).closest('table').attr('id');
		childNo = id.split("-")[1];
		deleteLast('pncC',pncCProviderId[childNo-1],childNo);
	});
});