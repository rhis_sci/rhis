iudTypeList = '[{"0":"","1":"কপার-টি ৩৮০"}]';
iudTypeJS = JSON.parse(iudTypeList);

iudAttendantDesignationList = '[{"0":"","1":"পরিবার কল্যাণ পরিদর্শিকা","2":"সাব-অ্যাসিস্ট্যান্ট কমিউনিটি মেডিকেল অফিসার","3":"মেডিকেল অফিসার (এমসিএইচ-এফপি)","4":"মেডিকেল অফিসার (ক্লিনিক)"}]';
iudAttendantDesignationJS = JSON.parse(iudAttendantDesignationList);

//treatmentIUDList = '[{"1":"প্যারাসিটামল ৫০০ মি.গ্রা. ১টি/২টি ৮ ঘন্টা পর পর ভরা পেটে","2":"আইবুপ্রোফেন ৪০০ মি.গ্রা. ১২ ঘন্টা পর পর ভরা পেটে","3":"ফেরাস সালফেট ৩০০ মি.গ্রা. ১-২ মাস","4":"ডক্সিসাইক্লিন ১০০ মি.গ্রা. ২ ঘন্টা পর পর"}]';
treatmentIUDList = '[{"1":"প্যারাসিটামল ৫০০ মি.গ্রা.","2":"আইবুপ্রোফেন ৪০০ মি.গ্রা.","3":"ফেরাস সালফেট ৩০০ মি.গ্রা.","4":"ডক্সিসাইক্লিন ১০০ মি.গ্রা."}]';
treatmentIUDJS = JSON.parse(treatmentIUDList);

function getIUDFieldValue(){
	
	var iudObj = new Object();
	
	iudObj.healthId = $('#healthid').text();
	iudObj.providerId = $('#providerid').text();
	iudObj.iudCount = $('#IUDiudCount').text();
	iudObj.mobileNo = $('#cellNo').val();
	iudObj.LMP = $('#IUDLMP').val();
	iudObj.iudType = $('#IUDiudType').val();
	iudObj.iudImplantDate = $('#IUDiudImplantDate').val();
	iudObj.companionName = $('#IUDcompanionName').val().toUpperCase();
	
	var address = "";
	if($('#IUDcompanionAddressZilla').val()!='none' && $('#IUDcompanionAddressZilla').val()!=null){
		address += $('#IUDcompanionAddressZilla').val().split("_")[1] + "," + $('#IUDcompanionAddressZilla').val().split("_")[0];
	}
	if($('#IUDcompanionAddressUpazila').val()!='none' && $('#IUDcompanionAddressUpazila').val()!=null){
		address += "," + $('#IUDcompanionAddressUpazila').val();
	}
	if($('#IUDcompanionAddressUnion').val()!='none' && $('#IUDcompanionAddressUnion').val()!=null){
		address += "," + $('#IUDcompanionAddressUnion').val();
	}
	if($('#IUDcompanionAddressVillage').val()!='none' && $('#IUDcompanionAddressVillage').val()!=null){
		address += "," + $('#IUDcompanionAddressVillage').val().split("_")[1] + "," + $('#IUDcompanionAddressVillage').val().split("_")[0];
	}	
	iudObj.companionAddress = address; // input need to be manipulated
	
	iudObj.companionAllowance = $('#IUDcompanionAllowance').prop('checked')? 1 : 2;//$('#IUDcompanionAllowance').val();
	iudObj.clientAllowance = $('#IUDclientAllowance').prop('checked')? 1 : 2;//$('#IUDclientAllowance').val();
	iudObj.attendantName = $('#IUDattendantName').val().toUpperCase();
	iudObj.attendantDesignation = $('#IUDattendantDesignation').val();
	iudObj.attendantAllowance = $('#IUDattendantAllowance').prop('checked')? 1 : 2;//$('#IUDattendantAllowance').val();
	iudObj.providerAsAttendant = $('#IUDproviderAsAttendant').is(":checked")?"1":"2";
	iudObj.iudAfterDelivery = $('#IUDiudAfterDelivery').is(":checked")?"1":"2";
	
	iudObj.treatment = $('#IUDtreatment').multipleSelect("getSelects");
	
	iudObj.serviceSource = "";
	iudObj.sateliteCenterName = "";
	iudObj.client = "1";
	
	return iudObj;
}

function setIUDFieldValue(response){
	
	$('#IUDLMP').combodate('setValue', response.LMP==""?"":response.LMP, true);
	$('#IUDiudAfterDelivery').prop( "checked", (response.iudAfterDelivery==1?true:false));
	$('#IUDiudType option[value=' + response.iudType + ']').prop('selected', true);
	$('#IUDiudImplantDate').combodate('setValue', response.iudImplantDate==""?"":response.iudImplantDate, true);
	$('#IUDcompanionName').val(response.companionName);
	
	var address = response.companionAddress.split(",");
	
	if(address.length >= 2){
		//var zillaDropdown = (address[1].length < 2 ?"0"+ address[1]:address[1]) + "_" + (address[0].length < 2 ?"0"+ address[0]:address[0]);
		var zillaDropdown = address[1] + "_" + address[0];
		$('#IUDcompanionAddressZilla option[value=' + zillaDropdown + ']').prop('selected', true);
		loadUpazila("#IUDcompanionAddressZilla","#IUDcompanionAddressUpazila");
		
		if(address.length >= 3){
			//var upazilaDropdown = (address[2].length < 2 ?"0"+ address[2]:address[2]);
			var upazilaDropdown = address[2];
			$('#IUDcompanionAddressUpazila option[value=' + upazilaDropdown + ']').prop('selected', true);
			loadUnion("#IUDcompanionAddressZilla","#IUDcompanionAddressUpazila","#IUDcompanionAddressUnion");
			
			if(address.length >= 4){
				//var unionDropdown = (address[3].length < 2 ?"0"+ address[3]:address[3]);
				var unionDropdown = address[3];
				$('#IUDcompanionAddressUnion option[value=' + unionDropdown + ']').prop('selected', true);
				loadVillage("#IUDcompanionAddressZilla","#IUDcompanionAddressUpazila","#IUDcompanionAddressUnion","#IUDcompanionAddressVillage");
				
				if(address.length >= 6){
					//var villageDropdown = (address[5].length < 2 ?"0"+ address[5]:address[5]) + "_" + (address[4].length < 3 ?(address[4].length <2 ? "00"+ address[4]:"0"+ address[4]):address[4]);
					var villageDropdown = address[5] + "_" + address[4];
					$('#IUDcompanionAddressVillage option[value=' + villageDropdown + ']').prop('selected', true);
				}
			}
		}
	}
		
	//$('#IUDcompanionAllowance').val(en_to_ben_number_conversion(response.companionAllowance.toString()));
	if(response.companionAllowance=="1"){
		$( "#IUDcompanionAllowance" ).prop( "checked", true );			
	}
	else{
		$( "#IUDcompanionAllowance" ).prop( "checked", false );
	}
	
	//$('#IUDclientAllowance').val(en_to_ben_number_conversion(response.clientAllowance.toString()));
	if(response.clientAllowance=="1"){
		$( "#IUDclientAllowance" ).prop( "checked", true );			
	}
	else{
		$( "#IUDclientAllowance" ).prop( "checked", false );
	}
	$('#IUDproviderAsAttendant').prop( "checked", (response.providerAsAttendant==1?true:false));
	$('#IUDattendantName').val(response.attendantName);
	$('#IUDattendantDesignation option[value=' + response.attendantDesignation + ']').prop('selected', true);
	
	//$('#IUDattendantAllowance').val(en_to_ben_number_conversion(response.attendantAllowance.toString()));
	if(response.attendantAllowance=="1"){
		$( "#IUDattendantAllowance" ).prop( "checked", true );			
	}
	else{
		$( "#IUDattendantAllowance" ).prop( "checked", false );
	}
	
	setMultiSelectDropdown('#IUDtreatment',response.treatment)	
}

/**
 * saving iud's information
 */

function insertIUDInfo() {

	var iudObj = new Object();
	iudObj = setCommonParam();

	iudObj = getIUDFieldValue();
	iudObj.iudLoad = "insert";
	
	confirmationBoxIUD(iudObj);
}

/**
 * retrieve iud information
 */
function retrieveIUDInfo() {
	
	var iudObj = new Object();
	iudObj = setCommonParam();

	iudObj.healthId = $('#healthid').text();
	iudObj.iudLoad = "retrieve";
	
	iudInfo(iudObj);	
}

/**
 * update iud information
 */
function updateIUDInfo(){
	 
	var iudObj = new Object();
	iudObj = setCommonParam();
	
	iudObj = getIUDFieldValue();
	iudObj.iudCount = $('#IUDiudCount').text();
	
	iudObj.iudLoad = "update";
	
	confirmationBoxIUD(iudObj);	
}


/**
 * insert and retrieve iud information in a single AJAX call
 */
function iudInfo(iudObj) {
	
	if (iudObj.healthId != "") {
		$.ajax({
			type : "POST",
			url : "iud",
			timeout:60000, //60 seconds timeout
			dataType : "json",
			data : {"iudInfo" : JSON.stringify(iudObj)},
			success : function(response) {
				parseIUDInfo(response);									
			},
			error : function(xhr, status, error) {
				alert(status);
				alert(error);
			}
		});		
	}	
}

/**
 * populating iud register's table dynamically
 */
function parseIUDInfo(response) {
		
	if(response.regSerialNo!="" && response.regDate!=""){
		$("#regNumber").text(en_to_ben_number_conversion((response.regSerialNo<=9 ? ("0" + response.regSerialNo): response.regSerialNo) + "/" + moment(response.regDate).format("YY")).toString());
		$("#regDate").text(en_to_ben_number_conversion(moment(response.regDate).format("DD/MM/YYYY").toString()));
	}
	else{
		$("#regNumber").text("");
		$("#regDate").text("");
	}
	
	$('#content').empty();
	
	var divIUD = $("<div id='iud' class='person_ANCdetail'>");
	
	var headerContent = $("<br><span id='IUDiudCount' hidden='true'>" + response.iudCount + "</span>");
	divIUD.append(headerContent);
	divIUD.append(createIUDBody(response));

	divIUD.append("</div>");
	$('#content').append(divIUD);

	fieldsInit();
	hideIUDButtons();
	
	if(response.hasOwnProperty("iudRetrieve")){
		if(response.iudRetrieve==1){
			setIUDFieldValue(response);
			$('#iud').find('*').prop('disabled',true);
			$('#iudInfoEditSpan').show();
			$('#iudInfoNewSpan').show();
			$('#iudInfoEditButton').prop('disabled',false);
		    $('#iudInfoNewButton').prop('disabled',false);
		    
		    parseIUDFollowupInfo(response);
		    $('#IUDFollowupiudRemoveDateColumn :input').prop('disabled', true);
		}	
		else{			
			$('#iudInfoSaveSpan').show();
			$('#iudInfoSaveButton').prop('disabled',false);
		}
	}
	
	/*if(response.hasOwnProperty("iudInsertSuccess")){
		if(response.iudInsertSuccess==1){
			$('#iud').find('*').prop('disabled',true);
			$('#iudInfoEditSpan').show();
			$('#iudInfoNewSpan').show();
			$('#iudInfoEditButton').prop('disabled',false);
		    $('#iudInfoNewButton').prop('disabled',false);	
		}	
		else{
			$('#iudInfoSaveSpan').show();
			$('#iudInfoSaveButton').prop('disabled',false);
		}
	}
	
	if(response.hasOwnProperty("iudUpdateSuccess")){
		if(response.iudUpdateSuccess==1){
			$('#iud').find('*').prop('disabled',true);
			$('#iudInfoEditSpan').show();
			$('#iudInfoNewSpan').show();
			$('#iudInfoEditButton').prop('disabled',false);
		    $('#iudInfoNewButton').prop('disabled',false);	
		}	
		else{
			$('#iudInfoUpdateSpan').show();
			$('#iudInfoNewSpan').show();
			$('#iudInfoUpdateButton').prop('disabled',false);
		    $('#iudInfoNewButton').prop('disabled',false);
		}
	}*/
}

function createIUDBody(response){
	
	var IUDBodyContent = $("<table id='IUDBody' width='100%'><tbody>");
	
	var IUDBodyContentRow1 = $("<tr>");
	var IUDBodyContentCol1 = $("<td>শেষ মাসিকের তারিখ </td>");
	var IUDBodyContentCol2 = $("<td><input type='text' id='IUDLMP' maxlength='11' size='11'/></td>");
	var IUDBodyContentCol3 = $("<td> <input type='checkbox' id='IUDiudAfterDelivery'/> &nbsp;প্রসব পরবর্তী আইইউডি </td>");
	var IUDBodyContentCol4 = $("<td>জীবিত সন্তান সংখ্যা: ছেলে&nbsp;<input type='text' id='IUDBoy' maxlength='2' size='2' value='" + en_to_ben_number_conversion(response.son.toString()) + "' disabled='true'/>");
	IUDBodyContentCol4.append("&nbsp;মেয়ে&nbsp;<input type='text' id='IUDGirl' maxlength='2' size='2' value='" + en_to_ben_number_conversion(response.dau.toString()) + "' disabled='true'/></td>");
	
	IUDBodyContentRow1.append(IUDBodyContentCol1);
	IUDBodyContentRow1.append(IUDBodyContentCol2);
	IUDBodyContentRow1.append(IUDBodyContentCol3);
	IUDBodyContentRow1.append(IUDBodyContentCol4);
	IUDBodyContentRow1.append("</tr>");
	
	IUDBodyContent.append(IUDBodyContentRow1);
	IUDBodyContent.append("<tr><td colspan='4'></td></tr>");
	
	var IUDBodyContentRow2 = $("<tr>");
	IUDBodyContentCol1 = $("<td>আইইউডি'র প্রকার  </td>");
	IUDBodyContentCol2 = $("<td> <select id='IUDiudType'>" + populateDropdownList(iudTypeJS[0]) + "</select></td>");
	IUDBodyContentCol3 = $("<td colspan='2'>আইইউডি প্রয়োগের তারিখ  <input type='text' id='IUDiudImplantDate' maxlength='11' size='11'/></td>");	
	
	IUDBodyContentRow2.append(IUDBodyContentCol1);
	IUDBodyContentRow2.append(IUDBodyContentCol2);
	IUDBodyContentRow2.append(IUDBodyContentCol3);
	IUDBodyContentRow2.append("</tr>");
		
	IUDBodyContent.append(IUDBodyContentRow2);
	IUDBodyContent.append("<tr><td colspan='4'></td></tr>");
	
	var IUDBodyContentRow3 = $("<tr>");
	
	IUDBodyContentCol1 = $("<td>আনয়নকারীর নাম &nbsp;</td>");
	IUDBodyContentCol2 = $("<td><input type='text' id='IUDcompanionName' size='11'/></td>");
	IUDBodyContentCol3 = $("<td colspan='2'>আনয়নকারীর ঠিকানা: "
						+ "জেলা <select id='IUDcompanionAddressZilla' class='iud_selectwidth'>" + loadZilla("#IUDcompanionAddressZilla") + "</select> &nbsp;"
						+ "উপজেলা <select id='IUDcompanionAddressUpazila' class='iud_selectwidth'></select>&nbsp;"
						+ "ইউনিয়ন <select id='IUDcompanionAddressUnion' class='iud_selectwidth'></select> &nbsp;"
						+ "গ্রাম  <select id='IUDcompanionAddressVillage'  class='iud_selectwidth'></select></td>");	

	IUDBodyContentRow3.append(IUDBodyContentCol1);
	IUDBodyContentRow3.append(IUDBodyContentCol2);
	IUDBodyContentRow3.append(IUDBodyContentCol3);
	IUDBodyContentRow3.append("</tr>");
	
	IUDBodyContent.append(IUDBodyContentRow3);
	IUDBodyContent.append("<tr><td colspan='4'></td></tr>");
	
	var IUDBodyContentRow4 = $("<tr>");
	//IUDBodyContentCol1 = $("<td>আনয়নকারী ভাতার পরিমাণ</td>");
	//IUDBodyContentCol2 = $("<td><input type='text' id='IUDcompanionAllowance' maxlength='3' size='3'/> টাকা</td>");
	//IUDBodyContentCol3 = $("<td colspan='2'>গ্রহীতার যাতায়াত ভাতার পরিমাণ <input type='text' id='IUDclientAllowance' value='' maxlength='3' size='3'/> টাকা</td>");
	IUDBodyContentCol1 = $("<td colspan='2'> <input type='checkbox' id='IUDcompanionAllowance'/> &nbsp;আনয়নকারী ভাতা দেয়া হয়েছে</td>");
	IUDBodyContentCol2 = $("<td colspan='2'> <input type='checkbox' id='IUDclientAllowance'/> &nbsp;গ্রহীতার যাতায়াত ভাতা দেয়া হয়েছে</td>");
			
	IUDBodyContentRow4.append(IUDBodyContentCol1);
	IUDBodyContentRow4.append(IUDBodyContentCol2);
	//IUDBodyContentRow4.append(IUDBodyContentCol3);
	IUDBodyContentRow4.append("</tr>");
		
	IUDBodyContent.append(IUDBodyContentRow4);
	IUDBodyContent.append("<tr><td colspan='4'></td></tr>");
	
	var IUDBodyContentRow5 = $("<tr>");
	IUDBodyContentCol1 = $("<td>চিকিৎসা </td>");
	IUDBodyContentCol2 = $("<td colspan='3'><select id='IUDtreatment' multiple='multiple'>" + populateDropdownList(treatmentIUDJS[0]) + "</select></td>");
			 
	IUDBodyContentRow5.append(IUDBodyContentCol1);
	IUDBodyContentRow5.append(IUDBodyContentCol2);
	IUDBodyContentRow5.append("</tr>");
	
	IUDBodyContent.append(IUDBodyContentRow5);
	IUDBodyContent.append("<tr><td colspan='4'></td></tr>");
	
	var IUDBodyContentRow6 = $("<tr>");
	IUDBodyContentCol1 = $("<td colspan='4'><input type='checkbox' id='IUDproviderAsAttendant'/> আমি  প্রয়োগকারী </td>");
	
	IUDBodyContentRow6.append(IUDBodyContentCol1);
	IUDBodyContentRow6.append("</tr>");
		
	IUDBodyContent.append(IUDBodyContentRow6);
	IUDBodyContent.append("<tr><td colspan='4'></td></tr>");
	
	var IUDBodyContentRow7 = $("<tr>");
	
	IUDBodyContentCol1 = $("<td>প্রয়োগকারীর নাম </td>");
	IUDBodyContentCol2 = $("<td><input type='text' id='IUDattendantName' size='11'/></td>");
	IUDBodyContentCol3 = $("<td>প্রয়োগকারীর পদবী <select id='IUDattendantDesignation' style='width:150px;'>" + populateDropdownList(iudAttendantDesignationJS[0]) + "</select></td>");
	//IUDBodyContentCol4 = $("<td>প্রয়োগকারীর ভাতার পরিমাণ &nbsp;<input type='text' id='IUDattendantAllowance' maxlength='3' size='3'/> টাকা</td>");
	IUDBodyContentCol4 = $("<td> <input type='checkbox' id='IUDattendantAllowance'/> &nbsp;প্রয়োগকারীর ভাতা দেয়া হয়েছে</td>");
		
	IUDBodyContentRow7.append(IUDBodyContentCol1);
	IUDBodyContentRow7.append(IUDBodyContentCol2);
	IUDBodyContentRow7.append(IUDBodyContentCol3);
	IUDBodyContentRow7.append(IUDBodyContentCol4);
	IUDBodyContentRow7.append("</tr>");
		
	IUDBodyContent.append(IUDBodyContentRow7);
	IUDBodyContent.append("<tr><td colspan='4'></td></tr>");
	
	var IUDBodyContentRow8 = $("<tr>");
	
	IUDBodyContentCol1 = $("<td colspan='4'>"
						+ "<span id='iudInfoEditSpan' class='right'><input type='button' id='iudInfoEditButton' width='25px' height='15px' value='Edit'></span>"
						+ "<span id='iudInfoUpdateSpan' class='right'><input type='button' id='iudInfoUpdateButton' width='25px' height='15px' value='Update'></span>"
						+ "<span id='iudInfoSaveSpan' class='right'><input type='button' id='iudInfoSaveButton' width='25px' height='15px' value='Save'></span>"
						+ "<span id='iudInfoNewSpan' class='right'><input type='button' id='iudInfoNewButton' width='25px' height='15px' value='New IUD Information'></span></td>");
			
	IUDBodyContentRow8.append(IUDBodyContentCol1);
	IUDBodyContentRow8.append("</tr>");
			
	IUDBodyContent.append(IUDBodyContentRow8);
	IUDBodyContent.append("<tr><td colspan='4'></td></tr>");
		
	IUDBodyContent.append("</tbody></table>");
	
	return IUDBodyContent;
}

$(document).on("change", '#IUDcompanionAddressZilla' ,function (){
	
	loadUpazila("#IUDcompanionAddressZilla","#IUDcompanionAddressUpazila");	
});

$(document).on("change", '#IUDcompanionAddressUpazila' ,function (){
	
	loadUnion("#IUDcompanionAddressZilla","#IUDcompanionAddressUpazila","#IUDcompanionAddressUnion");	
});

$(document).on("change", '#IUDcompanionAddressUnion' ,function (){
	
	loadVillage("#IUDcompanionAddressZilla","#IUDcompanionAddressUpazila","#IUDcompanionAddressUnion","#IUDcompanionAddressVillage");
});

function hideIUDButtons(){
	$('#iudInfoEditSpan').hide();
	$('#iudInfoUpdateSpan').hide();
	$('#iudInfoSaveSpan').hide();
	$('#iudInfoNewSpan').hide();
}

function fieldsInit(){
	
	$('#IUDLMP,#IUDiudImplantDate').combodate({
		format : "YYYY-MM-DD",
		template : "DD MMM YYYY",
		smartDays : true,
		value : moment()
	});
	
	fieldId = ["#IUDtreatment"];
	$.each(fieldId, function(key, val) {
		$(val).multipleSelect({
			width : 400,
			position : 'top',
			selectAll: false,
			selectedList: 1,
			maxHeight: 100,
			styler : function() {
				return 'font-size: 18px;';
			}	
		});
	});
}

//click event for New button
$(document).on("click",'#iudInfoNewButton', function(){
	var response = new Object();
	response.son = $('#IUDBoy').val();
	response.dau = $('#IUDGirl').val();
	
	$('#IUDBody').remove();
	$('#iudfollowup').remove();
	
	$('#iud').append(createIUDBody(response));
	fieldsInit();
		
	hideIUDButtons();
	$('#iudInfoSaveSpan').show();
});

//click event for Edit button
$(document).on("click",'#iudInfoEditButton', function(){
	$('#iudfollowup').remove();
	
	//$('#IUDcompanionAllowance').val(ben_to_en_number_conversion($('#IUDcompanionAllowance').val()));
	//$('#IUDclientAllowance').val(ben_to_en_number_conversion($('#IUDclientAllowance').val()));
	//$('#IUDattendantAllowance').val(ben_to_en_number_conversion($('#IUDattendantAllowance').val()));
	
	$('#iud').find('*').prop('disabled',false);
	
	$('#IUDBoy').prop('disabled',true);
	$('#IUDGirl').prop('disabled',true);
	
	hideIUDButtons();
	$('#iudInfoUpdateSpan').show();
	$('#iudInfoNewSpan').show();
});

//click event for Update button
$(document).on("click",'#iudInfoUpdateButton', function(){
	$('#iudfollowup').remove();
	updateIUDInfo();
});

//click event for Save button
$(document).on("click",'#iudInfoSaveButton', function(){
	insertIUDInfo();
});

// Only allowing numeric values for input fields and convert that to Bangla
$(document).on("keyup", '#IUDcompanionAllowance,#IUDclientAllowance,#IUDattendantAllowance', function() {
	$(this).val($(this).val().replace(/[^\d]/, ''));
});

$(document).on("click", '#IUDproviderAsAttendant',function (){
	if ($('#IUDproviderAsAttendant').is(":checked")){
		$('#IUDattendantName').val($('#providerName').text());
		$('#IUDattendantDesignation option[value=1]').prop('selected', true);
		$('#IUDattendantName').prop('disabled', true);
		$('#IUDattendantDesignation').prop('disabled', true);
	}
	else{
		$('#IUDattendantName').val("");
		$('#IUDattendantDesignation option[value=0]').prop('selected', true);
		$('#IUDattendantName').prop('disabled', false);
		$('#IUDattendantDesignation').prop('disabled', false);
	}
});

function confirmationBoxIUD(iudObj){
	
	var userVal = "";
	userVal = userVal + "<table class='list'>";
	userVal = userVal + "<tr><td>শেষ মাসিকের তারিখ: " + moment(iudObj.LMP).format("DD MMM YYYY") + " </td></tr>";
	userVal = userVal + "<tr><td>প্রসব পরবর্তী আইইউডি: " + (iudObj.iudAfterDelivery==1?"হ্যাঁ" : "না") + " </td></tr>";
	userVal = userVal + "<tr><td>আইইউডি'র প্রকার: " + iudTypeJS[0][iudObj.iudType]+"</td></tr>";
	userVal = userVal + "<tr><td>আইইউডি প্রয়োগের তারিখ: " + moment(iudObj.iudImplantDate).format("DD MMM YYYY") + " </td></tr>";
	userVal = userVal + "<tr><td>আনয়নকারীর নাম: " + iudObj.companionName + " </td></tr>";
	userVal = userVal + "<tr><td>আনয়নকারীর ঠিকানা: </td></tr>";
	userVal = userVal + "<tr><td>জেলা: " + $('#IUDcompanionAddressZilla').find('option:selected').text() + " "
					  +	"উপজেলা: " + $('#IUDcompanionAddressUpazila').find('option:selected').text() + " "
					  +	"ইউনিয়ন: " + $('#IUDcompanionAddressUnion').find('option:selected').text() + " "
					  +	"গ্রাম: " + $('#IUDcompanionAddressVillage').find('option:selected').text() + " "
					  + "</td></tr>";
	//userVal = userVal + "<tr><td>আনয়নকারী ভাতার পরিমাণ: " + iudObj.companionAllowance + " টাকা</td></tr>";
	//userVal = userVal + "<tr><td>গ্রহীতার যাতায়াত ভাতার পরিমাণ: " + iudObj.clientAllowance + " টাকা</td></tr>";
	
	userVal = userVal + "<tr><td>আনয়নকারী ভাতা দেয়া হয়েছে: " + (iudObj.companionAllowance==1?"হ্যাঁ":"না") + "</td></tr>";
	userVal = userVal + "<tr><td>গ্রহীতার যাতায়াত ভাতা দেয়া হয়েছে: " + (iudObj.clientAllowance==1?"হ্যাঁ":"না") + "</td></tr>";
	userVal = userVal + "</table>";
	
	userVal = userVal + "<table class='list'><tr><td>চিকিৎসা:</td></tr> ";
	textVal=iudObj.treatment;
	
	var i;
	var convertedText = "  ";
	
	for(i =0; i < textVal.length; i++){
		convertedText = convertedText +"<tr><td>-"+treatmentIUDJS[0][textVal[i]] +"</td></tr>";				
	}
	userVal+=convertedText;
	
	userVal = userVal + "<tr><td>প্রয়োগকারীর নাম: " + iudObj.attendantName + " </td></tr>";
	userVal = userVal + "<tr><td>প্রয়োগকারীর পদবী: " + iudAttendantDesignationJS[0][iudObj.attendantDesignation] + " </td></tr>";
	//userVal = userVal + "<tr><td>প্রয়োগকারীর ভাতার পরিমাণ: " + iudObj.attendantAllowance + " টাকা</td></tr>";
	userVal = userVal + "<tr><td>প্রয়োগকারীর ভাতা দেয়া হয়েছে: " + (iudObj.attendantAllowance==1?"হ্যাঁ":"না") + "</td></tr>";
	
	userVal = userVal + "</table>";
	
    $('<div></div>').dialog({
        modal: true,
        width: 450,
        hide: {effect: 'fade', duration: 500},
        show: {effect: 'fade', duration: 600},
        title: "নিশ্চিত করুন",
        open: function () {
            $(this).html(userVal);
        },
        buttons: {
        	 "Save" : function () {      
        		 iudInfo(iudObj);
             	$(this).dialog("close");
             },
             "Cancel": function () {
        		$(this).dialog("close");
        	}
        }
    });
}