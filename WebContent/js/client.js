var riskyPreg =false;
var deliveryDate="";

//getting the client information
	function getAjaxClientInfo(){

		var clientObj = new Object();
		clientObj = setCommonParam();
		clientObj.sOpt = $('#searchOption').val();
		clientObj.sStr = $('#searchString').val();
		clientObj.providerid = $('#providerid').text();
		clientObj.requestToGetAllInfo = 'true';
		//setCommonParam(clientObj);
		
		$.ajax({
               type: "POST",
               url:"client",
               timeout:60000, //60 seconds timeout
               dataType: "json",
               data:{"sClient": JSON.stringify(clientObj)},
               success: function (response) {
            	   parseClientInfo(response);
               },
               complete: function() {
            	   preventMultiC = 0;            	   
               },
               error: function(xhr, status, error) {
    				alert(status);
    				alert(error);
               }
		});		
	}
	
	function parseClientInfo(response){
			
		if(response.False=='false'){     //if Info not valid
		   callDialog(notValid);
      	   $('#searchString').val("");
      	   $('#healthid').html('');
      	   $('#pregNumber').html('');
      	   $('#searchString').focus();
      	   $("#regNumber").text("");
      	   $("#regDate").text("");
      	   $("div#personal").hide();
      	   $("div#register_personal").hide();
    	   }
 	   else{
		   	   var previousHealthId = $('#healthid').text();         //common part 1 		   
	 		   document.getElementById('healthid').innerHTML=response.cHealthID;
	 		   document.getElementById('healthIdTitle').innerHTML=(/*$('#searchOption').val()*/response.idType== 5 ? "NRC ID: " : "Health ID: "); 
	 		   document.getElementById('visibleHealthid').innerHTML=response.cVisibleID;
	 		   document.getElementById('gender').innerHTML=response.cSex;
	 		   document.getElementById('deathStatus').innerHTML=response.deathStatus;
	 		   document.getElementById('hasDeliveryInfo').innerHTML=response.hasDeliveryInformation;
	 		   document.getElementById('hasAbortionInfo').innerHTML=response.hasAbortionInformation;
	 		   
	 		   $("#clientName").val(response.cName);    		                 		  
			   $("#clientAge").val(en_to_ben_number_conversion(response.cAge));
			   var gender = "";
			   switch(response.cSex){
			   		case "1":
			   			gender = "পুরুষ";
			   		break;
			   		case "2":
			   			gender = "মহিলা";
			   		break;
			   		case "3":
			   			gender = "হিজরা";
			   		break;
			   }
			   $("#clientGender").val(gender); 
			   
			   $("#clientHusbandName").val(response.cHusbandName);
			   $("#zillaName").val(response.cDist);
			   
			   $("#upaZilaName").val(response.cUpz);
			   $("#unionName").val(response.cUnion);
			   $("#villageName").val(response.cVill);
			   $("#wardNo").val("");
			   
			   $("#unitNo").val("");
			   $("#hhNo").val(response.chGRHNo);
			   $("#elcoNo").val(response.cElcoNo);
			   $("#cellNo").val(response.cMobileNo==""?"":"0" + Number(response.cMobileNo).toString());//response.cMobileNo.replace(/^0+/, ''));
			   //$("#cellNo").val(response.cMobileNo==""?"":"0" + response.cMobileNo);
			   //duplicate. need this information even though client is dead 
			   if(response.cNewMCHClient=="false"){
				   
				   document.getElementById('pregNumber').innerHTML=response.cPregNo;
			   }
			
			   if(response.deathStatus=='1'){ // part for men & non-eligable women											
				   toggleServices("dead");			   
				   callDialog(dead);			   
				}
			   
			   else if(response.cSex!='2' || response.cAge < 15 || response.cAge > 49){	// part for men & non-eligible women											
				   toggleServices("notElco");					
				}
			   
				else{  			//part for eligable women
					
					 	if($("#hasDeliveryInfo").text()=="Yes" && $("#hasAbortionInfo").text()=="No"){	// part pac non-eligible women											
						   toggleServices("pacDisable");					
						}
					 
					   else if($("#hasDeliveryInfo").text()=="Yes" && $("#hasAbortionInfo").text()=="Yes"){	// part pac eligible women											
						   toggleServices("pacEnable");					
						}
					 
					   else /*if ($("#hasDeliveryInfo").text()=="No" && $("#hasAbortionInfo").text()=="No")*/{
						   toggleServices("elco");
					   }
					 
					   riskyPreg =false;
					   
					   if(response.cAge < 18 || response.cAge > 35)
					   {
						   riskyPreg =true;
					   }
					  
				   
				   /**
					* regNo part
				   */
				   //alert((response.regSerialNo<=9 ? ("0" + response.regSerialNo): response.regSerialNo) + "/" + moment(response.regDate).format("YY") + " " + moment(response.regDate).format("DD/MM/YYYY"));
				   if(response.regSerialNo!="" && response.regDate!=""){
					   $("#regNumber").text(en_to_ben_number_conversion((response.regSerialNo<=9 ? ("0" + response.regSerialNo): response.regSerialNo) + "/" + moment(response.regDate).format("YY")).toString());
					   $("#regDate").text(en_to_ben_number_conversion(moment(response.regDate).format("DD/MM/YYYY").toString()));
					   
					   $("#regNoMNCH").html($("#regNumber").text());
					   $("#regDateMNCH").html($("#regDate").text());					   
				   }
				   else{
					   $("#regNumber").text("");
					   $("#regDate").text("");
					   $("#regNoMNCH").text("");
					   $("#regDateMNCH").text("");
				   }
									   
				  
				   
				   for(i=1;i<=5;i++){
					   responseKey1 = "cTT" + i;
					   responseKey2 = "cTT" + i + "Date";
					   tt =  "#tt" + i;
					   ttspan = "#tt" + i + "span";
					   
					   $(ttspan).hide();
					   
					   if(response[responseKey1]!=""){
						   $(tt).prop("checked", true);
						   $(tt).prop("disabled", true);
						   if(response[responseKey2]==""){
							   addTTCSSWithoutDate(i);
						   }
						   else{
							   addTTCSSWithDate(i, response[responseKey2]);
						   }
						}
						else{
							removeTTCSS(i);
							$(tt).prop("checked", false);
							$(tt).prop("disabled", false);
						}
				   }
				   
				   //hiding TT checkboxes except immediate after checked one
				   for(i=1;i<=5;i++){
					   
					   tt =  "#tt" + i;
					   ttspan = "#tt" + i + "span";
					   
					   if($(tt).prop("checked")){
						   $(ttspan).show();
					   }
					   else{
						   $(ttspan).show();
						   break;
					   }
				   }
																			
				   if(response.cNewMCHClient=="false"){
					   
					   document.getElementById('pregNumber').innerHTML=response.cPregNo;
					   $('#lmp').combodate('setValue', response.cLMP, true);
					   $('#lmp').combodate('setValue', response.cLMP, true); //work around to set date field in case duplicate request
					   //LMP date disagreement between FWA and FWV
					   /*if(response.cLMP != response.cTempLMP){
					   
						   tempLMP = moment(response.cTempLMP).format("DD MMM YYYY");								   
						   $('#lmpDisagreement').html("এখানে ক্লিক করুন! <span id='updateLMP' hidden='true'> " + tempLMP + " </span>");
						   $('#lmpDisagreement').show();
						}
					   else{
						   $('#lmpDisagreement').html("");
						   $('#lmpDisagreement').hide();
						}*/
					   
					   $('#edd').val(moment(response.cEDD).format("DD MMM YYYY"));
					   $("#para").val(en_to_ben_number_conversion(response.cPara.toString()));
					   $("#gravida").val(en_to_ben_number_conversion(response.cGravida.toString()));
					   if(response.cGravida == 1 || response.cGravida > 3){
						   riskyPreg = true;
						}
					   $("#boy").val(en_to_ben_number_conversion(response.cBoy.toString()));
					   $("#girl").val(en_to_ben_number_conversion(response.cGirl.toString()));
						   
					   //converting months to year and month
					   var age = response.cLastChildAge;
					   var year = parseInt(age/12);
					   var month = parseInt(age%12);
						
					   $("#lastChildAgeYear").val(en_to_ben_number_conversion(year.toString()));
					   $("#lastChildAgeMonth").val(en_to_ben_number_conversion(month.toString()));
							
					   //converting inch to feet and inch
					   var h = response.cHeight;
					   var f = parseInt(h/12);
					   var i = parseInt(h%12);
					   if(response.cHeight < 58){
						   riskyPreg = true;
					   }
						   
					   $("#hfoot").val(en_to_ben_number_conversion(f.toString()));
					   $("#hinch").val(en_to_ben_number_conversion(i.toString()));
								
					   $('#bloodGroup option[value="' + response.cBloodGroup + '"]').prop('selected', true);
					   
						if(response.cHistoryComplicatedContent != ""){
						  var historyContent = response.cHistoryComplicatedContent.split(",");
						  for(i = 0; i < historyContent.length ; i++){
							 switch (historyContent[i]) { 
								case "1": 
									$('#bloodPreg').prop("checked", true);
									riskyPreg = true;
									break;
								case "2":
									$('#delayedDeliveryPreg').prop("checked", true);	
									riskyPreg = true;
									break;
								case "3":
									$('#blockedDeliveryPreg').prop("checked", true);
									riskyPreg = true;
									break;
								case "4":
									$('#uterusPreg').prop("checked", true);
									riskyPreg = true;
									break;
								case "5":
									$('#stillBirthPreg').prop("checked", true);
									riskyPreg = true;
									break;
								case "6":
									$('#newbornDeathPreg').prop("checked", true);
									riskyPreg = true;
									break;
								case "7":
									$('#edemaPreg').prop("checked", true);		             			 		
									break;
								case "8":
									$('#eclampsiaPreg').prop("checked", true);		             			 		
									break;
								case "9":
									$('#csectionPreg').prop("checked", true);
									riskyPreg = true;
									break;
							 }
						  }
						}
					   
					    $('#personalInfoEditColumn').show();
					    $('#personalInfoNewColumn').show();
					    $('#personalInfoSaveColumn').hide();
					    $('#personalInfoUpdateColumn').hide();
					    if(response.cGravida == 1){
					    	$('#pregHistory').hide();
					    }
					    else{
					    	$('#pregHistory').show();
					    }
					    $('#personalInfo').find('*').prop('disabled',true);
						
					   if(response.cSex!='2'){
						   riskyPreg==false;
					    }
					   
					   if(response.highRiskPreg=="Yes"){
						   riskyPreg==true;
					   }
											   					   
					    $('#lmpDisagreement').prop('disabled',false);
					    $('#personalInfoEditButton').prop('disabled',false);
					    $('#personalInfoNewButton').prop('disabled',false);		         	   

					    //$("div#personalReg").show();
				   }
				   
				    else{								
					   $('#personalInfoEditColumn').hide();
					   $('#personalInfoUpdateColumn').hide();
					   $('#personalInfoNewColumn').hide();
					   $('#personalInfoSaveColumn').show();
					   $('#personalInfo').find('*').prop('disabled',false);
					   
					   $('#pregNumber').html("");
					   $('#lmp').combodate('setValue', moment(),true);
					   eddDate = moment().add({days:280}).format("DD MMM YYYY");
											   
					   $('#lmpDisagreement').html("");
					   $('#lmpDisagreement').hide();
					   $('#personalInfo').find('input[type=text]').val("");
					   $('#personalInfo').find('input[type=select]').prop( "disabled", false );
					   //$('#personalInfo').find('input[type=checkbox]').prop( "disabled", false );
					   $('#pregHistory').find('input[type=checkbox]').prop( "disabled", false );
					   $('#pregHistory').find('input[type=checkbox]').prop( "checked", false );
					   disableTTCheckBox();
					   
					   $('#bloodPreg').prop("checked", false);
					   $('#delayedDeliveryPreg').prop("checked", false);
					   $('#blockedDeliveryPreg').prop("checked", false);
					   $('#uterusPreg').prop("checked", false);
					   $('#stillBirthPreg').prop("checked", false);
					   $('#newbornDeathPreg').prop("checked", false);        			 		
					   $('#edemaPreg').prop("checked", false);
					   $('#eclampsiaPreg').prop("checked", false);
					   $('#csectionPreg').prop("checked", false);
					 
					   $('#personalInfo').find('input[type=radio]').prop( "checked", false );           			  
					   $('#bloodGroup option[value=0]').prop('selected', true);
					   $("#edd").val(eddDate);
					   $('#edd').prop('disabled',true);           			              			  
				    }
				   
				   $('#personalDetail').css("background-color","#FFFFFF");
				   $('#personalInfo').css("background-color","#FFFFFF");
				   
			    }
		    
				$("div#register_personal").hide();			//common part 2
				$("div#personalReg").hide();
				$("div#personal").show();					
				$("div#personalInfo").hide();
 	   }				   

	}	
	
	//saving client information
	function insertUpdatePregInfo(){
	
	if($('#para').val()!='' && $('#gravida').val()!='' && $('#lastChildAgeYear').val()!='' && $('#lastChildAgeMonth').val()!='')
		{	
			var clientPregObj = new Object();
			clientPregObj = setCommonParam();
			
			riskyPreg =false;
       	   				
			clientPregObj.pregNo = $('#pregNumber').text()!=""?$('#pregNumber').text():"";
			clientPregObj.healthId = $('#healthid').text();
			clientPregObj.providerId = $('#providerid').text();
			clientPregObj.houseGRHoldingNo = $('#hhNo').val();
			clientPregObj.mobileNo = $('#cellNo').val();
			//clientPregObj.mobileNo = ben_to_en_number_conversion($('#cellNo').val());
			
			clientPregObj.lmp = $('#lmp').val()!="" ? $('#lmp').val():moment().format("YYYY-MM-DD");
			clientPregObj.edd = $('#edd').val()!="" ? $('#edd').val():moment().format("YYYY-MM-DD");
			clientPregObj.sateliteCenterName = "";
			clientPregObj.client = "1";
				
			//update operation conversion
			if(clientPregObj.pregNo!=""){
				clientPregObj.para = $('#para').val();
				clientPregObj.gravida = $('#gravida').val();
				clientPregObj.boy = $('#boy').val();
				clientPregObj.girl = $('#girl').val();
				var year = $('#lastChildAgeYear').val();
				if(year==""){
					year = "0";
				}			
				var month = $('#lastChildAgeMonth').val();
				if(month==""){
					month = "0";
				}
				
				var foot = $('#hfoot').val();
				if(foot==""){
					foot = "0";
				}
				var inch =  $('#hinch').val();
				if(inch==""){
					inch = "0";
				}
				
			}
			else{
				clientPregObj.para = $('#para').val();
				clientPregObj.gravida = $('#gravida').val();
				clientPregObj.boy = $('#boy').val();
				clientPregObj.girl = $('#girl').val();
				var year = $('#lastChildAgeYear').val();
				if(year==""){
					year = "0";
				}			
				var month = $('#lastChildAgeMonth').val();
				if(month==""){
					month = "0";
				}
				
				var foot = $('#hfoot').val();
				if(foot==""){
					foot = "0";
				}
				var inch =  $('#hinch').val();
				if(inch==""){
					inch = "0";
				}
			}
			
			clientPregObj.lastChildAge = parseInt(year*12) + parseInt(month);
			clientPregObj.height = parseInt(foot*12) + parseInt(inch);
			
			clientPregObj.bloodGroup = $('#bloodGroup').val();
			
			var tt1 = $('#tt1').prop('checked');
			var tt2 = $('#tt2').prop('checked');
			var tt3 = $('#tt3').prop('checked');
			var tt4 = $('#tt4').prop('checked');
			var tt5 = $('#tt5').prop('checked');
			for(i=1; i<=5; i++){
				tt = "#tt" + i;
				ttVal = "tt" + i;
				ttDate = "ttDate" + i;
				if(!$(tt).prop('disabled')){
					clientPregObj[ttVal] = ($(tt).prop('checked')? i : "");
					clientPregObj[ttDate] = ($(tt + "Date").text()!="" ? $(tt + "Date").text() : "" );				
				}
				else{
					clientPregObj[ttVal] = "";
					clientPregObj[ttDate] = "";
				}
			}
			
			//clientPregObj.complicatedHistory = $('input[name=anyAlermingHistory]:checked').val();
			clientPregObj.complicatedHistory = ""; //this field is not required anymore. 
				
			var bloodPregVal = ($('#bloodPreg').prop('checked')? (1 + ",") : "");
			var delayedDeliveryPregVal = ($('#delayedDeliveryPreg').prop('checked')? (2 + ",") : "");
			var blockedDeliveryPregVal = ($('#blockedDeliveryPreg').prop('checked')? (3 + ",") : "");
			var uterusPregVal = ($('#uterusPreg').prop('checked')? (4 + ",") : "");
			var stillBirthPregVal = ($('#stillBirthPreg').prop('checked')? (5 + ",") : "");
			var newbornDeathPregVal = ($('#newbornDeathPreg').prop('checked')? (6 + ",") : "");
			var edemaPregVal = ($('#edemaPreg').prop('checked')? (7 + ",") : "");
			var eclampsiaPregVal = ($('#eclampsiaPreg').prop('checked')? (8 + ",") : "");
			var csectionPregVal = ($('#csectionPreg').prop('checked')? (9 + ",") : "");
			
			
			clientPregObj.complicatedHistoryNote = bloodPregVal + delayedDeliveryPregVal + blockedDeliveryPregVal + uterusPregVal + stillBirthPregVal
												   + newbornDeathPregVal + edemaPregVal + eclampsiaPregVal + csectionPregVal;
			
			clientPregObj.complicatedHistoryNote = clientPregObj.complicatedHistoryNote.slice(0,-1);
									
			/*if(clientPregObj.complicatedHistory==null){
				clientPregObj.complicatedHistory = 2;
			}
			
			if(clientPregObj.complicatedHistory=="2"){
				clientPregObj.complicatedHistoryNote = "";
			}
			else{
				clientPregObj.complicatedHistoryNote = $('#anyAlermingHistoryContent').multipleSelect("getSelects");
			}*/
			
			
			var userVal = "<table class=confirmation>";	
						
	        userVal = userVal + "<tr><td>শেষ মাসিকের তারিখ:" + moment(clientPregObj.lmp).format("DD MMM YYYY")+"</td>";
	        userVal = userVal + "<td>প্রসবের সম্ভাব্য তারিখ:" + moment($('#edd').val()).format("DD MMM YYYY")+"</td>";
	        userVal = userVal + "<td>"+linemark("প্যারা",clientPregObj.para)+"&nbsp&nbsp&nbsp";
	        userVal = userVal + linemark("গ্রাভিডা",clientPregObj.gravida)+"</td></tr>";
	        userVal = userVal + "<tr><td>জীবিত সন্তান সংখ্যা:&nbspছেলে:"+clientPregObj.boy+"&nbsp&nbsp"+"মেয়ে:"+clientPregObj.girl+"</td>";
	        userVal = userVal + "<td>শেষ সন্তানের বয়স: "+year+" বছর "+month+" মাস "+"</td>";
	        userVal = userVal + (foot=="0" && inch=="0" ? "<span style=\"color: red;\">" : "<span style=\"color: black;\">") + "<td>উচ্চতা: </span>&nbsp"+foot+" ফুট &nbsp&nbsp"+ inch	+""+ " ইঞ্চি</td></tr>";   
	        //userVal = userVal + "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspউচ্চতা:"+linemark_compare("ফুট",foot,0)+"&nbsp&nbsp"+linemark_compare("ইঞ্চি",inch,0);
	        
	        switch(clientPregObj.bloodGroup){
	        	case "0":
	        		value = "";
	        		break;
	        	case "1":
	        		value = "A+";
	        		break;
	        	case "2":
	        		value = "A-";
	        		break;
	        	case "3":
	        		value = "B+";
	        		break;
	        	case "4":
	        		value = "B-";
	        		break;
	        	case "5":
	        		value = "AB+";
	        		break;
	        	case "6":
	        		value = "AB-";
	        		break;
	        	case "7":
	        		value = "O+";
	        		break;
	        	case "8":
	        		value = "O-";
	        		break;
	        	
	        }
	        userVal = userVal + "<tr><td>"+linemark_compare("রক্তের গ্রুপ",value,"none")+"</td>";
	        userVal = userVal + "<td>টিটি টিকা:&nbsp&nbsp";
	        userVal = userVal + (tt1=="1" ? "১: হ্যাঁ" : "")+"&nbsp&nbsp"+(tt2=="1" ? "২: হ্যাঁ" : "")+"&nbsp&nbsp"+(tt3=="1" ? "৩: হ্যাঁ" : "")+"&nbsp&nbsp"+(tt4=="1" ? "৪: হ্যাঁ" : "")+"&nbsp&nbsp"+(tt5=="1" ? "৫: হ্যাঁ" : "")+"</td></tr>"; 
	        userVal = userVal + "<tr><td>"+linemark("বাড়ি / জিআর / হোল্ডিং নম্বর",clientPregObj.houseGRHoldingNo)+"</td>";
			userVal = userVal + "<td>"+linemark("মোবাইল নম্বর",clientPregObj.mobileNo)+"</td></tr></table>";
	    	userVal = userVal + "<table id='list'><tr><td>পূর্ববর্তী গর্ভের ইতিহাস: </td></tr>";
	        
	    	textVal=clientPregObj.complicatedHistoryNote;
	    	convertedText = "  ";   
	    	for(i =0; i < textVal.length; i++){
	    	
	    		switch(textVal[i]){
					case "1":
						convertedText = convertedText + "<tr><td>-রক্তপাত (প্রসবপূর্ব অথবা প্রসবোত্তর)</td></tr>";
						break;
					case "2":
						convertedText = convertedText + "<tr><td>-দীর্ঘয়িত প্রসব</td></tr>";
						break;
					case "3":
						convertedText = convertedText + "<tr><td>-বাধাপ্রাপ্ত প্রসব</td></tr>";
						break;
					case "4":
						convertedText = convertedText + "<tr><td>-জরায়ুর ভিতর গর্ভফুল থেকে যাওয়া </td></tr>";
						break;							
					case "5":
						convertedText = convertedText + "<tr><td>-মৃত সন্তান প্রসব করা</td></tr>";
						break;
					case "6":
						convertedText = convertedText + "<tr><td>-৪৮ ঘন্টার মধ্যে নবজাতক মরে যাওয়া</td></tr>";
						break;
					case "7":
						convertedText = convertedText + "<tr><td>-পা বা সমস্ত শরীর বেশি রকম ফোলা (ইডিমা)</td></tr>";
						break;
					case "8":
						convertedText = convertedText + "<tr><td>-একল্যাম্পসিয়া বা খিঁচুনিসহ বার বার অজ্ঞান হওয়া</td></tr>";
						break;
					case "9":
						convertedText = convertedText + "<tr><td>-সিজারিয়ান অপারেশন বা যন্ত্রের মাধ্যমে (ফরসেফ/ভেকুয়াম) প্রসব</td></tr></table>";
						break;
	    			}
	    	}
	       userVal+=convertedText+"</table>";
	       //alert(userVal);
       
	       // To handle para>gravida case before save
	       if(clientPregObj.gravida<clientPregObj.para)  {	
	    	   callDialog(greaterPara);	      
			}
	       else
	    	{   
	      
				$('<div></div>').dialog({
			        modal: true,
			        title: "Confirmation (নিশ্চিত করুন) ",
			        width: 720,
			        hide: {effect: 'fade', duration: 500},
			        show: {effect: 'fade', duration: 600},
			        open: function () {
			            $(this).html(userVal);
			        },
			        buttons: {
			        	Save: function () {    
			        		 $.ajax({
			        		        type: "POST",
			        		        url:"handlepregwomen",
			        		        timeout:60000, //60 seconds timeout
			        		        dataType: "json",
			        		        data:{"pregWomen": JSON.stringify(clientPregObj)}, 
			        		        success: function (response) {
			        		       	   	//alert("তথ্য সফলভাবে সংরক্ষিত হয়েছে!");
			        		        	riskyPreg = false;
			        		        	if(response!=""){
			        		       	   		document.getElementById('pregNumber').innerHTML=response.pregNo;
			        		       	   		if(response.responseType=="insert"){
			        		       	   			document.getElementById('hasDeliveryInfo').innerHTML=response.hasDeliveryInformation;
			        		       	   			if(response.highRiskPreg=="Yes"){
					        					   riskyPreg==true;
			        		       	   			}
			        		       	   		}
			        		       	   	}
			        		       	 	if(clientPregObj.gravida == 1 || clientPregObj.gravida > 3){
			        		       	   		riskyPreg = true;
			        		       	   	}
			        					
			        					if($("#clientAge").val() < 18 || $("#clientAge").val() > 35)
			        					{
			        						riskyPreg =true;
			        					}
			        					
			        					if(clientPregObj.height < 58){
			        						riskyPreg = true;
			        					}
			        					complicatedHistoryNote = clientPregObj.complicatedHistoryNote.split(",");
			        					for(k=0;k<complicatedHistoryNote.length;k++){
			        						if(complicatedHistoryNote[k]!=7 && complicatedHistoryNote[k]!=8 && complicatedHistoryNote[k]!=""){
			        							riskyPreg = true;
			        						}
			        					}
			        					if(riskyPreg==true){
				   						  //#D39C8A #B7123C
				        		       		 $('#personalInfo').css("background-color","#D39C8A");
				        		       		$('#personalDetail').css("background-color","#D39C8A");//#D39C8A #B7123C
				        		       	 }
				        		       	 else{
					   						  $('#personalDetail').css("background-color","#FFFFFF");
					   						  $('#personalInfo').css("background-color","#FFFFFF");
				        		       	 }
				        		       	highlightRisk(riskyPreg);
			        		       	   				        		       	   				        		           	   	
			        		           	$("#regNumber").text(en_to_ben_number_conversion((response.regSerialNo<=9 ? ("0" + response.regSerialNo): response.regSerialNo) + "/" + moment(response.regDate).format("YY")).toString());
			        		           	$("#regDate").text(en_to_ben_number_conversion(moment(response.regDate).format("DD/MM/YYYY").toString()));
			        		            	   	
			        		        	$('#personalInfoSaveColumn').hide();
			        		        	$('#personalInfoUpdateColumn').hide();
			        		        	
			        		        	$('#cellNo').val();
			        		        	$('#para').val(en_to_ben_number_conversion($('#para').val()));
			        		    		$('#gravida').val(en_to_ben_number_conversion($('#gravida').val()));
			        		    		$('#boy').val(en_to_ben_number_conversion($('#boy').val()));
			        		    		$('#girl').val(en_to_ben_number_conversion($('#girl').val()));
			        		    		$('#lastChildAgeYear').val(en_to_ben_number_conversion($('#lastChildAgeYear').val()));
			        		    		$('#lastChildAgeMonth').val(en_to_ben_number_conversion($('#lastChildAgeMonth').val()));
			        		    		$('#hfoot').val(en_to_ben_number_conversion($('#hfoot').val()));
			        		    		$('#hinch').val(en_to_ben_number_conversion($('#hinch').val()));
			        		        	
			        		        	$('#personalInfo').find('*').prop('disabled',true);
			        		        	$('#personalInfoEditButton').prop('disabled',false);
			        		        	$('#personalInfoNewButton').prop('disabled',false);
			        		        	$('#personalInfoEditColumn').show();
			        		        	$('#personalInfoNewColumn').show();
			        		        	
			        		        	if(openTab!=""){   // To jump back to ANC/Delivery/PNC Client with no PregInfo
				        		        	goToId='#'+openTab+'.serviceTabButtonSelected';	
				        		        	$(goToId).click();
				        		        	openTab="";
			        		        	}	        		        
			        		        },
			        		       
			        		        error: function(xhr, status, error) {
			        		        	alert(status);
			        					alert(error);
				        }           
			        			}); 
			            	$(this).dialog("close");
			            },
			            Cancel: function () {
			        		$(this).dialog("close");
			        	}
			        	}
					});	    
			    	}
				}
	
	else{	
		callDialog(fill);
		}
}
	
	function insertShortPregInfo(){
		
		var clientPregObj = new Object();
		clientPregObj = setCommonParam();
		
		riskyPreg =false;
	   		
		clientPregObj.pregNo = $('#pregNumber').text()!=""?$('#pregNumber').text():"";
		clientPregObj.healthId = $('#healthid').text();
		clientPregObj.providerId = $('#providerid').text();
		clientPregObj.houseGRHoldingNo = $('#hhNo').val();
		clientPregObj.mobileNo = $('#cellNo').val();
		
		//lmp back calculation
		/*
		var tempDate = moment($('#shortPregDeliveryDate').val());	
		var lmpDate = tempDate.subtract({days:280}).format("YYYY-MM-DD");
		*/
		deliveryDate=moment($('#shortPregDeliveryDate').val()).format("YYYY-MM-DD");
		
		
		clientPregObj.lmp = moment($('#shortPregLMPDate').val()).format("YYYY-MM-DD");	
		clientPregObj.edd = moment(clientPregObj.lmp).add({days:280}).format("YYYY-MM-DD");
		
		clientPregObj.sateliteCenterName = "";
		clientPregObj.client = "1";
		
		clientPregObj.para = "";
		clientPregObj.gravida ="";
		clientPregObj.boy ="";
		clientPregObj.girl ="";
		clientPregObj.lastChildAge = "";
	
		var foot = $('#shortPregHfoot').val();
		if(foot==""){
			foot = "0";
		}
		var inch =  $('#shortPregHinch').val();
		if(inch==""){
			inch = "0";
		}
		
		clientPregObj.height = parseInt(foot*12) + parseInt(inch);
		
		var shortPregBloodGroup= $('#shortPregBloodGroup').val();
		clientPregObj.bloodGroup = "";//shortPregBloodGroup;
		
		for(i=1; i<=5; i++){	
			ttVal = "tt" + i;
			ttDate = "ttDate" + i;
				clientPregObj[ttVal] = "";
				clientPregObj[ttDate] = "";		
		}
		
		clientPregObj.complicatedHistory = 2;
		clientPregObj.complicatedHistoryNote = "";
		
		if(clientPregObj.height < 58){
			riskyPreg = true;
		}
		
		 $.ajax({
		        type: "POST",
		        url:"handlepregwomen",
		        timeout:60000, //60 seconds timeout
		        dataType: "json",
		        data:{"pregWomen": JSON.stringify(clientPregObj)}, 
		        success: function (response) {
		       	   	//alert("তথ্য সফলভাবে সংরক্ষিত হয়েছে!");
		       	   	if(response!=""){
		       	   		document.getElementById('pregNumber').innerHTML=response.pregNo;
		       	   		if(response.responseType=="insert"){
		       	   			document.getElementById('hasDeliveryInfo').innerHTML=response.hasDeliveryInformation;
		       	   			if(response.highRiskPreg=="Yes"){
	    					   riskyPreg==true;
		       	   			}
		       	   		}
		       	   	}
		       	   	
		       	   	if(clientPregObj.gravida == 1 || clientPregObj.gravida > 3){
		       	   		riskyPreg = true;
		       	   	}
					
					if($("#clientAge").val() < 18 || $("#clientAge").val() > 35)
					{
						riskyPreg =true;
					}
					
					if(clientPregObj.height < 58){
						riskyPreg = true;
					}
					/*complicatedHistoryNote = clientPregObj.complicatedHistoryNote.split(",");
					for(k=0;k<complicatedHistoryNote.length;k++){
						if(complicatedHistoryNote[k]!=7 && complicatedHistoryNote[k]!=8 && complicatedHistoryNote[k]!=""){
							riskyPreg = true;
						}
					}*/
		       	   	
			       	 if(riskyPreg==true){					
			       		 $('#personalInfo').css("background-color","#D39C8A");
			       		 $('#personalDetail').css("background-color","#D39C8A");
			       	 }
			       	 else{				
						  $('#personalInfo').css("background-color","#FFFFFF");
						  $('#personalDetail').css("background-color","#FFFFFF");
			       	 }
		       	   				        		       	   				        		           	   	
		           	$("#regNumber").text(en_to_ben_number_conversion((response.regSerialNo<=9 ? ("0" + response.regSerialNo): response.regSerialNo) + "/" + moment(response.regDate).format("YY")).toString());
		           	$("#regDate").text(en_to_ben_number_conversion(moment(response.regDate).format("DD/MM/YYYY").toString()));
		            	   	
		        	$('#personalInfoSaveColumn').hide();
		        	$('#personalInfoUpdateColumn').hide();
		        	
		        	//$('#lmp').combodate('setValue', lmpDate, true);clientPregObj.lmp
					//$('#edd').val(moment(deliveryDate).format("DD MMM YYYY"));
		        	$('#lmp').combodate('setValue', clientPregObj.lmp, true);
		        	$('#edd').val(moment(clientPregObj.edd).format("DD MMM YYYY"));
		    		$('#hfoot').val(en_to_ben_number_conversion($('#shortPregHfoot').val()));
		    		$('#hinch').val(en_to_ben_number_conversion($('#shortPregHinch').val()));
		        	
		    		$('#bloodGroup option[value="' + shortPregBloodGroup + '"]').prop('selected', true);
		        	$('#personalInfo').find('*').prop('disabled',true);
		        	$('#personalInfoEditButton').prop('disabled',false);
		        	$('#personalInfoNewButton').prop('disabled',false);
		        	$('#personalInfoEditColumn').show();
		        	$('#personalInfoNewColumn').show();		        
		        	$('#shortPreg').hide();
		        	
		        	openTab='2';
		        	goToId='#'+openTab+'.serviceTabButton';	
		        	$(goToId).click();		        	
		        	openTab="";  
		        },
		        error: function(xhr, status, error) {
		        	alert(status);
					alert(error);
		        }           
			}); 	            	
	}
	
	$(document).on("click", '#lmpDisagreement' ,function (){
		var set = confirm("LMP date provided by FWA: " + $('#updateLMP').text() + "\n\nDo you want to set this date?");
	    if (set == true) {
	    	lmpDate = moment($('#updateLMP').text(), "DD MMM YYYY");
	    	$('#lmp').combodate('setValue', moment(lmpDate).format("YYYY-MM-DD"), true);
	    	$('#lmp').combodate('setValue', moment(lmpDate).format("YYYY-MM-DD"), true); //work around for js issue
	    	$('#lmpDisagreement').hide();
	    } 
	});
	
	$(document).on("click", '#tt1' ,function (){
		if($('#tt1').prop('checked')){
			$('#ttField').html("1");
			$('#ttInfo').show();
		}
		else{
			removeTTCSS(1);
			$('#tt1Date').html("");
			removeTTInfo(2);
		}
	});
	$(document).on("click", '#tt2' ,function (){
		if($('#tt2').prop('checked')){
			$('#ttField').html("2");
			$('#ttInfo').show();
		}
		else{
			removeTTCSS(2);
			$('#tt2Date').html("");
			removeTTInfo(3);
		}
	});
	$(document).on("click", '#tt3' ,function (){
		if($('#tt3').prop('checked')){
			$('#ttField').html("3");
			$('#ttInfo').show();
		}
		else{
			removeTTCSS(3);
			$('#tt3Date').html("");
			removeTTInfo(4);
		}
	});
	$(document).on("click", '#tt4' ,function (){
		if($('#tt4').prop('checked')){
			$('#ttField').html("4");
			$('#ttInfo').show();
		}
		else{
			removeTTCSS(4);
			$('#tt4Date').html("");
			removeTTInfo(5);
		}
	});
	$(document).on("click", '#tt5' ,function (){
		if($('#tt5').prop('checked')){
			$('#ttField').html("5");
			$('#ttInfo').show();
		}
		else{
			removeTTCSS(5);
			$('#tt5Date').html("");
		}
	});
	
	function addTTCSSWithDate(id, date){
		ttspan = "#tt" + id + "span";
		
		$(ttspan).addClass('selected_with_date');
		$(ttspan).tooltip({
			items: 'span.selected_with_date',
			content: moment(date).format("DD MMM YYYY")
		}); 
	}
	
	function addTTCSSWithoutDate(id){
		ttspan = "#tt" + id + "span";
		
		$(ttspan).addClass('selected_without_date');
		$(ttspan).tooltip({
			items: 'span.selected_without_date',
			content: 'No date'
		});	
	}
	
	function removeTTCSS(id){
		ttspan = "#tt" + id + "span";
		
		$(ttspan).removeClass('selected_with_date');
   		$(ttspan).removeClass('selected_without_date');
   		
	}
	
	function removeTTInfo(id){
		for(i=id; i <=5; i++){
			tt = "#tt" + i;
			ttdate = "#tt" + i + "Date";
			ttspan = "#tt" + i + "span";
			
			removeTTCSS(i);
			$(tt).prop("checked", false);
			$(ttDate).html("");
			$(ttspan).hide();
		}   		
	}

	function disableTTCheckBox(){
		for(i=1; i<=5; i++ ){
			tt = "#tt" + i;
			if($(tt).prop('checked')){
				$(tt).prop('disabled',true);				
			}
			else{
				$(tt).prop('disabled',false);			
			}			
		}
	}
	
// To enable/disable ReferCenter and ReferReason on check/uncheck of refer 
	function handleOnclickCheckbox(refer,center,reason,type,span){
		
		if ($(refer).is(":checked")){		
			if(type=="enable"){
				$(center).prop('disabled', false);
				$(reason).multipleSelect("enable");
			}						
			else if(type=="show"){
				$(span).show();
			}
		}			
		else{
			$(center + " option[value='0']").prop('selected', true);
			$(reason).multipleSelect("uncheckAll");
			
			if(type=="enable"){
				$(reason).multipleSelect("disable");
				$(center).prop('disabled', true);
			}
			else if(type=="show"){
				$(span).hide();
			}
		}	
	}
	
	$(document).on("change", '#shortPregDeliveryDate' ,function (){
		var deliveryDate = moment($('#shortPregDeliveryDate').val());
		var lmpDate = moment(deliveryDate).subtract({days:280}).format("YYYY-MM-DD");
		$('#shortPregLMPDate').combodate('setValue', moment(lmpDate), true);		
	});
	
	
	$(document).on("change", '#searchOption' ,function (){
		$('#tableAdvanceSearchOption').hide();
		if($('#searchOption').val()=="6"){	
			$('#SclientName').val("");
			$('#SzillaName').html("");
			$('#SupaZilaName').html("");
			$('#SunionName').html("");
			$('#SvillageName').html("");	
			
			loadZilla("#SzillaName");				
			
			$('#AdvanceSearchoption option[value=2]').prop('selected', true);
			$('#AdvanceSearchnameAddressFields').show();
			$('#AdvanceSearchmobileFields').hide();
			$('#searchString').prop('disabled', true);
			$('#subheadingAdvanceSearch').html("Advance Search");
			$('#advanceSearch').show();
		}
		else if($('#searchOption').val()=="2"){	
			$('#SclientName').val("");
			$('#SzillaName').html("");
			$('#SupaZilaName').html("");
			$('#SunionName').html("");
			$('#SvillageName').html("");	
			$('#SmobileNumber').html("");
			
			loadZilla("#SzillaName");				
			
			$('#AdvanceSearchoption option[value=1]').prop('selected', true);
			$('#AdvanceSearchnameAddressFields').hide();
			$('#AdvanceSearchmobileFields').show();
			$("#searchString").prop("disabled", true);
			$('#subheadingAdvanceSearch').html("Mobile Search");
			$('#advanceSearch').show();
		}
		else{
			$("#searchString").prop("disabled", false);
			$("#searchString").focus();
		}
	});
	
	$(document).on("change", '#AdvanceSearchoption' ,function (){
		if($('#AdvanceSearchoption').val()=="2"){	
			$('#SclientName').val("");
			$('#SzillaName').html("");
			$('#SupaZilaName').html("");
			$('#SunionName').html("");
			$('#SvillageName').html("");	
			
			loadZilla("#SzillaName");				
					
			$('#AdvanceSearchnameAddressFields').show();
			$('#AdvanceSearchmobileFields').hide();			
		}
		else{	
			$('#SclientName').val("");
			$('#SzillaName').html("");
			$('#SupaZilaName').html("");
			$('#SunionName').html("");
			$('#SvillageName').html("");	
			$('#SmobileNumber').html("");
			
			loadZilla("#SzillaName");				
			
			$('#AdvanceSearchnameAddressFields').hide();
			$('#AdvanceSearchmobileFields').show();					
		}
	});
	

	//load zilla names in the dropdown from json
	function loadZilla(name){
		zillaJS = JSON.parse(zilla.replace(/\s+/g,""));
		$(name).empty();
		var output = "<option value ='none' selected></option>";
				
		$.each(zillaJS[0], function(key, val) {
			output = output + "<option value = " + key + "_" + val["divId"] + ">" + val["nameBangla"] + "</option>";
		});						
		
		$(name).append(output);
		return output;
	}
	
	//load upazila
	$(document).on("change", '#RzillaName' ,function (){
		loadUpazila("#RzillaName","#RupaZilaName");
	});
	$(document).on("change", '#SzillaName' ,function (){
		loadUpazila("#SzillaName","#SupaZilaName");
	});
	
	function loadUpazila(nameZilla,nameUpz){
		zillaJS = JSON.parse(zilla.replace(/\s+/g,""));
		var id = $(nameZilla).val();
		id = id.split("_")[0];
		//alert(id);
		$(nameUpz).empty();
		var output = "<option value ='none' selected></option>";
		
		$.each(zillaJS[0][id]["Upazila"], function(key, val) {
			output = output + "<option value = " + key + ">" + val["nameBanglaUpazila"] + "</option>"; 			
		});						
		
		$(nameUpz).append(output);
	}  
	
	//load union
	$(document).on("change", '#RupaZilaName' ,function (){
		loadUnion("#RzillaName","#RupaZilaName","#RunionName");
	});
	
	$(document).on("change", '#SupaZilaName' ,function (){
		loadUnion("#SzillaName","#SupaZilaName","#SunionName");
	});
	
	function loadUnion(nameZilla,nameUpz,nameUnion){
		zillaJS = JSON.parse(zilla.replace(/\s+/g,""));
		var idZilla = $(nameZilla).val();
		idZilla = idZilla.split("_")[0];
		var id = $(nameUpz).val();
		$(nameUnion).empty();
		var output = "<option value ='none' selected></option>";
		
		$.each(zillaJS[0][idZilla]["Upazila"][id]["Union"], function(key, val) {
			output = output + "<option value = " + key + ">" + val["nameBanglaUnion"] + "</option>"; 			
		});						
		
		$(nameUnion).append(output);
	}
	
	//load village
	$(document).on("change", '#RunionName' ,function (){
		loadVillage("#RzillaName","#RupaZilaName","#RunionName","#RvillageName");
	});
	
	$(document).on("change", '#SunionName' ,function (){
		loadVillage("#SzillaName","#SupaZilaName","#SunionName","#SvillageName");
	});
	
	function loadVillage(nameZilla,nameUpz,nameUnion,nameVillage){
		villJS = JSON.parse(vill.replace(/\s+/g,""));
		var idZilla = $(nameZilla).val();
		idZilla = idZilla.split("_")[0];
		var idUpazila = $(nameUpz).val();
		var idUnion = $(nameUnion).val();
		$(nameVillage).empty();
		var output = "<option value ='none' selected></option>";
		
		$.each(villJS[0][idZilla][idUpazila][idUnion], function(key, val) {
			$.each(villJS[0][idZilla][idUpazila][idUnion][key], function(key1, val1) {
				output = output + "<option value = " + key1 + "_" + key + ">" + val1 + "</option>"; 			
			});		 			
		});	
		
		$(nameVillage).append(output);
	}
	
	function getAdvanceSearchResult(){
		var searchObj = new Object();	
		searchObj = setCommonParam();
		searchObj.options = $('#AdvanceSearchoption').val();
		if(searchObj.options==1){
			searchObj.mobileNo = $('#SmobileNumber').val();
		}
		else{
			searchObj.name = $('#SclientName').val().toUpperCase();
			searchObj.gender = $('#SclientGender').val();
			searchObj.zilla = $('#SzillaName').val();
			searchObj.upz = $('#SupaZilaName').val();
			searchObj.union = $('#SunionName').val();
			searchObj.villagemouza = $('#SvillageName').val();
			//searchObj.village = $('#SvillageName').val().split("_")[0];
		}
			
		if (searchObj.name != "" && searchObj.mobileNo!="") {
			$.ajax({
				type: "POST",
				url: "advancesearch",
				timeout:60000, //60 seconds timeout
				dataType: "json",
				data: {"advanceSearch" : JSON.stringify(searchObj)},
				success : function(response) {
					$('#searchResult').empty();
					output =  "<br><hr>";
					output = output + "<div class='searchResultBox'>";
					if(response.count!="0"){
						
						output = output + "<table style='font-size:18px;' border='1px'><caption>Search Result (Found: " + response.count + ")</caption><tbody>";
						output = output + "<tr><th>SL.</th><th>HEALTH ID</th><th>NAME</th>";
						if($('#SclientGender').val()=="2" || response.options=="1"){
							if(response.options=="1"){
								output = output + "<th>SPOUSE'S NAME</th>";
							}
							else{
								output = output + "<th>HUSBAND'S NAME</th>";
							}
							
						}
						output = output + "<th>FATHER'S NAME</th><th>SELECT</th></tr>";
						if($('#SclientGender').val()=="2" || response.options==1){
							for(i=1; i<=response.count;i++){
								output = output + "<tr><td>" + i + "</td><td>"
										+ "<span class='searchLink' id='"+ response[i].healthId + "_" + response[i].healthIdPop +"'>" 
										+ response[i].healthId + "</span></td>"
										+ "<td>" + response[i].name + "</td><td>" + response[i].husbandName + "</td>"
										+ "<td>" + response[i].fatherName + "</td>"
										+ "<td><button class='searchButton' id='"+ response[i].healthId + "_" + response[i].healthIdPop + "'>Serve</button></td></tr>";						
							}
						}
						else{
							for(i=1; i<=response.count;i++){
								output = output + "<tr><td>" + i + "</td><td>"
								+ "<span class='searchLink' id='"+ response[i].healthId + "_" + response[i].healthIdPop +"'>" 
								+ response[i].healthId + "</span></td>"
								+ "<td>" + response[i].name + "</td>"
								+ "<td>" + response[i].fatherName + "</td>"
								+ "<td><button class='searchButton' id='"+ response[i].healthId + "_" + response[i].healthIdPop + "'>Serve</button></td></tr>";						
							}
						}
						output = output + "</tbody></table>";
					}
					else{
						output= output + "No matching information available!";
					}
					output = output + "</div>";
					$('#searchResult').append(output);
				},
				error : function(xhr, status, error) {
					alert(status);
					alert(error);
				}
			});
		}
		else{			
			if(searchObj.options==1){
				callDialog(mobileSearch);
			}
			else{
				callDialog(firstChar);
			}
		}		
	}
	