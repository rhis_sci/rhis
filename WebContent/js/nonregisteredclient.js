	//retrieve non-registered client information
	function retrieveGeneratedId(){
		var nonRegisteredClientObj = new Object();
		nonRegisteredClientObj = setCommonParam();
				
		if($('#RclientName').val() && $('#RclientAge').val() &&  $('#RclientFatherName').val() && $('#RclientMotherName').val() && $('#RzillaName').val()!='none' && $('#RupaZilaName').val()!='none' && $('#RunionName').val()!='none')
		{	
			var value = $('#RclientName').val() + $('#RclientFatherName').val() + $('#RclientMotherName').val() 
						+ moment().subtract({year:$('#RclientAge').val()}).format("YYYY-MM-DD")
						+ $('#RzillaName').val().split("_")[0] + $('#RupaZilaName').val() + $('#RunionName').val();
			if($('#RvillageName').val()!='none' || $('#RvillageName').val()!=null){
				value+=$('#RvillageName').val().split("_")[1] + $('#RvillageName').val().split("_")[0];
			}
			else{
				value+="";
			}
			//alert(moment().subtract({year:$('#RclientAge').val()}).format("YYYY-MM-DD"));
		hash = CryptoJS.MD5(value).toString();
		hash = hash.toString().substr(0,14);
		hash = parseInt(hash,16);
		hash = hash.toString().substr(0,14);
		//alert(hash);
		nonRegisteredClientObj.generatedId = hash;
		nonRegisteredClientObj.name = $('#RclientName').val().toUpperCase();
		nonRegisteredClientObj.age = $('#RclientAge').val();
		nonRegisteredClientObj.dob = moment().subtract({year:$('#RclientAge').val()}).format("YYYY-MM-DD");
		nonRegisteredClientObj.gender = $('#RclientGender').val();
		//alert(nonRegisteredClientObj.gender);
		nonRegisteredClientObj.husbandname = $('#RclientHusbandName').val().toUpperCase();
		nonRegisteredClientObj.fathername = $('#RclientFatherName').val().toUpperCase();
		nonRegisteredClientObj.mothername = $('#RclientMotherName').val().toUpperCase();		
		nonRegisteredClientObj.division = $('#RzillaName').val().split("_")[1];
		nonRegisteredClientObj.district = $('#RzillaName').val().split("_")[0];
		nonRegisteredClientObj.upazila = $('#RupaZilaName').val();
		nonRegisteredClientObj.union = $('#RunionName').val();
		if($('#RvillageName').val()!='none'){
			nonRegisteredClientObj.mouza = $('#RvillageName').val().split("_")[1];
			nonRegisteredClientObj.village = $('#RvillageName').val().split("_")[0];
		}
		else{
			nonRegisteredClientObj.mouza = "";
			nonRegisteredClientObj.village = "";
		}
		nonRegisteredClientObj.hhgrholdingno = $('#RhhNo').val();
		nonRegisteredClientObj.cellno = $('#RcellNo').val();
		
		nonRegisteredClientObj.providerid = $('#providerid').text();
				
		var userVal = "";
		userVal = userVal + (nonRegisteredClientObj.name=="" ? "<span style=\"color: red;\">" : "<span style=\"color: black;\">") + "নাম: </span>" + nonRegisteredClientObj.name + "<br>";
		userVal = userVal + (nonRegisteredClientObj.age=="" ? "<span style=\"color: red;\">" : "<span style=\"color: black;\">") + "বয়স: </span>" + nonRegisteredClientObj.age + "<br>";
		userVal = userVal + (nonRegisteredClientObj.gender=="" ? "<span style=\"color: red;\">" : "<span style=\"color: black;\">") + "লিঙ্গ: </span>" ; switch(nonRegisteredClientObj.gender){case '1':userVal+="পুরুষ";break; case '2':userVal+="মহিলা";break; case '3':userVal+="হিজরা";break;}; userVal+="<br>"; 
		userVal = userVal + (nonRegisteredClientObj.husbandname=="" ? "<span style=\"color: red;\">" : "<span style=\"color: black;\">") + "স্বামীর নাম: </span>" + nonRegisteredClientObj.husbandname + "<br>";
		userVal = userVal + (nonRegisteredClientObj.fathername=="" ? "<span style=\"color: red;\">" : "<span style=\"color: black;\">") + "পিতার নাম: </span>" + nonRegisteredClientObj.fathername + "<br>";
		userVal = userVal + (nonRegisteredClientObj.mothername=="" ? "<span style=\"color: red;\">" : "<span style=\"color: black;\">") + "মাতার নাম:</span>" + nonRegisteredClientObj.mothername + "<br>";
		userVal = userVal + (nonRegisteredClientObj.district =="" ? "<span style=\"color: red;\">" : "<span style=\"color: black;\">") + "জেলা: </span>" + zillaJS[0][nonRegisteredClientObj.district]["nameBangla"] + "<br>";
		userVal = userVal + (nonRegisteredClientObj.upazila =="" ? "<span style=\"color: red;\">" : "<span style=\"color: black;\">") + "উপজেলা: </span>" +zillaJS[0][nonRegisteredClientObj.district]["Upazila"][nonRegisteredClientObj.upazila]["nameBanglaUpazila"] + "<br>";
		userVal = userVal + (nonRegisteredClientObj.union =="" ? "<span style=\"color: red;\">" : "<span style=\"color: black;\">") + "ইউনিয়ন: </span>" + zillaJS[0][nonRegisteredClientObj.district]["Upazila"][nonRegisteredClientObj.upazila]["Union"][nonRegisteredClientObj.union]["nameBanglaUnion"]+ "<br>";
		//userVal = userVal + (nonRegisteredClientObj.mouza =="" ? "<span style=\"color: red;\">" : "<span style=\"color: black;\">") + "জেলা: </span>" + nonRegisteredClientObj.mouza + "<br>";
		//if(nonRegisteredClientObj.mouza!=undefined)
		if($('#RvillageName').val()!='none')
		userVal = userVal + (nonRegisteredClientObj.village =="" ? "<span style=\"color: red;\">" : "<span style=\"color: black;\">") + "গ্রাম / মহল্লা: </span>" + villJS[0][nonRegisteredClientObj.district][nonRegisteredClientObj.upazila][nonRegisteredClientObj.union][nonRegisteredClientObj.mouza][nonRegisteredClientObj.village]+ "<br>";
		userVal = userVal + (nonRegisteredClientObj.hhgrholdingno =="" ? "<span style=\"color: red;\">" : "<span style=\"color: black;\">") + "বাড়ি / জিআর / হোল্ডিং নম্বর: </span>" + nonRegisteredClientObj.hhgrholdingno + "<br>";
		userVal = userVal + (nonRegisteredClientObj.cellno =="" ? "<span style=\"color: red;\">" : "<span style=\"color: black;\">") + "মোবাইল নম্বর: </span>" + nonRegisteredClientObj.cellno + "<br>";

		//Confirmation Dialog box	
		
		$('<div></div>').dialog({
		        modal: true,
		        title: "নিশ্চিত করুন",
		        width: 420,
		        hide: {effect: 'fade', duration: 500},
		        show: {effect: 'fade', duration: 600},
		        open: function () {
		            $(this).html(userVal);
		        },
		        buttons: {
		        	"নিশ্চিত": function () {
		        		retrieveNonExistingClientInfo(nonRegisteredClientObj);
		            	$(this).dialog("close");
		            },
		            "পুনরায়": function () {
		        		$(this).dialog("close");
		        	}
		        }
		    });		
	}
	else{
		callDialog(fill);
	}
	}
	/**
	 * retrieve generatedId and all information in a single AJAX call and set client general information box
	*/
	function retrieveNonExistingClientInfo(nonRegisteredClientObj) {
    	$.ajax({
  			type: "POST",
  			url: "nonRegisteredClient",
  			timeout:60000, //60 seconds timeout
  			dataType: "json",
  			data: {"nonRegisteredClientGeneralInfo" : JSON.stringify(nonRegisteredClientObj)},
  			success : function(response) {
  				parseClientInfo(response);
  				
  			},
  			error : function(xhr, status, error) {
  				alert(status);
  				alert(error);
  			}
  		 });	
	}
	
	$(document).on("change", '#RclientGender' ,function (){
		
		if($('#RclientGender').val()=="2"){
			$('#RclientHusbandName').prop('disabled', false);
		}	
		else{
			$('#RclientHusbandName').prop('disabled', true);			
		}
	});