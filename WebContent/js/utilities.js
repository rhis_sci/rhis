var generalDeathReason = ["1","2","3","4","5","6","7","8","9","10","11","12","13","14"];
var maternalDeathReason = ["15","16","17","18","19","20","21","22","23","24","25","26","27"];

//ANC
/* Variable declaration (For Thresholding danger)*/
var t_ancdate='';
var t_ancbpsys =140;
var t_ancbpdias =90;
var t_ancweight ='';
var t_ancedema = '4';
var t_ancuheight ='';
var t_anchrate_up =160;
var t_anchrate_down =120;
var t_anchemoglobinH =20;
var t_ancanemia = '3';
var t_ancjaundice = '1';
var t_ancsugar = '5';
var t_ancFetalPresentation='2';
var t_ancalbumin = '5';
var t_anccomplication = '';
var t_ancsymptom ='';
var t_ancdisease = '';
var t_anctreatment = '';
var t_ancadvice ='';
var t_ancreferreason = '';

//Newborn
/* Variable declaration (For Thresholding danger)*/
var t_newborn_weight=2.5;

//PNC M
/* Variable declaration (For Thresholding danger)*/
var t_pncMdate='';
var t_pncMtemperature=100.4;
var t_pncMbpsys =90;
var t_pncMbpdias =140;
var t_pncMhemoglobin=20;
var t_pncManemia = '3';
var t_pncMedema = '4';
var t_pncMbreastcondition = '1';
var t_pncMuterusinvolution ='';
var t_pncMhematuria = 'normal';
var t_pncMperineum = 'normal';
var t_pncMfpmethod ='';
var t_pncMdisease = '';
var t_pncMcomplication = '';
var t_pncMtreatment = '';
var t_pncMadvice ='';
var t_pncMreferreason = '';
var t_pncMsymptom = '';
var t_pncMrefer= '';
var t_pncMreferreason= '';
var t_pncMrefercentername= '';


//PNC C
/* Variable declaration (For Thresholding danger)*/
var t_pncCdate='';
var t_pncCtemperature=100.4;
var t_pncCweight='';
var t_pncCbreathingperminuteH =60;
var t_pncCbreathingperminuteL =30;
var t_pncCdangersign ='';
var t_pncCbreastfeedingonly = '';
var t_pncCtreatment = '';
var t_pncCadvice ='';
var t_pncCreferreason = '';
var t_pncCsymptom = '';
var t_pncCdisease = '';
var t_pncCrefer= '';
var t_pncCreferreason= '';
var t_pncCrefercentername= '';

//PAC
/* Variable declaration (For Thresholding danger)*/
var t_pacdate='';
var t_pactemperature=100.4;
var t_pacbpsys ='140';
var t_pacbpdias =90;
var t_pachemoglobin=20;
var t_pacanemia = '3';
var t_pacuterusinvolution ='';
var t_pachematuria = 'normal';
var t_pacperineum = 'normal';
var t_pacfpmethod ='';
var t_pacdisease = '';
var t_paccomplication = '';
var t_pactreatment = '';
var t_pacadvice ='';
var t_pacreferreason = '';
var t_pacsymptom = '';
var t_pacrefer= '';
var t_pacreferreason= '';
var t_pacrefercentername= '';

//General Patient
/* Variable declaration (For Thresholding danger)*/
var t_gpbpsys =140;
var t_gpbpdias =90;
var t_gpweight =100;
var t_gpedema = '4';
var t_gphemoglobin =20;
var t_gpanemia = '3';
var t_gppulse = 90;
var t_gptemperature = 101;
var t_ancjaundice = '1';
var t_gpsugar = '5';
var t_gpalbumin = '5';

//Global Variables
var ancProviderId;
var pncMProviderId; 
var pncCProviderId = [];
var newbornProviderId;
var pacProviderId;
var gpProviderId;
var numberOfChild=0;
var danger = " class='logic_danger'";	
var safe = "";	
var openTab="";
var Abortion="" ;
var hideFlag="";

//Alert Messages with Bengali Translation
var noString="<h2>অনুসন্ধান সম্ভব নয়</h2>";
var noPreg="<h2>প্রথমে গর্ভাবস্থার তথ্য দিন</h2>";
var noDate="<h2>পরিদর্শনের সময় পূরণ বাধ্যতামূলক।</h2>";
var upcoming="<h2> সামনের সংস্করণ থেকে পাওয়া যাবে</h2>";
var newPreg="<h2>আপনি কি এই সেবাগ্রহিতার জন্য নতুন গর্ভের তথ্য দিতে চান ?</h2>";
var noClient="<h2>প্রথমে একজন সেবাগ্রহিতা নির্বাচন করুন</h2>";
var noANC="<h2>গর্ভকালীন সেবা দেয়া সম্ভব নয়, কারন এই গর্ভাবস্থার প্রসব সংঘটিত হয়ে গেছে </h2>";
var notValid="<h2>প্রদত্ত তথ্য সঠিক নয়! অনুগ্রহ করে আবার চেষ্টা করুন</h2>";
var dead="<h2>এই সেবাগ্রহীতাকে মৃত রিপোর্ট করা হয়েছে !</h2>";
var greaterPara="<h2>'গ্রাভিডা' 'প্যারা' থেকে বেশী হতে হবে. অনুগ্রহ করে পুনরায় যাচাই করুন</h2>";
var fill="<h2>(*) চিহ্নিত ঘর গুলি পূরণ করুন </h2>";
var firstChar="<h2>অন্তত নামের প্রথম অক্ষর উল্লেখ করুন !</h2>";
var mobileSearch="<h2> সঠিকভাবে মোবাইল নম্বরের ঘর পূরণ বাধ্যতামূলক। </h2> ";
var noDeath="<h2> এই সেবাগ্রহীতাকে মৃত রিপোর্ট করা হয়নি </h2>";
var diffProvider="<h2>অন্য সেবাপ্রদানকারী তথ্যটি প্রদান করেছেন । পরিবর্তন সম্ভব নয় !</h2>";
var PNCwithoutDelivery="<h2>প্রসবোত্তর সেবাদানের জন্য প্রসবকালীন সেবার তথ্য প্রয়োজন। প্রথমে প্রসবকালীন সেবার তথ্য প্রদান করুন।</h2>";
var selectGender="<h2>লিঙ্গ সনাক্ত করুন (ছেলে/মেয়ে)(*)</h2> ";
var noPNC="<h2>প্রসবের পর ৭ সপ্তাহ থেকে পার হয়ে গেছে, আর প্রসবোত্তর সেবা সম্ভব নয়।</h2>";
var noPAC="<h2>গর্ভপাতের পর ৪ সপ্তাহ পার হয়ে গেছে, আর গর্ভপাত পরবর্তী সেবা সম্ভব নয়।</h2>";
var selectMethod="<h2> পদ্ধতির ঘর পূরণ বাধ্যতামূলক। </h2> ";
var noDate="<h2>তারিখ/মাস পূরণ করা বাধ্যতামূলক।</h2>"
var selectSpecificMethod="<h2> সঠিকভাবে পদ্ধতির ঘর পূরণ করুন। </h2> ";
var methodAmount="<h2> পরিমান শূন্যের অধিক হতে হবে। </h2> ";
var confirmDelete="<h2>আপনি এই এন্ট্রি মুছে <br>ফেলার ব্যাপারে নিশ্চিত ?</h2>";
var futureDate="<h2>ভবিষ্যতের তারিখ গ্রহণযোগ্য নয়।</h2>"; 
var forgotPass="<h2>পাসওয়ার্ড নতুন করে নিতে আপনার ঊর্ধ্বতন কর্তৃপক্ষের সাথে যোগাযোগ করুন।</h2>";
var dDateLessThanLMP = "<h2>প্রদত্ত তথ্য সঠিক নয়! <br>প্রসবের  / গর্ভপাতের সময়, শেষ মাসিকের তারিখের পরে হতে হবে।</h2>";
var ancDateLessThanLMP = "<h2>প্রদত্ত তথ্য সঠিক নয়! <br>গর্ভকালীন পরিদর্শনের সময়, শেষ মাসিকের তারিখের পরে হতে হবে।</h2>";
var pncDateLessThanDelivery = "<h2>প্রদত্ত তথ্য সঠিক নয়! <br>প্রসবোত্তর পরিদর্শনের সময়, প্রসবের সময়ের পরে হতে হবে।</h2>";
var pacDateLessThanAbortion = "<h2>প্রদত্ত তথ্য সঠিক নয়! <br>গর্ভপাত পরবর্তী সেবার সময়, গর্ভপাতের সময়ের পরে হতে হবে।</h2>";
var iudFollowupDateLessThanIUDDate = "<h2>প্রদত্ত তথ্য সঠিক নয়! <br>ফলোআপের সময়, আইইউডি প্রয়োগের সময়ের পরে হতে হবে।</h2>";
var notAllowed = "<h2>আপনার জন্য এই রেজিস্টার প্রযোজ্য নয়।</h2>";

//Function for tool tip return
function get_tooltip(x){
	
	var msg;
	
	switch(x){	
			
			case "lmp_tp": 
			    msg="শেষ মাসিকের তারিখ (LMP): শেষ মাসিকের তারিখ (প্রথম দিন) খুবই গুরুত্বপূর্ণ ৷ যদি গর্ভবতী মহিলা শেষ মাসিকের তারিখ সঠিকভাবে বলতে না পারেন তবে বিভিন্ন প্রশ্নের মাধ্যমে তা বের করতে হবে ৷ যেমন- জুমার নামাজের দিন, গুরুত্বপূর্ণ ঘটনা, মাসের প্রথম দিকে, মাঝ খানে, শেষের দিকে ইত্যাদি ৷ শেষ মাসিকের তারিখ ঠিক না হলে সব হিসাব সঠিকভাবে করা যাবে না ৷";
		        break;
		        
	        case "para_tp": 
	            msg="প্যারা: সর্বমোট পূর্ববর্তী গর্ভ খালাসের (গর্ভকাল ৭ মাসের অধিক)সংখ্যা লিখুন ৷ [প্যারা ও গ্রাভিডার উদাহরণ - গর্ভকাল ৭ মাস অতিবাহিত হওয়ার পর মা ৩টি শিশুর জন্মদান করেছেন, তাহলে প্যারা ৩ লিখুন এবং বর্তমানে ৫ম বারের মতো গর্ভধারণ করেছেন; গর্ভধারণের ক্ষেত্রে গর্ভকাল যে কোন সময়ের হতে পারে; অন্য গর্ভধারণ, গর্ভপাত অথবা একটপিক বা মোলার গর্ভধারণ হতে পারে, তাহলেও গ্রাভিডা ৫ লিখুন ৷ যমজ হলে প্যারা ১ গণনা করুন ।]";
	            break;
			
			case "edd_tp":			
	            msg="প্রসবের সম্ভাব্য তারিখ (EDD): গর্ভাবস্থা ২৮০ দিন বা ৪০ সপ্তাহ ধরা হয়৷ মাসিক নিয়মিত হলে মা যদি শেষ মাসিকের প্রথম দিন (LMP) বলতে পারেন, তবে শেষ মাসিকের প্রথম দিনের সাথে ৯ মাস গণনা করে তার সাথে ৭ দিন যোগ করে দিলে মায়ের সম্ভাব্য প্রসবের তারিখ পাওয়া যায়৷ উদাহরণ : সালেহার শেষ মাসিকের তারিখ ২৩/০১/২০১৪; ৯ মাস যোগ দিলে ২৩/১০/২০১৪ হবে; এখন আর ৭ দিন যোগ দিতে হবে, তাহলে প্রসবের সম্ভাব্য তারিখ হবে ৩০/১০/২০১৪৷";
				break;
				
	         case "gravida_tp": 
	            msg="গ্রাভিডা: বর্তমান ও অতীতের সকল গর্ভধারণের (গর্ভকাল বিবেচনা না করে) সংখ্যা লিখুন ৷ [প্যারা ও গ্রাভিডার উদাহরণ - গর্ভকাল ৭ মাস অতিবাহিত হওয়ার পর মা ৩টি শিশুর জন্মদান করেছেন, তাহলে প্যারা ৩ লিখুন এবং বর্তমানে ৫ম বারের মতো গর্ভধারণ করেছেন; গর্ভধারণের ক্ষেত্রে গর্ভকাল যে কোন সময়ের হতে পারে; অন্য গর্ভধারণ, গর্ভপাত অথবা একটপিক বা মোলার গর্ভধারণ হতে পারে, তাহলেও গ্রাভিডা ৫ লিখুন ৷ যমজ হলে প্যারা ১ গণনা করুন ।]";
	            break;    	 
	                 
	         case "lmpDisagreement": 
	            msg="পরিবার পরিকল্পনা সহকারী (FWA) শেষ মাসিকের ভিন্ন তারিখ প্রদান করেছেন";
	            break;   
	        
	         case "alive_child_no": 
		            msg="জীবিত সন্তান সংখ্যা: বর্তমানে গর্ভবতী মহিলার যে কয়জন জীবিত ছেলে এবং মেয়ে আছে সে সংখ্যা লিখুন৷ ছেলে বা মেয়ে না থাকলে যথাযথ স্থানে ০ (শূন্য) লিখুন ৷";
		            break;  	           
		           
	         case "pregHistory_tp":
		            msg="পূর্বের ইতিহাস ঝুঁকিপূর্ণ কি না: এ অংশে গর্ভবতী মহিলার পূর্বের গর্ভকালীন, প্রসবকালীন অথবা প্রসব-পরবর্তী ইতিহাস ঝুঁকিপূর্ণ কি না তা জিজ্ঞেস করুন এবং নির্দিষ্ট বক্সে টিক (√)দিন ৷";
		            break;    	            

	         case "personalInfoNewButton": 
		            msg="নতুন (পুনরায়) গর্ভধারণ";
		            break;    	            

	         case "anc_bp":
	         case "pncm_bp":
	         case "PAC_bp":
		            msg="রক্তচাপ: চেক-আপের সময়  রক্তচাপ কত, তা ভিজিট অনুযায়ী লিখুন ৷ যেমনঃ ১২০/৮০ মিঃ মিঃ মার্কারি হলে বাম পাশের ঘরে ১২০ এবং ডান পাশের ঘরে ৮০ লিখুন ।";
		            break;      
		            
	         case "anc_edima": 
		            msg=" ইডিমা: পায়ে ইডিমা বা ফোলা আছে কিনা তা পরীক্ষা করে দেখুন এবং যদি ফোলা না থাকে তবে 'নাই'; অল্প থাকলে '+' মোটামুটি মাত্রায় থাকলে '++' এবং অধিক মাত্রায় থাকলে '+++' লিখুন  ৷";
		            break;   	
		     
	         case "PAC_anemia":
	         case "anc_himoglobin":
	         case "pncm_himoglobin":
	         case "PAC_himoglobin":
		            msg=" রক্তস্বল্পতা/ হিমোগ্লোবিন: যদি হিমোগ্লোবিন পরীক্ষা করেন তাহলে চেকআপের সময় গৃহীত রক্তে হিমোগ্লোবিনের মাত্রা কত, তা শতকরা হারে লিখুন ৷ যেমনঃ (৭০%) ৷ আর যদি রক্তস্বল্পতার মাত্রা লিখতে হয়, তাহলে স্বল্প (+)/মধ্যম(++)/ অতিরিক্ত(+++) এর কোনটি তা উল্লেখ করুন  ৷";
		            break;  
		            
	         case "anc_jondis": 
		            msg=" জন্ডিস: গর্ভবতী মায়ের চোখ, জিহ্বার উল্টোপিঠ, হাতের তালু হলদে কিনা তা পরীক্ষা করে দেখুন এবং যদি জন্ডিস না থাকে তবে 'নাই'; অল্প থাকলে '+' মোটামুটি মাত্রায় থাকলে '++' এবং অধিক মাত্রায় থাকলে '+++' লিখুন ৷ ৷";
		            break;
		            
	         case "anc_sugar": 
		            msg=" প্রস্রাবে সুগার: চেক-আপের সময় প্রস্রাবে সুগার এর মাত্রা কত পাওয়া গেল তা এই ঘরে লিখুন৷ যদি প্রস্রাবে কোন সুগার না থাকে তাহলে 'নাই' লিখুন  ৷ আর যদি পাওয়া যায় তাহলে তার মাত্রা লিখুন, যেমন- '+', '++', '+++' অথবা '++++' লিখুন  ৷ ";
		            break; 
		            
	         case "anc_albumin": 
		            msg=" প্রস্রাবে এলবুমিন: পরীক্ষা করে দেখুন গর্ভবতী মহিলার প্রস্রাবে এলবুমিন আছে কিনা, যদি না থাকে তবে 'নাই' লিখুন ৷  আর যদি পাওয়া যায় তাহলে তার মাত্রা যেমন  '+', '++', '+++' অথবা '++++' লিখুন ৷ ";
		            break;   
		            
	         case "delivery_result_tp": 	         
		        	msg=" স্বয়ংক্রিয় (Automatic) পূরণ হবে , পূরণের প্রয়োজন নেই";
		        	break;    
		        	
	         case "amtsl_tp": 
		        	msg=" প্রসবের তৃতীয় ধাপের সক্রিয় ব্যাবস্থাপনা (AMTSL) যা অনুসরণ করা হয়েছে : যদি স্বাভাবিক প্রসবের তৃতীয় ধাপের সক্রিয় ব্যাবস্থাপনা AMTSL অনুসরণ করা হয়ে থাকে তাহলে যা যা করা হয়েছে, তার জন্য নির্দিষ্ট ঘরে টিক (√) দিন ৷ AMTSL এর ৩টি ধাপ উলেস্নখ করা হয়েছে (ধাপগুলি: প্রসবের ১ মিনিটের মধ্যে জরায়ু সংকোচনকারী ঔষধ প্রয়োগ, গর্ভফুল প্রসবের জন্য নাভীরজ্জুতে নিয়ন্ত্রিত টান এবং জরায়ু ম্যাসাজ করা) ৷";
		        	break;     	

	         case "Miso_instead_Oxi": 
		        	msg="অক্সিটোসিন না থাকলে মিসোপ্রোস্টল খাওয়ানো হয়েছে : প্রসবের ১ মিনিটের মধ্যে জরায়ু সংকোচনকারী ঔষধ হিসেবে অক্সিটোসিন ব্যবহারের নিয়ম ৷ কিন্তু যদি কোন কারণে অক্সিটোসিন না থাকে তবে মিসোপ্রোস্টোল বড়ি খাওয়ান ৷ যদি প্রসব পরবর্তী রক্তক্ষরণ বন্ধ করার জন্য সন্তান প্রসবের সাথে সাথে গর্ভফুল বের হবার আগে মিসোপ্রোস্টল খাওয়ান তাহলে 'হ্যাঁ' এর ওপর টিক চিহ্ন দিন আর না খেয়ে থাকলে 'না' এর ওপর টিক চিহ্ন দিন ৷";
		        	break;
		      
	         case "abortionC": 
	         case "abortionC2":
	         case "closeAbortionC": 
	         case "closeAbortionC2":
		        	msg="প্রসব সম্পাদনকারীর নাম ও পদবী লিখুন ";
		        	break; 
	
	         case "delivery_complexity_tp": 		  
		        	msg="যদি মহিলার প্রসবকালীন জটিলতা থেকে থাকে তাহলে যে জটিলতায় আক্রান্ত সেটিতে টিক (√) দিন৷ একাধিক জটিলতাও থাকতে পারে, সেক্ষেত্রে একাধিক টিক চিহ্ন হবে  ৷ এই জটিলতার বাইরে যদি অন্য কোন জটিলতা থাকে তাহলে অন্যান্য এর ঘরে টিক দিয়ে সমস্যাটি নির্দিষ্ট করে লিখুন  ৷";
		        	break; 
		     /*
	         case "treatment":
		        	msg="চিকিৎসা: যদি মহিলাকে কোন চিকিৎসা  দিয়ে থাকেন তাহলে তা এই কলামে লিখুন ৷ ";
		        	break; 
		     */
	         case "resastation1":
	         case "resastation2":
	         case "resastation3":
	         case "resastation4":
	         case "resastation5":
		        	msg="রিসাসসিটেশন: জন্মকালীন শ্বাসরম্নদ্ধতায় আক্রান্ত নবজাতককে যদি রিসাসসিটেশন এর প্রয়োজন হয় তবে 'হ্যাঁ' এর পাশের ঘরে টিক দিন আর না হয়ে থাকলে 'না ' এর ঘরে টিক দিন৷ যদি হ্যাঁ হয়ে থাকে তবে কোন পদ্ধতি ব্যবহার করে রিসাসসিটেট করা হয়েছে সেই পদ্ধতির পাশের ঘরে টিক (√) দিন ৷ যদি স্টিমুলেশন দিয়ে রিসাসসিটেশন করা হয় তবে শুধু স্টিমুলেশনে টিক দিন আর যদি স্টিমুলেশন এবং ব্যাগ ও মাস্ক দুই-ই ব্যবহার করা হয়ে থাকে, তাহলে দুটি ঘরেই টিক দিন ৷ ";
		        	break;  
		     
	         case "pncm_Utr_Inv":
		        	msg="জরায়ুর ইনভলিউশন: প্রসবের পর মায়ের জরায়ু পূর্বের অবস্থায় ফিরে আসতে ৬ সপ্তাহ সময় লাগে ৷ একেই বলা হয় ইনভলিউশন বা জরায়ুর ক্রমসংকোচন ৷ প্রথম ২৪ ঘন্টায় কোন পরিবর্তন হয় না৷ ২য় থেকে ১১ তম দিন পর্যনত্ম জরায়ুর ফান্ডাসের উচ্চতা প্রতি ২৪ ঘন্টায় ১/২ ইঞ্চি করে কমে যায়৷ ১১ তম দিনে জরায়ু পিউবিক সিমফাইসিস এর পিছনে চলে যায় এবং পেটে হাত দিলে জরায়ু বোঝা যায় না৷ ৬ সপ্তাহ পরে জরায়ু প্রসবপূর্ব অবস্থায় ফেরত যায়৷ সেবাগ্রহীতা মা'র পেটে পরীক্ষা করে জরায়ুর উচ্চতা বের করুন এবং ফরমে জরায়ুর অবস্থান লিপিবদ্ধ করুন৷ যদি পেটে হাত দিয়ে জরায়ু না পান তাহলে সেটাও লিখুন ৷";
		        	break;   	
		        	
	         case "br_milk":
		        	msg=" 'হ্যাঁ' হলে পাশের ঘরে টিক দিন ";
		        	break; 
		        	
	         case "perineum":
	         case "PAC_perineum":
		        	msg=" পেরিনিয়াম (স্বাভাবিক/ ছিঁড়ে গেছে/ অন্যান্য জটিলতা): পেরিনিয়াম পরীক্ষা করে দেখুন ৷ যদি পেরিনিয়াম অক্ষত থাকে তবে স্বাভাবিক লিখুন আর কোন সমস্যা থাকলে পেরিনিয়ামের অবস্থা নির্দিষ্ট করে লিখুন ৷ ";
		        	break;    	
		        	
	         case "pncc_bpm":
		        	msg="নবজাতক প্রতি মিনিটে কতবার শ্বাস নেয় তা এই কলামে লিখুন ৷ নবজাতকের ক্ষেত্রে প্রতি মিনিটে ৬০ বারের কম শ্বাস নেয়া হচ্ছে স্বাভাবিক ৷ ";
		        	break;    			        			        	
	}	
	return msg;
}

//Functions to mark lines in Confirmation Box	
function linemark(text,value){	
	if(value==''){
		mark="style='color:red;'";
	}
	else{
		mark='';
	}
	line='<span '+mark+'>'+text+": </span>"+value;		
	return line;
}

function linemark_compare(text,value,compare){	
	if(value==compare){
		mark="style='color:red;'";
	}
	else{
		mark='';
	}
	line='<span '+mark+'>'+text+": </span>"+value;		
	return line;
}

function linemark_switch(text,value,compare){	
	if(value==compare){ 
		mark="style='color:red;'";
	}
	else{
		mark='';
	}
	line='<span '+mark+'>'+text+": </span>";		
	return line;
}

//Month Convertion
function month_convert(month_num)
{
	
	var month_name = "";
	switch(month_num){
		case "01":
			month_name = "জানুয়ারী";
			break;
		case "02":
			month_name = "ফেব্রুয়ারি";
			break;
		case "03":
			month_name = "মার্চ";	
			break;
		case "04":
			month_name = "এপ্রিল";
			break;
		case "05":
			month_name = "মে";
			break;
		case "06":
			month_name = "জুন";
			break;
		case "07":
			month_name = "জুলাই";
			break;
		case "08":
			month_name = "অগাস্ট";
			break;
		case "09":
			month_name = "সেপ্টেম্বর";
			break;	
		case "10":
			month_name = "অক্টোবর";
			break;
		case "11":
			month_name = "নভেম্বর";
			break;
		case "12":
			month_name = "ডিসেম্বর";
			break;
	}
	return month_name;
}

//Functions for Number conversions
function ben_to_en_number_conversion(ben_number) {
	
	var eng_number = '';
	
	if(ben_number!=null){
		for (var i = 0; i <= ben_number.length; i++) {
			switch(ben_number[i]){
				case "০":
					eng_number = eng_number + '0';
					break;
				case "১":
					eng_number = eng_number + '1';
					break;
				case "২":
					eng_number = eng_number + '2';
					break;
				case "৩":
					eng_number = eng_number + '3';
					break;
				case "৪":
					eng_number = eng_number + '4';
					break;
				case "৫":
					eng_number = eng_number + '5';
					break;
				case "৬":
					eng_number = eng_number + '6';
					break;
				case "৭":
					eng_number = eng_number + '7';
					break;
				case "৮":
					eng_number = eng_number + '8';
					break;
				case "৯":
					eng_number = eng_number + '9';
					break;	
				case ".":
					eng_number = eng_number + '.';
					break;	
				case ",":
					eng_number = eng_number + ',';
					break;	
				case "%":
					eng_number = eng_number + '%';
					break;
				case "/":
					eng_number = eng_number + '/';
					break;
			}			
		}
	}
	return eng_number;
}

function en_to_ben_number_conversion(en_number) {
	
	var ben_number = '';
	
	if(en_number!=null){
		
		for (var i = 0; i <= en_number.length; i++) {
			switch(en_number[i]){
				case "0":
					ben_number = ben_number + '০';
					break;
				case "1":
					ben_number = ben_number + '১';
					break;
				case "2":
					ben_number = ben_number + '২';
					break;
				case "3":
					ben_number = ben_number + '৩';
					break;
				case "4":
					ben_number = ben_number + '৪';
					break;
				case "5":
					ben_number = ben_number + '৫';
					break;
				case "6":
					ben_number = ben_number + '৬';
					break;
				case "7":
					ben_number = ben_number + '৭';
					break;
				case "8":
					ben_number = ben_number + '৮';
					break;
				case "9":
					ben_number = ben_number + '৯';
					break;	
				case "০":
					ben_number = ben_number + '০';
					break;
				case "১":
					ben_number = ben_number + '১';
					break;
				case "২":
					ben_number = ben_number + '২';
					break;
				case "৩":
					ben_number = ben_number + '৩';
					break;
				case "৪":
					ben_number = ben_number + '৪';
					break;
				case "৫":
					ben_number = ben_number + '৫';
					break;
				case "৬":
					ben_number = ben_number + '৬';
					break;
				case "৭":
					ben_number = ben_number + '৭';
					break;
				case "৮":
					ben_number = ben_number + '৮';
					break;
				case "৯":
					ben_number = ben_number + '৯';
					break;	
				case ".":
					ben_number = ben_number + '.';
					break;	
				case ",":
					ben_number = ben_number + ',';
					break;	
				case "%":
					ben_number = ben_number + '%';
					break;	
				case "/":
					ben_number = ben_number + '/';
					break;	
			}			
		}
	}		
	return ben_number;
}

//Generic Function for Delete ANC,PNCM,PNCC Entry
function deleteLast(serviceName,providerId,childNo){
	
	if(providerId==$('#providerid').text()){	
		
		$('<div></div>').dialog({
		   modal: true,
		   title: "নিশ্চিত করুন",
		   width: 420,
		   open: function () {
			   $(this).html(confirmDelete);
		   },
		   buttons: {
			   Yes: function () {
				   
				   switch(serviceName){		
				   		case 'anc':
				   			deleteLastANCInfo();
						break;
				   		case 'pncM':
				   			deleteLastPNCMotherInfo();
						break;	
				   		case 'pncC':
				   			deleteLastPNCChildInfo(childNo);
						break;
				   		case 'pac':
				   			deleteLastPACInfo();
						break;
				   		case 'gp':
				   			deleteLastGPInfo();
				   		break;
				   }
				   
				   $(this).dialog("close");
			  },  
			   No: function () {
				   $(this).dialog("close");
			 }  
		   }
	   });			
	}	
	else{
		callDialog(diffProvider);	
	}	
}

//Function for Alert Dialog
function callDialog(message){
	
	$('<div></div>').dialog({
		   modal: true,
		   title: "Message",
		   width: 420,
		   open: function () {
			   $(this).html(message);
		   },
		   buttons: {
			   Ok: function () {
				   $(this).dialog("close");
			   }      
		   }
	});		
}

//On change event for restricting future date
$(document).on("change", "#lmp,#admissionDate,#deliveryDate,#closeDate,"
						+ "#shortPregLMPDate,#shortPregDeliveryDate,#ttDate,#deathDate,#ancDate,"
						+ "#startDateReport,#endDateReport,#PACDate,#pncmotherDate,#PCVisitDate,#WIdoseDate,#WILMP,#IUDLMP,#IUDiudImplantDate,"
						+ "#IUDFollowupfollowupDate,#IUDFollowupiudRemoveDate,#GPvisitDate" ,function (){
	if(moment($(this).val()).format("YYYY-MM-DD") > moment().format("YYYY-MM-DD")){
		callDialog(futureDate);
		$(this).combodate('setValue', moment(), true);
	}
	
});

//On change event for restricting future date
$(document).on("change", "#monthReport" ,function (){
	if(moment($(this).val()).format("YYYY-MM") > moment().format("YYYY-MM")){
		callDialog(futureDate);
		$(this).combodate('setValue', moment(), true);
	}
	
});

//Function for removing PregHistory
function removePregHistory(){
	$('#bloodPreg').prop('checked',false);
	$('#delayedDeliveryPreg').prop('checked',false);
	$('#blockedDeliveryPreg').prop('checked',false);
	$('#uterusPreg').prop('checked',false);
	$('#stillBirthPreg').prop('checked',false);
	$('#newbornDeathPreg').prop('checked',false);
	$('#edemaPreg').prop('checked',false);
	$('#eclampsiaPreg').prop('checked',false);
	$('#csectionPreg').prop('checked',false);
	$('#pregHistory').hide();
}

//Function to clear all
function clearServicesUI(){
$('#personalInfo').css("background-color","#FFFFFF");
$('#personalDetail').css("background-color","#FFFFFF");
$("div#personalReg").hide();
$("div#personalInfo").hide();
$("div#service_tab_MNCH").hide();
$("div#service_tab_FP").hide();
$('div#anc').hide();
$('div#delivery').hide();
$('div#newborn').hide();
$("div#pncmother").hide();
$('div#pncchild').hide();
$('div#pac').hide();
$('div#content').hide();
$("#heading").html("");
//$("#heading").append($content);					
//$("#heading").hide();
$('div#new').show();
}

//set common parameter for every network call
function setCommonParam(){
	var jsonObject = new Object();
	
	jsonObject.providerType = $('#providerType').text();
	jsonObject.zillaid = $('#zillaid').text();
	jsonObject.upazilaid = $('#upazilaid').text();
	jsonObject.unionid = $('#unionid').text();
		
	return jsonObject;
}

//Function to get the initial screen
function getInitialSetup(){

	$('#personalDetail').css("background-color","#FFFFFF");
	$('#pregNumber').html("");
	$('#healthid').html("");
	$("#regNumber").text("");
	$("#regDate").text("");
	$("div#heading").hide();
	$("div#register_personal").hide();
	$("div#personal").hide();
	$("div#service_tab_MNCH").hide();
	$("div#service_tab_FP").hide();
	$('#ancVisitTable').remove();
	$("div#anc").hide();
	$("div#delivery").hide();
	
	for(i=1;i<=5;i++){
		id = "#newBornInfo_" + i;
		$(id).hide();
	}
	$("div#newborn").hide();
	$('#pncVisitMotherTable').remove();
	$("div#pncmother").hide();
	$('#pncVisitChild').empty();
	$('div#pncchild').hide();
	$('#PACVisitTable').remove();
	$("div#pac").hide();
	$('div#content').empty();
	$('div#content').hide();
	$('div#report').empty();
	$('div#report').hide();
	$("div#detailInfoWindow").hide();
	$('div#new').hide();
	$("#search").show();
	$("#searchString").val("");
	$("#searchString").focus();	
	
	resetAllServiceTab();
}


//highlighting the client's information div based on risk factor
function highlightRisk(riskFactor){
	if(riskFactor==true){
		$('#personalInfo').css("background-color","#D39C8A");
    	$('#personalDetail').css("background-color","#D39C8A");
	}
	if(riskFactor==false){
		$('#personalInfo').css("background-color","#FFFFFF");
		$('#personalDetail').css("background-color","#FFFFFF");
	}
}

function populateDropdownList(jsonVal){
	
	output = "";		
	$.each(jsonVal, function(key, val) {
		output = output + "<option value = " + key + ">" + val + "</option>"; 			
	});		
	return output;
}

function populateSpecificDropdownList(list,jsonVal){
	
	output = "";		
	$.each(list, function(key, val) {
		output = output + "<option value = " + val + ">" + jsonVal[val] + "</option>";
	}); 	
	return output;
}

function retrieveDetailHistory(colDialogId, jsonObject){
	
	textVal = $(colDialogId).text();
	textVal = textVal.slice(1, -1);
		
	if(textVal!=""){
		textVal = textVal.split(",");
		var j;
		var convertedText = "";
	
		for(j =0; j < textVal.length; j++){
			textVal[j] = textVal[j].slice(1,-1);
			convertedText = convertedText + "- " + jsonObject[0][textVal[j]] + "<br>";					
		}
		$('#detailList').html(convertedText);
		$('#detailInfoWindow').modal();
	
	}
	else{
		$('#detailList').html("There is no information")
		$('#detailInfoWindow').modal();
	}
}

function resetAllMenuItem(){
	$('#homeMenu').removeClass();
	$('#reportMenu').removeClass();
	$('#aboutMenu').removeClass();
	
	$('#homeMenu').addClass("home");
	$('#reportMenu').addClass("report");
	$('#aboutMenu').addClass("about");	
}

function resetAllServiceTab(){
	for(i=1;i<23;i++){
		id = "#" + i;
		$(id).removeClass("serviceTabButton");
		$(id).removeClass("serviceTabButtonSelected");
		
		$(id).addClass("serviceTabButton");
	}
}

function changeColorSelectedServiceTab(ids){
	
	$.each(ids, function(key, val) {
		id = "#" + val;
		
		$(id).removeClass("serviceTabButton");
		$(id).removeClass("serviceTabButtonSelected");
			
		$(id).addClass("serviceTabButtonSelected");
		
	});
}

function hideSetFieldsForCSBA(){
	$("#4").hide(); //service tab button PAC
	$("#14").hide(); //service tab button GP
	$("#22").hide(); //service tab button FP
	$("#epsiotomySpan").hide(); //episiotomy section in delivery
	$("#csba1").hide(); //admission, ward and bed section in delivery
	$("#csba2").hide(); //delivery done by section in delivery
	
	$('#deliveryType option[value= ' + 2 + ']').remove(); //CSection removed from delivery type
	
}

function returnMultiSelectedDropdownContent(selectedVal, jsContent){
	
	var i;
	var content = "  ";
	
	for(i =0; i < selectedVal.length; i++){
		content = content +"<tr><td>-"+jsContent[0][selectedVal[i]] +"</td></tr>";				
	}
		
	return content;
}

function setMultiSelectDropdown(fieldId, response){
	var options = response;
	options = options.slice(1,-1);
	options = options.split(",");
	
	var dropdownItem = "";
	for(i=0;i<options.length;i++){
		dropdownItem = dropdownItem + options[i].slice(1,-1) + ",";
	}
	
	dropdownItem = dropdownItem.slice(0,-1);
	dropdownItem = dropdownItem.split(",");
	$(fieldId).multipleSelect("uncheckAll");
	$(fieldId).multipleSelect("setSelects", dropdownItem);
}

function returnMultiSelectDropdownRow(label, fieldName, response, count, fieldJSonKey, serviceProviderId, jsonKeyProviderId, jsonList, deleteTrue, deleteButtonId, saveEnable, saveButtonId, active){
	var tr = "<tr><td>" + label + "</td>";
	var i;
	
	for(i=1; i<=count; i++){
		var colIdSpan = "#"+ fieldName + "-" + i + "-span";
							
		var col = "<td id='" + fieldName + "-" + i + "'>"
				 + "<span id='" + fieldName + "-" + i + "-span' style='cursor:pointer'> <strong>"+detail(response[i][fieldJSonKey])+"</strong> </span>";
		
		col = col + "<div id ='" + fieldName + "-" + i + "-dialog' hidden>" + response[i][fieldJSonKey] + "</div>";
		
		if(deleteTrue=="deleteTrue"){
			if(i==count){					
				switch(serviceProviderId){
					case "ancProviderId":
						ancProviderId=response[i][jsonKeyProviderId];
					break;
					case "pncMProviderId":
						pncMProviderId=response[i][jsonKeyProviderId];
					break;
					case "newbornProviderId":
						newbornProviderId=response[i][jsonKeyProviderId];
					break;
					case "pacProviderId":
						pacProviderId=response[i][jsonKeyProviderId];
					break;
					case "gpProviderId":
						gpProviderId=response[i][jsonKeyProviderId];
					break;
				}
				col = col + "<br>"
					+ "<input type='button' class='right' id='" + deleteButtonId + "' value='Delete'/>";			
			}
		}		
		col = col + "</td>";
		
		$(document).on("click",colIdSpan, function(){
			
			colDialogId = "#"+ fieldName + "-" + $(this).attr('id').split("-")[1] + "-dialog";
			retrieveDetailHistory(colDialogId, jsonList);			
			
		});
		
		tr += col;
	}
	
	if(active=='enable'){
		var endCol = "<td><select id='" + fieldName + "'  multiple='multiple'>";
	}
	else{
		var endCol = "<td><select id='" + fieldName + "'  multiple='multiple' disabled='true'>";
	}
	endCol = endCol + populateDropdownList(jsonList[0]);
	
	if(saveEnable=='saveEnable'){
		endCol = endCol + "</select><br>"
		+ "<input type='button' class='right' id='" + saveButtonId + "' value='Save'/>"
		+ "</td>";
	}
	else{
		endCol = endCol + "</select></td>";
	}
	
	tr += endCol;
	tr +="</tr>";
	
	return tr;
}

//loading gif for every ajax call
$(document).ajaxStart(function () {
	$('#loading_div').html("<img src='image/loading.gif' />");
	$('#loading_div').show();
}).ajaxStop(function () {
	$('#loading_div').html("");
	$('#loading_div').hide();	    
});