jaundiceList = '[{"0":"","1":"নাই","2":"+ (অল্প)","3":"++ (মোটামুটি)","4":"+++ (অধিক)"}]';
jaundicePCJS = JSON.parse(jaundiceList);

diabetesList = '[{"0":"","1":"নাই","2":"আছে"}]';
diabetesPCJS = JSON.parse(diabetesList);

//methodList = '[{"0":"","1":"খাবার বড়ি  \'ব\'","2":"কনডম  \‘ক\‘","3":"ছেড়ে দিয়েছেন  \‘ছে\‘","4":"অন্য পদ্ধতি নিয়েছেন  \‘অ\‘"}]';
methodList = '[{"0":"","1":"খাবার বড়ি","2":"কনডম","3":"ছেড়ে দিয়েছেন","4":"অন্য পদ্ধতি নিয়েছেন"}]';
//methodList = '[{"0":"","1":"খাবার বড়ি","2":"কনডম"}]';
methodPCJS = JSON.parse(methodList);

pillMethodList = '[{"0":"","1":"সুখী বড়ি","10":"আপন বড়ি","999":"অন্যান্য বড়ি"}]';
pillMethodPCJS = JSON.parse(pillMethodList);

pillAmount = '[{"0":"","1":"1","2":"2","3":"3","99":"মজুদ নাই"}]';
pillAmountPCJS = JSON.parse(pillAmount);

condomAmount = '[{"0":"","12":"12","24":"24","36":"36","99":"মজুদ নাই"}]';
condomAmountPCJS = JSON.parse(condomAmount);

reasonMethod = '[{"0":"","100":"পার্শ্ব প্রতিক্রিয়ার জন্য","101":"সন্তান নিবে","102":"মজুদ সংকট/সরবরাহ নাই","103":"স্বামী বিদেশে/কাছে নাই"}]';
reasonMethodPCJS = JSON.parse(reasonMethod);

otherMethod = '[{"0":"","3":"ইনজেকটেবলস","4":"আইইউডি","6":"ইমপ্ল্যান্ট","7":"স্থায়ী পদ্ধতি (পুরুষ)","8":"স্থায়ী পদ্ধতি(মহিলা)","9":"অন্য যে কোন অবস্থা"}]';
otherMethodPCJS = JSON.parse(otherMethod);

/**
 * saving pill, condom distribution information
 */

function insertPillCondomInfo() {

	var pillCondomObj = new Object();
	pillCondomObj = setCommonParam();

	pillCondomObj.healthId = $('#healthid').text();
	pillCondomObj.providerId = $('#providerid').text();
	pillCondomObj.mobileNo = $('#cellNo').val();
	pillCondomObj.screening = $('#PCScreening').prop('checked') ? 1 : 2;
	pillCondomObj.visitDate = $('#PCVisitDate').val();
	pillCondomObj.bpSystolic = $('#PCbpSystolic').val();
	pillCondomObj.bpDiastolic = $('#PCbpDiastolic').val();
	pillCondomObj.jaundice = $('#PCJaundice').val();
	pillCondomObj.diabetes = $('#PCDiabetes').val();
	
	if($('#PCMethod').val()=='2'){
		pillCondomObj.methodType = "2";
		pillCondomObj.amount = ($('#PCAmount').val()==0?"":($('#PCAmount').val()==99?"0":$('#PCAmount').val()));
		
	}
	else{
		pillCondomObj.methodType = $('#PCMethodType').val();
		if($('#PCMethod').val()=='1'){
			pillCondomObj.amount = ($('#PCAmount').val()==0?"":($('#PCAmount').val()==99?"0":$('#PCAmount').val()));//$('#PCAmount').val();
		}
		else{
			pillCondomObj.amount = "";
		}
	}
		
	pillCondomObj.sateliteCenterName = "";
	pillCondomObj.client = "1";
	
	pillCondomObj.pillCondomLoad = "";
	
	if($('#PCMethod').val() == 0){
		callDialog(selectMethod);
	}
	else if($('#PCMethod').val() <= 4){
		if(pillCondomObj.methodType == 0 || pillCondomObj.methodType == ""){
			callDialog(selectSpecificMethod);
		}
		else if(pillCondomObj.methodType >= 1 && pillCondomObj.methodType <= 2){
			//if(pillCondomObj.amount=="" || pillCondomObj.amount == 0){
			if(pillCondomObj.amount==""){
				callDialog(methodAmount);				
			}
			else{
				confirmationBox(pillCondomObj);
				//pillCondomInfo(pillCondomObj);
			}
		}
		else{
			confirmationBox(pillCondomObj);
			//pillCondomInfo(pillCondomObj);
		}		
	}
	//pillCondomInfo(pillCondomObj);
}

/**
 * retrieve pill, condom information
 */
function retrievePillCondomInfo() {
	
	var pillCondomObj = new Object();
	pillCondomObj = setCommonParam();

	pillCondomObj.healthId = $('#healthid').text();
	pillCondomObj.pillCondomLoad = "retrieve";
	
	pillCondomInfo(pillCondomObj);	
}

/**
 * delete last pill, condom information
 */

function deleteLastPillCondomInfo(){
	 
	var pillCondomObj = new Object();
	pillCondomObj = setCommonParam();
	
	pillCondomObj.healthId = $('#healthid').text();
	pillCondomObj.pillCondomLoad = "delete";
	
	pillCondomInfo(pillCondomObj);	
}


/**
 * insert and retrieve pillCondom information in a single AJAX call
 */
function pillCondomInfo(pillCondomObj) {
		
	if (pillCondomObj.healthId != "") {
		$.ajax({
			type : "POST",
			url : "pillcondom",
			timeout:60000, //60 seconds timeout
			dataType : "json",
			data : {"PillCondomInfo" : JSON.stringify(pillCondomObj)},
			success : function(response) {
				parsePillCondomInfo(response);									
			},
			error : function(xhr, status, error) {
				alert(status);
				alert(error);
			}
		});		
	}	
}

/**
 * populating pill, condom register's table dynamically
 */
function parsePillCondomInfo(response) {
	
	if(response.regSerialNo!="" && response.regDate!=""){
		$("#regNumber").text(en_to_ben_number_conversion((response.regSerialNo<=9 ? ("0" + response.regSerialNo): response.regSerialNo) + "/" + moment(response.regDate).format("YY")).toString());
		$("#regDate").text(en_to_ben_number_conversion(moment(response.regDate).format("DD/MM/YYYY").toString()));
	}
	else{
		$("#regNumber").text("");
		$("#regDate").text("");
	}
	
	$('#content').empty();
	
	var divPillCondom = $("<div id='pillCondom' class='person_ANCdetail'>");
	
	var headerContent = $("<table><tbody><tr><td><b>জীবিত সন্তান সংখ্যা</b>&nbsp;&nbsp;&nbsp;&nbsp;");
	headerContent.append("<b>ছেলে</b>&nbsp;<input type='text' id='pillCondomBoy' maxlength='2' size='2' value='" + $('#boy').val() + "' disabled='true'/>&nbsp;&nbsp;" 
			+ "<b>মেয়ে</b>&nbsp;<input type='text' id='pillCondomGirl' maxlength='2' size='2' value='" + $('#girl').val() + "' disabled='true'/>"
			+ "</td></tr></tbody></table>");

	divPillCondom.append(headerContent);
	divPillCondom.append("<br>");
		
	var tablePillCondom = $("<table id='PillCondomVisitTable' border='1px'><tbody>");
	var tr, col, endCol, i;

	// row 1
	tr = $("<tr><td>পরিদর্শন </td>");
	for (i = 1; i <= response.count; i++) {
		col = "<td>" + en_to_ben_number_conversion(i.toString()) + "</td>";
		tr.append(col);
	}
	endCol = "<td>" + en_to_ben_number_conversion(((response.count)+1).toString())
			+ "</td>";
	tr.append(endCol);
	
	tr.append("</tr>");
	tablePillCondom.append(tr);
	
	//row 2
	tr = $("<tr><td>গ্রহনের তারিখ </td>");
	for (i = 1; i <= response.count; i++) {
		col = "<td id='PCVisitDate-" + i + "'>"
			+ moment(response[i].visitDate).format("DD MMM YYYY") + "</td>";
		
		tr.append(col);
	}
	
	endCol = "<td><input type='text' id='PCVisitDate' maxlength='11' size='11'/>";	
	tr.append(endCol);
	
	tr.append("</td></tr>");
	tablePillCondom.append(tr);
	
	//row 3
	tr = $("<tr><td>বাছাইকরণ (উপযুক্ততা যাচাই)	</td>");

	for (i = 1; i <= response.count; i++) {
		col = "<td id='PCScreening-" + i + "'>"
			+ (response[i].screening == 1 ? 'হ্যাঁ' : 'না') + "</td>";
		tr.append(col);		
	}
	endCol = "<td><input type='checkbox' id='PCScreening'/></td>";
	tr.append(endCol);
	
	tr.append("</tr>");
	tablePillCondom.append(tr);
	
	//row 4
	tr = $("<tr><td>রক্তচাপ (মিঃ মিঃ মার্কারি) </td>");
	
	for (i = 1; i <= response.count; i++) {
		slash = (response[i].bpSystolic!="" || response[i].bpDiastolic!="")?"/":"";	
		
		col = "<td id='PCbp-" + i + "'>"
			+ en_to_ben_number_conversion((response[i].bpSystolic).toString())
			+ slash
			+ en_to_ben_number_conversion((response[i].bpDiastolic).toString())
			+ "</td>";
		
		tr.append(col);
	}
	endCol = "<td><input type='text' id='PCbpSystolic' maxlength='3' size='3'/> / <input type='text' id='PCbpDiastolic' maxlength='3' size='3'/></td>";
	tr.append(endCol);
	
	tr.append("</tr>");
	tablePillCondom.append(tr);
	
	//row 5
	tr = $("<tr><td>জন্ডিস </td>");
	for (i = 1; i <= response.count; i++) {
		
		col = "<td id='PCJaundice-" + i + "'>"
			+ jaundicePCJS[0][response[i].jaundice] + "</td>";
		tr.append(col);
	}
	
	endCol = "<td><select id='PCJaundice'>";
	endCol = endCol + populateDropdownList(jaundicePCJS[0]);
	endCol = endCol + "</select></td>";
		
	tr.append(endCol);
	
	tr.append("</tr>");
	tablePillCondom.append(tr);
	
	//row 6
	tr = $("<tr><td>ডায়াবেটিস </td>");
	for (i = 1; i <= response.count; i++) {
		
		col = "<td id='PCDiabetes-" + i + "'>"
			+ diabetesPCJS[0][response[i].diabetes] + "</td>";
		tr.append(col);
	}
	
	endCol = "<td><select id='PCDiabetes'>";
	endCol = endCol + populateDropdownList(diabetesPCJS[0]);
	endCol = endCol + "</select></td>";
		
	tr.append(endCol);
	
	tr.append("</tr>");
	tablePillCondom.append(tr);
	
	
	//row 7
	tr = $("<tr><td> পদ্ধতি </td>");
	for (i = 1; i <= response.count; i++) {
		var value = "";
		//if(response[i].methodType > 0 && response[i].methodType < 3){
		if(response[i].methodType == 1 || response[i].methodType == 10 || response[i].methodType == 999){
			value = value + pillMethodPCJS[0][response[i].methodType] 
					+ " - " + (response[i].amount==""? "" :(response[i].amount=="0"?pillAmountPCJS[0]["99"]:en_to_ben_number_conversion((response[i].amount).toString()))) 
					+ ((response[i].amount!="" && response[i].amount!="0")?" চক্র":"");
		}
		else if(response[i].methodType==2){
			value = value + "কনডম"
			+ " - " + (response[i].amount==""?"" :(response[i].amount=="0"?condomAmountPCJS[0]["99"]:en_to_ben_number_conversion((response[i].amount).toString())))
			+ ((response[i].amount!="" && response[i].amount!="0")?" পিস":"");
		}
		else if(response[i].methodType < 100){
			value = value + "অন্য পদ্ধতি নিয়েছেন<br>"
			+ "(" + otherMethodPCJS[0][response[i].methodType] + ")";
		}
		else if(response[i].methodType > 99){
			value = value + "ছেড়ে দিয়েছেন<br>"
			+ "(" + reasonMethodPCJS[0][response[i].methodType] + ")";
		}
		
		col = "<td id='PCMethod-" + i + "'>"
			+ value + "</td>";
		tr.append(col);
	}
			
	endCol = "<td><select id='PCMethod'>";
	endCol = endCol + populateDropdownList(methodPCJS[0]);
	endCol = endCol + "</select>&nbsp;<br><span id='methodDetail'></span>";
	endCol = endCol + "<hr><input type='button' class='right' id='saveButtonPC' value='Save'/>";
	endCol = endCol + "</td>";		
			
	tr.append(endCol);
	
	tr.append("</tr>");
	tablePillCondom.append(tr);
		
	tablePillCondom.append("</tbody></table>");
	divPillCondom.append(tablePillCondom);
	divPillCondom.append("</div>");
	$('#content').append(divPillCondom);
	
	$('#PCVisitDate').combodate({
		format : "YYYY-MM-DD",
		template : "DD MMM YYYY",
		smartDays : true,
		value : moment()
	});	
}

// Only allowing numeric values for input fields and convert that to Bangla
$(document).on("keyup", '#PCbpSystolic', function() {
	$(this).val($(this).val().replace(/[^\d]/, ''));
});

$(document).on("keyup", '#PCbpDiastolic', function() {
	$(this).val($(this).val().replace(/[^\d]/, ''));
});



$(document).on("change", '#PCMethod', function() {
	handleMethodDetail();		
});


$(document).on("focus", '#PCMethod', function() {
	if(($('#PCbpSystolic').val()!="" && $('#PCbpSystolic').val()>=140) || ($('#PCbpDiastolic').val()!="" && $('#PCbpDiastolic').val()>=90) || ($('#PCJaundice').val()!= 0 && $('#PCJaundice').val()!= 1)){
		$("select#PCMethod option[value='1']").remove();
	}
	else{
		$('#PCMethod').empty();    								
    	$('#PCMethod').append(populateDropdownList(methodPCJS[0]));   	
	}	
});

$(document).on("change", '#PCJaundice', function() {
	if(($('#PCJaundice').val()!= 0 && $('#PCJaundice').val()!= 1)){
		
		if($("#PCMethod").val()==1){
			$('#PCMethod').empty();    								
	    	$('#PCMethod').append(populateDropdownList(methodPCJS[0]));
			$("#PCMethod option[value=0]").prop('selected', true);
			handleMethodDetail();
		}
	}		
});


$(document).on("keyup", '#PCbpSystolic,#PCbpDiastolic', function() {
	if(($('#PCbpSystolic').val()!="" && $('#PCbpSystolic').val()>=140) || ($('#PCbpDiastolic').val()!="" && $('#PCbpDiastolic').val()>=90)){
		if($("#PCMethod").val()==1){
			$('#PCMethod').empty();    								
	    	$('#PCMethod').append(populateDropdownList(methodPCJS[0]));
			$("#PCMethod option[value=0]").prop('selected', true);
			handleMethodDetail();
		}
	}	
});

function handleMethodDetail(){
	$('#methodDetail').empty();
	
	var content = "";
	
	switch($('#PCMethod').val()){
		case '0':
			$('#methodDetail').html("");
			break;
		case '1':
			content = content + "<select id='PCMethodType'>";
			content = content + populateDropdownList(pillMethodPCJS[0]) + "</select> &nbsp;";
			content = content + "পরিমান <select id='PCAmount'>";
			content = content + populateDropdownList(pillAmountPCJS[0]);
			content = content + "</select> চক্র";
			break;
		case '2':
			content = content + "পরিমান <select id='PCAmount'>";
			content = content + populateDropdownList(condomAmountPCJS[0]);
			content = content + "</select> পিস";
			break;
		case '3':
			content = content + "কারণ <select id='PCMethodType'>";
			content = content + populateDropdownList(reasonMethodPCJS[0]) + "</select> &nbsp;";
			break;
		case '4':
			content = content + "অন্য পদ্ধতি <select id='PCMethodType'>";
			content = content + populateDropdownList(otherMethodPCJS[0]) + "</select> &nbsp;";
			break;
	}	
	$('#methodDetail').append(content);	
}

/*
$(document).on("click", '#PCScreening', function (){
	if($('#PCScreening').prop('checked')){
		
		$('#pillCondomScreening').empty();
		
		var pillCondomScreeningTable = $("<table border='1px'><tbody>");
		var tr = $("<tr>");
		tr.append("<td> ১. আপনার শেষ সন্তান কি বুকের দুধ খায়? (যদি শেষ সন্তানের বয়স ৬ মাস বা তার কম হয়)</td>");
		tr.append("<td><input type='radio' name='q1' value='1'/> হ্যাঁ <br> <input type='radio' name='q1' value='2'/> না</td>");
		tr.append("</tr>");
		
		pillCondomScreeningTable.append(tr);
		
		tr = $("<tr>");
		tr.append("<td> ২. আপনি কি ধূমপান করেন অথবা পানের সাথে জর্দা খান?</td>");
		tr.append("<td><input type='radio' name='q2' value='1'/> হ্যাঁ <br> <input type='radio' name='q2' value='2'/> না</td>");
		tr.append("</tr>");
		
		pillCondomScreeningTable.append(tr);
		
		tr = $("<tr>");
		tr.append("<td> ৩. আপনার শেষ সন্তান কি চার সপ্তাহের আগে হয়েছে ( মহিলা কি মনে করেন তার পেটে সন্তান এসে গেছে )?</td>");
		tr.append("<td><input type='radio' name='q3' value='1'/> হ্যাঁ  <br> <input type='radio' name='q3' value='2'/> না</td>");
		tr.append("</tr>");
		
		pillCondomScreeningTable.append(tr);
		
		tr = $("<tr>");
		tr.append("<td> ৪. আপনার স্তনে কি চাকা আছে?</td>");
		tr.append("<td><input type='radio' name='q4' value='1'/> হ্যাঁ  <br><input type='radio' name='q4' value='2'/> না</td>");
		tr.append("</tr>");
		
		pillCondomScreeningTable.append(tr);
		
		tr = $("<tr>");
		tr.append("<td> ৫. গত এক বছরের মধ্যে কোন সময় আপনার চোখ অথবা গায়ের রং হলুদ হয়ে গিয়েছিল কি?</td>");
		tr.append("<td><input type='radio' name='q5' value='1'/> হ্যাঁ <br> <input type='radio' name='q5' value='2'/> না</td>");
		tr.append("</tr>");
		
		pillCondomScreeningTable.append(tr);
		
		tr = $("<tr>");
		tr.append("<td> ৬. আপনার দুই মাসিকের মধ্যবর্তী সময়ে রক্ত যায় কি ? সহবাসের পর রক্ত যায় কি?</td>");
		tr.append("<td><input type='radio' name='q6' value='1'/> হ্যাঁ <br> <input type='radio' name='q6' value='2'/> না</td>");
		tr.append("</tr>");
		
		pillCondomScreeningTable.append(tr);

		tr = $("<tr>");
		tr.append("<td> ৭. সামান্য কাজের পর আপনার বুকে ব্যাথা বা শ্বাস কষ্ট হয় কি?</td>");
		tr.append("<td><input type='radio' name='q7' value='1'/> হ্যাঁ <br> <input type='radio' name='q7' value='2'/> না</td>");
		tr.append("</tr>");
		
		pillCondomScreeningTable.append(tr);

		tr = $("<tr>");
		tr.append("<td> ৮. আপনার কি ঘন ঘন খুব বেশি মাথা ব্যাথা হয় ও চোখে ঝাপসা দেখেন কি?</td>");
		tr.append("<td><input type='radio' name='q8' value='1'/> হ্যাঁ <br> <input type='radio' name='q8' value='2'/> না</td>");
		tr.append("</tr>");
		
		pillCondomScreeningTable.append(tr);

		tr = $("<tr>");
		tr.append("<td> ৯. আপনার পায়ের শিরাগুলো ফুলে গিয়ে ব্যাথা করে কি?</td>");
		tr.append("<td><input type='radio' name='q9' value='1'/> হ্যাঁ <br> <input type='radio' name='q9' value='2'/> না</td>");
		tr.append("</tr>");
		
		pillCondomScreeningTable.append(tr);

		tr = $("<tr>");
		tr.append("<td> ১০. কখনও কি আপনার যে কোন পায়ে বেশ কয়েক দিন ধরে প্রচণ্ড ব্যাথা হয়েছিল?</td>");
		tr.append("<td><input type='radio' name='q10' value='1'/> হ্যাঁ  <br> <input type='radio' name='q10' value='2'/> না</td>");
		tr.append("</tr>");
		
		pillCondomScreeningTable.append(tr);

		tr = $("<tr>");
		tr.append("<td> ১১. কোন স্বাস্থ্য কর্মী বা ডাক্তার আপনাকে কি জানিয়েছিল যে আপনার বহুমূত্র বা ডায়াবেটিস আছে?</td>");
		tr.append("<td><input type='radio' name='q11' value='1'/> হ্যাঁ <br> <input type='radio' name='q11' value='2'/> না</td>");
		tr.append("</tr>");
		
		pillCondomScreeningTable.append(tr);

		tr = $("<tr>");
		tr.append("<td> ১২. আপনার কি মৃগী রোগ আছে অথবা আপনি কি রিফামপিসিনের সাহায্যে যক্ষ্মার চিকিৎসা নিচ্ছেন?</td>");
		tr.append("<td><input type='radio' name='q12' value='1'/> হ্যাঁ <br> <input type='radio' name='q12' value='2'/> না</td>");
		tr.append("</tr>");
		
		pillCondomScreeningTable.append(tr);
		
		pillCondomScreeningTable.append("<tr><td></td><td><input type='button' id='popupButtonPCScreening' class='right' value='OK'></td>");
		
		pillCondomScreeningTable.append("</tbody></table>");		
		
		$('#pillCondomScreening').append(pillCondomScreeningTable);
		
		$('#pillCondomScreening').show();
	}
	else{
		$('#methodDetail').empty;
		$("#PCMethod").empty();
		$("#PCMethod").append(populateDropdownList(methodPCJS[0]));
		$('#pillCondomScreening').empty();		
	}

});*/

function confirmationBox(pillCondomObj){
	
	var userVal = "";
	userVal = userVal + "<table class='list'><tr><td>গ্রহনের তারিখ: " + moment(pillCondomObj.visitDate).format("DD MMM YYYY") + "</td></tr>";
	userVal = userVal + "<tr><td>বাছাইকরণ (উপযুক্ততা যাচাই): "+(pillCondomObj.screening=="1" ? "হ্যাঁ" : "না")+"</tr></td>";
	userVal = userVal + "<tr><td>" +"রক্তচাপ (মিঃ মিঃ মার্কারি): " + en_to_ben_number_conversion((pillCondomObj.bpSystolic).toString()) + " / " + en_to_ben_number_conversion((pillCondomObj.bpDiastolic).toString()) + "</td></tr>";
	userVal = userVal +"<tr><td>"+"জন্ডিস: "+ jaundicePCJS[0][pillCondomObj.jaundice] +"</td></tr>";
	userVal = userVal +"<tr><td>"+"ডায়াবেটিস: "+ diabetesPCJS[0][pillCondomObj.diabetes] +"</td></tr>";
	
	var value = "";
	//if(pillCondomObj.methodType > 0 && pillCondomObj.methodType < 3){
	if(pillCondomObj.methodType == 1 || pillCondomObj.methodType == 10 || pillCondomObj.methodType == 999){
		value = value + pillMethodPCJS[0][pillCondomObj.methodType] 
				+ " - " + (pillCondomObj.amount==""? "" :(pillCondomObj.amount=="0"?pillAmountPCJS[0]["99"]:en_to_ben_number_conversion((pillCondomObj.amount).toString()))) 
				+ ((pillCondomObj.amount!="" && pillCondomObj.amount!="0")?" চক্র":"");
	}
	else if(pillCondomObj.methodType==2){
		value = value + "কনডম"
		+ " - " + (pillCondomObj.amount==""? "" :(pillCondomObj.amount=="0"?condomAmountPCJS[0]["99"]:en_to_ben_number_conversion((pillCondomObj.amount).toString()))) 
		+ ((pillCondomObj.amount!="" && pillCondomObj.amount!="0")?" পিস":"");
	}
	else if(pillCondomObj.methodType < 100){
		value = value + "অন্য পদ্ধতি নিয়েছেন<br>"
		+ "(" + otherMethodPCJS[0][pillCondomObj.methodType] + ")";
	}
	else if(pillCondomObj.methodType > 99){
		value = value + "ছেড়ে দিয়েছেন<br>"
		+ "(" + reasonMethodPCJS[0][pillCondomObj.methodType] + ")";
	}
	
	userVal = userVal + "<tr><td>পদ্ধতি: " + value + "</tr></td>";
	userVal = userVal + "</table>";
	
    $('<div></div>').dialog({
        modal: true,
        width: 450,
        hide: {effect: 'fade', duration: 500},
        show: {effect: 'fade', duration: 600},
        title: "নিশ্চিত করুন",
        open: function () {
            $(this).html(userVal);
        },
        buttons: {
        	 "Save" : function () {      
        		 pillCondomInfo(pillCondomObj);
             	$(this).dialog("close");
             },
             "Cancel": function () {
        		$(this).dialog("close");
        	}
        }
    });
}