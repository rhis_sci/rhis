var isCurrentVisitFixed = "";

var firstFixedVisit = false;
var secondFixedVisit = false;
var thirdFixedVisit = false;

var firstFixedVisitRange = ["",""];
var secondFixedVisitRange = ["",""];
var thirdFixedVisitRange = ["",""];


IUDFollowupproviderAsAttendant = ["#IUDFollowupattendantName"];
IUDFollowupiudRemoved = ["#IUDFollowupiudRemoverName,#IUDFollowupiudRemoverDesignation,#IUDFollowupiudRemoveDate"];

//treatmentIUDFollowupJS = JSON.parse(treatment);

complicationIUDFollowupList = '[{"1":"অস্বাভাবিক রক্তস্রাব","2":"তলপেটে মোচড়ানো ব্যথা","3":"আইইউডি বের হয়ে যাওয়া","4":"জরায়ু ছিদ্র হয়ে যাওয়া","5":"সুতা পাওয়া যাচ্ছে না",'
							  + '"6":"সুতা ছোট","7":"সুতা বড়","8":"গর্ভধারণ","9":"তলপেটে প্রদাহ","10":"যোনি পথের স্রাব","11":"অন্যান্য"}]';
complicationIUDFollowupJS = JSON.parse(complicationIUDFollowupList);

managementIUDFollowupList = '[{"1":"আইইউডি পরানোর আগে এবং পরে গ্রহীতাকে এ বিষয়ে কাউন্সেলিং করুন","2":"গ্রহীতাকে আয়রণ সম্বলিত পুষ্টিকর খাবার খেতে পরামর্শ দিন",'
	 						+ '"3":"ক্ষত দেখা দিলে ক্ষতের আকার ও রক্তপাতের পরিমাণের উপর ভিত্তি করে রিপেয়ার করুন","4":"রক্তস্রাব অসহ্য মনে হলে আইইউডি খুলে দিন",'
	 						+ '"5":"রক্তচাপ,নাড়ীর গতি ও তাপমাত্রা দেখুন","6":"গর্ভবতী কি-না পরীক্ষা করে দেখুন",'
	 						+ '"7":"প্রচন্ড ব্যথা হলে ব্যথানাশক ইনজেকশন দিন","8":"প্রয়োজনীয় ইতিহাস নিন ও শারীরিক পরীক্ষা করুন","9":"অন্যান্য"}]';
managementIUDFollowupJS = JSON.parse(managementIUDFollowupList);

iudRemoverDesignationIUDFollowupList = '[{"0":"","1":"পরিবার কল্যাণ পরিদর্শিকা","2":"সাব-অ্যাসিস্ট্যান্ট কমিউনিটি মেডিকেল অফিসার","3":"মেডিকেল অফিসার (এমসিএইচ-এফপি)","4":"মেডিকেল অফিসার (ক্লিনিক)"}]';
iudRemoverDesignationIUDFollowupJS = JSON.parse(iudRemoverDesignationIUDFollowupList);

removeReasonIUDFollowupList = '[{"1":"গ্রহীতা কোন কারনে আইইউডি ব্যবহার করতে না চাওয়া এবং তা খোলার ইচ্ছা প্রকাশ করা","2":"গ্রহীতা সন্তান নিতে চাওয়া","3":"আইইউডি\'র মেয়াদ শেষ হয়ে যাওয়া",'
							+ '"4":"গ্রহীতার মেনোপেজ বা মাসিক বন্ধ হয়ে যাওয়া","5":"আইইউডি সংক্রান্ত কোন জটিলতা বা সমস্যা","6":"অসুস্থতা","7":"অন্যান্য"}]';
removeReasonIUDFollowupJS = JSON.parse(removeReasonIUDFollowupList);

iudFPMethodList = '[{"0":"","1":"খাবার বড়ি (সুখী)","10":"খাবার বড়ি (আপন)","2":"কনডম","3":"ইনজেকটেবলস","8":"ইসিপি"}]';
iudFPMethodJS = JSON.parse(iudFPMethodList);

//treatmentIUDFollowupList = '[{"1":"প্যারাসিটামল ৫০০ মি.গ্রা. ১টি/২টি ৮ ঘন্টা পর পর ভরা পেটে","2":"আইবুপ্রোফেন ৪০০ মি.গ্রা. ১২ ঘন্টা পর পর ভরা পেটে","3":"ফেরাস সালফেট ৩০০ মি.গ্রা. ১-২ মাস","4":"ডক্সিসাইকিন ১০০ মি.গ্রা. ২ ঘন্টা পর পর"}]';
treatmentIUDFollowupList = '[{"1":"প্যারাসিটামল ৫০০ মি.গ্রা.","2":"আইবুপ্রোফেন ৪০০ মি.গ্রা.","3":"ফেরাস সালফেট ৩০০ মি.গ্রা.","4":"ডক্সিসাইক্লিন ১০০ মি.গ্রা."}]';
treatmentIUDFollowupJS = JSON.parse(treatmentIUDFollowupList);

referReasonList = '[{"1":"তলপেটে প্রচন্ড ব্যথা","2":"অতিরিক্ত রক্তক্ষরণ","3":"সুতা হারিয়ে যাওয়া অথবা ছোট বা বড় হয়ে যাওয়া","4":"পদ্ধতি পরিবর্তন (ইমপ্ল্যান্ট বা অন্যান্য)","5":"অন্যান্য"}]';
referReasonIUDFollowupJS = JSON.parse(referReasonList);

/**
 * saving iud followup's information
 */

function insertIUDFollowupInfo() {

	var iudFollowupObj = new Object();
	iudFollowupObj = setCommonParam();

	iudFollowupObj.healthId = $('#healthid').text();
	iudFollowupObj.providerId = $('#providerid').text();
	iudFollowupObj.iudCount = $('#IUDiudCount').text();
	
	iudFollowupObj.followupDate = $('#IUDFollowupfollowupDate').val();
	iudFollowupObj.allowance = $('#IUDFollowupallowance').prop('checked')? 1 : 2;//$('#IUDFollowupallowance').val();
	iudFollowupObj.complication = $('#IUDFollowupcomplication').multipleSelect("getSelects");
	iudFollowupObj.treatment = $('#IUDFollowuptreatment').multipleSelect("getSelects");
	iudFollowupObj.management = $('#IUDFollowupmanagement').multipleSelect("getSelects");
	iudFollowupObj.attendantName = $('#IUDFollowupattendantName').val().toUpperCase();
	iudFollowupObj.providerAsAttendant = $('#IUDFollowupproviderAsAttendant').prop('checked')? 1 : 2;
	if($('#IUDFollowupiudRemoved').prop('checked')==1){
		iudFollowupObj.iudRemoverName = $('#IUDFollowupiudRemoverName').val().toUpperCase();
		iudFollowupObj.iudRemoverDesignation = $('#IUDFollowupiudRemoverDesignation').val();
		iudFollowupObj.iudRemoveDate = $('#IUDFollowupiudRemoveDate').val();
	}
	else{
		iudFollowupObj.iudRemoverName = "";
		iudFollowupObj.iudRemoverDesignation = "";
		iudFollowupObj.iudRemoveDate = "";
	}
	iudFollowupObj.iudRemoveReason = $('#IUDFollowupiudRemoveReason').multipleSelect("getSelects");
	iudFollowupObj.fpMethod = $('#IUDFollowupfpMethod').val();
	iudFollowupObj.fpAmount = $('#IUDFollowupfpAmount').val();
	iudFollowupObj.refer = $('#IUDFollowuprefer').prop('checked') ? 1 : 2;
	iudFollowupObj.referCenterName = $('#IUDFollowupreferCenterName').val();
	iudFollowupObj.referReason = $('#IUDFollowupreferReason').multipleSelect("getSelects");
	iudFollowupObj.monitoringOfficerName = $('#IUDFollowupmonitoringOfficerName').val();
	iudFollowupObj.comment = $('#IUDFollowupcomment').val();
	iudFollowupObj.isFixedVisit = isCurrentVisitFixed;//$('#IUDFollowupisFixedVisit').prop('checked') ? 1 : 2;

	iudFollowupObj.sateliteCenterName = "";
	iudFollowupObj.client = "1";
	
	iudFollowupObj.iudLoad = "";
	iudFollowupObj.iudFollowupLoad = "insert";
	
	confirmationBoxIUDFollowup(iudFollowupObj);
}

/**
 * retrieve iud followup information
 */

function retrieveIUDFollowupInfo() {
	
	var iudFollowupObj = new Object();
	iudFollowupObj = setCommonParam();

	iudFollowupObj.healthId = $('#healthid').text();
	
	iudFollowupObj.iudLoad = "";
	iudFollowupObj.iudFollowupLoad = "";
	
	iudFollowupInfo(iudFollowupObj);	
}

/**
 * delete last iud followup information
 */

function deleteLastIUDFollowupInfo(){
	 
	var iudFollowupObj = new Object();
	iudFollowupObj = setCommonParam();
	
	iudFollowupObj.healthId = $('#healthid').text();
	
	iudFollowupObj.iudLoad = "";
	iudFollowupObj.iudFollowupLoad = "delete";
	
	iudFollowupInfo(iudFollowupObj);	
}


/**
 * insert and retrieve iud information in a single AJAX call
 */

function iudFollowupInfo(iudFollowupObj) {
	
	if (iudFollowupObj.healthId != "") {
		$.ajax({
			type : "POST",
			url : "iud",
			timeout:60000, //60 seconds timeout
			dataType : "json",
			data : {"iudInfo" : JSON.stringify(iudFollowupObj)},
			success : function(response) {
				parseIUDFollowupInfo(response);									
			},
			error : function(xhr, status, error) {
				alert(status);
				alert(error);
			}
		});		
	}	
}

/**
 * populating iud followup register's table dynamically
 */

function parseIUDFollowupInfo(response) {
	
	resetFixedFollowupInfo();
	
	firstFixedVisitRange[0] =  moment($('#IUDiudImplantDate').val()).add({days:23}).format("DD MMM YYYY");
	firstFixedVisitRange[1] =  moment($('#IUDiudImplantDate').val()).add({days:37}).format("DD MMM YYYY");
	secondFixedVisitRange[0] =  moment($('#IUDiudImplantDate').val()).add({months:5}).format("DD MMM YYYY");
	secondFixedVisitRange[1] =  moment($('#IUDiudImplantDate').val()).add({months:7}).format("DD MMM YYYY");
	thirdFixedVisitRange[0] =  moment($('#IUDiudImplantDate').val()).add({months:11}).format("DD MMM YYYY");
	thirdFixedVisitRange[1] =  moment($('#IUDiudImplantDate').val()).add({months:13}).format("DD MMM YYYY");
	
	//suggested followup date
	//var suggestedIUDFollowupDate1 = moment($('#IUDiudImplantDate').val()).add({days:23}).format("DD MMM YYYY") + " - " + moment($('#IUDiudImplantDate').val()).add({days:37}).format("DD MMM YYYY");
	//var suggestedIUDFollowupDate2 = moment($('#IUDiudImplantDate').val()).add({months:5}).format("DD MMM YYYY") + " - " + moment($('#IUDiudImplantDate').val()).add({months:7}).format("DD MMM YYYY");
	//var suggestedIUDFollowupDate3 = moment($('#IUDiudImplantDate').val()).add({months:11}).format("DD MMM YYYY") + " - " + moment($('#IUDiudImplantDate').val()).add({months:13}).format("DD MMM YYYY");
	var suggestedIUDFollowupDate1 = firstFixedVisitRange[0] + " - " + firstFixedVisitRange[1];
	var suggestedIUDFollowupDate2 = secondFixedVisitRange[0] + " - " + secondFixedVisitRange[1];
	var suggestedIUDFollowupDate3 = thirdFixedVisitRange[0] + " - " + thirdFixedVisitRange[1];
		
	$('#iudfollowup').remove();
	
	var divIUDFollowup = $("<div id='iudfollowup' class='person_ANCdetail'>");
	
	var headerContent = $("<table><tbody><tr><td><b>নির্ধারিত ফলোআপের সময়</b></td></tr>");
	headerContent.append("<tr><td><table  width='924px' border='1px'><tbody>" 
				+ "<tr><td> ফলোআপ ১ </td><td> ফলোআপ ২ </td><td> ফলোআপ ৩ </td></tr>"
				+ "<tr>"
				+ "<td class='ideal'><span id='idealIUDFollowup1Date' style='font-size:14px;'>" + suggestedIUDFollowupDate1 + "</span></td>"
				+ "<td class='ideal'><span id='idealIUDFollowup2Date' style='font-size:14px;'>" + suggestedIUDFollowupDate2 + "</span></td>"
				+ "<td class='ideal'><span id='idealIUDFollowup3Date' style='font-size:14px;'>" + suggestedIUDFollowupDate3 + "</span></td>"
				+ "</tr>"
				+ "</tbody></table>");
	headerContent.append("</td></tr></tbody></table>");

	divIUDFollowup.append(headerContent);
	var iudFollowupHeading = $("<div id='subheading' class='subheadingside'> ফলোআপ সেবার তথ্য </div>");
	divIUDFollowup.append(iudFollowupHeading);
	divIUDFollowup.append("<br>");
		
	var tableIUDFollowup = $("<table id='iudFollowupVisitTable' border='1px'><tbody>");
	var tr, col, endCol, i;

	// row 1
	tr = $("<tr><td>ফলোআপ </td>");
	for (i = 1; i <= response.followupCount; i++) {
		//col = "<td>" + en_to_ben_number_conversion(i.toString()) + (response["followup"][i].isFixedVisit==1?" (নির্ধারিত ফলোআপ)":"" )+ "</td>";
		//col = "<td>" + en_to_ben_number_conversion(i.toString()) + (response["followup"][i].isFixedVisit==""?" অনির্ধারিত ফলোআপ":" (নির্ধারিত ফলোআপ - "
		//	  + en_to_ben_number_conversion(response["followup"][i].isFixedVisit.toString())) + ")" +"</td>";
		col = "<td>" + en_to_ben_number_conversion(i.toString()) + (response["followup"][i].isFixedVisit==""?" অনির্ধারিত ফলোআপ":" (নির্ধারিত ফলোআপ)") + "</td>";
		tr.append(col);
		
		switch(response["followup"][i].isFixedVisit){
			case '1':
				firstFixedVisit = true;
				break;
			case '2':
				secondFixedVisit = true;
				break;
			case '3':
				thirdFixedVisit = true;
				break;
		}
	}
	endCol = "<td>" + en_to_ben_number_conversion(((response.followupCount)+1).toString())
			+ "&nbsp;"
			//+ "<input type='checkbox' id='IUDFollowupisFixedVisit'/>"
			//+ "নির্ধারিত ফলোআপ"
			+ "<span id='IUDFollowupisFixedVisitSpan'></span>"
			+ "</td>";
	tr.append(endCol);
	
	tr.append("</tr>");
	tableIUDFollowup.append(tr);
	
	//row 2
	tr = $("<tr><td>তারিখ </td>");
	for (i = 1; i <= response.followupCount; i++) {
		col = "<td id='IUDFollowupfollowupDate-" + i + "'>"
			+ moment(response["followup"][i].followupDate).format("DD MMM YYYY") + "</td>";
		
		tr.append(col);
	}
	
	endCol = "<td><input type='text' id='IUDFollowupfollowupDate' maxlength='11' size='11'/>";	
	tr.append(endCol);
	
	tr.append("</td></tr>");
	tableIUDFollowup.append(tr);
	
	
	//row 3
	tr = $("<tr><td>গ্রহীতার  নাম</td>");
	for (i = 1; i <= response.followupCount; i++) {
		col = "<td>" + $('#clientName').val() + "</td>";
		tr.append(col);
	}
	
	endCol = "<td>" + $('#clientName').val() + "</td>";	
	tr.append(endCol);
	
	tr.append("</td></tr>");
	tableIUDFollowup.append(tr);

	
	//row 4
	tr = $("<tr><td>গ্রহীতার ফলোআপ ভাতা দেয়া হয়েছে</td>");
	for (i = 1; i <= response.followupCount; i++) {
		col = "<td>" 
			//+ en_to_ben_number_conversion(response["followup"][i].allowance.toString()) + " " 
			//+ (response["followup"][i].allowance==""?"":"টাকা") 
			+ (response["followup"][i].allowance==1?'হ্যাঁ':'না')
			+ "</td>";
		tr.append(col);
	}
	
	//endCol = "<td><input type='text' id='IUDFollowupallowance' maxlength='3' size='3'/> টাকা";
	endCol = "<td><input type='checkbox' id='IUDFollowupallowance'/>";
	tr.append(endCol);
	
	tr.append("</td></tr>");
	tableIUDFollowup.append(tr);
	
	//row 5
	tr = $("<tr><td>অসুবিধা</td>");
	for(i=1; i<=response.followupCount; i++){
		var colIdSpan = "#IUDFollowupcomplication-" + i + "-span";
							
		col = "<td id='IUDFollowupcomplication-" + i + "'>"
				 + "<span id='IUDFollowupcomplication-" + i + "-span' style='cursor:pointer'> <strong>"+detail(response["followup"][i].complication)+"</strong> </span>";
		
		col = col + "<div id ='IUDFollowupcomplication-" + i + "-dialog' hidden>" + response["followup"][i].complication + "</div>";
		
		col = col + "</td>";
		
		$(document).on("click",colIdSpan, function(){
			
			colDialogId = "#IUDFollowupcomplication-" + $(this).attr('id').split("-")[1] + "-dialog";
			retrieveDetailHistory(colDialogId, complicationIUDFollowupJS);			
			
		});
		tr.append(col);
	}
	
	endCol = "<td><select id='IUDFollowupcomplication'  multiple='multiple'>";
	endCol = endCol + populateDropdownList(complicationIUDFollowupJS[0]);
	endCol = endCol + "</select></td>";
		
	tr.append(endCol);
	
	tr.append("</tr>");
	tableIUDFollowup.append(tr);
	
	
	// row 6
	tr = $("<tr><td>চিকিৎসা</td>");
	for(i=1; i<=response.followupCount; i++){
		var colIdSpan = "#IUDFollowuptreatment-" + i + "-span";
							
		col = "<td id='IUDFollowuptreatment-" + i + "'>"
				 + "<span id='IUDFollowuptreatment-" + i + "-span' style='cursor:pointer'> <strong>"+detail(response["followup"][i].treatment)+"</strong> </span>";
		
		col = col + "<div id ='IUDFollowuptreatment-" + i + "-dialog' hidden>" + response["followup"][i].treatment + "</div>";
		
		col = col + "</td>";
		
		$(document).on("click",colIdSpan, function(){
			
			colDialogId = "#IUDFollowuptreatment-" + $(this).attr('id').split("-")[1] + "-dialog";
			retrieveDetailHistory(colDialogId, treatmentIUDFollowupJS);			
			
		});
		tr.append(col);
	}
	
	endCol = "<td><select id='IUDFollowuptreatment'  multiple='multiple'>";
	endCol = endCol + populateDropdownList(treatmentIUDFollowupJS[0]);
	endCol = endCol + "</select></td>";
		
	tr.append(endCol);
	
	tr.append("</tr>");
	tableIUDFollowup.append(tr);
	
	// row 7
	tr = $("<tr><td>ব্যবস্থাপনা</td>");
	for(i=1; i<=response.followupCount; i++){
		var colIdSpan = "#IUDFollowupmanagement-" + i + "-span";
							
		col = "<td id='IUDFollowupmanagement-" + i + "'>"
				 + "<span id='IUDFollowupmanagement-" + i + "-span' style='cursor:pointer'> <strong>"+detail(response["followup"][i].management)+"</strong> </span>";
		
		col = col + "<div id ='IUDFollowupmanagement-" + i + "-dialog' hidden>" + response["followup"][i].management + "</div>";
		
		col = col + "</td>";
		
		$(document).on("click",colIdSpan, function(){
			
			colDialogId = "#IUDFollowupmanagement-" + $(this).attr('id').split("-")[1] + "-dialog";
			retrieveDetailHistory(colDialogId, managementIUDFollowupJS);			
			
		});
		tr.append(col);
	}
	
	endCol = "<td><select id='IUDFollowupmanagement'  multiple='multiple'>";
	endCol = endCol + populateDropdownList(managementIUDFollowupJS[0]);
	endCol = endCol + "</select></td>";
		
	tr.append(endCol);
	
	tr.append("</tr>");
	tableIUDFollowup.append(tr);
	
	
	// row 8
	/*var tr = $("<tr><td>আমি ফলোআপ সম্পাদনকারী 	</td>");
	for (i = 1; i <= response.followupCount; i++) {
		col = "<td id='IUDFollowupproviderAsAttendant-" + i + "'>"
			+ (response["followup"][i].providerAsAttendant==1?'হ্যাঁ':'না') + "</td>";
		tr.append(col);		
	}
	
	endCol = "<td><input type='checkbox' id='IUDFollowupproviderAsAttendant'/></td>";
	tr.append(endCol);	
	
	tr.append("</tr>");
	tableIUDFollowup.append(tr);
	*/
	// row 9
	var tr = $("<tr><td>ফলোআপ সম্পাদনকারীর নাম 	</td>");
	for (i = 1; i <= response.followupCount; i++) {
		col = "<td id='IUDFollowupattendantName-" + i + "'>"
			+ response["followup"][i].attendantName.toUpperCase() + "</td>";
		tr.append(col);		
	}
	
	endCol = "<td> <input type='checkbox' id='IUDFollowupproviderAsAttendant'/> আমি &nbsp;&nbsp;"
			+ "<input type='text' id='IUDFollowupattendantName'/></td>";
	tr.append(endCol);	
	
	tr.append("</tr>");
	tableIUDFollowup.append(tr);
	
	// row 10
	var tr = $("<tr><td>আইইউডি খুলে ফেলা হয়েছে 	</td>");
	for (i = 1; i <= response.followupCount; i++) {
		col = "<td id='IUDFollowupiudRemoved-" + i + "'>"
			+ (response["followup"][i].iudRemoveDate!=""||response["followup"][i].iudRemoverName!=""||response["followup"][i].iudRemoverDesignation!="0"?'হ্যাঁ':'না') + "</td>";
		tr.append(col);		
	}
	
	endCol = "<td><input type='checkbox' id='IUDFollowupiudRemoved'/></td>";
	tr.append(endCol);	
	
	tr.append("</tr>");
	tableIUDFollowup.append(tr);
	
	// row 11
	var tr = $("<tr><td>যিনি খুলেছেন তার নাম 	</td>");
	for (i = 1; i <= response.followupCount; i++) {
		col = "<td id='IUDFollowupiudRemoverName-" + i + "'>"
			+ response["followup"][i].iudRemoverName + "</td>";
		tr.append(col);		
	}
	
	endCol = "<td><input type='text' id='IUDFollowupiudRemoverName' disabled/></td>";
	tr.append(endCol);	
	
	tr.append("</tr>");
	tableIUDFollowup.append(tr);
	
	// row 12
	tr = $("<tr><td>যিনি খুলেছেন তার পদবী</td>");
	for (i = 1; i <= response.followupCount; i++) {
		
		col = "<td id='IUDFollowupiudRemoverDesignation-" + i + "'>"
			+ iudRemoverDesignationIUDFollowupJS[0][response["followup"][i].iudRemoverDesignation] + "</td>";
		tr.append(col);
	}
	
	endCol = "<td><select id='IUDFollowupiudRemoverDesignation' disabled>";
	endCol = endCol + populateDropdownList(iudRemoverDesignationIUDFollowupJS[0]);
	endCol = endCol + "</select></td>";
	
	tr.append(endCol);
	
	tr.append("</tr>");
	tableIUDFollowup.append(tr);
	
	//row 13
	tr = $("<tr><td>খুলে ফেলার তারিখ </td>");
	for (i = 1; i <= response.followupCount; i++) {
		col = "<td id='IUDFollowupiudRemoveDate-" + i + "'>"
			+ (response["followup"][i].iudRemoveDate==""?"":moment(response["followup"][i].iudRemoveDate).format("DD MMM YYYY")) + "</td>";
		
		tr.append(col);
	}
	
	endCol = "<td id='IUDFollowupiudRemoveDateColumn' disabled><input type='text' id='IUDFollowupiudRemoveDate' maxlength='11' size='11'/>";	
	tr.append(endCol);
	
	tr.append("</td></tr>");
	tableIUDFollowup.append(tr);
	
	
	//row 14
	tr = $("<tr><td>খুলে ফেলার কারণ</td>");
	for(i=1; i<=response.followupCount; i++){
		var colIdSpan = "#IUDFollowupiudRemoveReason-" + i + "-span";
							
		col = "<td id='IUDFollowupiudRemoveReason-" + i + "'>"
				 + "<span id='IUDFollowupiudRemoveReason-" + i + "-span' style='cursor:pointer'> <strong>"+detail(response["followup"][i].iudRemoveReason)+"</strong> </span>";
		
		col = col + "<div id ='IUDFollowupiudRemoveReason-" + i + "-dialog' hidden>" + response["followup"][i].iudRemoveReason + "</div>";
		
		col = col + "</td>";
		
		$(document).on("click",colIdSpan, function(){
			
			colDialogId = "#IUDFollowupiudRemoveReason-" + $(this).attr('id').split("-")[1] + "-dialog";
			retrieveDetailHistory(colDialogId,removeReasonIUDFollowupJS);			
			
		});
		tr.append(col);
	}
	
	endCol = "<td><select id='IUDFollowupiudRemoveReason'  multiple='multiple' disabled='true'>";
	endCol = endCol + populateDropdownList(removeReasonIUDFollowupJS[0]);
	endCol = endCol + "</select></td>";
		
	tr.append(endCol);
	
	tr.append("</tr>");
	tableIUDFollowup.append(tr);
	
	
	// row 15
	var tr = $("<tr><td>জন্ম নিয়ন্ত্রণ পদ্ধতি দেয়া হয়েছে</td>");
	for (i = 1; i <= response.followupCount; i++) {
		col = "<td id='IUDFollowupfpMethodGiven-" + i + "'>"
			+ (response["followup"][i].fpMethod!="0"||response["followup"][i].fpAmount!=""?'হ্যাঁ':'না') + "</td>";
		tr.append(col);		
	}
	
	endCol = "<td><input type='checkbox' id='IUDFollowupfpMethodGiven' disabled='true'/></td>";
	tr.append(endCol);	
	
	tr.append("</tr>");
	tableIUDFollowup.append(tr);
	
	
	// row 16
	tr = $("<tr><td>নাম</td>");
	for (i = 1; i <= response.followupCount; i++) {
		
		col = "<td id='IUDFollowupfpMethod-" + i + "'>"
			+ iudFPMethodJS[0][response["followup"][i].fpMethod] + "</td>";
		tr.append(col);
	}
	
	endCol = "<td><select id='IUDFollowupfpMethod' disabled>";
	endCol = endCol + populateDropdownList(iudFPMethodJS[0]);
	endCol = endCol + "</select></td>";
	
	tr.append(endCol);
	
	tr.append("</tr>");
	tableIUDFollowup.append(tr);
	
	
	// row 17
	var tr = $("<tr><td>পরিমাণ</td>");
	for (i = 1; i <= response.followupCount; i++) {
		col = "<td id='IUDFollowupfpAmount-" + i + "'>"
			+ response["followup"][i].fpAmount + "</td>";
		tr.append(col);		
	}
	
	endCol = "<td><input type='text' id='IUDFollowupfpAmount' disabled/></td>";
	tr.append(endCol);	
	
	tr.append("</tr>");
	tableIUDFollowup.append(tr);
	
	// row 18
	var tr = $("<tr><td>রেফার	</td>");
	for (i = 1; i <= response.followupCount; i++) {
		col = "<td id='IUDFollowuprefer-" + i + "'>"
			+ (response["followup"][i].refer==1?'হ্যাঁ':'না') + "</td>";
		tr.append(col);		
	}
	
	endCol = "<td><input type='checkbox' id='IUDFollowuprefer'/></td>";
	tr.append(endCol);	
	
	tr.append("</tr>");
	tableIUDFollowup.append(tr);
	
	// row 19
	var tr = $("<tr><td>কেন্দ্রের নাম	</td>");
	referCenterJS = JSON.parse(referCenter);
	for (i = 1; i <= response.followupCount; i++) {
		
		col = "<td id='IUDFollowupreferCenterName-" + i + "'>"
			+ referCenterJS[0][response["followup"][i].referCenterName] + "</td>";
		tr.append(col);
	}
	
	endCol = "<td><select id='IUDFollowupreferCenterName' disabled='true'>";
	endCol = endCol + populateDropdownList(referCenterJS[0]);
	endCol = endCol + "</select></td>";
	
	tr.append(endCol);
	
	tr.append("</tr>");
	tableIUDFollowup.append(tr);
	
	// row 20
	tr = $("<tr><td>কারণ</td>");
	for(i=1; i<=response.followupCount; i++){
		var colIdSpan = "#IUDFollowupreferReason-" + i + "-span";
							
		col = "<td id='IUDFollowupreferReason-" + i + "'>"
				 + "<span id='IUDFollowupreferReason-" + i + "-span' style='cursor:pointer'> <strong>"+detail(response["followup"][i].referReason)+"</strong> </span>";
		
		col = col + "<div id ='IUDFollowupreferReason-" + i + "-dialog' hidden>" + response["followup"][i].referReason + "</div>";
		
		col = col + "</td>";
		
		$(document).on("click",colIdSpan, function(){
			
			colDialogId = "#IUDFollowupreferReason-" + $(this).attr('id').split("-")[1] + "-dialog";
			retrieveDetailHistory(colDialogId, referReasonIUDFollowupJS);			
			
		});
		tr.append(col);
	}
	
	endCol = "<td><select id='IUDFollowupreferReason'  multiple='multiple' disabled='true'>";
	endCol = endCol + populateDropdownList(referReasonIUDFollowupJS[0]);
	endCol = endCol + "</select></td>";					
		
	tr.append(endCol);
	
	tr.append("</tr>");
	tableIUDFollowup.append(tr);
	
	// row 21
	var tr = $("<tr><td>তদারককারী কর্মকর্তার নাম 	</td>");
	for (i = 1; i <= response.followupCount; i++) {
		col = "<td id='IUDFollowupmonitoringOfficerName-" + i + "'>"
			+ response["followup"][i].monitoringOfficerName + "</td>";
		tr.append(col);		
	}
	
	endCol = "<td><input type='text' id='IUDFollowupmonitoringOfficerName' disabled/></td>";
	tr.append(endCol);	
	
	tr.append("</tr>");
	tableIUDFollowup.append(tr);
	
	// row 22
	var tr = $("<tr><td>তদারককারী কর্মকর্তার মন্তব্য</td>");
	
	for (i = 1; i <= response.followupCount; i++) {
		col = "<td id='IUDFollowupcomment-" + i + "'>"
			+ response["followup"][i].comment + "</td>";
		tr.append(col);		
	}
	
	endCol = "<td><textarea rows='1' cols='30'  id='IUDFollowupcomment' disabled/>";
	endCol = endCol + "<br>"
			+ "<input type='button' class='right' id='saveButtonIUDFollowup' value='Save'/>"
			+ "</td>";
	tr.append(endCol);	
	
	tr.append("</tr>");
	tableIUDFollowup.append(tr);
	
	
	tableIUDFollowup.append("</tbody></table>");
	divIUDFollowup.append(tableIUDFollowup);
	divIUDFollowup.append("</div>");
	$('#content').append(divIUDFollowup);
	
	fieldInitIUDFollowup();	
}

// Only allowing numeric values for input fields and convert that to Bangla
$(document).on("keyup", '#IUDFollowupallowance,#IUDFollowupfpAmount', function() {
	$(this).val($(this).val().replace(/[^\d]/, ''));
});

function fieldInitIUDFollowup(){
		
	$('#IUDFollowupfollowupDate,#IUDFollowupiudRemoveDate').combodate({
		format : "YYYY-MM-DD",
		template : "DD MMM YYYY",
		smartDays : true,
		value : moment()
	});
	
	$('#IUDFollowupiudRemoveDateColumn span select option[value=""]').prop('selected', true);
	$('#IUDFollowupiudRemoveDateColumn span select').prop('disabled', true);
	
	fieldId = ["#IUDFollowupcomplication,#IUDFollowuptreatment,#IUDFollowupmanagement,#IUDFollowupiudRemoveReason,#IUDFollowupreferReason"];
	$.each(fieldId, function(key, val) {
		$(val).multipleSelect({
			width : 250,
			position : 'top',
			selectAll: false,
			selectedList: 1,
			maxHeight: 100,
			styler : function() {
				return 'font-size: 18px;';
			}	
		});
	});	
}


function confirmationBoxIUDFollowup(iudFollowupObj){
	
	var userVal = "";
	userVal = userVal + "<table class='list'><tr><td>ফলোআপের তারিখ: " + moment(iudFollowupObj.followupDate).format("DD MMM YYYY");
					 // + (iudFollowupObj.isFixedVisit=="1" ? " (নির্ধারিত)" : " (অনির্ধারিত)") + "</td></tr>";
	userVal = userVal + (iudFollowupObj.isFixedVisit=="" ? " (অনির্ধারিত)" : (" (নির্ধারিত - " + en_to_ben_number_conversion(iudFollowupObj.isFixedVisit.toString()) + ")" )) + "</td></tr>";
	//userVal = userVal + "<tr><td>গ্রহীতার ফলোআপ ভাতা: " + iudFollowupObj.allowance + " টাকা</tr></td>";
	userVal = userVal + "<tr><td>গ্রহীতার ফলোআপ ভাতা দেয়া হয়েছে: " + (iudFollowupObj.allowance==1?"হ্যাঁ":"না") + "</td></tr>";
	
	userVal = userVal + "<table class='list'><tr><td>অসুবিধা:</td></tr> ";
	textVal=iudFollowupObj.complication;
	
	var i;
	var convertedText = "  ";
	
	for(i =0; i < textVal.length; i++){
		convertedText = convertedText +"<tr><td>-"+complicationIUDFollowupJS[0][textVal[i]] +"</td></tr>";				
	}
	userVal+=convertedText;
	
	userVal = userVal + "<table class='list'><tr><td>চিকিৎসা:</tr></td> ";
	textVal=iudFollowupObj.treatment;
	
	convertedText = "  ";
	
	for(i =0; i < textVal.length; i++){
		convertedText = convertedText +"<tr><td>-"+treatmentIUDFollowupJS[0][textVal[i]] +"</td></tr>";				
	}
	userVal+=convertedText;
	
	userVal = userVal + "<table class='list'><tr><td>ব্যবস্থাপনা:</td></tr> ";
	textVal=iudFollowupObj.management;
		
	convertedText = "  ";
	
	for(i =0; i < textVal.length; i++){
		convertedText = convertedText +"<tr><td>-"+managementIUDFollowupJS[0][textVal[i]] +"</td></tr>";				
	}
	userVal+=convertedText;
	
	userVal = userVal + "<tr><td>ফলোআপ সম্পাদনকারীর নাম: " + iudFollowupObj.attendantName + "</tr></td>";
	
	
	userVal = userVal + "<tr><td>আইইউডি খুলে ফেলা হয়েছে : "+($('#IUDFollowupiudRemoved').is(":checked") ? "হ্যাঁ" : "না")+"</td></tr>";
    
    if($('#IUDFollowupiudRemoved').is(":checked")){
    	userVal = userVal + "<tr><td>যিনি খুলেছেন তার নাম: " + iudFollowupObj.iudRemoverName + "</tr></td>";
    	
    	userVal = userVal +"<tr><td>"+ linemark_switch("যিনি খুলেছেন তার পদবী",iudFollowupObj.iudRemoverDesignation,0);		   
		userVal += iudRemoverDesignationIUDFollowupJS[0][iudFollowupObj.iudRemoverDesignation]; 
		userVal+="</td></tr>";
		
		userVal = userVal +"<tr><td>"+ linemark_switch("খুলে ফেলার তারিখ",iudFollowupObj.iudRemoveDate,"");		   
		userVal += moment(iudFollowupObj.iudRemoveDate).format("DD MMM YYYY"); 
		userVal+="</td></tr>";
		   
    	userVal = userVal +"<tr><td>"+linemark_switch("খুলে ফেলার কারণ",iudFollowupObj.iudRemoveReason,"")+"</td></tr>";
    	textVal=iudFollowupObj.iudRemoveReason;
		convertedText = "  ";
	
		for(i =0; i < textVal.length; i++){
			convertedText = convertedText +"<tr><td>-"+ removeReasonIUDFollowupJS[0][textVal[i]] + "</td></tr>";	
		}
		userVal+=convertedText;
		
		userVal = userVal + "<tr><td>জন্ম নিয়ন্ত্রণ পদ্ধতি দেয়া হয়েছে: "+($('#IUDFollowupfpMethodGiven').is(":checked") ? "হ্যাঁ" : "না")+"</td></tr>";
	    
	    if($('#IUDFollowupfpMethodGiven').is(":checked")){
	    	userVal = userVal +"<tr><td>"+ linemark_switch("নাম",iudFollowupObj.fpMethod,0);		   
			userVal += iudFPMethodJS[0][iudFollowupObj.fpMethod]; 
			userVal+="</td></tr>"; 
			   
			userVal = userVal +"<tr><td>"+ linemark_switch("পরিমাণ",iudFollowupObj.fpAmount,"");		   
			userVal += iudFollowupObj.fpAmount; 
			userVal+="</td></tr>";
	    }
	
    }
    
	userVal = userVal + "<tr><td>রেফার: "+(iudFollowupObj.refer=="1" ? "হ্যাঁ" : "না")+"</td></tr>";
    
    if(iudFollowupObj.refer=="1"){
    	   userVal = userVal +"<tr><td>"+ linemark_switch("কেন্দ্রের নাম",iudFollowupObj.referCenterName,0);		   
		   
    	   userVal += referCenterJS[0][iudFollowupObj.referCenterName]; 
		   
    	   userVal+="</td></tr>"; 
		   
    	   userVal = userVal +"<tr><td>"+linemark_switch("কারণ",iudFollowupObj.referReason,"")+"</td></tr>";
    	   textVal=iudFollowupObj.referReason;
		   var i;
		   var convertedText = "  ";
	
		   for(i =0; i < textVal.length; i++){
			   convertedText = convertedText +"<tr><td>-"+ referReasonWIJS[0][textVal[i]] + "</td></tr>";	
		   }
		   userVal+=convertedText;
	
    }   
	
	userVal = userVal + "</table>";
	
	if(moment(iudFollowupObj.followupDate) > moment($('#IUDiudImplantDate').val())){
	    $('<div></div>').dialog({
	        modal: true,
	        width: 450,
	        hide: {effect: 'fade', duration: 500},
	        show: {effect: 'fade', duration: 600},
	        title: "নিশ্চিত করুন",
	        open: function () {
	            $(this).html(userVal);
	        },
	        buttons: {
	        	 "Save" : function () {      
	        		 iudFollowupInfo(iudFollowupObj);
	             	$(this).dialog("close");
	             },
	             "Cancel": function () {
	        		$(this).dialog("close");
	        	}
	        }
	    });
	}
	else{
		callDialog(iudFollowupDateLessThanIUDDate);
	}	
}


$(document).on("click", '#IUDFollowupproviderAsAttendant,#IUDFollowupiudRemoved',function (){
	
	if ($('#IUDFollowupproviderAsAttendant').is(":checked")){
		$('#IUDFollowupattendantName').val($('#providerName').text().toUpperCase());
		$('#IUDFollowupattendantName').prop('disabled', true);
	}
	else{
		$('#IUDFollowupattendantName').val("");
		$('#IUDFollowupattendantName').prop('disabled', false);		
	}
});

$(document).on("click", '#IUDFollowupiudRemoved',function (){
	
	if ($('#IUDFollowupiudRemoved').is(":checked")){
		$('#IUDFollowupiudRemoverName').prop('disabled', false);
		$('#IUDFollowupiudRemoverDesignation').prop('disabled', false);
		$('#IUDFollowupiudRemoveDate').prop('disabled', false);
		$('#IUDFollowupiudRemoveDateColumn span select').prop('disabled', false);
		$('#IUDFollowupiudRemoveReason').multipleSelect("enable");
		$('#IUDFollowupfpMethodGiven').prop('disabled',false);
		//$('#IUDFollowupiudRemoveDateColumn :input').prop('disabled', false);
	}
	else{
		$('#IUDFollowupiudRemoverName').val("");
		$('#IUDFollowupiudRemoverName').prop('disabled', true);
		$('#IUDFollowupiudRemoverDesignation option[value=0]').prop('selected', true);
		$('#IUDFollowupiudRemoverDesignation').prop('disabled', true);
		$('#IUDFollowupiudRemoveDateColumn span select option[value=""]').prop('selected', true);
		$('#IUDFollowupiudRemoveDateColumn span select').prop('disabled', true);
		$('#IUDFollowupiudRemoveReason').multipleSelect("disable");
		$('#IUDFollowupfpMethodGiven').prop('checked',false);
		$('#IUDFollowupfpMethodGiven').prop('disabled',true);
		$('#IUDFollowupfpMethod option[value=0]').prop('selected', true);
		$('#IUDFollowupfpMethod').prop('disabled', true);
		$('#IUDFollowupfpAmount').val("");
		$('#IUDFollowupfpAmount').prop('disabled', true);
		//$('#IUDFollowupiudRemoveDateColumn :input').prop('disabled', true);
	}
});

$(document).on("click", '#IUDFollowupfpMethodGiven',function (){
	
	if ($('#IUDFollowupfpMethodGiven').is(":checked")){
		$('#IUDFollowupfpMethod').prop('disabled', false);
		$('#IUDFollowupfpAmount').prop('disabled', false);				
	}
	else{
		$('#IUDFollowupfpMethod option[value=0]').prop('selected', true);
		$('#IUDFollowupfpMethod').prop('disabled', true);
		$('#IUDFollowupfpAmount').val("");
		$('#IUDFollowupfpAmount').prop('disabled', true);
	}
});

$(document).on("click", '#saveButtonIUDFollowup',function (){
	insertIUDFollowupInfo();
});

$(document).on("click", '#IUDFollowuprefer',function (){
	handleOnclickCheckbox("#IUDFollowuprefer","#IUDFollowupreferCenterName", "#IUDFollowupreferReason","enable","");	
});

$(document).on("change", '#IUDFollowupfollowupDate',function (){
	
	if(!firstFixedVisit){
		if(dateRangeCheck(firstFixedVisitRange, $('#IUDFollowupfollowupDate').val())){
			isCurrentVisitFixed = "1";
		}
		else{
			isCurrentVisitFixed = "";
		}
	}
	if(!secondFixedVisit){
		if(dateRangeCheck(secondFixedVisitRange, $('#IUDFollowupfollowupDate').val())){
			isCurrentVisitFixed = "2";
		}
		else{
			isCurrentVisitFixed = "";
		}
	}
	if(!thirdFixedVisit){
		if(dateRangeCheck(thirdFixedVisitRange, $('#IUDFollowupfollowupDate').val())){
			isCurrentVisitFixed = "3";			
		}
		else{
			isCurrentVisitFixed = "";
		}
	}
	var comment = "";
	isCurrentVisitFixed==""? comment = " (অনির্ধারিত)" : comment = " (নির্ধারিত - " + en_to_ben_number_conversion(isCurrentVisitFixed.toString()) + ")";
	
	$('#IUDFollowupisFixedVisitSpan').html(comment);
	
});

function dateRangeCheck(dateRange,specificDate){
	startDate = moment(dateRange[0]);
	endDate = moment(dateRange[1]);
	specDate = moment(specificDate);
	
	result = false;
	
	if(specDate.isBetween(startDate.subtract(1,'day'), endDate.add(1,'day'), 'days', '()')){ //isBetween is exclusive
		result = true;		
	}
		
	return result;
	
}

function resetFixedFollowupInfo(){
	
	isCurrentVisitFixed = "";
	
	firstFixedVisit = false;
	secondFixedVisit = false;
	thirdFixedVisit = false;
	
	firstFixedVisitRange = ["",""];
	secondFixedVisitRange = ["",""];
	thirdFixedVisitRange = ["",""];
}