breastConditionList = '[{"0":"","1":"নাই","2":"আছে"}]';
breastConditionWIJS = JSON.parse(breastConditionList);

uShapeList = '[{"0":"","1":"স্বাভাবিক","2":"স্বাভাবিক নয়"}]';
uShapeWIJS = JSON.parse(uShapeList);

uPositionList = '[{"0":"","1":"এন্টিভারটেড","2":"রেট্রোভারটেড"}]';
uPositionWIJS = JSON.parse(uPositionList);

uMovementList = '[{"0":"","1":"নড়ানো যায়","2":"নড়ানো যায় না"}]';
uMovementWIJS = JSON.parse(uMovementList);

uCMovePainList = '[{"0":"","1":"ব্যথা হয় না","2":"ব্যথা হয়"}]';
uCMovePainWIJS = JSON.parse(uCMovePainList);

injectionNameList = '[{"0":"","1":"ইনজেকশন (ডিএমপিএ)"}]';
injectionNameWIJS = JSON.parse(injectionNameList);

sideEffectList = '[{"0":"","1":"মাসিক বন্ধ","2":"ফোঁটা ফোঁটা রক্তস্রাব","3":"অতিরিক্ত রক্তস্রাব","4":"দুই মাসিকের মধ্যবর্তী সময়ে রক্তস্রাব","5":"ইনজেকশনের স্থানে সংক্রমণ",'
	+ '"6":"চোখ ও চামড়া অথবা এর যে কোন একটি হলুদ বর্ণ ধারণ করা (জন্ডিস)","7":"শ্বাসকষ্ট","8":"অল্প পরিশ্রমে বুকে ব্যথা হওয়া","9":"মাথা ধরা",'
	+ '"10":"ঘন ঘন প্রচণ্ড মাথা ব্যথা ও চোখে ঝাপসা দেখা","11":"পায়ের পিছনে প্রচণ্ড ব্যথা হওয়া এবং তা কয়েকদিন স্থায়ী হওয়া",'
	+ '"12":"ইনজেকশন নেয়া বন্ধ করার পরও গর্ভবতী না হওয়া","13":"মানসিক অবসাদ","14":"ওজন বৃদ্ধি","15":"ঝিমুনি","16":"অন্যান্য"}]';
sideEffectWIJS = JSON.parse(sideEffectList);

adviceList = '[{"1":"অন্য কোন পদ্ধতি গ্রহনের পরামর্শ","2":"ইনজেকশন নেয়া বাদ","3":"কম করে খাবার খাবেন",'
			+ '"4":"দিনে প্রায় ১ ঘণ্টা হাঁটবেন","5":"প্রতিদিন ১০-১২ গ্লাস পানি খাবেন","6":"অন্যান্য"}]';
adviceWIJS = JSON.parse(adviceList);

//treatmentList = '[{"1":"আয়রন ও ফলিক এসিড ট্যাবলেট","2":"স্বল্পমাত্রার খাবার বড়ি ১টি করে ২১দিন","3":"সাধারণ মাত্রার খাবার বড়ি ১টি করে ২১দিন",'
//				+ '"4":"ইথিনাইল ইস্ট্রাডিয়ল ৩০ মাইক্রোগ্রাম","5":"ইথিনাইল ইস্ট্রাডিয়ল ৫০ মাইক্রোগ্রাম",'
//				+ '"6":"ট্যাবলেট আইবুপ্রোফেন ৪০০ মি.গ্রা. দিনে ২ বার ভরা পেটে ৭ দিন"}]';
treatmentList = '[{"1":"আয়রন ও ফলিক এসিড ট্যাবলেট","2":"স্বল্পমাত্রার খাবার বড়ি","3":"সাধারণ মাত্রার খাবার বড়ি",'
	+ '"4":"ইথিনাইল ইস্ট্রাডিয়ল ৩০ মাইক্রোগ্রাম","5":"ইথিনাইল ইস্ট্রাডিয়ল ৫০ মাইক্রোগ্রাম",'
	+ '"6":"ট্যাবলেট আইবুপ্রোফেন ৪০০ মি.গ্রা."}]';
treatmentWIJS = JSON.parse(treatmentList);

referReasonList = '[{"1":"তলপেটে প্রচন্ড ব্যথা","2":"অতিরিক্ত রক্তক্ষরণ","3":"ইঞ্জেকশনের জায়গায় সংক্রমণ","4":"পদ্ধতি পরিবর্তন (ইমপ্ল্যান্ট বা অন্যান্য)","5":"অন্যান্য"}]';
referReasonWIJS = JSON.parse(referReasonList);

/**
 * saving injectable's information
 */

function insertInjectableInfo() {

	var injectableObj = new Object();
	injectableObj = setCommonParam();

	injectableObj.healthId = $('#healthid').text();
	injectableObj.providerId = $('#providerid').text();
	injectableObj.mobileNo = $('#cellNo').val();
	injectableObj.doseDate = $('#WIdoseDate').val();
	injectableObj.injectionId = $('#WIinjectionId').val();
	injectableObj.LMP = $('#WILMP').val();
	injectableObj.weight = $('#WIweight').val();
	injectableObj.pulse = $('#WIpulse').val();
	injectableObj.bpSystolic = $('#WIbpSystolic').val();
	injectableObj.bpDiastolic = $('#WIbpDiastolic').val();
	injectableObj.breastCondition = $('#WIbreastCondition').val();
	injectableObj.uterusShape = $('#WIuterusShape').val();
	injectableObj.uterusPosition = $('#WIuterusPosition').val();
	injectableObj.uterusMovement = $('#WIuterusMovement').val();
	injectableObj.uterusCervixMovePain = $('#WIuterusCervixMovePain').val();
	injectableObj.sideEffect = $('#WIsideEffect').multipleSelect("getSelects");
	injectableObj.treatment = $('#WItreatment').multipleSelect("getSelects");
	injectableObj.advice = $('#WIadvice').multipleSelect("getSelects");
	injectableObj.refer = $('#WIrefer').prop('checked') ? 1 : 2;
	injectableObj.referCenterName = $('#WIreferCenterName').val();
	injectableObj.referReason = $('#WIreferReason').multipleSelect("getSelects");

	injectableObj.sateliteCenterName = "";
	injectableObj.client = "1";
	
	injectableObj.injectableLoad = "";
	
	confirmationBoxInj(injectableObj);
}

/**
 * retrieve injectable information
 */
function retrieveInjectableInfo() {
	
	var injectableObj = new Object();
	injectableObj = setCommonParam();

	injectableObj.healthId = $('#healthid').text();
	injectableObj.injectableLoad = "retrieve";
	
	injectableInfo(injectableObj);	
}

/**
 * delete last injectable information
 */
function deleteLastinjectableInfo(){
	 
	var injectableObj = new Object();
	injectableObj = setCommonParam();
	
	injectableObj.healthId = $('#healthid').text();
	injectableObj.injectableLoad = "delete";
	
	injectableInfo(injectableObj);	
}


/**
 * insert and retrieve injectable information in a single AJAX call
 */
function injectableInfo(injectableObj) {
	
	if (injectableObj.healthId != "") {
		$.ajax({
			type : "POST",
			url : "womaninjectable",
			timeout:60000, //60 seconds timeout
			dataType : "json",
			data : {"injectableInfo" : JSON.stringify(injectableObj)},
			success : function(response) {
				parseInjectableInfo(response);									
			},
			error : function(xhr, status, error) {
				alert(status);
				alert(error);
			}
		});		
	}	
}

/**
 * populating injectable register's table dynamically
 */
function parseInjectableInfo(response) {
	
	if(response.regSerialNo!="" && response.regDate!=""){
		$("#regNumber").text(en_to_ben_number_conversion((response.regSerialNo<=9 ? ("0" + response.regSerialNo): response.regSerialNo) + "/" + moment(response.regDate).format("YY")).toString());
		$("#regDate").text(en_to_ben_number_conversion(moment(response.regDate).format("DD/MM/YYYY").toString()));
	}
	else{
		$("#regNumber").text("");
		$("#regDate").text("");
	}
	
	$('#content').empty();
	
	var divInjectable = $("<div id='injectable' class='person_ANCdetail'>");
	
	var headerContent = $("<table><tbody><tr><td><b>জীবিত সন্তান সংখ্যা</b>&nbsp;&nbsp;&nbsp;&nbsp;");
	headerContent.append("<b>ছেলে</b>&nbsp;<input type='text' id='injectableBoy' maxlength='2' size='2' value='" + $('#boy').val() + "' disabled='true'/>&nbsp;&nbsp;" 
			+ "<b>মেয়ে</b>&nbsp;<input type='text' id='injectableGirl' maxlength='2' size='2' value='" + $('#girl').val() + "' disabled='true'/>"
			+ "</td></tr></tbody></table>");

	divInjectable.append(headerContent);
	divInjectable.append("<br>");
		
	var tableInjectable = $("<table id='injectableVisitTable' border='1px'><tbody>");
	var tr, col, endCol, i;

	// row 1
	tr = $("<tr><td>পরিদর্শন </td>");
	for (i = 1; i <= response.count; i++) {
		col = "<td>" + en_to_ben_number_conversion(i.toString()) + "</td>";
		tr.append(col);
	}
	endCol = "<td>" + en_to_ben_number_conversion(((response.count)+1).toString())
			+ "</td>";
	tr.append(endCol);
	
	tr.append("</tr>");
	tableInjectable.append(tr);
	
	//row 2
	tr = $("<tr><td>ইনজেকশন ডিউ ডোজের তারিখ </td>");
	for (i = 1; i <= response.count; i++) {
		col = "<td id='WIdueDate-" + i + "'>";
		
		if(i>=2){
			col = col + moment(response[i-1].doseDate).add({months:3}).format("DD MMM YYYY");
		}
			
		col = col + "</td>";
		
		tr.append(col);
	}
	if(response.count>0){
		endCol = "<td>" + moment(response[response.count].doseDate).add({months:3}).format("DD MMM YYYY");
		endCol = endCol + "<span id='dueDoseWI' hidden='true'>" + moment(response[response.count].doseDate).add({months:3}).format("YYYY-MM-DD") + "</span>";
		endCol = endCol + "&nbsp;&nbsp;&nbsp;<input type='button' class='windowPeriodButton' id='windowPeriodWI' value='উইন্ডো পিরিয়ড'/>";
	}
	else{
		endCol = "<td>";
	}
	tr.append(endCol);
	
	tr.append("</td></tr>");
	tableInjectable.append(tr);
	
	//row 3
	tr = $("<tr><td>ইনজেকশন প্রদানের প্রকৃত তারিখ </td>");
	for (i = 1; i <= response.count; i++) {
		col = "<td id='WIdoseDate-" + i + "'>"
			+ moment(response[i].doseDate).format("DD MMM YYYY") + "</td>";
		
		tr.append(col);
	}
	
	endCol = "<td><input type='text' id='WIdoseDate' maxlength='11' size='11'/>";	
	tr.append(endCol);
	
	tr.append("</td></tr>");
	tableInjectable.append(tr);
	
	//row 4
	tr = $("<tr><td>শেষ মাসিকের তারিখ </td>");
	for (i = 1; i <= response.count; i++) {
		col = "<td id='WILMP-" + i + "'>"
			+ moment(response[i].LMP).format("DD MMM YYYY") + "</td>";
		tr.append(col);
	}
	
	endCol = "<td><input type='text' id='WILMP' maxlength='11' size='11'/>";	
	tr.append(endCol);
	
	tr.append("</td></tr>");
	tableInjectable.append(tr);
	
	// row 5
	tr = $("<tr><td>ওজন (কেজি)	</td>");
	for (i = 1; i <= response.count; i++) {
		col = "<td id='WIweight-" + i + "'>"
			+ en_to_ben_number_conversion(parseFloat(response[i].weight).toFixed(1).toString()) + "</td>";
		tr.append(col);
	}
	
	endCol = "<td><input type='text' id='WIweight' maxlength='4' size='4'/></td>";
	tr.append(endCol);
	
	tr.append("</tr>");
	tableInjectable.append(tr);
	
	// row 6
	tr = $("<tr><td>নাড়ির গতি	</td>");
	for (i = 1; i <= response.count; i++) {
		col = "<td id='WIpulse-" + i + "'>"
			+ en_to_ben_number_conversion(response[i].pulse.toString()) + "</td>";
		tr.append(col);
	}
	
	endCol = "<td><input type='text' id='WIpulse' maxlength='3' size='3'/></td>";
	tr.append(endCol);
	
	tr.append("</tr>");
	tableInjectable.append(tr);
	
	//row 7
	tr = $("<tr><td>রক্তচাপ (মিঃ মিঃ মার্কারি) </td>");
	
	for (i = 1; i <= response.count; i++) {
		slash = (response[i].bpSystolic!="" || response[i].bpDiastolic!="")?"/":"";	
		
		col = "<td id='WIbp-" + i + "'>"
			+ en_to_ben_number_conversion((response[i].bpSystolic).toString())
			+ slash
			+ en_to_ben_number_conversion((response[i].bpDiastolic).toString())
			+ "</td>";
		
		tr.append(col);
	}
	endCol = "<td><input type='text' id='WIbpSystolic' maxlength='3' size='3'/> / <input type='text' id='WIbpDiastolic' maxlength='3' size='3'/></td>";
	tr.append(endCol);
	
	tr.append("</tr>");
	tableInjectable.append(tr);
	
	// row 8
	tr = $("<tr><td>স্তনে চাকা</td>");
	for (i = 1; i <= response.count; i++) {
		
		col = "<td id='WIbreastCondition-" + i + "'>"
			+ breastConditionWIJS[0][response[i].breastCondition] + "</td>";
		tr.append(col);
	}
	
	endCol = "<td><select id='WIbreastCondition'>";
	endCol = endCol + populateDropdownList(breastConditionWIJS[0]);
	endCol = endCol + "</select></td>";
	
	tr.append(endCol);
	
	tr.append("</tr>");
	tableInjectable.append(tr);
	
	//PV heading row
	tr = $("<tr><td colspan='" + (response.count+2) + "'><b>পি. ভি. পরীক্ষা</b></td></tr>");
	tableInjectable.append(tr);
	
	// row 9
	tr = $("<tr><td>&nbsp;&nbsp;- জরায়ুর আকৃতি</td>");
	for (i = 1; i <= response.count; i++) {
		
		col = "<td id='WIuterusShape-" + i + "'>"
			+ uShapeWIJS[0][response[i].uterusShape] + "</td>";
		tr.append(col);
	}
	
	endCol = "<td><select id='WIuterusShape'>";
	endCol = endCol + populateDropdownList(uShapeWIJS[0]);
	endCol = endCol + "</select></td>";
	
	tr.append(endCol);
	
	tr.append("</tr>");
	tableInjectable.append(tr);
	
	// row 10
	tr = $("<tr><td>&nbsp;&nbsp;- জরায়ুর অবস্থান</td>");
	for (i = 1; i <= response.count; i++) {
		
		col = "<td id='WIuterusPosition-" + i + "'>"
			+ uPositionWIJS[0][response[i].uterusPosition] + "</td>";
		tr.append(col);
	}
	
	endCol = "<td><select id='WIuterusPosition'>";
	endCol = endCol + populateDropdownList(uPositionWIJS[0]);
	endCol = endCol + "</select></td>";
	
	tr.append(endCol);
	
	tr.append("</tr>");
	tableInjectable.append(tr);
	
	// row 11
	tr = $("<tr><td>&nbsp;&nbsp;- জরায়ুর নড়াচড়া</td>");
	for (i = 1; i <= response.count; i++) {
		
		col = "<td id='WIuterusMovement-" + i + "'>"
			+ uMovementWIJS[0][response[i].uterusMovement] + "</td>";
		tr.append(col);
	}
	
	endCol = "<td><select id='WIuterusMovement'>";
	endCol = endCol + populateDropdownList(uMovementWIJS[0]);
	endCol = endCol + "</select></td>";
	
	tr.append(endCol);
	
	tr.append("</tr>");
	tableInjectable.append(tr);

	// row 12
	tr = $("<tr><td>&nbsp;&nbsp;- জরায়ু বা সারভিক্স নাড়ানোর সময় ব্যথা</td>");
	for (i = 1; i <= response.count; i++) {
		
		col = "<td id='WIuterusCervixMovePain-" + i + "'>"
			+ uCMovePainWIJS[0][response[i].uterusCervixMovePain] + "</td>";
		tr.append(col);
	}
	
	endCol = "<td><select id='WIuterusCervixMovePain'>";
	endCol = endCol + populateDropdownList(uCMovePainWIJS[0]);
	endCol = endCol + "</select></td>";
	
	tr.append(endCol);
	
	tr.append("</tr>");
	tableInjectable.append(tr);
	
	// row 13
	tr = $("<tr><td>ইনজেকশনের নাম</td>");
	for (i = 1; i <= response.count; i++) {
		
		col = "<td id='WIinjectionId-" + i + "'>"
			+ injectionNameWIJS[0][response[i].injectionId] + "</td>";
		tr.append(col);
	}
	
	endCol = "<td><select id='WIinjectionId'>";
	endCol = endCol + populateDropdownList(injectionNameWIJS[0]);
	endCol = endCol + "</select></td>";
	
	tr.append(endCol);
	
	tr.append("</tr>");
	tableInjectable.append(tr);
	
	// row 14
	tr = $("<tr><td>পার্শ্ব-প্রতিক্রিয়া / জটিলতা</td>");
	for(i=1; i<=response.count; i++){
		var colIdSpan = "#WIsideEffect-" + i + "-span";
							
		col = "<td id='WIsideEffect-" + i + "'>"
				 + "<span id='WIsideEffect-" + i + "-span' style='cursor:pointer'> <strong>"+detail(response[i].sideEffect)+"</strong> </span>";
		
		col = col + "<div id ='WIsideEffect-" + i + "-dialog' hidden>" + response[i].sideEffect + "</div>";
		
		col = col + "</td>";
		
		$(document).on("click",colIdSpan, function(){
			
			colDialogId = "#WIsideEffect-" + $(this).attr('id').split("-")[1] + "-dialog";
			retrieveDetailHistory(colDialogId, sideEffectWIJS);			
			
		});
		tr.append(col);
	}
	
	endCol = "<td><select id='WIsideEffect'  multiple='multiple'";
	endCol = endCol + populateDropdownList(sideEffectWIJS[0]);
	endCol = endCol + "</select></td>";
		
	tr.append(endCol);
	
	tr.append("</tr>");
	tableInjectable.append(tr);
	
	// row 15
	tr = $("<tr><td>চিকিৎসা</td>");
	for(i=1; i<=response.count; i++){
		var colIdSpan = "#WItreatment-" + i + "-span";
							
		col = "<td id='WItreatment-" + i + "'>"
				 + "<span id='WItreatment-" + i + "-span' style='cursor:pointer'> <strong>"+detail(response[i].treatment)+"</strong> </span>";
		
		col = col + "<div id ='WItreatment-" + i + "-dialog' hidden>" + response[i].treatment + "</div>";
		
		col = col + "</td>";
		
		$(document).on("click",colIdSpan, function(){
			
			colDialogId = "#WItreatment-" + $(this).attr('id').split("-")[1] + "-dialog";
			retrieveDetailHistory(colDialogId, treatmentWIJS);			
			
		});
		tr.append(col);
	}
	
	endCol = "<td><select id='WItreatment'  multiple='multiple'>";
	endCol = endCol + populateDropdownList(treatmentWIJS[0]);
	endCol = endCol + "</select></td>";
		
	tr.append(endCol);
	
	tr.append("</tr>");
	tableInjectable.append(tr);
	
	// row 16
	tr = $("<tr><td>পরামর্শ</td>");
	for(i=1; i<=response.count; i++){
		var colIdSpan = "#WIadvice-" + i + "-span";
							
		col = "<td id='WIadvice-" + i + "'>"
				 + "<span id='WIadvice-" + i + "-span' style='cursor:pointer'> <strong>"+detail(response[i].advice)+"</strong> </span>";
		
		col = col + "<div id ='WIadvice-" + i + "-dialog' hidden>" + response[i].advice + "</div>";
		
		col = col + "</td>";
		
		$(document).on("click",colIdSpan, function(){
			
			colDialogId = "#WIadvice-" + $(this).attr('id').split("-")[1] + "-dialog";
			retrieveDetailHistory(colDialogId, adviceWIJS);			
			
		});
		tr.append(col);
	}
	
	endCol = "<td><select id='WIadvice'  multiple='multiple'>";
	endCol = endCol + populateDropdownList(adviceWIJS[0]);
	endCol = endCol + "</select></td>";
		
	tr.append(endCol);
	
	tr.append("</tr>");
	tableInjectable.append(tr);
	
	// row 17
	var tr = $("<tr><td>রেফার	</td>");
	for (i = 1; i <= response.count; i++) {
		col = "<td id='WIrefer-" + i + "'>"
			+ (response[i].refer==1?'হ্যাঁ':'না') + "</td>";
		tr.append(col);		
	}
	
	endCol = "<td><input type='checkbox' id='WIrefer'/></td>";
	tr.append(endCol);	
	
	tr.append("</tr>");
	tableInjectable.append(tr);
	
	// row 18
	var tr = $("<tr><td>কেন্দ্রের নাম	</td>");
	referCenterJS = JSON.parse(referCenter);
	for (i = 1; i <= response.count; i++) {
		
		col = "<td id='WIreferCenterName-" + i + "'>"
			+ referCenterJS[0][response[i].referCenterName] + "</td>";
		tr.append(col);
	}
	
	endCol = "<td><select id='WIreferCenterName' disabled='true'>";
	endCol = endCol + populateDropdownList(referCenterJS[0]);
	endCol = endCol + "</select></td>";
	
	tr.append(endCol);
	
	tr.append("</tr>");
	tableInjectable.append(tr);
	
	// row 19
	tr = $("<tr><td>কারণ</td>");
	for(i=1; i<=response.count; i++){
		var colIdSpan = "#WIreferReason-" + i + "-span";
							
		col = "<td id='WIreferReason-" + i + "'>"
				 + "<span id='WIreferReason-" + i + "-span' style='cursor:pointer'> <strong>"+detail(response[i].referReason)+"</strong> </span>";
		
		col = col + "<div id ='WIreferReason-" + i + "-dialog' hidden>" + response[i].referReason + "</div>";
		
		col = col + "</td>";
		
		$(document).on("click",colIdSpan, function(){
			
			colDialogId = "#WIreferReason-" + $(this).attr('id').split("-")[1] + "-dialog";
			retrieveDetailHistory(colDialogId, referReasonWIJS);			
			
		});
		tr.append(col);
	}
	
	endCol = "<td><select id='WIreferReason'  multiple='multiple' disabled='true'>";
	endCol = endCol + populateDropdownList(referReasonWIJS[0]);
	endCol = endCol + "</select><br>"
					+ "<input type='button' class='right' id='saveButtonWI' value='Save'/>"
					+ "</td>";
		
	tr.append(endCol);
	
	tr.append("</tr>");
	tableInjectable.append(tr);
	
	
	
	tableInjectable.append("</tbody></table>");
	divInjectable.append(tableInjectable);
	divInjectable.append("</div>");
	$('#content').append(divInjectable);
	
	$('#WIdoseDate').combodate({
		format : "YYYY-MM-DD",
		template : "DD MMM YYYY",
		smartDays : true,
		value : moment()
	});
	$('#WILMP').combodate({
		format : "YYYY-MM-DD",
		template : "DD MMM YYYY",
		smartDays : true,
		value : moment()
	});
	
	fieldId = ["#WIsideEffect","#WItreatment","#WIadvice","#WIreferReason"];
	$.each(fieldId, function(key, val) {
		//multiSelectDropDownInit(val);
		$(val).multipleSelect({
			width : 300,
			position : 'top',
			selectAll: false,
			selectedList : 1,
			styler : function() {
				return 'font-size: 18px;';
			}	
		});
	});
}

// Only allowing numeric values for input fields and convert that to Bangla
$(document).on("keyup", '#WIbpSystolic', function() {
	$(this).val($(this).val().replace(/[^\d]/, ''));
});

$(document).on("keyup", '#WIbpDiastolic', function() {
	$(this).val($(this).val().replace(/[^\d]/, ''));
});

$(document).on("keyup", '#WIweight', function() {
	$(this).val($(this).val().match(/^\d*\.?\d*$/) ? $(this).val() : "");			
});

$(document).on("keyup", '#WIpulse', function() {
	$(this).val($(this).val().replace(/[^\d]/, ''));
});


function confirmationBoxInj(injectableObj){
	
	var userVal = "";
	userVal = userVal + "<table class='list'><tr><td>প্রদানের প্রকৃত তারিখ: " + moment(injectableObj.doseDate).format("DD MMM YYYY") + "</td></tr>";
	userVal = userVal + "<tr><td>ইনজেকশনের নাম: "+injectionNameWIJS[0][injectableObj.injectionId]+"</tr></td>";
	
	userVal = userVal + "<table class='list'><tr><td>পার্শ্ব-প্রতিক্রিয়া / জটিলতা:</td></tr> ";
	textVal=injectableObj.sideEffect;
	
	var i;
	var convertedText = "  ";
	
	for(i =0; i < textVal.length; i++){
		convertedText = convertedText +"<tr><td>-"+sideEffectWIJS[0][textVal[i]] +"</td></tr>";				
	}
	userVal+=convertedText;
	
	userVal = userVal + "<table class='list'><tr><td>চিকিৎসা:</tr></td> ";
	textVal=injectableObj.treatment;
	
	var i;
	var convertedText = "  ";
	
	for(i =0; i < textVal.length; i++){
		convertedText = convertedText +"<tr><td>-"+treatmentWIJS[0][textVal[i]] +"</td></tr>";				
	}
	userVal+=convertedText;
	
	userVal = userVal + "<table class='list'><tr><td>পরামর্শ:</td></tr> ";
	textVal=injectableObj.advice;
	
	var i;
	var convertedText = "  ";
	
	for(i =0; i < textVal.length; i++){
		convertedText = convertedText +"<tr><td>-"+adviceWIJS[0][textVal[i]] +"</td></tr>";				
	}
	userVal+=convertedText;
	
	userVal = userVal + "<tr><td>রেফার: "+(injectableObj.refer=="1" ? "হ্যাঁ" : "না")+"</td></tr>";
    
    if(injectableObj.refer=="1"){
    	   userVal = userVal +"<tr><td>"+ linemark_switch("কেন্দ্রের নাম",injectableObj.referCenterName,0);		   
		   
    	   userVal += referCenterJS[0][injectableObj.referCenterName]; 
		   
    	   userVal+="</td></tr>"; 
		   
    	   userVal = userVal +"<tr><td>"+linemark_switch("কারণ",injectableObj.referReason,0)+"</td></tr>";
    	   textVal=injectableObj.referReason;
		   var i;
		   var convertedText = "  ";
	
		   for(i =0; i < textVal.length; i++){
			   convertedText = convertedText +"<tr><td>-"+ referReasonWIJS[0][textVal[i]] + "</td></tr>";	
		   }
		   userVal+=convertedText;
	
    }   
	
	userVal = userVal + "</table>";
	
    $('<div></div>').dialog({
        modal: true,
        width: 450,
        hide: {effect: 'fade', duration: 500},
        show: {effect: 'fade', duration: 600},
        title: "নিশ্চিত করুন",
        open: function () {
            $(this).html(userVal);
        },
        buttons: {
        	 "Save" : function () {      
        		 injectableInfo(injectableObj);
             	$(this).dialog("close");
             },
             "Cancel": function () {
        		$(this).dialog("close");
        	}
        }
    });
}

$(document).on("click", '#windowPeriodWI' ,function (){
	
	var windowPeriod = "<br>";//"<h2>ইনজেকশন ডিউ ডোজের উইন্ডো পিরিয়ড</h2><br>";
	var startDate = moment($('#dueDoseWI').html()).subtract({days:14}).format("DD MMM YYYY");
	var endDate = moment($('#dueDoseWI').html()).add({days:28}).format("DD MMM YYYY");
	windowPeriod = windowPeriod + startDate + " - " + endDate + "<br>";
	
	$('#detailList').html(windowPeriod);
	$('#detailInfoWindow').modal();
});


function retrieveDetailHistory(colDialogId, jsonObject){
	
	textVal = $(colDialogId).text();
	textVal = textVal.slice(1, -1);
		
	if(textVal!=""){
		textVal = textVal.split(",");
		var j;
		var convertedText = "";
	
		for(j =0; j < textVal.length; j++){
			textVal[j] = textVal[j].slice(1,-1);
			convertedText = convertedText + "- " + jsonObject[0][textVal[j]] + "<br>";					
		}
		$('#detailList').html(convertedText);
		$('#detailInfoWindow').modal();
	
	}
	else{
		$('#detailList').html("There is no information")
		$('#detailInfoWindow').modal();
	}
}

$(document).on("click", '#WIrefer',function (){
	handleOnclickCheckbox("#WIrefer","#WIreferCenterName", "#WIreferReason","enable","");	
});