function deleteLastPNCChildInfo(id){
	
	var PNCObj = new Object();
	PNCObj = setCommonParam();
	
	PNCObj.healthid = $('#healthid').text();
	PNCObj.pregno = $('#pregNumber').text();
	PNCObj.pncchildno = id;
	
	PNCObj.pncCLoad = "delete";
	
	PNCChildInfo(PNCObj);	
		
}
/**
 * saving PNC Mother information
 */
function insertPNCChildInfo(id) {

	var PNCObj = new Object();
	PNCObj = setCommonParam();

	PNCObj.healthid = $('#healthid').text();
	PNCObj.pregno = $('#pregNumber').text();
	PNCObj.providerid = $('#providerid').text();
	
	PNCObj.pncchildno = id; 
		
	pncdate = "#pncchildDate-" + id;
	PNCObj.pncdate = $(pncdate).val();
	
	pnctemperature = "#pncchildTemperature-" + id;
	PNCObj.pnctemperature = $(pnctemperature).val();
	
	pncweight = "#pncchildWeight-" + id;	
	PNCObj.pncweight = $(pncweight).val();
	
	pncbreathingperminute = "#pncchildBreathingPerMinute-" + id;	
	PNCObj.pncbreathingperminute = $(pncbreathingperminute).val();
	
	pncdangersign = "#pncchildDangerSign-" + id;
	PNCObj.pncdangersign = $(pncdangersign).multipleSelect("getSelects");
	
	pncbreastfeedingonly = "#pncchildBreastFeedingOnly-" + id;
	PNCObj.pncbreastfeedingonly = $(pncbreastfeedingonly).prop('checked') ? 1 : 2;
	
	pncsymptom = "#pncchildSymptom-" + id;
	PNCObj.pncsymptom = $(pncsymptom).multipleSelect("getSelects");
	
	pncdisease = "#pncchildDisease-" + id;
	PNCObj.pncdisease = $(pncdisease).multipleSelect("getSelects");
	
	pnctreatment = "#pncchildTreatment-" + id;
	PNCObj.pnctreatment = $(pnctreatment).multipleSelect("getSelects");
	
	pncadvice = "#pncchildAdvice-" + id;
	PNCObj.pncadvice = $(pncadvice).multipleSelect("getSelects");
	
	pncrefer = "#pncchildRefer-" + id;
	PNCObj.pncrefer = $(pncrefer).prop('checked') ? 1 : 2;
	
	pncreferreason = "#pncchildReferReason-" + id;
	PNCObj.pncreferreason = $(pncreferreason).multipleSelect("getSelects");
	
	pncrefercentername = "#pncchildReferCenterName-" + id;
	PNCObj.pncrefercentername = $(pncrefercentername).val();
		
	pncotherserviceprovider = "#otherServiceProviderCheckboxPNCC-" + id;
	pncotherserviceprovidercategory = "#otherServiceProviderPNCC-" + id;
	if($(pncotherserviceprovider).prop('checked')){
		PNCObj.pncservicesource = $(pncotherserviceprovidercategory).val();
	}
	else{
		PNCObj.pncservicesource = "";
	}
	//PNCObj.pncservicesource = "GOVERNMENT";
	PNCObj.sateliteCenterName = "";
	PNCObj.client = "1";
	PNCObj.pncCLoad = "";

	
	
	var userVal = "";
	userVal = userVal + "<table class='list'><tr><td>সেবাগ্রহনের তারিখ: " + moment(PNCObj.pncdate).format("DD MMM YYYY") + "</td></tr>";
	
	userVal = userVal + "<tr><td>অসুবিধা:</td></tr>";	
	textVal=PNCObj.pncsymptom;
	symptomPNCCJS = JSON.parse(symptomPNCC);
	var i;
	var convertedText = "";
	
	for(i =0; i < textVal.length; i++){
		convertedText = convertedText + '<tr><td>-'+symptomPNCCJS[0][textVal[i]] + "</td></tr>";
	}
	userVal+=convertedText+"</table>";
	
	userVal = userVal +"<table class='confirmation'><tr><td>"+linemark("তাপমাত্রা",PNCObj.pnctemperature)+"</td>";
	userVal = userVal +"<td>"+linemark("ওজন (কেজি)",PNCObj.pncweight)+"</td></tr>";
	userVal = userVal +"<tr><td>"+linemark("শ্বাস (মিনিটে কতবার)",PNCObj.pncbreathingperminute)+"</td></table>";
	userVal = userVal + "<table class='list'><tr><td>বিপদ চিহ্ন: </tr></td>";
	textVal=PNCObj.pncdangersign;
	dangerSignPNCCJS = JSON.parse(dangerSignPNCC);
	var i;
	var convertedText = "";
	
	for(i =0; i < textVal.length; i++){
		convertedText = convertedText + "<tr><td>-" + dangerSignPNCCJS[0][textVal[i]] + "</tr></td>";
	}
	userVal+=convertedText;
	   
	userVal = userVal + "<tr><td>শুধুমাত্র বুকের দুধ খাওয়ানো: "+(PNCObj.pncbreastfeedingonly=="1" ? "হ্যাঁ" : "না")+"</tr></td>";
	
	userVal = userVal + "<tr><td>রোগ: </tr></td>";	
	textVal=PNCObj.pncdisease;
	diseasePNCCJS = JSON.parse(diseasePNCC);
	var i;
	var convertedText = "";
	
	for(i =0; i < textVal.length; i++){
		convertedText = convertedText + '<tr><td>-'+diseasePNCCJS[0][textVal[i]] + "</tr></td>";
	}
	userVal+=convertedText;
    
	userVal = userVal + "<tr><td>চিকিৎসা: </tr></td>";
	textVal=PNCObj.pnctreatment;
	treatmentJS = JSON.parse(treatment);
    convertedText = "";
	
	for(i =0; i < textVal.length; i++){
		convertedText = convertedText +'<tr><td>-'+ treatmentJS[0][textVal[i]] + "</tr></td>";
	}
	userVal+=convertedText;
	
    userVal = userVal + "<tr><td>পরামর্শ: </tr></td>";
    textVal=PNCObj.pncadvice;
    advicePNCCJS = JSON.parse(advicePNCC);
    var i;
	var convertedText = "";
	
	for(i =0; i < textVal.length; i++){
		convertedText = convertedText +'<tr><td>-'+ advicePNCCJS[0][textVal[i]] + "</tr></td>";
	}
	userVal+=convertedText;
    
    userVal = userVal + "<tr><td>রেফার: "+(PNCObj.pncrefer=="1" ? "হ্যাঁ" : "না")+"<br>";
    
    if(PNCObj.pncrefer=="1"){
    userVal = userVal + "<tr><td>" +linemark_switch("কেন্দ্রের নাম :",PNCObj.pncrefercentername,0);	
    referCenterJS = JSON.parse(referCenter);
	userVal += referCenterJS[0][PNCObj.pncrefercentername]; 
    
    userVal+="</tr></td>"; 
    userVal = userVal + "<tr><td>" +linemark_switch("কারণ :",PNCObj.pncreferreason,0)+"</tr></td>";
    referReasonPNCCJS = JSON.parse(referReasonPNCC);
    textVal=PNCObj.pncreferreason;
    var i;
	var convertedText = "";
	
	for(i =0; i < textVal.length; i++){
		convertedText = convertedText + '<tr><td>-'+referReasonPNCCJS[0][textVal[i]] + "</tr></td>";
	}
	userVal+=convertedText+"</table>";}
    if(moment(PNCObj.pncdate) >= moment($('#idealPNC1Date').text())){
	    $('<div></div>').dialog({
	        modal: true,
	        width: 450,
	        hide: {effect: 'fade', duration: 500},
	        show: {effect: 'fade', duration: 600},
	        title: "নিশ্চিত করুন",
	        open: function () {
	            $(this).html(userVal);
	        },
	        buttons: {
	        	 "Save & Continue" : function () {      
	             	PNCChildInfo(PNCObj);
	             	$(this).dialog("close");
	             },
	             "Save & Serve Next Client": function () {
	 				hideFlag="doHide";
	 				PNCChildInfo(PNCObj);				
	         		goToId='.nextC';	
	 			    $(goToId).click();	
	             	$(this).dialog("close");
	             },
	             "Cancel": function () {
	        		$(this).dialog("close");
	        	}
	        }
	    });
    }
    else{
    	callDialog(pncDateLessThanDelivery);
    }
}

/**
 * retrieve PNC Mother information
 */
function retrievePNCChildInfo() {

	var PNCObj = new Object();
	PNCObj = setCommonParam();

	PNCObj.pregno = $('#pregNumber').text();
	PNCObj.healthid = $('#healthid').text();
	PNCObj.pncCLoad = "retrieve";

	PNCChildInfo(PNCObj);
}

/**
 * insert and retrieve PNC Mother information in a single AJAX call
 */
function PNCChildInfo(pncChildInfo) {

	if (pncChildInfo.healthid != "" && pncChildInfo.pregno != "") {
		$.ajax({
			type : "POST",
			url : "pncchild",
			timeout:60000, //60 seconds timeout
			dataType : "json",
			data : {"PNCChildInfo" : JSON.stringify(pncChildInfo)},
			success : function(response) {
				parsePNCChildInfo(response);
			},
			error : function(xhr, status, error) {
				alert(status);
				alert(error);
			}
		});
	}
}

/**
 * populating pnc table dynamically for child
 */
function parsePNCChildInfo(response) {
	//sorting the child number
	var responseSorted = [];
	//document.getElementById('hasDeliveryInfo').innerHTML=response.hasDeliveryInformation;
	/*if(response.hasDeliveryInformation=="No"){
		$("div#ancVisitOpen").hide();
	  	$("div#deliveryOpen").hide();	  	
	}*/
	
	for ( var prop in response) {
		responseSorted.push(prop);
	}
	responseSorted.sort();
	$('#pncVisitChild').empty();
	if(response.childCount=="0"){
		document.getElementById('childCount').innerHTML=response.childCount;
		//$('#childCount').text(response.childCount);
		$('div#pncchild').hide();
	}
	else{
		document.getElementById('childCount').innerHTML=response.childCount;
	
	//iterating through childs' pnc visits 
	for(a=0; a < response.childCount; a++){
		// populating pnc table
		var tablePNCChild = $("<table id='pncVisitChild-"+responseSorted[a]+"-Table' border='1px'><tbody>");
		var tr, col, endCol, i;

		// row 0
		tr = $("<tr><td>নবজাতক: " + en_to_ben_number_conversion(responseSorted[a].toString())
			+ "</td>");
		tr.append("</tr>");
		tablePNCChild.append(tr);
		
		// row 1
		tr = $("<tr><td>পরিদর্শন </td>");
		for (i = 1; i <= response[responseSorted[a]].serviceCount; i++) {
			col = "<td>" + en_to_ben_number_conversion(i.toString()) + "</td>";
			tr.append(col);
		}
		if (!response.pncStatus) {
			endCol = "<td>" + en_to_ben_number_conversion(((response[responseSorted[a]].serviceCount)+1).toString())
					+ "</td>";
			tr.append(endCol);
		}
		tr.append("</tr>");
		tablePNCChild.append(tr);
		
		// row 2
		tr = $("<tr><td>সেবাগ্রহনের তারিখ	</td>");
		otherServiceCenterJS =  JSON.parse(referCenter);
		for (i = 1; i <= response[responseSorted[a]].serviceCount; i++) {
			col = "<td id='pncchildDate-" + responseSorted[a] + "-" + i + "'>"
				+ moment(response[responseSorted[a]][i].visitDate).format("DD MMM YYYY");
			if(response[responseSorted[a]][i].serviceSource!=""){
				col = col + "<br>" + otherServiceCenterJS[0][response[responseSorted[a]][i].serviceSource];
			}
			col = col + "</td>";
			tr.append(col);
		}
		if (!response.pncStatus) {
			endCol = "<td><input type='text' id='pncchildDate-"+responseSorted[a]+"' maxlength='11' size='11'/>&nbsp;<input type='checkbox' id='otherServiceProviderCheckboxPNCC-"+responseSorted[a]+"'/> অন্যান্য উৎস <br>"
					+ "<select id='otherServiceProviderPNCC-"+responseSorted[a]+"' hidden='true'>";
			
			$.each(otherServiceCenterJS[0], function(key, val) {
				endCol = endCol + "<option value = " + key + ">" + val + "</option>"; 			
			});	
					
			endCol = endCol + "</select></td>";
			tr.append(endCol);
		}
		tr.append("</tr>");
		tablePNCChild.append(tr);
		
		// row 8
		tr = $("<tr><td>অসুবিধা	</td>");
		symptomPNCCJS = JSON.parse(symptomPNCC);
		for(i=1; i <= response[responseSorted[a]].serviceCount; i++){
			var colIdSpan = "#pncchildSymptom-" + responseSorted[a] + "_" + i + "-span";
								
			col = "<td id='pncchildSymptom-" + responseSorted[a] + "_" + i + "'>"
					 + "<span id='pncchildSymptom-" + responseSorted[a] + "_" + i + "-span' style='cursor:pointer'> <strong>"+detail(response[responseSorted[a]][i].symptom)+"</strong> </span>";
			
			col = col + "<div id ='pncchildSymptom-" + responseSorted[a] + "_" + i + "-dialog' hidden>" + response[responseSorted[a]][i].symptom + "</div></td>";
			
			$(document).on("click",colIdSpan, function(){
				id = $(this).attr('id');
				id = id.split("-")[1];
				colDialogId = "#pncchildSymptom-" + id + "-dialog";
								
				textVal = $(colDialogId).text();
				textVal = textVal.slice(1, -1);
				
				if(textVal!=""){
					textVal = textVal.split(",");
				
					var j;
					var convertedText = "";
					for(j =0; j < textVal.length; j++){
						textVal[j] = textVal[j].slice(1,-1);
						convertedText = convertedText + "- " + symptomPNCCJS[0][textVal[j]] + "<br>";						
					}					
					$('#detailList').html(convertedText);
					$('#detailInfoWindow').modal();
				}
				else{
					$('#detailList').html("There is no information");
					$('#detailInfoWindow').modal();					
				}
			});
			tr.append(col);
		}
		
		if (!response.pncStatus) {
			endCol = "<td><select id='pncchildSymptom-"+responseSorted[a]+"' multiple='multiple'>";

			endCol = endCol + populateDropdownList(symptomPNCCJS[0]);
			
			endCol = endCol + "</select></td>";
			tr.append(endCol);
		}
		tr.append("</tr>");
		tablePNCChild.append(tr);
		
		// row 3
		tr = $("<tr><td>তাপমাত্রা	</td>");
		for (i = 1; i <= response[responseSorted[a]].serviceCount; i++) {
			
			if(response[responseSorted[a]][i].temperature >=t_pncCtemperature)
				 mark=danger;
			else mark=safe;
			
			col = "<td id='pncchildTemperature-" + responseSorted[a] + "-" + i + "'"+mark+">"
				+ en_to_ben_number_conversion((response[responseSorted[a]][i].temperature).toString()) + "</td>";
			tr.append(col);
		}
		if (!response.pncStatus) {
			endCol = "<td><input type='text' id='pncchildTemperature-"+responseSorted[a]+"' maxlength='5' size='5'/></td>";
			tr.append(endCol);
		}
		tr.append("</tr>");
		tablePNCChild.append(tr);
		
		// row 4
		tr = $("<tr><td>ওজন (কেজি)	</td>");
		for (i = 1; i <= response[responseSorted[a]].serviceCount; i++) {
			
			/*if(response[responseSorted[a]][i].weight <=t_pncCweight)
				mark=danger;
			else*/ mark=safe;
			
			col = "<td id='pncchildWeight-" + responseSorted[a] + "-" + i + "'"+mark+">"
				+ en_to_ben_number_conversion(parseFloat(response[responseSorted[a]][i].weight).toFixed(1).toString()) + "</td>";
			tr.append(col);
		}
		if (!response.pncStatus) {
			endCol = "<td><input type='text' id='pncchildWeight-"+responseSorted[a]+"' maxlength='4' size='4'/></td>";
			tr.append(endCol);
		}
		tr.append("</tr>");
		tablePNCChild.append(tr);
	
		// row 5
		tr = $("<tr><td><span class=\"tp\" id=\"pncc_bpm\">শ্বাস (মিনিটে কতবার)	</span></td>");
		for (i = 1; i <= response[responseSorted[a]].serviceCount; i++) {
			
			if(response[responseSorted[a]][i].breathingPerMinute!='' && (response[responseSorted[a]][i].breathingPerMinute <=t_pncCbreathingperminuteL || response[responseSorted[a]][i].breathingPerMinute >=t_pncCbreathingperminuteH))
				mark=danger;
			else mark=safe;
			
			col = "<td id='pncchildBreathingPerMinute-" + responseSorted[a] + "-" + i + "'"+mark+">"
				+ en_to_ben_number_conversion((response[responseSorted[a]][i].breathingPerMinute).toString()) + "</td>";
			tr.append(col);
		}
		if (!response.pncStatus) {
			endCol = "<td><input type='text' id='pncchildBreathingPerMinute-"+responseSorted[a]+"' maxlength='3' size='3'/></td>";
			tr.append(endCol);
		}
		tr.append("</tr>");
		tablePNCChild.append(tr);
		
		// row 6
		tr = $("<tr><td>বিপদ চিহ্ন	</td>");
		dangerSignPNCCJS = JSON.parse(dangerSignPNCC);
		for(i=1; i <= response[responseSorted[a]].serviceCount; i++){
			var colIdSpan = "#pncchildDangerSign-" + responseSorted[a] + "_" + i + "-span";
								
			col = "<td id='pncchildDangerSign-" + responseSorted[a] + "_" + i + "'>"
					 + "<span id='pncchildDangerSign-" + responseSorted[a] + "_" + i + "-span' style='cursor:pointer'> <strong>"+detail(response[responseSorted[a]][i].dangerSign)+"</strong> </span>";
			
			col = col + "<div id ='pncchildDangerSign-" + responseSorted[a] + "_" + i + "-dialog' hidden>" + response[responseSorted[a]][i].dangerSign + "</div></td>";
			
			$(document).on("click",colIdSpan, function(){
				id = $(this).attr('id');
				id = id.split("-")[1];
				colDialogId = "#pncchildDangerSign-" + id + "-dialog";
								
				textVal = $(colDialogId).text();
				textVal = textVal.slice(1, -1);
				
				if(textVal!=""){
					textVal = textVal.split(",");
				
					var j;
					var convertedText = "";
					for(j =0; j < textVal.length; j++){
						textVal[j] = textVal[j].slice(1,-1);
						convertedText = convertedText + "- " + dangerSignPNCCJS[0][textVal[j]] + "<br>";
					}
					
					$('#detailList').html(convertedText);
					$('#detailInfoWindow').modal();
				}
				else{
					$('#detailList').html("There is no information");
					$('#detailInfoWindow').modal();
					//alert("There is no information");
				}
			});
			tr.append(col);
		}
		
		if (!response.pncStatus) {
			endCol = "<td><select id='pncchildDangerSign-"+responseSorted[a]+"' multiple='multiple'>";					
			
			endCol = endCol + populateDropdownList(dangerSignPNCCJS[0]);
			
			endCol = endCol + "</select></td>";
			tr.append(endCol);
		}
		tr.append("</tr>");
		tablePNCChild.append(tr);
		
		// row 7
		var tr = $("<tr><td><span class=\"tp\" id=\"br_milk\">শুধুমাত্র বুকের দুধ খাওয়ানো	</span></td>");
		
		for (i = 1; i <= response[responseSorted[a]].serviceCount; i++) {
			var value = response[responseSorted[a]][i].breastFeedingOnly.toString();
			
			switch (value) {
				case "1":
					value = 'হ্যাঁ';
					break;
				case "2":
					value = 'না';
					break;			
			}
			col = "<td id='pncchildBreastFeedingOnly-" + responseSorted[a] + "-" + i + "'>"
				+ value + "</td>";
			tr.append(col);		
		}
		if (!response.pncStatus) {
			endCol = "<td><input type='checkbox' id='pncchildBreastFeedingOnly-" + responseSorted[a] + "'/></td>";
			tr.append(endCol);	
		}
		tr.append("</tr>");
		tablePNCChild.append(tr);
		
		
		// row 9
		tr = $("<tr><td>রোগ	</td>");
		diseasePNCCJS = JSON.parse(diseasePNCC);
		for(i=1; i <= response[responseSorted[a]].serviceCount; i++){
			var colIdSpan = "#pncchildDisease-" + responseSorted[a] + "_" + i + "-span";
								
			col = "<td id='pncchildDisease-" + responseSorted[a] + "_" + i + "'>"
					 + "<span id='pncchildDisease-" + responseSorted[a] + "_" + i + "-span' style='cursor:pointer'> <strong>"+detail(response[responseSorted[a]][i].disease)+"</strong> </span>";
			
			col = col + "<div id ='pncchildDisease-" + responseSorted[a] + "_" + i + "-dialog' hidden>" + response[responseSorted[a]][i].disease + "</div></td>";
			
			$(document).on("click",colIdSpan, function(){
				id = $(this).attr('id');
				id = id.split("-")[1];
				colDialogId = "#pncchildDisease-" + id + "-dialog";
								
				textVal = $(colDialogId).text();
				textVal = textVal.slice(1, -1);
				
				if(textVal!=""){
					textVal = textVal.split(",");
				
					var j;
					var convertedText = "";
					for(j =0; j < textVal.length; j++){
						textVal[j] = textVal[j].slice(1,-1);
						convertedText = convertedText + "- " + diseasePNCCJS[0][textVal[j]] + "<br>";						
					}					
					$('#detailList').html(convertedText);
					$('#detailInfoWindow').modal();
				}
				else{
					$('#detailList').html("There is no information");
					$('#detailInfoWindow').modal();					
				}
			});
			tr.append(col);
		}
		
		if (!response.pncStatus) {
			endCol = "<td><select id='pncchildDisease-"+responseSorted[a]+"' multiple='multiple'>";
			
			endCol = endCol + populateDropdownList(diseasePNCCJS[0]);
			
			endCol = endCol + "</select></td>";
			tr.append(endCol);
		}
		tr.append("</tr>");
		tablePNCChild.append(tr);
		
		// row 10
		tr = $("<tr><td>চিকিৎসা	</td>");
		
		treatmentJS = JSON.parse(treatment);
		for(i=1; i <= response[responseSorted[a]].serviceCount; i++){
			var colIdSpan = "#pncchildTreatment-" + responseSorted[a] + "_" + i + "-span";
								
			col = "<td id='pncchildTreatment-" + responseSorted[a] + "_" + i + "'>"
					 + "<span id='pncchildTreatment-" + responseSorted[a] + "_" + i + "-span' style='cursor:pointer'> <strong>"+detail(response[responseSorted[a]][i].treatment)+"</strong> </span>";
			
			col = col + "<div id ='pncchildTreatment-" + responseSorted[a] + "_" + i + "-dialog' hidden>" + response[responseSorted[a]][i].treatment + "</div></td>";
			
			$(document).on("click",colIdSpan, function(){
				id = $(this).attr('id');
				id = id.split("-")[1];
				colDialogId = "#pncchildTreatment-" + id + "-dialog";
								
				textVal = $(colDialogId).text();
				textVal = textVal.slice(1, -1);
				
				if(textVal!=""){
					textVal = textVal.split(",");
				
					var j;
					var convertedText = "";
					for(j =0; j < textVal.length; j++){
						textVal[j] = textVal[j].slice(1,-1);
						convertedText = convertedText + "- " + treatmentJS[0][textVal[j]] + "<br>";						
					}					
					$('#detailList').html(convertedText);
					$('#detailInfoWindow').modal();
				}
				else{
					$('#detailList').html("There is no information");
					$('#detailInfoWindow').modal();					
				}
			});
			tr.append(col);
		}
		
		if (!response.pncStatus) {
			endCol = "<td><select id='pncchildTreatment-"+responseSorted[a]+"' multiple='multiple'>";
			
			endCol = endCol + populateDropdownList(treatmentJS[0]);
			
			endCol = endCol + "</select></td>";
			tr.append(endCol);
		}
		tr.append("</tr>");
		tablePNCChild.append(tr);
		
		
		// row 11
		tr = $("<tr><td>পরামর্শ	</td>");
		advicePNCCJS = JSON.parse(advicePNCC);
		for(i=1; i <= response[responseSorted[a]].serviceCount; i++){
			var colIdSpan = "#pncchildAdvice-" + responseSorted[a] + "_" + i + "-span";
								
			col = "<td id='pncchildAdvice-" + responseSorted[a] + "_" + i + "'>"
					 + "<span id='pncchildAdvice-" + responseSorted[a] + "_" + i + "-span' style='cursor:pointer'> <strong>"+detail(response[responseSorted[a]][i].advice)+"</strong> </span>";
			
			col = col + "<div id ='pncchildAdvice-" + responseSorted[a] + "_" + i + "-dialog' hidden>" + response[responseSorted[a]][i].advice + "</div></td>";
			
			$(document).on("click",colIdSpan, function(){
				id = $(this).attr('id');
				id = id.split("-")[1];
				colDialogId = "#pncchildAdvice-" + id + "-dialog";
								
				textVal = $(colDialogId).text();
				textVal = textVal.slice(1, -1);
				
				if(textVal!=""){
					textVal = textVal.split(",");
				
					var j;
					var convertedText = "";
					for(j =0; j < textVal.length; j++){
						textVal[j] = textVal[j].slice(1,-1);
						convertedText = convertedText + "- " + advicePNCCJS[0][textVal[j]] + "<br>";						
					}					
					$('#detailList').html(convertedText);
					$('#detailInfoWindow').modal();
				}
				else{
					$('#detailList').html("There is no information");
					$('#detailInfoWindow').modal();					
				}
			});
			tr.append(col);
		}
		
		if (!response.pncStatus) {
			endCol = "<td><select id='pncchildAdvice-"+responseSorted[a]+"' multiple='multiple'>";
			
			endCol = endCol + populateDropdownList(advicePNCCJS[0]);
			
			endCol = endCol + "</select></td>";
			tr.append(endCol);
		}
		tr.append("</tr>");
		tablePNCChild.append(tr);
		
		// row 12
		var tr = $("<tr><td>রেফার	</td>");
		
		for (i = 1; i <= response[responseSorted[a]].serviceCount; i++) {
			var value = response[responseSorted[a]][i].refer.toString();
			
			switch (value) {
				case "1":
					value = 'হ্যাঁ';
					break;
				case "2":
					value = 'না';
					break;			
			}
			col = "<td id='pncchildRefer-" + responseSorted[a] + "-" + i + "'>"
				+ value + "</td>";
			tr.append(col);		
		}
		if (!response.pncStatus) {
			endCol = "<td><input type='checkbox' id='pncchildRefer-" + responseSorted[a] + "'/></td>";
			tr.append(endCol);	
		}
		tr.append("</tr>");
		tablePNCChild.append(tr);
		
		// row 13
		var tr = $("<tr><td>কেন্দ্রের নাম	</td>");
		referCenterJS = JSON.parse(referCenter);
		for (i = 1; i <= response[responseSorted[a]].serviceCount; i++) {
			col = "<td id='pncchildReferCenterName-" + responseSorted[a] + "-" + i + "'>"
				+ referCenterJS[0][response[responseSorted[a]][i].referCenterName] + "</td>";
			tr.append(col);		
		}
		
		if (!response.pncStatus) {
			endCol = "<td>"
					+ "<select id='pncchildReferCenterName-" + responseSorted[a] + "' style='width:167px;' disabled='true'>";
			
			endCol = endCol + populateDropdownList(referCenterJS[0]);		
			
			endCol = endCol + "</select></td>";
			tr.append(endCol);
		}
		tr.append("</tr>");
		tablePNCChild.append(tr);
		
		// row 14
		tr = $("<tr><td>কারণ	</td>");
		referReasonPNCCJS = JSON.parse(referReasonPNCC);
		
		for(i=1; i <= response[responseSorted[a]].serviceCount; i++){
			var colIdSpan = "#pncchildReferReason-" + responseSorted[a] + "_" + i + "-span";
								
			col = "<td id='pncchildReferReason-" + responseSorted[a] + "_" + i + "'>"
					 + "<span id='pncchildReferReason-" + responseSorted[a] + "_" + i + "-span' style='cursor:pointer'> <strong>"+detail(response[responseSorted[a]][i].referReason)+"</strong> </span>";
			
			col = col + "<div id ='pncchildReferReason-" + responseSorted[a] + "_" + i + "-dialog' hidden>" + response[responseSorted[a]][i].referReason + "</div>";
			
			if(!response.pncStatus && i==response[responseSorted[a]].serviceCount){									
				pncCProviderId[a]=response[responseSorted[a]][i].providerId;				
				col = col + "<br>"
				+ "<input type='button' class='right' id='deleteLastButtonPNCChild' value='Delete'/>";
			}
			col = col + "</td>";
			
			$(document).on("click",colIdSpan, function(){
				id = $(this).attr('id');
				id = id.split("-")[1];
				colDialogId = "#pncchildReferReason-" + id + "-dialog";
								
				textVal = $(colDialogId).text();
				textVal = textVal.slice(1, -1);
				
				if(textVal!=""){
					textVal = textVal.split(",");
				
					var j;
					var convertedText = "";
					for(j =0; j < textVal.length; j++){
						textVal[j] = textVal[j].slice(1,-1);
						convertedText = convertedText + "- " + referReasonPNCCJS[0][textVal[j]] + "<br>";
					}
					
					$('#detailList').html(convertedText);
					$('#detailInfoWindow').modal();
				}
				else{
					$('#detailList').html("There is no information");
					$('#detailInfoWindow').modal();					
				}
			});
			tr.append(col);
					
		}
		
		if (!response.pncStatus) {
			endCol = "<td><select id='pncchildReferReason-"+responseSorted[a]+"' multiple='multiple' disabled='true'>";					
			
			endCol = endCol + populateDropdownList(referReasonPNCCJS[0]);
			
			endCol = endCol + "</select><br>"
					+ "<input type='button' class='right' id='saveButtonPNCChild' value='Save'/>"
					+ "</td>";

			tr.append(endCol);
		}
		tr.append("</tr>");
		tablePNCChild.append(tr);
		
		
		tablePNCChild.append("</tbody></table>");
		var heading = "<div id='subheading' class='subheadingside'>বর্তমান সেবার তথ্য: সন্তান </div>";

		$('#pncVisitChild').append(heading);
		$('#pncVisitChild').append(tablePNCChild);
		
		pncchilddangersign = "#pncchildDate-" + responseSorted[a];
		$(pncchilddangersign).combodate({
			format : "YYYY-MM-DD",
			template : "DD MMM YYYY",
			smartDays : true,
			//maxYear : 2050,
			value : moment()
		});
		
		$(document).on("change", pncchilddangersign ,function (){

			if(moment($(this).val()).format("YYYY-MM-DD") > moment().format("YYYY-MM-DD")){
				callDialog(futureDate);
				$(this).combodate('setValue', moment(), true);
			}

		});
		
		pncchilddangersign = "#pncchildDangerSign-" + responseSorted[a];
		pncchildsymptom = "#pncchildSymptom-" + responseSorted[a];
		pncchilddisease = "#pncchildDisease-" + responseSorted[a];
		pncchildtreatment = "#pncchildTreatment-" + responseSorted[a];
		pncchildadvice = "#pncchildAdvice-" + responseSorted[a];
		pncchildreferreason = "#pncchildReferReason-" + responseSorted[a]; 
		
		fieldId = [pncchilddangersign,pncchilddisease,pncchildtreatment,pncchildadvice,pncchildreferreason];
		$.each(fieldId, function(key, val) {
			//multiSelectDropDownInit(val);
			$(val).multipleSelect({
				width : 300,
				position : 'top',
				selectAll: false,
				selectedList : 1,
				styler : function() {
					return 'font-size: 18px;';
				}	
			});
		});
		
		fieldId = [pncchildsymptom];
		$.each(fieldId, function(key, val) {
			//multiSelectDropDownInit(val);
			$(val).multipleSelect({
				width : 300,				
				selectAll: false,
				selectedList : 1,
				styler : function() {
					return 'font-size: 18px;';
				}	
			});
		});
		
		// Only allowing numeric values for input fields
		pncchildtemperature = "#pncchildTemperature-" + responseSorted[a]; 
		$(document).on("keyup", pncchildtemperature, function() {
			$(this).val($(this).val().match(/^\d*\.?\d*$/) ? $(this).val() : "");
		});
		
		pncchildweight = "#pncchildWeight-" + responseSorted[a];
		$(document).on("keyup", pncchildweight, function() {
			$(this).val($(this).val().match(/^\d*\.?\d*$/) ? $(this).val() : "");			
		});
		
		pncchildbreathingperminute = "#pncchildBreathingPerMinute-" + responseSorted[a];
		$(document).on("keyup", pncchildbreathingperminute, function() {
			$(this).val($(this).val().replace(/[^\d]/, ''));			
		});
		
		pncchildRefer="#pncchildRefer-" + responseSorted[a] ;

		$(document).on("click",pncchildRefer,function (){
			
			id=$(this).attr('id').split("-")[1];
		    
		    pncchildRefer="#pncchildRefer-" + id ;
		    pncchildReferCenter="#pncchildReferCenterName-" + id;
		    pncchildReferReason="#pncchildReferReason-"+ id;

		    handleOnclickCheckbox(pncchildRefer,pncchildReferCenter,pncchildReferReason,"enable","");	
		});	
		
		//onclick event on other service provider checkbox
		otherserviceproviderpncc="#otherServiceProviderCheckboxPNCC-" + responseSorted[a] ;
		otherserviceprovidercategorypncc="#otherServiceProviderPNCC-" + responseSorted[a] ;
		$(document).on("click", otherserviceproviderpncc ,function (){
			if($(otherserviceproviderpncc).prop('checked')){
				$(otherserviceprovidercategorypncc).show();		
			}   		
			else{
				$(otherserviceprovidercategorypncc).hide();
			}
		});

	}
	$('div#pncchild').show();
	
	if(hideFlag=="doHide")
	{
		$('div#pncchild').hide();
		$('.nextC').hide();
		hideFlag="";
	}
	
  }	
	
}

/*function populateDropdownList(jsonVal){
	
	output = "";		
	$.each(jsonVal, function(key, val) {
		output = output + "<option value = " + key + ">" + val + "</option>"; 			
	});		
	return output;
}
*/
/*function multiSelectDropDownInit(fieldId){
	$(fieldId).multipleSelect({
		width : 167,
		position : 'top',
		selectAll: false,
		selectedList : 1,
		styler : function() {
			return 'font-size: 14px;';
		}
	
	});
}*/
