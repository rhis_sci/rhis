anemiaList = '[{"0":"","1":"+","2":"++","3":"+++"}]';
anemiaPACJS = JSON.parse(anemiaList);

abdomenList = '[{"1":"স্বাভাবিক (নরম) পেট","2":"অনমনীয় (পীড়িত এবং শক্ত) পেট","3":"চেপে ছেড়ে দিলে ব্যথা","4":"স্বাভাবিকের চেয়ে নরম জরায়ু","5":"জরায়ুতে  চাপ দিলে ব্যথা"}]';
abdomenPACJS = JSON.parse(abdomenList);

uterusInvolutionList = '[{"0":"","1":"গর্ভমেয়াদের সাথে সঙ্গতিপূর্ণ","2":"গর্ভমেয়াদের তুলনায় জরায়ুর আকার কম","3":"গর্ভমেয়াদের তুলনায় জরায়ুর আকার বেশী"}]';
uterusInvolutionPACJS = JSON.parse(uterusInvolutionList);

cervixList = '[{"1":"বন্ধ জরায়ুমুখ","2":"সম্প্রসারিত","3":"পুঁজযুক্ত জরায়ুমুখে স্রাব"}]';
cervixPACJS = JSON.parse(cervixList);

hematuriaList = '[{"1":"স্বাভাবিক","2":"হালকা রক্তক্ষরণ (১টা শুকনো প্যাড কিংবা কাপড় ভিজে যাওয়ার জন্য ৫ মিনিটের বেশী সময় লাগে)","3":"অতিরিক্ত রক্তক্ষরণ (১টা শুকনো প্যাড কিংবা কাপড় ভিজে যাওয়ার জন্য ৫ মিনিটের কম সময় লাগে)","4":"দুর্গন্ধযুক্ত স্রাব"}]';
hematuriaPACJS = JSON.parse(hematuriaList);

perineumList = '[{"0":"","1":"স্বাভাবিক","2":"ছিঁড়ে গিয়েছে","3":"অন্যান্য জটিলতা"}]';
perineumPACJS = JSON.parse(perineumList);

FPMethodList = '[{"0":"","1":"পরামর্শ","2":"Condom","3":"IUD","4":"Emergency Contraceptive Pill (ECP)","5":"Injectable","6":"Implant"}]';
FPMethodPACJS = JSON.parse(FPMethodList);

/**
 * saving PAC information
 */

function insertPACInfo() {

	var PACObj = new Object();
	PACObj = setCommonParam();
	
	PACObj.healthId = $('#healthid').text();
	PACObj.pregNo = $('#pregNumber').text();
	PACObj.providerId = $('#providerid').text();
	PACObj.visitDate = $('#PACDate').val();
	PACObj.temperature = $('#PACTemperature').val();
	PACObj.bpSystolic = $('#PACbpSystolic').val();
	PACObj.bpDiastolic = $('#PACbpDiastolic').val();
	PACObj.anemia = $('#PACAnemia').val();
	PACObj.hemoglobin = $('#PACHemoglobin').val();
	PACObj.abdomen = $('#PACAbdomen').multipleSelect("getSelects");
	PACObj.uterusInvolution = $('#PACUterusInvolution').val();
	PACObj.cervix = $('#PACCervix').multipleSelect("getSelects");
	PACObj.hematuria = $('#PACHematuria').multipleSelect("getSelects");
	PACObj.perineum = $('#PACPerineum').val();
	
	PACObj.FPMethod = $('#PACFPMethod').val();
	
	PACObj.complicationSign = $('#PACComplication').multipleSelect("getSelects");
	PACObj.symptom = $('#PACSymptom').multipleSelect("getSelects");
	PACObj.disease = $('#PACDisease').multipleSelect("getSelects");
	PACObj.treatment = $('#PACTreatment').multipleSelect("getSelects");
	PACObj.advice = $('#PACAdvice').multipleSelect("getSelects");
		
	PACObj.refer = $('#PACRefer').prop('checked') ? 1 : 2;
	PACObj.referReason = $('#PACReferReason').multipleSelect("getSelects");
	PACObj.referCenterName = $('#PACReferCenterName').val();
		
	if($('#PACOtherServiceProviderCheckbox').prop('checked')){
		PACObj.serviceSource = $('#PACOtherServiceProvider').val();
	}
	else{
		PACObj.serviceSource = "";
	}

	PACObj.sateliteCenterName = "";
	PACObj.client = "1";
	PACObj.pacLoad = "";

	
	
	var userVal = "<table class='list'>";
	userVal = userVal + "<tr><td>সেবাগ্রহনের তারিখ: " + moment(PACObj.visitDate).format("DD MMM YYYY") + "</td></tr>";
	
	userVal = userVal + "<tr><td>অসুবিধা:</td></tr>";
		
		textVal=PACObj.symptom;
		symptomPACJS = JSON.parse(symptomPAC);
		var i;
		var convertedText = "  ";

		for(i =0; i < textVal.length; i++){
			convertedText = convertedText + "<tr><td>-"+symptomPACJS[0][textVal[i]] + "</td></tr>";  			
	}
	userVal+=convertedText+"</table>";
		
	userVal = userVal +"<table class='confirmation'><tr><td>"+linemark("তাপমাত্রা",PACObj.temperature)+"</td>";
	
	if(PACObj.bpSystolic!="" && PACObj.bpDiastolic!="" )
		var slash="/";
	else    
		var slash="";
	
	userVal = userVal +"<td colspan='2'>"+((PACObj.bpSystolic=="" || PACObj.bpDiastolic=="") ? "<span style=\"color: red;\">" : "<span style=\"color: black;\">")+ "রক্তচাপ (মিঃ মিঃ মার্কারি):</span>" + PACObj.bpSystolic + slash + PACObj.bpDiastolic+"</td></tr>";
	
	userVal = userVal +"<tr><td>"+ linemark_switch("রক্তস্বল্পতা",PACObj.anemia,'none');
	
	userVal+= anemiaPACJS[0][PACObj.anemia];
	
	userVal+="</td>";
	
	userVal = userVal +"<td>"+ linemark("হিমোগ্লোবিন",PACObj.hemoglobin);
	if(PACObj.hemoglobin!=''){
		userVal+="%";
	}		
    userVal+="</td></tr></table>";
    
    userVal = userVal + "<table class='list'><tr><td>পেট:</tr></td> ";
    textVal=PACObj.abdomen;
	var i;
	var convertedText = "  ";

	for(i =0; i < textVal.length; i++){
		convertedText = convertedText + "<tr><td>-"+abdomenPACJS[0][textVal[i]] + "</td></tr>";  			
	}
	userVal+=convertedText+"</table>";
	
	userVal = userVal +"<table><tr><td> জরায়ুর উচ্চতা: ";
	userVal+= uterusInvolutionPACJS[0][PACObj.uterusInvolution];
	userVal+="</td></tr></table>";

	userVal = userVal +"<table class='list'><tr><td> জরায়ুমুখ: </td></tr>";
	textVal=PACObj.cervix;
	var i;
	var convertedText = "  ";

	for(i =0; i < textVal.length; i++){
		convertedText = convertedText + "<tr><td>-"+cervixPACJS[0][textVal[i]] + "</td></tr>";  			
	}
	userVal+=convertedText;
	
	
    userVal = userVal + "<tr><td>স্রাব / রক্তস্রাব: </td></tr>";
    textVal=PACObj.hematuria;
	var i;
	var convertedText = "  ";

	for(i =0; i < textVal.length; i++){
		convertedText = convertedText + "<tr><td>-"+hematuriaPACJS[0][textVal[i]] + "</td></tr>";  			
	}
	userVal+=convertedText;
	userVal+="</table>";
      
	userVal = userVal +"<table><tr><td> পেরিনিয়াম: ";
	userVal+= perineumPACJS[0][PACObj.perineum];
	userVal+="</td></tr>";
	
	userVal = userVal +"<tr><td> পরিবার পরিকল্পনা পদ্ধতি(পরামর্শ / পদ্ধতি প্রদান): ";
	userVal+= FPMethodPACJS[0][PACObj.FPMethod];
	userVal+="</td></tr></table>";
		
	userVal = userVal + "<table class='list'><tr><td>বিপদ চিহ্ন / জটিলতা:</tr></td> ";
	textVal=PACObj.complicationSign;
	dangerSignPACJS = JSON.parse(dangerSignPAC);
	var i;
	var convertedText = "  ";
	
	for(i =0; i < textVal.length; i++){
		convertedText = convertedText +"<tr><td>-"+dangerSignPACJS[0][textVal[i]] +"</tr></td>";				
	}
	userVal+=convertedText;

  	userVal = userVal + "<tr><td>রোগ:</tr></td>";	
  	textVal=PACObj.disease;
  	diseasePACJS = JSON.parse(diseasePAC);
  	var i;
  	var convertedText = "";
	
  	for(i =0; i < textVal.length; i++){
  		convertedText = convertedText +"<tr><td>-"+ diseasePACJS[0][textVal[i]] + "</tr></td>";
	}
  	userVal+=convertedText;
			
    userVal = userVal + "<tr><td>চিকিৎসা: </tr></td>";
    textVal=PACObj.treatment;
    treatmentJS = JSON.parse(treatment);
    convertedText = "";
	
	for(i =0; i < textVal.length; i++){
		convertedText = convertedText + "<tr><td>-"+treatmentJS[0][textVal[i]] + "</tr></td>";
	}
	userVal+=convertedText;
    
	userVal = userVal + "<tr><td>পরামর্শ:</tr></td>";
	textVal=PACObj.advice;
	advicePACJS = JSON.parse(advicePAC);
	var i;
	var convertedText = "  ";
	
	for(i =0; i < textVal.length; i++){
		convertedText = convertedText + "<tr><td>-"+advicePACJS[0][textVal[i]] + "</tr></td>";
	}
	userVal+=convertedText;
    
    
    userVal = userVal + "<tr><td>রেফার: "+(PACObj.refer=="1" ? "হ্যাঁ" : "না")+"</tr></td>";
    
    if(PACObj.refer=="1"){
    	   userVal = userVal +"<tr><td>"+ linemark_switch("কেন্দ্রের নাম",PACObj.referCenterName,0);		   
		   
    	   referCenterJS = JSON.parse(referCenter);
    	   userVal += referCenterJS[0][PACObj.referCenterName]; 
		   
    	   userVal+="</tr></td>"; 
		   
    	   userVal = userVal +"<tr><td>"+linemark_switch("কারণ",PACObj.referReason,0)+"</tr></td>";
    	   referReasonPACJS = JSON.parse(referReasonPAC);
		   textVal=PACObj.referReason;
		   var i;
		   var convertedText = "  ";
	
		   for(i =0; i < textVal.length; i++){
			   convertedText = convertedText +"<tr><td>-"+ referReasonPACJS[0][textVal[i]] + "</tr></td>";	
		   }
		   userVal+=convertedText.slice(0,-2)+"";
	
    }   
    
    userVal+="</table>";
    if(moment(PACObj.visitDate) >= moment($('#abortionDate').val())){
	    $('<div></div>').dialog({
	        modal: true,
	        width: 680,
	        hide: {effect: 'fade', duration: 500},
	        show: {effect: 'fade', duration: 600},
	        title: "নিশ্চিত করুন",
	        open: function () {
	            $(this).html(userVal);
	        },
	        buttons: {
	        	"Save & Continue" : function () {      
	        		PACInfo(PACObj);
	            	$(this).dialog("close");
	            },           
				"Save & Serve Next Client": function () {
					PACInfo(PACObj);				
	        		goToId='.nextC';	
				    $(goToId).click();	
	            	$(this).dialog("close");
	            },           
	            Cancel: function () {
	        		$(this).dialog("close");
	        	}
	        }
	    }); //end confirm dialog
    }
    else{
    	callDialog(pacDateLessThanAbortion);
    }
}

/**
 * retrieve PAC information
 */
function retrievePACInfo() {
	
	var PACObj = new Object();
	PACObj = setCommonParam();

	PACObj.pregNo = $('#pregNumber').text();
	PACObj.healthId = $('#healthid').text();
	PACObj.pacLoad = "retrieve";
	
	PACInfo(PACObj);	
}

/**
 * delete last PAC information
 */

function deleteLastPACInfo(){
	 
	var PACObj = new Object();
	PACObj = setCommonParam();
	
	PACObj.healthId = $('#healthid').text();
	PACObj.pregNo = $('#pregNumber').text();
	
	PACObj.pacLoad = "delete";
	
	PACInfo(PACObj);	
}


/**
 * insert and retrieve PAC information in a single AJAX call
 */
function PACInfo(PACInfo) {
		
	if (PACInfo.healthId != "" && PACInfo.pregNo != "") {
		$.ajax({
			type : "POST",
			url : "pac",
			timeout:60000, //60 seconds timeout
			dataType : "json",
			data : {"PACInfo" : JSON.stringify(PACInfo)},
			success : function(response) {
				parsePACInfo(response);									
			},
			error : function(xhr, status, error) {
				alert(status);
				alert(error);
			}
		});		
	}	
}

/**
 * populating PAC table dynamically
 */
function parsePACInfo(response) {
	
	if(response.hasAbortionInformation=="Yes"){
		toggleServices("pacEnable");
		if (response.pacStatus) {		
			callDialog(noPAC);			
		}
		
	$('#abortionPlace').val((response.outcomePlace=="0" ? "" :(response.outcomePlace=="1" ? "বাড়ি" : "সেবাকেন্দ্র")));
	$('#abortionDate').val(moment(response.outcomeDate).format("DD MMM YYYY"));
				
	// populating PAC table
	var tablePAC = $("<table id='PACVisitTable' border='1px'><tbody>");
	var tr, col, endCol, i;

	// row 1
	tr = $("<tr><td>পরিদর্শন </td>");
	for (i = 1; i <= response.count; i++) {
		col = "<td>" + en_to_ben_number_conversion(i.toString()) + "</td>";
		tr.append(col);
	}
	if (!response.pacStatus) {
		endCol = "<td>" + en_to_ben_number_conversion(((response.count)+1).toString())
				+ "</td>";
		tr.append(endCol);
	}
	tr.append("</tr>");
	tablePAC.append(tr);

	// row 2
	tr = $("<tr><td>সেবাগ্রহনের তারিখ	</td>");
	otherServiceCenterJS =  JSON.parse(referCenter);
	for (i = 1; i <= response.count; i++) {
		col = "<td id='PACDate-" + i + "'>"
			+ moment(response[i].visitDate).format("DD MMM YYYY");
		if(response[i].serviceSource!=""){
			col = col + "<br>" + otherServiceCenterJS[0][response[i].serviceSource];
		}
		col = col + "</td>";
		tr.append(col);
	}
	if (!response.pacStatus) {
		endCol = "<td><input type='text' id='PACDate' maxlength='11' size='11'/>&nbsp;<input type='checkbox' id='PACOtherServiceProviderCheckbox'/> অন্যান্য উৎস <br>"
				+ "<select id='PACOtherServiceProvider' hidden='true'>";
	
		$.each(otherServiceCenterJS[0], function(key, val) {
			endCol = endCol + "<option value = " + key + ">" + val + "</option>"; 			
		});	
				
		endCol = endCol + "</select></td>"; 
		tr.append(endCol);
	}
	tr.append("</tr>");
	tablePAC.append(tr);
	
	// row 3
	tr = $("<tr><td>অসুবিধা	</td>");
	symptomPACJS = JSON.parse(symptomPAC);
	for(i=1; i<=response.count; i++){
		var colIdSpan = "#PACSymptom-" + i + "-span";
							
		col = "<td id='PACSymptom-" + i + "'>"
				 + "<span id='PACSymptom-" + i + "-span' style='cursor:pointer'> <strong>"+detail(response[i].symptom)+"</strong> </span>";
		
		col = col + "<div id ='PACSymptom-" + i + "-dialog' hidden>" + response[i].symptom + "</div></td>";
		
		$(document).on("click",colIdSpan, function(){
			
			colDialogId = "#PACSymptom-" + $(this).attr('id').split("-")[1] + "-dialog";
			retrieveDetailHistory(colDialogId, symptomPACJS);			
			
		});
		tr.append(col);
	}
	
	if (!response.pacStatus) {
		endCol = "<td><select id='PACSymptom'  multiple='multiple'>";
		
		endCol = endCol + populateDropdownList(symptomPACJS[0]);
		
		endCol = endCol + "</select></td>";
	
		tr.append(endCol);
	}
	tr.append("</tr>");
	tablePAC.append(tr);
	
	// row 4
	tr = $("<tr><td>তাপমাত্রা	</td>");
	for (i = 1; i <= response.count; i++) {
		
		if(response[i].temperature >=t_pactemperature){
			mark=danger;
		}
		else{
			mark=safe;
		}
		
		col = "<td id='PACTemperature-" + i + "'"+mark+">"
			+ en_to_ben_number_conversion((response[i].temperature).toString()) + "</td>";
		tr.append(col);
	}
	if (!response.pacStatus) {
		endCol = "<td><input type='text' id='PACTemperature' maxlength='6' size='6'/></td>";
		tr.append(endCol);
	}
	tr.append("</tr>");
	tablePAC.append(tr);
	
	// row 5
	tr = $("<tr><td><span class=\"tp\" id=\"PAC_bp\">রক্তচাপ (মিঃ মিঃ মার্কারি) </span></td>");
	for (i = 1; i <= response.count; i++) {
		
		if(response[i].bpSystolic!="" || response[i].bpDiastolic!="" ){
			var slash="/";
		}
		else{    
			var slash="";
		}
		
		if((response[i].bpSystolic!='' && response[i].bpDiastolic!='') && ((response[i].bpSystolic<=t_pacbpsys) || (response[i].bpDiastolic>=t_pacbpdias))){
			mark=danger;
		}
		else{
			mark=safe; 
		}
		
		col = "<td id='PACmotherbpSystolic-" + i + "'"+mark+">"
			+ en_to_ben_number_conversion((response[i].bpSystolic).toString())
			+ slash
			+ en_to_ben_number_conversion((response[i].bpDiastolic).toString())
			+ "</td>";
		tr.append(col);
	}
	if (!response.pacStatus) {
		endCol = "<td><input type='text' id='PACbpSystolic' maxlength='3' size='3'/> / <input type='text' id='PACbpDiastolic' maxlength='3' size='3'/></td>";
		tr.append(endCol);
	}
	tr.append("</tr>");
	tablePAC.append(tr);
	
	// row 6
	tr = $("<tr><td><span class=\"tp\" id=\"PAC_anemia\">রক্তস্বল্পতা	</span></td>");
	for (i = 1; i <= response.count; i++) {
		if(response[i].anemia==t_pacanemia){ 
			mark=danger;
		}
		else{
			mark=safe;
		}
		
		col = "<td id='PACAnemia-" + i + "'"+mark+">"
			+ anemiaPACJS[0][response[i].anemia] + "</td>";
		tr.append(col);
	}
	if (!response.pacStatus) {
		
		endCol = "<td><select id='PACAnemia'>";
		
		endCol = endCol + populateDropdownList(anemiaPACJS[0]);
		
		endCol = endCol + "</select></td>";
		
		tr.append(endCol);
	}
	tr.append("</tr>");
	tablePAC.append(tr);
	
	// row 7
	tr = $("<tr><td><span class=\"tp\" id=\"PAC_himoglobin\"> হিমোগ্লোবিন	</span></td>");
	for (i = 1; i <= response.count; i++) {
		
			if(response[i].hemoglobin!='' && response[i].hemoglobin<=t_pachemoglobin){
				mark=danger;
			}		  
			else{
				mark=safe;	
			}
			
			if(response[i].hemoglobin==""){
				percent="";
			}
			else{
				percent="%";
			}
	
		col = "<td id='PACHemoglobin-" + i + "'"+mark+">"
			+ en_to_ben_number_conversion(response[i].hemoglobin) + percent + "</td>";
		tr.append(col);
	}
	if (!response.pacStatus) {
		endCol = "<td><input type='text' id='PACHemoglobin' maxlength='3' size='2'/>%</td>";
		tr.append(endCol);
	}
	tr.append("</tr>");
	tablePAC.append(tr);
	
	// row 8
	tr = $("<tr><td>পেট	</td>");
	for(i=1; i<=response.count; i++){
		var colIdSpan = "#PACAbdomen-" + i + "-span";
							
		col = "<td id='PACAbdomen-" + i + "'>"
				 + "<span id='PACAbdomen-" + i + "-span' style='cursor:pointer'> <strong>"+detail(response[i].abdomen)+"</strong> </span>";
		
		col = col + "<div id ='PACAbdomen-" + i + "-dialog' hidden>" + response[i].abdomen + "</div></td>";
		
		$(document).on("click",colIdSpan, function(){
			
			colDialogId = "#PACAbdomen-" + $(this).attr('id').split("-")[1] + "-dialog";
			retrieveDetailHistory(colDialogId, abdomenPACJS);			
			
		});
		tr.append(col);
	}
	
	if (!response.pacStatus) {
		endCol = "<td><select id='PACAbdomen'  multiple='multiple'>";
		
		endCol = endCol + populateDropdownList(abdomenPACJS[0]);
		
		endCol = endCol + "</select></td>";
	
		tr.append(endCol);
	}
	tr.append("</tr>");
	tablePAC.append(tr);
	
	// row 9
	tr = $("<tr><td><span id=\"PAC_uterusInvolution\">জরায়ুর উচ্চতা	</span></td>");
	for (i = 1; i <= response.count; i++) {
		if(response[i].uterusInvolution==t_pacuterusinvolution){ 
			mark=danger;
		}
		else{
			mark=safe;
		}
		
		col = "<td id='PACUterusInvolution-" + i + "'"+mark+">"
			+ uterusInvolutionPACJS[0][response[i].uterusInvolution] + "</td>";
		tr.append(col);
	}
	if (!response.pacStatus) {
		
		endCol = "<td><select id='PACUterusInvolution'>";
		
		endCol = endCol + populateDropdownList(uterusInvolutionPACJS[0]);
		
		endCol = endCol + "</select></td>";
		
		tr.append(endCol);
	}
	tr.append("</tr>");
	tablePAC.append(tr);
	
	// row 10
	tr = $("<tr><td>জরায়ুমুখ	</td>");
	for(i=1; i<=response.count; i++){
		var colIdSpan = "#PACCervix-" + i + "-span";
							
		col = "<td id='PACCervix-" + i + "'>"
				 + "<span id='PACCervix-" + i + "-span' style='cursor:pointer'> <strong>"+detail(response[i].cervix)+"</strong> </span>";
		
		col = col + "<div id ='PACCervix-" + i + "-dialog' hidden>" + response[i].cervix + "</div></td>";
		
		$(document).on("click",colIdSpan, function(){
			
			colDialogId = "#PACCervix-" + $(this).attr('id').split("-")[1] + "-dialog";
			retrieveDetailHistory(colDialogId, cervixPACJS);			
			
		});
		tr.append(col);
	}
	
	if (!response.pacStatus) {
		endCol = "<td><select id='PACCervix'  multiple='multiple'>";
		
		endCol = endCol + populateDropdownList(cervixPACJS[0]);
		
		endCol = endCol + "</select></td>";
	
		tr.append(endCol);
	}
	tr.append("</tr>");
	tablePAC.append(tr);
	
	// row 11
	tr = $("<tr><td>স্রাব/ রক্তস্রাব	</td>");
	for(i=1; i<=response.count; i++){
		var colIdSpan = "#PACHematuria-" + i + "-span";
							
		col = "<td id='PACHematuria-" + i + "'>"
				 + "<span id='PACHematuria-" + i + "-span' style='cursor:pointer'> <strong>"+detail(response[i].hematuria)+"</strong> </span>";
		
		col = col + "<div id ='PACHematuria-" + i + "-dialog' hidden>" + response[i].hematuria + "</div></td>";
		
		$(document).on("click",colIdSpan, function(){
			
			colDialogId = "#PACHematuria-" + $(this).attr('id').split("-")[1] + "-dialog";
			retrieveDetailHistory(colDialogId, hematuriaPACJS);			
			
		});
		tr.append(col);
	}
	
	if (!response.pacStatus) {
		endCol = "<td><select id='PACHematuria'  multiple='multiple'>";
		
		endCol = endCol + populateDropdownList(hematuriaPACJS[0]);
		
		endCol = endCol + "</select></td>";
	
		tr.append(endCol);
	}
	tr.append("</tr>");
	tablePAC.append(tr);
	
	// row 12
	tr = $("<tr><td><span class=\"tp\" id=\"PAC_perineum\">পেরিনিয়াম</span></td>");
	for (i = 1; i <= response.count; i++) {
		if(response[i].perineum==t_pacperineum){ 
			mark=danger;
		}
		else{
			mark=safe;
		}
		
		col = "<td id='PACPerineum-" + i + "'"+mark+">"
			+ perineumPACJS[0][response[i].perineum] + "</td>";
		tr.append(col);
	}
	if (!response.pacStatus) {
		
		endCol = "<td><select id='PACPerineum'>";
		
		endCol = endCol + populateDropdownList(perineumPACJS[0]);
		
		endCol = endCol + "</select></td>";
		
		tr.append(endCol);
	}
	tr.append("</tr>");
	tablePAC.append(tr);
	
	// row 13
	tr = $("<tr><td style='white-space: nowrap'>পরিবার পরিকল্পনা পদ্ধতি(পরামর্শ / পদ্ধতি প্রদান)	</td>");
	for (i = 1; i <= response.count; i++) {
		if(response[i].FPMethod==t_pacfpmethod){ 
			mark=danger;
		}
		else{
			mark=safe;
		}
		
		col = "<td id='PACFPMethod-" + i + "'"+mark+">"
			+ FPMethodPACJS[0][response[i].FPMethod] + "</td>";
		tr.append(col);
	}
	if (!response.pacStatus) {
		
		endCol = "<td><select id='PACFPMethod'>";
		
		endCol = endCol + populateDropdownList(FPMethodPACJS[0]);
		
		endCol = endCol + "</select></td>";
		
		tr.append(endCol);
	}
	tr.append("</tr>");
	tablePAC.append(tr);
	
	// row 14
	dangerSignPACJS = JSON.parse(dangerSignPAC);
	tr = $("<tr><td>বিপদ চিহ্ন / জটিলতা	</td>");
	for(i=1; i<=response.count; i++){
		var colIdSpan = "#PACComplication-" + i + "-span";
							
		col = "<td id='PACComplication-" + i + "'>"
				 + "<span id='PACComplication-" + i + "-span' style='cursor:pointer'> <strong>"+detail(response[i].complicationSign)+"</strong> </span>";
		
		col = col + "<div id ='PACComplication-" + i + "-dialog' hidden>" + response[i].complicationSign + "</div></td>";
		
		$(document).on("click",colIdSpan, function(){
			
			colDialogId = "#PACComplication-" + $(this).attr('id').split("-")[1] + "-dialog";
			retrieveDetailHistory(colDialogId, dangerSignPACJS);			
			
		});
		tr.append(col);
	}
	
	if (!response.pacStatus) {
		endCol = "<td><select id='PACComplication'  multiple='multiple'>";
		
		endCol = endCol + populateDropdownList(dangerSignPACJS[0]);
		
		endCol = endCol + "</select></td>";
	
		tr.append(endCol);
	}
	tr.append("</tr>");
	tablePAC.append(tr);
		
	// row 15
	tr = $("<tr><td>রোগ	</td>");
	diseasePACJS = JSON.parse(diseasePAC);
	for(i=1; i<=response.count; i++){
		var colIdSpan = "#PACDisease-" + i + "-span";
							
		col = "<td id='PACDisease-" + i + "'>"
				 + "<span id='PACDisease-" + i + "-span' style='cursor:pointer'> <strong>"+detail(response[i].disease)+"</strong> </span>";
		
		col = col + "<div id ='PACDisease-" + i + "-dialog' hidden>" + response[i].disease + "</div></td>";
		
		$(document).on("click",colIdSpan, function(){
			
			colDialogId = "#PACDisease-" + $(this).attr('id').split("-")[1] + "-dialog";
			retrieveDetailHistory(colDialogId, diseasePACJS);			
			
		});
		tr.append(col);
	}
	
	if (!response.pacStatus) {
		endCol = "<td><select id='PACDisease'  multiple='multiple'>";
		
		endCol = endCol + populateDropdownList(diseasePACJS[0]);
		
		endCol = endCol + "</select></td>";
	
		tr.append(endCol);
	}
	tr.append("</tr>");
	tablePAC.append(tr);
	
	// row 16
	tr = $("<tr><td>চিকিৎসা	</td>");
	
	treatmentJS = JSON.parse(treatment);
	for(i=1; i<=response.count; i++){
		var colIdSpan = "#PACTreatment-" + i + "-span";
							
		col = "<td id='PACTreatment-" + i + "'>"
				 + "<span id='PACTreatment-" + i + "-span' style='cursor:pointer'> <strong>"+detail(response[i].treatment)+"</strong> </span>";
		
		col = col + "<div id ='PACTreatment-" + i + "-dialog' hidden>" + response[i].treatment + "</div></td>";
		
		$(document).on("click",colIdSpan, function(){
			
			colDialogId = "#PACTreatment-" + $(this).attr('id').split("-")[1] + "-dialog";
			retrieveDetailHistory(colDialogId, treatmentJS);			
			
		});
		tr.append(col);
	}
	
	if (!response.pacStatus) {
		endCol = "<td><select id='PACTreatment'  multiple='multiple'>";
		
		endCol = endCol + populateDropdownList(treatmentJS[0]);
		
		endCol = endCol + "</select></td>";
	
		tr.append(endCol);
	}
	tr.append("</tr>");
	tablePAC.append(tr);
	
	
	// row 17
	tr = $("<tr><td>পরামর্শ	</td>");
	advicePACJS = JSON.parse(advicePAC);
	for(i=1; i<=response.count; i++){
		var colIdSpan = "#PACAdvice-" + i + "-span";
							
		col = "<td id='PACAdvice-" + i + "'>"
				 + "<span id='PACAdvice-" + i + "-span' style='cursor:pointer'> <strong>"+detail(response[i].advice)+"</strong> </span>";
		
		col = col + "<div id ='PACAdvice-" + i + "-dialog' hidden>" + response[i].advice + "</div></td>";
		
		$(document).on("click",colIdSpan, function(){
			
			colDialogId = "#PACAdvice-" + $(this).attr('id').split("-")[1] + "-dialog";
			retrieveDetailHistory(colDialogId, advicePACJS);			
			
		});
		tr.append(col);
	}
	
	if (!response.pacStatus) {
		endCol = "<td><select id='PACAdvice'  multiple='multiple'>";
		
		endCol = endCol + populateDropdownList(advicePACJS[0]);
		
		endCol = endCol + "</select></td>";
	
		tr.append(endCol);
	}
	tr.append("</tr>");
	tablePAC.append(tr);
	
	// row 18
	var tr = $("<tr><td>রেফার	</td>");
	
	for (i = 1; i <= response.count; i++) {
		var value = response[i].refer;
		switch (value) {
			case '1':
				value = 'হ্যাঁ';
				break;
			case '2':
				value = 'না';
				break;			
		}
		col = "<td id='PACRefer-" + i + "'>"
			+ value + "</td>";
		tr.append(col);		
	}
	if (!response.pacStatus) {
		endCol = "<td><input type='checkbox' id='PACRefer'/></td>";
		tr.append(endCol);	
	}
	tr.append("</tr>");
	tablePAC.append(tr);
	
	// row 19
	var tr = $("<tr><td>কেন্দ্রের নাম	</td>");
	referCenterJS = JSON.parse(referCenter);
	for (i = 1; i <= response.count; i++) {
		col = "<td id='PACReferCenterName-" + i + "'>"
			+ referCenterJS[0][response[i].referCenterName] + "</td>";
		tr.append(col);		
	}
	
	if (!response.pacStatus) {
		endCol = "<td>"
				+ "<select id='PACReferCenterName' disabled='true'>";
		
		endCol = endCol + populateDropdownList(referCenterJS[0]);			
		
		endCol = endCol + "</select></td>";
		tr.append(endCol);
	}
	tr.append("</tr>");
	tablePAC.append(tr);
	
	// row 20
	tr = $("<tr><td>কারণ	</td>");
	referReasonPACJS = JSON.parse(referReasonPAC);
	for(i=1; i<=response.count; i++){
		var colIdSpan = "#PACReferReason-" + i + "-span";
							
		col = "<td id='PACReferReason-" + i + "'>"
				 + "<span id='PACReferReason-" + i + "-span' style='cursor:pointer'> <strong>"+detail(response[i].referReason)+"</strong> </span>";
		
		col = col + "<div id ='PACReferReason-" + i + "-dialog' hidden>" + response[i].referReason + "</div>";
		
		if(!response.pncStatus && i==response.count){					
			
			pacProviderId=response[i].providerId;			
			col = col + "<br>"
			+ "<input type='button' class='right' id='deleteLastButtonPAC' value='Delete'/>";			
		}
		col = col + "</td>";
		
		$(document).on("click",colIdSpan, function(){
			
			colDialogId = "#PACReferReason-" + $(this).attr('id').split("-")[1] + "-dialog";
			retrieveDetailHistory(colDialogId, referReasonPACJS);			
			
		});
		tr.append(col);
	}
	
	if (!response.pacStatus) {
		endCol = "<td><select id='PACReferReason'  multiple='multiple' disabled='true'>";
		
		endCol = endCol + populateDropdownList(referReasonPACJS[0]);
		
		//endCol = endCol + "</select></td>";
		endCol = endCol + "</select><br>"
		+ "<input type='button' class='right' id='saveButtonPAC' value='Save'/>"
		+ "</td>";
	
		tr.append(endCol);
	}
	tr.append("</tr>");
	tablePAC.append(tr);
	
	tablePAC.append("</tbody></table>");
	$('#PACVisitTable').remove();
	$('#pacVisit').append(tablePAC);
	
	$('#PACDate').combodate({
		format : "YYYY-MM-DD",
		template : "DD MMM YYYY",
		smartDays : true,
		//maxYear : 2050,
		value : moment()
	});
	
	fieldId = ["#PACAbdomen","#PACCervix","#PACHematuria","#PACComplication","#PACSymptom","#PACDisease","#PACTreatment","#PACAdvice","#PACReferReason"];
	$.each(fieldId, function(key, val) {
		//multiSelectDropDownInit(val);
		$(val).multipleSelect({
			width : 300,
			position : 'top',
			selectAll: false,
			selectedList : 1,
			maxHeight: 100,
			styler : function() {
				return 'font-size: 18px;';
			}	
		});
	});	
	
	
	//$('div#pac').show();
	}
}

/*function populateDropdownList(jsonVal){
	
	output = "";		
	$.each(jsonVal, function(key, val) {
		output = output + "<option value = " + key + ">" + val + "</option>"; 			
	});		
	return output;
}
*/

//onclick event on other service provider checkbox
$(document).on("click", '#PACOtherServiceProviderCheckbox' ,function (){
	if($('#PACOtherServiceProviderCheckbox').prop('checked')){
		$('#PACOtherServiceProvider').show();		
	}   		
	else{
		$('#PACOtherServiceProvider').hide();
	}
});

function retrieveDetailHistory(colDialogId, jsonObject){
	
	textVal = $(colDialogId).text();
	textVal = textVal.slice(1, -1);
		
	if(textVal!=""){
		textVal = textVal.split(",");
		var j;
		var convertedText = "";
	
		for(j =0; j < textVal.length; j++){
			textVal[j] = textVal[j].slice(1,-1);
			convertedText = convertedText + "- " + jsonObject[0][textVal[j]] + "<br>";					
		}
		$('#detailList').html(convertedText);
		$('#detailInfoWindow').modal();
	
	}
	else{
		$('#detailList').html("There is no information")
		$('#detailInfoWindow').modal();
	}
}

// Only allowing numeric values for input fields and convert that to Bangla
$(document).on("keyup", '#PACTemperature', function() {
	$(this).val($(this).val().match(/^\d*\.?\d*$/) ? $(this).val() : "");
});

$(document).on("keyup", '#PACbpSystolic', function() {
	$(this).val($(this).val().replace(/[^\d]/, ''));
	// $(this).val(en_to_ben_number_conversion($('#bpSystolicANC').val()));
});

$(document).on("keyup", '#PACbpDiastolic', function() {
	$(this).val($(this).val().replace(/[^\d]/, ''));
	// $(this).val(en_to_ben_number_conversion($('#bpDiastolicANC').val()));
});

$(document).on("keyup", '#PACHemoglobin', function() {
	$(this).val($(this).val().replace(/[^\d]/, ''));
});

$(document).on("click", '#PACRefer',function (){
	handleOnclickCheckbox("#PACRefer","#PACReferCenterName", "#PACReferReason","enable","");	
});