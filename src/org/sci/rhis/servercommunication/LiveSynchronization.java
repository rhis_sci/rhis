package org.sci.rhis.servercommunication;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.Properties;

import org.sci.rhis.util.ConfInfoRetrieve;

/**
 * @author sabah.mugab
 * @since December, 2015
 */

public class LiveSynchronization {
	
	private static int READ_TIMEOUT = 15000;
    private static int CONNECTION_TIMEOUT = 15000;

	public static void synchronize(String jsonString, String servletName){
		
		String propLoc = "resource/domain.properties";
		Properties prop = ConfInfoRetrieve.readingConf(propLoc);
		
		boolean server = isServer(prop.getProperty("DOMAIN_NAME"));
		
		if(!server){
			System.out.println("Realtime communication with central server");
			System.out.println("Central server " + server + "\n" + prop.getProperty("URL")+servletName + "\n" + jsonString);
			serverSync(prop.getProperty("URL")+servletName,jsonString);
		}		
	}
	
	private static boolean isServer(String domainName){
		
		boolean status = true;
		InetAddress ipLocal, ipServer;
		
		try{
            ipLocal = InetAddress.getLocalHost();
            ipServer = InetAddress.getByName(domainName);
            
            if(!ipServer.equals(ipLocal)){
            	status = false;
            }
        } 
        catch (UnknownHostException e){
        	System.out.println("No internet connection or invalid host");
        	e.printStackTrace();
        	return status;
        }
		
		return status;
	}
	
	private static void serverSync(String uri, String jsonString){
		
		try{
			URL url = new URL(uri);
			
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		    conn.setReadTimeout(READ_TIMEOUT);
		    conn.setConnectTimeout(CONNECTION_TIMEOUT);
		    conn.setRequestMethod("POST");
		    conn.setDoInput(true);
		    conn.setDoOutput(true);

		    OutputStream os = conn.getOutputStream();
		    BufferedWriter writer =
		        new BufferedWriter(
		            new OutputStreamWriter(os, "UTF-8"));
		    writer.write(jsonString);

		    writer.flush();
		    writer.close();
		    os.close();	
		    
		    System.out.println("Server responsecode " + conn.getResponseCode());
		    
		}
		catch(MalformedURLException mul) {
            mul.getStackTrace();
        } 
		catch (ProtocolException pe) {
			pe.getStackTrace();
        }
		catch (IOException io) {
            io.getStackTrace();

        }
		catch (Exception e) {
            e.getStackTrace();
        }		
	}
}