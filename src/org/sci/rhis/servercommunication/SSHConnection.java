package org.sci.rhis.servercommunication;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.Session;

/**
 * @author sabah.mugab
 * @since April, 2018
 */
public class SSHConnection {
	
	private Session session;
	private Channel channel;
	private String host;
	private String user;
	private String password;
	private int port;
	
	SSHConnection(){
		session = null;
		channel = null;
		host = null;
		user = null;
		password = null;
		port = 0;
	}
	
	protected void setSession(Session unixSession){
		session = unixSession;
	}
	
	protected void setChannel(Channel unixChannel){
		channel = unixChannel;
	}
	
	protected void setHost(String unixHost){
		host = unixHost;
	}
	
	protected void setUser(String unixUser){
		user = unixUser;
	}
	
	protected void setPassword(String unixPassword){
		password = unixPassword;
	}
	
	protected void setPort(int unixPort){
		port = unixPort;
	}
	
	public Session getSession(){
		return session;
	}
	
	public Channel getChannel(){
		return channel;
	}
	
	public String getHost(){
		return host;
	}
	
	public String getUser(){
		return user;
	}
	
	public String getPassword(){
		return password;
	}
	
	public int getPort(){
		return port;
	}
}