package org.sci.rhis.servercommunication;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Properties;

import org.sci.rhis.db.PropertiesReader;

import com.jcraft.jsch.JSch;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSchException;

/**
 * @author sabah.mugab
 * @since April, 2018
 */
public class UnixOperation {
	
	private SSHConnection getUnixSession(SSHConnection ssh){
		
		JSch jsch = new JSch();
		Properties config = new Properties();
		try {
			config.put("StrictHostKeyChecking", "no");
			ssh.setSession(jsch.getSession(ssh.getUser(), ssh.getHost(), ssh.getPort()));
			ssh.getSession().setPassword(ssh.getPassword());
			ssh.getSession().setConfig(config);
			ssh.getSession().connect();
		} 
		catch (JSchException e) {
			e.printStackTrace();
		}		
		return ssh;
	}
	
	private void getUnixSessionChannel(SSHConnection ssh, String command){
				
		try {
			ssh.setChannel(getUnixSession(ssh).getSession().openChannel("exec"));
			((ChannelExec)ssh.getChannel()).setCommand(command);
	        ssh.getChannel().setInputStream(null);
	        ((ChannelExec)ssh.getChannel()).setErrStream(System.err);

	        ssh.getChannel().connect();
		} 
		catch (JSchException e) {
			e.printStackTrace();
		}
	}
	
	private void setSSHConnection(SSHConnection ssh, String[] hostInformation){
		ssh.setHost(hostInformation[0]);
		ssh.setUser(hostInformation[1]);
		ssh.setPassword(hostInformation[2]);
		ssh.setPort(Integer.valueOf(hostInformation[3]));		
	}
			
	public ArrayList<String> getUnixResponse(String districtId, String unixCommand){
		ArrayList<String> result = new ArrayList<String>();
		SSHConnection ssh = null; 
		
		try {
			ssh = new SSHConnection(); 
			setSSHConnection(ssh, PropertiesReader.getPropReader().getHostProperties().getProperty(districtId).split(","));
			getUnixSessionChannel(ssh, unixCommand);
			
			BufferedReader reader = new BufferedReader(new InputStreamReader(ssh.getChannel().getInputStream()));
			String line;
			while ((line = reader.readLine()) != null)
            {
                result.add(line);
            }
		} 
		catch (IOException e) {
			e.printStackTrace();
		}
		finally{
			disconnect(ssh);
		}
		return result;
	}
	
	private void disconnect(SSHConnection ssh){
		ssh.getChannel().disconnect();
		ssh.getSession().disconnect();
		
		ssh = null;
	}
}