package org.sci.rhis.allserviceinfo;

import org.json.JSONObject;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class GetAllServiceInfo {
    public JSONObject callServiceMethod(Class<?> className,JSONObject searchClient) throws NoSuchMethodException, IllegalAccessException, InstantiationException, InvocationTargetException {

        Class [] params = new Class[1];
        params[0] = JSONObject.class;

        //get the method reference of a class
        Method m = className.getMethod("getDetailInfo",params);
        System.out.println(className.getMethods());

        Object arglist[] = new Object[1];
        arglist[0] = searchClient;

        return (JSONObject)m.invoke(className.getConstructor().newInstance(),arglist[0]);

    }
}
