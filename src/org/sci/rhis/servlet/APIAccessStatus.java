package org.sci.rhis.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author sabah.mugab
 * @since February, 2019
 */
@SuppressWarnings("serial")
public class APIAccessStatus extends HttpServlet {
	
	public void doPost(HttpServletRequest request, HttpServletResponse response){

		try{
			response.setContentType("application/html; charset=UTF-8");
			response.setCharacterEncoding("UTF-8");
			PrintWriter out = response.getWriter();
			
			System.out.println("API ACCESS Status Servlet: Accessible");
			out.print("API is accessible!!!");
			out.flush();
			out.close();			
		}
		catch(IOException e){
			e.printStackTrace();			
		}
		catch(Exception e){
			e.printStackTrace();			
		}
	}
	
	public void doGet(HttpServletRequest request, HttpServletResponse response){

		try{
			response.setContentType("application/html; charset=UTF-8");
			response.setCharacterEncoding("UTF-8");
			PrintWriter out = response.getWriter();
						
			System.out.println("API ACCESS Status Servlet: Accessible");
			out.print("API is accessible!!!");
			out.flush();
			out.close();			
		}
		catch(IOException e){
			e.printStackTrace();			
		}
		catch(Exception e){
			e.printStackTrace();			
		}
	}
}