package org.sci.rhis.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;
import org.sci.rhis.uploaddownload.UploadDownloadStatusHandler;

/**
 * @author sabah.mugab
 * @since April, 2018
 */
@SuppressWarnings("serial")
public class UploadDownloadStatus extends HttpServlet {

	public JSONObject jsUploadDownloadStatus = new JSONObject();
	
	public void doPost(HttpServletRequest request, HttpServletResponse response){

		try{
			response.setContentType("application/json; charset=UTF-8");
			response.setCharacterEncoding("UTF-8");
			PrintWriter out = response.getWriter();
			JSONObject uploadDownloadStatus;
			System.out.println(request.getParameter("info"));
			uploadDownloadStatus = new JSONObject(request.getParameter("info"));
			jsUploadDownloadStatus = UploadDownloadStatusHandler.changeStatus(uploadDownloadStatus);
			System.out.println("Upload Download Status Servlet: " + jsUploadDownloadStatus);
			out.print(jsUploadDownloadStatus);
			out.flush();
			out.close();			
		}
		catch(ParseException e){
			e.printStackTrace();			
		}
		catch(IOException e){
			e.printStackTrace();			
		}
		catch(Exception e){
			e.printStackTrace();			
		}
	}
}