package org.sci.rhis.servlet;

import java.io.*;
import java.text.ParseException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jdk.nashorn.internal.parser.JSONParser;
import org.json.JSONObject;
import org.sci.rhis.ssp.SSPlanning;

/**
 * @author sabah.mugab
 * @since June, 2019
 */

@SuppressWarnings("serial")
public class SatelliteSessionPlanningServlet extends HttpServlet {

    public JSONObject jsSSPInfo = new JSONObject();

    public void doPost(HttpServletRequest request, HttpServletResponse response) {

        try {
            response.setContentType("application/json; charset=UTF-8");
            response.setCharacterEncoding("UTF-8");
            PrintWriter out = response.getWriter();
            JSONObject SSPInfo;
            //part for sending req from postman
//            String body = readInput(request);
//            SSPInfo = new JSONObject(body);


			SSPInfo = new JSONObject(request.getParameter("SSPInfo"));
			jsSSPInfo = SSPlanning.getDetailInfo(SSPInfo);
            System.out.println("Satellite Session Planning Servlet: " + jsSSPInfo);
            out.print(jsSSPInfo);
            out.flush();
            out.close();

        } catch (ParseException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String readInput(HttpServletRequest request) throws IOException {
        StringBuilder stringBuilder = new StringBuilder();
        try {
            InputStream inputStream = request.getInputStream();
            if (inputStream != null) {
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                char[] charBuffer = new char[128];
                int bytesRead = -1;
                while ((bytesRead = bufferedReader.read(charBuffer)) > 0) {
                    stringBuilder.append(charBuffer, 0, bytesRead);
                }
            } else {
                stringBuilder.append("");
            }
        } catch (Exception ex) {
            throw ex;
        }
        String body = stringBuilder.toString();
        return body;
    }
}