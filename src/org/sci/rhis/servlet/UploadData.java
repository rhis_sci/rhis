package org.sci.rhis.servlet;

import java.io.*;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;

import org.json.JSONObject;
import org.sci.rhis.uploaddownload.UploadDownloadStatusHandler;
import org.sci.rhis.util.FileOperation;

/**
 * @author sabah.mugab
 * @since September, 2018
 */
@SuppressWarnings("serial")
@WebServlet(name = "UploadData", urlPatterns = {"/upload"})
@MultipartConfig
public class UploadData extends HttpServlet {
	
	private final static String serverPath = System.getProperty("catalina.base") + File.separator + "webapps" + File.separator;

	protected void doPost(HttpServletRequest request, HttpServletResponse response) {

		OutputStream out = null;
	    InputStream filecontent = null;
	    PrintWriter writer = null;
	    
		try{
			writer = response.getWriter();
			
			JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("uploadstatus","in-complete");
			
			response.setContentType("text/html;charset=UTF-8");
		    final Part filePart = request.getPart("file");

			/**
			 * Added for getting specific DB in uploaddownloadstatushandler
			 * customheader Ex: 134002:13:47
 			 */
		    String headers = request.getHeader("CustomHeader");
		    String[] params = headers.split(":");
			jsonResponse.put("providerid",params[0]);
		    jsonResponse.put("zillaid",params[1]);
		    jsonResponse.put("upazilaid",params[2]);

		    String fileName = getFileName(filePart);
		    jsonResponse.put("providerid",fileName.split("_")[0]);
	
		    String uploadedFile = serverPath + (fileName.endsWith(".zip") ? "facility_uploaded_db" : "unsync_data") + File.separator + fileName;
		    out = new FileOutputStream(new File(uploadedFile));
		    filecontent = filePart.getInputStream();
	
		    int read = 0;
		    final byte[] bytes = new byte[1024];
	
		    while ((read = filecontent.read(bytes)) != -1) {
		    	out.write(bytes, 0, read);
		    }
		      
		    if(new File(uploadedFile).isFile()){
		    	jsonResponse.put("uploadstatus","complete");
		    	if(!uploadedFile.endsWith(".zip")){
			    	if(!FileOperation.getChecksum(uploadedFile).equals(
			    		FileOperation.readingFromFile(uploadedFile + "_checksum"))){
			    		
			    		jsonResponse.put("uploadstatus","in-complete");
				   	}
		    	}
		    }
		    
		    if(jsonResponse.getString("uploadstatus").equals("complete")){
		    	UploadDownloadStatusHandler.changeStatus(jsonResponse);
		    }
		    	
		    writer.println(jsonResponse);		        
	    }
		catch(ServletException se){
			se.printStackTrace();			
		}
		catch (FileNotFoundException fne) {
	    	fne.printStackTrace();
	    }
		catch(IOException e){
			e.printStackTrace();			
		}
		catch(Exception e){
			e.printStackTrace();			
		}				    
		finally {
			try{
				if (out != null) {
					out.close();
				}
			    if (filecontent != null) {
			    	filecontent.close();
			    }
			    if (writer != null) {
			    	writer.close();
			    }
			}
			catch(IOException ioe){
				ioe.printStackTrace();
			}
		}
	 }

	 private String getFileName(Part filePart) {
	    String header = filePart.getHeader("content-disposition");
	    String name = header.substring(header.indexOf("filename=\"")+10);
	    return name.substring(0, name.indexOf("\""));
	 }
}