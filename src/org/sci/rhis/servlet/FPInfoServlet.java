package org.sci.rhis.servlet;

import org.json.JSONObject;
import org.sci.rhis.fp.FPInfo;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
//import org.sci.rhis.servercommunication.LiveSynchronization;

/**
 * @author sabah.mugab
 * @since September, 2016
 */
@SuppressWarnings("serial")
public class FPInfoServlet extends HttpServlet {

    public JSONObject jsFPInfo = new JSONObject();

    public void doPost(HttpServletRequest request, HttpServletResponse response) {

        try {
            response.setContentType("application/json; charset=UTF-8");
            response.setCharacterEncoding("UTF-8");
            PrintWriter out = response.getWriter();
            JSONObject fpInfo;

            fpInfo = new JSONObject(request.getParameter("fpInfo"));
            jsFPInfo = FPInfo.getDetailInfo(fpInfo);
            System.out.println("FP Info Servlet: " + jsFPInfo);
            out.print(jsFPInfo);
            out.flush();
            out.close();

            //trying to synchronize real time in application level
            //LiveSynchronization.synchronize("iudInfo="+request.getParameter("iudInfo"),request.getRequestURI().split("/")[(request.getRequestURI().split("/")).length-1]);
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}