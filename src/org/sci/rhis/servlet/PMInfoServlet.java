package org.sci.rhis.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;
import org.sci.rhis.permanentmethod.PMInfo;

/**
 * @author sabah.mugab
 * @since February, 2019
 */
@SuppressWarnings("serial")
public class PMInfoServlet extends HttpServlet {

	public JSONObject jsPMInfo = new JSONObject();
	
	public void doPost(HttpServletRequest request, HttpServletResponse response){

		try{
		response.setContentType("application/json; charset=UTF-8");
		response.setCharacterEncoding("UTF-8");
		PrintWriter out = response.getWriter();
		JSONObject pmsInfo;
		
			pmsInfo = new JSONObject(request.getParameter("pmsInfo"));
			jsPMInfo = PMInfo.getDetailInfo(pmsInfo);
			System.out.println("Permanent Method Servlet: " + jsPMInfo);
			out.print(jsPMInfo);
			out.flush();
			out.close();			
		}
		catch(ParseException e){
			e.printStackTrace();			
		}
		catch(IOException e){
			e.printStackTrace();			
		}
		catch(Exception e){
			e.printStackTrace();			
		}
	}
}
