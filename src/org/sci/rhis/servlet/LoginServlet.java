package org.sci.rhis.servlet;

import java.io.*;
import java.text.ParseException;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;
import org.sci.rhis.login.LoginDao;
import org.sci.rhis.util.FileOperation;

/**
 * @author sabah.mugab
 * @since July, 2015
 */
@SuppressWarnings("serial")
public class LoginServlet extends HttpServlet {
		
	public void doPost(HttpServletRequest request, HttpServletResponse response){

		response.setContentType("text/html; charset=UTF-8");
		response.setCharacterEncoding("UTF-8");
		//response.setHeader("Access-Control-Allow-Origin", "*");//cross domain request/CORS
		
		
		JSONObject loginCred;
		
		try {
			PrintWriter out = response.getWriter();
			System.out.println("location: " + System.getProperty("user.dir"));
			System.out.println("Service Name: " + request.getRequestURI().split("/")[1]);//request.getRequestURI());

			loginCred = new JSONObject(request.getParameter("loginInfo"));
			loginCred.put("loginrequest", true);
			JSONObject loginResponse = new LoginDao().validate(loginCred);
			
			if(loginResponse!=null && Boolean.valueOf(loginResponse.getString("status"))){
				if(loginCred.getString("client").equals("1")){
					request.setAttribute("uName", loginResponse.get("pName"));
					request.setAttribute("uID", loginResponse.get("pID"));
					request.setAttribute("ProvType", loginResponse.get("pType"));
					request.setAttribute("csba", loginResponse.get("csba"));
					request.setAttribute("zillaid", loginResponse.get("zillaid"));
		 			request.setAttribute("upazilaid", loginResponse.get("upazilaid"));
					request.setAttribute("unionid", loginResponse.get("unionid"));
					request.setAttribute("faciltyName", loginResponse.get("facilityName"));
					RequestDispatcher requestDispatcher = request.getRequestDispatcher("jsp/home.jsp");
				    requestDispatcher.forward(request, response);
				}
				else if(loginCred.getString("client").equals("2")){
					JSONObject providerInfo = new JSONObject();
					
					providerInfo.put("ProvName", loginResponse.get("pName"));
					providerInfo.put("ProvCode", loginResponse.get("pID"));
					providerInfo.put("ProvType", loginResponse.get("pType"));
					providerInfo.put("csba", loginResponse.get("csba"));
					providerInfo.put("divid", loginResponse.get("divid"));
					providerInfo.put("zillaid", loginResponse.get("zillaid"));
					providerInfo.put("upazilaid", loginResponse.get("upazilaid"));
					providerInfo.put("unionid", loginResponse.get("unionid"));
					providerInfo.put("FacilityName", loginResponse.get("facilityName"));
					providerInfo.put("loginStatus", true);
					providerInfo.put("client", loginResponse.get("client"));
					providerInfo.put("server", loginResponse.get("server"));
					providerInfo.put("version", loginResponse.get("version"));
					providerInfo.put("base_url", loginResponse.get("base_url"));
					providerInfo.put("sync_url", loginResponse.get("sync_url"));
					providerInfo.put("dbdownloadstatus", loginResponse.get("db_download_status"));
					providerInfo.put("dbsyncrequest", loginResponse.get("db_sync_request"));
					providerInfo.put("changerequest", loginResponse.get("change_request"));
					providerInfo.put("community_active", loginResponse.get("community_active"));
					providerInfo.put("offlinedbchecksum", providerInfo.get("dbdownloadstatus").equals("1")?
							(FileOperation.getChecksum( System.getProperty("catalina.base") + "/webapps/facility_offline_db/RHIS_" + providerInfo.getString("zillaid") + "_" 
										+ providerInfo.getString("upazilaid") + ".zip")):"");
					providerInfo.put("id", loginResponse.get("id"));
					providerInfo.put("assigned_facilities", loginResponse.getJSONObject("associated_id_details"));
					if(loginCred.getString("version").equals(loginResponse.get("version"))){
						providerInfo.put("update", false);
					}
					else{
						providerInfo.put("update", true);
					}
					System.out.println("Login Servlet: " + providerInfo);
					//System.out.println("location: " + System.getProperty("user.dir"));
					out.print(providerInfo);
				}				
			}
			else{
				if(loginCred.getString("client").equals("1")){
					out.print("false");
				}
				else if(loginCred.getString("client").equals("2")){
					JSONObject providerInfo = new JSONObject();
					
					providerInfo.put("ProvName", "");
					providerInfo.put("ProvCode", "");
					providerInfo.put("FacilityName", "");
					providerInfo.put("loginStatus", false);
					System.out.println("Login Servlet: " + providerInfo);
					out.print(providerInfo);
				}
			}
			out.close();
		} 
		catch (ParseException e) {
			e.printStackTrace();
		}
		catch(IOException e){
			e.printStackTrace();			
		}
		catch(Exception e){
			e.printStackTrace();			
		}		
	}
}