package org.sci.rhis.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;
import org.sci.rhis.client.NonRegisteredInfo;
import org.sci.rhis.servercommunication.LiveSynchronization;

/**
 * @author sabah.mugab
 * @since August, 2015
 */
@SuppressWarnings("serial")
public class NonExistingClientInfoServlet extends HttpServlet {

	public JSONObject jsTempRegisteredClientInfo = new JSONObject();
	
	public void doPost(HttpServletRequest request, HttpServletResponse response){

		try{
			response.setContentType("application/json; charset=UTF-8");
			response.setCharacterEncoding("UTF-8");
			PrintWriter out = response.getWriter();
			JSONObject nonRegisteredInfo;
		
			nonRegisteredInfo = new JSONObject(request.getParameter("nonRegisteredClientGeneralInfo"));
			jsTempRegisteredClientInfo = NonRegisteredInfo.getDetailInfo(nonRegisteredInfo);
			System.out.println("Temp Client Servlet: " + jsTempRegisteredClientInfo);
			out.print(jsTempRegisteredClientInfo);
			out.flush();
			out.close();
			
			//trying to synchronize real time in application level
			//LiveSynchronization.synchronize("nonRegisteredClientGeneralInfo="+request.getParameter("nonRegisteredClientGeneralInfo"),request.getRequestURI().split("/")[(request.getRequestURI().split("/")).length-1]);
		}
		catch(ParseException e){
			e.printStackTrace();			
		}
		catch(IOException e){
			e.printStackTrace();			
		}
		catch(Exception e){
			e.printStackTrace();			
		}
	}
}
