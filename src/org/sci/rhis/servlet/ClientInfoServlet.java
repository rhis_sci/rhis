package org.sci.rhis.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.util.Iterator;
import java.util.Map;

import org.json.JSONObject;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.sci.rhis.allserviceinfo.GetAllServiceInfo;
import org.sci.rhis.anc_updated.ANCVisit;
import org.sci.rhis.client.ClientInfo;
import org.sci.rhis.db.PropertiesReader;
import org.sci.rhis.death.DeathInfo;
import org.sci.rhis.general_patient.GPVisit;
import org.sci.rhis.servercommunication.LiveSynchronization;

/**
 * @author sabah.mugab
 * @since July, 2015
 */

@SuppressWarnings("serial")
public class ClientInfoServlet extends HttpServlet {

    public JSONObject jsClientInfo = new JSONObject();

    public void doPost(HttpServletRequest request, HttpServletResponse response) {

        try {
            response.setContentType("application/json; charset=UTF-8");
            response.setCharacterEncoding("UTF-8");
            PrintWriter out = response.getWriter();
            JSONObject searchClient;

            searchClient = new JSONObject(request.getParameter("sClient"));
            jsClientInfo = ClientInfo.getDetailInfo(searchClient);
            System.out.println("Client Servlet: " + jsClientInfo);



            //todo: need to move this  code part in a new class
//			to get all service info into a jsclientinfo
            if (searchClient.has("requestToGetAllInfo")) {
                if (searchClient.get("requestToGetAllInfo").equals("true")) {
                    JSONObject jsonInfo;
                    jsClientInfo.put("specialRequest", true);

                    GetAllServiceInfo all = new GetAllServiceInfo();
                    searchClient.put("gpLoad", "retrieve");
                    searchClient.put("healthId", jsClientInfo.get("cHealthID"));
                    jsonInfo = all.callServiceMethod(org.sci.rhis.general_patient.GPVisit.class, searchClient);
                    jsClientInfo.put("GP", jsonInfo);


                    //todo: where chealthid not found then dont need to search for any service


//				gpInfo = GPVisit.getDetailInfo(searchClient);


                    //pillcondom
                    searchClient.put("pillCondomLoad", "retrieve");
                    jsonInfo = all.callServiceMethod(org.sci.rhis.pillcondom.PillCondomService.class, searchClient);
                    jsClientInfo.put("PILLCONDOM", jsonInfo);

                    //death

                    /**assign new jsonobject for death
                     * Reason: when we change "pregNo" of searchclient it also change resultjson of DEATH's "pregNo"
                     * As we send searchclient to retrieve death info ,during this process we assign
                     * searchclient into resultjson(response = resultJson)
                     * so for any changes of searchclient it also changes "DEATH" too
                     */
                    JSONObject requestJsonDeath = new JSONObject(searchClient.toString());
                    requestJsonDeath.put("deathLoad","retrieve");
                    jsonInfo = DeathInfo.reportDeath(requestJsonDeath);
                    jsClientInfo.put("DEATH", jsonInfo);

                    if (jsClientInfo.has("cPregNo")) {
                        searchClient.put("pregNo", jsClientInfo.get("cPregNo"));
                        searchClient.put("pregno", jsClientInfo.get("cPregNo"));

                        //anc
                        searchClient.put("ancLoad", "retrieve");
                        searchClient.put("healthid", jsClientInfo.get("cHealthID"));
                        jsonInfo = ANCVisit.getDetailInfo(searchClient);
                        jsClientInfo.put("ANC_S", jsonInfo);

                        //delivery
                        searchClient.put("deliveryLoad", "retrieve");
//                searchClient.put("isRetrival","");
                        jsonInfo = all.callServiceMethod(org.sci.rhis.delivery.DeliveryInfo.class, searchClient);
                        jsClientInfo.put("DELIVERY", jsonInfo);

                        //newborn
                        searchClient.put("newbornLoad", "retrieve");
                        jsonInfo = all.callServiceMethod(org.sci.rhis.newborn.NewbornInfo.class, searchClient);
                        jsClientInfo.put("NEWBORN", jsonInfo);

                        //pncmother
                        searchClient.put("pncMLoad", "retrieve");
                        jsonInfo = all.callServiceMethod(org.sci.rhis.pncmother.PNCVisitMother.class, searchClient);
                        jsClientInfo.put("PNCMOTHER", jsonInfo);

                        //pncchild
                        searchClient.put("pncCLoad", "retrieve");
                        jsonInfo = all.callServiceMethod(org.sci.rhis.pncchild.PNCVisitChild.class, searchClient);
                        jsClientInfo.put("PNCCHILD", jsonInfo);

                        //PAC
                        searchClient.put("pacLoad", "retrieve");
                        jsonInfo = all.callServiceMethod(org.sci.rhis.pac.PACVisit.class, searchClient);
                        jsClientInfo.put("PAC", jsonInfo);
                    }


                    //iud
                    searchClient.put("iudLoad", "retrieve");
//                searchClient.put("iudCount","");
                    jsonInfo = all.callServiceMethod(org.sci.rhis.iud.IUDInfo.class, searchClient);
                    jsClientInfo.put("IUD", jsonInfo);

                    //iudfollowup
//                searchClient.put("iudLoad", "");
                    searchClient.put("iudFollowupLoad", "retrieve");
                    searchClient.put("iudCount", "");
                    jsonInfo = all.callServiceMethod(org.sci.rhis.iud.IUDInfo.class, searchClient);
                    jsClientInfo.put("IUDFOLLOWUP", jsonInfo);

                    //implant
                    searchClient.put("implantLoad", "retrieve");
                    searchClient.put("implantCount", "");
                    jsonInfo = all.callServiceMethod(org.sci.rhis.implant.ImplantInfo.class, searchClient);
                    jsClientInfo.put("IMPLANT", jsonInfo);

                    //implant followup
                    searchClient.put("implantFollowupLoad", "retrieve");
                    jsonInfo = all.callServiceMethod(org.sci.rhis.implant.ImplantInfo.class, searchClient);
                    jsClientInfo.put("IMPLANTFOLLOWUP", jsonInfo);

                    //injectable
                    searchClient.put("injectableLoad", "retrieve");
                    jsonInfo = all.callServiceMethod(org.sci.rhis.womaninjectable.WomanInjectableService.class, searchClient);
                    jsClientInfo.put("WOMANINJECTABLE", jsonInfo);

                    //permanent method
                    searchClient.put("pmsLoad", "retrieve");
                    searchClient.put("gender", jsClientInfo.get("cSex"));
//                searchClient.put("pmsCount", "");
                    jsonInfo = all.callServiceMethod(org.sci.rhis.permanentmethod.PMInfo.class, searchClient);
                    jsClientInfo.put("PMS", jsonInfo);

                    //permanent method follow up
                    searchClient.put("pmsFollowupLoad", "retrieve");
//                searchClient.put("pmsLoad", "");
                    jsonInfo = all.callServiceMethod(org.sci.rhis.permanentmethod.PMInfo.class, searchClient);
                    jsClientInfo.put("PMSFOLLOWUP", jsonInfo);

                    //child
                    searchClient.put("childLoad", "retrieve");
                    searchClient.put("serviceId", ""); //’childCountValue’
                    jsonInfo = all.callServiceMethod(org.sci.rhis.child.ChildVisit.class, searchClient);
                    jsClientInfo.put("childInfo", jsonInfo);

                    //fpinfo
                    searchClient.put("fpInfoLoad", "retrieve");
                    searchClient.put("client", "2");
                    jsonInfo = all.callServiceMethod(org.sci.rhis.fp.FPInfo.class, searchClient);
                    jsClientInfo.put("IU_FPINFO", jsonInfo);




                }
            }


            out.print(jsClientInfo);
            out.flush();
            out.close();

            //trying to synchronize real time in application level
            //LiveSynchronization.synchronize("sClient="+request.getParameter("sClient"),request.getRequestURI().split("/")[(request.getRequestURI().split("/")).length-1]);
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}