package org.sci.rhis.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;
import org.sci.rhis.womaninjectable.WomanInjectableService;
import org.sci.rhis.servercommunication.LiveSynchronization;

/**
 * @author sabah.mugab
 * @since February, 2016
 */
@SuppressWarnings("serial")
public class WomanInjectableServiceServlet extends HttpServlet {

	public JSONObject jsInjectableInfo = new JSONObject();
	
	public void doPost(HttpServletRequest request, HttpServletResponse response){

		try{
		response.setContentType("application/json; charset=UTF-8");
		response.setCharacterEncoding("UTF-8");
		PrintWriter out = response.getWriter();
		JSONObject injectableInfo;
		
			injectableInfo = new JSONObject(request.getParameter("injectableInfo"));
			jsInjectableInfo = WomanInjectableService.getDetailInfo(injectableInfo);
			System.out.println("Woman Injectable Servlet: " + jsInjectableInfo);
			out.print(jsInjectableInfo);
			out.flush();
			out.close();
			
			//trying to synchronize real time in application level
			//LiveSynchronization.synchronize("injectableInfo="+request.getParameter("injectableInfo"),request.getRequestURI().split("/")[(request.getRequestURI().split("/")).length-1]);
		}
		catch(ParseException e){
			e.printStackTrace();			
		}
		catch(IOException e){
			e.printStackTrace();			
		}
		catch(Exception e){
			e.printStackTrace();			
		}
	}
}