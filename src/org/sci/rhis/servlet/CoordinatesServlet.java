package org.sci.rhis.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;
import org.sci.rhis.coordinate.VisitCoordinate;

/**
 * @author sabah.mugab
 * @since January, 2018
 */
@SuppressWarnings("serial")
public class CoordinatesServlet extends HttpServlet {

	public JSONObject jsCoordinatesInfo = new JSONObject();
	
	public void doPost(HttpServletRequest request, HttpServletResponse response){

		try{
			response.setContentType("application/json; charset=UTF-8");
			response.setCharacterEncoding("UTF-8");
			response.setHeader("Access-Control-Allow-Origin", "*");
			PrintWriter out = response.getWriter();
			JSONObject requestInfo;
		
			requestInfo = new JSONObject(request.getParameter("info"));			
			System.out.println("Coordinates Servlet request: " + requestInfo);
			jsCoordinatesInfo = VisitCoordinate.getDetailInfo(requestInfo);
			System.out.println("Coordinates Servlet response: " + jsCoordinatesInfo);
			out.print(jsCoordinatesInfo);
			out.flush();
			out.close();			
		}
		catch(ParseException e){
			e.printStackTrace();			
		}
		catch(IOException e){
			e.printStackTrace();			
		}
		catch(Exception e){
			e.printStackTrace();			
		}
	}
}