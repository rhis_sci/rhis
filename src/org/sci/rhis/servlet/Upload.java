package org.sci.rhis.servlet;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import org.json.JSONObject;
import org.sci.rhis.uploaddownload.UploadDownloadStatusHandler;
import org.sci.rhis.util.FileOperation;

/**
 * @author sabah.mugab
 * @since March, 2018
 * 
 * THIS SERVLET IS OBSOLETE!!!
 */
@SuppressWarnings("serial")
@MultipartConfig
public class Upload extends HttpServlet {
	
	public void doPost(HttpServletRequest request, HttpServletResponse response){
		
		try {
			JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("uploadstatus","in-complete");
			Part filePart = request.getPart("file");
		    String fileName = getFileName(filePart);
		    jsonResponse.put("providerid",fileName);
		    InputStream fileContent = filePart.getInputStream();
		    String uploadedFile = System.getProperty("catalina.base") + "/webapps/unsync_data/" + fileName;
		    
		    FileOperation.writeToFile(uploadedFile, FileOperation.getContentFromInputStream(fileContent));
		    System.out.println("FileName: " + uploadedFile + "\nChecksum: " + FileOperation.getChecksum(uploadedFile));
		    System.out.println("FileName: " + uploadedFile + "_checksum\nChecksum: " + FileOperation.readingFromFile(uploadedFile + "_checksum"));
		    if(new File(uploadedFile).isFile()){
		    	if(FileOperation.getChecksum(uploadedFile).equals(
		    			FileOperation.readingFromFile(uploadedFile + "_checksum"))){
		    		System.out.println("\n\n\n\n**************UPLOAD JSON: " + jsonResponse + "\n\n\n\n\n\n\n\n\n");
		    		UploadDownloadStatusHandler.changeStatus(jsonResponse);
		    		jsonResponse.put("uploadstatus","complete");		    		
		    	}
		    	else{
		    		jsonResponse.put("uploadstatus","in-complete");
		    	}
		    }
		    
		    PrintWriter out = response.getWriter();
		    out.print(jsonResponse);
			out.flush();
			out.close();				
		}
		catch(ServletException se){
			se.printStackTrace();			
		}
		catch(IOException e){
			e.printStackTrace();			
		}
		catch(Exception e){
			e.printStackTrace();			
		}
	}

	private String getFileName(final Part part) {
		if(part!=null){
	        for (String content : part.getHeader("Content-Disposition").split(";")) {
	            if (content.trim().startsWith("filename")) {
	                return content.substring(
	                    content.indexOf('=') + 1).trim().replace("\"", "");
	            }
	        }
		}
        return null;
    }
}