package org.sci.rhis.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;
import org.sci.rhis.child.ChildVisit;
//import org.sci.rhis.servercommunication.LiveSynchronization;

/**
 * @author sabah.mugab
 * @since March, 2018
 */

@SuppressWarnings("serial")
public class ChildServiceServlet extends HttpServlet {

	public JSONObject jsChildServiceInfo = new JSONObject();
	
	public void doPost(HttpServletRequest request, HttpServletResponse response){

		try{
		response.setContentType("application/json; charset=UTF-8");
		response.setCharacterEncoding("UTF-8");
		PrintWriter out = response.getWriter();
		JSONObject ChildServiceInfo;
		
			ChildServiceInfo = new JSONObject(request.getParameter("childInfo"));
			jsChildServiceInfo = ChildVisit.getDetailInfo(ChildServiceInfo);
			System.out.println("Child Service Servlet: " + jsChildServiceInfo);
			out.print(jsChildServiceInfo);
			out.flush();
			out.close();
			
			//trying to synchronize real time in application level
			//LiveSynchronization.synchronize("ChildServiceInfo="+request.getParameter("childserviceinfo"),request.getRequestURI().split("/")[(request.getRequestURI().split("/")).length-1]);
		}
		catch(ParseException e){
			e.printStackTrace();			
		}
		catch(IOException e){
			e.printStackTrace();			
		}
		catch(Exception e){
			e.printStackTrace();			
		}
	}
}