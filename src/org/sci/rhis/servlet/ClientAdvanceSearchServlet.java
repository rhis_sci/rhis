package org.sci.rhis.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;

import org.json.JSONObject;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.sci.rhis.client.ClientAdvanceSearch;
import org.sci.rhis.servercommunication.LiveSynchronization;

/**
 * @author sabah.mugab
 * @since September, 2015
 */

@SuppressWarnings("serial")
public class ClientAdvanceSearchServlet extends HttpServlet {

	public JSONObject jsClientSearch = new JSONObject();
	
	public void doPost(HttpServletRequest request, HttpServletResponse response){

		try{
		response.setContentType("application/json; charset=UTF-8");
		response.setCharacterEncoding("UTF-8");
		PrintWriter out = response.getWriter();
		JSONObject searchClient;
		
			searchClient = new JSONObject(request.getParameter("advanceSearch"));
			jsClientSearch = ClientAdvanceSearch.getSearchResult(searchClient);
			System.out.println("Advance Search Servlet: " + jsClientSearch);
			out.print(jsClientSearch);
			out.flush();
			out.close();
			
			//trying to synchronize real time in application level
			//LiveSynchronization.synchronize("advanceSearch="+request.getParameter("advanceSearch"),request.getRequestURI().split("/")[(request.getRequestURI().split("/")).length-1]);
		}
		catch(ParseException e){
			e.printStackTrace();			
		}
		catch(IOException e){
			e.printStackTrace();			
		}
		catch(Exception e){
			e.printStackTrace();			
		}
	}
}
