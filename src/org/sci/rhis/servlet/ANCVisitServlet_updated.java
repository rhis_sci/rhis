package org.sci.rhis.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;
import org.sci.rhis.anc_updated.ANCVisit;
import org.sci.rhis.servercommunication.LiveSynchronization;

/**
 * @author sabah.mugab
 * @since July, 2015
 */

@SuppressWarnings("serial")
public class ANCVisitServlet_updated extends HttpServlet {

	public JSONObject jsANCInfo = new JSONObject();
	
	public void doPost(HttpServletRequest request, HttpServletResponse response){

		try{
			response.setContentType("application/json; charset=UTF-8");
			response.setCharacterEncoding("UTF-8");
			PrintWriter out = response.getWriter();
			JSONObject ANCInfo;
		
			ANCInfo = new JSONObject(request.getParameter("ANCInfo"));
			jsANCInfo = ANCVisit.getDetailInfo(ANCInfo);
			System.out.println("ANC Servlet: " + jsANCInfo);
			out.print(jsANCInfo);
			out.flush();
			out.close();
			
			//trying to synchronize real time in application level
			//LiveSynchronization.synchronize("ANCInfo="+request.getParameter("ANCInfo"),request.getRequestURI().split("/")[(request.getRequestURI().split("/")).length-1]);
		}
		catch(ParseException e){
			e.printStackTrace();			
		}
		catch(IOException e){
			e.printStackTrace();			
		}
		catch(Exception e){
			e.printStackTrace();			
		}
	}
}
