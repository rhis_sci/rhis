package org.sci.rhis.servlet;

import java.io.*;
import java.text.ParseException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;
import org.sci.rhis.delivery.DeliveryInfo;
import org.sci.rhis.servercommunication.LiveSynchronization;

/**
 * @author sabah.mugab
 * @since July, 2015
 */
@SuppressWarnings("serial")
public class DeliveryInfoServlet extends HttpServlet {

	public JSONObject jsDeliveryInfo = new JSONObject();
	
	public void doPost(HttpServletRequest request, HttpServletResponse response){

		try{
		response.setContentType("application/json; charset=UTF-8");
		response.setCharacterEncoding("UTF-8");
		PrintWriter out = response.getWriter();
		JSONObject deliveryInfo;

			deliveryInfo = new JSONObject(request.getParameter("deliveryInfo"));
			jsDeliveryInfo = DeliveryInfo.getDetailInfo(deliveryInfo);
			System.out.println("Delivery Servlet: " + jsDeliveryInfo);
			out.print(jsDeliveryInfo);
			out.flush();
			out.close();
			
			//trying to synchronize real time in application level
			//LiveSynchronization.synchronize("deliveryInfo="+request.getParameter("deliveryInfo"),request.getRequestURI().split("/")[(request.getRequestURI().split("/")).length-1]);
		}
		catch(ParseException e){
			e.printStackTrace();			
		}
		catch(IOException e){
			e.printStackTrace();			
		}
		catch(Exception e){
			e.printStackTrace();			
		}
	}
}
