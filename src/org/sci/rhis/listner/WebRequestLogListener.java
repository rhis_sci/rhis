package org.sci.rhis.listner;

import org.json.JSONObject;

import javax.servlet.ServletRequest;
import javax.servlet.ServletRequestEvent;
import javax.servlet.ServletRequestListener;
import javax.servlet.annotation.WebListener;
import java.io.IOException;
import java.text.ParseException;
import java.time.LocalDateTime;
import java.util.Map;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Logger;

//@WebListener
public class WebRequestLogListener implements ServletRequestListener {

    private static Handler fileHandler;
    private static Logger logger;

    static {
        try {
            //clientrequest_logger
            String filepath =  System.getProperty("catalina.home")+"/webapps/clientrequest_logger/requestlogger.log";//"../clientrequest_logger/requestlogger.log";
//            String filepath = "C://Users/arjuda.anjum/IdeaProjects/anjum_newDataSync/out/logger.log";
            fileHandler = new FileHandler(filepath, true);
            //added logger class to log output
            logger = Logger.getLogger(WebRequestLogListener.class.getName());
            fileHandler.setFormatter(new CustomRecordFormatter());
            logger.addHandler(fileHandler);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void requestDestroyed(ServletRequestEvent servletRequestEvent) {
//        ServletRequest servletRequest = servletRequestEvent.getServletRequest();
//        System.out.println("ServletRequest destroyed. Remote IP="+servletRequest.getRemoteAddr());
    }

    public void requestInitialized(ServletRequestEvent servletRequestEvent) {
        ServletRequest servletRequest = servletRequestEvent.getServletRequest();
        String msg = "";
//        DataLogger.logData("------ Start Log ------", logger);

        Map<String, String[]> params = servletRequest.getParameterMap();

        for (Map.Entry<String, String[]> entry : params.entrySet()) {

            try {
                JSONObject json = new JSONObject(entry.getValue()[0]);
                if (json.has("client")) {
                    if (json.getString("client").equals("3")) {
                        msg = "Request: " + servletRequest.getServletContext().getContextPath() + " Time: " + LocalDateTime.now() + " IP=" + servletRequest.getRemoteAddr() + " Data=" + json;
                        DataLogger.logData(msg, logger);
                    }
                }

            } catch (ParseException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}