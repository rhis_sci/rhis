package org.sci.rhis.listner;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class DataLogger {
    public static String inputStreamToString(InputStream inputStream, boolean reset) {
        String res = new BufferedReader(
                new InputStreamReader(inputStream, StandardCharsets.UTF_8))
                .lines()
                .collect(Collectors.joining("\n"));

        if (reset && inputStream.markSupported()) {
            try {
                inputStream.reset();
            } catch (IOException ignored) {
            }
        }

        return res;
    }

    public static void logData(String msg,Logger logger){
        /**
         * removed this from here because it prints line multiple times
         */
//        Logger logger = Logger.getLogger(DataLogger.class.getName());
//        fileHandler.setFormatter(new CustomRecordFormatter());
//        logger.addHandler(fileHandler);

        //logging messages
        logger.log(Level.INFO, msg);
    }
}
