package org.sci.rhis.fp.common;

import java.sql.ResultSet;

import org.json.JSONObject;
import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.db.DBOperation;
import org.sci.rhis.db.QueryBuilder;
import org.sci.rhis.distribution.StockDistributionRequest;
import org.sci.rhis.util.JsonHandler;

/**
 * @author sabah.mugab
 * @since January, 2016
 */
public class FPOp {
	
	public static boolean deleteFPExamination(JSONObject FPInfo, DBOperation dbOp, 
					DBInfoHandler dbObject, QueryBuilder dynamicQueryBuilder, String columnName) {
		
		try{
			String returningSql = " RETURNING *";
			ResultSet rs = dbOp.dbExecute(dbObject, new QueryBuilder().getDeleteQuery
					(new JsonHandler().addJsonKeyValueEdit(new JsonHandler().addJsonKeyMaxField
							(FPInfo,""), "FPEXAMINATIONCOMMON")) + returningSql).getResultSet();
			StockDistributionRequest.deleteDistributionInfoHandler(rs, columnName,
														FPInfo, dbOp, dbObject, dynamicQueryBuilder);

			if(!rs.isClosed()){
				rs.close();
			}
			return true;
		}
		catch(Exception e){
			e.printStackTrace();
			return false;
		}
	}
}