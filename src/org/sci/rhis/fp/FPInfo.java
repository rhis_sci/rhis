package org.sci.rhis.fp;

import org.json.JSONObject;
import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.db.DBOperation;
import org.sci.rhis.db.QueryBuilder;
import org.sci.rhis.db.UpzDBSelector;
import org.sci.rhis.util.CheckSystemEntryDate;

/**
 * @author sabah.mugab
 * @since September, 2016
 */
public class FPInfo {

    public static JSONObject getDetailInfo(JSONObject fpInfo) {

        DBOperation dbOp = new DBOperation();
        DBInfoHandler dbObject = new UpzDBSelector().upzDBInfo(fpInfo);
        QueryBuilder dynamicQueryBuilder = new QueryBuilder();

        try {
            JSONObject fpInformation = new JSONObject();

            if (fpInfo.get("fpInfoLoad").equals("insert")) {
				fpInformation = InsertUpdateFPInfo.insertUpdateFPInfo(fpInfo, fpInformation,
												dbOp, dbObject, dynamicQueryBuilder);

//                //check if systementrydate exist in requestjson and DB to remove duplicate entry
//                if (fpInfo.has("systemEntryDate") && !CheckSystemEntryDate.systemEntryDateExist(fpInfo, dbOp, dbObject, "IU_FPINFO", "IU_FPINFO_systemEntryDate", "IU_FPINFO_healthId")) {
//                    fpInformation = InsertUpdateFPInfo.insertUpdateFPInfo(fpInfo, fpInformation, dbOp, dbObject, dynamicQueryBuilder);
//                } else if (!fpInfo.has("systemEntryDate")) {
//                    fpInformation = InsertUpdateFPInfo.insertUpdateFPInfo(fpInfo, fpInformation, dbOp, dbObject, dynamicQueryBuilder);
//                }
            }

            fpInformation = RetrieveFPInfo.getFPInfo(fpInfo, fpInformation, dbOp, dbObject, dynamicQueryBuilder);
            return fpInformation;
        } catch (Exception e) {
            e.printStackTrace();
            return new JSONObject();
        } finally {
            dbOp.dbObjectNullify(dbObject);
        }
    }
}