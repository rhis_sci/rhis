package org.sci.rhis.fp;

import java.sql.ResultSet;

import org.json.JSONObject;
import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.db.DBOperation;
import org.sci.rhis.db.QueryBuilder;

/**
 * @author sabah.mugab
 * @since September, 2016
 */
public class InsertUpdateFPInfo {
	
	public static JSONObject insertUpdateFPInfo(JSONObject fpInfo, JSONObject fpInformation, 
						DBOperation dbOp, DBInfoHandler dbObject, QueryBuilder dynamicQueryBuilder) {
					
		try{
			ResultSet rs = dbOp.dbExecute(dbObject,
								dynamicQueryBuilder.getUpsertQuery(fpInfo, "IU_FPINFO")).getResultSet();
			
			if(rs.next()){
				fpInformation.put("fpInfoSuccess","1");
	        	fpInformation.put("healthId",rs.getString(dynamicQueryBuilder.getColumn("IU_FPINFO_healthId")));        	
			}
	        else{
	        	fpInformation.put("fpInfoSuccess","2");
	        	fpInformation.put("healthId","");
	        }		
			if(!rs.isClosed()){
				rs.close();
			}
			
			dbOp.dbStatementExecute(dbObject,dynamicQueryBuilder.getUpsertQuery(fpInfo, "IU_FPINFO_ELCO"));
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return fpInformation;
	}
	
	public static void insertUpdateCurrentFP(JSONObject fpInfo, JSONObject fpInformation, 
							DBOperation dbOp, DBInfoHandler dbObject, QueryBuilder dynamicQueryBuilder) {
		
		try{
			dbOp.dbStatementExecute(dbObject,dynamicQueryBuilder.getUpsertQuery(fpInfo, "IUC_FP"));
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
}