package org.sci.rhis.anc_updated;

import java.sql.ResultSet;

import org.json.JSONObject;
import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.db.DBOperation;
import org.sci.rhis.db.QueryBuilder;
import org.sci.rhis.distribution.StockDistributionRequest;
import org.sci.rhis.util.JsonHandler;

/**
 * @author sabah.mugab
 * @since June, 2015
 */
public class InsertANCVisitInfo {
	
	public static boolean createANCVisit(DBOperation dbOp, DBInfoHandler dbObject, JSONObject ANCInfo, QueryBuilder dynamicQueryBuilder) {
		
		try{
			ResultSet rs = dbOp.dbExecute(dbObject,dynamicQueryBuilder.getInsertQuery
					(new JsonHandler().addJsonKeyValueEdit(new JsonHandler().addJsonKeyIncrementalField
							(ANCInfo,"serviceId"), "ANC"))).getResultSet();
			
			StockDistributionRequest.insertDistributionInfoHandler(rs.next(), ANCInfo, dbOp, dbObject, dynamicQueryBuilder);
		
			if(!rs.isClosed()){
				rs.close();
			}
			return true;
		}
		catch(Exception e){
			e.printStackTrace();
			return false;
		}				
	}
}