package org.sci.rhis.anc_updated;

import java.sql.ResultSet;

import org.json.JSONObject;
import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.db.DBOperation;
import org.sci.rhis.db.QueryBuilder;
import org.sci.rhis.distribution.StockDistributionRequest;
import org.sci.rhis.util.JsonHandler;

/**
 * @author sabah.mugab
 * @since February, 2017
 */
public class UpdateANCVisitInfo {
	
	public static boolean updateANCVisit(DBOperation dbOp, DBInfoHandler dbObject, JSONObject ANCInfo, QueryBuilder dynamicQueryBuilder) {
		
		try{
			String returningSql = " RETURNING " + dynamicQueryBuilder.getColumn("", "ANC_anctreatment");
			ResultSet rs = dbOp.dbExecute(dbObject,(dynamicQueryBuilder.getUpdateQuery(
									new JsonHandler().addJsonKeyValueEdit(ANCInfo, "ANC")) + returningSql)).getResultSet();
			
			StockDistributionRequest.updateDistributionInfoHandler(rs, dynamicQueryBuilder.getColumn("ANC_anctreatment"), 
															ANCInfo, dbOp, dbObject, dynamicQueryBuilder);
			
			if(!rs.isClosed()){
				rs.close();
			}
			return true;
		}
		catch(Exception e){
			e.printStackTrace();
			return false;
		}		
	}
}