package org.sci.rhis.anc_updated;

import org.json.JSONObject;
import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.db.DBOperation;
import org.sci.rhis.db.JSONKeyMapper;
import org.sci.rhis.db.QueryBuilder;
import org.sci.rhis.db.UpzDBSelector;
import org.sci.rhis.general_patient.InsertGPVisitInfo;
import org.sci.rhis.util.CheckSystemEntryDate;
import org.sci.rhis.util.JsonHandler;

/**
 * @author sabah.mugab
 * @since June, 2015
 */
public class ANCVisit {
	
	final static int ANCSERVICETYPE = 1; 
	
	public static JSONObject getDetailInfo(JSONObject ANCInfo) {
		
		ANCInfo = new JsonHandler().addJsonKeyValueStockDistribution(new JSONKeyMapper().setRequiredKeys(ANCInfo, "ANC"), ANCSERVICETYPE);
		DBOperation dbOp = new DBOperation();
		DBInfoHandler dbObject = new UpzDBSelector().upzDBInfo(ANCInfo);
		QueryBuilder dynamicQueryBuilder = new QueryBuilder();
		
		try{
			if(ANCInfo.get("ancLoad").equals("")){
				//check if systementrydate exist in requestjson and DB to remove duplicate entry
				if (ANCInfo.has("systemEntryDate") && !CheckSystemEntryDate.systemEntryDateExist(ANCInfo, dbOp, dbObject, "ANC","ANC_systemEntryDate","ANC_healthid")) {
					InsertANCVisitInfo.createANCVisit(dbOp, dbObject, ANCInfo, dynamicQueryBuilder);
				}else if(!ANCInfo.has("systemEntryDate")){
					InsertANCVisitInfo.createANCVisit(dbOp, dbObject, ANCInfo, dynamicQueryBuilder);
				}

			}
			else if(ANCInfo.get("ancLoad").equals("update")){
				UpdateANCVisitInfo.updateANCVisit(dbOp, dbObject, ANCInfo, dynamicQueryBuilder);
			}
			else if(ANCInfo.get("ancLoad").equals("delete")){
				DeleteANCVisitInfo.deleteANCVisit(dbOp, dbObject, ANCInfo, dynamicQueryBuilder);
			}
			JSONObject ANCVisits = new JSONObject();
			RetrieveANCVisitInfo.getANCVisits(dbOp, dbObject, ANCInfo, ANCVisits, dynamicQueryBuilder);
			return ANCVisits;
		}
		catch(Exception e){
			e.printStackTrace();
			return new JSONObject();
		}
		finally{
			dbOp.dbObjectNullify(dbObject);			
		}		
	}
}