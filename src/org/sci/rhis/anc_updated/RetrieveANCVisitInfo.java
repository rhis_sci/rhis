package org.sci.rhis.anc_updated;

import java.sql.ResultSet;

import org.json.JSONObject;
import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.db.DBOperation;
import org.sci.rhis.db.QueryBuilder;
import org.sci.rhis.util.CalendarDate;
import org.sci.rhis.util.JsonHandler;

/**
 * @author sabah.mugab
 * @since June, 2015
 */
public class RetrieveANCVisitInfo {

	public static JSONObject getANCVisits(DBOperation dbOp, DBInfoHandler dbObject, JSONObject ANCInfo, JSONObject ANCVisits, QueryBuilder dynamicQueryBuilder) {
		
		try{
			String sql = "SELECT * FROM " + dynamicQueryBuilder.getTable("ANC") + " "
						+ "WHERE " + dynamicQueryBuilder.getColumn("table", "ANC_healthid",new String[]{ANCInfo.getString("healthid")},"=")
						+ " AND " + dynamicQueryBuilder.getColumn("table", "ANC_pregNo",new String[]{ANCInfo.getString("pregNo")},"=") 
						+ " ORDER BY " + dynamicQueryBuilder.getColumn("table", "ANC_serviceId") + " ASC"; 
			
			ResultSet rs = dbOp.dbExecute(dbObject,sql).getResultSet();
			ANCVisits.put("count", 0);
			ANCInfo.put("distributionJson","anctreatment");
						
			while(rs.next()){
				
				ANCVisits.put(rs.getString(dynamicQueryBuilder.getColumn("ANC_serviceId")), new JsonHandler().getServiceDetail(rs, 
																							ANCInfo, "ANC", dynamicQueryBuilder, 2));
				ANCVisits.put("count", (ANCVisits.getInt("count")+1));
				ANCVisits.put("ancStatus", false);
			}
			
			/*
			 * If client already moved to next service that means has delivery information
			 * or current date is more than 42 weeks from LMP date 
			 * then provider should not be able to insert anymore anc information 
			 */
			sql = "SELECT de.cnt, " + dynamicQueryBuilder.getColumn("table", "PREGWOMEN_lmp") + " "
				+ "FROM " + dynamicQueryBuilder.getTable("PREGWOMEN") + ", "
				+ "(SELECT COUNT(*) AS cnt "
				+ "FROM " + dynamicQueryBuilder.getTable("DELIVERY") + " "
				+ "WHERE " + dynamicQueryBuilder.getColumn("table", "DELIVERY_healthid",new String[]{ANCInfo.getString("healthid")},"=")
				+ " AND " + dynamicQueryBuilder.getColumn("table", "DELIVERY_pregno",new String[]{ANCInfo.getString("pregNo")},"=") + ") AS de "
				+ "WHERE " + dynamicQueryBuilder.getColumn("table", "PREGWOMEN_healthId",new String[]{ANCInfo.getString("healthid")},"=")
				+ " AND " + dynamicQueryBuilder.getColumn("table", "PREGWOMEN_pregNo",new String[]{ANCInfo.getString("pregNo")},"=");
								
			rs = dbOp.dbExecute(dbObject,sql).getResultSet();
			
			if(rs.next()){
				int days = CalendarDate.getDaysCount(rs.getDate(2));
				
				if(rs.getInt(1)==1){
					ANCVisits.put("ancStatus", true);
				}
				else if(rs.getInt(1)==0){
					if(days > (42*7)){
						ANCVisits.put("ancStatus", true);
					}				
				}				
			}
			
			if(!rs.isClosed()){
				rs.close();
			}
			return ANCVisits;
		}
		catch(Exception e){
			e.printStackTrace();
			return new JSONObject();
		}		
	}
}