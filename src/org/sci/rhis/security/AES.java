package org.sci.rhis.security;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import javax.xml.bind.DatatypeConverter;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

/**
 * @author sabah.mugab
 * @since July, 2017
 */
public class AES {
 
    private static SecretKeySpec secretKey;
     
    public static void setKey(String myKey) 
    {
        try{
            secretKey = new SecretKeySpec(Arrays.copyOf(MessageDigest.getInstance("SHA-1").digest(myKey.getBytes("UTF-8")), 16), "AES");
        } 
        catch (NoSuchAlgorithmException e){
            e.printStackTrace();
        } 
        catch (UnsupportedEncodingException e){
            e.printStackTrace();
        }
    }
 
    public static String encrypt(String strToEncrypt, String secret) 
    {
        try{
            return DatatypeConverter.printBase64Binary(getCipherInstance("encrypt",secret).doFinal(strToEncrypt.getBytes("UTF-8")));
        } 
        catch (Exception e){
            System.out.println("Error while encrypting: " + e.toString());
            e.printStackTrace();
        }
        return null;
    }
 
    public static String decrypt(String strToDecrypt, String secret) 
    {
        try{
            return new String(getCipherInstance("decrypt",secret).doFinal(DatatypeConverter.parseBase64Binary(strToDecrypt)));
        } 
        catch (Exception e){
            System.out.println("Error while decrypting: " + e.toString());
            e.printStackTrace();
        }
        return null;
    }
    
    private static Cipher getCipherInstance(String mode, String secret){
    	try{
            setKey(secret);
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5PADDING");
            if(mode.equals("encrypt")){
            	cipher.init(Cipher.ENCRYPT_MODE, secretKey);
            }
            else{
            	cipher.init(Cipher.DECRYPT_MODE, secretKey);
            }
            return cipher;
        } 
        catch (Exception e){
            e.printStackTrace();
        }        
        return null;
    }
}