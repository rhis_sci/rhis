package org.sci.rhis.util;

import java.util.Properties;
import java.util.Enumeration;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

/**
 * @author sabah.mugab
 * @since November, 2015
 */
public class ConfInfoRetrieve {

	public static Properties readingConf(String location){
				
		Properties properties = new Properties();
		try{
			properties.load(Thread.currentThread().getContextClassLoader().getResourceAsStream(location));
		}
		catch(IOException e){
			e.printStackTrace();
		}
	    return properties;
	}
	
	public static Properties readingConf(String location, int fileSystem){
		
		Properties properties = new Properties();
		try{
			if(fileSystem == 1) {System.out.println(location);
				properties.load(new FileInputStream(new File(location)));
			}
			else {
				properties.load(Thread.currentThread().getContextClassLoader().getResourceAsStream(location));
			}
			System.out.println(properties.getProperty("FACILITY"));
		    return properties;
		}
		catch(Exception e){System.out.println("--------------------------------I am trying to read the config file---------------------");
			e.printStackTrace();
			return new Properties();
		}		
	}
	
	public static Enumeration<?> getPropertiesNameList(String propertiesFileName){
		return ConfInfoRetrieve.readingConf(propertiesFileName).propertyNames();
	}
}