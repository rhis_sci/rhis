package org.sci.rhis.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * @author sabah.mugab
 * @since September, 2015
 */
public class CalendarDate {
		
	public static String getCurrentDate(String format){
		
		Calendar calendar = Calendar.getInstance();
        SimpleDateFormat date1 = new SimpleDateFormat(format);
        String date = date1.format(calendar.getTime());
        
		return date;
	}
	
	public static String getCurrentDate(){
		
		return getCurrentDate("yyyy-MM-dd");
	}
	
	public static String getCurrentDay(){
		
		return getCurrentDate("dd");
	}

	public static String getCurrentMonth(){
		
		return getCurrentDate("MM");
	}

	public static String getCurrentYear(){
		
		return getCurrentDate("yyyy");
	}
	
	public static String getCurrentDateTime(){
		return getCurrentDate("yyyy-MM-dd HH:mm:ss");
	}

	public static String getCurrentDateTimeWithMill(){
		return getCurrentDate("yyyy-MM-dd HH:mm:ss.SSS");
	}
	
	public static Long getCurrentTimeInMill(){
		
		Calendar calendar = Calendar.getInstance();
		long timeInMill = calendar.getTimeInMillis();
        
		return timeInMill;
	}

	public static int getDaysCount(Date earlyDate){
		int days = 0;
		
		Calendar calendar = Calendar.getInstance();
		long milliSecondLatestDate = calendar.getTimeInMillis();

		calendar.setTime(earlyDate);
		long milliSecondEarlyDate= calendar.getTimeInMillis();
			
		long milliSecondDiff = milliSecondLatestDate - milliSecondEarlyDate;

		days = (int) Math.floor((((milliSecondDiff/1000)/60)/60)/24);
       
		return days;
	}
	
	public static int getDaysCount(Date earlyDate, Date latestDate){
		int days = 0;
		
		Calendar calendar = Calendar.getInstance();
		
		calendar.setTime(earlyDate);
		long milliSecondEarlyDate= calendar.getTimeInMillis();
		
		calendar.setTime(latestDate);
		long milliSecondLatestDate= calendar.getTimeInMillis();
		
		long milliSecondDiff = milliSecondLatestDate - milliSecondEarlyDate;

		days = (int) Math.floor((((milliSecondDiff/1000)/60)/60)/24);
       
		return days;
	}	
}