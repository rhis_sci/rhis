package org.sci.rhis.util;

import org.json.JSONObject;
import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.db.DBOperation;
import org.sci.rhis.db.DBTableInfoHandler;
import org.sci.rhis.db.QueryBuilder;
import org.sci.rhis.db.UpzDBSelector;
import org.sci.rhis.distribution.HandleStockDistribution;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/**
 * @author sabah.mugab
 * @since February, 2017
 */
public class JsonHandler {

	public JSONObject addJsonKeyValueEdit(JSONObject jsonObj, String service){
		jsonObj.put("serviceName", service);
        /**
         * check whether systementrydate exist in requestjson
         * if not exist then we will add json key for systementrydate
         * NB: need to review later
         */
		if(!(jsonObj.has("systemEntryDate"))) {
            jsonObj.put("systemEntryDate", CalendarDate.getCurrentDateTime());
        }
		jsonObj.put("modifyDate", CalendarDate.getCurrentDateTime());
		
		return jsonObj;
	}
	
	public JSONObject addJsonKeyValueEdit(JSONObject jsonObj, String service, boolean systemEntryDateModifiable){
		jsonObj.put("serviceName", service);
		if(systemEntryDateModifiable){
			jsonObj.put("systemEntryDate", CalendarDate.getCurrentDateTime());
		}
		jsonObj.put("modifyDate", CalendarDate.getCurrentDateTime());

		return jsonObj;
	}
	
	public JSONObject addJsonKeyIncrementalField(JSONObject jsonObj, String keyName){
		if(!jsonObj.has(keyName)){
			jsonObj.put(keyName,"");
		}
		jsonObj.put("incrementalField", keyName);
				
		return jsonObj;
	}
	
	public JSONObject addJsonKeyMaxField(JSONObject jsonObj, String keyName){
		if(!jsonObj.has(keyName)){
			jsonObj.put(keyName,"");
		}
		jsonObj.put("maxField", keyName);
				
		return jsonObj;
	}
	
	public JSONObject addJsonKeyValueStockDistribution(JSONObject jsonObj, int serviceType){
		
		jsonObj.put("serviceType", serviceType);
		jsonObj.put("treatment_key", "distributionId");
		if(jsonObj.has("treatment")){
			if(jsonObj.getString("treatment").contains("_")){
				jsonObj.put("distributionId", CalendarDate.getCurrentTimeInMill());
			}	
			else{
				jsonObj.put("distributionId",jsonObj.getString("treatment"));
			}
		}
		else{
			jsonObj.put("distributionId","");
		}
		
		return jsonObj;
	}
	
	public String getStringValue(String stringValue, String comparisonStringValue){
		
		try {
			return (stringValue.equals(comparisonStringValue) ? "" : stringValue);
		} 
		catch (Exception e) {
			e.printStackTrace();
			return "";
		}		
	}
	
	public String getResultSetValue(ResultSet rs, String columnName){
		
		try {
			return (rs.getObject(columnName)==null?"":rs.getString(columnName));
		} 
		catch (Exception e) {
			e.printStackTrace();
			return "";
		}		
	}
	
	public String getResultSetValue(ResultSet rs, String columnName, String defaultValue){
		
		try {
			return (rs.getObject(columnName)==null ? defaultValue : rs.getString(columnName));
		} 
		catch (Exception e) {
			e.printStackTrace();
			return defaultValue;
		}		
	}
	
	@SuppressWarnings("unchecked")
	public JSONObject getServiceDetail(ResultSet rs, JSONObject serviceInfo, 
							String serviceName, QueryBuilder dynamicQueryBuilder, int addAdditional){
		JSONObject response = new JSONObject();
		if(addAdditional==1){
			response = serviceInfo;
		}
		
		String key = "";
		if(!serviceInfo.has("distributionJson")){
			serviceInfo.put("distributionJson", "");
		}
		
		DBOperation dbOp = new DBOperation();
		DBInfoHandler dbObject = new UpzDBSelector().upzDBInfo(serviceInfo);
		
		try{
			JSONObject detailJson = new DBTableInfoHandler(serviceName,"retrieve").getDetail().getJSONObject("keyMapField");
			
			Iterator<String> keys = detailJson.keys();
			
			while(keys.hasNext()){
				key = keys.next();
				if(key.equals(serviceInfo.getString("distributionJson"))){
					serviceInfo.put("distributionId",getResultSetValue(rs,detailJson.getJSONObject(key).getString("dbColumnName")));
					response.put(key, (serviceInfo.getString("distributionId").equals("")||serviceInfo.getString("distributionId").startsWith("["))?
											serviceInfo.getString("distributionId"):
											HandleStockDistribution.getDistributionInfo(serviceInfo, dbOp, dbObject, dynamicQueryBuilder));
				}
				else{
					response.put(key,getResultSetValue(rs,detailJson.getJSONObject(key).getString("dbColumnName"),
													getStringValue(detailJson.getJSONObject(key).getString("responseDefaultValue"),"null")));
				}
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}	
		finally{
			dbOp.dbObjectNullify(dbObject);			
		}
		
		return response;
	}
	
	@SuppressWarnings("unchecked")
	public JSONObject getResponse(ResultSet rs, JSONObject resultJson, String serviceName, int addAdditional){
		
		JSONObject response = new JSONObject();
		if(addAdditional==1){
			response = resultJson;
		}
			
		String key = "";
		
		try{
			JSONObject detailJson = new DBTableInfoHandler(serviceName,"retrieve").getDetail().getJSONObject("keyMapField");
			
			Iterator<String> keys = detailJson.keys();
			
			while(keys.hasNext()){
				key = keys.next();
				response.put(key,getResultSetValue(rs,detailJson.getJSONObject(key).getString("dbColumnName"),
								getStringValue(detailJson.getJSONObject(key).getString("responseDefaultValue"),"null")));				
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return response;
	}

	/**
	 * @param detail(jsonreq from different service)
	 * @param keys(specific keys that we want to set null)
	 * @return changed jsonobject of param:detail
	 */
	public static JSONObject setJsonKeyValueNull(JSONObject detail, String[] keys) {
		for (String key : keys)
			if (detail.has(key)) {
				if (detail.getString(key).matches(":\\s*[AP]M")) {
					detail.put(key, "");
				}
			}
		return detail;
	}
}