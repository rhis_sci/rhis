package org.sci.rhis.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.MessageDigest;

/**
 * @author sabah.mugab
 * @since March, 2018
 */
public class FileOperation {
		
	public static String getChecksum(String filePath) {
	    
		InputStream inputStream = null;
	    
		try {
	        inputStream = new FileInputStream(filePath);
	        byte[] buffer = new byte[1024];
	        MessageDigest digest = MessageDigest.getInstance("MD5");
	        int numRead = 0;
	        while (numRead != -1) {
	            numRead = inputStream.read(buffer);
	            if (numRead > 0)
	                digest.update(buffer, 0, numRead);
	        }
	        byte [] md5Bytes = digest.digest();
	        return convertHashToString(md5Bytes);
	    } 
		catch (Exception e) {
			e.printStackTrace();
	        return null;
	    } 
		finally {
	        if (inputStream != null) {
	            try {
	                inputStream.close();
	            } catch (Exception e) { }
	        }
	    }
	}

	private static String convertHashToString(byte[] md5Bytes) {
	    String returnVal = "";
	    for (int i = 0; i < md5Bytes.length; i++) {
	        returnVal += Integer.toString(( md5Bytes[i] & 0xff ) + 0x100, 16).substring(1);
	    }
	    return returnVal.toUpperCase();
	}
	
	public static boolean writeToFile(String file, String content){
		
		try{
			BufferedWriter writer = new BufferedWriter(new FileWriter(file));
		    writer.write(content);
		    writer.close();	
		    return true;
		}
		catch(Exception e){
			e.printStackTrace();
			return false;
		}		
	}
	
	public static boolean appendContentToFile(String file, String content){
		
		try{
			BufferedWriter writer = new BufferedWriter(new FileWriter(file, true));
		    writer.write(content);
		    writer.newLine();
		    writer.close();
		    return true;
		}
		catch(Exception e){
			e.printStackTrace();
			return false;
		}		
	}
	
	public static String getContentFromInputStream(InputStream is){
		StringBuilder sb = new StringBuilder();
		String line;
		try(BufferedReader br = new BufferedReader(new InputStreamReader(is))) {
    		while ((line = br.readLine()) != null) {
				sb.append(line);
			}
    		return sb.toString();
    	}
		catch(IOException ioe){
			ioe.printStackTrace();
			return "";
		}		
	}
	
	public static String readingFromFile(String file){
		String fileContent = "";
		String eachLine = null;
		try{
			FileReader fileReader = new FileReader(file);
			BufferedReader bufferedReader = new BufferedReader(fileReader);
			while((eachLine = bufferedReader.readLine()) != null) {
                if(fileContent.equals("")){
                	fileContent = eachLine;
                }
                else{
                	fileContent = fileContent + ";" + eachLine;
                }
            }
			bufferedReader.close();
			fileReader.close();
			
			return fileContent;
		}
		catch(FileNotFoundException ex) {
            System.out.println("Unable to open file '" + file + "'");
            return "";
        }
        catch(IOException ex) {
            System.out.println("Error reading file '" + file + "'");                  
            ex.printStackTrace();
            return "";
        }		
	}
}