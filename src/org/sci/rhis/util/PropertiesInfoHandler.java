package org.sci.rhis.util;

import java.util.Enumeration;
import java.util.Properties;

import org.sci.rhis.db.PropertiesReader;

/**
 * @author sabah.mugab
 * @since January, 2018
 */
public class PropertiesInfoHandler extends PropertiesDetail {
	
	public PropertiesInfoHandler(){
		setProp();
		setPropKeys();		
	}
	
	public Properties getProp(){
    	return PROPERTIES;
    }
	
	public Enumeration<?> getPropKeys(){
    	return PROPERTIES_KEYS;
    }
	
	private void setProp(){
		PROPERTIES = PropertiesReader.getPropReader().getUpazilaListProperties();
    }
	
	private void setPropKeys(){
		PROPERTIES_KEYS = PropertiesReader.getPropReader().getUpazilaListProperties().propertyNames();
    }
}