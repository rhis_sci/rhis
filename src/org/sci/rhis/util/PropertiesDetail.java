package org.sci.rhis.util;

import java.util.Properties;
import java.util.Enumeration;

/**
 * @author sabah.mugab
 * @since January, 2018
 */
abstract class PropertiesDetail {
	
	protected Properties PROPERTIES = null;
	protected Enumeration <?> PROPERTIES_KEYS = null;
	
}