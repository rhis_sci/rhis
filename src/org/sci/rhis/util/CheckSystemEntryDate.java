package org.sci.rhis.util;

import org.json.JSONObject;
import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.db.DBOperation;
import org.sci.rhis.db.QueryBuilder;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CheckSystemEntryDate {

    public static boolean systemEntryDateExist(JSONObject serviceInfo, DBOperation dbOp, DBInfoHandler dbObject,String serviceName,String columnName1,String columnName2) throws SQLException {
        QueryBuilder dynamicQueryBuilder = new QueryBuilder();
        ResultSet rs = null;
        boolean result = false;
        String healthId ;
        if(serviceInfo.has("healthid")){
            healthId = serviceInfo.getString("healthid");
        }else{
            healthId = serviceInfo.getString("healthId");
        }

        String sql = "SELECT * from " + dynamicQueryBuilder.getTable(serviceName)+
                " where "+dynamicQueryBuilder.getColumn(columnName1)+
                " = \'" + serviceInfo.getString("systemEntryDate") + "\'" +
                "and "+dynamicQueryBuilder.getColumn(columnName2)+
                " = " + healthId;

        rs = dbOp.dbExecute(dbObject, sql).getResultSet();

        if (rs.next()) {
            result = true;


        }
        return result;
    }

}
