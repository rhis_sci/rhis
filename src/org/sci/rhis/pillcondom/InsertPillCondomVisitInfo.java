package org.sci.rhis.pillcondom;

import java.sql.ResultSet;

import org.json.JSONObject;
import org.sci.rhis.client.ClientInfoUtil;
import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.db.DBOperation;
import org.sci.rhis.db.QueryBuilder;
import org.sci.rhis.util.JsonHandler;

/**
 * @author sabah.mugab
 * @since January, 2016
 */
public class InsertPillCondomVisitInfo {
	
	public static JSONObject createPillCondomVisit(DBOperation dbOp, DBInfoHandler dbObject, 
												JSONObject pillCondomInfo, QueryBuilder dynamicQueryBuilder) {
		
		try{
			ResultSet rs = dbOp.dbExecute(dbObject,dynamicQueryBuilder.getInsertQuery
					(new JsonHandler().addJsonKeyValueEdit(new JsonHandler().addJsonKeyIncrementalField
							(pillCondomInfo,"serviceId"), "PILLCONDOM"))).getResultSet();
	        
	        if(rs.next()){
	        	pillCondomInfo.put("serviceId",rs.getString(dynamicQueryBuilder.getColumn("PILLCONDOM_serviceId")));
	        	dbOp.dbStatementExecute(dbObject, dynamicQueryBuilder.getInsertQuery(
	        					new JsonHandler().addJsonKeyValueEdit(new JsonHandler().addJsonKeyIncrementalField
	        							(pillCondomInfo,""), "FPEXAMINATION_PILLCONDOM")));
	        	
	        	if(pillCondomInfo.getString("methodType").equals("1") || pillCondomInfo.getString("methodType").equals("2")
	        			|| pillCondomInfo.getString("methodType").equals("10")){
	        		pillCondomInfo.put("isNewClient", 1);
	        		pillCondomInfo.put("currentMethod", pillCondomInfo.get("methodType"));
	        	}
	        	else{
	        		pillCondomInfo.put("isNewClient", 2);
	        		pillCondomInfo.put("currentMethod", "");
	        	}
                dbOp.dbStatementExecute(dbObject,dynamicQueryBuilder.getUpdateQuery(
                		new JsonHandler().addJsonKeyValueEdit(pillCondomInfo, "FPSTATUS")));
			}
	        else{
	        	pillCondomInfo.put("serviceId","");
	        }
	        			
			if(!rs.isClosed()){
				rs.close();
			}
			if(!pillCondomInfo.get("mobileNo").equals("")){			
				ClientInfoUtil.updateClientMobileNo(pillCondomInfo, dbOp, dbObject);
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return pillCondomInfo;		
	}
}