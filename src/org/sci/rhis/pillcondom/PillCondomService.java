package org.sci.rhis.pillcondom;

import org.json.JSONObject;
import org.sci.rhis.client.ClientInfoUtil;
import org.sci.rhis.client.CreateRegNo;
import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.db.DBOperation;
import org.sci.rhis.db.QueryBuilder;
import org.sci.rhis.db.UpzDBSelector;
import org.sci.rhis.util.CheckSystemEntryDate;

/**
 * @author sabah.mugab
 * @since January, 2016
 */
public class PillCondomService {
	
	static final int pillCondom = 2; //for registration number
	static final int FPType = 1; //for FPExamination
	
	public static JSONObject getDetailInfo(JSONObject pillCondomInfo) {
		
		JSONObject pillCondomVisits = new JSONObject();
		DBOperation dbOp = new DBOperation();
		DBInfoHandler dbObject = new UpzDBSelector().upzDBInfo(pillCondomInfo);
		QueryBuilder dynamicQueryBuilder = new QueryBuilder();
		
		pillCondomInfo.put("serviceCategory", pillCondom);
		pillCondomInfo.put("FPType",FPType);
		
		try{
			if(pillCondomInfo.get("pillCondomLoad").equals("")){
				//check if systementrydate exist in requestjson and DB to remove duplicate entry
				if (pillCondomInfo.has("systemEntryDate") && !CheckSystemEntryDate.systemEntryDateExist(pillCondomInfo, dbOp, dbObject, "PILLCONDOM","PILLCONDOM_systemEntryDate","PILLCONDOM_healthId")) {
					pillCondomInfo = InsertPillCondomVisitInfo.createPillCondomVisit(dbOp, dbObject, pillCondomInfo, dynamicQueryBuilder);
				}else if(!pillCondomInfo.has("systemEntryDate")){
					pillCondomInfo = InsertPillCondomVisitInfo.createPillCondomVisit(dbOp, dbObject, pillCondomInfo, dynamicQueryBuilder);
				}

				if(pillCondomInfo.getString("serviceId").equals("1")){
					CreateRegNo.pushReg(dbOp, dbObject, pillCondomInfo, pillCondomVisits);
				}
			}
			else if(pillCondomInfo.get("pillCondomLoad").equals("update")){			
				UpdatePillCondomVisitInfo.updatePillCondomVisit(dbOp, dbObject, pillCondomInfo, dynamicQueryBuilder);
			}
			else if(pillCondomInfo.get("pillCondomLoad").equals("delete")){			
				DeletePillCondomVisitInfo.deletePillCondomVisit(dbOp, dbObject, pillCondomInfo, dynamicQueryBuilder);
			}
			pillCondomVisits = RetrievePillCondomVisitInfo.getPillCondomVisits(dbOp, dbObject, pillCondomInfo,pillCondomVisits, dynamicQueryBuilder);
			pillCondomVisits = ClientInfoUtil.getRegNumber(dbOp, dbObject, pillCondomInfo, pillCondomVisits);
		}
		catch(Exception e){
			e.printStackTrace();
		}
		finally{
			dbOp.dbObjectNullify(dbObject);
		}
		return pillCondomVisits;
	}
}