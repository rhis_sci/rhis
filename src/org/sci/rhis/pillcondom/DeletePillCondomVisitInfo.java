package org.sci.rhis.pillcondom;

import java.sql.ResultSet;

import org.json.JSONObject;
import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.db.DBOperation;
import org.sci.rhis.db.QueryBuilder;
import org.sci.rhis.fp.common.FPOp;
import org.sci.rhis.util.JsonHandler;

/**
 * @author sabah.mugab
 * @since January,2016
 */
public class DeletePillCondomVisitInfo {

	public static boolean deletePillCondomVisit(DBOperation dbOp, DBInfoHandler dbObject, 
												JSONObject pillCondomInfo, QueryBuilder dynamicQueryBuilder) {
		
		try{
	    	String returningSql = " RETURNING *";
			ResultSet rs = dbOp.dbExecute(dbObject,dynamicQueryBuilder.getDeleteQuery
					(new JsonHandler().addJsonKeyValueEdit(new JsonHandler().addJsonKeyMaxField
										(pillCondomInfo,"serviceId"), "PILLCONDOM")) + returningSql).getResultSet();
	        if(rs.next()){
	        	pillCondomInfo.put("serviceId",rs.getString(dynamicQueryBuilder.getColumn("PILLCONDOM_serviceId")));
	        	FPOp.deleteFPExamination(pillCondomInfo, dbOp, dbObject, dynamicQueryBuilder, 
	        				dynamicQueryBuilder.getColumn("FPEXAMINATION_PILLCONDOM_treatment"));
	        	
	        	pillCondomInfo.put("isNewClient", 2);
                pillCondomInfo.put("currentMethod", "");
                dbOp.dbStatementExecute(dbObject, dynamicQueryBuilder.getUpdateQuery(
                		new JsonHandler().addJsonKeyValueEdit(pillCondomInfo, "FPSTATUS")));
			}
	        else{
	        	pillCondomInfo.put("serviceId","");
	        }
	        			
			if(!rs.isClosed()){
				rs.close();
			}
			return true;
		}
		catch(Exception e){
			e.printStackTrace();
			return false;
		}	
	}
}