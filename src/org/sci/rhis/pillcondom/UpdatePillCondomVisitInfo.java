package org.sci.rhis.pillcondom;

import org.json.JSONObject;
import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.db.DBOperation;
import org.sci.rhis.db.QueryBuilder;
import org.sci.rhis.util.JsonHandler;

/**
 * @author sabah.mugab
 * @since January,2016
 */
public class UpdatePillCondomVisitInfo {

	public static boolean updatePillCondomVisit(DBOperation dbOp, DBInfoHandler dbObject, 
											JSONObject pillCondomInfo, QueryBuilder dynamicQueryBuilder) {
				
		try{
			dbOp.dbStatementExecute(dbObject, dynamicQueryBuilder.getUpdateQuery(
						new JsonHandler().addJsonKeyValueEdit(pillCondomInfo, "PILLCONDOM")));
			
			if(pillCondomInfo.getString("methodType").equals("1") || 
							pillCondomInfo.getString("methodType").equals("2") || 
								pillCondomInfo.getString("methodType").equals("10")){
        		pillCondomInfo.put("isNewClient", 1);
        		pillCondomInfo.put("currentMethod", pillCondomInfo.get("methodType"));
        	}
        	else{
        		pillCondomInfo.put("isNewClient", 2);
        		pillCondomInfo.put("currentMethod", "");
        	}
			
			dbOp.dbStatementExecute(dbObject, dynamicQueryBuilder.getUpdateQuery(
					new JsonHandler().addJsonKeyValueEdit(pillCondomInfo, "FPEXAMINATION_PILLCONDOM")));
			dbOp.dbStatementExecute(dbObject, dynamicQueryBuilder.getUpdateQuery(
					new JsonHandler().addJsonKeyValueEdit(pillCondomInfo, "FPSTATUS")));
			return true;
		}
		catch(Exception e){
			e.printStackTrace();
			return false;
		}	
	}
}