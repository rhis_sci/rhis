package org.sci.rhis.pncchild;

import org.json.JSONObject;
import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.db.DBOperation;
import org.sci.rhis.db.JSONKeyMapper;
import org.sci.rhis.db.QueryBuilder;
import org.sci.rhis.db.UpzDBSelector;
import org.sci.rhis.pncmother.InsertPNCVisitMotherInfo;
import org.sci.rhis.util.CheckSystemEntryDate;
import org.sci.rhis.util.JsonHandler;

/**
 * @author sabah.mugab
 * @since July, 2015
 */
public class PNCVisitChild {
	
	final static int PNCCHILDSERVICETYPE = 4;
	
	public static JSONObject getDetailInfo(JSONObject PNCChildInfo) {
		
		JSONObject PNCVisitsChild = new JSONObject();
		PNCChildInfo = new JsonHandler().addJsonKeyValueStockDistribution(new JSONKeyMapper().setRequiredKeys(PNCChildInfo, "PNCCHILD"), PNCCHILDSERVICETYPE);
		DBOperation dbOp = new DBOperation();
		DBInfoHandler dbObject = new UpzDBSelector().upzDBInfo(PNCChildInfo);
		DBInfoHandler dbObject1 = new UpzDBSelector().upzDBInfo(PNCChildInfo);
		QueryBuilder dynamicQueryBuilder = new QueryBuilder();
				
		try{
			if(PNCChildInfo.get("pncCLoad").equals("")){
//				InsertPNCVisitChildInfo.createPNCVisitChild(dbOp, dbObject, PNCChildInfo, dynamicQueryBuilder);

				//check if systementrydate exist in requestjson and DB to remove duplicate entry
				if (PNCChildInfo.has("systemEntryDate") && !CheckSystemEntryDate.systemEntryDateExist(PNCChildInfo, dbOp, dbObject, "PNCCHILD","PNCCHILD_systemEntryDate","PNCCHILD_healthid")) {
					InsertPNCVisitChildInfo.createPNCVisitChild(dbOp, dbObject, PNCChildInfo, dynamicQueryBuilder);
				}else if(!PNCChildInfo.has("systemEntryDate")){
					InsertPNCVisitChildInfo.createPNCVisitChild(dbOp, dbObject, PNCChildInfo, dynamicQueryBuilder);
				}
			}
			else if(PNCChildInfo.get("pncCLoad").equals("update")){			
				UpdatePNCVisitChildInfo.updatePNCVisitChild(dbOp, dbObject, PNCChildInfo, dynamicQueryBuilder);
			}
			else if(PNCChildInfo.get("pncCLoad").equals("delete")){			
				DeletePNCVisitChildInfo.deletePNCVisitChild(dbOp, dbObject, PNCChildInfo, dynamicQueryBuilder);
			}
			
			PNCVisitsChild = RetrievePNCVisitChildInfo.getPNCVisitsChild(dbOp, dbObject, dbObject1, PNCChildInfo,PNCVisitsChild, dynamicQueryBuilder);
		}
		catch(Exception e){
			e.printStackTrace();
		}
		finally{
			dbOp.dbObjectNullify(dbObject);
			dbOp.dbObjectNullify(dbObject1);
		}
		return PNCVisitsChild;
	}
}