package org.sci.rhis.pncchild;

import java.sql.ResultSet;

import org.json.JSONObject;
import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.db.DBOperation;
import org.sci.rhis.db.QueryBuilder;
import org.sci.rhis.distribution.StockDistributionRequest;
import org.sci.rhis.util.JsonHandler;

/**
 * @author sabah.mugab
 * @since February, 2017
 */
public class UpdatePNCVisitChildInfo {
	
	public static boolean updatePNCVisitChild(DBOperation dbOp, DBInfoHandler dbObject, JSONObject PNCChildInfo, QueryBuilder dynamicQueryBuilder) {
		
		try{
			String returningSql = " RETURNING " + dynamicQueryBuilder.getColumn("", "PNCCHILD_pnctreatment");
			ResultSet rs = dbOp.dbExecute(dbObject,(dynamicQueryBuilder.getUpdateQuery(
									new JsonHandler().addJsonKeyValueEdit(PNCChildInfo, "PNCCHILD")) + returningSql)).getResultSet();
			
			StockDistributionRequest.updateDistributionInfoHandler(rs, dynamicQueryBuilder.getColumn("PNCCHILD_pnctreatment"), 
											PNCChildInfo, dbOp, dbObject, dynamicQueryBuilder);
			
			if(!rs.isClosed()){
				rs.close();
			}
			return true;
		}
		catch(Exception e){
			e.printStackTrace();
			return false;
		}		
	}
}