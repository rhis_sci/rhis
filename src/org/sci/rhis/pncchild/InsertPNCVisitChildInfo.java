package org.sci.rhis.pncchild;

import java.sql.ResultSet;

import org.json.JSONObject;
import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.db.DBOperation;
import org.sci.rhis.db.QueryBuilder;
import org.sci.rhis.distribution.StockDistributionRequest;
import org.sci.rhis.util.JsonHandler;

/**
 * @author sabah.mugab
 * @since July, 2015
 */
public class InsertPNCVisitChildInfo {
	
	public static boolean createPNCVisitChild(DBOperation dbOp, DBInfoHandler dbObject, JSONObject PNCChildInfo, QueryBuilder dynamicQueryBuilder) {
		
		try{      
			ResultSet rs = dbOp.dbExecute(dbObject,dynamicQueryBuilder.getInsertQuery
					(new JsonHandler().addJsonKeyValueEdit(new JsonHandler().addJsonKeyIncrementalField
							(PNCChildInfo,"serviceId"), "PNCCHILD"))).getResultSet();
			
			StockDistributionRequest.insertDistributionInfoHandler(rs.next(), PNCChildInfo, dbOp, dbObject, dynamicQueryBuilder);
			
			if(!rs.isClosed()){
				rs.close();
			}
			return true;
		}
		catch(Exception e){
			e.printStackTrace();
			return false;
		}				
	}
}