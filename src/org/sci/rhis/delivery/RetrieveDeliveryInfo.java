package org.sci.rhis.delivery;

import java.sql.ResultSet;

import org.json.JSONObject;
import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.db.DBOperation;
import org.sci.rhis.db.QueryBuilder;
import org.sci.rhis.util.CalendarDate;
import org.sci.rhis.util.JsonHandler;

/**
 * @author sabah.mugab
 * @since June, 2015
 */
public class RetrieveDeliveryInfo {

	public static JSONObject getDeliveryInfo(DBOperation dbOp, DBInfoHandler dbObject, JSONObject deliveryInfo,
												JSONObject deliveryInformation, QueryBuilder dynamicQueryBuilder) {

		//functionalities for emonc register
		String delivery_extension_condition="";
		String delivery_extension_all_column = "";
		if(deliveryInfo.has("IsEMONC")) {
			delivery_extension_all_column = dynamicQueryBuilder.getTable( "DELIVERY_EXTENSION")+".*, ";
			delivery_extension_condition =" LEFT JOIN "+dynamicQueryBuilder.getTable( "DELIVERY_EXTENSION") + " USING ("  +dynamicQueryBuilder.getColumn("","DELIVERY_healthid")+"," +dynamicQueryBuilder.getColumn("","DELIVERY_pregno") +")";

		}

		try{
			String sql = "SELECT " + dynamicQueryBuilder.getTable("DELIVERY") + ".*, "
					    + delivery_extension_all_column
					    + dynamicQueryBuilder.getColumn("table", "PREGWOMEN_lmp")
					    + " FROM " + dynamicQueryBuilder.getTable("DELIVERY") + " INNER JOIN " + dynamicQueryBuilder.getTable("PREGWOMEN")
					    + " USING ("  +dynamicQueryBuilder.getColumn("","DELIVERY_healthid")+"," +dynamicQueryBuilder.getColumn("","DELIVERY_pregno") +")"
                        + delivery_extension_condition
					    + " WHERE " + dynamicQueryBuilder.getColumn("table", "DELIVERY_healthid",new String[]{deliveryInfo.getString("healthid")},"=")
						+ " AND " + dynamicQueryBuilder.getColumn("table", "DELIVERY_pregno",new String[]{deliveryInfo.getString("pregno")},"=");

			ResultSet rs = dbOp.dbExecute(dbObject,sql).getResultSet();
			deliveryInfo.put("distributionJson","dTreatment");		
			if(rs.next()){
				deliveryInformation = new JsonHandler().getServiceDetail(rs, deliveryInfo, "DELIVERY", dynamicQueryBuilder, 2);
                //functionalities for emonc
				if(deliveryInfo.has("IsEMONC")) {
					 new JsonHandler().getServiceDetail(rs, deliveryInformation, "DELIVERY_EXTENSION", dynamicQueryBuilder, 1);
				}

				if(deliveryInformation.getString("dTime").split(":").length > 2){
					String time = "";
					if(Integer.valueOf(deliveryInformation.getString("dTime").split(":")[0]) > 12){
						time = String.valueOf(Integer.valueOf(deliveryInformation.getString("dTime").split(":")[0]) - 12) 
							  + ":" + deliveryInformation.getString("dTime").split(":")[1] + " PM";  
					}
					else if(Integer.valueOf(deliveryInformation.getString("dTime").split(":")[0]) == 12){
						time = (deliveryInformation.getString("dTime").split(":")[0]) 
								  + ":" + deliveryInformation.getString("dTime").split(":")[1] + " PM";
					}
					else if(Integer.valueOf(deliveryInformation.getString("dTime").split(":")[0]) == 00){
						time = String.valueOf(Integer.valueOf(deliveryInformation.getString("dTime").split(":")[0]) + 12)
								  + ":" + deliveryInformation.getString("dTime").split(":")[1] + " AM";
					}
					else {
						time = (deliveryInformation.getString("dTime").split(":")[0]) 
								  + ":" + deliveryInformation.getString("dTime").split(":")[1] + " AM";
					}
					deliveryInformation.put("dTime",time);
				}
				deliveryInformation.put("LMP",new JsonHandler().getResultSetValue(rs,dynamicQueryBuilder.getColumn("PREGWOMEN_lmp")));
				
				if(!deliveryInformation.get("dDate").equals("") && !deliveryInformation.get("LMP").equals("")){

					int days = CalendarDate.getDaysCount(rs.getDate(dynamicQueryBuilder.getColumn("PREGWOMEN_lmp")), 
														rs.getDate(dynamicQueryBuilder.getColumn("DELIVERY_dDate")));
					if(days < (37*7)){
						deliveryInformation.put("immatureBirth", "1");
						deliveryInformation.put("immatureBirthWeek", (int) Math.floor(days/7));
					}
					else{
						deliveryInformation.put("immatureBirth", "2");
						deliveryInformation.put("immatureBirthWeek", (int) Math.floor(days/7));
					}
				}				
				
				deliveryInformation.put("dNew","No");
				if(!rs.isClosed()){
					rs.close();
				}		
			}
			else{
				deliveryInformation.put("dNew","Yes");
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}				
		return deliveryInformation;
	}
}