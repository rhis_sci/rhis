package org.sci.rhis.delivery;

import org.json.JSONObject;
import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.db.DBOperation;
import org.sci.rhis.db.JSONKeyMapper;
import org.sci.rhis.db.QueryBuilder;
import org.sci.rhis.db.UpzDBSelector;
import org.sci.rhis.util.JsonHandler;

/**
 * @author sabah.mugab
 * @since June, 2015
 */
public class DeliveryInfo {
	
	final static int DELIVERYSERVICETYPE = 2;
	
	public static JSONObject getDetailInfo(JSONObject deliveryInfo) {

		JSONObject deliveryInformation = new JSONObject();
		deliveryInfo = new JsonHandler().addJsonKeyValueStockDistribution(new JSONKeyMapper().setRequiredKeys(deliveryInfo, "DELIVERY"), DELIVERYSERVICETYPE);
		DBOperation dbOp = new DBOperation();
		DBInfoHandler dbObject = new UpzDBSelector().upzDBInfo(deliveryInfo);
		QueryBuilder dynamicQueryBuilder = new QueryBuilder();
		
		try{
			if(deliveryInfo.get("deliveryLoad").equals("")){
				InsertDeliveryInfo.createDelivery(dbOp, dbObject, deliveryInfo, dynamicQueryBuilder);
			}
			
			deliveryInformation = RetrieveDeliveryInfo.getDeliveryInfo(dbOp, dbObject, deliveryInfo, deliveryInformation, dynamicQueryBuilder);
		}
		catch(Exception e){
			e.printStackTrace();
		}
		finally{
			dbOp.dbObjectNullify(dbObject);
		}
		return deliveryInformation;
	}
}