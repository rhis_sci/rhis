package org.sci.rhis.delivery;

import java.sql.ResultSet;
import java.util.ArrayList;

import org.json.JSONObject;
import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.db.DBOperation;
import org.sci.rhis.db.QueryBuilder;
import org.sci.rhis.distribution.StockDistributionRequest;
import org.sci.rhis.util.JsonHandler;

/**
 * @author sabah.mugab
 * @since June, 2015
 */
public class InsertDeliveryInfo {
	
	public static boolean createDelivery(DBOperation dbOp, DBInfoHandler dbObject, JSONObject deliveryInfo, QueryBuilder dynamicQueryBuilder) {
		
		try{
			//quick fix
			if(deliveryInfo.getString("dTime").equals(": AM") || deliveryInfo.getString("dTime").equals(": PM")){
				deliveryInfo.put("dTime","");
			}
			
			ResultSet rs = dbOp.dbExecute(dbObject, dynamicQueryBuilder.getUpsertQuery(deliveryInfo, "DELIVERY")).getResultSet();
			StockDistributionRequest.upsertDistributionInfoHandler(rs, dynamicQueryBuilder.getColumn("DELIVERY_dTreatment"), 
												deliveryInfo, dbOp, dbObject, dynamicQueryBuilder);
			//functionalities for emonc register
			if(deliveryInfo.has("IsEMONC")) {
				//when time sets to null from tabend
				String [] keyset = new String[]{"dDischargeReferralTime","dAdmissionTime"};
				JsonHandler.setJsonKeyValueNull(deliveryInfo,keyset);
				dbOp.dbExecute(dbObject, dynamicQueryBuilder.getUpsertQuery(deliveryInfo, "DELIVERY_EXTENSION"));
			}
			return true;
		}
		catch(Exception e){
			e.printStackTrace();
			return false;
		}		
	}
}