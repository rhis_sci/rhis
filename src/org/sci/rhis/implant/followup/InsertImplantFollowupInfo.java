package org.sci.rhis.implant.followup;

import java.sql.ResultSet;

import org.json.JSONObject;
import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.db.DBOperation;
import org.sci.rhis.db.QueryBuilder;
import org.sci.rhis.distribution.StockDistributionRequest;
import org.sci.rhis.util.JsonHandler;

/**
 * @author sabah.mugab
 * @since March, 2016
 */
public class InsertImplantFollowupInfo {
	
	public static boolean createImplantFollowup(JSONObject implantInfo, JSONObject implantInformation, 
										DBOperation dbOp, DBInfoHandler dbObject, QueryBuilder dynamicQueryBuilder) {
					
		try{
			ResultSet rs = dbOp.dbExecute(dbObject,dynamicQueryBuilder.getInsertQuery
					(new JsonHandler().addJsonKeyValueEdit
							(new JsonHandler().addJsonKeyIncrementalField
									(implantInfo,"serviceId"), "IMPLANTFOLLOWUP"))).getResultSet();
						
			if(rs.next()){
				implantInfo.put("implantFollowupInsertSuccess","1");
	        	implantInfo.put("healthId",rs.getString(dynamicQueryBuilder.getColumn("IMPLANTFOLLOWUP_healthId")));
	        	implantInfo.put("implantCount",rs.getString(dynamicQueryBuilder.getColumn("IMPLANTFOLLOWUP_implantCount")));
	        	implantInfo.put("serviceId",rs.getString(dynamicQueryBuilder.getColumn("IMPLANTFOLLOWUP_serviceId")));
	        	implantInfo.put("treatment",rs.getString(dynamicQueryBuilder.getColumn("IMPLANTFOLLOWUP_treatment")));
	        	
	        	StockDistributionRequest.insertDistributionInfoHandler(true, implantInfo, dbOp, dbObject, dynamicQueryBuilder);	        	
			}
	        else{
	        	implantInfo.put("implantFollowupInsertSuccess","2");
	        	implantInfo.put("healthId","");
	        	implantInfo.put("implantCount","");
	        	implantInfo.put("serviceId","");
	        	implantInfo.put("treatment","");
	        }
			
			if(!rs.isClosed()){
				rs.close();
			}
			return true;
		}
		catch(Exception e){
			e.printStackTrace();
			return false;
		}
	}
}