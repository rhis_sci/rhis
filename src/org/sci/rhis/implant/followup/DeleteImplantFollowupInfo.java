package org.sci.rhis.implant.followup;

import java.sql.ResultSet;

import org.json.JSONObject;
import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.db.DBOperation;
import org.sci.rhis.db.QueryBuilder;
import org.sci.rhis.distribution.StockDistributionRequest;
import org.sci.rhis.util.JsonHandler;

/**
 * @author sabah.mugab
 * @since March, 2016
 */
public class DeleteImplantFollowupInfo {
	
	public static boolean deleteImplantFollowup(JSONObject implantInfo, JSONObject implantInformation, 
											DBOperation dbOp, DBInfoHandler dbObject, QueryBuilder dynamicQueryBuilder) {
					
		try{      	
			String returningSql = " RETURNING " + dynamicQueryBuilder.getColumn("", "IMPLANTFOLLOWUP_treatment");
			ResultSet rs = dbOp.dbExecute(dbObject,dynamicQueryBuilder.getDeleteQuery
					(new JsonHandler().addJsonKeyValueEdit(new JsonHandler().addJsonKeyMaxField
							(implantInfo,"serviceId"), "IMPLANTFOLLOWUP")) + returningSql).getResultSet();
			
			StockDistributionRequest.deleteDistributionInfoHandler(rs, dynamicQueryBuilder.getColumn("IMPLANTFOLLOWUP_treatment"), 
												implantInfo, dbOp, dbObject, dynamicQueryBuilder);
			if(!rs.isClosed()){
				rs.close();
			}			
			return true;
		}
		catch(Exception e){
			e.printStackTrace();
			return false;
		}			
	}
}