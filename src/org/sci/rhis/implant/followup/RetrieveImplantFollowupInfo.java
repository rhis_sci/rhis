package org.sci.rhis.implant.followup;

import java.sql.ResultSet;

import org.json.JSONObject;
import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.db.DBOperation;
import org.sci.rhis.db.QueryBuilder;
import org.sci.rhis.util.JsonHandler;

/**
 * @author sabah.mugab
 * @since March, 2016
 */
public class RetrieveImplantFollowupInfo {

	public static JSONObject getImplantFollowup(JSONObject implantInfo, JSONObject implantInformation, DBOperation dbOp, 
										DBInfoHandler dbObject, QueryBuilder dynamicQueryBuilder) {
		
		try{      	
			String sql = "SELECT * FROM " + dynamicQueryBuilder.getTable("IMPLANTFOLLOWUP")
						+ " WHERE " + dynamicQueryBuilder.getColumn("table", "IMPLANTFOLLOWUP_healthId",new String[]{implantInfo.getString("healthId")},"=")
						+ " AND " + dynamicQueryBuilder.getColumn("table", "IMPLANTFOLLOWUP_implantCount",new String[]{implantInformation.getString("implantCount")},"=") 
						+ " ORDER BY " + dynamicQueryBuilder.getColumn("table", "IMPLANTFOLLOWUP_serviceId") + " ASC";
			
			ResultSet rs = dbOp.dbExecute(dbObject,sql).getResultSet();
			
			JSONObject ImplantFollowupVisits = new JSONObject();
			implantInformation.put("followupCount", 0);
			implantInfo.put("distributionJson","treatment");
			
			while(rs.next()){
				ImplantFollowupVisits.put(rs.getString(dynamicQueryBuilder.getColumn("IMPLANTFOLLOWUP_serviceId")), 
											new JsonHandler().getServiceDetail(rs, implantInfo, "IMPLANTFOLLOWUP", dynamicQueryBuilder, 2));
				
				implantInformation.put("followupCount", (implantInformation.getInt("followupCount")+1));				
			}
			implantInformation.put("followup", ImplantFollowupVisits);
			
			if(!rs.isClosed()){
				rs.close();
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return implantInformation;
	}
}