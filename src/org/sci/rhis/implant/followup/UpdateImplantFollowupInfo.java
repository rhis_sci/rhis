package org.sci.rhis.implant.followup;

import java.sql.ResultSet;

import org.json.JSONObject;
import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.db.DBOperation;
import org.sci.rhis.db.QueryBuilder;
import org.sci.rhis.distribution.StockDistributionRequest;
//import org.sci.rhis.distribution.HandleStockDistribution;
import org.sci.rhis.util.JsonHandler;

/**
 * @author sabah.mugab
 * @since March, 2016
 */
public class UpdateImplantFollowupInfo {
	
	public static boolean updateImplantFollowup(JSONObject implantInfo, JSONObject implantInformation, 
										DBOperation dbOp, DBInfoHandler dbObject, QueryBuilder dynamicQueryBuilder) {
					
		try{ 
			String returningSql = " RETURNING " + dynamicQueryBuilder.getColumn("", "IMPLANTFOLLOWUP_treatment");
			ResultSet rs = dbOp.dbExecute(dbObject,(dynamicQueryBuilder.getUpdateQuery(
							new JsonHandler().addJsonKeyValueEdit(implantInfo, "IMPLANTFOLLOWUP")) + returningSql)).getResultSet();
			StockDistributionRequest.updateDistributionInfoHandler(rs, dynamicQueryBuilder.getColumn("IMPLANTFOLLOWUP_treatment"), 
									implantInfo, dbOp, dbObject, dynamicQueryBuilder);
			if(!rs.isClosed()){
				rs.close();
			}
			return true;
		}
		catch(Exception e){
			e.printStackTrace();
			return false;
		}			
	}
}