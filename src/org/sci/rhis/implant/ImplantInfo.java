package org.sci.rhis.implant;

import org.json.JSONObject;
import org.sci.rhis.client.ClientInfoUtil;
import org.sci.rhis.client.CreateRegNo;
import org.sci.rhis.db.*;
import org.sci.rhis.implant.followup.DeleteImplantFollowupInfo;
import org.sci.rhis.implant.followup.InsertImplantFollowupInfo;
import org.sci.rhis.implant.followup.RetrieveImplantFollowupInfo;
import org.sci.rhis.implant.followup.UpdateImplantFollowupInfo;
import org.sci.rhis.util.CheckSystemEntryDate;
import org.sci.rhis.util.JsonHandler;

/**
 * @author sabah.mugab
 * @since February, 2016
 */
public class ImplantInfo {
	
	final static int implant = 6;
	final static int IMPLANTSERVICTYPE = 11;
	final static int IMPLANTFOLLOWUPSERVICTYPE = 12;
	
	public static JSONObject getDetailInfo(JSONObject implantInfo) {
		
		JSONObject implantInformation = new JSONObject();
		implantInfo = new JsonHandler().addJsonKeyValueStockDistribution(new JSONKeyMapper().setRequiredKeys(implantInfo, "IMPLANT"), IMPLANTSERVICTYPE);
		DBOperation dbOp = new DBOperation();
		DBInfoHandler dbObject = new UpzDBSelector().upzDBInfo(implantInfo);
		QueryBuilder dynamicQueryBuilder = new QueryBuilder();
		implantInfo.put("serviceCategory", implant);

		try{
			implantInformation = new JSONObject();
			
			if(implantInfo.get("implantLoad").equals("insert")){

//				implantInformation = InsertImplantInfo.createImplant(implantInfo, implantInformation,
//														dbOp, dbObject, dynamicQueryBuilder);

				//check if systementrydate exist in requestjson and DB to remove duplicate entry
				if (implantInfo.has("systemEntryDate") && !CheckSystemEntryDate.systemEntryDateExist(implantInfo, dbOp, dbObject, "IMPLANT","IMPLANT_systemEntryDate","IMPLANT_healthId")) {
					implantInformation = InsertImplantInfo.createImplant(implantInfo, implantInformation, dbOp, dbObject, dynamicQueryBuilder);
				}else if(!implantInfo.has("systemEntryDate")){
					implantInformation = InsertImplantInfo.createImplant(implantInfo, implantInformation, dbOp, dbObject, dynamicQueryBuilder);
				}
				if(implantInformation.getString("implantInsertSuccess").equals("1")){
					CreateRegNo.pushReg(dbOp, dbObject, implantInfo, implantInformation);
				}
			}
			else if(implantInfo.get("implantLoad").equals("update")){
				implantInformation = UpdateImplantInfo.updateImplant(implantInfo, implantInformation, dbOp, dbObject, dynamicQueryBuilder);
			}
			else if(implantInfo.get("implantLoad").equals("retrieve")){
				implantInformation = RetrieveImplantInfo.getImplant(implantInfo, implantInformation, dbOp, dbObject, dynamicQueryBuilder);System.out.println(implantInformation);
			}
			else if(implantInfo.get("implantLoad").equals("")){
				implantInformation.put("implantCount", implantInfo.getString("implantCount"));
				implantInfo.put("serviceType", IMPLANTFOLLOWUPSERVICTYPE);
				
				if(implantInfo.get("implantFollowupLoad").equals("insert")){
					//check if systementrydate exist in requestjson and DB to remove duplicate entry
					if (implantInfo.has("systemEntryDate") && !CheckSystemEntryDate.systemEntryDateExist(implantInfo, dbOp, dbObject, "IMPLANTFOLLOWUP","IMPLANTFOLLOWUP_systemEntryDate","IMPLANTFOLLOWUP_healthId")) {
						InsertImplantFollowupInfo.createImplantFollowup(implantInfo, implantInformation, dbOp, dbObject, dynamicQueryBuilder);
					}else if(!implantInfo.has("systemEntryDate")){
						InsertImplantFollowupInfo.createImplantFollowup(implantInfo, implantInformation, dbOp, dbObject, dynamicQueryBuilder);
					}
//					InsertImplantFollowupInfo.createImplantFollowup(implantInfo, implantInformation, dbOp, dbObject, dynamicQueryBuilder);
				}
				else if(implantInfo.get("implantFollowupLoad").equals("update")){
					UpdateImplantFollowupInfo.updateImplantFollowup(implantInfo, implantInformation, dbOp, dbObject, dynamicQueryBuilder);
				}
				else if(implantInfo.get("implantFollowupLoad").equals("delete")){
					DeleteImplantFollowupInfo.deleteImplantFollowup(implantInfo, implantInformation, dbOp, dbObject, dynamicQueryBuilder);
				}				
			}
			
			if(!implantInformation.getString("implantCount").equals("")){
				implantInformation = RetrieveImplantFollowupInfo.getImplantFollowup(implantInfo, implantInformation, dbOp, dbObject, dynamicQueryBuilder);
			}
			
			implantInformation = ClientInfoUtil.getRegNumber(dbOp, dbObject, implantInfo, implantInformation);
		}
		catch(Exception e){
			e.printStackTrace();
		}
		finally{
			dbOp.dbObjectNullify(dbObject);			
		}
		
		return implantInformation;
	}
}