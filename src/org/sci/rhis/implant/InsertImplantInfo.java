package org.sci.rhis.implant;

import java.sql.ResultSet;

import org.json.JSONObject;
import org.sci.rhis.client.ClientInfoUtil;
import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.db.DBOperation;
import org.sci.rhis.db.QueryBuilder;
import org.sci.rhis.distribution.StockDistributionRequest;
import org.sci.rhis.util.JsonHandler;

/**
 * @author sabah.mugab
 * @since March, 2016
 */
public class InsertImplantInfo {
	
	public static JSONObject createImplant(JSONObject implantInfo, JSONObject implantInformation, 
								DBOperation dbOp, DBInfoHandler dbObject, QueryBuilder dynamicQueryBuilder) {
					
		try{
			ResultSet rs = dbOp.dbExecute(dbObject,dynamicQueryBuilder.getInsertQuery
					(new JsonHandler().addJsonKeyValueEdit
							(new JsonHandler().addJsonKeyIncrementalField
									(implantInfo,"implantCount"), "IMPLANT"))).getResultSet();			
						
			if(rs.next()){
				implantInfo.put("implantInsertSuccess","1");
	        	implantInfo.put("healthId",rs.getString(dynamicQueryBuilder.getColumn("IMPLANT_healthId")));
	        	implantInfo.put("implantCount",rs.getString(dynamicQueryBuilder.getColumn("IMPLANT_implantCount")));
	        	
	        	implantInfo.put("isNewClient", 1);
                implantInfo.put("currentMethod", ((implantInfo.getString("implantType").equals("1"))? 6:7));
                dbOp.dbStatementExecute(dbObject,
                		dynamicQueryBuilder.getUpdateQuery(new JsonHandler().addJsonKeyValueEdit(implantInfo, "FPSTATUS")));
                
                StockDistributionRequest.insertDistributionInfoHandler(true, implantInfo, dbOp, dbObject, dynamicQueryBuilder);
			}
	        else{
	        	implantInfo.put("implantInsertSuccess","2");
	        	implantInfo.put("healthId","");
	        	implantInfo.put("implantCount","");
	        }
				
			if(!implantInfo.get("mobileNo").equals("")){			
				ClientInfoUtil.updateClientMobileNo(implantInfo, dbOp, dbObject);				
			}
			
			implantInformation = RetrieveImplantInfo.getImplant(implantInfo, implantInformation, 
												dbOp, dbObject, dynamicQueryBuilder);
			
			if(!rs.isClosed()){
				rs.close();
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return implantInformation;
	}
}