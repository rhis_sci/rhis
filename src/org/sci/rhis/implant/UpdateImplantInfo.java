package org.sci.rhis.implant;

import java.sql.ResultSet;

import org.json.JSONObject;
import org.sci.rhis.client.ClientInfoUtil;
import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.db.DBOperation;
import org.sci.rhis.db.QueryBuilder;
import org.sci.rhis.distribution.StockDistributionRequest;
import org.sci.rhis.util.JsonHandler;

/**
 * @author sabah.mugab
 * @since March, 2016
 */
public class UpdateImplantInfo {
	
	public static JSONObject updateImplant(JSONObject implantInfo, JSONObject implantInformation, 
													DBOperation dbOp, DBInfoHandler dbObject, QueryBuilder dynamicQueryBuilder) {
					
		try{      	
			String returningSql = " RETURNING *";
			ResultSet rs = dbOp.dbExecute(dbObject,(dynamicQueryBuilder.getUpdateQuery(new JsonHandler().
													addJsonKeyValueEdit(implantInfo, "IMPLANT")) + returningSql)).getResultSet();
			
			if(rs.next()){
				implantInfo.put("implantUpdateSuccess","1");
	        	implantInfo.put("implantCount",rs.getString(dynamicQueryBuilder.getColumn("IMPLANT_implantCount")));
	        	implantInfo.put("treatment",new JsonHandler().getResultSetValue(rs,dynamicQueryBuilder.getColumn("IMPLANT_treatment")));
	        	
			}
	        else{
	        	implantInfo.put("implantUpdateSuccess","2");
	        	implantInfo.put("implantCount","");
	        	implantInfo.put("treatment","");
	        }
			rs.beforeFirst();
			StockDistributionRequest.updateDistributionInfoHandler(rs, dynamicQueryBuilder.getColumn("IMPLANT_treatment"), 
																implantInfo, dbOp, dbObject, dynamicQueryBuilder);
			
			if(!implantInfo.get("mobileNo").equals("")){			
				ClientInfoUtil.updateClientMobileNo(implantInfo, dbOp, dbObject);				
			}
			
			implantInformation = RetrieveImplantInfo.getImplant(implantInfo, implantInformation, dbOp, dbObject, dynamicQueryBuilder);
			
			if(!rs.isClosed()){
				rs.close();
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}	
		return implantInformation;
	}
}