package org.sci.rhis.implant;

import java.sql.ResultSet;

import org.json.JSONObject;
import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.db.DBOperation;
import org.sci.rhis.db.QueryBuilder;
import org.sci.rhis.util.JsonHandler;

/**
 * @author sabah.mugab
 * @since March, 2016
 */
public class RetrieveImplantInfo {

	public static JSONObject getImplant(JSONObject implantInfo, JSONObject implantInformation, 
								DBOperation dbOp, DBInfoHandler dbObject, QueryBuilder dynamicQueryBuilder) {
		
		try{      	
			String sql = "SELECT " + dynamicQueryBuilder.getTable("IMPLANT") + ".*,"
						+ dynamicQueryBuilder.getColumn("table", "IU_FPINFO_ELCO_boy") + ","
						+ dynamicQueryBuilder.getColumn("table", "IU_FPINFO_ELCO_girl") + ","
						+ dynamicQueryBuilder.getColumn("table", "IU_FPINFO_ELCO_marrDate")
						+ " FROM " + dynamicQueryBuilder.getTable("IMPLANT")
						+ " LEFT JOIN " + dynamicQueryBuilder.getTable("IU_FPINFO_ELCO") + " ON "
						+ dynamicQueryBuilder.getPartialCondition("table", "IU_FPINFO_ELCO_healthId","table","IMPLANT_healthId","=")
						+ " WHERE " + dynamicQueryBuilder.getColumn("table", "IMPLANT_healthId",new String[]{implantInfo.getString("healthId")},"=") 
						+ " AND " + dynamicQueryBuilder.getColumn("table", "IMPLANT_implantCount") + " IN (SELECT MAX("
						+ dynamicQueryBuilder.getColumn("table", "IMPLANT_implantCount") + ") FROM " + dynamicQueryBuilder.getTable("IMPLANT")
						+ " WHERE " + dynamicQueryBuilder.getColumn("table", "IMPLANT_healthId",new String[]{implantInfo.getString("healthId")},"=") + ")";
			
			ResultSet rs = dbOp.dbExecute(dbObject,sql).getResultSet();
			implantInfo.put("distributionJson","treatment");
			
			if(rs.next()){
				implantInformation = new JsonHandler().getServiceDetail(rs, 
											implantInfo, "IMPLANT", dynamicQueryBuilder, 1);
				implantInformation = new JsonHandler().getResponse(rs, implantInformation, "IU_FPINFO_ELCO", 1);
				
				implantInformation.put("implantRetrieve","1");	        	
	        }
	        else{
	        	implantInformation.put("implantRetrieve","2");
	        	implantInformation.put("implantCount","");
	        }
			
			if(!rs.isClosed()){
				rs.close();
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return implantInformation;
	}
}