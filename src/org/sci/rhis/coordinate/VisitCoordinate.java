package org.sci.rhis.coordinate;

import org.json.JSONObject;

/**
 * @author sabah.mugab
 * @since January, 2018
 */
public class VisitCoordinate {
	
	public static JSONObject VisitsCoordinate = new JSONObject();
	
	public static JSONObject getDetailInfo(JSONObject requestInfo) {
		
		try{
			VisitsCoordinate = new JSONObject();
			DistrictVisitCoordinates.getAllCoordinates(requestInfo, VisitsCoordinate);
		}
		catch(Exception e){
			System.out.println(e);
			e.printStackTrace();
		}
		return VisitsCoordinate;
	}
}