package org.sci.rhis.coordinate;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.json.JSONObject;
import org.sci.rhis.util.PropertiesInfoHandler;

/**
 * @author sabah.mugab
 * @since January, 2018
 */
public class DistrictVisitCoordinates {
	
	private static JSONObject generateGeoListRequest(JSONObject requestInfo) {
		
		JSONObject upazilaList = new JSONObject();
		PropertiesInfoHandler upazilaDetail =  new PropertiesInfoHandler();
		
		try{
			if(requestInfo.get("zillaid").equals("All")){
						
				while (upazilaDetail.getPropKeys().hasMoreElements()) {					
				      String key = (String) upazilaDetail.getPropKeys().nextElement();
				      upazilaList.put(key,upazilaDetail.getProp().getProperty(key));					
				}
			}
			else{
				if(requestInfo.get("upazilaid").equals("All")){
					upazilaList.put(requestInfo.getString("zillaid"),upazilaDetail.getProp().getProperty(requestInfo.getString("zillaid")));
				}
				else{
					upazilaList.put(requestInfo.getString("zillaid"),requestInfo.getString("upazilaid"));
				}
			}
			
			requestInfo.put("upazilaid_list", upazilaList);
		}
		catch(Exception e){
			e.printStackTrace();			
			requestInfo.put("upazilaid_list", "");
		}
		return requestInfo;
	}
	
	private static JSONObject getGeoList (JSONObject requestInfo){
		try{
			if(!requestInfo.has("zillaid")){
				requestInfo.put("zillaid", "All");
			}
			if(!requestInfo.has("upazilaid")){
				requestInfo.put("upazilaid", "All");
			}
			
			if(requestInfo.getString("zillaid").equals("")){
				requestInfo.put("zillaid", "All");
			}
			if(requestInfo.getString("upazilaid").equals("")){
				requestInfo.put("upazilaid", "All");
			}
			if(!requestInfo.getString("zillaid").equals("") && !requestInfo.getString("upazilaid").equals("")){
				requestInfo = generateGeoListRequest(requestInfo);
			}
			return requestInfo;
		}
		catch(Exception e){
			e.printStackTrace();
			requestInfo.put("zillaid", "All");
			return generateGeoListRequest(requestInfo);
		}
	}
	
	private static List<Future<JSONObject>> getCoordinateList(JSONObject request){
        
		ExecutorService executor = Executors.newFixedThreadPool(25);
        List <Future<JSONObject>> coordinateList = new ArrayList<Future<JSONObject>>();
        JSONObject geo = new JSONObject();
        request = getGeoList (request);
        
        if(!request.getString("upazilaid_list").equals("")){
        	Iterator<?> keys = request.getJSONObject("upazilaid_list").keys();
        	while(keys.hasNext()) {
        		geo.put("zillaid", (String)keys.next());
        		for(String upazilaid : request.getJSONObject("upazilaid_list").getString(geo.getString("zillaid")).split(",")){
        			geo.put("upazilaid", upazilaid);
                    coordinateList.add(executor.submit(new loginLocationCallableExecutor(geo)));
        		}        		
        	}        	
        }
        executor.shutdown();
        return coordinateList;
	}
	
	private static List<Future<JSONObject>> getFacilityCoordinates(JSONObject request){
        
		ExecutorService executor = Executors.newFixedThreadPool(25);
        List <Future<JSONObject>> facilityCoordinates = new ArrayList<Future<JSONObject>>();
        JSONObject geo = new JSONObject();
        request = getGeoList (request);
        
        if(!request.getString("upazilaid_list").equals("")){
        	Iterator<?> keys = request.getJSONObject("upazilaid_list").keys();
        	while(keys.hasNext()) {
        		geo.put("zillaid", (String)keys.next());
        		for(String upazilaid : request.getJSONObject("upazilaid_list").getString(geo.getString("zillaid")).split(",")){
        			geo.put("upazilaid", upazilaid);
        			facilityCoordinates.add(executor.submit(new facilityDetailCallableExecutor(geo)));
        		}        		
        	}        	
        }
        executor.shutdown();
        return facilityCoordinates;
	}
	
	public static void getAllCoordinates(JSONObject requestInfo, JSONObject VisitsCoordinate){
		List <Future<JSONObject>> coordinateList = getCoordinateList(requestInfo);
		//List <Future<JSONObject>> facilityCoordinates = getFacilityCoordinates(requestInfo);
        
		try{
			JSONObject facilities = new JSONObject();
			for(Future<JSONObject> upazilaCoordinates : coordinateList){
				if(upazilaCoordinates.get().has("zillaid") && upazilaCoordinates.get().has("upazilaid")){
					if(!upazilaCoordinates.get().getString("zillaid").equals("") && !upazilaCoordinates.get().getString("upazilaid").equals("")){
						VisitsCoordinate.put((upazilaCoordinates.get().getString("zillaid") + "_" + upazilaCoordinates.get().getString("upazilaid")),
								upazilaCoordinates.get());
					}
				}
			}
			
			/*for(Future<JSONObject> upazilaFacilityCoordinates : facilityCoordinates){
				if(upazilaFacilityCoordinates.get().has("zillaid") && upazilaFacilityCoordinates.get().has("upazilaid")){
					if(!upazilaFacilityCoordinates.get().getString("zillaid").equals("") && !upazilaFacilityCoordinates.get().getString("upazilaid").equals("")){
						facilities.put((upazilaFacilityCoordinates.get().getString("zillaid") + "_" + upazilaFacilityCoordinates.get().getString("upazilaid")),
								upazilaFacilityCoordinates.get());
					}
				}
			}*/
			VisitsCoordinate.put("facilitydetail", facilities);
		}
		catch(Exception e){
			e.printStackTrace();
		}		
	}
}