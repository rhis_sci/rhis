package org.sci.rhis.coordinate;

import java.util.concurrent.Callable;

import org.json.JSONObject;
import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.db.DBOperation;
import org.sci.rhis.db.DistrictsUpzDBSelector;

/**
 * @author sabah.mugab
 * @since January, 2018
 */
public class facilityDetailCallableExecutor implements Callable<JSONObject>{
	
	DBOperation dbOp = new DBOperation();
	DBInfoHandler dbObject = new DBInfoHandler();
	JSONObject request = new JSONObject();
	
	public facilityDetailCallableExecutor(JSONObject req){
		this.dbObject =  new DistrictsUpzDBSelector().getDistDBInfo(req);
		this.request = req;
	}
		
	public JSONObject call() {
		JSONObject response = new JSONObject();
		try{
			response = FacilityInfo.getFacilityCoordinates(dbOp, dbObject, request);
			return response;
		}
		catch(Exception e){
			e.printStackTrace();
			response.put("zillaid", "");
			response.put("upazilaid", "");
			
			return response;
		}
		finally{
			dbOp.dbObjectNullify(dbObject);
		}
	}
}