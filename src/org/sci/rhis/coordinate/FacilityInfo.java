package org.sci.rhis.coordinate;

import java.sql.ResultSet;

import org.json.JSONObject;
import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.db.DBOperation;
import org.sci.rhis.db.QueryBuilder;
import org.sci.rhis.util.JsonHandler;

/**
 * @author sabah.mugab
 * @since January, 2018
 */
public class FacilityInfo {

	public static JSONObject getFacilityCoordinates(DBOperation dbOp, DBInfoHandler dbObject, JSONObject request) {

		JSONObject facilityPoint =  new JSONObject();
		QueryBuilder dynamicQueryBuilder = new QueryBuilder();
		
		try{
			String sql = "SELECT " + dynamicQueryBuilder.getColumn("","LOGIN_LOCATION_zillaid") + ", "
					+ dynamicQueryBuilder.getColumn("","LOGIN_LOCATION_upazilaid") + ", "
					+ dynamicQueryBuilder.getColumn("","LOGIN_LOCATION_unionid") + ", "
					+ dynamicQueryBuilder.getColumn("","LOGIN_LOCATION_ProvType") + ", "
					+ "CASE WHEN " + dynamicQueryBuilder.getColumn("","LOGIN_LOCATION_ProvType",new String[]{"4"},"=")
					+ " THEN format('%s (FWV)'," + dynamicQueryBuilder.getColumn("","LOGIN_LOCATION_ProvName") + ") ELSE " 
					+ "CASE WHEN " + dynamicQueryBuilder.getColumn("","LOGIN_LOCATION_ProvType",new String[]{"5"},"=")
					+ " THEN format('%s (SACMO-DGFP)'," + dynamicQueryBuilder.getColumn("","LOGIN_LOCATION_ProvName") + ") ELSE " 
					+ "CASE WHEN " + dynamicQueryBuilder.getColumn("","LOGIN_LOCATION_ProvType",new String[]{"6"},"=")
					+ " THEN format('%s (SACMO-DGHS)'," + dynamicQueryBuilder.getColumn("","LOGIN_LOCATION_ProvName") + ") ELSE "
					+ "CASE WHEN " + dynamicQueryBuilder.getColumn("","LOGIN_LOCATION_ProvType",new String[]{"17"},"=")
					+ " THEN format('%s (MIDWIFE)'," + dynamicQueryBuilder.getColumn("","LOGIN_LOCATION_ProvName") + ") ELSE "
					+ "CASE WHEN " + dynamicQueryBuilder.getColumn("","LOGIN_LOCATION_ProvType",new String[]{"101"},"=")
					+ " THEN format('%s (PARAMEDIC)'," + dynamicQueryBuilder.getColumn("","LOGIN_LOCATION_ProvName") + ") ELSE "
					+ dynamicQueryBuilder.getColumn("","LOGIN_LOCATION_ProvName") + " END END END END END, "
					+ dynamicQueryBuilder.getColumn("pd","LOGIN_LOCATION_FacilityName") + ", lt.* "
					+ " FROM " + dynamicQueryBuilder.getTable("LOGIN_LOCATION") + " lt "
					+ " NATURAL JOIN ( "
						+ "SELECT " + dynamicQueryBuilder.getColumn("","LOGIN_LOCATION_provider_id") + ", "
						+ "MAX(" + dynamicQueryBuilder.getColumn("","LOGIN_LOCATION_login_time") + ") AS login_time"
						+ " FROM " + dynamicQueryBuilder.getTable("LOGIN_LOCATION")
						+ " WHERE " + dynamicQueryBuilder.getColumn("","LOGIN_LOCATION_latitude",new String[]{""},"isnotnull")
						+ " AND " + dynamicQueryBuilder.getColumn("","LOGIN_LOCATION_latitude") + " <> 0"
						+ " AND " + dynamicQueryBuilder.getColumn("","LOGIN_LOCATION_login_time") + "::date = CURRENT_DATE"
						+ " GROUP BY " + dynamicQueryBuilder.getColumn("","LOGIN_LOCATION_provider_id") + ") sq"
					+ " INNER JOIN " + dynamicQueryBuilder.getTable("PROVIDER") + " pd ON "
					+ dynamicQueryBuilder.getPartialCondition("", "LOGIN_LOCATION_provider_id","pd","PROVIDER_providerid","=") + " AND "
					+ dynamicQueryBuilder.getColumn("pd","PROVIDER_providerexitdate",new String[]{""},"isnull") + " AND "
					+ dynamicQueryBuilder.getColumn("pd","PROVIDER_providerisactive",new String[]{"1"},"=") + " AND "
					+ dynamicQueryBuilder.getColumn("pd","PROVIDER_providertype",new String[]{"4","5","6","17","101"},"in")
					+ " ORDER BY " + dynamicQueryBuilder.getColumn("","LOGIN_LOCATION_zillaid") + ", "
					+ dynamicQueryBuilder.getColumn("","LOGIN_LOCATION_upazilaid") + ", "
					+ dynamicQueryBuilder.getColumn("","LOGIN_LOCATION_unionid") + ", "
					+ dynamicQueryBuilder.getColumn("","LOGIN_LOCATION_login_time") + " DESC";
			
			ResultSet rs = dbOp.dbExecute(dbObject,sql).getResultSet();
												
			while(rs.next()){
				
				facilityPoint.put(rs.getString(dynamicQueryBuilder.getColumn("LOGIN_LOCATION_provider_id")), 
								new JsonHandler().getResponse(rs, request, "LOGIN_LOCATION", 2));
				facilityPoint.put("zillaid", rs.getString(dynamicQueryBuilder.getColumn("LOGIN_LOCATION_zillaid")));
				facilityPoint.put("upazilaid", rs.getString(dynamicQueryBuilder.getColumn("LOGIN_LOCATION_upazilaid")));
			}
			
			if(!rs.isClosed()){
				rs.close();
			}
			
			return facilityPoint;
		}
		catch(Exception e){
			e.printStackTrace();
			
			facilityPoint.put("zillaid","");
			facilityPoint.put("upazilaid","");
			return facilityPoint;
		}		
	}
}