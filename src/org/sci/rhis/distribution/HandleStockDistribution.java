package org.sci.rhis.distribution;

import java.sql.ResultSet;

import org.json.JSONObject;
import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.db.DBOperation;
import org.sci.rhis.db.QueryBuilder;
import org.sci.rhis.util.CalendarDate;

/**
 * @author sabah.mugab
 * @since March, 2016
 */
public class HandleStockDistribution {
	
	public static boolean insertDistributionInfo(JSONObject distributionInfo, DBOperation dbOp, 
										DBInfoHandler dbObject, QueryBuilder dynamicQueryBuilder) {
		
		boolean status = false;
		
		try{
			String sql = "INSERT INTO " + dynamicQueryBuilder.getTable("ITEMDISTRIBUTION")
					+ " (" + dynamicQueryBuilder.getColumn("", "ITEMDISTRIBUTION_healthid") + ","
					+ dynamicQueryBuilder.getColumn("", "ITEMDISTRIBUTION_providerid") + ","
					+ dynamicQueryBuilder.getColumn("", "ITEMDISTRIBUTION_itemcode") + ","
					+ dynamicQueryBuilder.getColumn("", "ITEMDISTRIBUTION_itemqty") + ","
					+ dynamicQueryBuilder.getColumn("", "ITEMDISTRIBUTION_source") + ","
					+ dynamicQueryBuilder.getColumn("", "ITEMDISTRIBUTION_distributionid") + ","
					+ dynamicQueryBuilder.getColumn("", "ITEMDISTRIBUTION_servicetype") + ","
					+ dynamicQueryBuilder.getColumn("", "ITEMDISTRIBUTION_systemEntryDate") + ","
					+ dynamicQueryBuilder.getColumn("", "ITEMDISTRIBUTION_modifyDate") + ") VALUES ";
			
			for(String singleItem : distributionInfo.getString("treatment").replaceAll("\\[|\\]","").replaceAll("\"", "").split(",")){
				sql = sql + "(" + distributionInfo.get("healthid") + ","
						+ distributionInfo.get("providerid") + ","
						+ singleItem.split("_")[0] + ","
						+ singleItem.split("_")[1].split(":")[1] + ","
						+ singleItem.split("_")[1].split(":")[0] + ","
						+ distributionInfo.get("distributionId") + ","
						+ distributionInfo.get("serviceType") + ","
						+ "'" + CalendarDate.getCurrentDate() + "',"
						+ "'" + CalendarDate.getCurrentDate() + "')," ;
			}
			
			sql = sql.substring(0, sql.length() - 1);
			status = dbOp.dbStatementExecute(dbObject, sql);
	    }
		catch(Exception e){
			e.printStackTrace();
		}				
		return status;		
	}
	
	public static String getDistributionInfo(JSONObject distributionInfo, DBOperation dbOp, 
									DBInfoHandler dbObject, QueryBuilder dynamicQueryBuilder) {
		
		String treatmentDetail = "";
		
		try{
			String sql = "SELECT " + dynamicQueryBuilder.getTable("ITEMDISTRIBUTION") + ".*"
						+ " FROM " + dynamicQueryBuilder.getTable("ITEMDISTRIBUTION")
						+ " WHERE " + dynamicQueryBuilder.getColumn("table", "ITEMDISTRIBUTION_healthid",new String[]{distributionInfo.getString("healthid")},"=")
						+ " AND " + dynamicQueryBuilder.getColumn("table", "ITEMDISTRIBUTION_distributionid",new String[]{distributionInfo.getString("distributionId")},"=")
						+ " AND " + dynamicQueryBuilder.getColumn("table", "ITEMDISTRIBUTION_servicetype",new String[]{distributionInfo.getString("serviceType")},"=");
			
			ResultSet rs = dbOp.dbExecute(dbObject,sql).getResultSet();
									
			while(rs.next()){
				treatmentDetail = treatmentDetail + "\"";
				treatmentDetail = treatmentDetail + rs.getString(dynamicQueryBuilder.getColumn("ITEMDISTRIBUTION_itemcode")) 
								  + "_" + rs.getString(dynamicQueryBuilder.getColumn("ITEMDISTRIBUTION_source")) + ":" 
								  + rs.getString(dynamicQueryBuilder.getColumn("ITEMDISTRIBUTION_itemqty"));
				treatmentDetail = treatmentDetail + "\",";
			}
			treatmentDetail = (treatmentDetail.equals("") ? "" : ("[".concat(treatmentDetail.substring(0, treatmentDetail.length() - 1)).concat("]")));
						
			if(!rs.isClosed()){
				rs.close();
			}			
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return treatmentDetail;
	}
	
	public static void deleteDistributionInfo(JSONObject distributionInfo, DBOperation dbOp, 
										DBInfoHandler dbObject, QueryBuilder dynamicQueryBuilder) {
		
		try{
			String sql = "DELETE FROM " + dynamicQueryBuilder.getTable("ITEMDISTRIBUTION")
					+ " WHERE " + dynamicQueryBuilder.getColumn("table", "ITEMDISTRIBUTION_healthid",new String[]{distributionInfo.getString("healthid")},"=")
					+ " AND " + dynamicQueryBuilder.getColumn("table", "ITEMDISTRIBUTION_distributionid",new String[]{distributionInfo.getString("distributionId")},"=")
					+ " AND " + dynamicQueryBuilder.getColumn("table", "ITEMDISTRIBUTION_servicetype",new String[]{distributionInfo.getString("serviceType")},"=");
			
			dbOp.dbStatementExecute(dbObject,sql);			
		}
		catch(Exception e){
			e.printStackTrace();
		}			
	}
	
	public static void updateDistributionInfo(JSONObject distributionInfo, DBOperation dbOp, 
										DBInfoHandler dbObject, QueryBuilder dynamicQueryBuilder) {
		
		try{
			deleteDistributionInfo(distributionInfo, dbOp, dbObject, dynamicQueryBuilder);
			insertDistributionInfo(distributionInfo, dbOp, dbObject, dynamicQueryBuilder);			
		}
		catch(Exception e){
			e.printStackTrace();
		}			
	}
}