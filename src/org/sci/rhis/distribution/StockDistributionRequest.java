package org.sci.rhis.distribution;

/**
 * @author sabah.mugab
 * @since February, 2018
 */
import java.sql.ResultSet;

import org.json.JSONObject;
import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.db.DBOperation;
import org.sci.rhis.db.QueryBuilder;
import org.sci.rhis.util.JsonHandler;

public class StockDistributionRequest extends HandleStockDistribution{
	
	public static boolean insertDistributionInfoHandler(boolean status, JSONObject distributionInfo, 
											DBOperation dbOp, DBInfoHandler dbObject, QueryBuilder dynamicQueryBuilder){
		
		try{
			if(status && !distributionInfo.getString("treatment").equals("") && distributionInfo.getString("treatment").contains("_")){
				insertDistributionInfo(distributionInfo, dbOp, dbObject, dynamicQueryBuilder);
			}
			return true;
		}
		catch (Exception e){
			e.printStackTrace();
			return false;
		}		
	}
	
	public static JSONObject insertDistributionInfoHandler(ResultSet rs, String columnName, String columnName1, 
							JSONObject distributionInfo, DBOperation dbOp, DBInfoHandler dbObject, QueryBuilder dynamicQueryBuilder){
		
		try{
			if(rs.next()){
				distributionInfo.put("distributionId",new JsonHandler().getResultSetValue(rs,columnName));
				distributionInfo.put("serviceId",new JsonHandler().getResultSetValue(rs,columnName1));
				if(!distributionInfo.getString("treatment").equals("") && distributionInfo.getString("treatment").contains("_")){
					insertDistributionInfo(distributionInfo, dbOp, dbObject, dynamicQueryBuilder);
				}
			}
			else{
				distributionInfo.put("serviceId","");
			}
			return distributionInfo;
		}
		catch (Exception e){
			e.printStackTrace();
			distributionInfo.put("serviceId","");
			return distributionInfo;
		}		
	}
	
	public static boolean updateDistributionInfoHandler(ResultSet rs, String columnName, JSONObject distributionInfo, 
									DBOperation dbOp, DBInfoHandler dbObject, QueryBuilder dynamicQueryBuilder){
		
		try{
			if(rs.next() && !distributionInfo.getString("treatment").equals("") && distributionInfo.getString("treatment").contains("_")){
				distributionInfo.put("distributionId", new JsonHandler().getResultSetValue(rs,columnName));
				updateDistributionInfo(distributionInfo, dbOp, dbObject, dynamicQueryBuilder);
			}			
			return true;
		}
		catch (Exception e){
			e.printStackTrace();
			return false;
		}		
	}
	
	public static boolean upsertDistributionInfoHandler(ResultSet rs, String columnName, 
							JSONObject distributionInfo, DBOperation dbOp, DBInfoHandler dbObject, QueryBuilder dynamicQueryBuilder){
		
		try{
			if(rs.next() && !distributionInfo.getString("treatment").equals("") && distributionInfo.getString("treatment").contains("_")){
					distributionInfo.put("distributionId", new JsonHandler().getResultSetValue(rs,columnName));
					if(getDistributionInfo(distributionInfo, dbOp, dbObject, dynamicQueryBuilder).equals("")){
						insertDistributionInfo(distributionInfo, dbOp, dbObject, dynamicQueryBuilder);
					}
					else{
						updateDistributionInfo(distributionInfo, dbOp, dbObject, dynamicQueryBuilder);
					}
			}
			return true;
		}
		catch (Exception e){
			e.printStackTrace();
			return false;
		}		
	}
	
	public static boolean deleteDistributionInfoHandler(ResultSet rs, String columnName, 
					JSONObject distributionInfo, DBOperation dbOp, DBInfoHandler dbObject, QueryBuilder dynamicQueryBuilder){
		
		try{
			if(rs.next()){
				distributionInfo.put("distributionId",new JsonHandler().getResultSetValue(rs,columnName));
				if(!distributionInfo.getString("distributionId").equals("") && !distributionInfo.getString("distributionId").startsWith("[")){
					deleteDistributionInfo(distributionInfo, dbOp, dbObject, dynamicQueryBuilder);
				}
			}			
			return true;
		}
		catch (Exception e){
			e.printStackTrace();
			return false;
		}		
	}
}