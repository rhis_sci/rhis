package org.sci.rhis.permanentmethod;

import org.json.JSONObject;
import org.sci.rhis.client.ClientInfoUtil;
import org.sci.rhis.client.CreateRegNo;
import org.sci.rhis.db.*;
import org.sci.rhis.permanentmethod.followup.DeletePMSFollowupInfo;
import org.sci.rhis.permanentmethod.followup.InsertPMSFollowupInfo;
import org.sci.rhis.permanentmethod.followup.RetrievePMSFollowupInfo;
import org.sci.rhis.permanentmethod.followup.UpdatePMSFollowupInfo;
import org.sci.rhis.util.CheckSystemEntryDate;
import org.sci.rhis.util.JsonHandler;

/**
 * @author sabah.mugab
 * @since February, 2019
 */
public class PMInfo {


    final static int pms = 8; //for registration number
    final static int PMSSERVICTYPE = 14; //for item distribution
    final static int PMSFOLLOWUPSERVICTYPE = 15; //for item distribution

    public static JSONObject getDetailInfo(JSONObject pmsInfo) {
    	
    	JSONObject pmsInformation= null;
        QueryBuilder dynamicQueryBuilder = new QueryBuilder();
        DBOperation dbOp = new DBOperation();
		DBInfoHandler dbObject = new UpzDBSelector().upzDBInfo(pmsInfo);

        try{
        	pmsInformation = new JSONObject();
            pmsInfo = new JsonHandler().addJsonKeyValueStockDistribution(new JSONKeyMapper().setRequiredKeys(pmsInfo, "PMS"), PMSSERVICTYPE);
            pmsInfo.put("serviceCategory", pms);

            if(pmsInfo.get("pmsLoad").equals("insert")){
                //check if systementrydate exist in requestjson and DB to remove duplicate entry
                if (pmsInfo.has("systemEntryDate") && !CheckSystemEntryDate.systemEntryDateExist(pmsInfo, dbOp, dbObject, "PMS","PMS_systemEntryDate","PMS_healthId")) {
                    pmsInformation = InsertPMInfo.createPMS(pmsInfo, pmsInformation, dbOp, dbObject, dynamicQueryBuilder);
                }else if(!pmsInfo.has("systemEntryDate")){
                    pmsInformation = InsertPMInfo.createPMS(pmsInfo, pmsInformation, dbOp, dbObject, dynamicQueryBuilder);
                }
//                pmsInformation = InsertPMInfo.createPMS(pmsInfo, pmsInformation, dbOp, dbObject, dynamicQueryBuilder);
                if(pmsInformation.getString("pmsInsertSuccess").equals("1")){
                    CreateRegNo.pushReg(dbOp, dbObject, pmsInfo, pmsInformation);
                }
            }
            else if(pmsInfo.get("pmsLoad").equals("update")){
                pmsInformation = UpdatePMInfo.updatePM(pmsInfo, pmsInformation, dbOp, dbObject, dynamicQueryBuilder);
            }
            else if(pmsInfo.get("pmsLoad").equals("retrieve")){
                pmsInformation = RetrievePMInfo.getPM(pmsInfo, pmsInformation, dbOp, dbObject, dynamicQueryBuilder);
            }
            else if(pmsInfo.get("pmsLoad").equals("")){
                pmsInformation.put("pmsCount", pmsInfo.getString("pmsCount"));
                pmsInfo.put("serviceType", PMSFOLLOWUPSERVICTYPE);

                if(pmsInfo.get("pmsFollowupLoad").equals("insert")){
                    //check if systementrydate exist in requestjson and DB to remove duplicate entry
                    if (pmsInfo.has("systemEntryDate") && !CheckSystemEntryDate.systemEntryDateExist(pmsInfo, dbOp, dbObject, "PMSFOLLOWUP","PMSFOLLOWUP_systemEntryDate","PMSFOLLOWUP_healthId")) {
                        InsertPMSFollowupInfo.createPMSFollowup(pmsInfo, pmsInformation, dbOp, dbObject, dynamicQueryBuilder);
                    }else if(!pmsInfo.has("systemEntryDate")){
                        InsertPMSFollowupInfo.createPMSFollowup(pmsInfo, pmsInformation, dbOp, dbObject, dynamicQueryBuilder);
                    }
//                    InsertPMSFollowupInfo.createPMSFollowup(pmsInfo, pmsInformation, dbOp, dbObject, dynamicQueryBuilder);
                }
                else if(pmsInfo.get("pmsFollowupLoad").equals("update")){
                    UpdatePMSFollowupInfo.updatePMSFollowup(pmsInfo, pmsInformation, dbOp, dbObject, dynamicQueryBuilder);
                }
                else if(pmsInfo.get("pmsFollowupLoad").equals("delete")){
                    DeletePMSFollowupInfo.deletePMSFollowup(pmsInfo, pmsInformation, dbOp, dbObject, dynamicQueryBuilder);
                }
            }

            if(!pmsInformation.getString("pmsCount").equals("")){
                pmsInformation = RetrievePMSFollowupInfo.getPMSFollowup(pmsInfo, pmsInformation, dbOp, dbObject, dynamicQueryBuilder);
            }

            pmsInformation = ClientInfoUtil.getRegNumber(dbOp, dbObject, pmsInfo, pmsInformation);
        }
        catch(Exception e){
            e.printStackTrace();
        }
        return pmsInformation;
    }
}