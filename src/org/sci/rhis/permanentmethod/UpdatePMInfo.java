package org.sci.rhis.permanentmethod;

import java.sql.ResultSet;

import org.json.JSONObject;
import org.sci.rhis.client.ClientInfoUtil;
import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.db.DBOperation;
import org.sci.rhis.db.QueryBuilder;
import org.sci.rhis.distribution.StockDistributionRequest;
import org.sci.rhis.util.JsonHandler;

/**
 * @author sabah.mugab
 * @since February, 2019
 */
public class UpdatePMInfo {
	
	public static JSONObject updatePM(JSONObject pmsInfo, JSONObject pmsInformation, DBOperation dbOp, 
											DBInfoHandler dbObject, QueryBuilder dynamicQueryBuilder) {
					
		try{      	
			String returningSql = " RETURNING *";
			ResultSet rs = dbOp.dbExecute(dbObject,(dynamicQueryBuilder.getUpdateQuery(
									new JsonHandler().addJsonKeyValueEdit(pmsInfo, "PMS")) + returningSql)).getResultSet();
			
			if(rs.next()){
				pmsInfo.put("pmsUpdateSuccess","1");
				pmsInfo.put("pmsCount",rs.getString(dynamicQueryBuilder.getColumn("PMS_pmsCount")));
				pmsInfo.put("treatment",new JsonHandler().getResultSetValue(rs,dynamicQueryBuilder.getColumn("PMS_treatment")));
	        	
			}
	        else{
	        	pmsInfo.put("pmsUpdateSuccess","2");
	        	pmsInfo.put("pmsCount","");
	        	pmsInfo.put("treatment","");
	        }
			
			rs.beforeFirst();
			StockDistributionRequest.updateDistributionInfoHandler(rs, dynamicQueryBuilder.getColumn("PMS_treatment"), 
									pmsInfo, dbOp, dbObject, dynamicQueryBuilder);
			
			if(!pmsInfo.get("mobileNo").equals("")){			
				ClientInfoUtil.updateClientMobileNo(pmsInfo, dbOp, dbObject);				
			}
			
			pmsInformation = RetrievePMInfo.getPM(pmsInfo, pmsInformation, dbOp, dbObject, dynamicQueryBuilder);
			
			if(!rs.isClosed()){
				rs.close();
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}	
		return pmsInformation;
	}
}