package org.sci.rhis.permanentmethod;

import java.sql.ResultSet;

import org.json.JSONObject;

import org.sci.rhis.client.ClientInfoUtil;
import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.db.DBOperation;
import org.sci.rhis.db.QueryBuilder;
import org.sci.rhis.distribution.StockDistributionRequest;
import org.sci.rhis.util.JsonHandler;

/**
 * @author sabah.mugab
 * @since February, 2019
 */
public class InsertPMInfo {
	
	public static JSONObject createPMS(JSONObject pmsInfo, JSONObject pmsInformation, DBOperation dbOp, 
										DBInfoHandler dbObject, QueryBuilder dynamicQueryBuilder) {
					
		try{
			ResultSet rs = dbOp.dbExecute(dbObject,dynamicQueryBuilder.getInsertQuery
					(new JsonHandler().addJsonKeyValueEdit
							(new JsonHandler().addJsonKeyIncrementalField
									(pmsInfo,"pmsCount"), "PMS"))).getResultSet();
						
			if(rs.next()){
				pmsInfo.put("pmsInsertSuccess","1");
				pmsInfo.put("healthId",rs.getString(dynamicQueryBuilder.getColumn("PMS_healthId")));
				pmsInfo.put("pmsCount",rs.getString(dynamicQueryBuilder.getColumn("PMS_pmsCount")));
	        	
				pmsInfo.put("isNewClient", 1);
				pmsInfo.put("currentMethod", pmsInfo.getString("gender").equals("2")? 997 : 998);
                dbOp.dbStatementExecute(dbObject,dynamicQueryBuilder.getUpdateQuery(new JsonHandler().addJsonKeyValueEdit(pmsInfo, "FPSTATUS")));
	        	
                StockDistributionRequest.insertDistributionInfoHandler(true, pmsInfo, dbOp, dbObject, dynamicQueryBuilder);
			}
	        else{
	        	pmsInfo.put("iudInsertSuccess","2");
	        	pmsInfo.put("healthId","");
	        	pmsInfo.put("pmsCount","");
	        }
		
			if(!pmsInfo.get("mobileNo").equals("")){			
				ClientInfoUtil.updateClientMobileNo(pmsInfo, dbOp, dbObject);				
			}
			
			pmsInformation = RetrievePMInfo.getPM(pmsInfo, pmsInformation, dbOp, dbObject, dynamicQueryBuilder);
			
			if(!rs.isClosed()){
				rs.close();
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}			
		return pmsInformation;
	}
}