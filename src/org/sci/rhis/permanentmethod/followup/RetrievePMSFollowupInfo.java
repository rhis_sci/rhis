package org.sci.rhis.permanentmethod.followup;

import java.sql.ResultSet;

import org.json.JSONObject;
import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.db.DBOperation;
import org.sci.rhis.db.QueryBuilder;
import org.sci.rhis.util.JsonHandler;

/**
 * @author sabah.mugab
 * @since March, 2016
 */
public class RetrievePMSFollowupInfo {
   
	public static JSONObject getPMSFollowup(JSONObject pmsInfo, JSONObject pmsInformation, DBOperation dbOp, 
											DBInfoHandler dbObject, QueryBuilder dynamicQueryBuilder) {
		
		try{      	
			String sql = "SELECT * FROM " + dynamicQueryBuilder.getTable("PMSFOLLOWUP")
						+ " WHERE " + dynamicQueryBuilder.getColumn("table", "PMSFOLLOWUP_healthId",new String[]{pmsInfo.getString("healthId")},"=")
						+ " AND " + dynamicQueryBuilder.getColumn("table", "PMSFOLLOWUP_pmsCount",new String[]{pmsInformation.getString("pmsCount")},"=") 
						+ " ORDER BY " + dynamicQueryBuilder.getColumn("table", "PMSFOLLOWUP_serviceId") + " ASC";
			
			ResultSet rs = dbOp.dbExecute(dbObject,sql).getResultSet();
			
			JSONObject PMSFollowupVisits = new JSONObject();
			pmsInformation.put("followupCount", 0);
			pmsInfo.put("distributionJson","treatment");
			
			while(rs.next()){
				PMSFollowupVisits.put(rs.getString(dynamicQueryBuilder.getColumn("PMSFOLLOWUP_serviceId")), 
							new JsonHandler().getServiceDetail(rs, pmsInfo, "PMSFOLLOWUP", dynamicQueryBuilder, 2));
				
				pmsInformation.put("followupCount", (pmsInformation.getInt("followupCount")+1));				
			}
			pmsInformation.put("followup", PMSFollowupVisits);
			
			if(!rs.isClosed()){
				rs.close();
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}	
		return pmsInformation;
	}
}