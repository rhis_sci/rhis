package org.sci.rhis.permanentmethod.followup;

import java.sql.ResultSet;

import org.json.JSONObject;
import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.db.DBOperation;
import org.sci.rhis.db.QueryBuilder;
import org.sci.rhis.distribution.StockDistributionRequest;
import org.sci.rhis.util.JsonHandler;

/**
 * @author sabah.mugab
 * @since February, 2019
 */
public class InsertPMSFollowupInfo {
	
	public static boolean createPMSFollowup(JSONObject pmsInfo, JSONObject pmsInformation, 
											DBOperation dbOp, DBInfoHandler dbObject, QueryBuilder dynamicQueryBuilder) {
					
		try{
			ResultSet rs = dbOp.dbExecute(dbObject,new QueryBuilder().getInsertQuery
					(new JsonHandler().addJsonKeyValueEdit
							(new JsonHandler().addJsonKeyIncrementalField
									(pmsInfo,"serviceId"), "PMSFOLLOWUP"))).getResultSet();
			
			if(rs.next()){
				pmsInfo.put("pmsFollowupInsertSuccess","1");
				pmsInfo.put("healthId",rs.getString(dynamicQueryBuilder.getColumn("PMSFOLLOWUP_healthId")));
				pmsInfo.put("pmsCount",rs.getString(dynamicQueryBuilder.getColumn("PMSFOLLOWUP_pmsCount")));
				pmsInfo.put("serviceId",rs.getString(dynamicQueryBuilder.getColumn("PMSFOLLOWUP_serviceId")));
				pmsInfo.put("treatment",rs.getString(dynamicQueryBuilder.getColumn("PMSFOLLOWUP_treatment")));
	        	
	        	StockDistributionRequest.insertDistributionInfoHandler(true, pmsInfo, dbOp, dbObject, dynamicQueryBuilder);	        	
			}
	        else{
	        	pmsInfo.put("pmsFollowupInsertSuccess","2");
	        	pmsInfo.put("healthId","");
	        	pmsInfo.put("pmsCount","");
	        	pmsInfo.put("serviceId","");
	        	pmsInfo.put("treatment","");
	        }
			
			if(!rs.isClosed()){
				rs.close();
			}
			return true;
		}
		catch(Exception e){
			e.printStackTrace();
			return false;
		}	
	}
}