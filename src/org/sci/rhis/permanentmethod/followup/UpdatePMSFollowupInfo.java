package org.sci.rhis.permanentmethod.followup;

import java.sql.ResultSet;

import org.json.JSONObject;
import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.db.DBOperation;
import org.sci.rhis.db.QueryBuilder;
import org.sci.rhis.distribution.StockDistributionRequest;
import org.sci.rhis.util.JsonHandler;

/**
 * @author sabah.mugab
 * @since February, 2019
 */
public class UpdatePMSFollowupInfo {
	
	public static boolean updatePMSFollowup(JSONObject pmsInfo, JSONObject pmsInformation, DBOperation dbOp, 
											DBInfoHandler dbObject, QueryBuilder dynamicQueryBuilder) {
				
		try{      	
			String returningSql = " RETURNING " + dynamicQueryBuilder.getColumn("", "PMSFOLLOWUP_treatment");
			ResultSet rs = dbOp.dbExecute(dbObject,(dynamicQueryBuilder.getUpdateQuery(
							new JsonHandler().addJsonKeyValueEdit(pmsInfo, "PMSFOLLOWUP")) + returningSql)).getResultSet();
			StockDistributionRequest.updateDistributionInfoHandler(rs, dynamicQueryBuilder.getColumn("PMSFOLLOWUP_treatment"), 
											pmsInfo, dbOp, dbObject, dynamicQueryBuilder);
			if(!rs.isClosed()){
				rs.close();
			}
			return true;
		}
		catch(Exception e){
			e.printStackTrace();
			return false;
		}			
	}
}