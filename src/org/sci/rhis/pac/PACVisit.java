package org.sci.rhis.pac;

import org.json.JSONObject;
import org.sci.rhis.db.*;
import org.sci.rhis.util.CheckSystemEntryDate;
import org.sci.rhis.util.JsonHandler;

/**
 * @author sabah.mugab
 * @since October, 2015
 */
public class PACVisit {
	
	final static int PACSERVICETYPE = 5;
	
	public static JSONObject getDetailInfo(JSONObject PACInfo) {
			
		JSONObject PACVisits = new JSONObject();
		PACInfo = new JsonHandler().addJsonKeyValueStockDistribution(new JSONKeyMapper().setRequiredKeys(PACInfo, "PAC"), PACSERVICETYPE);
		DBOperation dbOp = new DBOperation();
		DBInfoHandler dbObject = new UpzDBSelector().upzDBInfo(PACInfo);
		QueryBuilder dynamicQueryBuilder = new QueryBuilder();
		
		try{
			if(PACInfo.get("pacLoad").equals("")){
//				InsertPACVisitInfo.createPACVisit(dbOp, dbObject, PACInfo, dynamicQueryBuilder);

				//check if systementrydate exist in requestjson and DB to remove duplicate entry
				if (PACInfo.has("systemEntryDate") && !CheckSystemEntryDate.systemEntryDateExist(PACInfo, dbOp, dbObject, "PAC","PAC_systemEntryDate","PAC_healthId")) {
					InsertPACVisitInfo.createPACVisit(dbOp, dbObject, PACInfo, dynamicQueryBuilder);
				}else if(!PACInfo.has("systemEntryDate")){
					InsertPACVisitInfo.createPACVisit(dbOp, dbObject, PACInfo, dynamicQueryBuilder);
				}
			}
			if(PACInfo.get("pacLoad").equals("update")){
				UpdatePACVisitInfo.updatePACVisit(dbOp, dbObject, PACInfo, dynamicQueryBuilder);
			}
			else if(PACInfo.get("pacLoad").equals("delete")){			
				DeletePACVisitInfo.deletePACVisit(dbOp, dbObject, PACInfo, dynamicQueryBuilder);
			}
			
			PACVisits = RetrievePACVisitInfo.getPACVisits(dbOp, dbObject, PACInfo,PACVisits, dynamicQueryBuilder);
		}
		catch(Exception e){
			e.printStackTrace();
		}
		finally{
			dbOp.dbObjectNullify(dbObject);
		}
		return PACVisits;
	}
}