package org.sci.rhis.pac;

import java.sql.ResultSet;

import org.json.JSONObject;
import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.db.DBOperation;
import org.sci.rhis.db.QueryBuilder;
import org.sci.rhis.distribution.StockDistributionRequest;
import org.sci.rhis.util.JsonHandler;

/**
 * @author sabah.mugab
 * @since October, 2015
 */
public class InsertPACVisitInfo {
	
	public static boolean createPACVisit(DBOperation dbOp, DBInfoHandler dbObject, 
											JSONObject PACInfo, QueryBuilder dynamicQueryBuilder) {
		try{
			ResultSet rs = dbOp.dbExecute(dbObject,new QueryBuilder().getInsertQuery
							(new JsonHandler().addJsonKeyValueEdit(new JsonHandler().addJsonKeyIncrementalField
													(PACInfo,"serviceId"), "PAC"))).getResultSet();
			StockDistributionRequest.insertDistributionInfoHandler(rs.next(), PACInfo, dbOp, dbObject, dynamicQueryBuilder);
	        
	        if(!rs.isClosed()){
				rs.close();
			}
			return true;
		}
		catch(Exception e){
			e.printStackTrace();
			return false;
		}		
	}
}