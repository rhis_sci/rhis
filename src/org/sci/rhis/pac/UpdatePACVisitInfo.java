package org.sci.rhis.pac;

import java.sql.ResultSet;

import org.json.JSONObject;
import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.db.DBOperation;
import org.sci.rhis.db.QueryBuilder;
import org.sci.rhis.distribution.StockDistributionRequest;
import org.sci.rhis.util.JsonHandler;

/**
 * @author sabah.mugab
 * @since February,2017
 */
public class UpdatePACVisitInfo {

	public static boolean updatePACVisit(DBOperation dbOp, DBInfoHandler dbObject, 
											JSONObject PACInfo, QueryBuilder dynamicQueryBuilder) {
		
		try{
			String returningSql = " RETURNING " + dynamicQueryBuilder.getColumn("", "PAC_treatment");
			ResultSet rs = dbOp.dbExecute(dbObject,(dynamicQueryBuilder.getUpdateQuery(
									new JsonHandler().addJsonKeyValueEdit(PACInfo, "PAC")) + returningSql)).getResultSet();
			
			StockDistributionRequest.updateDistributionInfoHandler(rs, dynamicQueryBuilder.getColumn("PAC_treatment"), 
									PACInfo, dbOp, dbObject, dynamicQueryBuilder);
			
			if(!rs.isClosed()){
				rs.close();
			}
			return true;
		}
		catch(Exception e){
			e.printStackTrace();
			return false;
		}			
	}
}