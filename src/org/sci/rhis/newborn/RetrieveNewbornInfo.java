package org.sci.rhis.newborn;

import java.sql.ResultSet;

import org.json.JSONObject;
import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.db.DBOperation;
import org.sci.rhis.db.QueryBuilder;
import org.sci.rhis.util.JsonHandler;

/**
 * @author sabah.mugab
 * @since July, 2015
 */
public class RetrieveNewbornInfo {

	public static JSONObject getNewbornInfo(DBOperation dbOp, DBInfoHandler dbObject, JSONObject newbornInfo, 
												JSONObject newbornInformation, QueryBuilder dynamicQueryBuilder) {
		
		boolean status = false;
				
		try{
			String sql = "SELECT " + dynamicQueryBuilder.getColumn("table", "DELIVERY_pregno") + ","
						+ dynamicQueryBuilder.getColumn("", "DELIVERY_dAbortion")
						+ " FROM " + dynamicQueryBuilder.getTable("DELIVERY")
						+ " WHERE " + dynamicQueryBuilder.getColumn("table", "DELIVERY_healthid",new String[]{newbornInfo.getString("healthid")},"=") 
						+ " AND " + dynamicQueryBuilder.getColumn("table", "DELIVERY_pregno",new String[]{newbornInfo.getString("pregno")},"=");
													
			ResultSet rs = dbOp.dbExecute(dbObject,sql).getResultSet();
					
			if(rs.next()){
				status = true;
				newbornInformation.put("deliveryInfo","1");
				newbornInformation.put("abortion",new JsonHandler().getResultSetValue(rs,dynamicQueryBuilder.getColumn("DELIVERY_dAbortion")));
			}
			else{
				newbornInformation.put("deliveryInfo","0");
				newbornInformation.put("abortion","");
			}

			//functionalities for emonc register
			String newborn_extension_table = "";
			String newborn_extension_condition="";
			String newborn_extension_order_by="";
			if(newbornInfo.has("IsEMONC")) {
				newborn_extension_table = ", "+dynamicQueryBuilder.getTable("NEWBORN_EXTENSION")+ ".*";
				newborn_extension_condition = " LEFT JOIN " + dynamicQueryBuilder.getTable("NEWBORN_EXTENSION")
						+ " USING (" +dynamicQueryBuilder.getColumn("","NEWBORN_healthid")+"," +dynamicQueryBuilder.getColumn("","NEWBORN_pregno")
						+ ","+dynamicQueryBuilder.getColumn("","NEWBORN_childno")+")";

				newborn_extension_order_by = " ORDER BY " + dynamicQueryBuilder.getColumn("table","NEWBORN_childno") + " ASC";


			}

			sql = "SELECT " + dynamicQueryBuilder.getTable("NEWBORN") + ".*"
					    + newborn_extension_table
						+ " FROM " + dynamicQueryBuilder.getTable("NEWBORN")
					    + newborn_extension_condition
						+ " WHERE " + dynamicQueryBuilder.getColumn("table", "NEWBORN_healthid",new String[]{newbornInfo.getString("healthid")},"=") 
						+ " AND " + dynamicQueryBuilder.getColumn("table", "NEWBORN_pregno",new String[]{newbornInfo.getString("pregno")},"=")
						+ newborn_extension_order_by;
			
			rs = dbOp.dbExecute(dbObject,sql).getResultSet();
			newbornInformation.put("hasNewbornInfo", "No");
			newbornInformation.put("count", 0);
			
			int cnt = 1;
			
			while(rs.next()){
				status = (status && true);
			
				newbornInformation.put("count", cnt);
				newbornInformation.put(rs.getString(dynamicQueryBuilder.getColumn("NEWBORN_childno")), 
															new JsonHandler().getResponse(rs, newbornInfo, "NEWBORN", 2));
				if(newbornInfo.has("IsEMONC")) {
					newbornInformation.put(rs.getString(dynamicQueryBuilder.getColumn("NEWBORN_EXTENSION_childno")),
							new JsonHandler().getResponse(rs, newbornInformation.getJSONObject(""+cnt), "NEWBORN_EXTENSION", 1));
				}
				newbornInformation.put("hasNewbornInfo", "Yes");		
				
				cnt = cnt + 1;
			}
			
			if(!rs.isClosed()){
				rs.close();
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return newbornInformation;
	}
}