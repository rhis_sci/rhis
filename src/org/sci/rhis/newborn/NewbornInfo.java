package org.sci.rhis.newborn;

import org.json.JSONObject;
import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.db.DBOperation;
import org.sci.rhis.db.QueryBuilder;
import org.sci.rhis.db.UpzDBSelector;
import org.sci.rhis.util.CheckSystemEntryDate;

/**
 * @author sabah.mugab
 * @since July, 2015
 */
public class NewbornInfo {

	public static JSONObject getDetailInfo(JSONObject newbornInfo) {
		
		JSONObject newbornInformation = new JSONObject();
		boolean newbornResult = false;
		DBOperation dbOp = new DBOperation();
		DBInfoHandler dbObject = new UpzDBSelector().upzDBInfo(newbornInfo);
		QueryBuilder dynamicQueryBuilder = new QueryBuilder();
		
		try{
			if(newbornInfo.get("newbornLoad").equals("")){
//				newbornResult = InsertNewbornInfo.createNewborn(dbOp, dbObject, newbornInfo, dynamicQueryBuilder);

				//check if systementrydate exist in requestjson and DB to remove duplicate entry
				if (newbornInfo.has("systemEntryDate") && !CheckSystemEntryDate.systemEntryDateExist(newbornInfo, dbOp, dbObject, "NEWBORN","NEWBORN_systemEntryDate","NEWBORN_healthid")) {
					newbornResult = InsertNewbornInfo.createNewborn(dbOp, dbObject, newbornInfo, dynamicQueryBuilder);
				}else if(!newbornInfo.has("systemEntryDate")){
					newbornResult = InsertNewbornInfo.createNewborn(dbOp, dbObject, newbornInfo, dynamicQueryBuilder);
				}
				newbornInformation.put("operation", "insert");
			}
			else if(newbornInfo.get("newbornLoad").equals("update")){
				newbornResult = UpdateNewbornInfo.updateNewborn(dbOp, dbObject, newbornInfo, dynamicQueryBuilder);
				newbornInformation.put("operation", "update");
			}
			else if(newbornInfo.get("newbornLoad").equals("delete")){
				newbornResult = DeleteNewbornInfo.deleteNewborn(dbOp, dbObject, newbornInfo, dynamicQueryBuilder);
				newbornInformation.put("operation", "delete");
			}
			else{
				newbornInformation = RetrieveNewbornInfo.getNewbornInfo(dbOp, dbObject, newbornInfo,newbornInformation, dynamicQueryBuilder);
				newbornResult = true;
				newbornInformation.put("operation", "retrieve");
			}
			newbornInformation.put("result", newbornResult);
		}
		catch(Exception e){
			e.printStackTrace();
		}
		finally{
			dbOp.dbObjectNullify(dbObject);
		}
		return newbornInformation;
	}
}