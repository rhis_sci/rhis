package org.sci.rhis.newborn;

import org.json.JSONObject;
import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.db.DBOperation;
import org.sci.rhis.db.QueryBuilder;
import org.sci.rhis.util.JsonHandler;

/**
 * @author sabah.mugab
 * @since February, 2017
 */
public class UpdateNewbornInfo {
	
	static boolean status;
	
	public static boolean updateNewborn(DBOperation dbOp, DBInfoHandler dbObject, JSONObject newbornInfo, QueryBuilder dynamicQueryBuilder) {
		
		try{
			status = dbOp.dbStatementExecute(dbObject,dynamicQueryBuilder.getUpdateQuery(new JsonHandler().addJsonKeyValueEdit(newbornInfo, "NEWBORN")));
			//functionalities for emonc register
			if(newbornInfo.has("IsEMONC")) {
				status = dbOp.dbStatementExecute(dbObject, dynamicQueryBuilder.getUpdateQuery(new JsonHandler().addJsonKeyValueEdit(newbornInfo, "NEWBORN_EXTENSION")));
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return status;		
	}
}