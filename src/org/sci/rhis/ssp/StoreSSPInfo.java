package org.sci.rhis.ssp;

import java.sql.ResultSet;
import java.util.Iterator;

import org.json.JSONObject;
import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.db.DBOperation;
import org.sci.rhis.db.QueryBuilder;
import org.sci.rhis.util.JsonHandler;

public class StoreSSPInfo {

    @SuppressWarnings("unchecked")
    public static JSONObject insertInfo(JSONObject sspInfo, JSONObject sspInformation, DBOperation dbOp,
                                        DBInfoHandler dbObject, QueryBuilder dynamicQueryBuilder) {

        try {
            String key_planning = "";
            String key_detail = "";
            int success = 0;

            Iterator<String> keys_planning = sspInfo.getJSONObject("planning").keys();
            sspInformation.put("success", "1");
            while (keys_planning.hasNext()) {
                key_planning = keys_planning.next();
                //System.out.println(dynamicQueryBuilder.getUpsertQuery(new JsonHandler().addJsonKeyValueEdit(sspInfo.getJSONObject("planning").getJSONObject(key_planning).getJSONObject("main"),"SSP"),"SSP"));
                //success = dbOp.dbStatementExecuteUpdate(dbObject,
                //	dynamicQueryBuilder.getUpsertQuery(new JsonHandler().addJsonKeyValueEdit(sspInfo.getJSONObject("planning").getJSONObject(key_planning).getJSONObject("main"),"SSP"),"SSP"));
                ResultSet rs = dbOp.dbExecute(dbObject, dynamicQueryBuilder.getUpsertQuery(
                        new JsonHandler().addJsonKeyValueEdit(sspInfo.getJSONObject("planning").getJSONObject(key_planning).
                                getJSONObject("main"), "SSP"), "SSP")).getResultSet();
                rs.last();
                success = rs.getRow();
                if (success == 0) {
                    sspInformation.put("success", "0");
                }

                Iterator<String> keys_sspDetail = sspInfo.getJSONObject("planning").getJSONObject(key_planning).getJSONObject("detail").keys();


                //delet 1st for Satelite session detail
//                dbOp.dbExecute(dbObject,"delete FROM \"satelite_planning_detail\" WHERE \"planning_id\"=20210");
                dbOp.dbStatementExecute(dbObject, dynamicQueryBuilder.getDeleteQuery
                        (new JsonHandler().addJsonKeyValueEdit(sspInfo.getJSONObject("planning").getJSONObject(key_planning).
                                getJSONObject("main"), "SSD", false)));
                while (keys_sspDetail.hasNext()) {
                    key_detail = keys_sspDetail.next();
                    //System.out.println(dynamicQueryBuilder.getUpsertQuery(new JsonHandler().addJsonKeyValueEdit(sspInfo.getJSONObject("planning").getJSONObject(key_planning).getJSONObject("detail").getJSONObject(key_detail),"SSD"),"SSD"));
                    //success = dbOp.dbStatementExecuteUpdate(dbObject,
                    //dynamicQueryBuilder.getUpsertQuery(new JsonHandler().addJsonKeyValueEdit(sspInfo.getJSONObject("planning").getJSONObject(key_planning).getJSONObject("detail").getJSONObject(key_detail),"SSD"),"SSD"));

                    //used upsert instead of insert as it works already.so didnt write new insert
                    rs = dbOp.dbExecute(dbObject, dynamicQueryBuilder.getUpsertQuery(
                            new JsonHandler().addJsonKeyValueEdit(sspInfo.getJSONObject("planning").getJSONObject(key_planning).
                                    getJSONObject("detail").getJSONObject(key_detail), "SSD"), "SSD")).getResultSet();
                    rs.last();
                    success = rs.getRow();
                    if (success == 0) {
                        sspInformation.put("success", "0");
                    }
                }
                if (!rs.isClosed()) {
                    rs.close();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            sspInformation.put("success", "0");
        }
        return sspInformation;
    }
}