package org.sci.rhis.ssp;

import java.sql.ResultSet;

import org.json.JSONObject;
import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.db.DBOperation;
import org.sci.rhis.db.QueryBuilder;
import org.sci.rhis.util.CalendarDate;
import org.sci.rhis.util.JsonHandler;

/**
 * @author sabah.mugab
 * @since July, 2019
 */
public class RetrieveSSPInfo {

	public static JSONObject getSSPInfo(JSONObject sspInfo, DBOperation dbOp,
			DBInfoHandler dbObject, QueryBuilder dynamicQueryBuilder) {
		
		JSONObject sspInformation = new JSONObject();
		String SSP_status = "3"; //Pending = 3
		try{
			String sql = "SELECT * FROM " + dynamicQueryBuilder.getTable("SSP") + " "
						+ "WHERE " + dynamicQueryBuilder.getColumn("table", "SSP_providerid",new String[]{sspInfo.getString("provider_id")},"=")
						+ " AND " + dynamicQueryBuilder.getColumn("table", "SSP_status",new String[]{SSP_status},"!=")
						+ " AND " + dynamicQueryBuilder.getColumn("table", "SSP_year",new String[]{CalendarDate.getCurrentYear()},"=");
			
			ResultSet rs = dbOp.dbExecute(dbObject,sql).getResultSet();

			//set this to get db properties as we search by "FACILITY_"+zillaid
			sspInfo.put("zillaid",sspInfo.getString("facility_zillaid"));

			while(rs.next()){
				sspInformation.put(rs.getString(dynamicQueryBuilder.getColumn("SSP_planning_id")), 
						new JsonHandler().getServiceDetail(rs, sspInfo, "SSP", dynamicQueryBuilder, 2));
			}			
			
			if(!rs.isClosed()){
				rs.close();
			}

			//remove this to back previous state
			sspInfo.remove("zillaid");
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return sspInformation;
	}
}