package org.sci.rhis.ssp;

import org.json.JSONObject;

import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.db.DBOperation;
import org.sci.rhis.db.QueryBuilder;
import org.sci.rhis.db.UpzDBSelector;

/**
 * @author sabah.mugab
 * @since July, 2019
 */
public class SSPlanning {
		
	public static JSONObject getDetailInfo(JSONObject sspInfo) {
		String upazilaid = null;
		DBOperation dbOp = new DBOperation();
		//for ssp it need to save info in zillaDB so removed upazilaid from json
		if(sspInfo.has("upazilaid")) {
			upazilaid = sspInfo.getString("upazilaid");
			sspInfo.remove("upazilaid");
		}

		DBInfoHandler dbObject = new UpzDBSelector().upzDBInfo(sspInfo);
		QueryBuilder dynamicQueryBuilder = new QueryBuilder();
				
		try{
			JSONObject sspInformation = new JSONObject();
			
			if(sspInfo.get("operation").equals("insert")){
				sspInfo.put("upazilaid",upazilaid);
				sspInformation = StoreSSPInfo.insertInfo(sspInfo, sspInformation, dbOp, dbObject, dynamicQueryBuilder);
			}

			return sspInformation;
		}
		catch(Exception e){
			e.printStackTrace();
			return new JSONObject();
		}
		finally{
			dbOp.dbObjectNullify(dbObject);			
		}		
	}
}