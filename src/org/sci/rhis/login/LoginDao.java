package org.sci.rhis.login;

import java.sql.ResultSet;
import java.util.Arrays;

import org.json.JSONObject;
import org.sci.rhis.db.*;
import org.sci.rhis.ssp.RetrieveSSPInfo;
import org.sci.rhis.util.CalendarDate;
import org.sci.rhis.util.JsonHandler;

/**
 * @author sabah.mugab
 * @since June, 2015
 */
public class LoginDao {

    public JSONObject validate(JSONObject loginCred) {

        JSONObject response = new JSONObject();
        response.put("status", false);

        DBOperation dbOp = new DBOperation();
        //for common webservice we pass zillaid during login. so need to exclude zillaid = null
        if(!(loginCred.get("loginrequest").equals(true))) {
            loginCred.put("providerType", null);
            loginCred.put("zillaid", null);
            loginCred.put("upazilaid", null);
        }

        DBInfoHandler dbObject = new UpzDBSelector().upzDBInfo(loginCred), dbObject1 = new UpzDBSelector().upzDBInfo(loginCred);
        QueryBuilder dynamicQueryBuilder = new QueryBuilder();
        JsonHandler resultSet = new JsonHandler();

        try {
            String sql = "SELECT " + dynamicQueryBuilder.getColumn("fr", "FACILITY_facilityid") + " AS facility_id,"
                    + dynamicQueryBuilder.getColumn("fr", "FACILITY_facilityname") + " AS facility_name,"
                    + dynamicQueryBuilder.getColumn("fr", "FACILITY_zillaid") + " AS facility_zillaid,"
                    + dynamicQueryBuilder.getColumn("fr", "FACILITY_upazilaid") + " AS facility_upazilaid,"
                    + dynamicQueryBuilder.getColumn("fr", "FACILITY_unionid") + " AS facility_unionid,"
                    + dynamicQueryBuilder.getColumn("fr", "FACILITY_facilitytype") + " AS facility_type,"
                    + dynamicQueryBuilder.getColumn("fp", "PROVIDERFACILITY_providerid") + ","
                    + dynamicQueryBuilder.getColumn("pdb", "PROVIDER_providername") + ","
                    + dynamicQueryBuilder.getColumn("pdb", "PROVIDER_providertype") + ","
                    + dynamicQueryBuilder.getColumn("pdb", "PROVIDER_providerid") + " AS pid,"
                    + dynamicQueryBuilder.getColumn("pdb", "PROVIDER_providerassignedfacility") + ","
                    + dynamicQueryBuilder.getColumn("pdb", "PROVIDER_provideriscsba") + ","
                    + dynamicQueryBuilder.getColumn("pdb", "PROVIDER_divid") + " AS \"divid\","
                    + dynamicQueryBuilder.getColumn("pdb", "PROVIDER_zillaid") + ","
                    + dynamicQueryBuilder.getColumn("pdb", "PROVIDER_upazilaid") + ","
                    + dynamicQueryBuilder.getColumn("pdb", "PROVIDER_unionid") + ","
                    + dynamicQueryBuilder.getColumn("nd", "PROVIDERNODE_nodeid") + ","
                    + dynamicQueryBuilder.getColumn("nd", "PROVIDERNODE_clienttype") + ","
                    + dynamicQueryBuilder.getColumn("nd", "PROVIDERNODE_servernodeid") + ","
                    + dynamicQueryBuilder.getColumn("nd", "PROVIDERNODE_appversion") + ","
                    + dynamicQueryBuilder.getColumn("nd", "PROVIDERNODE_webserviceurl") + ","
                    + dynamicQueryBuilder.getColumn("nd", "PROVIDERNODE_downloadstatus") + ","
                    + dynamicQueryBuilder.getColumn("nd", "PROVIDERNODE_syncrequest") + ","
                    + dynamicQueryBuilder.getColumn("nd", "PROVIDERNODE_changerequest") + ","
                    + dynamicQueryBuilder.getColumn("nd", "PROVIDERNODE_syncurl") + ","
                    + dynamicQueryBuilder.getColumn("nd", "PROVIDERNODE_communityactive") + " "
                    + "FROM " + dynamicQueryBuilder.getTable("PROVIDER") + " AS pdb "
                    + "INNER JOIN " + dynamicQueryBuilder.getTable("PROVIDERNODE") + " AS nd ON "
                    + dynamicQueryBuilder.getPartialCondition("nd", "PROVIDERNODE_providerid", "pdb", "PROVIDER_providerid", "=") + " "
                    + "LEFT JOIN " + dynamicQueryBuilder.getTable("PROVIDERFACILITY") + " AS fp ON " + dynamicQueryBuilder.getColumn("fp", "PROVIDERFACILITY_providerid") + " IN (?) "
                    + "OR " + dynamicQueryBuilder.getColumn("fp", "PROVIDERFACILITY_providerid") + " IN "
                    + "(SELECT " + dynamicQueryBuilder.getColumn("", "ASSOCIATEDPROVIDER_providerassociatedid") + " "
                    + "FROM " + dynamicQueryBuilder.getTable("ASSOCIATEDPROVIDER") + " WHERE " + dynamicQueryBuilder.getColumn("", "ASSOCIATEDPROVIDER_providerid") + "=?) "
                    + "LEFT JOIN " + dynamicQueryBuilder.getTable("FACILITY") + " AS fr ON "
                    + dynamicQueryBuilder.getColumn("fr", "FACILITY_facilityid") + "=" + dynamicQueryBuilder.getColumn("fp", "PROVIDERFACILITY_facilityid") + " "
                    + "WHERE " + dynamicQueryBuilder.getColumn("pdb", "PROVIDER_providerid") + "=? AND "
                    + dynamicQueryBuilder.getColumn("pdb", "PROVIDER_providerpassword") + "=? AND "
                    + dynamicQueryBuilder.getColumn("pdb", "PROVIDER_providerisactive", new String[]{"1"}, "=") + " AND ("
                    + dynamicQueryBuilder.getColumn("fp", "PROVIDERFACILITY_isactive", new String[]{"1"}, "=") + " OR "
                    + dynamicQueryBuilder.getColumn("pdb", "PROVIDER_provideriscsba", new String[]{"1"}, "=") + ")";

            System.out.println(sql);
            dbObject = dbOp.dbCreatePreparedStatement(dbObject, sql);

            int num = 0;
            try {
                num = loginCred.getInt("uid");
                dbObject.getPreparedStatement().setInt(1, num);
                dbObject.getPreparedStatement().setInt(2, num);
                dbObject.getPreparedStatement().setInt(3, num);
            } catch (NumberFormatException e) {
                dbObject.getPreparedStatement().setInt(1, num);
                dbObject.getPreparedStatement().setInt(2, num);
                dbObject.getPreparedStatement().setInt(3, num);
            }

            dbObject.getPreparedStatement().setString(4, loginCred.getString("upass"));

            ResultSet rs = dbOp.dbExecute(dbObject).getResultSet();
            JSONObject associated_id_details = new JSONObject();
            associated_id_details.put("facility_count", String.valueOf(0));


            while (rs.next()) {
                String changeRequestValue = resultSet.getResultSetValue(rs, dynamicQueryBuilder.getColumn("PROVIDERNODE_changerequest"));
                response.put("status", true);
                response.put("pID", rs.getInt("pid"));//pID = rs.getInt("pid");
                response.put("pName", resultSet.getResultSetValue(rs, dynamicQueryBuilder.getColumn("PROVIDER_providername")));//pName = resultSet.getResultSetValue(rs,dynamicQueryBuilder.getColumn("PROVIDER_providername"));
                response.put("pType", resultSet.getResultSetValue(rs, dynamicQueryBuilder.getColumn("PROVIDER_providertype")));//pType = resultSet.getResultSetValue(rs,dynamicQueryBuilder.getColumn("PROVIDER_providertype"));
                response.put("facilityName", resultSet.getResultSetValue(rs, dynamicQueryBuilder.getColumn("PROVIDER_providerassignedfacility")));//facilityName = resultSet.getResultSetValue(rs,dynamicQueryBuilder.getColumn("PROVIDER_providerassignedfacility"));
                response.put("csba", Integer.valueOf(resultSet.getResultSetValue(rs, dynamicQueryBuilder.getColumn("PROVIDER_provideriscsba"), "2")));//csba = Integer.valueOf(resultSet.getResultSetValue(rs,dynamicQueryBuilder.getColumn("PROVIDER_provideriscsba"),"2"));
                response.put("divid", resultSet.getResultSetValue(rs, "divid").equals("") ? "" : String.format("%02d", Integer.valueOf(resultSet.getResultSetValue(rs, "divid"))));//divid = resultSet.getResultSetValue(rs,"divid").equals("")?"":String.format("%02d", Integer.valueOf(resultSet.getResultSetValue(rs,"divid")));
                response.put("zillaid", resultSet.getResultSetValue(rs, dynamicQueryBuilder.getColumn("PROVIDER_zillaid")).equals("") ? "" : String.format("%02d", Integer.valueOf(resultSet.getResultSetValue(rs, dynamicQueryBuilder.getColumn("PROVIDER_zillaid")))));//zillaid = resultSet.getResultSetValue(rs,dynamicQueryBuilder.getColumn("PROVIDER_zillaid")).equals("")?"":String.format("%02d", Integer.valueOf(resultSet.getResultSetValue(rs,dynamicQueryBuilder.getColumn("PROVIDER_zillaid"))));
                response.put("upazilaid", resultSet.getResultSetValue(rs, dynamicQueryBuilder.getColumn("PROVIDER_upazilaid")).equals("") ? "" : String.format("%02d", Integer.valueOf(resultSet.getResultSetValue(rs, dynamicQueryBuilder.getColumn("PROVIDER_upazilaid")))));//upazilaid = resultSet.getResultSetValue(rs,dynamicQueryBuilder.getColumn("PROVIDER_upazilaid")).equals("")?"":String.format("%02d", Integer.valueOf(resultSet.getResultSetValue(rs,dynamicQueryBuilder.getColumn("PROVIDER_upazilaid"))));
                response.put("unionid", resultSet.getResultSetValue(rs, dynamicQueryBuilder.getColumn("PROVIDER_unionid")).equals("") ? "" : String.format("%02d", Integer.valueOf(resultSet.getResultSetValue(rs, dynamicQueryBuilder.getColumn("PROVIDER_unionid")))));//unionid = resultSet.getResultSetValue(rs,dynamicQueryBuilder.getColumn("PROVIDER_unionid")).equals("")?"":String.format("%02d", Integer.valueOf(resultSet.getResultSetValue(rs,dynamicQueryBuilder.getColumn("PROVIDER_unionid"))));
                response.put("id", resultSet.getResultSetValue(rs, dynamicQueryBuilder.getColumn("PROVIDERNODE_nodeid")));//id = resultSet.getResultSetValue(rs,dynamicQueryBuilder.getColumn("PROVIDERNODE_nodeid"));
                response.put("client", Integer.valueOf(resultSet.getResultSetValue(rs, dynamicQueryBuilder.getColumn("PROVIDERNODE_clienttype"), "2")));//client = Integer.valueOf(resultSet.getResultSetValue(rs,dynamicQueryBuilder.getColumn("PROVIDERNODE_clienttype"),"2"));
                response.put("server", resultSet.getResultSetValue(rs, dynamicQueryBuilder.getColumn("PROVIDERNODE_servernodeid")));//server = resultSet.getResultSetValue(rs,dynamicQueryBuilder.getColumn("PROVIDERNODE_servernodeid"));
                response.put("version", resultSet.getResultSetValue(rs, dynamicQueryBuilder.getColumn("PROVIDERNODE_appversion")));//version = resultSet.getResultSetValue(rs,dynamicQueryBuilder.getColumn("PROVIDERNODE_appversion"));
                response.put("base_url", resultSet.getResultSetValue(rs, dynamicQueryBuilder.getColumn("PROVIDERNODE_webserviceurl")));//base_url = resultSet.getResultSetValue(rs,dynamicQueryBuilder.getColumn("PROVIDERNODE_webserviceurl"));
                response.put("sync_url", resultSet.getResultSetValue(rs, dynamicQueryBuilder.getColumn("PROVIDERNODE_syncurl")));//sync_url = resultSet.getResultSetValue(rs,dynamicQueryBuilder.getColumn("PROVIDERNODE_syncurl"));
                response.put("db_download_status", resultSet.getResultSetValue(rs, dynamicQueryBuilder.getColumn("PROVIDERNODE_downloadstatus"), "0"));//db_download_status = resultSet.getResultSetValue(rs,dynamicQueryBuilder.getColumn("PROVIDERNODE_downloadstatus"),"0");
                response.put("db_sync_request", resultSet.getResultSetValue(rs, dynamicQueryBuilder.getColumn("PROVIDERNODE_syncrequest"), "0"));//db_sync_request = resultSet.getResultSetValue(rs,dynamicQueryBuilder.getColumn("PROVIDERNODE_syncrequest"),"0");
                response.put("change_request", resultSet.getResultSetValue(rs, dynamicQueryBuilder.getColumn("PROVIDERNODE_changerequest")));//change_request = resultSet.getResultSetValue(rs,dynamicQueryBuilder.getColumn("PROVIDERNODE_changerequest"));
                response.put("community_active", resultSet.getResultSetValue(rs, dynamicQueryBuilder.getColumn("PROVIDERNODE_communityactive")));//change_request = resultSet.getResultSetValue(rs,dynamicQueryBuilder.getColumn("PROVIDERNODE_changerequest"));

                JSONObject detail = new JSONObject();
                detail.put("provider_id", resultSet.getResultSetValue(rs, dynamicQueryBuilder.getColumn("PROVIDERFACILITY_providerid")));
                detail.put("facility_id", resultSet.getResultSetValue(rs, "facility_id"));
                detail.put("facility_name", resultSet.getResultSetValue(rs, "facility_name"));
                detail.put("facility_zillaid", resultSet.getResultSetValue(rs, "facility_zillaid"));
                detail.put("facility_upazilaid", resultSet.getResultSetValue(rs, "facility_upazilaid"));
                detail.put("facility_unionid", resultSet.getResultSetValue(rs, "facility_unionid"));
                detail.put("facility_type", resultSet.getResultSetValue(rs, "facility_type"));

                //which provider have no instance in facility_provider Ex:CSBA
                if (!(detail.get("provider_id").equals(""))) {
                    detail.put("planning", RetrieveSSPInfo.getSSPInfo(detail, dbOp, dbObject1, dynamicQueryBuilder));
                } else {
                    detail.put("planning",new JSONObject());
                }

                associated_id_details.put("facility_count", String.valueOf(associated_id_details.getInt("facility_count") + 1));
                associated_id_details.put(associated_id_details.getString("facility_count"), detail);
                response.put("associated_id_details", associated_id_details);

                if (!changeRequestValue.isEmpty()){

                        String updateChangerequest_sql = "update node_details set changerequest = changerequest::jsonb - 'sql' where providerid=" + response.get("pID") +" and changerequest::jsonb ? 'sql'";
                        dbOp.dbStatementExecute(dbObject, updateChangerequest_sql);

                }

                //we are setting dbsync req  0
                if (response.has("db_sync_request")){
                    if(!(Arrays.asList(new String[]{"0", "1", "2"}).contains(response.getString("db_sync_request")))){
                        String synreq_sql = "update node_details set dbsyncrequest= 0 where providerid=" + response.get("pID");
                        dbOp.dbStatementExecute(dbObject, synreq_sql);
                        response.put("db_sync_request",response.getString("db_sync_request"));

                    }
                }
            }

            if (!rs.isClosed()) {
                rs.close();
            }

            /**
             * inserting currentdatetimewithmillsec in login_time column to
             * fix 'duplicate key value violates unique constraint "last_login_detail_pkey"'
             */
            sql = "INSERT INTO " + dynamicQueryBuilder.getTable("LOGININFO") + "("
                    + dynamicQueryBuilder.getColumn("LOGININFO_providerid") + ","
                    + dynamicQueryBuilder.getColumn("LOGININFO_logintime") + ","
                    + dynamicQueryBuilder.getColumn("LOGININFO_loginrequest") + ","
                    + dynamicQueryBuilder.getColumn("LOGININFO_loginsuccess")
                    + ") "
                    + "VALUES ("
                    + loginCred.getInt("uid") + ","
                    + "'" + CalendarDate.getCurrentDateTimeWithMill() + "',"
                    + "'" + loginCred + "',"
                    + Boolean.valueOf(response.getString("status")) + ")";
            System.out.println(sql);

            if (dbOp.dbStatementExecute(dbObject1, sql)) {
                return response;
            } else {
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            dbOp.dbObjectNullify(dbObject);
            dbOp.dbObjectNullify(dbObject1);
        }
    }
}