package org.sci.rhis.womaninjectable;

import java.sql.ResultSet;

import org.json.JSONObject;
import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.db.DBOperation;
import org.sci.rhis.db.QueryBuilder;
import org.sci.rhis.util.JsonHandler;

/**
 * @author sabah.mugab
 * @since February, 2016
 */
public class RetrieveInjectableVisitInfo {

	public static JSONObject getInjectableVisits(DBOperation dbOp, DBInfoHandler dbObject, 
						JSONObject injectableInfo, JSONObject injectableVisits, QueryBuilder dynamicQueryBuilder) {
		
		try{
			String sql = "SELECT " + dynamicQueryBuilder.getColumn("table", "WOMANINJECTABLE_providerId") + ", "
						+ dynamicQueryBuilder.getColumn("table", "WOMANINJECTABLE_doseId") + ", "
						+ dynamicQueryBuilder.getColumn("table", "WOMANINJECTABLE_doseDate") + ", "
						+ dynamicQueryBuilder.getColumn("table", "WOMANINJECTABLE_injectionId") + ", "
						+ dynamicQueryBuilder.getColumn("table", "WOMANINJECTABLE_isNewClient") + ", "
						+ dynamicQueryBuilder.getTable("FPEXAMINATION_WOMANINJECTABLE") + ".*"
						+ " FROM " + dynamicQueryBuilder.getTable("WOMANINJECTABLE")
						+ " LEFT JOIN " + dynamicQueryBuilder.getTable("FPEXAMINATION_WOMANINJECTABLE") + " ON "
						+ dynamicQueryBuilder.getPartialCondition("table", "FPEXAMINATION_WOMANINJECTABLE_healthId","table","WOMANINJECTABLE_healthId","=") + " AND "
						+ dynamicQueryBuilder.getPartialCondition("table", "FPEXAMINATION_WOMANINJECTABLE_serviceId","table","WOMANINJECTABLE_doseId","=") + " AND "
						+ dynamicQueryBuilder.getColumn("table", "FPEXAMINATION_WOMANINJECTABLE_FPType",new String[]{injectableInfo.getString("FPType")},"=")
						+ " WHERE " + dynamicQueryBuilder.getColumn("table", "WOMANINJECTABLE_healthId",new String[]{injectableInfo.getString("healthId")},"=")
						+ " ORDER BY " + dynamicQueryBuilder.getColumn("table", "WOMANINJECTABLE_doseDate") + " ASC";
			
			System.out.println(sql);
			
			ResultSet rs = dbOp.dbExecute(dbObject,sql).getResultSet();
			injectableVisits.put("count", 0);
			injectableInfo.put("distributionJson","treatment");
						
			while(rs.next()){
				injectableVisits.put("count", (injectableVisits.getInt("count")+1));
				
				injectableVisits.put(injectableVisits.getString("count"), 
						new JsonHandler().getResponse(rs, new JsonHandler().getServiceDetail(rs, 
								injectableInfo, "FPEXAMINATION_WOMANINJECTABLE", dynamicQueryBuilder, 2), "WOMANINJECTABLE", 1));							
			}
			
			if(!rs.isClosed()){
				rs.close();
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}					
		return injectableVisits;
	}
}