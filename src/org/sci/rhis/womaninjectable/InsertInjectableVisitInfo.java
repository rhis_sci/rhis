package org.sci.rhis.womaninjectable;

import java.sql.ResultSet;

import org.json.JSONObject;
import org.sci.rhis.client.ClientInfoUtil;
import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.db.DBOperation;
import org.sci.rhis.db.QueryBuilder;
import org.sci.rhis.distribution.StockDistributionRequest;
import org.sci.rhis.util.JsonHandler;

/**
 * @author sabah.mugab
 * @since February, 2016
 */
public class InsertInjectableVisitInfo {
	
	public static JSONObject createInjectableVisit(DBOperation dbOp, DBInfoHandler dbObject, 
									JSONObject injectableInfo, QueryBuilder dynamicQueryBuilder) {
		try{
			injectableInfo.put("sideEffectCore",injectableInfo.get("sideEffect").equals("")? 2 : 1);
	        
			ResultSet rs = dbOp.dbExecute(dbObject,dynamicQueryBuilder.getInsertQuery
	        		(new JsonHandler().addJsonKeyValueEdit(new JsonHandler().addJsonKeyIncrementalField
							(injectableInfo,"doseId"), "WOMANINJECTABLE"))).getResultSet();
	        
	        if(rs.next()){
	           	injectableInfo.put("serviceId",rs.getObject(dynamicQueryBuilder.getColumn("WOMANINJECTABLE_doseId")));
	           	dbOp.dbStatementExecute(dbObject,dynamicQueryBuilder.getInsertQuery(
	           			new JsonHandler().addJsonKeyValueEdit(new JsonHandler().addJsonKeyIncrementalField
								(injectableInfo,""), "FPEXAMINATION_WOMANINJECTABLE")));
	        	
	        	injectableInfo.put("isNewClient", 1);
	        	injectableInfo.put("currentMethod", 3);
	        	dbOp.dbStatementExecute(dbObject,dynamicQueryBuilder.getUpdateQuery(
	        			new JsonHandler().addJsonKeyValueEdit(injectableInfo, "FPSTATUS")));
	        	StockDistributionRequest.insertDistributionInfoHandler(true, injectableInfo, dbOp, dbObject, dynamicQueryBuilder);
			}
	        else{
	        	injectableInfo.put("serviceId","");
	        }
	        			
			if(!rs.isClosed()){
				rs.close();
			}
			if(!injectableInfo.get("mobileNo").equals("")){			
				ClientInfoUtil.updateClientMobileNo(injectableInfo, dbOp, dbObject);
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}						
		return injectableInfo;		
	}
}