package org.sci.rhis.womaninjectable;

import java.sql.ResultSet;

import org.json.JSONObject;
import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.db.DBOperation;
import org.sci.rhis.db.QueryBuilder;
import org.sci.rhis.distribution.StockDistributionRequest;
import org.sci.rhis.util.JsonHandler;

/**
 * @author sabah.mugab
 * @since March,2017
 */
public class UpdateInjectableVisitInfo {

	public static boolean updateInjectableVisit(DBOperation dbOp, DBInfoHandler dbObject, 
								JSONObject injectableInfo, QueryBuilder dynamicQueryBuilder) {
		try{			
			injectableInfo.put("sideEffectCore",injectableInfo.get("sideEffect").equals("")? 2 : 1);
			dbOp.dbStatementExecute(dbObject,dynamicQueryBuilder.getUpdateQuery(
					new JsonHandler().addJsonKeyValueEdit(injectableInfo, "WOMANINJECTABLE")));
			
			injectableInfo.put("serviceId",injectableInfo.get("doseId"));
			injectableInfo.put("isNewClient", 1);
        	injectableInfo.put("currentMethod", 3);
        	String returningSql = " RETURNING *";
        	ResultSet rs = dbOp.dbExecute(dbObject,dynamicQueryBuilder.getUpdateQuery(
					new JsonHandler().addJsonKeyValueEdit(injectableInfo, "FPEXAMINATION_WOMANINJECTABLE")) + returningSql).getResultSet();
        	StockDistributionRequest.updateDistributionInfoHandler(rs, 
        			dynamicQueryBuilder.getColumn("FPEXAMINATION_WOMANINJECTABLE_treatment"), 
        			injectableInfo, dbOp, dbObject, dynamicQueryBuilder);
			dbOp.dbStatementExecute(dbObject,dynamicQueryBuilder.getUpdateQuery(
					new JsonHandler().addJsonKeyValueEdit(injectableInfo, "FPSTATUS")));
			if(!rs.isClosed()){
				rs.close();
			}
			return true;
		}
		catch(Exception e){
			e.printStackTrace();
			return false;
		}		
	}
}