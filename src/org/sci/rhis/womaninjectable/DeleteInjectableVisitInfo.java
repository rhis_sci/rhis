package org.sci.rhis.womaninjectable;

import java.sql.ResultSet;

import org.json.JSONObject;
import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.db.DBOperation;
import org.sci.rhis.db.QueryBuilder;
import org.sci.rhis.fp.common.FPOp;
import org.sci.rhis.util.JsonHandler;

/**
 * @author sabah.mugab
 * @since January,2016
 */

public class DeleteInjectableVisitInfo {

	public static boolean deleteInjectableVisit(DBOperation dbOp, DBInfoHandler dbObject, 
								JSONObject injectableInfo, QueryBuilder dynamicQueryBuilder) {
		try{
			String returningSql = " RETURNING *";
			ResultSet rs = dbOp.dbExecute(dbObject,dynamicQueryBuilder.getDeleteQuery
					(new JsonHandler().addJsonKeyValueEdit(new JsonHandler().addJsonKeyMaxField
							(injectableInfo,"doseId"), "WOMANINJECTABLE")) + returningSql).getResultSet();
			
	        if(rs.next()){
	        	injectableInfo.put("serviceId",rs.getObject(dynamicQueryBuilder.getColumn("WOMANINJECTABLE_doseId")));
	        	FPOp.deleteFPExamination(injectableInfo, dbOp, dbObject, dynamicQueryBuilder,
	        				dynamicQueryBuilder.getColumn("FPEXAMINATION_WOMANINJECTABLE_treatment"));
	        	
	        	injectableInfo.put("isNewClient", 2);
	        	injectableInfo.put("currentMethod", "");
	        	dbOp.dbStatementExecute(dbObject,new QueryBuilder().getUpdateQuery(
	        			new JsonHandler().addJsonKeyValueEdit(injectableInfo, "FPSTATUS")));
			}
	        else{
	        	injectableInfo.put("serviceId","");
	        }
	        			
			if(!rs.isClosed()){
				rs.close();
			}
			return true;
		}
		catch(Exception e){
			e.printStackTrace();
			return false;
		}	
	}
}