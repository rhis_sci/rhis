package org.sci.rhis.womaninjectable;

import org.json.JSONObject;
import org.sci.rhis.client.ClientInfoUtil;
import org.sci.rhis.client.CreateRegNo;
import org.sci.rhis.db.*;
import org.sci.rhis.util.CheckSystemEntryDate;
import org.sci.rhis.util.JsonHandler;

/**
 * @author sabah.mugab
 * @since February, 2016
 */
public class WomanInjectableService {
	
	final static int injectables = 3; //for registration number
	final static int FPType = 2; //for FPExamination
	final static int WOMENINJECTBLESERVICETYPE = 8; //for item distribution
	
	public static JSONObject getDetailInfo(JSONObject injectableInfo) {
		
		JSONObject injectableVisits = new JSONObject();
		injectableInfo = new JsonHandler().addJsonKeyValueStockDistribution(new JSONKeyMapper().setRequiredKeys(injectableInfo, "FPEXAMINATION_WOMANINJECTABLE"), WOMENINJECTBLESERVICETYPE);
		injectableInfo.put("serviceCategory", injectables);
		injectableInfo.put("FPType", FPType);
		
		DBOperation dbOp = new DBOperation();
		DBInfoHandler dbObject = new UpzDBSelector().upzDBInfo(injectableInfo);
		QueryBuilder dynamicQueryBuilder = new QueryBuilder();
		
		try{
			if(injectableInfo.get("injectableLoad").equals("")){
//				injectableInfo = InsertInjectableVisitInfo.createInjectableVisit(dbOp, dbObject, injectableInfo, dynamicQueryBuilder);

				//check if systementrydate exist in requestjson and DB to remove duplicate entry
				if (injectableInfo.has("systemEntryDate") && !CheckSystemEntryDate.systemEntryDateExist(injectableInfo, dbOp, dbObject, "WOMANINJECTABLE","WOMANINJECTABLE_systemEntryDate","WOMANINJECTABLE_healthId")) {
					injectableInfo = InsertInjectableVisitInfo.createInjectableVisit(dbOp, dbObject, injectableInfo, dynamicQueryBuilder);
				}else if(!injectableInfo.has("systemEntryDate")){
					injectableInfo = InsertInjectableVisitInfo.createInjectableVisit(dbOp, dbObject, injectableInfo, dynamicQueryBuilder);
				}
				if(injectableInfo.getString("serviceId").equals("1")){
					CreateRegNo.pushReg(dbOp, dbObject, injectableInfo, injectableVisits);
				}
			}
			else if(injectableInfo.get("injectableLoad").equals("update")){			
				UpdateInjectableVisitInfo.updateInjectableVisit(dbOp, dbObject, injectableInfo, dynamicQueryBuilder);
			}
			else if(injectableInfo.get("injectableLoad").equals("delete")){			
				DeleteInjectableVisitInfo.deleteInjectableVisit(dbOp, dbObject, injectableInfo, dynamicQueryBuilder);
			}
			injectableVisits = RetrieveInjectableVisitInfo.getInjectableVisits(dbOp, dbObject, injectableInfo,injectableVisits, dynamicQueryBuilder);
			injectableVisits = ClientInfoUtil.getRegNumber(dbOp, dbObject, injectableInfo, injectableVisits);
		}
		catch(Exception e){
			e.printStackTrace();
		}
		finally{
			dbOp.dbObjectNullify(dbObject);
		}
		return injectableVisits;
	}
}