package org.sci.rhis.iud;

import org.json.JSONObject;
import org.sci.rhis.client.ClientInfoUtil;
import org.sci.rhis.client.CreateRegNo;
import org.sci.rhis.db.*;
import org.sci.rhis.iud.followup.DeleteIUDFollowupInfo;
import org.sci.rhis.iud.followup.InsertIUDFollowupInfo;
import org.sci.rhis.iud.followup.RetrieveIUDFollowupInfo;
import org.sci.rhis.iud.followup.UpdateIUDFollowupInfo;
import org.sci.rhis.util.CheckSystemEntryDate;
import org.sci.rhis.util.JsonHandler;

/**
 * @author sabah.mugab
 * @since February, 2016
 */
public class IUDInfo {
	
	final static int iud = 4; //for registration number
	final static int IUDSERVICTYPE = 9; //for item distribution
	final static int IUDFOLLOWUPSERVICTYPE = 10; //for item distribution
	
	public static JSONObject getDetailInfo(JSONObject iudInfo) {
		
		JSONObject iudInformation = new JSONObject();
		iudInfo = new JsonHandler().addJsonKeyValueStockDistribution(new JSONKeyMapper().setRequiredKeys(iudInfo, "IUD"), IUDSERVICTYPE);
		DBOperation dbOp = new DBOperation();
		DBInfoHandler dbObject = new UpzDBSelector().upzDBInfo(iudInfo);
		QueryBuilder dynamicQueryBuilder = new QueryBuilder();
		iudInfo.put("serviceCategory", iud);
		
		try{
			iudInformation = new JSONObject();

			if(iudInfo.get("iudLoad").equals("insert")){
				//check if systementrydate exist in requestjson and DB to remove duplicate entry
				if (iudInfo.has("systemEntryDate") && !CheckSystemEntryDate.systemEntryDateExist(iudInfo, dbOp, dbObject, "IUD","IUD_systemEntryDate","IUD_healthId")) {
					iudInformation = InsertIUDInfo.createIUD(iudInfo, iudInformation, dbOp, dbObject, dynamicQueryBuilder);
				}else if(!iudInfo.has("systemEntryDate")){
					iudInformation = InsertIUDInfo.createIUD(iudInfo, iudInformation, dbOp, dbObject, dynamicQueryBuilder);
				}

				if(iudInformation.getString("iudInsertSuccess").equals("1")){
					CreateRegNo.pushReg(dbOp, dbObject, iudInfo, iudInformation);
				}
			}
			else if(iudInfo.get("iudLoad").equals("update")){
				iudInformation = UpdateIUDInfo.updateIUD(iudInfo, iudInformation, dbOp, dbObject, dynamicQueryBuilder);
			}
			else if(iudInfo.get("iudLoad").equals("retrieve")){
				iudInformation = RetrieveIUDInfo.getIUD(iudInfo, iudInformation, dbOp, dbObject, dynamicQueryBuilder);
			}
			else if(iudInfo.get("iudLoad").equals("")){
				iudInformation.put("iudCount", iudInfo.getString("iudCount"));
				iudInfo.put("serviceType", IUDFOLLOWUPSERVICTYPE);
				
				if(iudInfo.get("iudFollowupLoad").equals("insert")){
					//check if systementrydate exist in requestjson and DB to remove duplicate entry
					if (iudInfo.has("systemEntryDate") && !CheckSystemEntryDate.systemEntryDateExist(iudInfo, dbOp, dbObject, "IUDFOLLOWUP","IUDFOLLOWUP_systemEntryDate","IUDFOLLOWUP_healthId")) {
						InsertIUDFollowupInfo.createIUDFollowup(iudInfo, iudInformation, dbOp, dbObject, dynamicQueryBuilder);
					}else if(!iudInfo.has("systemEntryDate")){
						InsertIUDFollowupInfo.createIUDFollowup(iudInfo, iudInformation, dbOp, dbObject, dynamicQueryBuilder);
					}
//					InsertIUDFollowupInfo.createIUDFollowup(iudInfo, iudInformation, dbOp, dbObject, dynamicQueryBuilder);
				}
				else if(iudInfo.get("iudFollowupLoad").equals("update")){
					UpdateIUDFollowupInfo.updateIUDFollowup(iudInfo, iudInformation, dbOp, dbObject, dynamicQueryBuilder);
				}
				else if(iudInfo.get("iudFollowupLoad").equals("delete")){
					DeleteIUDFollowupInfo.deleteIUDFollowup(iudInfo, iudInformation, dbOp, dbObject, dynamicQueryBuilder);
				}				
			}
			
			if(!iudInformation.getString("iudCount").equals("")){
				iudInformation = RetrieveIUDFollowupInfo.getIUDFollowup(iudInfo, iudInformation, dbOp, dbObject, dynamicQueryBuilder);
			}
			
			iudInformation = ClientInfoUtil.getRegNumber(dbOp, dbObject, iudInfo, iudInformation);
		}
		catch(Exception e){
			e.printStackTrace();
		}
		finally{
			dbOp.dbObjectNullify(dbObject);
		}		
		return iudInformation;
	}
}