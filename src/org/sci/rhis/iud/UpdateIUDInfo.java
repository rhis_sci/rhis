package org.sci.rhis.iud;

import java.sql.ResultSet;

import org.json.JSONObject;
import org.sci.rhis.client.ClientInfoUtil;
import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.db.DBOperation;
import org.sci.rhis.db.QueryBuilder;
import org.sci.rhis.distribution.StockDistributionRequest;
import org.sci.rhis.util.JsonHandler;

/**
 * @author sabah.mugab
 * @since March, 2016
 */
public class UpdateIUDInfo {
	
	public static JSONObject updateIUD(JSONObject iudInfo, JSONObject iudInformation, DBOperation dbOp, 
											DBInfoHandler dbObject, QueryBuilder dynamicQueryBuilder) {
					
		try{      	
			String returningSql = " RETURNING *";
			ResultSet rs = dbOp.dbExecute(dbObject,(dynamicQueryBuilder.getUpdateQuery(
									new JsonHandler().addJsonKeyValueEdit(iudInfo, "IUD")) + returningSql)).getResultSet();
			
			if(rs.next()){
				iudInfo.put("iudUpdateSuccess","1");
	        	iudInfo.put("iudCount",rs.getString(dynamicQueryBuilder.getColumn("IUD_iudCount")));
	        	iudInfo.put("treatment",new JsonHandler().getResultSetValue(rs,dynamicQueryBuilder.getColumn("IUD_treatment")));
	        	
			}
	        else{
	        	iudInfo.put("iudUpdateSuccess","2");
	        	iudInfo.put("iudCount","");
	        	iudInfo.put("treatment","");
	        }
			
			rs.beforeFirst();
			StockDistributionRequest.updateDistributionInfoHandler(rs, dynamicQueryBuilder.getColumn("IUD_treatment"), 
											iudInfo, dbOp, dbObject, dynamicQueryBuilder);
			
			if(!iudInfo.get("mobileNo").equals("")){			
				ClientInfoUtil.updateClientMobileNo(iudInfo, dbOp, dbObject);				
			}
			
			iudInformation = RetrieveIUDInfo.getIUD(iudInfo, iudInformation, dbOp, dbObject, dynamicQueryBuilder);
			
			if(!rs.isClosed()){
				rs.close();
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}	
		return iudInformation;
	}
}