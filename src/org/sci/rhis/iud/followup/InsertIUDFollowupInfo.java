package org.sci.rhis.iud.followup;

import java.sql.ResultSet;

import org.json.JSONObject;
import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.db.DBOperation;
import org.sci.rhis.db.QueryBuilder;
import org.sci.rhis.distribution.StockDistributionRequest;
import org.sci.rhis.util.JsonHandler;

/**
 * @author sabah.mugab
 * @since March, 2016
 */
public class InsertIUDFollowupInfo {
	
	public static boolean createIUDFollowup(JSONObject iudInfo, JSONObject iudInformation, 
											DBOperation dbOp, DBInfoHandler dbObject, QueryBuilder dynamicQueryBuilder) {
					
		try{
			ResultSet rs = dbOp.dbExecute(dbObject,new QueryBuilder().getInsertQuery
					(new JsonHandler().addJsonKeyValueEdit
							(new JsonHandler().addJsonKeyIncrementalField
									(iudInfo,"serviceId"), "IUDFOLLOWUP"))).getResultSet();
			
			if(rs.next()){
				iudInfo.put("iudFollowupInsertSuccess","1");
	        	iudInfo.put("healthId",rs.getString(dynamicQueryBuilder.getColumn("IUDFOLLOWUP_healthId")));
	        	iudInfo.put("iudCount",rs.getString(dynamicQueryBuilder.getColumn("IUDFOLLOWUP_iudCount")));
	        	iudInfo.put("serviceId",rs.getString(dynamicQueryBuilder.getColumn("IUDFOLLOWUP_serviceId")));
	        	iudInfo.put("treatment",rs.getString(dynamicQueryBuilder.getColumn("IUDFOLLOWUP_treatment")));
	        	
	        	StockDistributionRequest.insertDistributionInfoHandler(true, iudInfo, dbOp, dbObject, dynamicQueryBuilder);	        	
			}
	        else{
	        	iudInfo.put("iudFollowupInsertSuccess","2");
	        	iudInfo.put("healthId","");
	        	iudInfo.put("iudCount","");
	        	iudInfo.put("serviceId","");
	        	iudInfo.put("treatment","");
	        }
			
			if(!rs.isClosed()){
				rs.close();
			}
			return true;
		}
		catch(Exception e){
			e.printStackTrace();
			return false;
		}	
	}
}