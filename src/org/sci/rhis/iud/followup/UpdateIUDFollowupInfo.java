package org.sci.rhis.iud.followup;

import java.sql.ResultSet;

import org.json.JSONObject;
import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.db.DBOperation;
import org.sci.rhis.db.QueryBuilder;
import org.sci.rhis.distribution.StockDistributionRequest;
//import org.sci.rhis.distribution.HandleStockDistribution;
import org.sci.rhis.util.JsonHandler;

/**
 * @author sabah.mugab
 * @since March, 2016
 */
public class UpdateIUDFollowupInfo {
	
	public static boolean updateIUDFollowup(JSONObject iudInfo, JSONObject iudInformation, DBOperation dbOp, 
											DBInfoHandler dbObject, QueryBuilder dynamicQueryBuilder) {
					
		try{      	
			String returningSql = " RETURNING " + dynamicQueryBuilder.getColumn("", "IUDFOLLOWUP_treatment");
			ResultSet rs = dbOp.dbExecute(dbObject,(dynamicQueryBuilder.getUpdateQuery(
							new JsonHandler().addJsonKeyValueEdit(iudInfo, "IUDFOLLOWUP")) + returningSql)).getResultSet();
			StockDistributionRequest.updateDistributionInfoHandler(rs, dynamicQueryBuilder.getColumn("IUDFOLLOWUP_treatment"), 
										iudInfo, dbOp, dbObject, dynamicQueryBuilder);
			if(!rs.isClosed()){
				rs.close();
			}
			return true;
		}
		catch(Exception e){
			e.printStackTrace();
			return false;
		}			
	}
}