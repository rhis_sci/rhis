package org.sci.rhis.iud.followup;

import java.sql.ResultSet;

import org.json.JSONObject;
import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.db.DBOperation;
import org.sci.rhis.db.QueryBuilder;
import org.sci.rhis.util.JsonHandler;

/**
 * @author sabah.mugab
 * @since March, 2016
 */
public class RetrieveIUDFollowupInfo {

	public static JSONObject getIUDFollowup(JSONObject iudInfo, JSONObject iudInformation, DBOperation dbOp, 
											DBInfoHandler dbObject, QueryBuilder dynamicQueryBuilder) {
		
		try{      	
			String sql = "SELECT * FROM " + dynamicQueryBuilder.getTable("IUDFOLLOWUP")
						+ " WHERE " + dynamicQueryBuilder.getColumn("table", "IUDFOLLOWUP_healthId",new String[]{iudInfo.getString("healthId")},"=")
						+ " AND " + dynamicQueryBuilder.getColumn("table", "IUDFOLLOWUP_iudCount",new String[]{iudInformation.getString("iudCount")},"=") 
						+ " ORDER BY " + dynamicQueryBuilder.getColumn("table", "IUDFOLLOWUP_serviceId") + " ASC";
			
			ResultSet rs = dbOp.dbExecute(dbObject,sql).getResultSet();
			
			JSONObject IUDFollowupVisits = new JSONObject();
			iudInformation.put("followupCount", 0);
			iudInfo.put("distributionJson","treatment");
			
			while(rs.next()){
				IUDFollowupVisits.put(rs.getString(dynamicQueryBuilder.getColumn("IUDFOLLOWUP_serviceId")), 
							new JsonHandler().getServiceDetail(rs, iudInfo, "IUDFOLLOWUP", dynamicQueryBuilder, 2));
				
				iudInformation.put("followupCount", (iudInformation.getInt("followupCount")+1));				
			}
			iudInformation.put("followup", IUDFollowupVisits);
			
			if(!rs.isClosed()){
				rs.close();
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}	
		return iudInformation;
	}
}