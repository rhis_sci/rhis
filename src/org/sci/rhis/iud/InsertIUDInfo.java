package org.sci.rhis.iud;

import java.sql.ResultSet;

import org.json.JSONObject;
import org.sci.rhis.client.ClientInfoUtil;
import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.db.DBOperation;
import org.sci.rhis.db.QueryBuilder;
import org.sci.rhis.distribution.StockDistributionRequest;
import org.sci.rhis.util.JsonHandler;

/**
 * @author sabah.mugab
 * @since March, 2016
 */
public class InsertIUDInfo {
	
	public static JSONObject createIUD(JSONObject iudInfo, JSONObject iudInformation, DBOperation dbOp, 
										DBInfoHandler dbObject, QueryBuilder dynamicQueryBuilder) {
					
		try{
			ResultSet rs = dbOp.dbExecute(dbObject,dynamicQueryBuilder.getInsertQuery
					(new JsonHandler().addJsonKeyValueEdit
							(new JsonHandler().addJsonKeyIncrementalField
									(iudInfo,"iudCount"), "IUD"))).getResultSet();
						
			if(rs.next()){
				iudInfo.put("iudInsertSuccess","1");
	        	iudInfo.put("healthId",rs.getString(dynamicQueryBuilder.getColumn("IUD_healthId")));
	        	iudInfo.put("iudCount",rs.getString(dynamicQueryBuilder.getColumn("IUD_iudCount")));
	        	
	        	iudInfo.put("isNewClient", 1);
                iudInfo.put("currentMethod", 4);
                dbOp.dbStatementExecute(dbObject,dynamicQueryBuilder.getUpdateQuery(new JsonHandler().addJsonKeyValueEdit(iudInfo, "FPSTATUS")));
	        	
                StockDistributionRequest.insertDistributionInfoHandler(true, iudInfo, dbOp, dbObject, dynamicQueryBuilder);
			}
	        else{
	        	iudInfo.put("iudInsertSuccess","2");
	        	iudInfo.put("healthId","");
	        	iudInfo.put("iudCount","");
	        }
		
			if(!iudInfo.get("mobileNo").equals("")){			
				ClientInfoUtil.updateClientMobileNo(iudInfo, dbOp, dbObject);				
			}
			
			iudInformation = RetrieveIUDInfo.getIUD(iudInfo, iudInformation, dbOp, dbObject, dynamicQueryBuilder);
			
			if(!rs.isClosed()){
				rs.close();
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}			
		return iudInformation;
	}
}