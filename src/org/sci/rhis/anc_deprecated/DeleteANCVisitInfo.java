package org.sci.rhis.anc_deprecated;

import org.json.JSONObject;
import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.db.DBOperation;

/**
 * @author armaan.islam
 * @since November, 2015
 */
public class DeleteANCVisitInfo {

	public static boolean deleteANCVisit(DBOperation dbOp, DBInfoHandler dbObject, JSONObject ANCInfo) {
		
		boolean status=false;
		
		try{		
			String sql="DELETE FROM \"ancService\""
			  	    + " WHERE \"healthId\" = " + ANCInfo.get("healthid")
					+ " AND \"pregNo\" =" + ANCInfo.get("pregNo")
					+ " AND \"serviceId\"="
							+ "(SELECT MAX(\"serviceId\")"
							+ " FROM \"ancService\" "
							+ " WHERE \"healthId\" =" + ANCInfo.get("healthid")
							+ " AND \"pregNo\" =" + ANCInfo.get("pregNo")
							+  ")";		 			

			status = dbOp.dbStatementExecute(dbObject, sql);
		}
		catch(Exception e){
			System.out.println(e);
			e.printStackTrace();
		}
		return status;		
	}
}