package org.sci.rhis.anc_deprecated;

import org.json.JSONObject;
import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.db.DBOperation;
import org.sci.rhis.db.QueryBuilder;
import org.sci.rhis.db.UpzDBSelector;
import org.sci.rhis.anc_updated.DeleteANCVisitInfo;
import org.sci.rhis.anc_updated.RetrieveANCVisitInfo;

/**
 * @author sabah.mugab
 * @since June, 2015
 */
public class ANCVisit {
	
	public static JSONObject ANCVisits = new JSONObject();
	static boolean ANCVisitInsertion, ANCVisitDeletion, ANCVisitResult;
	
	public static JSONObject getDetailInfo(JSONObject ANCInfo) {
		
		DBOperation dbOp = new DBOperation();
		DBInfoHandler dbObject = new UpzDBSelector().upzDBInfo(ANCInfo);
		try{
			if(ANCInfo.get("ancLoad").equals("")){
				System.out.println(ANCInfo);
				ANCVisitInsertion = InsertANCVisitInfo.createANCVisit(dbOp, dbObject, ANCInfo);
			}
			else if(ANCInfo.get("ancLoad").equals("delete")){
				//ANCVisitDeletion = DeleteANCVisitInfo.deleteANCVisit(dbOp, dbObject, ANCInfo);
				ANCVisitDeletion = DeleteANCVisitInfo.deleteANCVisit(dbOp, dbObject, ANCInfo, new QueryBuilder());
			}
			ANCVisits = new JSONObject();
			//ANCVisitResult = RetrieveANCVisitInfo.getANCVisits(dbOp, dbObject, ANCInfo,ANCVisits);
			ANCVisits = RetrieveANCVisitInfo.getANCVisits(dbOp, dbObject, ANCInfo,ANCVisits, new QueryBuilder());
		}
		catch(Exception e){
			System.out.println(e);
			e.printStackTrace();
		}
		finally{
			dbOp.dbObjectNullify(dbObject);			
		}	
		return ANCVisits;
	}
}