package org.sci.rhis.anc_deprecated;

import java.sql.ResultSet;

import org.json.JSONArray;
import org.json.JSONObject;
import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.db.DBOperation;

import org.sci.rhis.util.CalendarDate;

/**
 * @author sabah.mugab
 * @since June, 2015
 */
public class RetrieveANCVisitInfo {

	private static boolean status;
	
	public static boolean getANCVisits(DBOperation dbOp, DBInfoHandler dbObject, JSONObject ANCInfo, JSONObject ANCVisits) {
		
		try{
			String sql = "SELECT * FROM \"ancService\" "
						+ "WHERE \"ancService\".\"healthId\" = " + ANCInfo.get("healthid")
						+ " AND \"ancService\".\"pregNo\" = " + ANCInfo.get("pregNo")
						+ " ORDER BY \"ancService\".\"serviceId\" ASC";
			
			ResultSet rs = dbOp.dbExecute(dbObject,sql).getResultSet();
			int count =0;
			String count1;
						
			while(rs.next()){
				
				JSONArray ancVisitInfo = new JSONArray();
								
				ancVisitInfo.put(rs.getString("serviceId"));
				ancVisitInfo.put(rs.getObject("visitDate")==null?"":("" + rs.getObject("visitDate") + ""));
				ancVisitInfo.put(rs.getObject("bpSystolic")==null?"":rs.getObject("bpSystolic"));
				ancVisitInfo.put(rs.getObject("bpDiastolic")==null?"":rs.getObject("bpDiastolic"));
				ancVisitInfo.put(rs.getObject("weight")==null?"":rs.getObject("weight"));
				ancVisitInfo.put(rs.getObject("edema")==null?"0":rs.getObject("edema"));
				ancVisitInfo.put(rs.getObject("uterusHeight")==null?"":rs.getObject("uterusHeight"));
				ancVisitInfo.put(rs.getObject("fetusHeartRate")==null?"":rs.getObject("fetusHeartRate"));
				ancVisitInfo.put(rs.getObject("fetalPresentation")==null?"0":rs.getObject("fetalPresentation"));
				ancVisitInfo.put(rs.getObject("hemoglobin")==null?"":rs.getObject("hemoglobin"));
				ancVisitInfo.put(rs.getObject("jaundice")==null?"0":rs.getObject("jaundice"));
				ancVisitInfo.put(rs.getObject("urineSugar")==null?"0":rs.getObject("urineSugar"));
				ancVisitInfo.put(rs.getObject("urineAlbumin")==null?"0":rs.getObject("urineAlbumin"));
				ancVisitInfo.put(rs.getObject("complicationSign")==null?"":rs.getObject("complicationSign"));
				ancVisitInfo.put(rs.getObject("symptom")==null?"":rs.getObject("symptom"));
				ancVisitInfo.put(rs.getObject("disease")==null?"":rs.getObject("disease"));
				ancVisitInfo.put(rs.getObject("treatment")==null?"":rs.getObject("treatment"));
				ancVisitInfo.put(rs.getObject("advice")==null?"":rs.getObject("advice"));
				ancVisitInfo.put(rs.getObject("refer")==null?"2":rs.getObject("refer"));
				ancVisitInfo.put(rs.getObject("referCenterName")==null?"0":rs.getObject("referCenterName"));
				ancVisitInfo.put(rs.getObject("referReason")==null?"":rs.getObject("referReason"));
				ancVisitInfo.put(rs.getObject("serviceSource")==null?"":rs.getObject("serviceSource"));
				ancVisitInfo.put(rs.getString("anemia")==null?"0":rs.getObject("anemia"));
				ancVisitInfo.put(rs.getString("providerId")==null?"":rs.getObject("providerId"));		//24
				ancVisitInfo.put(rs.getObject("systemEntryDate")== null ? "" : rs.getString("systemEntryDate"));
				count = count+1;
				
				count1 = count<10 ? ("0" + count) : String.valueOf(count);
				ANCVisits.put("ancVisit"+count1 ,ancVisitInfo);	
				ANCVisits.put("ancStatus", false);
			}
			
			/*
			 * If client already moved to next service that means has delivery information
			 * or current date is more than 44 weeks from LMP date 
			 * then provider should not be able to insert anymore anc information 
			 */
			sql = "SELECT de.cnt, \"pregWomen\".\"LMP\" FROM \"pregWomen\", "
				+ "(SELECT COUNT(*) AS cnt FROM \"delivery\" WHERE \"delivery\".\"healthId\" = " + ANCInfo.get("healthid")
				+ " AND \"delivery\".\"pregNo\" = " + ANCInfo.get("pregNo") + ") AS de"
				+ " WHERE \"pregWomen\".\"healthId\" = " + ANCInfo.get("healthid")
				+ " AND \"pregWomen\".\"pregNo\" = " + ANCInfo.get("pregNo");
				
			rs = dbOp.dbExecute(dbObject,sql).getResultSet();
			
			if(rs.next()){
				int days = CalendarDate.getDaysCount(rs.getDate(2));
				//ANCVisits.put("pregWeek", days%7);
				
				if(rs.getInt(1)==1){
					ANCVisits.put("ancStatus", true);
				}
				else if(rs.getInt(1)==0){
					if(days > (44*7)){
						//System.out.println("lmp calc");
						ANCVisits.put("ancStatus", true);
					}				
				}				
			}
			
			if(!rs.isClosed()){
				rs.close();
			}
		}
		catch(Exception e){
			System.out.println(e);
			e.printStackTrace();
		}
					
		return status;
	}
}