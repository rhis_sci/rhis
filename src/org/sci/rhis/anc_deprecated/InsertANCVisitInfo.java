package org.sci.rhis.anc_deprecated;

import org.json.JSONObject;
import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.db.DBOperation;
import org.sci.rhis.util.CalendarDate;

/**
 * @author sabah.mugab
 * @since June, 2015
 */
public class InsertANCVisitInfo {
	
	static boolean status;
	
	public static boolean createANCVisit(DBOperation dbOp, DBInfoHandler dbObject, JSONObject ANCInfo) {
		
		try{		
	    
			String sql = "INSERT INTO \"ancService\" ("
	    			+ "\"healthId\",\"pregNo\",\"serviceId\",\"providerId\",\"visitDate\",\"serviceSource\","
	    			+ "\"bpSystolic\",\"bpDiastolic\",\"weight\",\"edema\",\"uterusHeight\",\"fetusHeartRate\","
	    			+ "\"fetalPresentation\",\"anemia\", \"hemoglobin\",\"jaundice\",\"urineSugar\",\"urineAlbumin\","
	    		//	+ "\"ironFolStatus\",\"ironFolQty\",\"ironFolUnit\",\"misoStatus\",\"misoQty\",\"misoUnit\","
	    			+ "\"complicationSign\",\"symptom\",\"disease\",\"treatment\",\"advice\",\"refer\",\"referReason\","
	    			+ "\"referCenterName\",\"sateliteCenterName\",\"client\",\"systemEntryDate\",\"modifyDate\") "
	    			+ "VALUES(" + ANCInfo.get("healthid") + ","
	    			+ ANCInfo.get("pregNo") + ","
	    			+ "(SELECT COUNT(\"serviceId\") FROM \"ancService\" WHERE \"healthId\" = " + ANCInfo.get("healthid") + " AND \"pregNo\" = " + ANCInfo.get("pregNo") + ") + 1,"
	    			+ ANCInfo.get("providerid") + ","
	    			+ (ANCInfo.get("ancdate").equals("")?null:("'"+ANCInfo.get("ancdate")+"'")) + ","
	    			+ (ANCInfo.get("ancservicesource").equals("")?null:("'"+ANCInfo.get("ancservicesource")+"'")) + ","
	    			//+ ANCInfo.get("ancservicesource") + "',"
	    			+ (ANCInfo.get("ancbpsys").equals("")?null:ANCInfo.get("ancbpsys")) + ","
	    			+ (ANCInfo.get("ancbpdias").equals("")?null:ANCInfo.get("ancbpdias")) + ","	    			
	    			+ (ANCInfo.get("ancweight").equals("")?null:ANCInfo.get("ancweight")) + ","
	    			+ (ANCInfo.get("ancedema").equals("")?null:("'"+ANCInfo.get("ancedema")+"'")) + ","
	    			//+ ANCInfo.get("ancedema") + "',"
	    			+ (ANCInfo.get("ancuheight").equals("")?null:ANCInfo.get("ancuheight")) + ","
	    			+ (ANCInfo.get("anchrate").equals("")?null:ANCInfo.get("anchrate")) + ","
	    			+ (ANCInfo.get("ancfpresentation").equals("")?null:("'"+ANCInfo.get("ancfpresentation")+"'")) + ","
	    			//+ ANCInfo.get("ancfpresentation") + "','"
	    			+ (ANCInfo.get("ancanemia").equals("")?null:("'"+ANCInfo.get("ancanemia")+"'")) + ","
	    			//+ ANCInfo.get("ancanemia") + "',"
	    			+ (ANCInfo.get("anchemoglobin").equals("")?null:ANCInfo.get("anchemoglobin")) + ","
	    			+ (ANCInfo.get("ancjaundice").equals("")?null:("'"+ANCInfo.get("ancjaundice")+"'")) + ","
	    			//+ ANCInfo.get("ancjaundice") + "','"
	    			+ (ANCInfo.get("ancsugar").equals("")?null:("'"+ANCInfo.get("ancsugar")+"'")) + ","
	    			//+ ANCInfo.get("ancsugar") + "','"
	    			+ (ANCInfo.get("ancalbumin").equals("")?null:("'"+ANCInfo.get("ancalbumin")+"'")) + ","
	    			//+ ANCInfo.get("ancalbumin") + "','"
	    			/*+ communityFields + ","
	    			+ communityFields + ","
	    			+ communityFields + ","
	    			+ communityFields + ","
	    			+ communityFields + ","
	    			+ communityFields + ",'"*/
	    			+ (ANCInfo.get("anccomplication").equals("")?null:("'"+ANCInfo.get("anccomplication")+"'")) + ","
	    			//+ ANCInfo.get("anccomplication") + "','"
	    			+ (ANCInfo.get("ancsymptom").equals("")?null:("'"+ANCInfo.get("ancsymptom")+"'")) + ","
	    			//+ ANCInfo.get("ancsymptom") + "','"
	    			+ (ANCInfo.get("ancdisease").equals("")?null:("'"+ANCInfo.get("ancdisease")+"'")) + ","
	    			//+ ANCInfo.get("ancdisease") + "','"
	    			+ (ANCInfo.get("anctreatment").equals("")?null:("'"+ANCInfo.get("anctreatment")+"'")) + ","
	    			//+ ANCInfo.get("anctreatment") + "','"
	    			+ (ANCInfo.get("ancadvice").equals("")?null:("'"+ANCInfo.get("ancadvice")+"'")) + ","
	    			//+ ANCInfo.get("ancadvice") + "',"
	    			+ ANCInfo.get("ancrefer") + ","
	    			+ (ANCInfo.get("ancreferreason").equals("")?null:("'"+ANCInfo.get("ancreferreason")+"'")) + ","
	    			//+ ANCInfo.get("ancreferreason") + "','"
	    			+ (ANCInfo.get("anccentername").equals("")?null:("'"+ANCInfo.get("anccentername")+"'")) + ","
	    			//+ ANCInfo.get("anccentername") + "','"
	    			+ (ANCInfo.get("ancsatelitecentername").equals("")?null:("'"+ANCInfo.get("ancsatelitecentername")+"'")) + ","
	    			+ (ANCInfo.get("client").equals("")?null:ANCInfo.get("client")) + ",'"
	    			//+ ANCInfo.get("ancsatelitecentername") + "','"
	    			+ CalendarDate.getCurrentDate() + "','"
	    			+ CalendarDate.getCurrentDate() + "')";	    			
			//System.out.println(sql);
			status = dbOp.dbStatementExecute(dbObject, sql);
		}
		catch(Exception e){
			System.out.println(e);
			e.printStackTrace();
		}				
		return status;		
	}
}