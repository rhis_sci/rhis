package org.sci.rhis.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 * @author sabah.mugab
 * @since June, 2015
 */
public class DBInfoHandler extends DBInfo{
	
	public String getPropLoc(){
    	return PROPERTIES_LOCATION;
    }
	
	public String getDistrictPropLoc(){
    	return PROPERTIES_LOCATION_DISTICTS;
    }
	
	public String getConnCls(){
    	return DB_CONNECTION_CLASS;
    }
	
	public void setHost(String host){
    	HOST = host;
    }
	
	public String getHost(){
    	return HOST;
    }
	
	public void setDBName(String dbName){
		DB_NAME = dbName;
    }
	
	public String getDBName(){
    	return DB_NAME;
    }
	
	public void setUserName(String userName){
    	USERNAME = userName;
    }
	
	public String getUserName(){
    	return USERNAME;
    }
	
	public void setUserPassword(String userPassword){
    	PASSWORD = userPassword;
    }
	
	public String getUserPassword(){
    	return PASSWORD;
    }
	
	public void setConnection(Connection c){
    	CONNECTION = c;
    }
	
	public Connection getConnection(){
    	return CONNECTION;
    }
	
	public void setStatement(Statement stmt){
		STATEMENT = stmt;
    }
	
	public Statement getStatement(){
    	return STATEMENT;
    }
	
	public void setPreparedStatement(PreparedStatement ps){
		PREPAREDSTATEMENT = ps;
    }
	
	public PreparedStatement getPreparedStatement(){
    	return PREPAREDSTATEMENT;
    }
	
	public void setResultSet(ResultSet rs){
		RESULTSET = rs;
    }
	
	public ResultSet getResultSet(){
    	return RESULTSET;
    }

}