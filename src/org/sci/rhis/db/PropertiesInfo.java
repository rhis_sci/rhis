package org.sci.rhis.db;

import java.util.Properties;

import org.sci.rhis.util.ConfInfoRetrieve;

/**
 * @author sabah.mugab
 * @since January, 2018
 */
public class PropertiesInfo {
	
	private final String DB_PROPERTIES_LOCATION = "resource/db.properties";
	private final String SERVICE_JSON_PROPERTIES_LOCATION = "resource/service.properties";
	//private final String SERVICE_JSON_PROPERTIES_LOCATION = "resource/service_old.properties";
	private final String SERVICE_JSON_PROPERTIES_OLD_LOCATION = "resource/service_old.properties";
	private final String SERVICE_JSON_MAP_PROPERTIES_LOCATION = "resource/json_map.properties";
	//private final String SERVICE_JSON_MAP_PROPERTIES_LOCATION = "resource/json_map_old.properties";
	private final String SERVICE_JSON_MAP_PROPERTIES_OLD_LOCATION = "resource/json_map_old.properties";
	private final String DISTRICTS_PROPERTIES_LOCATION = "resource/implementing_district_db.properties";
	private final String DISTRICTS_DETAIL_PROPERTIES_LOCATION = "resource/implementing_district_detail.properties";
	private final String UPAZILA_LIST_PROPERTIES_LOCATION = "resource/upazilaid_list.properties";
	private final String HOST_PROPERTIES_LOCATION = "resource/host.properties";
	private final String EMAIL_CONFIGURATION_PROPERTIES_LOCATION = "resource/email_configuration.properties";
		
	private Properties DB_PROPERTIES = null;
	private Properties SERVICE_JSON_PROPERTIES = null;
	private Properties SERVICE_JSON_MAP_PROPERTIES = null;
	private Properties SERVICE_JSON_PROPERTIES_OLD = null;
	private Properties SERVICE_JSON_MAP_PROPERTIES_OLD = null;
	private Properties DISTRICTS_PROPERTIES = null;
	private Properties DISTRICTS_DETAIL_PROPERTIES = null;
	private Properties UPAZILA_LIST_PROPERTIES = null;
	private Properties HOST_PROPERTIES = null;
	private Properties EMAIL_CONFIGURATION_PROPERTIES = null;
		
	PropertiesInfo(){
		setDBProperties();
		setServiceJsonProperties();
		setOldServiceJsonProperties();
		setServiceJsonMapProperties();
		setOldServiceJsonMapProperties();
		setDistrictsProperties();
		setUpazilaListProperties();
		setHostProperties();
		setDistrictsDetailProperties();
		setEmailConfigurationProperties();
	}
	
	private void setDBProperties(){
		DB_PROPERTIES = new Properties(ConfInfoRetrieve.readingConf(DB_PROPERTIES_LOCATION));
	}
	
	private void setServiceJsonProperties(){
		SERVICE_JSON_PROPERTIES = new Properties(ConfInfoRetrieve.readingConf(SERVICE_JSON_PROPERTIES_LOCATION));
	}
	
	private void setOldServiceJsonProperties(){
		SERVICE_JSON_PROPERTIES_OLD = new Properties(ConfInfoRetrieve.readingConf(SERVICE_JSON_PROPERTIES_OLD_LOCATION));
	}
	
	private void setServiceJsonMapProperties(){
		SERVICE_JSON_MAP_PROPERTIES = new Properties(ConfInfoRetrieve.readingConf(SERVICE_JSON_MAP_PROPERTIES_LOCATION));
	}
	
	private void setOldServiceJsonMapProperties(){
		SERVICE_JSON_MAP_PROPERTIES_OLD = new Properties(ConfInfoRetrieve.readingConf(SERVICE_JSON_MAP_PROPERTIES_OLD_LOCATION));
	}
	
	private void setDistrictsProperties(){
		DISTRICTS_PROPERTIES = new Properties(ConfInfoRetrieve.readingConf(DISTRICTS_PROPERTIES_LOCATION));
	}
	
	private void setDistrictsDetailProperties(){
		DISTRICTS_DETAIL_PROPERTIES = new Properties(ConfInfoRetrieve.readingConf(DISTRICTS_DETAIL_PROPERTIES_LOCATION));
	}
	
	private void setUpazilaListProperties(){
		UPAZILA_LIST_PROPERTIES = new Properties(ConfInfoRetrieve.readingConf(UPAZILA_LIST_PROPERTIES_LOCATION));
	}
	
	private void setHostProperties(){
		HOST_PROPERTIES = new Properties(ConfInfoRetrieve.readingConf(HOST_PROPERTIES_LOCATION));
	}
	
	private void setEmailConfigurationProperties(){
		EMAIL_CONFIGURATION_PROPERTIES = new Properties(ConfInfoRetrieve.readingConf(EMAIL_CONFIGURATION_PROPERTIES_LOCATION));
	}
		
	public Properties getDBProperties(){
		return DB_PROPERTIES;
	}
	
	public Properties getServiceJsonProperties(){
		return SERVICE_JSON_PROPERTIES;
	}
	
	public Properties getOldServiceJsonProperties(){
		return SERVICE_JSON_PROPERTIES_OLD;
	}
	
	public Properties getServiceJsonMapProperties(){
		return SERVICE_JSON_MAP_PROPERTIES;
	}
	
	public Properties getOldServiceJsonMapProperties(){
		return SERVICE_JSON_MAP_PROPERTIES_OLD;
	}
	
	public Properties getDistrictsProperties(){
		return DISTRICTS_PROPERTIES;
	}
	
	public Properties getDistrictsDetailProperties(){
		return DISTRICTS_DETAIL_PROPERTIES;
	}
	
	public Properties getUpazilaListProperties(){
		return UPAZILA_LIST_PROPERTIES;
	}
	
	public Properties getHostProperties(){
		return HOST_PROPERTIES;
	}
	
	public Properties getEmailConfigurationProperties(){
		return EMAIL_CONFIGURATION_PROPERTIES;
	}
}