package org.sci.rhis.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 * @author sabah.mugab
 * @since June, 2015
 */
abstract class DBInfo {
	
	protected String PROPERTIES_LOCATION = "resource/db.properties";
	protected String PROPERTIES_LOCATION_DISTICTS = "resource/implementing_district_db.properties";
	protected String DB_CONNECTION_CLASS = "org.postgresql.Driver";
	protected String HOST = null;
	protected String DB_NAME = null;
	protected String USERNAME = null;
	protected String PASSWORD = null;
    
	protected Connection CONNECTION = null;
	protected Statement STATEMENT = null;
	protected PreparedStatement PREPAREDSTATEMENT = null;
	protected ResultSet RESULTSET = null;

}