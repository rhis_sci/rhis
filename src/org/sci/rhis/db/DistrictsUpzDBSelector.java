package org.sci.rhis.db;

import java.util.Properties;

import org.json.JSONObject;
import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.security.AES;

/**
 * @author sabah.mugab
 * @since January, 2018
 */
public class DistrictsUpzDBSelector extends UpzDBSelector{
	
	DBInfoHandler dbDetail = new DBInfoHandler();
	
	public DBInfoHandler getUpzDBInfo(JSONObject geoJson){
		String[] dbDetails = getDBDetails(geoJson);
		
		dbDetail.setHost(dbDetails[0]);
		dbDetail.setDBName(getDBName(geoJson, dbDetails[1]));
		dbDetail.setUserName(dbDetails[2]);
		dbDetail.setUserPassword(dbDetails[3]);
		
		return dbDetail;
	}
	
	public DBInfoHandler getDistDBInfo(JSONObject geoJson){
		String[] dbDetails = getDBDetails(geoJson);
		
		dbDetail.setHost(dbDetails[0]);
		dbDetail.setDBName(dbDetails[1]);
		dbDetail.setUserName(dbDetails[2]);
		dbDetail.setUserPassword(dbDetails[3]);
		
		return dbDetail;
	}
	
	private String[] getDBDetails(JSONObject geoJson){
		Properties prop = PropertiesReader.getPropReader().getDistrictsProperties();
		return AES.decrypt(prop.getProperty(geoJson.getString("zillaid")),"rhis_db_info").split(","); 
	}
}