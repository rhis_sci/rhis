package org.sci.rhis.db;

import org.json.JSONObject;

import java.util.Iterator;
import java.util.Properties;

/**
 * @author sabah.mugab
 * @since December, 2016
 */
public class JSONKeyMapper extends Mapper{
	
	public JSONKeyMapper(){}
	
	JSONKeyMapper(DBTableInfoHandler dbTableInfoHandler){
	
		dbTableInfoHandler.serviceJSON.put("table", PropertiesReader.getPropReader().getServiceJsonProperties().getProperty(dbTableInfoHandler.serviceName + /*"_" + dbTableInfoHandler.serviceType +*/ "_table"));
		dbTableInfoHandler.serviceJSON.put("condition", PropertiesReader.getPropReader().getServiceJsonProperties().getProperty(dbTableInfoHandler.serviceName + "_" + dbTableInfoHandler.serviceType + "_condition"));
		dbTableInfoHandler.serviceJSON.put("fields", PropertiesReader.getPropReader().getServiceJsonProperties().getProperty(dbTableInfoHandler.serviceName + "_" + dbTableInfoHandler.serviceType + "_json"));
		
		this.resetObject();
		this.setKeyMap(dbTableInfoHandler);
		this.setConditionKeyMap(dbTableInfoHandler);
		
		dbTableInfoHandler.serviceJSON.put("keyMapField", this.getJSONKeyDetail());
		dbTableInfoHandler.serviceJSON.put("keyMapCondition", this.getConditionJSONKeyDetail());
	}
	
	private void resetObject(){

		serviceMapper = new JSONObject();
		conditionMapper = new JSONObject();
	}
	
	private void setKeyMap(DBTableInfoHandler dbTableInfoHandler){
		
		this.setInnerKeys(dbTableInfoHandler, serviceMapper, "fields");
	}
	
	private void setConditionKeyMap(DBTableInfoHandler dbTableInfoHandler){
		
		this.setInnerKeys(dbTableInfoHandler, conditionMapper, "condition");
	}
	
	private void setInnerKeys(DBTableInfoHandler dbTableInfoHandler, JSONObject mapper, String keyDetail){
		
		JSONObject innerKey = new JSONObject();
				
		for(String keyName:dbTableInfoHandler.serviceJSON.getString(keyDetail).split(",")){
			innerKey = new JSONObject();
			if(PropertiesReader.getPropReader().getServiceJsonMapProperties().getProperty(dbTableInfoHandler.serviceName + "_" + keyName)!=null){
				String[] dbInfo = PropertiesReader.getPropReader().getServiceJsonMapProperties().getProperty(dbTableInfoHandler.serviceName + "_" + keyName).split(",");
				
				innerKey.put("dbColumnName", dbInfo[0]);
				innerKey.put("dbColumnType", dbInfo[1]);
				innerKey.put("dbColumnDefaultValue", dbInfo[2]);
				innerKey.put("responseDefaultValue", dbInfo[3]);
			}
			
			mapper.put(keyName, innerKey);
		}		
	}
	
	@SuppressWarnings("unchecked")
	public JSONObject setRequiredKeys(JSONObject serviceJSON, String serviceName){
		
		Properties serviceKeys = PropertiesReader.getPropReader().getServiceJsonMapProperties();
		Iterator<String> keys = serviceJSON.keys();
		String key = "";
		JSONObject additionalKeys = new JSONObject();
		
		while( keys.hasNext() ) {
		    key = keys.next();
		    
		    if(serviceKeys.getProperty(serviceName + "_" + key)!=null){
		    	additionalKeys.put(serviceKeys.getProperty(serviceName + "_" + key).split(",")[0],serviceJSON.getString(key));		    			    	
		    }		   
		    
		    additionalKeys.put(key,serviceJSON.getString(key));
		}
		additionalKeys.put("serviceName",serviceName);
		return additionalKeys;
	}
	
	@Deprecated
	private String getPropLoc(){
    	return PROPERTIES_LOCATION;
    }
	
	public JSONObject getJSONKeyDetail(){
		return serviceMapper;
	}	
	
	public JSONObject getConditionJSONKeyDetail(){
		return conditionMapper;
	}	
}