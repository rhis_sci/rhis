package org.sci.rhis.db;

import org.json.JSONObject;

/**
 * @author sabah.mugab
 * @since December, 2016
 */
public class DBTableInfoHandler extends DBTableInfo{
	
	public DBTableInfoHandler(String serviceN,String serviceT){
		serviceJSON = new JSONObject();
		serviceName = serviceN;
		serviceType = serviceT;
		this.setProp();
	}
	
	private void setProp(){
		keyMapper = new JSONKeyMapper(this);		
	}
	
	public String getPropLoc(){
    	return PROPERTIES_LOCATION;
    }
	
	public JSONObject getDetail(){
		return serviceJSON; 
	}
}