package org.sci.rhis.db;

import org.json.JSONObject;

/**
 * @author sabah.mugab
 * @since December, 2016
 */
abstract class Mapper {
	
	protected final String PROPERTIES_LOCATION = "resource/json_map.properties";
	
	protected JSONObject serviceMapper = new JSONObject();
	protected JSONObject conditionMapper = new JSONObject();
}