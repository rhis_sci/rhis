package org.sci.rhis.db;

/**
 * @author sabah.mugab
 * @since February, 2018
 */
public class PropertiesReader {
	
	private static PropertiesInfo propReader = null;
    
    public static PropertiesInfo getPropReader(){
        if(propReader==null){
        	propReader = new PropertiesInfo();
        }
    	return propReader;
    }	
}