package org.sci.rhis.db;

import com.sun.org.apache.xerces.internal.impl.xpath.regex.Match;
import org.json.JSONObject;
import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.security.AES;
import org.sci.rhis.security.Constant;

import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author sabah.mugab
 * @since December, 2016
 */
public class UpzDBSelector{
	
	private final int providerType = 900;
	DBInfoHandler dbDetail = new DBInfoHandler();
	
	public DBInfoHandler upzDBInfo(JSONObject geoJson){
		String zillaid = getZillaid(geoJson); //for testing purpose it should be removed
		zillaid = String.format("%02d", Integer.parseInt(zillaid));
		String[] dbDetails = AES.decrypt(PropertiesReader.getPropReader().getDBProperties().getProperty("FACILITY_"+zillaid), Constant.SECRET_KEY).split(","); //for testing purpose it should be removed
		
//		String[] dbDetails = AES.decrypt(PropertiesReader.getPropReader().getDBProperties().getProperty("FACILITY"), Constant.SECRET_KEY).split(","); //for testing purpose

		dbDetail.setHost(dbDetails[0]);
		if(geoJson.has("providerType") && geoJson.has("zillaid") && geoJson.has("upazilaid")&& !geoJson.has("loginrequest")){
			if(geoJson.getInt("providerType") > providerType){
				dbDetail.setDBName(dbDetails[1]);
			}
			else{
				dbDetail.setDBName(getDBName(geoJson, dbDetails[1]));
			}
		}
		else{
			dbDetail.setDBName(dbDetails[1]);
		}
//		dbDetail.setDBName(dbDetails[1]); //for testing purpose. In production this line should be removed.
		dbDetail.setUserName(dbDetails[2]);
		dbDetail.setUserPassword(dbDetails[3]);
		System.out.println("\n#####################\nSELECTED DB: " + dbDetail.getDBName() + "\n##########################\n");
		return dbDetail;
	}
	
	protected String getDBName(JSONObject json, String districtDB){
		
		if(!json.getString("zillaid").equals("") && !json.getString("upazilaid").equals("")){
			return "RHIS_" + String.format("%02d", Integer.valueOf(json.getString("zillaid"))) + "_" + String.format("%02d", Integer.valueOf(json.getString("upazilaid")));
			//return "NGO_" + String.format("%02d", Integer.valueOf(json.getString("zillaid"))) + "_" + String.format("%02d", Integer.valueOf(json.getString("upazilaid")));
		}
		else{
			return districtDB;
		}
	}

	protected String getZillaid(JSONObject geoJson){
		String zillaid = "";
		String providerid;

		if(geoJson.has(("zillaid"))){
			zillaid = geoJson.getString("zillaid");

		}
		else {
			if (geoJson.has("client")) {
				if (geoJson.get("client").equals("1")) {
					if (geoJson.has("uid")) {
						providerid = geoJson.getString("uid");
						Pattern r = Pattern.compile("^\\d{2}(?=[1-9]|1[0-5]|101)");
						Matcher m = r.matcher(providerid);
						if (m.find()) {
							zillaid = m.group(0);
						} else {
							r = Pattern.compile("^\\d");
							m = r.matcher(providerid);
							if (m.find()) {
								zillaid = m.group(0);
							}
						}
					}
				}
			}
		}
		if(zillaid.isEmpty()) {
			zillaid = (String) getIgnoreCase(geoJson,"zillaid");

		}
		return zillaid;
	}

	public Object getIgnoreCase(JSONObject jobj, String key) {
		Iterator<String> iter = jobj.keys();
		while (iter.hasNext()) {
			String key1 = iter.next();
			if (key1.equalsIgnoreCase(key)) {
				return jobj.get(key1);
			}
		}

		return null;
	}
}