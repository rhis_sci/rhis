package org.sci.rhis.db;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.sci.rhis.email.SendEmail;

/**
 * @author sabah.mugab
 * @since June, 2015
 */
public class DBOperation {
	
	private void sendNotificationOnError(DBInfoHandler getDBInfo, SQLException sqle, String sql) {
		SendEmail se = new SendEmail();
		
		String emailSubject = "eMIS: DB error occurred";
		String emailBody = "DB URL and Name: " + getDBInfo.getHost().split("://")[1].split(":")[0] + " -> " + getDBInfo.DB_NAME + "\n\n";
		if(!sql.equals("")) {
			emailBody = emailBody + "=======================================================================\n";
			emailBody = emailBody + "SQL: \n" + sql + "\n";
			emailBody = emailBody + "=======================================================================\n";
		}
		emailBody = emailBody + "=======================================================================\n";
		emailBody = emailBody + "StackTrace: \n" + ExceptionUtils.getStackTrace(sqle) + "\n";
		emailBody = emailBody + "=======================================================================\n";
		
		se.sendEmailAlert(emailSubject, emailBody);		
	}
	
	public DBInfoHandler dbConnect(DBInfoHandler getDBInfo){
		
		try {
			Class.forName(getDBInfo.getConnCls());
			getDBInfo.setConnection( DriverManager.getConnection(
			            getDBInfo.getHost()+getDBInfo.getDBName(),
			            getDBInfo.getUserName(),
			            getDBInfo.getUserPassword()));
			return getDBInfo;
		}
		catch (ClassNotFoundException | SQLException e) {
			System.out.println("connection error while trying to connect " + getDBInfo.getHost() + "->" + getDBInfo.getDBName());
			e.printStackTrace();
			getDBInfo.setConnection(null);
			return getDBInfo;
		}		
	}	
	
	public DBInfoHandler dbCreateStatement(DBInfoHandler getDBInfo){
		
		try {
			getDBInfo.setStatement(dbConnect(getDBInfo).getConnection().createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY,ResultSet.HOLD_CURSORS_OVER_COMMIT));
			return getDBInfo;
		} 
		catch (SQLException e) {
			System.out.println("Create statement error");
			e.printStackTrace();
			getDBInfo.setStatement(null);
			return getDBInfo;
		}		
	}
		
	public DBInfoHandler dbCreatePreparedStatement(DBInfoHandler getDBInfo, String sql){
		
		try {
			getDBInfo.setPreparedStatement(dbConnect(getDBInfo).getConnection().prepareStatement(sql));
			return getDBInfo;
		} 
		catch (SQLException e) {
			System.out.println("Prepared statement error");
			e.printStackTrace();
			getDBInfo.setPreparedStatement(null);
			return getDBInfo;
		}	
	}
	
	public DBInfoHandler dbExecute(DBInfoHandler getDBInfo){
		
		try {
			getDBInfo.setResultSet(getDBInfo.getPreparedStatement().executeQuery());
			return getDBInfo;
		} 
		catch (SQLException e) {
			sendNotificationOnError(getDBInfo, e, getDBInfo.getPreparedStatement().toString());
			System.out.println("Resultset error");
			e.printStackTrace();
			getDBInfo.setResultSet(null);
			return getDBInfo;
		}		
	}
	
	public DBInfoHandler dbExecute(DBInfoHandler getDBInfo, String sql){

		try {
			if(getDBInfo.getStatement()!=null){
				getDBInfo.setResultSet(getDBInfo.getStatement().executeQuery(sql));
			}
			else{
				this.dbCreateStatement(getDBInfo);
				getDBInfo.setResultSet(getDBInfo.getStatement().executeQuery(sql));
			}			
			return getDBInfo;
		} 
		catch (SQLException e) {
			sendNotificationOnError(getDBInfo, e, sql);
			System.out.println("Statement error");
			e.printStackTrace();
			getDBInfo.setResultSet(null);
			return getDBInfo;
		}		
	}
	
	public boolean dbStatementExecute(DBInfoHandler getDBInfo, String sql){
			
		try {
			if(getDBInfo.getStatement()!=null){
				getDBInfo.getStatement().execute(sql);
			}
			else{
				this.dbCreateStatement(getDBInfo);
				getDBInfo.getStatement().execute(sql);				
			}				
			return true;
		} 
		catch (SQLException e) {
			sendNotificationOnError(getDBInfo, e, sql);
			System.out.println("SQL error: " + ExceptionUtils.getStackTrace(e));
			//e.printStackTrace();
			
			return false;
		}		
	}
	
	public int dbStatementExecuteUpdate(DBInfoHandler getDBInfo, String sql){
			
		try {
			if(getDBInfo.getStatement()!=null){
				getDBInfo.getStatement().executeUpdate(sql);
			}
			else{
				this.dbCreateStatement(getDBInfo);
				getDBInfo.getStatement().executeUpdate(sql);				
			}			
			return 1;
		} 
		catch (SQLException e) {
			sendNotificationOnError(getDBInfo, e, sql);
			e.printStackTrace();
			return 0;
		}		
	}
	
	public void closeConn(DBInfoHandler getDBInfo){
		try {
			if(getDBInfo.getConnection() != null){
				if(!getDBInfo.getConnection().isClosed()){
					getDBInfo.getConnection().close();
				}
			}
		} 
		catch (SQLException e) {
			e.printStackTrace();
		}
		/*finally{
			// This manually deregisters JDBC driver, which prevents Tomcat 7 and upward from complaining about memory leaks
	        Enumeration<Driver> drivers = DriverManager.getDrivers();
	        while (drivers.hasMoreElements()){
	            Driver driver = drivers.nextElement();
	            try{
	                DriverManager.deregisterDriver(driver);	                
	            } 
	            catch (SQLException e){
	            	e.printStackTrace();	                
	            }
	        }
		}*/
	}
	
	public void closeStatement(DBInfoHandler getDBInfo){
		try {
			if(getDBInfo.getStatement() != null){
				if(!getDBInfo.getStatement().isClosed()){
					getDBInfo.getStatement().close();
				}
			}
		} 
		catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void closePreparedStatement(DBInfoHandler getDBInfo){
		try {
			if(getDBInfo.getPreparedStatement() != null){
				if(!getDBInfo.getPreparedStatement().isClosed()){
					getDBInfo.getPreparedStatement().close();
				}
			}
		} 
		catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void closeResultSet(DBInfoHandler getDBInfo){
		try {
			if(getDBInfo.getResultSet() != null){
				if(!getDBInfo.getResultSet().isClosed()){
					getDBInfo.getResultSet().close();
				}
			}
		} 
		catch (SQLException e) {
			e.printStackTrace();
		}
	}	
	
	public void dbObjectNullify(DBInfoHandler getDBInfo){
		closeResultSet(getDBInfo);
		closeStatement(getDBInfo);
		closePreparedStatement(getDBInfo);
		closeConn(getDBInfo);	
		getDBInfo = null;
	}
}