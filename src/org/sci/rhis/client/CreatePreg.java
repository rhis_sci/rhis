package org.sci.rhis.client;

import java.sql.ResultSet;
import java.text.SimpleDateFormat;

import org.json.JSONObject;
import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.db.DBOperation;
import org.sci.rhis.db.QueryBuilder;
import org.sci.rhis.util.CalendarDate;
import org.sci.rhis.util.JsonHandler;

/**
 * @author sabah.mugab
 * @since June, 2015
 */
public class CreatePreg {
	
	public static JSONObject insertPregInfo(DBOperation dbOp, DBInfoHandler dbObject, JSONObject pregInfo) {
		
		QueryBuilder dynamicQueryBuilder = new QueryBuilder();
		JsonHandler result = new JsonHandler();
		
		JSONObject pInfo = new JSONObject();
		pInfo.put("pregNo", "");
		pInfo.put("highRiskPreg", "No");
		pInfo.put("False", "");
		pInfo.put("responseType", "insert");
		pInfo.put("hasDeliveryInformation", "No");
		pInfo.put("hasAbortionInformation", "No");
		int statusLMP = 0;
		
		try{
			String sql = "";
			pInfo.put("tempLMP", pregInfo.getString("lmp"));
			pInfo.put("statusLMP", statusLMP);
			pregInfo.put("tempLMP", pregInfo.getString("lmp"));
			pregInfo.put("statusLMP", statusLMP);
						
			ResultSet rs = dbOp.dbExecute(dbObject, dynamicQueryBuilder.getInsertQuery
					(result.addJsonKeyValueEdit(result.addJsonKeyIncrementalField
							(pregInfo,"pregNo"), "PREGWOMEN"))).getResultSet();
			
			if(rs.next()){
				pInfo.put("pregNo", result.getResultSetValue(rs, dynamicQueryBuilder.getColumn("PREGWOMEN_pregNo")));
			}
			if(pInfo.getInt("pregNo")>1){
				sql = "SELECT " + dynamicQueryBuilder.getColumn("table","DELIVERY_dDate")
					  + " FROM " + dynamicQueryBuilder.getTable("DELIVERY") 
					  + " WHERE " + dynamicQueryBuilder.getColumn("table", "DELIVERY_healthid",new String[]{pregInfo.getString("healthId")},"=")
					  + " AND " + dynamicQueryBuilder.getColumn("table", "DELIVERY_pregno",new String[]{String.valueOf(pInfo.getInt("pregNo") - 1)},"=");
				
				rs = dbOp.dbExecute(dbObject,sql).getResultSet();
				SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
				
				if(rs.next()){
					int days = CalendarDate.getDaysCount(rs.getDate(dynamicQueryBuilder.getColumn("DELIVERY_dDate")), formatter.parse(pregInfo.getString("lmp")));
					if(days < (365*2)){
						pInfo.put("highRiskPreg", "Yes");
					}					
				}
			}			
			rs.close();
						
			/*pregInfo.put("special", 1);
			sql = "WITH upsert AS (" 
				  + new QueryBuilder().getUpdateQuery(new JsonHandler().addJsonKeyValueEdit(pregInfo, "PREGWOMEN_ELCO")) 
				  + " RETURNING *) " 
				  + new QueryBuilder().getInsertQuery(new JsonHandler().addJsonKeyValueEdit(pregInfo, "PREGWOMEN_ELCO")) 
				  + " WHERE NOT EXISTS (SELECT * FROM upsert)";
			dbOp.dbStatementExecute(dbObject, sql);
			pregInfo.put("special", null);*/
			dbOp.dbStatementExecute(dbObject, dynamicQueryBuilder.getUpsertQuery(pregInfo, "PREGWOMEN_ELCO"));			
			ClientInfoUtil.updateClientMobileNo(pregInfo, dbOp, dbObject);
		}
		catch(Exception e){
			e.printStackTrace();
		}				
		return pInfo;
	}
}