package org.sci.rhis.client;

import java.sql.ResultSet;

import org.json.JSONObject;
import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.db.DBOperation;
import org.sci.rhis.db.QueryBuilder;
import org.sci.rhis.util.CalendarDate;
import org.sci.rhis.util.JsonHandler;

/**
 * @author sabah.mugab
 * @since September, 2015
 */
public class AdvanceSearchExecution {
	
	public static void getPOPResult(DBOperation dbOp, DBInfoHandler dbObject, ClientInfoUtil clientHelper, String sqlPOP, JSONObject searchList, QueryBuilder dynamicQueryBuilder) {
		
		try{
			ResultSet rs = dbOp.dbExecute(dbObject,sqlPOP).getResultSet();
					
			while(rs.next()){
				JSONObject resultSet = new JSONObject();
				
				searchList.put("count", searchList.getInt("count") + 1 );
				
				resultSet.put("healthId",rs.getString("HealthID"));
				resultSet.put("name",rs.getString("NameEng"));
				resultSet.put("fatherName",rs.getObject("Father")==null?"":rs.getString("Father"));
				resultSet.put("husbandName",rs.getObject("husbandName")==null?"":rs.getString("husbandName"));
				resultSet.put("age",rs.getObject("DOB")==null?"":String.valueOf((CalendarDate.getDaysCount(rs.getDate("DOB")))/ClientGeneralInfo.yearInDays));
				resultSet.put("healthIdPop",1);
				
				if(searchList.getString("options").equals("3")){
					resultSet.put("serviceDate", rs.getObject(searchList.getString("column"))==null?"":rs.getString(searchList.getString("column")));
				}
				else{
					resultSet.put("serviceDate", "");
				}
				
				searchList.put(searchList.getString("count"), resultSet);
							
			}
			if(!rs.isClosed()){
				rs.close();
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}

	public static void getNRCResult(DBOperation dbOp, DBInfoHandler dbObject, ClientInfoUtil clientHelper, String sqlNRC, JSONObject searchList, QueryBuilder dynamicQueryBuilder) {
		JsonHandler result = new JsonHandler();
		String sql = "SELECT " + dynamicQueryBuilder.getColumn("m", "MEMBER_healthid") + " AS \"HealthID\","
					+ dynamicQueryBuilder.getColumn("m", "MEMBER_nameeng") + " AS \"NameEng\","
					+ dynamicQueryBuilder.getColumn("m", "MEMBER_dob") + " AS \"DOB\","
					+ clientHelper.getFatherSQL(dynamicQueryBuilder)
					+ clientHelper.getSpouseSQL(dynamicQueryBuilder)					
					+ "FROM " + dynamicQueryBuilder.getTable("MEMBER") + " AS m "
					+ "WHERE (" /*+ dynamicQueryBuilder.getColumn("m", "MEMBER_exittype",new String[]{""},"=")
					+ " OR "*/ 
					+ dynamicQueryBuilder.getColumn("m", "MEMBER_exittype",new String[]{"0"},"=")
					+ " OR " + dynamicQueryBuilder.getColumn("m", "MEMBER_exittype",new String[]{},"isnull") + ")"
					+ " AND " + dynamicQueryBuilder.getColumn("m", "MEMBER_healthid") + " IN (";
		
		try{
			ResultSet rs = dbOp.dbExecute(dbObject,sqlNRC).getResultSet();
			String healthId = "";
					
			while(rs.next()){
				JSONObject resultSet = new JSONObject();			
				/*
				 * checking if this is still a NRC (temporary) ID
				 */System.out.println("Result (" + dynamicQueryBuilder.getColumn("CM_healthid") + "): " + rs.getString(dynamicQueryBuilder.getColumn("CM_healthid")));
				if(rs.getLong(dynamicQueryBuilder.getColumn("CM_healthid")) == rs.getLong(dynamicQueryBuilder.getColumn("CM_generatedid"))){
					
					searchList.put("count", searchList.getInt("count") + 1 );
					
					//resultSet.put("healthId",rs.getString("healthId"));
					resultSet.put("healthId",result.getResultSetValue(rs,dynamicQueryBuilder.getColumn("CM_healthid")));
					resultSet.put("name",result.getResultSetValue(rs,dynamicQueryBuilder.getColumn("CM_name")));
					resultSet.put("fatherName",result.getResultSetValue(rs,dynamicQueryBuilder.getColumn("CM_fathername")));
					resultSet.put("husbandName",result.getResultSetValue(rs,dynamicQueryBuilder.getColumn("CM_husbandname")));
					resultSet.put("age",rs.getObject(dynamicQueryBuilder.getColumn("CM_dob"))==null?"":
						String.valueOf((CalendarDate.getDaysCount(rs.getDate(dynamicQueryBuilder.getColumn("CM_dob"))))/ClientGeneralInfo.yearInDays));
					resultSet.put("healthIdPop",0);
					
					if(searchList.getString("options").equals("3")){
						resultSet.put("serviceDate",result.getResultSetValue(rs,searchList.getString("column")));
					}
					else{
						resultSet.put("serviceDate", "");
					}
					
					searchList.put(searchList.getString("count"), resultSet);
				}
				else if(searchList.getString("options").equals("1")){
					healthId = healthId + rs.getString(dynamicQueryBuilder.getColumn("CM_healthid")) + ","; 
				}				
			}
			
			if(healthId.length() > 5){
				sql = sql + healthId.substring(0, healthId.length()-1) + ")";
				getPOPResult(dbOp, dbObject, clientHelper, sql, searchList, dynamicQueryBuilder);
			}
			
			if(!rs.isClosed()){
				rs.close();
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
	
	
	public static void getPregwomenResult(DBOperation dbOp, DBInfoHandler dbObject, String sql, JSONObject searchList) {
		JsonHandler result = new JsonHandler();
		try{
			ResultSet rs = dbOp.dbExecute(dbObject,sql).getResultSet();
					
			while(rs.next()){
				JSONObject resultSet = new JSONObject();
				
				searchList.put("count", searchList.getInt("count") + 1 );
				
				resultSet.put("healthId",result.getResultSetValue(rs,"healthId"));
				//resultSet.put("generatedId",(rs.getObject("generatedId")==null?"":rs.getString("generatedId")));
				resultSet.put("name",result.getResultSetValue(rs,"name"));
				resultSet.put("age",rs.getObject("DOB")==null?(rs.getObject("Age")==null?"":rs.getString("Age")):String.valueOf((CalendarDate.getDaysCount(rs.getDate("DOB")))/ClientGeneralInfo.yearInDays));
				resultSet.put("husbandName",result.getResultSetValue(rs,"husbandName"));
				resultSet.put("mobileNo",rs.getObject("mobileNo")==null?(rs.getObject("MobileNo1")==null?"":rs.getObject("MobileNo1")):rs.getString("mobileNo"));
				resultSet.put("elcoNo",result.getResultSetValue(rs,"elcoNo"));
				resultSet.put("son",result.getResultSetValue(rs,"son","0"));
				resultSet.put("dau",result.getResultSetValue(rs,"dau","0"));
				resultSet.put("gravida",result.getResultSetValue(rs,"gravida"));
				resultSet.put("LMP",result.getResultSetValue(rs,"LMP"));
				resultSet.put("EDD",result.getResultSetValue(rs,"EDD"));
				resultSet.put("VILLAGENAME",result.getResultSetValue(rs,"VILLAGENAME"));
				if(rs.getLong("healthId") == rs.getLong("generatedId")){
					resultSet.put("healthIdPop",0);
					resultSet.put("name",result.getResultSetValue(rs,"cmname"));
					resultSet.put("husbandName",result.getResultSetValue(rs,"cmhusbandname"));
					resultSet.put("age",rs.getObject("dob")==null?(rs.getObject("age")==null?"":rs.getString("age")):String.valueOf((CalendarDate.getDaysCount(rs.getDate("dob")))/ClientGeneralInfo.yearInDays));
				}
				else{
					resultSet.put("healthIdPop",1);
				}
				
				resultSet.put("zillaId",result.getResultSetValue(rs,"zillaId"));
				resultSet.put("upazilaId",result.getResultSetValue(rs,"upazilaId"));
				resultSet.put("unionId",result.getResultSetValue(rs,"unionId"));
				resultSet.put("mouzaId",result.getResultSetValue(rs,"mouzaId"));
				resultSet.put("villageId",result.getResultSetValue(rs,"villageId"));
				
				searchList.put(searchList.getString("count"), resultSet);
							
			}
			if(!rs.isClosed()){
				rs.close();
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
}