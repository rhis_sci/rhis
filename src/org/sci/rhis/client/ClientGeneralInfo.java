package org.sci.rhis.client;

import java.sql.ResultSet;
import java.text.SimpleDateFormat;

import org.json.JSONObject;
import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.db.DBOperation;
import org.sci.rhis.db.QueryBuilder;
import org.sci.rhis.util.CalendarDate;
import org.sci.rhis.util.JsonHandler;

/**
 * @author sabah.mugab
 * @since June, 2015
 */
public class ClientGeneralInfo {

	public final static int yearInDays=365;
	
	public static JSONObject retrieveInfo(DBOperation dbOp, DBInfoHandler dbObject, DBInfoHandler dbObject1, JSONObject searchClient, QueryBuilder dynamicQueryBuilder) {
		
		ClientInfoUtil clientHelper = new ClientInfoUtil();
		JSONObject clientInformation = new JSONObject();
		JsonHandler result = new JsonHandler();
				
		try{
			String sql = "SELECT " + dynamicQueryBuilder.getColumn("m", "MEMBER_healthid") + ","
						+ dynamicQueryBuilder.getColumn("m", "MEMBER_nameeng") + ","
						+ dynamicQueryBuilder.getColumn("m", "MEMBER_age") + ","
						+ dynamicQueryBuilder.getColumn("m", "MEMBER_dob") + ","
						+ dynamicQueryBuilder.getColumn("m", "MEMBER_gender") + ","
						+ dynamicQueryBuilder.getColumn("m", "MEMBER_mobilenumber") + ","
						+ dynamicQueryBuilder.getColumn("m", "MEMBER_householdid") + ","
						+ dynamicQueryBuilder.getColumn("m", "MEMBER_zillaid") + ","
						+ dynamicQueryBuilder.getColumn("m", "MEMBER_upazilaid") + ","
						+ dynamicQueryBuilder.getColumn("m", "MEMBER_unionid") + ","
						+ dynamicQueryBuilder.getColumn("m", "MEMBER_mouzaid") + ","
						+ dynamicQueryBuilder.getColumn("m", "MEMBER_villageid") + ","
					    + dynamicQueryBuilder.getColumn("m","MEMBER_mobilenumber2") + ","  //new added from
					    + dynamicQueryBuilder.getColumn("m","MEMBER_providerId") + ","
					    + dynamicQueryBuilder.getColumn("m", "MEMBER_exittype") + ","
					    + dynamicQueryBuilder.getColumn("m","MEMBER_serialnumber") + ","
					    + dynamicQueryBuilder.getColumn("m","MEMBER_nid") + ","
					    + dynamicQueryBuilder.getColumn("m","MEMBER_brid") + ","
					    + dynamicQueryBuilder.getColumn("m","MEMBER_endt") + ","
					    + dynamicQueryBuilder.getColumn("m","MEMBER_modifydate") + ","
					    + dynamicQueryBuilder.getColumn("m","MEMBER_provType") + ","
					    + dynamicQueryBuilder.getColumn("m", "MEMBER_maritalStatus") + "," // for CM_maritalStatus
					    + dynamicQueryBuilder.getColumn("m", "MEMBER_exdate") + ","     //new added done
						+ clientHelper.getFatherSQL(dynamicQueryBuilder)
						+ clientHelper.getMotherSQL(dynamicQueryBuilder)
						+ clientHelper.getSpouseSQL(dynamicQueryBuilder)
						+ "FROM " + dynamicQueryBuilder.getTable("MEMBER") + " AS m "
						+ "WHERE (" 
						//+ dynamicQueryBuilder.getColumn("m", "MEMBER_exittype",new String[]{""},"=")
						//+ " OR " 
						+ dynamicQueryBuilder.getColumn("m", "MEMBER_exittype",new String[]{"0"},"=")
						+ " OR " + dynamicQueryBuilder.getColumn("m", "MEMBER_exittype",new String[]{},"isnull") + ")"
						+ " AND " + searchClient.get("colName") + " = ?";
					
			if(searchClient.getInt("sOpt")==1){
				dbOp.dbCreatePreparedStatement(dbObject, sql).getPreparedStatement().setLong(1,searchClient.getLong("sStr"));
			}
			else{
				if(searchClient.getInt("sOpt")==3||searchClient.getInt("sOpt")==4){
					try{
						searchClient.put("sStr",searchClient.getString("sStr"));
						//dbOp.dbCreatePreparedStatement(dbObject, sql).getPreparedStatement().setString(1,searchClient.getString("sStr")); //no need after introducing new schema 
					}
					catch(NumberFormatException ne){
						searchClient.put("sStr","123");						
					}
				}
				//else //no need after introducing new schema
					dbOp.dbCreatePreparedStatement(dbObject, sql).getPreparedStatement().setLong(1,Long.valueOf(searchClient.getString("sStr")));
			}
			
			ResultSet rs = dbOp.dbExecute(dbObject).getResultSet();
			
			if(rs.next()){
				clientInformation = result.getResponse(rs, clientInformation, "MEMBERINFO", 1);
				clientInformation.put("divisionId", "");
				clientInformation.put("is_registered", "Yes");
				
				if(!clientInformation.getString("cSex").equals("2")){
					clientInformation.put("cHusbandName", "");
				}
				
				if(!clientInformation.getString("cDob").equals("")){
					clientInformation.put("cAge", String.valueOf((CalendarDate.getDaysCount
							(new SimpleDateFormat("yyyy-MM-dd").parse(clientInformation.getString("cDob"))))/yearInDays));
				}
								
				clientInformation = getClientGeoInfo(clientInformation, dbOp, dbObject, dynamicQueryBuilder);

				//Added for healthid to retrieve maritalstatus .. previously its giving marital status for NRC search
				if(!clientInformation.has("cMaritalStatus")){
					String maritalStatus = rs.getString("ms");
					if(maritalStatus!=null){
						if(maritalStatus.equals("5"))
							clientInformation.put("cMaritalStatus","2");
						else
						    clientInformation.put("cMaritalStatus", maritalStatus);

					}else{
						clientInformation.put("cMaritalStatus", "");
					}
				}
				
				sql = "SELECT " + dynamicQueryBuilder.getColumn("cm", "CM_generatedid") + ","
					  + dynamicQueryBuilder.getColumn("cm", "CM_mobileNo") + ","
					  + dynamicQueryBuilder.getColumn("e", "ELCO_elconumber") + ","
					  + dynamicQueryBuilder.getColumn("e", "ELCO_husbandname") + " "
					  + "FROM " + dynamicQueryBuilder.getTable("CM") + " AS cm "
					  + "LEFT JOIN " + dynamicQueryBuilder.getTable("ELCO") + " AS e ON "
					  + dynamicQueryBuilder.getPartialCondition("e", "ELCO_healthid","cm","CM_generatedid","=") + " "
					  + "WHERE " + dynamicQueryBuilder.getColumn("cm", "CM_healthid",new String[]{clientInformation.getString("cVisibleID")},"=");
								
				/**
				 * nullifying the existing object's resultset
				 */
				dbOp.closeResultSet(dbObject);
			
				rs = dbOp.dbExecute(dbObject,sql).getResultSet();
				
				if(rs.next()){
					clientInformation = result.getResponse(rs, clientInformation, "MEMBERINFOELCO", 1);
					clientInformation.put("cHusbandName", clientInformation.getString("eHusbandName").equals("") ? 
														clientInformation.getString("cHusbandName"):clientInformation.getString("eHusbandName"));
					clientInformation.put("cMobileNo", clientInformation.getString("eMobileNo").equals("") ? 
							clientInformation.getString("cMobileNo"):clientInformation.getString("eMobileNo"));									
				}
				else{
					clientInformation.put("cElcoNo", "");
					clientInformation.put("cHusbandName", "");
				}
				clientInformation.put("False","");
			}
			else{
				clientInformation.put("False", "false");
				if(searchClient.getInt("sOpt")==2){
					searchClient.put("colName",(dynamicQueryBuilder.getColumn("table","CM_mobileNo") + "=? OR " + dynamicQueryBuilder.getColumn("table","NRCEXTENSION_cellNo2")));
					clientInformation = ClientMapGeneralInfo.retrieveDetailInfo(dbOp, dbObject, dbObject1, searchClient, dynamicQueryBuilder);
				}				
				else if(searchClient.getInt("sOpt")==3){
					searchClient.put("colName",dynamicQueryBuilder.getColumn("table","NRCEXTENSION_nid"));//"\"clientmap_extension\".\"nid\"");
					clientInformation = ClientMapGeneralInfo.retrieveDetailInfo(dbOp, dbObject, dbObject1, searchClient, dynamicQueryBuilder);
				}
				else if(searchClient.getInt("sOpt")==4){
					searchClient.put("colName",dynamicQueryBuilder.getColumn("table","NRCEXTENSION_brId"));//"\"clientmap_extension\".\"br_id\"");
					clientInformation = ClientMapGeneralInfo.retrieveDetailInfo(dbOp, dbObject, dbObject1, searchClient, dynamicQueryBuilder);
				}
			}
			if(!rs.isClosed()){
				rs.close();
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
		return clientInformation;
	}
	
	public static JSONObject getClientGeoInfo(JSONObject clientInformation, DBOperation dbOp, DBInfoHandler dbObject, QueryBuilder dynamicQueryBuilder){
		
		try{
			String sql = "SELECT " + dynamicQueryBuilder.getColumn("zilla", "ZILLA_zillaname") + " AS \"ZillaName\"," 
					+ dynamicQueryBuilder.getColumn("upazila", "UPAZILA_upazilaname") + " AS \"UpazilaName\","
					+ dynamicQueryBuilder.getColumn("un", "UNION_unionname") + " AS \"UnionName\"," 
					+ dynamicQueryBuilder.getColumn("village", "VILLAGE_villagename") + " AS \"VillageName\" " 
					+ "FROM " + dynamicQueryBuilder.getTable("ZILLA") + " AS zilla "
					+ "LEFT JOIN " + dynamicQueryBuilder.getTable("UPAZILA") + " AS upazila ON "
					+ dynamicQueryBuilder.getPartialCondition("upazila", "UPAZILA_zillaid","zilla","ZILLA_zillaid","=") + " AND "
					+ dynamicQueryBuilder.getColumn("upazila", "UPAZILA_upazilaid",new String[]{clientInformation.getString("upazilaId")},"=")
					+ " LEFT JOIN " + dynamicQueryBuilder.getTable("UNION") + " AS un ON "
					+ dynamicQueryBuilder.getPartialCondition("un", "UNION_zillaid","zilla","ZILLA_zillaid","=") + " AND "
					+ dynamicQueryBuilder.getPartialCondition("un", "UNION_upazilaid","upazila","UPAZILA_upazilaid","=") + " AND "
					+ dynamicQueryBuilder.getColumn("un", "UNION_unionid",new String[]{clientInformation.getString("unionId")},"=")
					+ " LEFT JOIN " + dynamicQueryBuilder.getTable("VILLAGE") + " AS village ON "
					+ dynamicQueryBuilder.getPartialCondition("village", "VILLAGE_zillaid","zilla","ZILLA_zillaid","=") + " AND "
					+ dynamicQueryBuilder.getPartialCondition("village", "VILLAGE_upazilaid","upazila","UPAZILA_upazilaid","=") + " AND "
					+ dynamicQueryBuilder.getPartialCondition("village", "VILLAGE_unionid","un","UNION_unionid","=") + " AND "
					+ dynamicQueryBuilder.getColumn("village", "VILLAGE_mouzaid",new String[]{clientInformation.getString("mouzaId")},"=") + " AND "
					+ dynamicQueryBuilder.getColumn("village", "VILLAGE_villageid",new String[]{clientInformation.getString("villageId")},"=")
					+ " WHERE " + dynamicQueryBuilder.getColumn("zilla", "ZILLA_zillaid",new String[]{clientInformation.getString("zillaId")},"=");
			
			ResultSet rs = dbOp.dbExecute(dbObject,sql).getResultSet();
			
			if(rs.next()){
				clientInformation = new JsonHandler().getResponse(rs, clientInformation, "GEO", 1);
			}	
			else{
				System.out.println("No record");
			}
			if(!rs.isClosed()){
				rs.close();
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return clientInformation;
	}
}