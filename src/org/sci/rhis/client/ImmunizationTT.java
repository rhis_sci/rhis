package org.sci.rhis.client;

import java.sql.ResultSet;

import org.json.JSONObject;
import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.db.DBOperation;
import org.sci.rhis.db.QueryBuilder;
import org.sci.rhis.util.JsonHandler;

/**
 * @author sabah.mugab
 * @since August, 2015
 */
public class ImmunizationTT {
	
	public static JSONObject getImmunizationHistory(DBOperation dbOp, DBInfoHandler dbObject, JSONObject clientInformation){
		
		QueryBuilder dynamicQueryBuilder = new QueryBuilder();
		int i, cnt=0;
		
		try{
			for(i=1;i<=5;i++){
				clientInformation.put("cTT" + i,"");
				clientInformation.put("cTT" + i + "Date","");
			}
			
			String sql = "SELECT " + dynamicQueryBuilder.getColumn("table","TT_ttDate") + ","
						 + dynamicQueryBuilder.getColumn("table","TT_imuDose") + " "
						 + " FROM " + dynamicQueryBuilder.getTable("TT") 
						 + " WHERE " + dynamicQueryBuilder.getColumn("table", "TT_healthId",new String[]{clientInformation.getString("cHealthID")},"=")
						 + " AND " + dynamicQueryBuilder.getColumn("table", "TT_imuCode",new String[]{"16"},"=")
						 + " ORDER BY " + dynamicQueryBuilder.getColumn("table","TT_imuDose") + " DESC LIMIT 5";
				
			ResultSet rs = dbOp.dbExecute(dbObject,sql).getResultSet();
			rs.last();
			cnt = rs.getRow();
			rs.beforeFirst();
			
			while(rs.next()){
				clientInformation.put("cTT" + cnt, "1");
				/**
				 * if rs.getString("imuDate") is null then set "".
				 */
				clientInformation.put("cTT" + cnt + "Date", new JsonHandler().getResultSetValue(rs, dynamicQueryBuilder.getColumn("TT_ttDate")));
				
				cnt = cnt - 1;
			}			
			if(!rs.isClosed()){
				rs.close();
			}			
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return clientInformation;
	}
	
	public static void pushImmunizationHistory(DBOperation dbOp, DBInfoHandler dbObject, JSONObject immuHistory){
		
		String tt = "", ttDate = "";
		immuHistory.put("imuCode", 16);
		immuHistory.put("imuDose", "");
		
		try{
			for(int i=1; i<=5; i++){
				tt = "tt" + i;
				ttDate = "ttDate" + i;
				if(!immuHistory.get(tt).equals("")){
					dbOp.dbStatementExecute(dbObject,new QueryBuilder().getInsertQuery
							(new JsonHandler().addJsonKeyValueEdit(new JsonHandler().addJsonKeyIncrementalField
									(immuHistory,"imuDose"), "TT")));					
				}
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
}