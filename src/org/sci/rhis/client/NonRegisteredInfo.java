package org.sci.rhis.client;

import java.sql.ResultSet;

import org.json.JSONObject;
import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.db.DBOperation;
import org.sci.rhis.db.QueryBuilder;
import org.sci.rhis.db.UpzDBSelector;
import org.sci.rhis.util.JsonHandler;

/**
 * @author sabah.mugab
 * @since August, 2015
 */
public class NonRegisteredInfo {

	public static JSONObject getDetailInfo(JSONObject nonRegisteredInfo) {

		DBOperation dbOp = new DBOperation();
		DBInfoHandler dbObject = new UpzDBSelector().upzDBInfo(nonRegisteredInfo);
		JSONObject tempClient = new JSONObject();
		QueryBuilder dynamicQueryBuilder = new QueryBuilder();
		JsonHandler result = new JsonHandler();
		
		try{
		    //fix of a db error "null generatedid "
		    if(!(nonRegisteredInfo.get("generatedId").equals(null))) {
                nonRegisteredInfo.put("healthId", nonRegisteredInfo.get("generatedId"));
                nonRegisteredInfo.put("treatment_key", "");

                if (nonRegisteredInfo.has("nrcUpdate")) {
                    if (nonRegisteredInfo.getString("nrcUpdate").equals("1")) {
                        dbOp.dbStatementExecute(dbObject, dynamicQueryBuilder.getUpdateQuery(result.addJsonKeyValueEdit(nonRegisteredInfo, "NRC")));
                        if (nonRegisteredInfo.has("memberInfo")) {
                            dbOp.dbStatementExecute(dbObject, dynamicQueryBuilder.getUpdateQuery(result.addJsonKeyValueEdit(nonRegisteredInfo, "NRCEXTENSION")));
                        }
                    }
                }

                ResultSet rs = dbOp.dbExecute(dbObject, dynamicQueryBuilder.getOnConflictInsertQuery(result.addJsonKeyValueEdit(nonRegisteredInfo, "NRC"))).getResultSet();

                if (rs.next()) {
                    JSONObject searchInfo = new JSONObject();
                    searchInfo.put("providerid", rs.getString(dynamicQueryBuilder.getColumn("CM_providerid")));
                    searchInfo.put("sStr", rs.getString(dynamicQueryBuilder.getColumn("CM_generatedid")));
                    searchInfo.put("sOpt", "5");
                    if (nonRegisteredInfo.has("providerType")) {
                        searchInfo.put("providerType", nonRegisteredInfo.get("providerType"));
                        searchInfo.put("zillaid", nonRegisteredInfo.get("zillaid"));
                        searchInfo.put("upazilaid", nonRegisteredInfo.get("upazilaid"));
                        searchInfo.put("unionid", nonRegisteredInfo.get("unionid"));
                    } else {
                        searchInfo.put("providerType", "");
                        searchInfo.put("zillaid", "");
                        searchInfo.put("upazilaid", "");
                        searchInfo.put("unionid", "");
                    }
                    if (nonRegisteredInfo.has("memberInfo")) {
                        dbOp.dbExecute(dbObject, dynamicQueryBuilder.getOnConflictInsertQuery(result.addJsonKeyValueEdit(nonRegisteredInfo, "NRCEXTENSION")));
                    }
                    tempClient = ClientInfo.getDetailInfo(searchInfo);
                }

                if (!rs.isClosed()) {
                    rs.close();
                }
            }else{
                System.out.println("Please send a value of generated ID");
            }

		}
		catch(Exception e){
			e.printStackTrace();
		}
		finally{
			dbOp.dbObjectNullify(dbObject);			
		}				
		return tempClient;
	}
}