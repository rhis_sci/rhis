package org.sci.rhis.client;

import java.sql.ResultSet;
import java.text.SimpleDateFormat;

import org.json.JSONObject;
import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.db.DBOperation;
import org.sci.rhis.db.QueryBuilder;
import org.sci.rhis.util.CalendarDate;
import org.sci.rhis.util.JsonHandler;

/**
 * @author sabah.mugab
 * @since June, 2015
 */
public class UpdatePreg {
	
	public static JSONObject updatePregInfo(DBOperation dbOp, DBInfoHandler dbObject, JSONObject pregInfo) {
		
		JSONObject pInfo = new JSONObject();
		pInfo.put("pregNo", "");
		pInfo.put("responseType", "update");
		pInfo.put("False", "");
		pInfo.put("cHealthID", pregInfo.get("healthId"));
		pInfo.put("highRiskPreg", "No");
		String sql = "";
		QueryBuilder dynamicQueryBuilder = new QueryBuilder();
		JsonHandler result = new JsonHandler();
		
		try{
			String returningSql = " RETURNING " + dynamicQueryBuilder.getColumn("", "PREGWOMEN_pregNo");
			ResultSet rs = dbOp.dbExecute(dbObject,dynamicQueryBuilder.getUpdateQuery(result.addJsonKeyValueEdit(pregInfo, "PREGWOMEN")) + returningSql).getResultSet();
			
			if(rs.next()){
				pInfo.put("pregNo", rs.getInt(dynamicQueryBuilder.getColumn("PREGWOMEN_pregNo")));
			}
			
			//checking the difference between last delivery date and latest lmp
			if(pInfo.getInt("pregNo")>1){
				sql = "SELECT " + dynamicQueryBuilder.getColumn("table","DELIVERY_dDate")
					 + " FROM " + dynamicQueryBuilder.getTable("DELIVERY") 
			         + " WHERE " + dynamicQueryBuilder.getColumn("table", "DELIVERY_healthid",new String[]{pregInfo.getString("healthId")},"=")
			         + " AND " + dynamicQueryBuilder.getColumn("table", "DELIVERY_pregno",new String[]{String.valueOf(pInfo.getInt("pregNo") - 1)},"=");
				
				rs = dbOp.dbExecute(dbObject,sql).getResultSet();
				SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
				
				if(rs.next()){
					int days = CalendarDate.getDaysCount(rs.getDate(dynamicQueryBuilder.getColumn("DELIVERY_dDate")), formatter.parse(pregInfo.getString("lmp")));
					if(days < (365*2)){
						pInfo.put("highRiskPreg", "Yes");
					}					
				}
			}			
			rs.close();
			
			/*pregInfo.put("special", 1);
			sql = "WITH upsert AS (" 
					  + dynamicQueryBuilder.getUpdateQuery(new JsonHandler().addJsonKeyValueEdit(pregInfo, "PREGWOMEN_ELCO")) 
					  + " RETURNING *) " 
					  + dynamicQueryBuilder.getInsertQuery(new JsonHandler().addJsonKeyValueEdit(pregInfo, "PREGWOMEN_ELCO")) 
					  + " WHERE NOT EXISTS (SELECT * FROM upsert)";		    
		    dbOp.dbStatementExecute(dbObject, sql);
		    pregInfo.put("special", null);*/
			dbOp.dbStatementExecute(dbObject, dynamicQueryBuilder.getUpsertQuery(pregInfo, "PREGWOMEN_ELCO"));
		    ClientInfoUtil.updateClientMobileNo(pregInfo, dbOp, dbObject);
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return pInfo;
	}
}