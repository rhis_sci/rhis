package org.sci.rhis.client;

import java.sql.ResultSet;
import java.util.Calendar;

import org.json.JSONObject;
import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.db.DBOperation;
import org.sci.rhis.db.QueryBuilder;
import org.sci.rhis.util.JsonHandler;

/**
 * @author sabah.mugab
 * @since August, 2015
 */
public class CreateRegNo {

	public static boolean pushReg(DBOperation dbOp, DBInfoHandler dbObject, JSONObject serviceRequest, JSONObject serviceDetail) {
		
		boolean status = false;
		QueryBuilder dynamicQueryBuilder = new QueryBuilder();
		serviceRequest.put("serial",0);
						
		try{
			Calendar currentDate = Calendar.getInstance();
	        String sql = "SELECT " + dynamicQueryBuilder.getColumn("","REGISTRATION_SERIAL_serial") + ","
						 + dynamicQueryBuilder.getColumn("","REGISTRATION_SERIAL_systemEntryDate") + " "
						 + " FROM " + dynamicQueryBuilder.getTable("REGISTRATION_SERIAL") 
						 + " WHERE " + dynamicQueryBuilder.getColumn("table", "REGISTRATION_SERIAL_providerId",new String[]{serviceRequest.getString("providerId")},"=")
						 + " AND " + dynamicQueryBuilder.getColumn("table", "REGISTRATION_SERIAL_serviceCategory",new String[]{serviceRequest.getString("serviceCategory")},"=")
						 + " AND (SELECT EXTRACT (year FROM " + dynamicQueryBuilder.getColumn("","REGISTRATION_SERIAL_systemEntryDate") 
						 + ") IN (SELECT EXTRACT(year FROM now()))) "
						 + "ORDER BY " + dynamicQueryBuilder.getColumn("","REGISTRATION_SERIAL_serial") + " DESC";
	        
			ResultSet rs = dbOp.dbExecute(dbObject,sql).getResultSet();
			if(rs.next()){
				
				int maxSerial = rs.getInt(dynamicQueryBuilder.getColumn("REGISTRATION_SERIAL_serial"));				
				Calendar lastEntryDate = Calendar.getInstance();
				lastEntryDate.setTime(rs.getDate(dynamicQueryBuilder.getColumn("REGISTRATION_SERIAL_systemEntryDate")));
				
				if(currentDate.get(Calendar.YEAR) > lastEntryDate.get(Calendar.YEAR)){
					serviceRequest.put("serial",1);					  
				}
				else{
					serviceRequest.put("serial", (maxSerial + 1));					
				}			
			}
			else{
				serviceRequest.put("serial",1);				
			}
			
			rs = dbOp.dbExecute(dbObject, dynamicQueryBuilder.getInsertQuery(new JsonHandler().addJsonKeyValueEdit(serviceRequest, "REGISTRATION_SERIAL"))).getResultSet();
			
			if(rs.next()){
				status = true;
				serviceDetail.put("regSerialNo", rs.getInt(dynamicQueryBuilder.getColumn("REGISTRATION_SERIAL_serial")));
				serviceDetail.put("regDate", rs.getString(dynamicQueryBuilder.getColumn("REGISTRATION_SERIAL_systemEntryDate")));
			}
			else{				
				serviceDetail.put("regSerialNo", "");
				serviceDetail.put("regDate", "");
			}
			
			if(!rs.isClosed()){
				rs.close();
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return status;
	}
}