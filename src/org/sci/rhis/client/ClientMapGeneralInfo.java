package org.sci.rhis.client;

import java.sql.ResultSet;
import java.text.SimpleDateFormat;

import org.json.JSONObject;
import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.db.DBOperation;
import org.sci.rhis.db.QueryBuilder;
import org.sci.rhis.db.UpzDBSelector;
import org.sci.rhis.util.CalendarDate;
import org.sci.rhis.util.JsonHandler;

/**
 * @author sabah.mugab
 * @since August, 2015
 */
public class ClientMapGeneralInfo {

	public final static int yearInDays=365;
	public static JSONObject retrieveDetailInfo(DBOperation dbOp, DBInfoHandler dbObject, DBInfoHandler dbObject1, JSONObject searchClient, QueryBuilder dynamicQueryBuilder) {
		
		JSONObject clientInformation = new JSONObject();
		JsonHandler result = new JsonHandler();
			
		try{
			String sql = "SELECT " + dynamicQueryBuilder.getTable("CM") + ".*, "
					+ dynamicQueryBuilder.getTable("NRCEXTENSION") + ".* "
					+ "FROM " + dynamicQueryBuilder.getTable("CM") + " "
					+ "LEFT JOIN " + dynamicQueryBuilder.getTable("NRCEXTENSION") + " ON "
					+ dynamicQueryBuilder.getPartialCondition("table", "CM_generatedid","table","NRCEXTENSION_generatedId","=") + " "
					+ "WHERE " + searchClient.get("colName") + " = ?"; 
			System.out.println(sql);
			long num = 0;
			try{
				num = searchClient.getLong("sStr");
				if(searchClient.getInt("sOpt") == 1 || searchClient.getInt("sOpt") == 2 || searchClient.getInt("sOpt") == 5){
					dbOp.dbCreatePreparedStatement(dbObject, sql).getPreparedStatement().setLong(1,num);
					if(searchClient.getInt("sOpt") == 2){
						dbOp.dbCreatePreparedStatement(dbObject, sql).getPreparedStatement().setLong(2,num);
					}
				}
				else{
					dbOp.dbCreatePreparedStatement(dbObject, sql).getPreparedStatement().setLong(1,Long.valueOf(searchClient.getString("sStr")));
				}
			}
			catch (NumberFormatException e) {
				if(searchClient.getInt("sOpt") == 1 || searchClient.getInt("sOpt") == 2 || searchClient.getInt("sOpt") == 5){
					dbOp.dbCreatePreparedStatement(dbObject, sql).getPreparedStatement().setLong(1,num);
					if(searchClient.getInt("sOpt") == 2){
						dbOp.dbCreatePreparedStatement(dbObject, sql).getPreparedStatement().setLong(2,num);
					}
				}
				else{
					dbOp.dbCreatePreparedStatement(dbObject, sql).getPreparedStatement().setLong(1,Long.valueOf(searchClient.getString("sStr")));
				}
			}
			
			ResultSet rs = dbOp.dbExecute(dbObject).getResultSet();
						
			if(rs.next()){
				clientInformation.put("False","");
				sql = "SELECT " + dynamicQueryBuilder.getColumn("table", "DEATH_healthId") + " "
					  + "FROM " + dynamicQueryBuilder.getTable("DEATH") + " "
					  + "WHERE " + dynamicQueryBuilder.getColumn("table", "DEATH_healthId",new String[]{result.getResultSetValue(rs,dynamicQueryBuilder.getColumn("CM_generatedid"))},"=")
					  + " AND " + dynamicQueryBuilder.getColumn("table", "DEATH_pregNo",new String[]{"0"},"=")
					  + " AND " + dynamicQueryBuilder.getColumn("table", "DEATH_childNo",new String[]{"0"},"=");
				
				ResultSet rs1 = dbOp.dbExecute(dbOp.dbCreateStatement(dbObject1), sql).getResultSet();
				
				if(rs1.next()){
					clientInformation.put("deathStatus", "1");
				}
				else{
					clientInformation.put("deathStatus", "");
				}
				if(!rs1.isClosed()){
					rs1.close();
				}
				
				/**
				 * nullifying the existing object
				 */
				dbOp.dbObjectNullify(dbObject1);
				
				if(rs.getLong(dynamicQueryBuilder.getColumn("CM_healthid"))==rs.getLong(dynamicQueryBuilder.getColumn("CM_generatedid"))){
					if(searchClient.getInt("sOpt")!=1 && (searchClient.getInt("sOpt")==5 || rs.getObject(dynamicQueryBuilder.getColumn("NRCEXTENSION_generatedId"))!=null)){
						
						clientInformation = result.getResponse(rs, clientInformation, "CLIENTINFO", 1);
						clientInformation.put("is_registered", "No");
						clientInformation.put("cElcoNo", "");
						
						if(!clientInformation.getString("cDob").equals("")){
							clientInformation.put("cAge", String.valueOf((CalendarDate.getDaysCount
									(new SimpleDateFormat("yyyy-MM-dd").parse(clientInformation.getString("cDob"))))/yearInDays));
						}
						
						/**
						 * nullifying the existing object
						 */
						dbOp.dbObjectNullify(dbObject);
						
						clientInformation = ClientGeneralInfo.getClientGeoInfo(clientInformation, dbOp, dbOp.dbCreateStatement(new UpzDBSelector().upzDBInfo(searchClient)), dynamicQueryBuilder);					
					}
					else{
						clientInformation.put("False", "false");
					}
				}
				else{
					if(searchClient.getInt("sOpt")==1){
						searchClient.put("colName",dynamicQueryBuilder.getColumn("m", "MEMBER_healthid"));
						searchClient.put("sStr",rs.getLong(dynamicQueryBuilder.getColumn("CM_healthid")));
						clientInformation = ClientGeneralInfo.retrieveInfo(dbOp, dbObject, dbObject1, searchClient, dynamicQueryBuilder);
					}
					else{
						clientInformation.put("False", "false");
					}
				}				
			}
			else{
				clientInformation.put("False", "false");
			}
			if(!rs.isClosed()){
				rs.close();
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}		
		return clientInformation;
	}
}