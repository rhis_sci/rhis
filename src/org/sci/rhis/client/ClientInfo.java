package org.sci.rhis.client;

import org.json.JSONObject;
import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.db.DBOperation;
import org.sci.rhis.db.QueryBuilder;
import org.sci.rhis.db.UpzDBSelector;

/**
 * @author sabah.mugab
 * @since June, 2015
 */
public class ClientInfo {

	public static JSONObject getDetailInfo(JSONObject searchClient) {
		
		JSONObject clientInformation = new JSONObject();
		
		DBOperation dbOp = new DBOperation();
		DBInfoHandler dbObject = new UpzDBSelector().upzDBInfo(searchClient);
		DBInfoHandler dbObject1 = new UpzDBSelector().upzDBInfo(searchClient);
		QueryBuilder dynamicQueryBuilder = new QueryBuilder();
		
		int searchItem = searchClient.getInt("sOpt");
		searchClient.put("colName","");

		try{
			switch(searchItem){
				case 1:
					searchClient.put("colName",dynamicQueryBuilder.getColumn("table","CM_healthid"));			
					clientInformation = ClientMapGeneralInfo.retrieveDetailInfo(dbOp, dbObject, dbObject1, searchClient, dynamicQueryBuilder);
					break;
				case 2:
					searchClient.put("colName",dynamicQueryBuilder.getColumn("m","MEMBER_mobilenumber"));
					clientInformation = ClientGeneralInfo.retrieveInfo(dbOp, dbObject, dbObject1, searchClient, dynamicQueryBuilder);
					break;
				case 3:
					searchClient.put("colName",dynamicQueryBuilder.getColumn("m","MEMBER_nid"));
					clientInformation = ClientGeneralInfo.retrieveInfo(dbOp, dbObject, dbObject1, searchClient, dynamicQueryBuilder);
					break;
				case 4:
					searchClient.put("colName",dynamicQueryBuilder.getColumn("m","MEMBER_brid"));
					clientInformation = ClientGeneralInfo.retrieveInfo(dbOp, dbObject, dbObject1, searchClient, dynamicQueryBuilder);
					break;
				case 5:
					searchClient.put("colName",dynamicQueryBuilder.getColumn("table","CM_healthid"));
					clientInformation = ClientMapGeneralInfo.retrieveDetailInfo(dbOp, dbObject, dbObject1, searchClient, dynamicQueryBuilder);
					clientInformation.put("idType", "5");
					break;			
			}
			
			if(!clientInformation.has("idType")){
				clientInformation.put("idType", "1");
			}
			searchClient.put("serviceCategory", 1);
			clientInformation = RetrieveRegNo.pullReg(dbOp, dbObject, searchClient,clientInformation);
			
			if(!clientInformation.get("False").equals("false")){
				clientInformation.put("False","");
				clientInformation = ImmunizationTT.getImmunizationHistory(dbOp, dbObject, clientInformation);
				clientInformation = PregInfo.retrievePregInfo(dbOp, dbObject, clientInformation);
			}
			else{
				clientInformation.put("False", "false");			
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
		finally{			
			dbOp.dbObjectNullify(dbObject);
			dbOp.dbObjectNullify(dbObject1);
		}
		return clientInformation;
	}
}