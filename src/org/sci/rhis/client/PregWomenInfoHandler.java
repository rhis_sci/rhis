package org.sci.rhis.client;

import org.json.JSONObject;
import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.db.DBOperation;
import org.sci.rhis.db.QueryBuilder;
import org.sci.rhis.db.UpzDBSelector;
import org.sci.rhis.iud.InsertIUDInfo;
import org.sci.rhis.util.CheckSystemEntryDate;

/**
 * @author sabah.mugab
 * @since June, 2015
 */
public class PregWomenInfoHandler {

    public static JSONObject pregnancyInfo = new JSONObject();
    static final int mnc = 1;

    public static JSONObject handleInsertUpdateOp(JSONObject pregInfo) {

        pregInfo.put("serviceCategory", mnc);
        DBOperation dbOp = new DBOperation();
        DBInfoHandler dbObject = new UpzDBSelector().upzDBInfo(pregInfo);
        QueryBuilder dynamicQueryBuilder = new QueryBuilder();

        try {
            /**
             * creation of new pregnancy information
             */
            if (pregInfo.get("pregNo").equals("")) {

                pregnancyInfo = new JSONObject();
                //check for removing duplication
                if (pregInfo.has("systemEntryDate") && !CheckSystemEntryDate.systemEntryDateExist(pregInfo, dbOp, dbObject, "PREGWOMEN", "PREGWOMEN_systemEntryDate", "PREGWOMEN_healthId")) {
                    pregnancyInfo = CreatePreg.insertPregInfo(dbOp, dbObject, pregInfo);
                } else if (!pregInfo.has("systemEntryDate")) {
                    pregnancyInfo = CreatePreg.insertPregInfo(dbOp, dbObject, pregInfo);
                }
//				pregnancyInfo = CreatePreg.insertPregInfo(dbOp, dbObject, pregInfo);
                CreateRegNo.pushReg(dbOp, dbObject, pregInfo, pregnancyInfo);
            }
            /**
             * update of pregnancy information
             */
            else {
                pregnancyInfo = new JSONObject();
                pregnancyInfo = UpdatePreg.updatePregInfo(dbOp, dbObject, pregInfo);
                RetrieveRegNo.pullReg(dbOp, dbObject, pregInfo, pregnancyInfo);
            }

            /**
             * insert new tt information
             */
            ImmunizationTT.pushImmunizationHistory(dbOp, dbObject, pregInfo);

            /**
             * return tt information
             */
            pregnancyInfo.put("cHealthID", pregInfo.get("healthId"));
            ImmunizationTT.getImmunizationHistory(dbOp, dbObject, pregnancyInfo);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            dbOp.dbObjectNullify(dbObject);
        }
        return pregnancyInfo;
    }
}