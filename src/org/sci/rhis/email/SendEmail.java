package org.sci.rhis.email;

import org.sci.rhis.db.PropertiesReader;

import microsoft.exchange.webservices.data.core.enumeration.property.BodyType;
import microsoft.exchange.webservices.data.core.service.item.EmailMessage;
import microsoft.exchange.webservices.data.property.complex.EmailAddress;
import microsoft.exchange.webservices.data.property.complex.MessageBody;

public class SendEmail extends EmailConnector {
	
	public boolean sendEmailAlert(String subject, String message) {
		try {
			EmailMessage email = new EmailMessage(getExchangeService());
	        email.setSubject(subject);
	        email.setBody(new MessageBody(BodyType.Text, message));
	        email.setFrom(new EmailAddress(PropertiesReader.getPropReader().getEmailConfigurationProperties().getProperty("SENDER_EMAIL"), "Alert"));
	        for (String recipient : PropertiesReader.getPropReader().getEmailConfigurationProperties().getProperty("RECIPIENTS").split(",")) {
	        	email.getToRecipients().add(recipient);
	        }
	        email.send();
	        
	        return true;
		}
	    catch (Exception e) { 
	    	e.printStackTrace(); 
	    	return false; 
	    }
	}
}