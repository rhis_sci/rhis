package org.sci.rhis.email;

import microsoft.exchange.webservices.data.core.ExchangeService;
import microsoft.exchange.webservices.data.core.enumeration.misc.ExchangeVersion;

public interface ExchangeConnect {
	
	ExchangeService service = new ExchangeService(ExchangeVersion.Exchange2007_SP1);
		
	ExchangeService getExchangeService();
	
}