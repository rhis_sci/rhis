package org.sci.rhis.email;

import java.net.URI;

import microsoft.exchange.webservices.data.core.ExchangeService;
import microsoft.exchange.webservices.data.credential.WebCredentials;

import org.sci.rhis.db.PropertiesReader;
import org.sci.rhis.security.AES;
import org.sci.rhis.security.Constant;

public class EmailConnector implements ExchangeConnect {

	@Override
	public ExchangeService getExchangeService() {
		try {
			service.setCredentials(new WebCredentials(PropertiesReader.getPropReader().getEmailConfigurationProperties().getProperty("SENDER_EMAIL"),
									AES.decrypt(PropertiesReader.getPropReader().getEmailConfigurationProperties().getProperty("SENDER_PASSWORD"), Constant.SECRET_KEY)));
	        service.setUrl(new URI(PropertiesReader.getPropReader().getEmailConfigurationProperties().getProperty("EXCHANGE_URI")));
	        return service;
	    }
	    catch (Exception e) {
	    	e.printStackTrace();
	    	return service;
	    }
	}
}