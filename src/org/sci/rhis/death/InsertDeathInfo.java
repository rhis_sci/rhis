package org.sci.rhis.death;

import java.sql.ResultSet;

import org.json.JSONObject;
import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.db.DBOperation;
import org.sci.rhis.db.QueryBuilder;
import org.sci.rhis.util.JsonHandler;

/**
 * @author sabah.mugab
 * @since September, 2015
 */
public class InsertDeathInfo {

	public static JSONObject submitDeathInfo(DBOperation dbOp, DBInfoHandler dbObject, JSONObject deathInfo, QueryBuilder dynamicQueryBuilder) {
		
		JSONObject result = new JSONObject();
		
		if(deathInfo.get("childNo").equals("") || deathInfo.get("childNo").equals("0")){
			deathInfo.put("pregNo","0");
		}
		
		try{				
			ResultSet rs = dbOp.dbExecute(dbObject,dynamicQueryBuilder.getInsertQuery(new JsonHandler().addJsonKeyValueEdit(deathInfo, "DEATH"))).getResultSet();
			
			if(rs.next()){
				result.put("healthId", rs.getString(dynamicQueryBuilder.getColumn("DEATH_healthId")));
				result.put("pregNo", rs.getString(dynamicQueryBuilder.getColumn("DEATH_pregNo")));
				result.put("childNo", rs.getString(dynamicQueryBuilder.getColumn("DEATH_childNo")));
			}
			else{
				result.put("healthId", "");
				result.put("pregNo", "");
				result.put("childNo", "");
			}
			result.put("operation","");
		}
		catch(Exception e){
			e.printStackTrace();
		}				
		return result;
	}
}