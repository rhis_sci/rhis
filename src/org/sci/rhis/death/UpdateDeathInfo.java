package org.sci.rhis.death;

import java.sql.ResultSet;

import org.json.JSONObject;
import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.db.DBOperation;
import org.sci.rhis.db.QueryBuilder;
import org.sci.rhis.util.JsonHandler;

/**
 * @author sabah.mugab
 * @since October, 2015
 */
public class UpdateDeathInfo {

	public static JSONObject updateInfo(DBOperation dbOp, DBInfoHandler dbObject, JSONObject deathInfo, QueryBuilder dynamicQueryBuilder) {
		
		JSONObject result = new JSONObject();
		
		if(deathInfo.get("childNo").equals("") || deathInfo.get("childNo").equals("0")){
			deathInfo.put("pregNo","0");
			deathInfo.put("childNo","0");
		}
		
		try{		
	    	String returningSql = " RETURNING *";
			ResultSet rs = dbOp.dbExecute(dbObject, (dynamicQueryBuilder.getUpdateQuery(
											new JsonHandler().addJsonKeyValueEdit(deathInfo, "DEATH")) 
											+ returningSql)).getResultSet();
			
			if(rs.next()){
				result.put("healthId", rs.getString(dynamicQueryBuilder.getColumn("DEATH_healthId")));
				result.put("pregNo", rs.getString(dynamicQueryBuilder.getColumn("DEATH_pregNo")));
				result.put("childNo", rs.getString(dynamicQueryBuilder.getColumn("DEATH_childNo")));
			}
			else{
				result.put("healthId", "");
				result.put("pregNo", "");
				result.put("childNo", "");
			}
			result.put("operation","update");
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return result;
	}
}