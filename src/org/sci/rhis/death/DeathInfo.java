package org.sci.rhis.death;

import org.json.JSONObject;
import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.db.DBOperation;
import org.sci.rhis.db.QueryBuilder;
import org.sci.rhis.db.UpzDBSelector;
import org.sci.rhis.util.CheckSystemEntryDate;

/**
 * @author sabah.mugab
 * @since September, 2015
 */
public class DeathInfo {
	
	public static JSONObject reportDeath(JSONObject deathInfo) {
		
		JSONObject deathDetail = new JSONObject();
		DBOperation dbOp = new DBOperation();
		DBInfoHandler dbObject = new UpzDBSelector().upzDBInfo(deathInfo);
		QueryBuilder dynamicQueryBuilder = new QueryBuilder();
		
		try{
			if(deathInfo.get("deathLoad").equals("")){
				//check if systementrydate exist in requestjson and DB to remove duplicate entry
				if (deathInfo.has("systemEntryDate") && !CheckSystemEntryDate.systemEntryDateExist(deathInfo, dbOp, dbObject, "DEATH","DEATH_systemEntryDate","DEATH_healthId")) {
					deathDetail = InsertDeathInfo.submitDeathInfo(dbOp, dbObject, deathInfo, dynamicQueryBuilder);
				}else if(!deathInfo.has("systemEntryDate")){
					deathDetail = InsertDeathInfo.submitDeathInfo(dbOp, dbObject, deathInfo, dynamicQueryBuilder);
				}
			}
			else if(deathInfo.get("deathLoad").equals("update")){		
				deathDetail = UpdateDeathInfo.updateInfo(dbOp, dbObject, deathInfo, dynamicQueryBuilder);
			}
			else if(deathInfo.get("deathLoad").equals("retrieveChild")){		
				deathDetail = RetrieveChildInfo.getChild(dbOp, dbObject, deathInfo, dynamicQueryBuilder);
			}
			else{
				deathDetail = RetrieveDeathInfo.getDeath(dbOp, dbObject, deathInfo, dynamicQueryBuilder);
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
		finally{
			dbOp.dbObjectNullify(dbObject);
		}
		return deathDetail;
	}
}