package org.sci.rhis.child;

import java.sql.ResultSet;
import org.json.JSONObject;

import org.sci.rhis.child.InsertChildVisitInfo;
import org.sci.rhis.child.DeleteChildVisitInfo;
import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.db.DBOperation;
import org.sci.rhis.db.QueryBuilder;
import org.sci.rhis.distribution.StockDistributionRequest;
import org.sci.rhis.util.JsonHandler;

/**
 * @author sabah.mugab
 * @since April, 2018
 */
public class UpdateChildVisitInfo {

	public static boolean updateChildVisit(JSONObject childServiceInfo, DBOperation dbOp, DBInfoHandler dbObject, QueryBuilder dynamicQueryBuilder) {
		
		try{		
			String returningSql = " RETURNING *";
			ResultSet rs = dbOp.dbExecute(dbObject,(dynamicQueryBuilder.getUpdateQuery
					(new JsonHandler().addJsonKeyValueEdit(childServiceInfo, "CHILD",false)) + returningSql)).getResultSet();
			
			/*
			 * StockDistributionRequest.updateDistributionInfoHandler(rs,
			 * dynamicQueryBuilder.getColumn("CHILD_treatment"), childServiceInfo, dbOp,
			 * dbObject, dynamicQueryBuilder);
			 */
			
			DeleteChildVisitInfo.deleteChildVisitDetail(rs, childServiceInfo, dbOp, dbObject, dynamicQueryBuilder);
			InsertChildVisitInfo.insertChildVisitDetail(childServiceInfo, dbOp, dbObject, dynamicQueryBuilder);
			
			if(!rs.isClosed()){
				rs.close();
			}
			return true;
		}
		catch(Exception e){
			e.printStackTrace();
			return false;
		}					
	}
}