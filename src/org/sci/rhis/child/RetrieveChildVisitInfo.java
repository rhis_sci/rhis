package org.sci.rhis.child;

import java.sql.ResultSet;

import org.json.JSONObject;
import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.db.DBOperation;
import org.sci.rhis.db.PropertiesReader;
import org.sci.rhis.db.QueryBuilder;
import org.sci.rhis.util.JsonHandler;

/**
 * @author sabah.mugab
 * @since March, 2018
 */
public class RetrieveChildVisitInfo {

	public static JSONObject getChildVisits(JSONObject childServiceInfo, JSONObject childVisits, DBOperation dbOp, DBInfoHandler dbObject, QueryBuilder dynamicQueryBuilder) {
			
		try{
			String sql = "SELECT * FROM " + dynamicQueryBuilder.getTable("CHILD") + " "
						+ "WHERE " + dynamicQueryBuilder.getColumn("table", "CHILD_healthId",new String[]{childServiceInfo.getString("healthId")},"=")
						+ " ORDER BY " + dynamicQueryBuilder.getColumn("table", "CHILD_systemEntryDate") + " ASC";
									
			ResultSet rs = dbOp.dbExecute(dbObject,sql).getResultSet();
			childVisits.put("count", 0);
			//childServiceInfo.put("distributionJson","treatment");			
						
			while(rs.next()){
				childVisits.put(rs.getString(dynamicQueryBuilder.getColumn("CHILD_systemEntryDate")),
						addAdditionalKey(new JsonHandler().getServiceDetail(rs, childServiceInfo, "CHILD", dynamicQueryBuilder, 2)));
				childVisits.put("count", (childVisits.getInt("count")+1));				
			}
			
			sql = "SELECT * FROM " + dynamicQueryBuilder.getTable("CHILDSERVICERECORD") + " "
					+ "WHERE " + dynamicQueryBuilder.getColumn("table", "CHILDSERVICERECORD_healthId",new String[]{childServiceInfo.getString("healthId")},"=")
					+ " ORDER BY " + dynamicQueryBuilder.getColumn("table", "CHILDSERVICERECORD_entryDate") + " ASC";
			
			rs = dbOp.dbExecute(dbObject,sql).getResultSet();
			
			while(rs.next()){
				childVisits.getJSONObject(rs.getString(dynamicQueryBuilder.getColumn("CHILDSERVICERECORD_entryDate"))).
										put(rs.getString(dynamicQueryBuilder.getColumn("CHILDSERVICERECORD_inputId")), rs.getString(dynamicQueryBuilder.getColumn("CHILDSERVICERECORD_inputValue")));
			}
			
			if(!rs.isClosed()){
				rs.close();
			}
			return childVisits;
		}
		catch(Exception e){
			e.printStackTrace();
			return new JSONObject();
		}	
	}
	
	private static JSONObject addAdditionalKey(JSONObject json){
		for (String key : PropertiesReader.getPropReader().getServiceJsonProperties().getProperty("CHILDSERVICERECORD_retrieve_json").split(",")){
			json.put(key, "");
		}				
		return json;
	}
}