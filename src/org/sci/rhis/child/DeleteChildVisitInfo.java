package org.sci.rhis.child;

import java.sql.ResultSet;

import org.json.JSONObject;
import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.db.DBOperation;
import org.sci.rhis.db.QueryBuilder;
import org.sci.rhis.distribution.StockDistributionRequest;
import org.sci.rhis.util.JsonHandler;

/**
 * @author sabah.mugab
 * @since April, 2018
 */
public class DeleteChildVisitInfo {

	public static boolean deleteChildVisit(JSONObject childServiceInfo, DBOperation dbOp, DBInfoHandler dbObject, QueryBuilder dynamicQueryBuilder) {
		
		try{		
			String returningSql = " RETURNING *";
			ResultSet rs = dbOp.dbExecute(dbObject,dynamicQueryBuilder.getDeleteQuery
					(new JsonHandler().addJsonKeyValueEdit(childServiceInfo, "CHILD",false)) + returningSql).getResultSet();
			
			/*StockDistributionRequest.deleteDistributionInfoHandler(rs, dynamicQueryBuilder.getColumn("CHILD_treatment"), 
									childServiceInfo, dbOp, dbObject, dynamicQueryBuilder);*/
			deleteChildVisitDetail(rs, childServiceInfo, dbOp, dbObject, dynamicQueryBuilder);
			
			if(!rs.isClosed()){
				rs.close();
			}
			return true;			
		}
		catch(Exception e){
			e.printStackTrace();
			return false;
		}					
	}
	
	public static boolean deleteChildVisitDetail(ResultSet rs, JSONObject childServiceInfo, DBOperation dbOp, DBInfoHandler dbObject, QueryBuilder dynamicQueryBuilder){
		try{
			rs.beforeFirst();
			if(rs.next()){
				//TODO: Has to test rigorously... 
				childServiceInfo.put("entryDate", childServiceInfo.getString("systemEntryDate"));
				dbOp.dbStatementExecute(dbObject,dynamicQueryBuilder.getDeleteQuery
						(new JsonHandler().addJsonKeyValueEdit(childServiceInfo, "CHILDSERVICERECORD",false)));
			}
			return true;
		}
		catch(Exception e){
			e.printStackTrace();
			return false;
		}		
	}
}