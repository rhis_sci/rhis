package org.sci.rhis.child;

import java.sql.ResultSet;

import org.json.JSONObject;
import org.sci.rhis.client.ClientInfoUtil;
import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.db.DBOperation;
import org.sci.rhis.db.PropertiesReader;
import org.sci.rhis.db.QueryBuilder;
import org.sci.rhis.distribution.StockDistributionRequest;
import org.sci.rhis.util.JsonHandler;

/**
 * @author sabah.mugab
 * @since March, 2018
 */
public class InsertChildVisitInfo {
	
	public static JSONObject createChildVisit(JSONObject childServiceInfo, DBOperation dbOp, DBInfoHandler dbObject, QueryBuilder dynamicQueryBuilder) {
		
		try{	
			ResultSet rs = dbOp.dbExecute(dbObject, dynamicQueryBuilder.getInsertQuery
					(new JsonHandler().addJsonKeyValueEdit(new JsonHandler().addJsonKeyIncrementalField
					(childServiceInfo,"serviceId"), "CHILD"))).getResultSet();

			/* TODO: Has to check when stockservice is available */
//			childServiceInfo = StockDistributionRequest.insertDistributionInfoHandler(rs, dynamicQueryBuilder.getColumn("CHILD_treatment"),
//									dynamicQueryBuilder.getColumn("CHILD_serviceId"), childServiceInfo, dbOp, dbObject, dynamicQueryBuilder);
			
			insertChildVisitDetail(childServiceInfo, dbOp, dbObject, dynamicQueryBuilder);
					
			if(!rs.isClosed()){
				rs.close();
			}
			
			if(!childServiceInfo.get("mobileNo").equals("")){			
				ClientInfoUtil.updateClientMobileNo(childServiceInfo, dbOp, dbObject);
			}
			return childServiceInfo;
		}
		catch(Exception e){
			e.printStackTrace();
			return childServiceInfo;
		}			
	}
	
	public static boolean insertChildVisitDetail(JSONObject childServiceInfo, DBOperation dbOp, DBInfoHandler dbObject, QueryBuilder dynamicQueryBuilder){
		try{
			String sql = "";
			String commonVal = "";
			
			if(!childServiceInfo.getString("serviceId").equals("")){
				childServiceInfo.put("entryDate", childServiceInfo.getString("systemEntryDate"));
				sql = dynamicQueryBuilder.getMultiInsertQuery(new JsonHandler().addJsonKeyValueEdit(childServiceInfo, "CHILDSERVICERECORD"));
				
				commonVal = getValue("CHILDSERVICERECORD_healthId",childServiceInfo.getString("healthId")) + "," 
							+ getValue("CHILDSERVICERECORD_systemEntryDate",childServiceInfo.getString("entryDate")) + ",";
				
				for (String key : PropertiesReader.getPropReader().getServiceJsonProperties().getProperty("CHILDSERVICERECORD_insert_variable_json").split(",")){
					if(!childServiceInfo.getString(key).equals("")){
						sql = sql + "(" + commonVal + getValue("CHILDSERVICERECORD_inputId",key) + ","
									+ getValue("CHILDSERVICERECORD_inputValue",childServiceInfo.getString(key)) + "),";
					}
				}
				dbOp.dbStatementExecute(dbObject, sql.substring(0, sql.length()-1));
			}			
			return true;
		}
		catch(Exception e){
			e.printStackTrace();
			return false;
		}		
	}
	
	private static String getValue(String column, String val){
		String[] columnDetail = PropertiesReader.getPropReader().getServiceJsonMapProperties().getProperty(column).split(",");
		return val.equals("") ? columnDetail[2] : (columnDetail[1].equals("int") ? val : ("'" + val + "'"));		
	}
}