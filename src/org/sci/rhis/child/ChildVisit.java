package org.sci.rhis.child;

import org.json.JSONObject;
import org.sci.rhis.client.ClientInfoUtil;
import org.sci.rhis.client.CreateRegNo;
import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.db.DBOperation;
import org.sci.rhis.db.JSONKeyMapper;
import org.sci.rhis.db.QueryBuilder;
import org.sci.rhis.db.UpzDBSelector;
import org.sci.rhis.util.CheckSystemEntryDate;
import org.sci.rhis.util.JsonHandler;
import org.sci.rhis.womaninjectable.InsertInjectableVisitInfo;

/**
 * @author sabah.mugab
 * @since March, 2018
 */
public class ChildVisit {
	
	final static int childService = 7; //for registration number
	final static int CHILDSERVICETYPE = 13; //for item distribution
	
	public static JSONObject getDetailInfo(JSONObject childServiceInfo) {
		
		JSONObject childVisits = new JSONObject();
		childServiceInfo = new JsonHandler().addJsonKeyValueStockDistribution(new JSONKeyMapper().setRequiredKeys(childServiceInfo, "CHILD"), CHILDSERVICETYPE);
		DBOperation dbOp = new DBOperation();
		DBInfoHandler dbObject = new UpzDBSelector().upzDBInfo(childServiceInfo);
		QueryBuilder dynamicQueryBuilder = new QueryBuilder();
		childServiceInfo.put("serviceCategory", childService);
		
		try{
			if(childServiceInfo.get("childLoad").equals("insert")){
				childServiceInfo = InsertChildVisitInfo.createChildVisit(childServiceInfo, dbOp, dbObject, dynamicQueryBuilder);
				/*
				 * if(childServiceInfo.getString("serviceId").equals("1")){
				 * CreateRegNo.pushReg(dbOp, dbObject, childServiceInfo, childVisits); }
				 */

				//check if systementrydate exist in requestjson and DB to remove duplicate entry
				if (childServiceInfo.has("systemEntryDate") && !CheckSystemEntryDate.systemEntryDateExist(childServiceInfo, dbOp, dbObject, "CHILD","CHILD_systemEntryDate","CHILD_healthId")) {
					childServiceInfo = InsertChildVisitInfo.createChildVisit(childServiceInfo, dbOp, dbObject, dynamicQueryBuilder);
				}else if(!childServiceInfo.has("systemEntryDate")){
					childServiceInfo = InsertChildVisitInfo.createChildVisit(childServiceInfo,dbOp, dbObject, dynamicQueryBuilder);
				}
			}
			else if(childServiceInfo.get("childLoad").equals("update")){
				UpdateChildVisitInfo.updateChildVisit(childServiceInfo, dbOp, dbObject, dynamicQueryBuilder);
			}
			else if(childServiceInfo.get("childLoad").equals("delete")){
				DeleteChildVisitInfo.deleteChildVisit(childServiceInfo, dbOp, dbObject, dynamicQueryBuilder);
			}			
			
			RetrieveChildVisitInfo.getChildVisits(childServiceInfo,childVisits, dbOp, dbObject, dynamicQueryBuilder);
			//ClientInfoUtil.getRegNumber(dbOp, dbObject, childServiceInfo, childVisits);
			
			return childVisits;
		}
		catch(Exception e){
			e.printStackTrace();
			return new JSONObject();
		}
		finally{
			dbOp.dbObjectNullify(dbObject);		
		}		
	}
}