package org.sci.rhis.report.mis3;

import java.sql.ResultSet;

import org.json.JSONObject;
import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.db.DBOperation;

/**
 * @author sabah.mugab
 * @since February, 2016
 */
public class MIS3ANC {
	
	public static void getANCReport(JSONObject MIS3RequestInfo, JSONObject MIS3Report, DBOperation dbOp, DBInfoHandler dbObject){
	
		JSONObject MIS3ReportANC = new JSONObject();
		JSONObject individualProviderName = new JSONObject();
		boolean isRSEmpty = true;
		
		try{
			MIS3Report.put("providerlist", MIS3RequestInfo.get("providerId"));
						
			String sql = "SELECT \"ancService\".\"providerId\", \"ProviderDB\".\"ProvName\","
						+ "COUNT(CASE WHEN (\"ancService\".\"serviceId\"=1 AND \"ancService\".\"sateliteCenterName\" IS NULL) THEN 1 ELSE NULL END) AS anc1center,"
						+ "COUNT(CASE WHEN (\"ancService\".\"serviceId\"=1 AND \"ancService\".\"sateliteCenterName\" IS NOT NULL) THEN 1 ELSE NULL END) AS anc1satelite,"
						+ "COUNT(CASE WHEN (\"ancService\".\"serviceId\"=2 AND \"ancService\".\"sateliteCenterName\" IS NULL) THEN 1 ELSE NULL END) AS anc2center,"
						+ "COUNT(CASE WHEN (\"ancService\".\"serviceId\"=2 AND \"ancService\".\"sateliteCenterName\" IS NOT NULL) THEN 1 ELSE NULL END) AS anc2satelite,"
						+ "COUNT(CASE WHEN (\"ancService\".\"serviceId\"=3 AND \"ancService\".\"sateliteCenterName\" IS NULL) THEN 1 ELSE NULL END) AS anc3center,"
						+ "COUNT(CASE WHEN (\"ancService\".\"serviceId\"=3 AND \"ancService\".\"sateliteCenterName\" IS NOT NULL) THEN 1 ELSE NULL END) AS anc3satelite,"
						+ "COUNT(CASE WHEN (\"ancService\".\"serviceId\"=4 AND \"ancService\".\"sateliteCenterName\" IS NULL) THEN 1 ELSE NULL END) AS anc4center,"
						+ "COUNT(CASE WHEN (\"ancService\".\"serviceId\"=4 AND \"ancService\".\"sateliteCenterName\" IS NOT NULL) THEN 1 ELSE NULL END) AS anc4satelite,"
						+ "COUNT(CASE WHEN (\"ancService\".\"refer\"=1 AND \"ancService\".\"sateliteCenterName\" IS NULL) THEN 1 ELSE NULL END) AS ancRefercenter,"
						+ "COUNT(CASE WHEN (\"ancService\".\"refer\"=1 AND \"ancService\".\"sateliteCenterName\" IS NOT NULL) THEN 1 ELSE NULL END) AS ancRefersatelite "
						+ "FROM \"ancService\",\"ProviderDB\" "
						+ "WHERE \"ancService\".\"providerId\" IN ( " + MIS3RequestInfo.get("providerId") + ") "
						+ "AND (\"ancService\".\"serviceSource\" IS NULL OR \"ancService\".\"serviceSource\" = '0') "
						+ "AND \"ancService\".\"visitDate\" BETWEEN ('" + MIS3RequestInfo.get("startDate") + "') AND ('" + MIS3RequestInfo.get("endDate") +"') "
						+ "AND \"ancService\".\"providerId\" = \"ProviderDB\".\"ProvCode\""
						+ "GROUP BY \"ancService\".\"providerId\", \"ProviderDB\".\"ProvName\"";
			
			ResultSet rs = dbOp.dbExecute(dbObject,sql).getResultSet();
					
			while(rs.next()){
				
				isRSEmpty = false;
				
				JSONObject individualProviderANC = new JSONObject();
				individualProviderName = new JSONObject();
				
				individualProviderName.put(rs.getString("providerId"), rs.getString("ProvName"));
				
				individualProviderANC.put("ProvName", rs.getString("ProvName"));
				individualProviderANC.put("anc1center", rs.getString("anc1center"));
				individualProviderANC.put("anc2center", rs.getString("anc2center"));
				individualProviderANC.put("anc3center", rs.getString("anc3center"));
				individualProviderANC.put("anc4center", rs.getString("anc4center"));
				
				individualProviderANC.put("anc1satelite", rs.getString("anc1satelite"));
				individualProviderANC.put("anc2satelite", rs.getString("anc2satelite"));
				individualProviderANC.put("anc3satelite", rs.getString("anc3satelite"));
				individualProviderANC.put("anc4satelite", rs.getString("anc4satelite"));
				
				individualProviderANC.put("ancrefercenter", rs.getString("ancRefercenter"));
				individualProviderANC.put("ancrefersatelite", rs.getString("ancRefersatelite"));
				
				individualProviderANC.put("hasancdata", 1);
				
				MIS3ReportANC.put(rs.getString("providerId"), individualProviderANC);
			}
			
			if(isRSEmpty){
				String[] providerids = MIS3RequestInfo.getString("providerId").split(",");
				for(String id:providerids){
					
					JSONObject individualProviderANCStatus = new JSONObject();
					individualProviderANCStatus.put("hasancdata", 2);
					
					MIS3ReportANC.put(id, individualProviderANCStatus);
				}
			}
			MIS3Report.put("anc", MIS3ReportANC);
			MIS3Report.put("providernamelist", individualProviderName);
			
			if(!rs.isClosed()){
				rs.close();
			}
		}
		catch(Exception e){
			System.out.println(e);
			e.printStackTrace();
		}						
	}
}