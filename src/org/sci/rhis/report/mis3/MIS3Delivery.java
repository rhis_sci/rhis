package org.sci.rhis.report.mis3;

import java.sql.ResultSet;

import org.json.JSONObject;
import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.db.DBOperation;

/**
 * @author sabah.mugab
 * @since February, 2016
 */
public class MIS3Delivery {
	
	final static int normalDelivery = 1;
	final static int csectionDelivery = 2;
	final static int applyOxytocinYes = 1;
	final static int applyTractionYes = 1;
	final static int applyUterusMassageYes = 1;
	final static int deliveryDoneByThisProviderYes = 1;
		
	public static void getDeliveryReport(JSONObject MIS3RequestInfo, JSONObject MIS3Report, DBOperation dbOp, DBInfoHandler dbObject){
			
		JSONObject MIS3ReportDelivery = new JSONObject();
		boolean isRSEmpty = true;
		
		try{
			String sql = "SELECT \"delivery\".\"providerId\", \"ProviderDB\".\"ProvName\"," 
						+ "COUNT(CASE WHEN (\"delivery\".\"outcomeType\"=" + normalDelivery + ") THEN 1 ELSE NULL END) AS normaldelivery, "
						+ "COUNT(CASE WHEN (\"delivery\".\"outcomeType\"=" + csectionDelivery + ") THEN 1 ELSE NULL END) AS csectiondelivery, "
						+ "COUNT(CASE WHEN (\"delivery\".\"applyOxytocin\"=" + applyOxytocinYes + " "
								+ "AND \"delivery\".\"applyTraction\"=" + applyTractionYes + " "
								+ "AND \"delivery\".\"uterusMassage\"=" + applyUterusMassageYes + ") THEN 1 ELSE NULL END) AS amtsl, "
						+ "(CASE WHEN (SUM(\"delivery\".\"liveBirth\") IS NOT NULL) THEN SUM(\"delivery\".\"liveBirth\") ELSE 0 END) AS livebirth, "
						+ "(CASE WHEN (SUM(\"delivery\".\"stillBirth\") IS NOT NULL) THEN SUM(\"delivery\".\"stillBirth\") ELSE 0 END) AS stillbirth,"
						+ "COUNT(CASE WHEN (\"delivery\".\"refer\"=1 AND \"delivery\".\"sateliteCenterName\" IS NULL) THEN 1 ELSE NULL END) AS deliveryRefercenter,"
						+ "COUNT(CASE WHEN (\"delivery\".\"refer\"=1 AND \"delivery\".\"sateliteCenterName\" IS NOT NULL) THEN 1 ELSE NULL END) AS deliveryRefersatelite "
						+ "FROM \"delivery\",\"ProviderDB\"  "
						+ "WHERE \"delivery\".\"providerId\" IN ( " + MIS3RequestInfo.get("providerId") + ") "
						+ "AND \"delivery\".\"deliveryDoneByThisProvider\" = " + deliveryDoneByThisProviderYes + " "
						+ "AND \"delivery\".\"outcomeDate\" BETWEEN ('" + MIS3RequestInfo.get("startDate") + "') AND ('" + MIS3RequestInfo.get("endDate") +"') "
						+ "AND \"delivery\".\"providerId\" = \"ProviderDB\".\"ProvCode\""
						+ "GROUP BY \"delivery\".\"providerId\", \"ProviderDB\".\"ProvName\"";
			
			ResultSet rs = dbOp.dbExecute(dbObject,sql).getResultSet();
				
			while(rs.next()){
				
				isRSEmpty = false;
				JSONObject individualProviderDelivery = new JSONObject();
				
				individualProviderDelivery.put("ProvName", rs.getString("ProvName"));
				individualProviderDelivery.put("normaldelivery", rs.getString("normaldelivery"));
				individualProviderDelivery.put("csectiondelivery", rs.getString("csectiondelivery"));
				individualProviderDelivery.put("amtsl", rs.getString("amtsl"));
				individualProviderDelivery.put("livebirth", rs.getString("livebirth"));
				individualProviderDelivery.put("stillbirth", rs.getString("stillbirth"));
				
				individualProviderDelivery.put("deliveryrefercenter", rs.getString("deliveryRefercenter"));
				individualProviderDelivery.put("deliveryrefersatelite", rs.getString("deliveryRefersatelite"));
				
				individualProviderDelivery.put("hasdeliverydata", 1);
				
				MIS3ReportDelivery.put(rs.getString("providerId"), individualProviderDelivery);
			}
			
			if(isRSEmpty){
				String[] providerids = MIS3RequestInfo.getString("providerId").split(",");
				for(String id:providerids){
					
					JSONObject individualProviderDeliveryStatus = new JSONObject();
					individualProviderDeliveryStatus.put("hasdeliverydata", 2);
					
					MIS3ReportDelivery.put(id, individualProviderDeliveryStatus);
				}
			}
			
			MIS3Report.put("delivery", MIS3ReportDelivery);
			
			if(!rs.isClosed()){
				rs.close();
			}
		}
		catch(Exception e){
			System.out.println(e);
			e.printStackTrace();
		}						
	}
}