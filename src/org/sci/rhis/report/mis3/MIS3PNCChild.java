package org.sci.rhis.report.mis3;

import java.sql.ResultSet;

import org.json.JSONObject;
import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.db.DBOperation;

/**
 * @author sabah.mugab
 * @since February, 2016
 */
public class MIS3PNCChild {
	
	public static void getPNCChildReport(JSONObject MIS3RequestInfo, JSONObject MIS3Report, DBOperation dbOp, DBInfoHandler dbObject){
	
		JSONObject MIS3ReportPNCChild = new JSONObject();
		boolean isRSEmpty = true;
		
		try{
			String sql = "SELECT \"pncServiceChild\".\"providerId\", \"ProviderDB\".\"ProvName\","
					+ "COUNT(CASE WHEN (\"pncServiceChild\".\"serviceId\"=1 AND \"pncServiceChild\".\"sateliteCenterName\" IS NULL) THEN 1 ELSE NULL END) AS pncchild1center,"
					+ "COUNT(CASE WHEN (\"pncServiceChild\".\"serviceId\"=1 AND \"pncServiceChild\".\"sateliteCenterName\" IS NOT NULL) THEN 1 ELSE NULL END) AS pncchild1satelite,"
					+ "COUNT(CASE WHEN (\"pncServiceChild\".\"serviceId\"=2 AND \"pncServiceChild\".\"sateliteCenterName\" IS NULL) THEN 1 ELSE NULL END) AS pncchild2center,"
					+ "COUNT(CASE WHEN (\"pncServiceChild\".\"serviceId\"=2 AND \"pncServiceChild\".\"sateliteCenterName\" IS NOT NULL) THEN 1 ELSE NULL END) AS pncchild2satelite,"
					+ "COUNT(CASE WHEN (\"pncServiceChild\".\"serviceId\"=3 AND \"pncServiceChild\".\"sateliteCenterName\" IS NULL) THEN 1 ELSE NULL END) AS pncchild3center,"
					+ "COUNT(CASE WHEN (\"pncServiceChild\".\"serviceId\"=3 AND \"pncServiceChild\".\"sateliteCenterName\" IS NOT NULL) THEN 1 ELSE NULL END) AS pncchild3satelite,"
					+ "COUNT(CASE WHEN (\"pncServiceChild\".\"serviceId\"=4 AND \"pncServiceChild\".\"sateliteCenterName\" IS NULL) THEN 1 ELSE NULL END) AS pncchild4center,"
					+ "COUNT(CASE WHEN (\"pncServiceChild\".\"serviceId\"=4 AND \"pncServiceChild\".\"sateliteCenterName\" IS NOT NULL) THEN 1 ELSE NULL END) AS pncchild4satelite "
					+ "FROM \"pncServiceChild\",\"ProviderDB\" "
					+ "WHERE \"pncServiceChild\".\"providerId\" IN ( " + MIS3RequestInfo.get("providerId") + ") "
					+ "AND (\"pncServiceChild\".\"serviceSource\" IS NULL OR \"pncServiceChild\".\"serviceSource\" = '0') "
					+ "AND \"pncServiceChild\".\"visitDate\" BETWEEN ('" + MIS3RequestInfo.get("startDate") + "') AND ('" + MIS3RequestInfo.get("endDate") +"') "
					+ "AND \"pncServiceChild\".\"providerId\" = \"ProviderDB\".\"ProvCode\" "
					+ "GROUP BY \"pncServiceChild\".\"providerId\", \"ProviderDB\".\"ProvName\"";
			
			ResultSet rs = dbOp.dbExecute(dbObject,sql).getResultSet();
				
			while(rs.next()){
			
				isRSEmpty = false;
				JSONObject individualProviderPNCChild = new JSONObject();
				
				individualProviderPNCChild.put("ProvName", rs.getString("ProvName"));
				individualProviderPNCChild.put("pncchild1center", rs.getString("pncchild1center"));
				individualProviderPNCChild.put("pncchild2center", rs.getString("pncchild2center"));
				individualProviderPNCChild.put("pncchild3center", rs.getString("pncchild3center"));
				individualProviderPNCChild.put("pncchild4center", rs.getString("pncchild4center"));
				
				individualProviderPNCChild.put("pncchild1satelite", rs.getString("pncchild1satelite"));
				individualProviderPNCChild.put("pncchild2satelite", rs.getString("pncchild2satelite"));
				individualProviderPNCChild.put("pncchild3satelite", rs.getString("pncchild3satelite"));
				individualProviderPNCChild.put("pncchild4satelite", rs.getString("pncchild4satelite"));
								
				individualProviderPNCChild.put("haspncchilddata", 1);
				
				MIS3ReportPNCChild.put(rs.getString("providerId"), individualProviderPNCChild);
			}
			
			if(isRSEmpty){
				String[] providerids = MIS3RequestInfo.getString("providerId").split(",");
				for(String id:providerids){
					
					JSONObject individualProviderPNCChildStatus = new JSONObject();
					individualProviderPNCChildStatus.put("haspncchilddata", 2);
					
					MIS3ReportPNCChild.put(id, individualProviderPNCChildStatus);
				}
			}		
			
			MIS3Report.put("pncchild", MIS3ReportPNCChild);
			
			if(!rs.isClosed()){
				rs.close();
			}
		}
		catch(Exception e){
			System.out.println(e);
			e.printStackTrace();
		}					
	}
}