package org.sci.rhis.report.mis3;

import java.sql.ResultSet;

import org.json.JSONObject;
import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.db.DBOperation;

/**
 * @author sabah.mugab
 * @since February, 2016
 */
public class MIS3PNCMother {
	
	public static void getPNCMotherReport(JSONObject MIS3RequestInfo, JSONObject MIS3Report, DBOperation dbOp, DBInfoHandler dbObject){
			
		JSONObject MIS3ReportPNCMother = new JSONObject();
		boolean isRSEmpty = true;
		
		try{
			String sql = "SELECT \"pncServiceMother\".\"providerId\", \"ProviderDB\".\"ProvName\","
					+ "COUNT(CASE WHEN (\"pncServiceMother\".\"serviceId\"=1 AND \"pncServiceMother\".\"sateliteCenterName\" IS NULL) THEN 1 ELSE NULL END) AS pncmother1center,"
					+ "COUNT(CASE WHEN (\"pncServiceMother\".\"serviceId\"=1 AND \"pncServiceMother\".\"sateliteCenterName\" IS NOT NULL) THEN 1 ELSE NULL END) AS pncmother1satelite,"
					+ "COUNT(CASE WHEN (\"pncServiceMother\".\"serviceId\"=2 AND \"pncServiceMother\".\"sateliteCenterName\" IS NULL) THEN 1 ELSE NULL END) AS pncmother2center,"
					+ "COUNT(CASE WHEN (\"pncServiceMother\".\"serviceId\"=2 AND \"pncServiceMother\".\"sateliteCenterName\" IS NOT NULL) THEN 1 ELSE NULL END) AS pncmother2satelite,"
					+ "COUNT(CASE WHEN (\"pncServiceMother\".\"serviceId\"=3 AND \"pncServiceMother\".\"sateliteCenterName\" IS NULL) THEN 1 ELSE NULL END) AS pncmother3center,"
					+ "COUNT(CASE WHEN (\"pncServiceMother\".\"serviceId\"=3 AND \"pncServiceMother\".\"sateliteCenterName\" IS NOT NULL) THEN 1 ELSE NULL END) AS pncmother3satelite,"
					+ "COUNT(CASE WHEN (\"pncServiceMother\".\"serviceId\"=4 AND \"pncServiceMother\".\"sateliteCenterName\" IS NULL) THEN 1 ELSE NULL END) AS pncmother4center,"
					+ "COUNT(CASE WHEN (\"pncServiceMother\".\"serviceId\"=4 AND \"pncServiceMother\".\"sateliteCenterName\" IS NOT NULL) THEN 1 ELSE NULL END) AS pncmother4satelite,"
					+ "COUNT(CASE WHEN (\"pncServiceMother\".\"FPMethod\"<>'0' AND \"pncServiceMother\".\"FPMethod\"<>'1' AND \"pncServiceMother\".\"FPMethod\" IS NOT NULL AND \"pncServiceMother\".\"sateliteCenterName\" IS NULL) THEN 1 ELSE NULL END) AS pncmotherFPcenter,"
					+ "COUNT(CASE WHEN (\"pncServiceMother\".\"FPMethod\"<>'0' AND \"pncServiceMother\".\"FPMethod\"<>'1' AND \"pncServiceMother\".\"FPMethod\" IS NOT NULL AND \"pncServiceMother\".\"sateliteCenterName\" IS NOT NULL) THEN 1 ELSE NULL END) AS pncmotherFPsatelite,"
					+ "COUNT(CASE WHEN (\"pncServiceMother\".\"refer\"=1 AND \"pncServiceMother\".\"sateliteCenterName\" IS NULL) THEN 1 ELSE NULL END) AS pncmotherRefercenter,"
					+ "COUNT(CASE WHEN (\"pncServiceMother\".\"refer\"=1 AND \"pncServiceMother\".\"sateliteCenterName\" IS NOT NULL) THEN 1 ELSE NULL END) AS pncmotherRefersatelite "
					+ "FROM \"pncServiceMother\",\"ProviderDB\" "
					+ "WHERE \"pncServiceMother\".\"providerId\" IN ( " + MIS3RequestInfo.get("providerId") + ") "
					+ "AND (\"pncServiceMother\".\"serviceSource\" IS NULL OR \"pncServiceMother\".\"serviceSource\" = '0') "
					+ "AND \"pncServiceMother\".\"visitDate\" BETWEEN ('" + MIS3RequestInfo.get("startDate") + "') AND ('" + MIS3RequestInfo.get("endDate") +"') "
					+ "AND \"pncServiceMother\".\"providerId\" = \"ProviderDB\".\"ProvCode\""
					+ "GROUP BY \"pncServiceMother\".\"providerId\", \"ProviderDB\".\"ProvName\"";
			
			ResultSet rs = dbOp.dbExecute(dbObject,sql).getResultSet();
				
			while(rs.next()){
			
				isRSEmpty = false;
				JSONObject individualProviderPNCMother = new JSONObject();
				
				individualProviderPNCMother.put("ProvName", rs.getString("ProvName"));
				individualProviderPNCMother.put("pncmother1center", rs.getString("pncmother1center"));
				individualProviderPNCMother.put("pncmother2center", rs.getString("pncmother2center"));
				individualProviderPNCMother.put("pncmother3center", rs.getString("pncmother3center"));
				individualProviderPNCMother.put("pncmother4center", rs.getString("pncmother4center"));
				
				individualProviderPNCMother.put("pncmother1satelite", rs.getString("pncmother1satelite"));
				individualProviderPNCMother.put("pncmother2satelite", rs.getString("pncmother2satelite"));
				individualProviderPNCMother.put("pncmother3satelite", rs.getString("pncmother3satelite"));
				individualProviderPNCMother.put("pncmother4satelite", rs.getString("pncmother4satelite"));
				
				individualProviderPNCMother.put("pncmotherfpcenter", rs.getString("pncmotherFPcenter"));
				individualProviderPNCMother.put("pncmotherfpsatelite", rs.getString("pncmotherFPsatelite"));
				
				individualProviderPNCMother.put("pncmotherrefercenter", rs.getString("pncmotherRefercenter"));
				individualProviderPNCMother.put("pncmotherrefersatelite", rs.getString("pncmotherRefersatelite"));
				
				individualProviderPNCMother.put("haspncmotherdata", 1);
				
				MIS3ReportPNCMother.put(rs.getString("providerId"), individualProviderPNCMother);
			}
			
			if(isRSEmpty){
				String[] providerids = MIS3RequestInfo.getString("providerId").split(",");
				for(String id:providerids){
					
					JSONObject individualProviderPNCMotherStatus = new JSONObject();
					individualProviderPNCMotherStatus.put("haspncmotherdata", 2);
					
					MIS3ReportPNCMother.put(id, individualProviderPNCMotherStatus);
				}
			}
			
			MIS3Report.put("pncmother", MIS3ReportPNCMother);
			
			if(!rs.isClosed()){
				rs.close();
			}
		}
		catch(Exception e){
			System.out.println(e);
			e.printStackTrace();
		}			
	}
}