package org.sci.rhis.report.mis3;

import java.sql.ResultSet;

import org.json.JSONObject;
import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.db.DBOperation;

/**
 * @author sabah.mugab
 * @since February, 2016
 */
public class MIS3NewbornRefer {
	
	final static int referYes = 1;
	final static int newborn = 28;
	
	public static void getReferReport(JSONObject MIS3RequestInfo, JSONObject MIS3Report, DBOperation dbOp, DBInfoHandler dbObject){
	
		JSONObject MIS3NewbornRefer = new JSONObject();
		boolean isRSEmpty = true;
		
		try{
			String sql = " SELECT DISTINCT(\"newbornReferTablePNC\".\"providerId\"),\"newbornReferTable\".\"newbornRefer\","
						+ "\"newbornReferTablePNC\".\"pncnewbornReferCenter\",\"newbornReferTablePNC\".\"pncnewbornReferSatelite\" "
						+ "FROM "
							+ "(SELECT \"pncServiceChild\".\"providerId\", "
							+ "COUNT(CASE WHEN (\"pncServiceChild\".\"refer\" = " + referYes + " "
							+ "AND (\"pncServiceChild\".\"visitDate\" - \"newBorn\".\"outcomeDate\") <= " + newborn + " "
							+ "AND \"pncServiceChild\".\"sateliteCenterName\" IS NULL)"
							+ "THEN 1 ELSE NULL END) AS \"pncnewbornReferCenter\","
							+ "COUNT(CASE WHEN (\"pncServiceChild\".\"refer\" = " + referYes + " "
							+ "AND (\"pncServiceChild\".\"visitDate\" - \"newBorn\".\"outcomeDate\") <= " + newborn + " "
							+ "AND \"pncServiceChild\".\"sateliteCenterName\" IS NOT NULL)"
							+ "THEN 1 ELSE NULL END) AS \"pncnewbornReferSatelite\" "
							+ "FROM \"pncServiceChild\", \"newBorn\" "
							+ "WHERE \"pncServiceChild\".\"providerId\" IN (" + MIS3RequestInfo.get("providerId") + ") "
							+ "AND \"pncServiceChild\".\"visitDate\" "
							+ "BETWEEN ('" + MIS3RequestInfo.get("startDate") + "') AND ('" + MIS3RequestInfo.get("endDate") +"') "
							+ "AND \"pncServiceChild\".\"healthId\" = \"newBorn\".\"healthId\" "
							+ "AND \"pncServiceChild\".\"pregNo\" = \"newBorn\".\"pregNo\" "
							+ "AND \"pncServiceChild\".\"childNo\" = \"newBorn\".\"childNo\" "
							+ "GROUP BY \"pncServiceChild\".\"providerId\") AS \"newbornReferTablePNC\", "
							+ "(SELECT \"newBorn\".\"providerId\", COUNT(\"newBorn\".\"refer\") AS \"newbornRefer\" "
							+ "FROM \"newBorn\" "
							+ "WHERE \"newBorn\".\"providerId\" IN (" + MIS3RequestInfo.get("providerId") + ") "
							+ "AND \"newBorn\".\"outcomeDate\" "
							+ "BETWEEN ('" + MIS3RequestInfo.get("startDate") + "') AND ('" + MIS3RequestInfo.get("endDate") +"') "
							+ "AND \"newBorn\".\"refer\"= " + referYes + " "
							+ "GROUP BY \"newBorn\".\"providerId\")  AS \"newbornReferTable\" "
						+ "WHERE \"newbornReferTablePNC\".\"providerId\" = \"newbornReferTable\".\"providerId\" "
						+ "ORDER BY \"newbornReferTablePNC\".\"providerId\""; 
					
			ResultSet rs = dbOp.dbExecute(dbObject,sql).getResultSet();
					
			while(rs.next()){
				
				isRSEmpty = false;
				
				JSONObject individualProviderNewbornRefer = new JSONObject();
								
				//individualProviderNewbornRefer.put("newbornrefer", rs.getString("newbornRefer"));
				//individualProviderNewbornRefer.put("pncnewbornrefer", rs.getString("pncnewbornRefer"));
				individualProviderNewbornRefer.put("newbornreferalcenter", (rs.getInt("newbornRefer")+ rs.getInt("pncnewbornReferCenter")));
				individualProviderNewbornRefer.put("newbornreferalsatelite", rs.getInt("pncnewbornReferSatelite"));
				
				individualProviderNewbornRefer.put("hasnewbornreferdata", 1);
				
				MIS3NewbornRefer.put(rs.getString("providerId"), individualProviderNewbornRefer);
			}
						
			if(isRSEmpty){
				String[] providerids = MIS3RequestInfo.getString("providerId").split(",");
				for(String id:providerids){
					
					JSONObject individualProviderReferStatus = new JSONObject();
					individualProviderReferStatus.put("hasnewbornreferdata", 2);
					
					MIS3NewbornRefer.put(id, individualProviderReferStatus);
				}
			}
			
			MIS3Report.put("mnchnewbornrefer", MIS3NewbornRefer);
		
			if(!rs.isClosed()){
				rs.close();
			}
		}
		catch(Exception e){
			System.out.println(e);
			e.printStackTrace();
		}				
	}
}