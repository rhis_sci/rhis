package org.sci.rhis.report.mis3;

import org.json.JSONObject;
import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.db.DBOperation;
import org.sci.rhis.db.UpzDBSelector;

/**
 * @author sabah.mugab
 * @since February, 2016
 */
public class Report {
	
	public static JSONObject MIS3Report = new JSONObject();
	
	public static JSONObject getReport(JSONObject MIS3RequestInfo) {
		
		DBOperation dbOp = new DBOperation();
		DBInfoHandler dbObject = new UpzDBSelector().upzDBInfo(MIS3RequestInfo);
		
		try{
			MIS3ProviderInfo.getProviderInfo(MIS3RequestInfo, MIS3Report, dbOp, dbObject); // Provider Info
			
			MIS3ANC.getANCReport(MIS3RequestInfo, MIS3Report, dbOp, dbObject); //ANC
			
			MIS3Delivery.getDeliveryReport(MIS3RequestInfo, MIS3Report, dbOp, dbObject); //Delivery
			
			MIS3PNCMother.getPNCMotherReport(MIS3RequestInfo, MIS3Report, dbOp, dbObject); //PNC Mother
			
			MIS3PNCChild.getPNCChildReport(MIS3RequestInfo, MIS3Report, dbOp, dbObject); //PNC Child
			
			MIS3PAC.getPACReport(MIS3RequestInfo, MIS3Report, dbOp, dbObject); //PAC
			
			MIS3NewbornRefer.getReferReport(MIS3RequestInfo, MIS3Report, dbOp, dbObject); //Refer			
		}
		catch(Exception e){
			System.out.println(e);
			e.printStackTrace();
		}
		finally{
			dbOp.dbObjectNullify(dbObject);
		}
		
		return MIS3Report;
	}
}