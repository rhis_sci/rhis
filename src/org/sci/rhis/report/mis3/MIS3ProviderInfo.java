package org.sci.rhis.report.mis3;

import java.sql.ResultSet;

import org.json.JSONObject;
import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.db.DBOperation;

/**
 * @author sabah.mugab
 * @since June, 2016
 */
public class MIS3ProviderInfo {

	public static void getProviderInfo(JSONObject MIS3RequestInfo, JSONObject MIS3Report, DBOperation dbOp, DBInfoHandler dbObject) {
		
		
		JSONObject MIS3ReportProvider = new JSONObject();
		
		try{
			MIS3Report.put("providerlist", MIS3RequestInfo.get("providerId"));
						
			String sql = "select \"ProviderDB\".\"ProvCode\", \"ProviderDB\".\"ProvName\", \"ProviderDB\".\"FacilityName\", \"Zilla\".\"ZILLANAME\", \"Upazila\".\"UPAZILANAME\", \"Unions\".\"UNIONNAME\" from \"ProviderDB\" "
					+ "join \"Zilla\" on \"Zilla\".\"ZILLAID\" = \"ProviderDB\".zillaid "
					+ "join \"Upazila\" on \"Upazila\".\"ZILLAID\" = \"ProviderDB\".zillaid and \"Upazila\".\"UPAZILAID\" = \"ProviderDB\".upazilaid "
					+ "join \"Unions\" on \"Unions\".\"ZILLAID\" = \"ProviderDB\".zillaid and \"Unions\".\"UPAZILAID\" = \"ProviderDB\".upazilaid and \"Unions\".\"UNIONID\" = \"ProviderDB\".unionid "
					+ "where \"ProviderDB\".\"ProvCode\" IN ( " + MIS3RequestInfo.get("providerId") + " )";
			
			ResultSet rs = dbOp.dbExecute(dbObject,sql).getResultSet();
					
			while(rs.next()){
				
				JSONObject individualProviderInfo = new JSONObject();
				
				individualProviderInfo.put("ProvName", rs.getString("ProvName"));
				individualProviderInfo.put("facilityname", rs.getString("FacilityName"));
				individualProviderInfo.put("zillaname", rs.getString("ZILLANAME"));
				individualProviderInfo.put("upazilaname", rs.getString("UPAZILANAME"));
				individualProviderInfo.put("unionname", rs.getString("UNIONNAME"));
				
				individualProviderInfo.put("hasProviderdata", 1);
				
				MIS3ReportProvider.put(rs.getString("ProvCode"), individualProviderInfo);
			}
			
			MIS3Report.put("providerinfo", MIS3ReportProvider);
			
			if(!rs.isClosed()){
				rs.close();
			}
		}
		catch(Exception e){
			System.out.println(e);
			e.printStackTrace();
		}		
	}
}