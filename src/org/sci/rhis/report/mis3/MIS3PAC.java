package org.sci.rhis.report.mis3;

import java.sql.ResultSet;

import org.json.JSONObject;
import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.db.DBOperation;

/**
 * @author sabah.mugab
 * @since February, 2016
 */
public class MIS3PAC {
	
	public static void getPACReport(JSONObject MIS3RequestInfo, JSONObject MIS3Report, DBOperation dbOp, DBInfoHandler dbObject){
	
		JSONObject MIS3ReportPAC = new JSONObject();
		boolean isRSEmpty = true;
		
		try{
			String sql = "SELECT \"pacService\".\"providerId\", COUNT (\"pacService\".\"serviceId\") AS serviceCount "
						+ "FROM \"pacService\" "
						+ "WHERE \"pacService\".\"providerId\" IN ( " + MIS3RequestInfo.get("providerId") + ") "
						+ "AND (\"pacService\".\"serviceSource\" IS NULL OR \"pacService\".\"serviceSource\" = '0') "
						+ "AND \"pacService\".\"visitDate\" BETWEEN ('" + MIS3RequestInfo.get("startDate") + "') AND ('" + MIS3RequestInfo.get("endDate") +"') "
						+ "GROUP BY \"pacService\".\"providerId\"";
			
			ResultSet rs = dbOp.dbExecute(dbObject,sql).getResultSet();
					
			while(rs.next()){
				
				isRSEmpty = false;
				JSONObject individualProviderPAC = new JSONObject();
				
				individualProviderPAC.put("pacservicecount", rs.getString("serviceCount"));
				
				individualProviderPAC.put("haspacdata", 1);
				
				MIS3ReportPAC.put(rs.getString("providerId"), individualProviderPAC);
			}
			
			if(isRSEmpty){
				String[] providerids = MIS3RequestInfo.getString("providerId").split(",");
				for(String id:providerids){
					
					JSONObject individualProviderPACStatus = new JSONObject();
					individualProviderPACStatus.put("haspacdata", 2);
					
					MIS3ReportPAC.put(id, individualProviderPACStatus);
				}
			}
			
			MIS3Report.put("pac", MIS3ReportPAC);
			
			if(!rs.isClosed()){
				rs.close();
			}
		}
		catch(Exception e){
			System.out.println(e);
			e.printStackTrace();
		}			
	}
}