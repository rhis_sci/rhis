package org.sci.rhis.pncmother;

import org.json.JSONObject;
import org.sci.rhis.db.*;
import org.sci.rhis.util.CheckSystemEntryDate;
import org.sci.rhis.util.JsonHandler;

/**
 * @author sabah.mugab
 * @since July, 2015
 */
public class PNCVisitMother {
	
	final static int PNCMOTHERSERVICETYPE = 3;
	
	public static JSONObject getDetailInfo(JSONObject PNCMotherInfo) {
		
		JSONObject PNCVisitsMother = new JSONObject();
		PNCMotherInfo = new JsonHandler().addJsonKeyValueStockDistribution(new JSONKeyMapper().setRequiredKeys(PNCMotherInfo, "PNCMOTHER"), PNCMOTHERSERVICETYPE);
		DBOperation dbOp = new DBOperation();
		DBInfoHandler dbObject = new UpzDBSelector().upzDBInfo(PNCMotherInfo);
		QueryBuilder dynamicQueryBuilder = new QueryBuilder();
		
		try{
			if(PNCMotherInfo.get("pncMLoad").equals("")){
//				InsertPNCVisitMotherInfo.createPNCVisitMother(dbOp, dbObject, PNCMotherInfo, dynamicQueryBuilder);

				//check if systementrydate exist in requestjson and DB to remove duplicate entry
				if (PNCMotherInfo.has("systemEntryDate") && !CheckSystemEntryDate.systemEntryDateExist(PNCMotherInfo, dbOp, dbObject, "PNCMOTHER","PNCMOTHER_systemEntryDate","PNCMOTHER_healthid")) {
					InsertPNCVisitMotherInfo.createPNCVisitMother(dbOp, dbObject, PNCMotherInfo, dynamicQueryBuilder);
				}else if(!PNCMotherInfo.has("systemEntryDate")){
					InsertPNCVisitMotherInfo.createPNCVisitMother(dbOp, dbObject, PNCMotherInfo, dynamicQueryBuilder);
				}
			}
			else if(PNCMotherInfo.get("pncMLoad").equals("update")){
				UpdatePNCVisitMotherInfo.updatePNCVisitMother(dbOp, dbObject, PNCMotherInfo, dynamicQueryBuilder);
			}
			else if(PNCMotherInfo.get("pncMLoad").equals("delete")){			
				DeletePNCVisitMotherInfo.deletePNCVisitMother(dbOp, dbObject, PNCMotherInfo, dynamicQueryBuilder);
			}
			
			PNCVisitsMother = RetrievePNCVisitMotherInfo.getPNCVisitsMother(dbOp, dbObject, PNCMotherInfo,PNCVisitsMother, dynamicQueryBuilder);
		}
		catch(Exception e){
			e.printStackTrace();
		}
		finally{
			dbOp.dbObjectNullify(dbObject);
		}
		return PNCVisitsMother;
	}
}