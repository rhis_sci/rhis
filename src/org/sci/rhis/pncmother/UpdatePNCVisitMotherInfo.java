package org.sci.rhis.pncmother;

import java.sql.ResultSet;

import org.json.JSONObject;
import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.db.DBOperation;
import org.sci.rhis.db.QueryBuilder;
import org.sci.rhis.distribution.StockDistributionRequest;
import org.sci.rhis.util.JsonHandler;

/**
 * @author sabah.mugab
 * @since February, 2017
 */
public class UpdatePNCVisitMotherInfo {
	
	public static boolean updatePNCVisitMother(DBOperation dbOp, DBInfoHandler dbObject, JSONObject PNCVisitMother, QueryBuilder dynamicQueryBuilder) {
		
		try{
			String returningSql = " RETURNING " + dynamicQueryBuilder.getColumn("", "PNCMOTHER_pnctreatment");
			ResultSet rs = dbOp.dbExecute(dbObject,(dynamicQueryBuilder.getUpdateQuery(
									new JsonHandler().addJsonKeyValueEdit(PNCVisitMother, "PNCMOTHER")) + returningSql)).getResultSet();
			
			StockDistributionRequest.updateDistributionInfoHandler(rs, dynamicQueryBuilder.getColumn("PNCMOTHER_pnctreatment"), 
										PNCVisitMother, dbOp, dbObject, dynamicQueryBuilder);
			
			if(!rs.isClosed()){
				rs.close();
			}
			return true;
		}
		catch(Exception e){
			e.printStackTrace();
			return false;
		}		
	}
}