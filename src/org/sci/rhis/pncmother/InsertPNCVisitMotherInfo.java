package org.sci.rhis.pncmother;

import java.sql.ResultSet;

import org.json.JSONObject;
import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.db.DBOperation;
import org.sci.rhis.db.QueryBuilder;
import org.sci.rhis.distribution.StockDistributionRequest;
import org.sci.rhis.util.JsonHandler;

/**
 * @author sabah.mugab
 * @since July, 2015
 */
public class InsertPNCVisitMotherInfo {
	
	public static boolean createPNCVisitMother(DBOperation dbOp, DBInfoHandler dbObject, JSONObject PNCMotherInfo, QueryBuilder dynamicQueryBuilder) {
		
		try{
			ResultSet rs = dbOp.dbExecute(dbObject,dynamicQueryBuilder.getInsertQuery
					(new JsonHandler().addJsonKeyValueEdit(new JsonHandler().addJsonKeyIncrementalField
							(PNCMotherInfo,"serviceId"), "PNCMOTHER"))).getResultSet();
			
	        StockDistributionRequest.insertDistributionInfoHandler(rs.next(), PNCMotherInfo, dbOp, dbObject, dynamicQueryBuilder);
	        
	        if(!rs.isClosed()){
				rs.close();
			}
			return true;
		}
		catch(Exception e){
			e.printStackTrace();
			return false;
		}		
	}
}