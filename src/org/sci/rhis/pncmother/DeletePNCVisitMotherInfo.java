package org.sci.rhis.pncmother;

import java.sql.ResultSet;

import org.json.JSONObject;
import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.db.DBOperation;
import org.sci.rhis.db.QueryBuilder;
import org.sci.rhis.distribution.StockDistributionRequest;
import org.sci.rhis.util.JsonHandler;

/**
 * @author armaan.islam
 * @since November,2015
 */
public class DeletePNCVisitMotherInfo {
	
	public static boolean deletePNCVisitMother(DBOperation dbOp, DBInfoHandler dbObject, JSONObject PNCMotherInfo, QueryBuilder dynamicQueryBuilder) {
		
		try{
	        String returningSql = " RETURNING " + dynamicQueryBuilder.getColumn("", "PNCMOTHER_pnctreatment");
			ResultSet rs = dbOp.dbExecute(dbObject,dynamicQueryBuilder.getDeleteQuery
					(new JsonHandler().addJsonKeyValueEdit(new JsonHandler().addJsonKeyMaxField
							(PNCMotherInfo,"serviceId"), "PNCMOTHER")) + returningSql).getResultSet();
			
			StockDistributionRequest.deleteDistributionInfoHandler(rs, dynamicQueryBuilder.getColumn("PNCMOTHER_pnctreatment"), 
											PNCMotherInfo, dbOp, dbObject, dynamicQueryBuilder);
			
			if(!rs.isClosed()){
				rs.close();
			}
			return true;
		}
		catch(Exception e){
			e.printStackTrace();
			return false;
		}		
	}
}