package org.sci.rhis.uploaddownload;

import java.sql.ResultSet;

import org.json.JSONObject;
import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.db.DBOperation;
import org.sci.rhis.db.QueryBuilder;
import org.sci.rhis.db.UpzDBSelector;
import org.sci.rhis.util.FileOperation;

/**
 * @author sabah.mugab
 * @since May, 2018
 */
public class UploadDownloadStatusHandler {
	
	public static JSONObject changeStatus(JSONObject uploadDownloadStatus) {
		
		JSONObject status = new JSONObject();
		uploadDownloadStatus.put("providerType", null);
		DBOperation dbOp = new DBOperation();
		DBInfoHandler dbObject = new UpzDBSelector().upzDBInfo(uploadDownloadStatus);
		QueryBuilder dynamicQueryBuilder = new QueryBuilder();
				
		try{
			status.put("downloadstatus", "not-acknowledged");
			String sql;			
			//if(uploadDownloadStatus.has("downloadstatus") && uploadDownloadStatus.getString("downloadstatus").equalsIgnoreCase("true")){
			if(uploadDownloadStatus.has("downloadstatus")){
				sql = "UPDATE " + dynamicQueryBuilder.getTable("PROVIDERNODE") + " SET "
							+ dynamicQueryBuilder.getColumn("", "PROVIDERNODE_downloadstatus", new String[]{"0"},"=")
							+ " WHERE " + dynamicQueryBuilder.getColumn("", "PROVIDERNODE_providerid", new String[]{uploadDownloadStatus.getString("providerid")},"=")
							+ " RETURNING *";
				
				ResultSet rs = dbOp.dbExecute(dbObject, sql).getResultSet();
				if(rs.next()){
								    					
					recordSymmetricdsRegistrationOpenRequest(rs.getString(dynamicQueryBuilder.getColumn("PROVIDERNODE_servernodeid")), 
															rs.getString(dynamicQueryBuilder.getColumn("PROVIDERNODE_nodeid")));
				    status.put("downloadstatus", "acknowledged");System.out.println("db updated and registration opened");
				}				
			}
			
			if(uploadDownloadStatus.has("uploadstatus")){
				sql = "UPDATE " + dynamicQueryBuilder.getTable("PROVIDERNODE") + " SET "
							+ dynamicQueryBuilder.getColumn("", "PROVIDERNODE_syncrequest", new String[]{"0"},"=")
							+ " WHERE " + dynamicQueryBuilder.getColumn("", "PROVIDERNODE_providerid", new String[]{uploadDownloadStatus.getString("providerid")},"=");
				System.out.println("\n\n\n\n**************UPLOAD Status: " + sql + "\n\n\n\n\n\n\n\n\n");				
				dbOp.dbStatementExecute(dbObject, sql);								
				//status.put("uploadstatus", "complete");
			}
			
			if(uploadDownloadStatus.has("symserver") && uploadDownloadStatus.has("symnode")){
				recordSymmetricdsRegistrationOpenRequest(uploadDownloadStatus.getString("symserver"), 
														 uploadDownloadStatus.getString("symnode"));
				if(uploadDownloadStatus.getString("symnode").equals("")){
					status.put("symtablerequest", "recorded");
				}
				else{
					status.put("symregistration", "recorded");
				}														
			}
			
			if(uploadDownloadStatus.has("checksum")){
				if(uploadDownloadStatus.has("providerid")){
					FileOperation.writeToFile(System.getProperty("catalina.base") + "/webapps/unsync_data/" + uploadDownloadStatus.getString("providerid") + "_checksum", 
							uploadDownloadStatus.getString("checksum"));
					status.put("checksum", "acknowledged");
					//status.put("downloadstatus", "acknowledged"); //quick/hacky solution. need proper implementation
				}
			}			
			return status;
		}
		catch(Exception e){
			e.printStackTrace();
			return status;
		}
		finally{
			dbOp.dbObjectNullify(dbObject);			
		}		
	}
	
	private static void recordSymmetricdsRegistrationOpenRequest(String serverNode, String nodeId){
		//String unixCommand = "bash /var/lib/symmetricds/symmetric-server-3.7.27/bin/symadmin --engine " + serverNode;
		String unixCommand = "bash /var/lib/symmetricds/symmetric-server-3.9.0/bin/symadmin --engine " + serverNode;
		if(nodeId.equals("")){
			unixCommand = unixCommand + " create-sym-tables";
		}
		else{
			unixCommand = unixCommand + " open-registration FWV \'" + nodeId + "\' > /dev/null &";
		}
		
		FileOperation.appendContentToFile(System.getProperty("catalina.base") + "/webapps/unsync_data/symmetricds_registration_request.sh", 
								unixCommand);
	}
}