package org.sci.rhis.general_patient;

import java.sql.ResultSet;

import org.json.JSONObject;
import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.db.DBOperation;
import org.sci.rhis.db.QueryBuilder;
import org.sci.rhis.distribution.StockDistributionRequest;
import org.sci.rhis.util.JsonHandler;

/**
 * @author jamil.zaman
 * @since April, 2016
 */

public class DeleteGPVisitInfo {

	public static boolean deleteGPVisit(JSONObject GPInfo, DBOperation dbOp, DBInfoHandler dbObject, QueryBuilder dynamicQueryBuilder) {
		
		try{		
			String returningSql = " RETURNING " + dynamicQueryBuilder.getColumn("", "GP_treatment");
			ResultSet rs = dbOp.dbExecute(dbObject,dynamicQueryBuilder.getDeleteQuery
					(new JsonHandler().addJsonKeyValueEdit(new JsonHandler().addJsonKeyMaxField
							(GPInfo,"serviceId"), "GP")) + returningSql).getResultSet();
			
			StockDistributionRequest.deleteDistributionInfoHandler(rs, dynamicQueryBuilder.getColumn("GP_treatment"), 
										GPInfo, dbOp, dbObject, dynamicQueryBuilder);
			
			if(!rs.isClosed()){
				rs.close();
			}
			return true;
			
			/*if(status && !GPInfo.getString("distributionId").equals("") && !GPInfo.getString("distributionId").startsWith("[")){
				HandleStockDistribution.deleteDistributionInfo(GPInfo, dbOp, dbObject);
			}*/
		}
		catch(Exception e){
			e.printStackTrace();
			return false;
		}					
	}
}