package org.sci.rhis.general_patient;

import java.sql.ResultSet;

import org.json.JSONObject;
import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.db.DBOperation;
import org.sci.rhis.db.QueryBuilder;
import org.sci.rhis.distribution.StockDistributionRequest;
import org.sci.rhis.util.JsonHandler;

/**
 * @author sabah.mugab
 * @since February, 2017
 */
public class UpdateGPVisitInfo {

	public static boolean updateGPVisit(JSONObject GPInfo, DBOperation dbOp, DBInfoHandler dbObject, QueryBuilder dynamicQueryBuilder) {
		
		try{		
			String returningSql = " RETURNING " + dynamicQueryBuilder.getColumn("", "GP_treatment");
			ResultSet rs = dbOp.dbExecute(dbObject,(dynamicQueryBuilder.getUpdateQuery
					(new JsonHandler().addJsonKeyValueEdit(GPInfo, "GP")) + returningSql)).getResultSet();
			
			StockDistributionRequest.updateDistributionInfoHandler(rs, dynamicQueryBuilder.getColumn("GP_treatment"), 
										GPInfo, dbOp, dbObject, dynamicQueryBuilder);
			
			if(!rs.isClosed()){
				rs.close();
			}
			/*
			if(status && !GPInfo.getString("treatment").equals("") && GPInfo.getString("treatment").contains("_")){
				HandleStockDistribution.updateDistributionInfo(GPInfo, dbOp, dbObject);
			}*/
			return true;
		}
		catch(Exception e){
			e.printStackTrace();
			return false;
		}					
	}
}