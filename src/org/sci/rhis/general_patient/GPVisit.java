package org.sci.rhis.general_patient;

import org.json.JSONObject;
import org.sci.rhis.client.ClientInfoUtil;
import org.sci.rhis.client.CreateRegNo;
import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.db.DBOperation;
import org.sci.rhis.db.JSONKeyMapper;
import org.sci.rhis.db.QueryBuilder;
import org.sci.rhis.db.UpzDBSelector;
import org.sci.rhis.util.CheckSystemEntryDate;
import org.sci.rhis.util.JsonHandler;

import java.util.Arrays;

/**
 * @author jamil.zaman
 * @since April, 2016
 */
public class GPVisit {
	
	final static int generalPatient = 5; //for registration number
	final static int GPSERVICETYPE = 6; //for item distribution
	
	public static JSONObject getDetailInfo(JSONObject GPInfo) {
		
		JSONObject GPVisits = new JSONObject();
		GPInfo = new JsonHandler().addJsonKeyValueStockDistribution(new JSONKeyMapper().setRequiredKeys(GPInfo, "GP"), GPSERVICETYPE);
		DBOperation dbOp = new DBOperation();
		DBInfoHandler dbObject = new UpzDBSelector().upzDBInfo(GPInfo);
		QueryBuilder dynamicQueryBuilder = new QueryBuilder();
		GPInfo.put("serviceCategory", generalPatient);
		
		try{
			if(GPInfo.get("gpLoad").equals("")){
                //check if systementrydate exist in requestjson and DB to remove duplicate entry
//				GPInfo.put("systemEntryDate","2021-04-22 15:10:02");
//				String[] columnArray = new String[]{"GP_systemEntryDate","GP_healthId"};
				if (GPInfo.has("systemEntryDate") && !CheckSystemEntryDate.systemEntryDateExist(GPInfo, dbOp, dbObject, "GP","GP_systemEntryDate","GP_healthId")) {
					GPInfo = InsertGPVisitInfo.createGPVisit(GPInfo, dbOp, dbObject, dynamicQueryBuilder);
				}else if(!GPInfo.has("systemEntryDate")){
					GPInfo = InsertGPVisitInfo.createGPVisit(GPInfo, dbOp, dbObject, dynamicQueryBuilder);
				}
			}
			else if(GPInfo.get("gpLoad").equals("update")){
				UpdateGPVisitInfo.updateGPVisit(GPInfo, dbOp, dbObject, dynamicQueryBuilder);
			}
			else if(GPInfo.get("gpLoad").equals("delete")){
				DeleteGPVisitInfo.deleteGPVisit(GPInfo, dbOp, dbObject, dynamicQueryBuilder);
			}

			if(GPInfo.get("gpLoad").equals("") && GPInfo.has("serviceId") && GPInfo.getString("serviceId").equals("1")){
				CreateRegNo.pushReg(dbOp, dbObject, GPInfo, GPVisits);
			}
			
			GPVisits = RetrieveGPVisitInfo.getGPVisits(GPInfo,GPVisits, dbOp, dbObject, dynamicQueryBuilder);
			GPVisits = ClientInfoUtil.getRegNumber(dbOp, dbObject, GPInfo, GPVisits);
		
		}
		catch(Exception e){
			e.printStackTrace();
		}
		finally{
			dbOp.dbObjectNullify(dbObject);		
		}
		return GPVisits;
	}
}