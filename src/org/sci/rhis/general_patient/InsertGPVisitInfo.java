package org.sci.rhis.general_patient;

import java.sql.ResultSet;

import org.json.JSONObject;
import org.sci.rhis.client.ClientInfoUtil;
import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.db.DBOperation;
import org.sci.rhis.db.QueryBuilder;
import org.sci.rhis.distribution.StockDistributionRequest;
import org.sci.rhis.util.JsonHandler;

/**
 * @author jamil.zaman
 * @since April, 2016
 */
public class InsertGPVisitInfo {
	
	public static JSONObject createGPVisit(JSONObject GPInfo, DBOperation dbOp, DBInfoHandler dbObject, QueryBuilder dynamicQueryBuilder) {
		
		try{		
			ResultSet rs = dbOp.dbExecute(dbObject, dynamicQueryBuilder.getInsertQuery
					(new JsonHandler().addJsonKeyValueEdit(new JsonHandler().addJsonKeyIncrementalField
					(GPInfo,"serviceId"), "GP"))).getResultSet();
			
			GPInfo = StockDistributionRequest.insertDistributionInfoHandler(rs, dynamicQueryBuilder.getColumn("GP_treatment"), 
							dynamicQueryBuilder.getColumn("GP_serviceId"), GPInfo, dbOp, dbObject, dynamicQueryBuilder);
			
			/*if(status && !GPInfo.getString("treatment").equals("") && GPInfo.getString("treatment").contains("_")){
				HandleStockDistribution.insertDistributionInfo(GPInfo, dbOp, dbObject);
			}*/
			
			if(!rs.isClosed()){
				rs.close();
			}
			
			if(!GPInfo.get("mobileNo").equals("")){			
				ClientInfoUtil.updateClientMobileNo(GPInfo, dbOp, dbObject);
			}
			return GPInfo;
		}
		catch(Exception e){
			e.printStackTrace();
			return GPInfo;
		}			
	}
}