package org.sci.rhis.general_patient;

import java.sql.ResultSet;

import org.json.JSONObject;
import org.sci.rhis.db.DBInfoHandler;
import org.sci.rhis.db.DBOperation;
import org.sci.rhis.db.QueryBuilder;
import org.sci.rhis.util.JsonHandler;

/**
 * @author jamil.zaman
 * @since April, 2016
 */
public class RetrieveGPVisitInfo {

	public static JSONObject getGPVisits(JSONObject GPInfo, JSONObject GPVisits, DBOperation dbOp, DBInfoHandler dbObject, QueryBuilder dynamicQueryBuilder) {
			
		try{
			String sql = "SELECT * FROM " + dynamicQueryBuilder.getTable("GP") + " "
						+ "WHERE " + dynamicQueryBuilder.getColumn("table", "GP_healthId",new String[]{GPInfo.getString("healthId")},"=")
						+ " ORDER BY " + dynamicQueryBuilder.getColumn("table", "GP_serviceId") + " ASC";
									
			ResultSet rs = dbOp.dbExecute(dbObject,sql).getResultSet();
			GPVisits.put("count", 0);
			GPInfo.put("distributionJson","treatment");			
			
			while(rs.next()){
				GPVisits.put(rs.getString(dynamicQueryBuilder.getColumn("GP_serviceId")),  new JsonHandler().getServiceDetail(rs, GPInfo, "GP", dynamicQueryBuilder, 2));
				GPVisits.put("count", (GPVisits.getInt("count")+1));				
			}			
						
			if(!rs.isClosed()){
				rs.close();
			}
			return GPVisits;
		}
		catch(Exception e){
			e.printStackTrace();
			return new JSONObject();
		}	
	}
}